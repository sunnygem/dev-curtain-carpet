<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    //$query = 'select product_id from dtb_products limit 1';
    //$res = $objQuery->select( 'product_id', 'dtb_products', array(), array( 'limit' => 1 ) );

    // CSVを読み込む
    $target = 'side_open_matrix';
    //$target = 'center_open_matrix';
    $target_files = $target . '.csv';
    if ( file_exists( $target_files ) )
    {
        $files = new SplFileObject( $target_files );
        $files->setFlags( SplFileObject::READ_CSV );
        foreach( $files as $key => $val )
        {
            if ( $key === 0 ) continue; // header行
            if ( is_null( $val[0] ) === false )
            {
                $title      = $val[0];      
                $width1     = $val[1];      
                $width2     = $val[2];      
                $width3     = $val[3];      
                $width4     = $val[4];      
                $width5     = $val[5];      
                $width6     = $val[6];      
                $width7     = $val[7];      
                $length1    = $val[8];      
                $length2    = $val[9];      
                $length3    = $val[10];     
                $length4    = $val[11];     
                $length5    = $val[12];     
                $length6    = $val[13];     
                $length7    = $val[14];     
                $price1_1   = $val[15];     
                $price1_2   = $val[16];     
                $price1_3   = $val[17];     
                $price1_4   = $val[18];     
                $price1_5   = $val[19];     
                $price1_6   = $val[20];     
                $price1_7   = $val[21];     
                $price2_1   = $val[22];     
                $price2_2   = $val[23];     
                $price2_3   = $val[24];     
                $price2_4   = $val[25];     
                $price2_5   = $val[26];     
                $price2_6   = $val[27];     
                $price2_7   = $val[28];     
                $price3_1   = $val[29];     
                $price3_2   = $val[30];     
                $price3_3   = $val[31];     
                $price3_4   = $val[32];     
                $price3_5   = $val[33];     
                $price3_6   = $val[34];     
                $price3_7   = $val[35];     
                $price4_1   = $val[36];     
                $price4_2   = $val[37];     
                $price4_3   = $val[38];     
                $price4_4   = $val[39];     
                $price4_5   = $val[40];     
                $price5_1   = $val[41];     
                $price5_2   = $val[42];     
                $price5_3   = $val[43];     
                $price5_4   = $val[44];     
                $price5_5   = $val[45];     
                $price6_1   = $val[46];     
                $price6_2   = $val[47];     
                $price6_3   = $val[48];     
                $price6_4   = $val[49];     
                $price6_5   = $val[50];     
                $price7_1   = $val[51];     
                $price7_2   = $val[52];     
                $price7_3   = $val[53];     
                $price7_4   = $val[54];     
                $price7_5   = $val[55];     

                $columns = compact('title' ,'length1' ,'length2' ,'length3' ,'length4' ,'length5' ,'length6' ,'length7' ,'width1' ,'width2' ,'width3' ,'width4' ,'width5' ,'width6' ,'width7' ,'price1_1' ,'price1_2' ,'price1_3' ,'price1_4' ,'price1_5' ,'price1_6' ,'price1_7' ,'price2_1' ,'price2_2' ,'price2_3' ,'price2_4' ,'price2_5' ,'price2_6' ,'price2_7' ,'price3_1' ,'price3_2' ,'price3_3' ,'price3_4' ,'price3_5' ,'price3_6' ,'price3_7' ,'price4_1' ,'price4_2' ,'price4_3' ,'price4_4' ,'price4_5' ,'price5_1' ,'price5_2' ,'price5_3' ,'price5_4' ,'price5_5' ,'price6_1' ,'price6_2' ,'price6_3' ,'price6_4' ,'price6_5' ,'price7_1' ,'price7_2' ,'price7_3' ,'price7_4' ,'price7_5' ) ;
                $objQuery->insert( $target, $columns );
            }
        }
    }
    else
    {
        throw new Exception( 'file not exists' );
    }
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}

