<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

// 本番に合わせる ishibashi
//$classcategory_id1 = 3246;
//$classcategory_id2 = 3247;
$classcategory_id1 = 3084;
$classcategory_id2 = 3085;

try
{
    // CSVを読み込む
    $target_files = 'zakka_data.csv';
    if ( file_exists( $target_files ) )
    {
        $files = new SplFileObject( $target_files );
        $files->setFlags( SplFileObject::READ_CSV );
        foreach( $files as $key => $val )
        {
            if ( $key !== 0 ) continue; // header行
            if ( is_null( $val[1] ) === false )
            {
                $product_code      = mb_convert_kana( $val[1], 'KVas' ); // 商品コード
                $name              = $val[2]; // 商品名
                $category_name     = $val[3]; // カテゴリ名
                $price02           = $val[4]; // 価格
                $main_comment      = $val[5]; // メインコメント
                $image_folder_name = $val[6]; // 画像フォルダ名
                $comment3          = $val[7]; // 検索ワード

                // 画像名の設定
                if ( file_exists( IMAGE_SAVE_REALDIR . $image_folder_name ) )
                {
                    for( $i = 1; $i < 9; $i++ )
                    {
                        $image_path = $image_folder_name . '/' . $image_folder_name . '_' . $i . '.jpg';
                        $variable  = 'sub_image' . $i;
                        $variable2 = 'sub_large_image' . $i;
                        if ( file_exists( IMAGE_SAVE_REALDIR . $image_path ) )
                        {
                            if ( $i === 1 )
                            {
                                $main_list_image  = $image_path;
                                $main_image       = $image_path;
                                $main_large_image = $image_path;
                            }
                            $$variable  = $image_path;
                            $$variable2 = $image_path;

                        }
                        else
                        {
                            $$variable  = null;
                            $$variable2 = null;
                        }

                    }
                }
                else
                {
                    throw new Exception( 'image folder not exists' );
                }

                $columns = compact( 'name', 'main_comment', 'comment3', 'main_list_image', 'main_image', 'main_large_image'
                    ,'sub_image1'
                    ,'sub_large_image1'
                    ,'sub_image2'
                    ,'sub_large_image2'
                    ,'sub_image3'
                    ,'sub_large_image3'
                    ,'sub_image4'
                    ,'sub_large_image4'
                    ,'sub_image5'
                    ,'sub_large_image5'
                    ,'sub_image6'
                    ,'sub_large_image6'
                    ,'sub_image7'
                    ,'sub_large_image7'
                    ,'sub_image8'
                    ,'sub_large_image8'
                    ,'sub_image9'
                    ,'sub_large_image9'
                ) ;
                $columns['status'] = 1; // 1:公開 2:非公開

                // product_codeから更新をかける
                $res = $objQuery->select( 'product_class_id,product_id', 'dtb_products_class', 'product_code = ? AND del_flg = ?', array( $product_code, 0 ) );
                if ( isset( $res[0]['product_id'] ) )
                {
                    $product_id = $res[0]['product_id'];
                    $product_class_id = $res[0]['product_class_id'];

                    // 商品情報の更新
                    $objQuery->update( 'dtb_products', $columns, 'product_id=?', array( $product_id ) );
                    $sqlval = array(
                        'price02'          => $price02
                        ,'stock_unlimited' => 1
                        ,'product_type_id' => 1
                        ,'classcategory_id1' => $classcategory_id1
                        ,'classcategory_id2' => $classcategory_id2
                    );
                    $objQuery->update( 'dtb_products_class', $sqlval, 'product_class_id=?', array( $product_class_id ) );
                }
                else
                {
                    $res = $objQuery->select( 'product_id', 'dtb_products', 'name = ? AND del_flg = ?', array( $product_code, 0 ) );
                    if ( isset( $res[0]['product_id'] ) )
                    {
                        $product_id = $res[0]['product_id'];
                    }
                    else
                    {
                        $product_id = $objQuery->nextVal( 'dtb_products_product_id' );
                        $columns['product_id'] = $product_id;
                        // 新規登録
                        $objQuery->insert('dtb_products', $columns );
                    }
                    $sqlval = array(
                        'product_class_id' => $objQuery->nextVal( 'dtb_products_class_product_class_id' )
                        ,'product_id'      => $product_id
                        ,'product_code'    => $product_code
                        ,'price02'         => $price02
                        ,'stock_unlimited' => 1
                        ,'product_type_id' => 1
                        ,'classcategory_id1' => $classcategory_id1
                        ,'classcategory_id2' => $classcategory_id2
                    );
                    $objQuery->insert( 'dtb_products_class', $sqlval );


                }
                // dtb_product_categories
                $res = $objQuery->count( 'dtb_category', 'parent_category_id=? and category_name=?', array( 1858, $category_name ) );
                if ( (int)$res === 0 )
                {
                    $category_id = $objQuery->nextVal( 'dtb_category_category_id' );
                    $sqlval = array(
                        'category_id'         => $category_id
                        ,'category_name'      => $category_name
                        ,'parent_category_id' => 1858
                        ,'level'              => 2
                        ,'rank'               => $objQuery->max( 'rank', 'dtb_category', 'parent_category_id=?', array( 1858 ) ) + 1
                    );
                    $objQuery->insert( 'dtb_category', $sqlval );

                    $rank = $objQuery->max( 'rank', 'dtb_product_categories' ) + 1;
                    $sqlval = array(); 
                    $sqlval['product_id']  = $product_id;
                    $sqlval['category_id'] = $category_id;
                    $sqlval['rank']        = $rank;
                    $objQuery->insert( 'dtb_product_categories', $sqlval );
                }
                else
                {
                    $res  = $objQuery->select( 'category_id', 'dtb_category', 'parent_category_id=? and category_name=?', array( 1858, $category_name ) );
                    $category_id = $res[0]['category_id'];
                    $rank = $objQuery->max( 'rank', 'dtb_product_categories' ) + 1;
                    $sqlval = array(); 
                    $sqlval['product_id']  = $product_id;
                    $sqlval['category_id'] = $category_id;
                    $sqlval['rank']        = $rank;

                    $res2 = $objQuery->count( 'dtb_product_categories', 'product_id=? and category_id=?', array( $product_id, $category_id ) );
                    if ( (int)$res2 === 0 )
                    {
                        $objQuery->insert( 'dtb_product_categories', $sqlval );
                    }
                }
            }
        }
    }
    else
    {
        throw new Exception( 'file not exists' );
    }
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}
