<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    $target_files = 'easy_order_matrix.csv';

    $files = new SplFileObject( $target_files );
    $files->setFlags( SplFileObject::READ_CSV );
    foreach( $files as $key => $val )
    {
        if ( $key === 0 || strlen( $val[0] ) === 0 ) continue;

        $product_code = $val[0];
        $product_name = $val[1];
        $price01      = $val[3];
        $matrix1      = $val[4];
        $matrix2      = $val[5];

        $product_ids = [];
        $table = 'dtb_products_class';
        $res = $objQuery->select( 'product_id', 'dtb_products_class', 'product_code = ? AND del_flg = ? order by product_class_id', array( $product_code, 0 ) );
        if ( isset( $res[0]['product_id'] ) )
        {
            foreach( $res as $key2 => $val2 )
            {
                $product_ids[] = $val2['product_id'];
            }
        }
        else
        {
            $product_id = $objQuery->nextVal( 'dtb_products_product_id' );
            $product_ids[] = $product_id;
            $sqlval = [
                'product_id'  => $product_id
                ,'name'       => $product_name
                ,'status'     => 1
                ,'sample_flg' => 1
            ];
            $objQuery->insert( 'dtb_products', $sqlval );
        }

        foreach( $product_ids as $product_id )
        {
            $tmp = [
                'plg_productoptions_flg' => '4,3,2,1'
                ,'status' => 1
            ];
            $objQuery->update( 'dtb_products', $tmp, 'product_id=?', array( $product_id ) );

            $category_ids = [ 1706, 1796 ]; // カーテン、イージーオーダーカーテン
            foreach( $category_ids as $category_id )
            {
                $res = $objQuery->select( '*', 'dtb_product_categories', 'product_id=? AND category_id=?', array( $product_id, $category_id ) );
                if ( count( $res ) === 0 )
                {
                    $rank = $objQuery->max( 'rank', 'dtb_product_categories', 'product_id=?', array( $product_id ) ) + 1;
                    $objQuery->insert( 'dtb_product_categories', array( 'product_id' => $product_id, 'category_id' => $category_id, 'rank' => $rank ) );
                }
            }

            $classcategory_id1 = 127;

            $sqlval = array(
                'product_id'      => $product_id
                ,'classcategory_id1' => $classcategory_id1
                ,'classcategory_id2' => 0
                ,'product_type_id'   => 1
                ,'product_code'      => $product_code
                ,'stock_unlimited'   => 1
                ,'price01'           => $price01
                ,'price02'           => 0
            );

            $res = $objQuery->select( '*', 'dtb_class', 'name=? order by class_id', array( $matrix1 ) );
            if ( isset( $res[0]['class_id'] ) )
            {
                $class_id = $res[0]['class_id'];
            }
            else
            {
                $class_id = $objQuery->nextVal( 'dtb_class_class_id' );
                $tmp = [
                    'class_id' => $class_id
                    ,'name'    => $matrix1
                    ,'rank'    => $objQuery->max( 'rank', 'dtb_class' ) + 1
                ];
                $objQuery->insert( 'dtb_class', $tmp );
            }

            //$matrix = $objQuery->select( '*', 'matrix', 'title=?', array( $matrix1 ) );

            //$objQuery->delete( 'dtb_products_class', 'product_code=?', array( $product_code ) );

            $matrix = $objQuery->select( '*', 'side_open_matrix', 'title=?', array( $matrix1 ) );
            $matrix = $matrix[0];
            $title = $matrix['title'];

            execute( $matrix, 1, $class_id, $objQuery, $sqlval, 1 );

            $matrix = $objQuery->select( '*', 'center_open_matrix', 'title=?', array( $matrix1 ) );
            $matrix = $matrix[0];
            $title = $matrix['title'];

            execute( $matrix, 1, $class_id, $objQuery, $sqlval, 2 );

            if ( strlen( trim( $matrix2 ) ) > 0 )
            {
//var_dump('matrix2',$matrix2);
                $classcategory_id1 = 128;   

                $sqlval = array(
                    'product_id'      => $product_id
                    ,'classcategory_id1' => $classcategory_id1
                    ,'classcategory_id2' => 0
                    ,'product_type_id'   => 1
                    ,'product_code'      => $product_code
                    ,'stock_unlimited'   => 1
                    ,'price01'           => $price01
                    ,'price02'           => 0
                );

//                $res = $objQuery->select( '*', 'dtb_class', 'name=? order by class_id', array( $matrix2 ) );
////var_dump('class res',$res);
//                if ( isset( $res[0]['class_id'] ) )
//                {
//                    $class_id = $res[0]['class_id'];
//                }
//                else
//                {
//                    $class_id = $objQuery->nextVal( 'dtb_class_class_id' );
//                    $tmp = [
//                        'class_id' => $class_id
//                        ,'name'    => $matrix2
//                        ,'rank'    => $objQuery->max( 'rank', 'dtb_class' ) + 1
//                    ];
//                    $objQuery->insert( 'dtb_class', $tmp );
//                }

                //$matrix = $objQuery->select( '*', 'matrix', 'title=?', array( $matrix2 ) );
                $matrix = $objQuery->select( '*', 'side_open_matrix', 'title=?', array( $matrix2 ) );
                $matrix = $matrix[0];
                $title = $matrix['title'];

                execute( $matrix, 1, $class_id, $objQuery, $sqlval, 1 );

                $matrix = $objQuery->select( '*', 'center_open_matrix', 'title=?', array( $matrix2 ) );
                $matrix = $matrix[0];
                $title = $matrix['title'];

                execute( $matrix, 1, $class_id, $objQuery, $sqlval, 2 );
            }
        }
    }

    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}
function execute( $matrix, $flg, $class_id, $objQuery, $sqlval, $flg2=null )
{
        $min = ( $flg === 1 ) ? 50 : 40;
        $length_min = 0;
        $length_max = 0;
        foreach( $matrix as $key => $val )
        {
            if ( preg_match( '/length(\d)/', $key, $match ) === 1 )
            {
                if ( strlen( $val ) === 0 ) continue;
                $length_num = $match[1];

                $length_min = ( $length_min === 0 ) ? $min : $length_max + 1;
                $length_max = $val;

                $width_min  = 0;
                $width_max  = 0;
                foreach( $matrix as $key2 => $val2 )
                {
                    if ( preg_match( '/width(\d)/', $key2, $match2 ) === 1 )
                    {
                        if ( strlen( $val2 ) === 0 ) continue;
                        $width_num = $match2[1];

                        $width_min  = ( $width_min  === 0 ) ? $min : $width_max  + 1;
                        $width_max  = $val2;

                        if ( $flg2 === null )
                        {
                            $name = 'order_' . $width_min .'_'.$width_max.'_'.$length_min.'_'.$length_max;
                        }
                        else if ( $flg2 === 1 )
                        {
                            $name = '巾' . $width_max . 'cm×丈' . $length_max . 'cm(1枚価格)';
                        }
                        else if ( $flg2 === 2 )
                        {
                            $name = '巾' . $width_max . 'cm×丈' . $length_max . 'cm(2枚価格)';
                        }

                        $res = $objQuery->select( 'classcategory_id', 'dtb_classcategory', 'name=? AND class_id=? order by classcategory_id', array( $name, $class_id ) );
                        if ( isset( $res[0]['classcategory_id'] ) )
                        {
                            $classcategory_id2 = $res[0]['classcategory_id'];
                        }
                        else
                        {
                            $classcategory_id2 = $objQuery->nextVal( 'dtb_classcategory_classcategory_id' );
                            $tmp = [
                                'classcategory_id' => $classcategory_id2
                                ,'name'            => $name
                                ,'class_id'        => $class_id
                                ,'rank'            => 1
                            ];
                            $objQuery->insert( 'dtb_classcategory', $tmp );
                        }
                        $price02 = $matrix['price'.$length_num.'_'.$width_num];
                        if ( strlen( $price02 ) > 0 )
                        {
                            $res = $objQuery->select( '*','dtb_products_class','product_id=? AND classcategory_id1=? AND classcategory_id2=? order by product_class_id', array( $sqlval['product_id'],$sqlval['classcategory_id1'],$classcategory_id2));
//var_dump('$class_id');
//var_dump($class_id);
//var_dump('$res');
//var_dump($res);
                            if ( count( $res ) === 0 )
                            {
                                $sqlval['product_class_id']  = $objQuery->nextVal( 'dtb_products_class_product_class_id' );
                                $sqlval['price02']           = $price02;
                                $sqlval['classcategory_id2'] = $classcategory_id2;
                                $objQuery->insert( 'dtb_products_class', $sqlval );
//var_dump($sqlval);
                                unset( $sqlval['product_class_id'], $sqlval['price02'], $sqlval['classcategory_id2'] );
                            }
                            else
                            {
                                $sqlval['price02']           = $price02;
                                $sqlval['classcategory_id2'] = $classcategory_id2;
                                $sqlval['del_flg']           = 0;
                                $objQuery->update( 'dtb_products_class', $sqlval, 'product_class_id=?', array( $res[0]['product_class_id'] ) );
                                unset( $sqlval['price02'], $sqlval['classcategory_id2'] );
                            }
                        }
                        else
                        {
                        var_dump('$price02がありません');
                        var_dump($length_num,$width_num,$classcategory_id2);
                        }
                    }
                }
            }
        }

}
