<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    //$query = 'select product_id from dtb_products limit 1';
    //$res = $objQuery->select( 'product_id', 'dtb_products', array(), array( 'limit' => 1 ) );

    // CSVを読み込む
    $target_files = 'comment_add_0604.csv';
    if ( file_exists( $target_files ) )
    {
        $files = new SplFileObject( $target_files );
        $files->setFlags( SplFileObject::READ_CSV );
        foreach( $files as $key => $val )
        {
            if ( $key === 0 ) continue; // header行
            if ( is_null( $val[0] ) === false )
            {
                $product_code  = mb_convert_kana( $val[0], 'KVas' ); // 商品コード
                $comment1      = trim( $val[1] );
                $comment2      = trim( $val[2] );

                // product_codeから更新をかける
                $res = $objQuery->select( 'product_id', 'dtb_products_class', 'product_code = ? AND del_flg = ?', array( $product_code, 0 ) );

                if ( isset( $res[0]['product_id'] ) )
                {
                    $product_id = $res[0]['product_id'];
                    $products = $objQuery->select( 'main_comment', 'dtb_products', 'product_id=?', array( $product_id ) );
                    $main_comment = $products[0]['main_comment'];
                    if ( strlen( $main_comment ) > 0 )
                    {
                        if ( strlen( $comment1 ) > 0 )
                        {
                            $main_comment .= "\n" . $comment1;
                        }
                        if ( strlen( $comment2 ) > 0 )
                        {
                            $main_comment .= "\n" . $comment2;
                        }
                    }
                    else
                    {
                        $main_comment = $comment1;
                        if ( strlen( $comment2 ) > 0 )
                        {
                            $main_comment .= "\n" . $comment2;
                        }
                    }
                    //$product_id = $res[0]['product_id'];
                    $objQuery->update( 'dtb_products', compact( 'main_comment' ), 'product_id=?', array( $product_id ) );
                }
                else
                {
                    var_dump('---------------------------------------------');
                    var_dump('product_idがありません。product_code:'.$product_code);
                    var_dump('---------------------------------------------');
                }
            }
        }
    }
    else
    {
        throw new Exception( 'file not exists' );
    }
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}
