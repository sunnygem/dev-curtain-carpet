<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    //$query = 'select product_id from dtb_products limit 1';
    //$res = $objQuery->select( 'product_id', 'dtb_products', array(), array( 'limit' => 1 ) );

    // CSVを読み込む
    $target_files = 'test.csv';
    if ( file_exists( $target_files ) )
    {
        $files = new SplFileObject( $target_files );
        $files->setFlags( SplFileObject::READ_CSV );
        foreach( $files as $key => $val )
        {
            if ( $key === 0 ) continue; // header行
            if ( is_null( $val[0] ) === false )
            {
                $product_id = mb_convert_kana( $val[0], 'KVas' );
                $product_code  = mb_convert_kana( $val[1], 'KVas' );

                $sqlval = array(
                    'product_class_id' => $objQuery->nextVal('dtb_products_class_product_class_id')
                    ,'product_id' => $product_id
                    ,'product_code' => $product_code
                );

                $objQuery->insert( 'dtb_products_class', $sqlval );
            }
        }
    }
    else
    {
        throw new Exception( 'file not exists' );
    }
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}

