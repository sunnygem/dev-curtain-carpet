<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    //$query = 'select product_id from dtb_products limit 1';
    //$res = $objQuery->select( 'product_id', 'dtb_products', array(), array( 'limit' => 1 ) );

    // CSVを読み込む
    $target_files = 'delete_product.csv';
    if ( file_exists( $target_files ) )
    {
        $files = new SplFileObject( $target_files );
        $files->setFlags( SplFileObject::READ_CSV );
        foreach( $files as $key => $val )
        {
            if ( $key === 0 ) continue; // header行
            if ( is_null( $val[0] ) === false )
            {
                $product_code   = mb_convert_kana( $val[0], 'KVas' );
                // product_codeから更新をかける
                $res = $objQuery->select( 'product_id', 'dtb_products_class', 'product_code = ? AND del_flg = ?', array( $product_code, 0 ) );
                if ( isset( $res[0]['product_id'] ) )
                {
                    $product_id = $res[0]['product_id'];
                    $objQuery->update( 'dtb_products', array( 'del_flg' => 1 ), 'product_id=?', array( $product_id ) );

                }
                else
                {
                    //var_dump('---------------------------------------------');
                    //var_dump('product_idがありません。product_code:'.$product_code);
                    //var_dump('---------------------------------------------');
                }
            }
        }
    }
    else
    {
        throw new Exception( 'file not exists' );
    }
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}

