<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
require_once './require.php';
require_once CLASS_EX_REALDIR . 'page_extends/LC_Page_Index_Ex.php';

$objPage = new LC_Page_Index_Ex();
$objPage->init();
//$objPage->process();
$_SESSION["affiliate"]["affiliateid"]=$_GET['aid'];
$_SESSION["affiliate"]["affiliate_user_id"]=$_GET['uid'];

	if(strpos($_SERVER["REQUEST_URI"],'shopping/complete.php')===false){
		$sql =sprintf('INSERT INTO dtb_affiliate_log (affiliate_id,affiliate_user_id,customer_id,conversion,ip,browser,create_date,update_date,url,afrom,post,get) VALUES (');
		$sql.=sprintf("%d,",getaffiliateid($_SESSION["affiliate"]["affiliateid"])); //affiliate_id
		$sql.=sprintf("'%s',",$_SESSION["affiliate"]["affiliate_user_id"]); //affiliate_user_id
		$sql.=sprintf("'%s',",$_SESSION["customer"]["customer_id"]); //customer_id
		$sql.=sprintf("%d,",3); //conversion
		$sql.=sprintf("'%s',",$_SERVER["REMOTE_ADDR"]); //ip
		$sql.=sprintf("'%s',",isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : ""); //browser
		$sql.=sprintf("'%s',",date("Y/m/d H:i:s",$_SERVER["REQUEST_TIME"])); //create_date
		$sql.=sprintf("'%s',",date("Y/m/d H:i:s",$_SERVER["REQUEST_TIME"])); //update_date
		$sql.=sprintf("'%s',",$_SERVER['PHP_SELF']); //url
		$sql.=sprintf("'%s',",isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ""); //from
		$sql.=sprintf("'%s',",arraydump($_POST)); //post
		$sql.=sprintf("'%s'",arraydump($_GET)); //get
		$sql.=sprintf(');');

		$objQuery =& SC_Query_Ex::getSingletonInstance();
		$objQuery->query($sql);
	} else {
		$objQuery =& SC_Query_Ex::getSingletonInstance();
		$col = '*';
		$where = sprintf("order_id='%s'",$_SESSION["order_id"]);
		$table = 'dtb_order_detail';
		$objQuery->setOrder('order_detail_id ASC');
		$arrRet = $objQuery->select($col, $table, $where);

		for($i=0;$i<count($arrRet);$i++){
			$sql =sprintf('INSERT INTO dtb_affiliate_log (affiliate_id,affiliate_user_id,customer_id,conversion,');
			$sql.=sprintf('order_id,product_id,product_class_id,maker_id,quantity,totalprice,');
			$sql.=sprintf('ip,browser,create_date,update_date,url,afrom,post,get) VALUES (');
			$sql.=sprintf("%d,",getaffiliateid($_SESSION["affiliate"]["affiliateid"])); //affiliate_id
			$sql.=sprintf("'%s',",$_SESSION["affiliate"]["affiliate_user_id"]); //affiliate_user_id
			$sql.=sprintf("'%s',",$_SESSION["customer"]["customer_id"]); //customer_id
			$sql.=sprintf("%d,",1); //conversion
			
			$sql.=sprintf("%d,",$_SESSION["order_id"]); //order_id
			$sql.=sprintf("%d,",$arrRet[$i]["product_id"]); //product_id
			$sql.=sprintf("%d,",$arrRet[$i]["product_class_id"]); //product_class_id
			$sql.=sprintf("%d,",getmakerid($arrRet[$i]["product_id"])); //maker_id
			$sql.=sprintf("%d,",$arrRet[$i]["quantity"]); //quantity
			$sql.=sprintf("%d,",$arrRet[$i]["price"]*$arrRet[$i]["quantity"]); //totalprice

			$sql.=sprintf("'%s',",$_SERVER["REMOTE_ADDR"]); //ip
			$sql.=sprintf("'%s',",isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : ""); //browser
			$sql.=sprintf("'%s',",date("Y/m/d H:i:s",$_SERVER["REQUEST_TIME"])); //create_date
			$sql.=sprintf("'%s',",date("Y/m/d H:i:s",$_SERVER["REQUEST_TIME"])); //update_date
			$sql.=sprintf("'%s',",$_SERVER['PHP_SELF']); //url
			$sql.=sprintf("'%s',",isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ""); //from
			$sql.=sprintf("'%s',",arraydump($_POST)); //post
			$sql.=sprintf("'%s'",arraydump($_GET)); //get
			$sql.=sprintf(');');

			$objQuery =& SC_Query_Ex::getSingletonInstance();
			$objQuery->query($sql);
		}
	
	}

function arraydump($array){
	if(is_array($array)){
		$result=(string)"";
		foreach($array as $key=>$val) {
			if(is_array($key)){
				$result.=arraydump($key);
			} elseif(is_array($val)){
				$result.=arraydump($val);
			} else {
				$result.=sprintf("[%s]=>(%s) ",$key,$val);
			}
		}
		return $result;
	} else {
		return "";
	}
}
function makeuserid(){
	for($flag=0,$i=0;$flag==0 && $i<100;$i++){
		$newaccessid=generate_password(1,16);
		if(checkuserid($newaccessid)){
			$flag=1;
			return $newaccessid;
		}
	}
	return FALSE;
}
function generate_password($mode,$size) {
	if($mode==0){
		$password_chars = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-<>_()#$%&';
	} elseif($mode==1) {
		$password_chars = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	} else {
		$password_chars = '1234567890';
	}
	$password_chars_count = strlen($password_chars);
	
	$data = substr(str_shuffle(str_repeat($password_chars, $size)), 0, $size);
    $pin = '';
    for ($n = 0; $n < $size; $n ++) {
        $pin .= substr($password_chars, ord(substr($data, $n, 1)) % $password_chars_count, 1);
    }
    return $pin;
}
function checkuserid($userid) {
	$objQuery =& SC_Query_Ex::getSingletonInstance();
	$col = '*';
	$where = sprintf("affiliate_user_id='%s' and del_flg=0",$userid);
	$table = 'dtb_affiliate_log';
	$objQuery->setOrder('log_id ASC');
	$arrRet = $objQuery->select($col, $table, $where);
	if(empty($arrRet[0])){
		return TRUE;
	} else {
		return FALSE;
	}
}
function getaffiliateid($affiliateid) {
	$objQuery =& SC_Query_Ex::getSingletonInstance();
	$col = 'affiliate_id';
	$where = sprintf("affiliateid='%s' and del_flg=0 and status=1",$affiliateid);
	$table = 'dtb_affiliate';
	$objQuery->setOrder('affiliate_id ASC');
	$arrRet = $objQuery->select($col, $table, $where);
	if(empty($arrRet[0])){
		return 0;
	} else {
		return $arrRet[0]['affiliate_id'];
	}
}
function getmakerid($product_id) {
	$objQuery =& SC_Query_Ex::getSingletonInstance();
	$col = 'maker_id';
	$where = sprintf("product_id=%d and del_flg=0",$product_id);
	$table = 'dtb_products';
	$objQuery->setOrder('product_id ASC');
	$arrRet = $objQuery->select($col, $table, $where);
	if(empty($arrRet[0])){
		return 0;
	} else {
		return $arrRet[0]['maker_id'];
	}
}
