<?php
/*
 * Copyright(c) 2017 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 */

require_once(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php');
require_once(MDL_PG_MULPAY_CLASS_PATH . 'util/SC_Util_PG_MULPAY_AccountLock.php');

/**
 * 決済モジュール用 アカウントロック情報の管理クラス
 */
class SC_Util_PG_MULPAY_AccountLock_Ex extends SC_Util_PG_MULPAY_AccountLock {
}
