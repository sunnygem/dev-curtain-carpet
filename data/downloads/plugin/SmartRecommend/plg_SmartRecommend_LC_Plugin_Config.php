<?php
/*
 * SmartRecommend
 * Copyright (C) 2017 bitmop, Inc. All Rights Reserved.
 * http://www.bitmop.co.jp/
 * 
 * This library is NOT free software; you can NOT redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * プラグインの設定クラス
 *
 * @package SmartRecommend
 * @author bitmop, Inc.
 * @version $Id: $
 */
class plg_SmartRecommend_LC_Plugin_Config extends LC_Page_Admin_Ex {

    var $arrForm = array();

    private $peers_mode_1_year_min = 0;
    private $peers_mode_1_year_max = 50;
    private $display_count_min     = 0;
    private $display_count_max     = 50;

    /**
     * 初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        // プラグインコードの取得（フォルダ名からプラグインコードを取得する）
        $this->plugin_code  = basename(dirname(__FILE__));
        // メインページファイル
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . $this->plugin_code . "/templates/config.tpl";
        // サブタイトル
        $this->tpl_subtitle = "スマートリコメンドプラグイン設定";
        // 管理画面で表示する説明文
        $this->tpl_note     = "スマートリコメンドプラグインの各種パラメータを設定・編集できます";
    }

    /**
     * プロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();

        $arrForm = array();
        switch ($this->getMode()) {
            case 'edit':
                // データ取得
                $arrForm        = $objFormParam->getHashArray();
                $this->arrErr   = $objFormParam->checkError();

                if ($arrForm['peers_mode_1_year'] < $this->peers_mode_1_year_min) {
                    $this->arrErr['peers_mode_1_year'] = "※ 年数は「" . $this->peers_mode_1_year_min . "」以上の数値で入力してください。<br />";
                }
                if ($arrForm['peers_mode_1_year'] > $this->peers_mode_1_year_max) {
                    $this->arrErr['peers_mode_1_year'] = "※ 年数は「" . $this->peers_mode_1_year_max . "」以下の数値で入力してください。<br />";
                }

                if ($arrForm['display_count'] < $this->display_count_min) {
                    $this->arrErr['display_count'] = "※ 件数は「" . $this->display_count_min . "」以上の数値で入力してください。<br />";
                }
                if ($arrForm['display_count'] > $this->display_count_max) {
                    $this->arrErr['display_count'] = "※ 件数は「" . $this->display_count_max . "」以下の数値で入力してください。<br />";
                }

                // エラーなしの場合にはデータを更新
                if (count($this->arrErr) == 0) {
                    // データ更新
                    // ファイルデータがあれば処理前に取り除く
                    $arrConf = $arrForm;
                    $this->arrErr = $this->lfSetPluginConfig($arrConf);
                    $this->tpl_onload = "alert('登録が完了しました。');";
                    $this->tpl_onload .= 'window.close();';
                }
                break;

            default:
                // プラグイン情報を取得.
                $data = $this->lfGetPluginConfig();
                foreach ($data as $data_key => $data_value) {
                    $arrForm[$data_key] = $data_value;
                }

                break;
        }
        $this->arrForm = $arrForm;
        $this->setTemplate($this->tpl_mainpage);
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
        parent::destroy();
    }

    /**
     * パラメーター情報の初期化
     *
     * @param object $objFormParam SC_FormParamインスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam('どの区切りで「同世代」と呼ぶか', 'peers_mode', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('誕生日から±N年で区切る場合の年数', 'peers_mode_1_year', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('抽出された商品を並び替えるキー', 'recommend_type', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('性別によるリコメンドの分類', 'classification_by_sex', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('スマートリコメンドブロックに表示する商品の件数', 'display_count', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('スマートリコメンドブロックの表示タイトル', 'block_display_title', STEXT_LEN, 'aKV', array('EXIST_CHECK', 'SPTAB_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('スマートリコメンドブロックにタイトルを表示', 'block_display_title_flg', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
    }

    /**
     * プラグイン設定の取得
     *
     * @param type $arrData
     * @return type
     */
    function lfGetPluginConfig() {
        $plugin = SC_Plugin_Util_Ex::getPluginByPluginCode($this->plugin_code);
        return unserialize($plugin['free_field1']);
    }

    /**
     * プラグイン設定の更新
     *
     * @param type $arrData
     * @return type
     */
    function lfSetPluginConfig($arrData) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $sql_conf = array();
        $sql_conf['free_field1'] = serialize($arrData);
        $sql_conf['update_date'] = 'CURRENT_TIMESTAMP';
        $where = "plugin_code = ?";
        $objQuery->update('dtb_plugin', $sql_conf, $where, array($this->plugin_code));
    }
}
