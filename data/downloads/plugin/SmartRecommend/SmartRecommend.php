<?php
/*
 * SmartRecommend
 * Copyright (C) 2017 bitmop, Inc. All Rights Reserved.
 * http://www.bitmop.co.jp/
 * 
 * This library is NOT free software; you can NOT redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* 
 * スマートリコメンドプラグイン
 */
class SmartRecommend extends SC_Plugin_Base {

    public $expire;

    /**
     * コンストラクタ
     * プラグイン情報(dtb_plugin)をメンバ変数をセットします.
     * @param array $arrSelfInfo dtb_pluginの情報配列
     * @return void
     */
    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
        // プラグインコードの取得（フォルダ名からプラグインコードを取得する）
        $this->plugin_code  = basename(dirname(__FILE__));
    }

    /**
     * インストール時に実行される処理を記述します.
     * @param array $arrPlugin dtb_pluginの情報配列
     * @return void
     */
    function install($arrPlugin) {

        SmartRecommend::insertBloc($arrPlugin);
        SmartRecommend::insertFreeField();

        // ロゴファイルをhtmlディレクトリにコピーします.
        copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/logo.png", PLUGIN_HTML_REALDIR . $arrPlugin['plugin_code'] . "/logo.png");

        // 必要なファイルをコピーします.
        copy(PLUGIN_UPLOAD_REALDIR . "SmartRecommend/templates/pc/bloc/plg_SmartRecommend_smartrecommend.tpl", TEMPLATE_REALDIR . "frontparts/bloc/plg_SmartRecommend_smartrecommend.tpl");
        copy(PLUGIN_UPLOAD_REALDIR . "SmartRecommend/templates/sp/bloc/plg_SmartRecommend_smartrecommend.tpl", SMARTPHONE_TEMPLATE_REALDIR . "frontparts/bloc/plg_SmartRecommend_smartrecommend.tpl");
        copy(PLUGIN_UPLOAD_REALDIR . "SmartRecommend/bloc/plg_SmartRecommend_smartrecommend.php", HTML_REALDIR . "frontparts/bloc/plg_SmartRecommend_smartrecommend.php");
        copy(PLUGIN_UPLOAD_REALDIR . "SmartRecommend/config.php", PLUGIN_HTML_REALDIR . "SmartRecommend/config.php");
        mkdir(PLUGIN_HTML_REALDIR . "SmartRecommend/media");
        SC_Utils_Ex::sfCopyDir(PLUGIN_UPLOAD_REALDIR . "SmartRecommend/media/", PLUGIN_HTML_REALDIR . "SmartRecommend/media/");
    }

    /**
     * 削除時に実行される処理を記述します.
     * @param array $arrPlugin dtb_pluginの情報配列
     * @return void
     */
    function uninstall($arrPlugin) {
        // 作成したDBのテーブルを削除する
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 作成したブロックを削除
        $arrBlocIdPC = $objQuery->getCol('bloc_id', "dtb_bloc", "device_type_id = ? AND filename = ?", array(DEVICE_TYPE_PC , "plg_SmartRecommend_smartrecommend"));
        $bloc_id_pc = (int) $arrBlocIdPC[0];
        $where = "bloc_id = ?";
        $objQuery->delete("dtb_bloc", $where, array($bloc_id_pc));
        $objQuery->delete("dtb_blocposition", $where, array($bloc_id_pc));

        $arrBlocIdSP = $objQuery->getCol('bloc_id', "dtb_bloc", "device_type_id = ? AND filename = ?", array(DEVICE_TYPE_SMARTPHONE , "plg_SmartRecommend_smartrecommend"));
        $bloc_id_sp = (int) $arrBlocIdSP[0];
        $where = "bloc_id = ?";
        $objQuery->delete("dtb_bloc", $where, array($bloc_id_sp));
        $objQuery->delete("dtb_blocposition", $where, array($bloc_id_sp));

        // 作成したファイルやディレクトリを削除
        SC_Helper_FileManager_Ex::deleteFile(TEMPLATE_REALDIR . "frontparts/bloc/plg_smartrecommend_smartrecommend.tpl");
        SC_Helper_FileManager_Ex::deleteFile(SMARTPHONE_TEMPLATE_REALDIR . "frontparts/bloc/plg_smartrecommend_smartrecommend.tpl");
        SC_Helper_FileManager_Ex::deleteFile(HTML_REALDIR . "frontparts/bloc/plg_smartrecommend_smartrecommend.php");
        SC_Helper_FileManager_Ex::deleteFile(PLUGIN_HTML_REALDIR . "SmartRecommend/media");
    }

    /**
     * 有効にした際に実行される処理を記述します.
     * @param array $arrPlugin dtb_pluginの情報配列
     * @return void
     */
    function enable($arrPlugin) {
        // nop
    }

    /**
     * 無効にした際に実行される処理を記述します.
     * @param array $arrPlugin dtb_pluginの情報配列
     * @return void
     */
    function disable($arrPlugin) {
        // nop
    }

    function insertBloc($arrPlugin) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        // dtb_blocにブロックを追加する[PC].
        $sqlval_bloc = array();
        $sqlval_bloc['device_type_id'] = DEVICE_TYPE_PC;
        $sqlval_bloc['bloc_id'] = $objQuery->max('bloc_id', "dtb_bloc", "device_type_id = " . DEVICE_TYPE_PC) + 1;
        $sqlval_bloc['bloc_name'] = $arrPlugin['plugin_name'];
        $sqlval_bloc['tpl_path'] = "plg_SmartRecommend_smartrecommend.tpl";
        $sqlval_bloc['filename'] = "plg_SmartRecommend_smartrecommend";
        $sqlval_bloc['create_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['update_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['php_path'] = "frontparts/bloc/plg_SmartRecommend_smartrecommend.php";
        $sqlval_bloc['deletable_flg'] = 0;
        $sqlval_bloc['plugin_id'] = $arrPlugin['plugin_id'];
        $objQuery->insert("dtb_bloc", $sqlval_bloc);
        $new_bloc_id_pc = $sqlval_bloc['bloc_id'];

        $col = 'page_id';
        $table = 'dtb_pagelayout';
        $where = 'device_type_id = ? AND filename LIKE ?';
        $arrRet = $objQuery->select($col, $table, $where, array(DEVICE_TYPE_PC , "cart/index"));
        $page_id_pc = $arrRet[0]['page_id'];

        // dtb_blocpositionにブロックのレコードを追加する[PC].
        $sqlval_pc = array();
        $sqlval_pc['bloc_row'] = $objQuery->max('bloc_row', 'dtb_blocposition', "device_type_id = ? AND page_id = ? AND target_id = ?", array(DEVICE_TYPE_PC, $page_id_pc, 4)) + 1;
        $sqlval_pc['device_type_id'] = DEVICE_TYPE_PC;
        $sqlval_pc['page_id'] = $page_id_pc;
        $sqlval_pc['bloc_id'] = $new_bloc_id_pc;
        $sqlval_pc['target_id'] = 4;
        $objQuery->insert('dtb_blocposition', $sqlval_pc);

        // dtb_blocにブロックを追加する[SP].
        $sqlval_bloc = array();
        $sqlval_bloc['device_type_id'] = DEVICE_TYPE_SMARTPHONE;
        $sqlval_bloc['bloc_id'] = $objQuery->max('bloc_id', "dtb_bloc", "device_type_id = " . DEVICE_TYPE_SMARTPHONE) + 1;
        $sqlval_bloc['bloc_name'] = $arrPlugin['plugin_name'];
        $sqlval_bloc['tpl_path'] = "plg_SmartRecommend_smartrecommend.tpl";
        $sqlval_bloc['filename'] = "plg_SmartRecommend_smartrecommend";
        $sqlval_bloc['create_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['update_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['php_path'] = "frontparts/bloc/plg_SmartRecommend_smartrecommend.php";
        $sqlval_bloc['deletable_flg'] = 0;
        $sqlval_bloc['plugin_id'] = $arrPlugin['plugin_id'];
        $objQuery->insert("dtb_bloc", $sqlval_bloc);
        $new_bloc_id_sp = $sqlval_bloc['bloc_id'];

        $col = 'page_id';
        $table = 'dtb_pagelayout';
        $where = 'device_type_id = ? AND filename LIKE ?';
        $arrRet = $objQuery->select($col, $table, $where, array(DEVICE_TYPE_SMARTPHONE , "cart/index"));
        $page_id_sp = $arrRet[0]['page_id'];

        // dtb_blocpositionにブロックのレコードを追加する[SP].
        $sqlval_sp = array();
        $sqlval_sp['bloc_row'] = $objQuery->max('bloc_row', 'dtb_blocposition', "device_type_id = ? AND page_id = ? AND target_id = ?", array(DEVICE_TYPE_SMARTPHONE, $page_id_sp, 4)) + 1;
        $sqlval_sp['device_type_id'] = DEVICE_TYPE_SMARTPHONE;
        $sqlval_sp['page_id'] = $page_id_sp;
        $sqlval_sp['bloc_id'] = $new_bloc_id_sp;
        $sqlval_sp['target_id'] = 4;
        $objQuery->insert('dtb_blocposition', $sqlval_sp);
    }

    // プラグイン独自の設定データを追加
    function insertFreeField($mailtemplate_id) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $sqlval = array();

        $arrInitConfigInFreeField1['peers_mode']              = '1'; // どの区切りで「同世代」と呼ぶか
        $arrInitConfigInFreeField1['peers_mode_1_year']       = '5'; // 誕生日から±N年で区切る場合の年数
        $arrInitConfigInFreeField1['recommend_type']          = '1'; // 抽出された商品を並び替えるキー
        $arrInitConfigInFreeField1['classification_by_sex']   = '2'; // 性別によるリコメンドの分類
        $arrInitConfigInFreeField1['display_count']           = '8'; // スマートリコメンドブロックに表示する商品の件数
        $arrInitConfigInFreeField1['block_display_title']     = 'スマートリコメンドによるお薦め商品'; // スマートリコメンドブロックの表示タイトル
        $arrInitConfigInFreeField1['block_display_title_flg'] = '2'; // スマートリコメンドブロックにタイトルを表示

        $free_field1 = serialize($arrInitConfigInFreeField1);

        $sqlval['free_field1'] = $free_field1;
        $sqlval['free_field2'] = "1";
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $where = "plugin_code = ?";
        // UPDATEの実行
        $objQuery->update('dtb_plugin', $sqlval, $where, array('SmartRecommend'));
    }

    /**
     * 処理の介入箇所とコールバック関数を設定
     * registerはプラグインインスタンス生成時に実行されます
     * 
     * @param SC_Helper_Plugin $objHelperPlugin 
     */
    function register(SC_Helper_Plugin $objHelperPlugin) {
        // ヘッダへの追加
        $objHelperPlugin->addAction('LC_Page_Mypage_Login_action_before', array($this, 'LC_Page_Mypage_Login_action_before'));
        $objHelperPlugin->addAction('LC_Page_Shopping_Complete_action_after', array($this, 'LC_Page_Shopping_Complete_action_after'));
    }

    function LC_Page_Shopping_Complete_action_after($objPage) {
        if ($_SESSION['order_id'] > 0) {
            self::addCustomerProfileIntoCookieByOrderId($_SESSION['order_id']);
        }
    }

    function LC_Page_Mypage_Login_action_before($objPage) {
        if ($_SESSION['customer']['customer_id']) {
            self::addCustomerProfileIntoCookie($_SESSION['customer']['birth'], $_SESSION['customer']['sex']);
        }
    }

    private function getSaveKey()
    {
        return 'plg_SmartRecommend';
    }

    /**
     * add customer data into Cookie
     *
     * @param date $user_birthday
     * @param int $user_sex
     *
     * @return SmartRecommendService
     */
    public function addCustomerProfileIntoCookie($user_birthday, $user_sex)
    {
        if ($user_birthday != "" && $user_sex != "") {
            list($user_birthday_date, $user_birthday_time) = explode(' ', $user_birthday);
            list($Y, $m, $d) = explode('-', $user_birthday_date);
            if (checkdate($m, $d, $Y) === true) {
                if ($user_sex == 1 || $user_sex == 2) {

                    // クッキー管理クラス
                    $objCookie = new SC_Cookie_Ex();
                    $cookie_key = $this->getSaveKey();

                    $user_profile_in_cookies = $user_birthday_date . "," . $user_sex;
                    $objCookie->expire = time() + 86400 * 365;
                    $objCookie->setCookie($cookie_key, $user_profile_in_cookies);
                }
            }
        }
    }

    /**
     * add customer data into Cookie
     *
     * @param int $order_id
     *
     * @return SmartRecommendService
     */
    public function addCustomerProfileIntoCookieByOrderId($order_id)
    {
        // クッキー管理クラス
        $objCookie = new SC_Cookie_Ex();
        $cookie_key = $this->getSaveKey();

        // Cookieがあるかチェック
        $item = $objCookie->getCookie($cookie_key);

        $customer_profile = !empty($item) ? $item : null;
        $arrCustomerProfile = explode(",", $customer_profile);

        $available_customer_profile_in_cookie = FALSE;
        if (count($arrCustomerProfile) == 2) {
            $user_birthday = $arrCustomerProfile[0];
            $user_sex      = $arrCustomerProfile[1];
            if ($user_birthday != "" && $user_sex != "") {
                list($Y, $m, $d) = explode('-', $user_birthday);
                if (checkdate($m, $d, $Y) === true) {
                    if ($user_sex == 1 || $user_sex == 2) {
                        $available_customer_profile_in_cookie = TRUE;
                    }
                }
            }
        }

        if ($available_customer_profile_in_cookie) {
            // 既にCookieに有効なデータが設定されているので、後の処理は不要
        } else {
            // Cookieにデータが無いので、受注データから誕生日と性別を取得して、設定する

            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $col = 'order_birth, order_sex';
            $table = 'dtb_order';
            $where = 'order_id = ?';
            $arrRet = $objQuery->select($col, $table, $where, array($order_id));
            foreach ($arrRet as $key => $value) {
                $user_birthday = $value['order_birth'];
                $user_sex      = $value['order_sex'];
            }

            if ($user_birthday != "" && $user_sex != "") {
                list($user_birthday_date, $user_birthday_time) = explode(' ', $user_birthday);
                list($Y, $m, $d) = explode('-', $user_birthday_date);
                if (checkdate($m, $d, $Y) === true) {
                    if ($user_sex == 1 || $user_sex == 2) {
                        $user_profile_in_cookies = $user_birthday_date . "," . $user_sex;
                        $objCookie->expire = time() + 86400 * 365;
                        $objCookie->setCookie($cookie_key, $user_profile_in_cookies);
                    }
                }
            }
        }
    }
}
