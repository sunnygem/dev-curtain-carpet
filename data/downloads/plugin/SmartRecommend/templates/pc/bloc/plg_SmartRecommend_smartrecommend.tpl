<!--{*
 *
 * This file is part of the SmartRecommend
 *
 * Copyright (C) 2017 bitmop, Inc.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *}-->

<!--▼ SmartRecommend -->
<!--{if count($arrSmartRecommendItems) > 0}-->
    <link rel="stylesheet" type="text/css" href="<!--{$smarty.const.PLUGIN_HTML_URLPATH}-->SmartRecommend/media/pc/smartrecommend.css">
    <div class="block_outer clearfix">
        <div id="recommend_area">
            <!--{if $block_display_title_flg == 1}--><h2><!--{$block_display_title}--></h2><!--{/if}-->
            <div class="block_body clearfix">
                <!--{foreach from=$arrSmartRecommendItems item=arrProduct name="recommend_products"}-->
                    <div class="product_item clearfix">
                        <div class="productImage">
                            <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                                <img src="<!--{$smarty.const.ROOT_URLPATH}-->resize_image.php?image=<!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->&amp;width=80&amp;height=80" alt="<!--{$arrProduct.name|h}-->" />
                            </a>
                        </div>
                        <div class="productContents">
                            <h3>
                                <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->"><!--{$arrProduct.name|h}--></a>
                            </h3>
                            <p class="sale_price">
                                <!--{$smarty.const.SALE_PRICE_TITLE}-->(税込)： <span class="price">
                                <!--{if $arrProduct.price02_min_inctax == $arrProduct.price02_max_inctax}-->
                                    <!--{$arrProduct.price02_min_inctax|number_format}-->
                                <!--{else}-->
                                    <!--{$arrProduct.price02_min_inctax|number_format}-->～<!--{$arrProduct.price02_max_inctax|number_format}-->
                                <!--{/if}-->
                            円</span>
                            </p>
                            <p class="mini comment"><!--{$arrProduct.comment|h|nl2br}--></p>
                        </div>
                    </div>
                    <!--{if $smarty.foreach.recommend_products.iteration % 2 === 0}-->
                        <div class="clear"></div>
                    <!--{/if}-->
                <!--{/foreach}-->
            </div>
        </div>
    </div>
<!--{/if}-->
<!--▲ SmartRecommend -->
