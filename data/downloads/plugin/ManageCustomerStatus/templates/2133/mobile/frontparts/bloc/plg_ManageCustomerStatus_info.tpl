<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $smarty.session.customer}-->
<hr />
<!--{if $arrCustomer.status_id > 0}-->
現在の会員ランクは「<font color="#0000FF"><!--{$arrStatus[$arrCustomer.status_id]}--></font>」です。
<!--{/if}-->
<!--{if $arrCustomer.fixed_rank != 1}-->
<!--{if $rankup > 0}-->
<br>次回の更新で「<font color="#FF0000"><!--{$arrStatus[$arrNextRank.status_id]}--></font>」にランクアップします！
<!--{/if}-->
<!--{if $rankup_total > 0}-->
<br>あと、<!--{$rankup_total|n2s}-->円で次のランクにアップします。
<!--{/if}-->

<!--{if $rankup_times > 0}-->
<br>あと、<!--{$rankup_times}-->回ご購入頂きますと次のランクにアップします。
<!--{/if}-->
<!--{if $rankup_points > 0}-->
<br>あと、<!--{$rankup_points}-->ポイント獲得されますと次のランクにアップします。
<!--{/if}-->
<!--{/if}-->
<hr />
<!--{/if}-->