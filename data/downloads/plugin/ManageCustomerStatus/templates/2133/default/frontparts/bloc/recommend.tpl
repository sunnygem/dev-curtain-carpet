<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->


<br>
<!-- ▼会員価格 -->
<!--{if strlen($arrProduct.plg_managecustomerstatus_price_min) > 0 && (($smarty.session.customer.customer_id && $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 1) || $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 0)}-->
<font color="#0000FF"><!--{if $smarty.const.MEMBER_RANK_PRICE_TITLE_MODE == 1}--><!--{$arrCustomerRank[$customer_rank_id]}--><!--{/if}--><!--{$smarty.const.MEMBER_RANK_PRICE_TITLE}-->(税込)： <span class="price"><!--{$arrProduct.plg_managecustomerstatus_price_min_inctax|n2s}--> 円</span></font>

<!--{/if}-->
<!-- ▲会員価格 -->