<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<br>
<!-- ▼会員価格 -->
<!--{if strlen($arrProduct.plg_managecustomerstatus_price_min) > 0  && (($smarty.session.customer.customer_id && $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 1) || $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 0)}-->
<div class="pricebox member_price" style="color:#0000FF;">
    <!--{if $smarty.const.MEMBER_RANK_PRICE_TITLE_MODE == 1}--><!--{$arrCustomerRank[$customer_rank_id]}--><!--{/if}--><!--{$smarty.const.MEMBER_RANK_PRICE_TITLE}-->(税込)：
    <span class="price">
        <span id="price03_default_<!--{$id}-->"><!--{strip}-->
            <!--{if $arrProduct.plg_managecustomerstatus_price_min_inctax == $arrProduct.plg_managecustomerstatus_price_max_inctax}-->
            <!--{$arrProduct.plg_managecustomerstatus_price_min_inctax|n2s}-->
            <!--{else}-->
            <!--{$arrProduct.plg_managecustomerstatus_price_min_inctax|n2s}-->～<!--{$arrProduct.plg_managecustomerstatus_price_max_inctax|n2s}-->
            <!--{/if}-->
        </span><span id="price03_dynamic_<!--{$id}-->"></span><!--{/strip}-->
        円</span>
</div>
<!--{/if}-->
<!-- ▲会員価格 -->
