<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $smarty.session.customer}-->
<style type="text/css">
    #member_rank_info {
        margin-bottom: 20px;
    }
    #member_rank_info li {
        display: block;
        clear: both;
        padding: 10px;
        line-height: 1.3;
        background-color: #FEFEFE;
        background: -moz-linear-gradient(center top, #FEFEFE 0%,#EEEEEE 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #FEFEFE),color-stop(1, #EEEEEE));
        border-top: #FFF solid 1px;
        border-bottom: #CCC solid 1px;
    }
</style>

<section id="member_rank_info">
    <h2 class="title_block">会員情報</h2>
    <ul>
        <!--{if $arrCustomer.status_id > 0}-->
        <li>
            現在の会員ランクは「<span style="color:#0000FF; font-weight:bold;"><!--{$arrStatus[$arrCustomer.status_id]}--></span>」です。
        </li>
        <!--{/if}-->
        <!--{if $arrCustomer.fixed_rank != 1}-->
        <!--{if $rankup > 0}-->
        <li>次回の更新で「<span style="color:#FF0000; font-weight:bold;"><!--{$arrStatus[$arrNextRank.status_id]}--></span>」にランクアップします！</li>
        <!--{/if}-->
        <!--{if $rankup_total > 0}-->
        <li>あと、<!--{$rankup_total|n2s}-->円で次のランクにアップします。</li>
        <!--{/if}-->

        <!--{if $rankup_times > 0}-->
        <li>あと、<!--{$rankup_times}-->回ご購入頂きますと次のランクにアップします。</li>
        <!--{/if}-->
        <!--{if $rankup_points > 0}-->
        <li>あと、<!--{$rankup_points}-->ポイント獲得されますと次のランクにアップします。</li>
        <!--{/if}-->
        <!--{/if}-->
        </li>
    </ul>
</section>
<!--{/if}-->