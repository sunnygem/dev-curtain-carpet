<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if strlen($arrFavorite[cnt].plg_managecustomerstatus_price_min) > 0}-->
<br>
<span class="mini productPrice" style="color:#0000FF;">
    <!--{if $smarty.const.MEMBER_RANK_PRICE_TITLE_MODE == 1}--><!--{$arrCustomerRank[$customer_rank_id]}--><!--{/if}--><!--{$smarty.const.MEMBER_RANK_PRICE_TITLE}-->：
    <!--{if $arrFavorite[cnt].plg_managecustomerstatus_price_min_inctax == $arrFavorite[cnt].plg_managecustomerstatus_price_max_inctax}-->
    <!--{$arrFavorite[cnt].plg_managecustomerstatus_price_min_inctax|n2s}-->
    <!--{else}-->
    <!--{$arrFavorite[cnt].plg_managecustomerstatus_price_min_inctax|n2s}-->～<!--{$arrFavorite[cnt].plg_managecustomerstatus_price_max_inctax|n2s}-->
    <!--{/if}-->
    円</span>
<!--{/if}-->