<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrProduct.plg_managecustomerstatus_hidden_flg > 0}-->
<div class="cartin">
    <div class="cartin_btn">
        <span class="attention">
            <!--{if $arrProduct.plg_managecustomerstatus_hidden_flg == 2}-->
            この商品はランクによる購入制限がかかっております。<br>
            ログイン後にもう1度ご確認下さい。
            <!--{else}-->
            この商品は現在のランクではご購入頂けません。
            <!--{/if}-->
        </span>
    </div>
</div>
<!--{else}-->
<div class="cartin">

    <!--★販売価格★-->
    <dl class="sale_price price01">
        <dt><!--{$smarty.const.SALE_PRICE_TITLE}--></dt>
        <dd class="price <!--{if $arrProduct.sale_flg === "1"}-->sale<!--{/if}-->">
            <!--{if $arrProduct.sale_flg === "1"}-->
                <span class="normal_price">
                    <span class="price_sale_default"><!--{$arrProduct.price01_min|n2s}--></span><span class="price_sale_dynamic"></span> 円～
                </span>
            <!--{/if}-->

            <span id="price02_default_cart"><!--{strip}-->
            <!--{if $arrProduct.price02_min === $arrProduct.price02_max}-->
                <!--{$arrProduct.price02_min|n2s}-->
            <!--{else}-->
                <!--{$arrProduct.price02_min|n2s}--> <smaLL>円</small> ～
            <!--{/if}-->
            <!--{/strip}--></span><span id="price02_dynamic_cart"></span>
            <small>(税抜)</small>
            <p class="inctax">
                <small>(消費税込み：<span id="price02_inctax_default_cart"><!--{strip}-->
                <!--{if $arrProduct.price02_min_inctax === $arrProduct.price02_max_inctax}-->
                    <!--{$arrProduct.price02_min_inctax|n2s}-->
                <!--{else}-->
                    <!--{$arrProduct.price02_min_inctax|n2s}--><small>円</small> ～
                <!--{/if}-->
                <!--{/strip}--></span><span id="price02_inctax_dynamic_cart"></span>)</small>
            </p>
        </dd>
    </dl>

    <!--{if time() < strtotime('2021-01-06') }-->
        <p style="text-align:center; font-weight:bold;margin:20px; font-size:1rem;" class="nenmatsunenshi"><a href="http://www.oukoku.co.jp/shop/user_data/new-years-holiday-delivery.php" style="background-color:#FFE6E2;color:red;padding:4px 8px;line-height:1.3em;display:inline-block;" target="_blank">【オンラインショップ】年末年始のお届けに関するご案内</a></p>
        <style>
            .nenmatsunenshi{
                font-size: 1rem;
            }
            @media screen and (max-width: 960px) {
                .nenmatsunenshi > a{
                    font-size: 14px;
                }
            }
        </style>
    <!--{/if}-->
    <div class="cartin_btn">
        <div id="cartbtn_default">
            <!--★カゴに入れる★-->
            <div class="largeBtn">
                <button onClick="form_submit();return false;">カートに入れる</button>
            </div>
            <!-- .largeBtn -->
        </div>
    </div>
</div>

<!--{/if}-->
