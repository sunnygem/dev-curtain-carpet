<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<div class="point">ポイント：
    <span id="point_default"><!--{strip}-->
        <!--{if $arrProduct.plg_managecustomerstatus_price_min > 0 && (($smarty.session.customer && $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 1) || $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 0)}-->
        <!--{if $arrProduct.plg_managecustomerstatus_price_min == $arrProduct.plg_managecustomerstatus_price_max}-->
        <!--{$arrProduct.plg_managecustomerstatus_price_min|sfPrePoint:$arrProduct.point_rate|number_format}-->
        <!--{else}-->
        <!--{if $arrProduct.plg_managecustomerstatus_price_min|sfPrePoint:$arrProduct.point_rate == $arrProduct.plg_managecustomerstatus_price_max|sfPrePoint:$arrProduct.point_rate}-->
        <!--{$arrProduct.plg_managecustomerstatus_price_min|sfPrePoint:$arrProduct.point_rate|number_format}-->
        <!--{else}-->
        <!--{$arrProduct.plg_managecustomerstatus_price_min|sfPrePoint:$arrProduct.point_rate|number_format}-->～<!--{$arrProduct.plg_managecustomerstatus_price_max|sfPrePoint:$arrProduct.point_rate|number_format}-->
        <!--{/if}-->
        <!--{/if}-->
        <!--{else}-->
        <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
        <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|number_format}-->
        <!--{else}-->
        <!--{if $arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate == $arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate}-->
        <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|number_format}-->
        <!--{else}-->
        <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|number_format}-->～<!--{$arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate|number_format}-->
        <!--{/if}-->
        <!--{/if}-->
        <!--{/if}-->
    </span><span id="point_dynamic"></span><!--{/strip}-->
    Pt
</div>