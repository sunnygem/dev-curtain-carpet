<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrProduct.plg_managecustomerstatus_hidden_flg > 0}-->
<div class="cartbtn attention">
    <!--{if $arrProduct.plg_managecustomerstatus_hidden_flg == 2}-->
    この商品はランクによる購入制限がかかっております。<br>
    ログイン後にもう1度ご確認下さい。
    <!--{else}-->
    この商品は現在のランクではご購入頂けません。
    <!--{/if}-->
</div>
<!--{else}-->
<div class="cartin clearfix">
    <dl class="quantity">
        <dt>数量</dt>
        <dd>
            <div style="overflow:hidden;">
            <input type="text" name="quantity" class="kazu box box60" value="<!--{$arrProduct.quantity|default:1|h}-->" maxlength="<!--{$smarty.const.INT_LEN}-->" style="<!--{$arrErr.quantity|sfGetErrorColor}-->" />
            <!--★ボタン★-->
            <div class="button">
            <input type="button" name="spinner_up" class="spinner_up" value="" onclick="javascript:spinner('up');">
            <input type="button" name="spinner_up" class="spinner_down" value="" onclick="javascript:spinner('down');">
            </div>
            </div>
        <!--{if $arrErr.quantity != ""}-->
        <br /><span class="attention"><!--{$arrErr.quantity}--></span>
        <!--{/if}-->
    </dl>
    <!-- .quantity -->
    <div class="cartin_btn">
        <!--★カゴに入れる★-->
        <div id="cartbtn_default_<!--{$id}-->">
            <input type="image" id="cart<!--{$id}-->" src="<!--{$TPL_URLPATH}-->img/button/btn_cartin.jpg" alt="カゴに入れる" onclick="fnInCart(this.form); return false;" onmouseover="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_cartin_on.jpg', this);" onmouseout="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_cartin.jpg', this);" />
        </div>
        <div class="attention" id="cartbtn_dynamic_<!--{$id}-->"></div>
    </div>
</div>
<!--{/if}-->
