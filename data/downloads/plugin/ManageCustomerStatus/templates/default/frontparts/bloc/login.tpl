<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->


<!--{if $smarty.const.USE_POINT !== false && $tpl_user_point > 0 && $expired_date}-->
<br>ポイントの有効期限は「<span style="color:#FF0000;"><!--{$expired_date|date_format:"%Y/%m/%d"}--></span>」です。
<!--{/if}-->
