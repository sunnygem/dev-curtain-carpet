<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<tr>
    <th>会員ランク</th>
    <td colspan="3"><!--{html_checkboxes name="search_plg_managecustomerstatus_status" options=$arrPlgManageCustomerStatus separator="&nbsp;" selected=$arrForm.search_plg_managecustomerstatus_status.value}--></td>
</tr>
