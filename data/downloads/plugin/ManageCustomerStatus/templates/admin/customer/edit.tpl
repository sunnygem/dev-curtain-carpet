<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<tr>
    <th>会員ランク</th>
    <td>
        <span class="attention"><!--{$arrErr.plg_managecustomerstatus_status}--></span>
        <span <!--{if $arrErr.status != ""}--><!--{sfSetErrorStyle}--><!--{/if}-->>
            <!--{html_radios name="plg_managecustomerstatus_status" options=$arrPlgManageCustomerStatus separator=" " selected=$arrForm.plg_managecustomerstatus_status|default:0}-->
    </span>
</td>
</tr>