<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

// {{{ requires
require_once PLUGIN_UPLOAD_REALDIR . 'ManageCustomerStatus/class/LC_Page_Plugin_ManageCustomerStatus_Config.php';

// }}}
// {{{ generate page
$objPage = new LC_Page_Plugin_ManageCustomerStatus_Config();
register_shutdown_function(array($objPage, 'destroy'));
$objPage->init();
$objPage->process();
?>
