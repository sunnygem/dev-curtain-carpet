<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/plg_ManageCustomerStatus_Utils.php";
require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/class/plg_ManageCustomerStatus_LC_Page.php";

class plg_ManageCustomerStatus_LC_Page_Frontparts_Bloc_Login extends plg_ManageCustomerStatus_LC_Page
{

    /**
     * @param LC_Page_FrontParts_Bloc_Login $objPage
     * @return void
     */
    function frontparts_login_after($objPage)
    {
        $objCustomer = new SC_Customer_Ex();
        $expired_period = plg_ManageCustomerStatus_Utils::getConfig("point_term");
        if ($expired_period > 0) {
            $objQuery = & SC_Query_Ex::getSingletonInstance();
            $last_buy_date = $objQuery->get("last_buy_date", "dtb_customer", "customer_id = ?", array($objCustomer->getValue('customer_id')));
            $objPage->expired_date = date("Y-m-d", strtotime($last_buy_date . "+" . $expired_period . " day"));
        }
    }

}
