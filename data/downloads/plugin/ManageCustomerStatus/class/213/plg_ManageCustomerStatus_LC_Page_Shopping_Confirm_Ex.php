<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/plg_ManageCustomerStatus_Utils.php";
require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/class/plg_ManageCustomerStatus_LC_Page_Shopping_Confirm.php";

class plg_ManageCustomerStatus_LC_Page_Shopping_Confirm_Ex extends plg_ManageCustomerStatus_LC_Page_Shopping_Confirm
{

    /**
     * @param LC_Page_Shopping_Confirm $objPage 購入手続きのページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Shopping_Confirm $objPage 購入手続きのページクラス
     * @return void
     */
    function after($objPage)
    {
        parent::after($objPage);
    }

}
