<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/plg_ManageCustomerStatus_Utils.php";
require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/class/plg_ManageCustomerStatus_LC_Page_FrontParts_LoginCheck.php";

class plg_ManageCustomerStatus_LC_Page_FrontParts_LoginCheck_Ex extends plg_ManageCustomerStatus_LC_Page_FrontParts_LoginCheck
{

    /**
     * @param LC_Page_FrontParts_LoginChek $objPage
     * @return void
     */
    function login($objPage)
    {
        parent::login($objPage);
    }

    /**
     * @param LC_Page_FrontParts_LoginChek $objPage
     * @return void
     */
    function logout($objPage)
    {
        parent::logout($objPage);
    }

}
