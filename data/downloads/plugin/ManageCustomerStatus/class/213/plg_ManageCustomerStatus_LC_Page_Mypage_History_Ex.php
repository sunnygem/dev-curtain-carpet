<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/class/plg_ManageCustomerStatus_LC_Page_Mypage_History.php";

class plg_ManageCustomerStatus_LC_Page_Mypage_History_Ex extends plg_ManageCustomerStatus_LC_Page_Mypage_History
{

    /**
     * @param LC_Page_Mypage_History $objPage MYページ購入履歴のページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Mypage_History $objPage MYページ購入履歴のページクラス
     * @return void
     */
    function after($objPage)
    {
        $objProduct = new SC_Product_Ex();
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $objPage->is_price_change = false;
        foreach ($objPage->tpl_arrOrderDetail as $product_index => $arrOrderDetail) {
            $arrTempProductDetail = $objProduct->getProductsClass($arrOrderDetail['product_class_id']);
            if (strlen($arrTempProductDetail['plg_managecustomerstatus_price']) > 0) {
                $arrTempProductDetail['plg_managecustomerstatus_price_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax(
                                $arrTempProductDetail['plg_managecustomerstatus_price'], $arrTempProductDetail['product_id'], $arrTempProductDetail['product_class_id']
                );
                if ($objPage->tpl_arrOrderDetail[$product_index]['price_inctax'] != $arrTempProductDetail['plg_managecustomerstatus_price_inctax']) {
                    $objPage->is_price_change = true;
                }
                $objPage->tpl_arrOrderDetail[$product_index]['product_price_inctax'] = $arrTempProductDetail['plg_managecustomerstatus_price_inctax'];
            } else {
                $arrTempProductDetail['price02_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax(
                                $arrTempProductDetail['price02'], $arrTempProductDetail['product_id'], $arrTempProductDetail['product_class_id']
                );
                if ($objPage->tpl_arrOrderDetail[$product_index]['price_inctax'] != $arrTempProductDetail['price02_inctax']) {
                    $objPage->is_price_change = true;
                }
                $objPage->tpl_arrOrderDetail[$product_index]['product_price_inctax'] = ($arrTempProductDetail['price02_inctax']) ? $arrTempProductDetail['price02_inctax'] : 0;
            }
        }
    }

}
