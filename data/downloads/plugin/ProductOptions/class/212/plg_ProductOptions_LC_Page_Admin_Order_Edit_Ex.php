<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page_Admin_Order_Edit.php";

class plg_ProductOptions_LC_Page_Admin_Order_Edit_Ex extends plg_ProductOptions_LC_Page_Admin_Order_Edit
{

    /**
     * @param LC_Page_Admin_Order_Edit $objPage
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Admin_Order_Edit $objPage 受注編集ページクラス
     * @return void
     */
    function after($objPage)
    {
        $objPage->arrOptions = plg_ProductOptions_Util::lfGetOption();
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $objFormParam = new SC_FormParam_Ex();

        // パラメーター情報の初期化
        $objPage->lfInitParam($objFormParam);
        plg_ProductOptions_Util::addProductOptionsParam($objFormParam);
        $objFormParam->addParam('商品オプション', 'shipment_plg_productoptions_flg');
        $objFormParam->setParam($_REQUEST);
        $objFormParam->convParam();
        $order_id = $objFormParam->getValue('order_id');

        // DBから受注情報を読み込む
        if (!SC_Utils_Ex::isBlank($order_id)) {
            $objPage->setOrderToFormParam($objFormParam, $order_id);
        } else {
            $arrShippingIds[] = null;
            $objFormParam->setValue('shipping_id', $arrShippingIds);

            // 新規受注登録で入力エラーがあった場合の画面表示用に、会員の現在ポイントを取得
            if (!SC_Utils_Ex::isBlank($objFormParam->getValue('customer_id'))) {
                $arrCustomer = SC_Helper_Customer_Ex::sfGetCustomerDataFromId($customer_id);
                $objFormParam->setValue('customer_point', $arrCustomer['point']);

                // 新規受注登録で、ポイント利用できるように現在ポイントを設定
                $objFormParam->setValue('point', $arrCustomer['point']);
            }
        }

        switch ($objPage->getMode()) {
            case 'plg_productoptions_select_product':
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();

                $arrOption = array();
                foreach ($_POST['add_plg_productoptions'] as $option_id => $optioncategory_id) {
                    $default_id = plg_ProductOptions_Util::lfGetOptionDefaultCatId($option_id);
                    if ($option_id > 0 && strlen($optioncategory_id) > 0 && $default_id != $optioncategory_id) {
                        $arrOption[$option_id] = $optioncategory_id;
                    }
                }

                $product_class_id = $objFormParam->getValue('add_product_class_id');

                if (SC_Utils_Ex::isBlank($product_class_id)) {
                    $product_class_id = $objFormParam->getValue('edit_product_class_id');
                    $changed_no = $objFormParam->getValue('no');
                }

                // 選択済みの商品であれば数量を1増やす
                $exists = false;
                $arrExistsProductClassIds = $objFormParam->getValue('product_class_id');
                $arrExistsProductOptions = $objFormParam->getValue('plg_productoptions');

                $nochange_quantity = false;
                foreach ($arrExistsProductClassIds as $key => $value) {
                    $exists_product_class_id = $arrExistsProductClassIds[$key];
                    if ($exists_product_class_id == $product_class_id) {
                        $options = $arrExistsProductOptions[$key];
                        $buff_options = array();
                        foreach ($options as $option_id => $option) {
                            $buff_options[$option_id] = $option['optioncategory_id'];
                        }
                        if (plg_ProductOptions_Util::compareOptions($buff_options, $arrOption)) {
                            $exists = true;
                            $exists_no = $key;
                            break;
                        } elseif (plg_ProductOptions_Util::getUpdateFlg() != 1) {
                            $changed_no = $key;
                            $nochange_quantity = true;
                            break;
                        }
                    }
                }

                // 新しく商品を追加した場合はフォームに登録
                // 商品を変更した場合は、該当行を変更
                if (!$exists) {
                    if (isset($changed_no)) {
                        $no = $changed_no;
                    } else {
                        $added_no = 0;
                        if (is_array($arrExistsProductClassIds)) {
                            $added_no = count($arrExistsProductClassIds);
                        }
                        $no = $added_no;
                    }

                    $objProduct = new SC_Product_Ex();
                    $arrProduct = $objProduct->getDetailAndProductsClass($product_class_id);

                    $arrProduct['quantity'] = 1;
                    $arrProduct['price'] = $arrProduct['price02'];
                    $arrProduct['product_name'] = $arrProduct['name'];

                    $arrUpdateKeys = array(
                        'product_id', 'product_class_id', 'product_type_id', 'point_rate',
                        'product_code', 'product_name', 'classcategory_name1', 'classcategory_name2',
                        'price', 'plg_productoptions'
                    );
                    if (!$nochange_quantity)
                        $arrUpdateKeys[] = 'quantity';

                    foreach ($arrUpdateKeys as $key) {
                        $arrValues = $objFormParam->getValue($key);
                        // FIXME getValueで文字列が返る場合があるので配列であるかをチェック
                        if (!is_array($arrValues)) {
                            $arrValues = array();
                        }
                        $arrValues[$no] = $arrProduct[$key];

                        $objFormParam->setValue($key, $arrValues);
                    }
                } elseif (isset($changed_no) && $exists_no != $changed_no) {
                    self::doDeleteProduct($changed_no, $objFormParam);
                } else {
                    $arrExistsQuantity = $objFormParam->getValue('quantity');
                    $arrExistsQuantity[$exists_no] ++;
                    $objFormParam->setValue('quantity', $arrExistsQuantity);
                }

                $arrShipmentItemKeys = $objPage->arrShipmentItemKeys;
                $arrShipmentItemKeys[] = 'shipment_plg_productoptions_flg';
                $objPage->arrAllShipping = $objFormParam->getSwapArray(array_merge($objPage->arrShippingKeys, $arrShipmentItemKeys));
                $objPage->arrForm = $objFormParam->getFormParamList();

                $arrOption = array();
                foreach ($_POST['add_plg_productoptions'] as $option_id => $optioncategory_id) {
                    if ($option_id > 0 && strlen($optioncategory_id) > 0) {
                        $arrOption[$option_id] = $optioncategory_id;
                    }
                }
                if (strlen($no) > 0) {
                    $objPage->arrForm['plg_productoptions']['value'][$no] = $arrOption;
                    $add_price = plg_ProductOptions_Util::getTotalOptionPrice($arrOption);

                    if (strlen($add_price) > 0) {
                        $objPage->arrForm['price']['value'][$no] += $add_price;
                    }
                }
                break;

            case 'plg_productoptions_multiple_set_to':
                $objPage->lfInitMultipleParam($objFormParam);
                $objFormParam->addParam('product_index', 'multiple_product_index', INT_LEN, 'n', array('EXIST_CHECK', 'MAX_LENGTH_CHECK', 'NUM_CHECK'), 1);
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                self::setMultipleItemTo($objFormParam);
                $arrShipmentItemKeys = $objPage->arrShipmentItemKeys;
                $arrShipmentItemKeys[] = 'shipment_plg_productoptions_flg';
                $objPage->arrAllShipping = $objFormParam->getSwapArray(array_merge($objPage->arrShippingKeys, $arrShipmentItemKeys));
                $objPage->arrForm = $objFormParam->getFormParamList();
                break;

            case 'plg_productoptions_delete_product':
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                $delete_no = $objFormParam->getValue('delete_no');
                self::doDeleteProduct($delete_no, $objFormParam);
                $arrShipmentItemKeys = $objPage->arrShipmentItemKeys;
                $arrShipmentItemKeys[] = 'shipment_plg_productoptions_flg';
                $objPage->arrAllShipping = $objFormParam->getSwapArray(array_merge($objPage->arrShippingKeys, $arrShipmentItemKeys));
                $objPage->arrForm = $objFormParam->getFormParamList();
                break;
            case 'plg_productoptions_add':
                $objPage->tpl_mode = 'edit';
                $objFormParam->setValue('order_id', $_POST['plg_productoptions_order_id']);
            case 'plg_productoptions_edit':
                $objPage->setOrderToFormParam($objFormParam, $_POST['plg_productoptions_order_id']);
                $objPage->arrForm['order_id']['value'] = $objFormParam->getValue('order_id');
                $objPage->arrForm['create_date']['value'] = $objFormParam->getValue('create_date');
                $arrShipmentItemKeys = $objPage->arrShipmentItemKeys;
                $arrShipmentItemKeys[] = 'shipment_plg_productoptions_flg';
                $objPage->arrAllShipping = $objFormParam->getSwapArray(array_merge($objPage->arrShippingKeys, $arrShipmentItemKeys));
                break;
        }


        $add_point = 0;
        $deliv_free_cnt = 0;
        foreach ($objPage->arrForm['plg_productoptions']['value'] as $key => $item) {
            list($add_price, $option_point, $option_cnt, $arrOption) = plg_ProductOptions_Util::getTotalOptionInfo($item);
            $add_point += $option_point;
            $deliv_free_cnt += $option_cnt;

            if (!empty($arrOption)) {
                unset($objPage->arrForm['plg_productoptions']['value'][$key]);
                $objPage->arrForm['plg_productoptions']['value'][$key] = $arrOption;
            }
        }
        self::lfCheck($objPage->arrForm, $deliv_free_cnt);
        $objPage->arrForm['add_point']['value'] += $add_point;

        if (!SC_Utils_Ex::isBlank($order_id) && ($objPage->getMode() == "pre_edit" || $objPage->getMode() == "order_id")) {
            $objPurchase = new SC_Helper_Purchase_Ex();
            $arrShipmentItemKeys = array(
                'shipment_plg_productoptions_flg'
            );


            $arrShippings = $objPurchase->getShippings($order_id);
            $arrShipmentItem = array();
            foreach ($arrShippings as $shipping_id => $arrShipping) {
                $arrProductQuantity[$shipping_id] = count($arrShipping['shipment_item']);
                foreach ($arrShipping['shipment_item'] as $item_index => $arrItem) {
                    foreach ($arrItem as $item_key => $item_val) {
                        $arrShipmentItem['shipment_' . $item_key][$shipping_id][$item_index] = $item_val;
                    }
                }
            }
            $objFormParam->setParam($arrShipmentItem);

            $arrShippingItemParam = $objFormParam->getSwapArray($arrShipmentItemKeys);
            foreach ($objPage->arrAllShipping as $key => $item) {
                $objPage->arrAllShipping[$key]['shipment_plg_productoptions_flg'] = $arrShippingItemParam[$key]['shipment_plg_productoptions_flg'];
            }
        }

        foreach ($objPage->arrAllShipping as $shipping_id => $shipping) {
            foreach ($shipping['shipment_plg_productoptions_flg'] as $index => $options) {
                $options = @unserialize($options);
                $arrOption = array();
                if (count($options) > 0) {
                    foreach ($options as $option_id => $optioncategory_id) {
                        if (!is_array($optioncategory_id)) {
                            if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                                $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                                $arrOption[$option_id]['optioncategory_name'] = $optioncategory_id;
                            } else {
                                $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                                $arrOption[$option_id]['optioncategory_name'] = plg_ProductOptions_Util::lfGetOptionCatName($optioncategory_id);
                            }
                            $arrOption[$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                        }
                    }
                }
                $objPage->arrAllShipping[$shipping_id]['shipment_plg_productoptions'][$index] = $arrOption;
            }
        }
    }

    /**
     * 受注商品を削除する.
     *
     * @param integer $delete_no 削除する受注商品の項番
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function doDeleteProduct($delete_no, &$objFormParam)
    {
        $arrDeleteKeys = array(
            'product_id', 'product_class_id', 'product_type_id', 'point_rate',
            'product_code', 'product_name', 'classcategory_name1', 'classcategory_name2',
            'quantity', 'price', 'plg_productoptions'
        );
        foreach ($arrDeleteKeys as $key) {
            $arrNewValues = array();
            $arrValues = $objFormParam->getValue($key);
            foreach ($arrValues as $index => $val) {
                if ($index != $delete_no) {
                    $arrNewValues[] = $val;
                }
            }
            $objFormParam->setValue($key, $arrNewValues);
        }
    }

}
