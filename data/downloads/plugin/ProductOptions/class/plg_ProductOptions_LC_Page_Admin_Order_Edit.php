<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Admin_Order_Edit extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Admin_Order_Edit $objPage
     * @return void
     */
    function before($objPage)
    {
        switch ($objPage->getMode()) {
            case 'edit':
            case 'add':
                $objPurchase = new SC_Helper_Purchase_Ex();
                $objFormParam = new SC_FormParam_Ex();

                // パラメーター情報の初期化
                $objPage->lfInitParam($objFormParam);
                plg_ProductOptions_Util::addProductOptionsParam($objFormParam);
                $objFormParam->addParam('商品オプション', 'shipment_plg_productoptions_flg');
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                $order_id = $objFormParam->getValue('order_id');
                $arrValuesBefore = array();

                // DBから受注情報を読み込む
                if (!SC_Utils_Ex::isBlank($order_id)) {
                    $objPage->setOrderToFormParam($objFormParam, $order_id);
                    $objPage->tpl_subno = 'index';
                    $arrValuesBefore['payment_id'] = $objFormParam->getValue('payment_id');
                    $arrValuesBefore['payment_method'] = $objFormParam->getValue('payment_method');
                } else {
                    $objPage->tpl_subno = 'add';
                    $objPage->tpl_mode = 'add';
                    $arrValuesBefore['payment_id'] = NULL;
                    $arrValuesBefore['payment_method'] = NULL;
                    // お届け先情報を空情報で表示
                    $arrShippingIds[] = null;
                    $objFormParam->setValue('shipping_id', $arrShippingIds);

                    // 新規受注登録で入力エラーがあった場合の画面表示用に、会員の現在ポイントを取得
                    if (!SC_Utils_Ex::isBlank($objFormParam->getValue('customer_id'))) {
                        $arrCustomer = SC_Helper_Customer_Ex::sfGetCustomerDataFromId($customer_id);
                        $objFormParam->setValue('customer_point', $arrCustomer['point']);

                        // 新規受注登録で、ポイント利用できるように現在ポイントを設定
                        $objFormParam->setValue('point', $arrCustomer['point']);
                    }
                }

                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                $customer_id = $objFormParam->getValue('customer_id');

                if (method_exists($objPage, 'setProductsQuantity')) {
                    $objPage->setProductsQuantity($objFormParam);
                }
                $objPage->arrErr = $objPage->lfCheckError($objFormParam);
                if ($customer_id > 0) {
                    $arrForm = $objFormParam->getFormParamList();
                    $add_point = 0;
                    foreach ($arrForm['plg_productoptions']['value'] as $key => $item) {
                        $add_point += plg_ProductOptions_Util::getTotalOptionPoint($item);
                    }


                    $totalpoint = 0;
                    $max = count($arrForm['quantity']['value']);
                    for ($i = 0; $i < $max; $i++) {
                        // 加算ポイントの計算
                        $totalpoint += SC_Utils_Ex::sfPrePoint($arrForm['price']['value'][$i], $arrForm['point_rate']['value'][$i]) * $arrForm['quantity']['value'][$i];
                    }

                    $point = SC_Helper_DB_Ex::sfGetAddPoint($totalpoint, $arrForm['use_point']['value']);

                    $objFormParam->setValue('add_point', $point + $add_point);
                }
                if (SC_Utils_Ex::isBlank($objPage->arrErr)) {
                    if ($objPage->getMode() == "edit") {
                        $message = '受注を編集しました。';
                        $order_id = self::doRegister($order_id, $objPurchase, $objFormParam, $message, $arrValuesBefore);
                        if ($order_id >= 0) {
                            $objPage->setOrderToFormParam($objFormParam, $order_id);
                            $_POST['plg_productoptions_order_id'] = $order_id;
                            $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'plg_productoptions_edit';
                        }
                    } else {
                        $message = '受注を登録しました。';
                        $order_id = self::doRegister(null, $objPurchase, $objFormParam, $message, $arrValuesBefore);
                        if ($order_id >= 0) {
                            $objPage->tpl_mode = 'edit';
                            $objFormParam->setValue('order_id', $order_id);
                            $objPage->setOrderToFormParam($objFormParam, $order_id);
                            $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'plg_productoptions_add';
                            $_POST['plg_productoptions_order_id'] = $order_id;
                        }
                    }

                    $objPage->tpl_onload = "window.alert('" . $message . "');";
                } else {
                    $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'order_id';
                }
                break;

            case 'select_product_detail':
                $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'plg_productoptions_select_product';
                break;
            case 'multiple_set_to':
                $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'plg_productoptions_multiple_set_to';
                break;
            case 'delete_product':
                $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'plg_productoptions_delete_product';
                break;
        }
    }

    /**
     * @param LC_Page_Admin_Order_Edit $objPage 受注編集ページクラス
     * @return void
     */
    function after($objPage)
    {
        parent::after($objPage);
    }

    /**
     * 入力内容のチェックを行う.
     *
     */
    function lfCheck(&$arrValues, $deliv_free_cnt = 0)
    {
        // 商品の種類数
        $max = count($arrValues['quantity']['value']);
        $subtotal = 0;
        $totalpoint = 0;
        $totaltax = 0;
        for ($i = 0; $i < $max; $i++) {
            // 小計の計算
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                $subtotal += SC_Helper_DB_Ex::sfCalcIncTax($arrValues['price']['value'][$i], $arrValues['tax_rate']['value'][$i], $arrValues['tax_rule']['value'][$i]) * $arrValues['quantity']['value'][$i];
            } else {
                $subtotal += SC_Helper_DB_Ex::sfCalcIncTax($arrValues['price']['value'][$i]) * $arrValues['quantity']['value'][$i];
            }
            $totaltax += SC_Utils_Ex::sfTax($arrValues['price']['value'][$i], $arrValues['tax_rate']['value'][$i], $arrValues['tax_rule']['value'][$i]) * $arrValues['quantity']['value'][$i];

            // 加算ポイントの計算
            $totalpoint += SC_Utils_Ex::sfPrePoint($arrValues['price']['value'][$i], $arrValues['point_rate']['value'][$i]) * $arrValues['quantity']['value'][$i];
        }

        if ($deliv_free_cnt > 0) {
            $arrValues['deliv_fee']['value'] = 0;
        }

        // 消費税
        $arrValues['tax']['value'] = $totaltax;
        // 小計
        $arrValues['subtotal']['value'] = $subtotal;
        // 合計
        $arrValues['total']['value'] = $subtotal - $arrValues['discount']['value'] + $arrValues['deliv_fee']['value'] + $arrValues['charge']['value'];
        // お支払い合計
        $arrValues['payment_total']['value'] = $arrValues['total']['value'] - ($arrValues['use_point']['value'] * POINT_VALUE);

        // 加算ポイント
        $arrValues['add_point']['value'] = SC_Helper_DB_Ex::sfGetAddPoint($totalpoint, $arrValues['use_point']['value']);

        // 最終保持ポイント
        $arrValues['total_point']['value'] = $_POST['point'] - $arrValues['use_point']['value'];
    }

    /**
     * DB更新処理
     *
     * @param  integer            $order_id        受注ID
     * @param  SC_Helper_Purchase $objPurchase     SC_Helper_Purchase インスタンス
     * @param  SC_FormParam       $objFormParam    SC_FormParam インスタンス
     * @param  string             $message         通知メッセージ
     * @param  array              $arrValuesBefore 更新前の受注情報
     * @return integer            $order_id 受注ID
     *
     * エラー発生時は負数を返す。
     */
    public function doRegister($order_id, &$objPurchase, &$objFormParam, &$message, &$arrValuesBefore)
    {
        if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
            $arrPayment = SC_Helper_Payment_Ex::getIDValueList();
        } else {
            $arrPayment = SC_Helper_DB_Ex::sfGetIDValueList('dtb_payment', 'payment_id', 'payment_method');
        }

        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $arrValues = $objFormParam->getDbArray();

        $where = 'order_id = ?';

        $objQuery->begin();

        // 支払い方法が変更されたら、支払い方法名称も更新
        if ($arrValues['payment_id'] != $arrValuesBefore['payment_id']) {
            $arrValues['payment_method'] = $arrPayment[$arrValues['payment_id']];
            $arrValuesBefore['payment_id'] = NULL;
        }

        if (plg_ProductOptions_Util::getECCUBEVer() >= 2132) {
            // 生年月日の調整
            $arrValues['order_birth'] = SC_Utils_Ex::sfGetTimestamp($arrValues['order_birth_year'], $arrValues['order_birth_month'], $arrValues['order_birth_day']);
        }


        // 受注テーブルの更新
        $order_id = $objPurchase->registerOrder($order_id, $arrValues);

        $arrSwap = array('product_id',
            'product_class_id',
            'product_code',
            'product_name',
            'price', 'quantity',
            'point_rate',
            'classcategory_name1',
            'classcategory_name2',
            'plg_productoptions'
        );
        if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
            $arrSwap[] = 'tax_rate';
            $arrSwap[] = 'tax_rule';
        }
        $arrDetail = $objFormParam->getSwapArray($arrSwap);

        // 変更しようとしている商品情報とDBに登録してある商品情報を比較することで、更新すべき数量を計算

        $max = count($arrDetail);
        $k = 0;
        $arrStockData = array();
        for ($i = 0; $i < $max; $i++) {
            if (!empty($arrDetail[$i]['product_id'])) {
                $arrPreDetail = $objQuery->select('*', 'dtb_order_detail', 'order_id = ? AND product_class_id = ? AND plg_productoptions_flg = ?', array($order_id, $arrDetail[$i]['product_class_id'], serialize($arrDetail[$i]['plg_productoptions'])));
                if (!empty($arrPreDetail) && $arrPreDetail[0]['quantity'] != $arrDetail[$i]['quantity']) {
                    // 数量が変更された商品
                    $arrStockData[$k]['product_class_id'] = $arrDetail[$i]['product_class_id'];
                    $arrStockData[$k]['quantity'] = $arrPreDetail[0]['quantity'] - $arrDetail[$i]['quantity'];
                    ++$k;
                } elseif (empty($arrPreDetail)) {
                    // 新しく追加された商品 もしくは 違う商品に変更された商品
                    $arrStockData[$k]['product_class_id'] = $arrDetail[$i]['product_class_id'];
                    $arrStockData[$k]['quantity'] = -$arrDetail[$i]['quantity'];
                    ++$k;
                }
                $objQuery->delete('dtb_order_detail', 'order_id = ? AND product_class_id = ? AND plg_productoptions_flg = ?', array($order_id, $arrDetail[$i]['product_class_id'], serialize($arrDetail[$i]['plg_productoptions'])));
            }
        }

        // 上記の新しい商品のループでDELETEされなかった商品は、注文より削除された商品
        $arrPreDetail = $objQuery->select('*', 'dtb_order_detail', 'order_id = ?', array($order_id));
        foreach ($arrPreDetail as $key => $val) {
            $arrStockData[$k]['product_class_id'] = $val['product_class_id'];
            $arrStockData[$k]['quantity'] = $val['quantity'];
            ++$k;
        }

        // 受注詳細データの更新
        foreach ($arrDetail as $key => $item) {
            foreach ($item as $key2 => $value) {
                if ($key2 == "plg_productoptions") {
                    if (!is_array($value))
                        $value = array();
                    $arrDetail[$key]['plg_productoptions_flg'] = serialize($value);
                }
            }
        }
        $objPurchase->registerOrderDetail($order_id, $arrDetail);

        // 在庫数調整
        if (ORDER_DELIV != $arrValues['status'] && ORDER_CANCEL != $arrValues['status']) {
            foreach ($arrStockData as $stock) {
                $objQuery->update('dtb_products_class', array(), 'product_class_id = ?', array($stock['product_class_id']), array('stock' => 'stock + ?'), array($stock['quantity']));
            }
        }

        $arrShippingKeys = array(
            'shipping_id',
            'shipping_name01',
            'shipping_name02',
            'shipping_kana01',
            'shipping_kana02',
            'shipping_tel01',
            'shipping_tel02',
            'shipping_tel03',
            'shipping_fax01',
            'shipping_fax02',
            'shipping_fax03',
            'shipping_pref',
            'shipping_zip01',
            'shipping_zip02',
            'shipping_addr01',
            'shipping_addr02',
            'shipping_date_year',
            'shipping_date_month',
            'shipping_date_day',
            'time_id',
        );

        if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
            $arrShippingKeys[] = 'shipping_company_name';
            $arrShippingKeys[] = 'shipping_country_id';
            $arrShippingKeys[] = 'shipping_zipcode';
        }

        $arrShipmentItemKeys = array(
            'shipment_product_class_id',
            'shipment_product_code',
            'shipment_product_name',
            'shipment_classcategory_name1',
            'shipment_classcategory_name2',
            'shipment_price',
            'shipment_quantity',
            'shipment_plg_productoptions_flg'
        );

        $arrAllShipping = $objFormParam->getSwapArray($arrShippingKeys);
        $arrAllShipmentItem = $objFormParam->getSwapArray($arrShipmentItemKeys);

        if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
            $arrDelivTime = SC_Helper_Delivery_Ex::getDelivTime($objFormParam->getValue('deliv_id'));
        } else {
            $arrDelivTime = $objPurchase->getDelivTime($objFormParam->getValue('deliv_id'));
        }
        //商品単価を複数配送にも適応
        $arrShippingValues = array();
        $arrIsNotQuantityUp = array();
        foreach ($arrAllShipping as $shipping_index => $arrShipping) {
            $shipping_id = $arrShipping['shipping_id'];
            $arrShippingValues[$shipping_index] = $arrShipping;

            $arrShippingValues[$shipping_index]['shipping_date'] = SC_Utils_Ex::sfGetTimestamp($arrShipping['shipping_date_year'], $arrShipping['shipping_date_month'], $arrShipping['shipping_date_day']);

            //商品単価を複数配送にも反映する
            foreach ($arrDetail as $product_detail) {
                foreach ($arrAllShipmentItem[$shipping_index]['shipment_product_class_id'] as $relation_index => $shipment_product_class_id) {
                    if ($product_detail['product_class_id'] == $shipment_product_class_id) {
                        if (plg_ProductOptions_Util::compareOptions($product_detail['plg_productoptions'], @unserialize($arrAllShipmentItem[$shipping_index]['shipment_plg_productoptions_flg'][$relation_index])) || plg_ProductOptions_Util::getUpdateFlg() != 1) {
                            $arrAllShipmentItem[$shipping_index]['shipment_price'][$relation_index] = $product_detail['price'];
                        }
                    }
                }
            }
            // 配送業者IDを取得
            $arrShippingValues[$shipping_index]['deliv_id'] = $objFormParam->getValue('deliv_id');

            // お届け時間名称を取得
            $arrShippingValues[$shipping_index]['shipping_time'] = $arrDelivTime[$arrShipping['time_id']];

            // 複数配送の場合は配送商品を登録
            if (!SC_Utils_Ex::isBlank($arrAllShipmentItem)) {
                $arrShipmentValues = array();

                foreach ($arrAllShipmentItem[$shipping_index] as $key => $arrItem) {
                    $i = 0;
                    if (is_array($arrItem)) {
                        foreach ($arrItem as $item) {
                            $arrShipmentValues[$shipping_index][$i][0][str_replace('shipment_', '', $key)] = $item;
                            $i++;
                        }
                    }
                }
                $objPurchase->registerShipmentItem($order_id, $shipping_id, $arrShipmentValues[$shipping_index]);
            } elseif (plg_ProductOptions_Util::getECCUBEVer() < 2130) {
                $arrShipmentItem = array();
                $arrShipmentItem[] = $arrDetail;
                $objPurchase->registerShipmentItem($order_id, $shipping_id, $arrShipmentItem);
            }
        }

        $objPurchase->registerShipping($order_id, $arrShippingValues, false);
        $objQuery->commit();

        return $order_id;
    }

    /**
     * 複数配送入力フォームで入力された値を SC_FormParam へ設定する.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function setMultipleItemTo(&$objFormParam)
    {
        $arrMultipleKey = array('multiple_shipping_id',
            'multiple_product_class_id',
            'multiple_product_name',
            'multiple_product_code',
            'multiple_classcategory_name1',
            'multiple_classcategory_name2',
            'multiple_price',
            'multiple_quantity',
            'multiple_product_index');
        $arrMultipleParams = $objFormParam->getSwapArray($arrMultipleKey);

        $arrProductOptions = $objFormParam->getValue('plg_productoptions');

        /*
         * 複数配送フォームの入力値を shipping_id ごとにマージ
         *
         * $arrShipmentItem[お届け先ID][商品規格ID]['shipment_(key)'] = 値
         */
        $arrShipmentItem = array();
        foreach ($arrMultipleParams as $arrMultiple) {
            $shipping_id = $arrMultiple['multiple_shipping_id'];
            $product_class_id = $arrMultiple['multiple_product_class_id'];
            $product_index = $arrMultiple['multiple_product_index'];

            foreach ($arrMultiple as $key => $val) {
                if ($key == 'multiple_quantity') {
                    $arrShipmentItem[$shipping_id][$product_class_id . "_" . $product_index][str_replace('multiple', 'shipment', $key)] += $val;
                } elseif ($key == "multiple_product_index") {
                    $arrShipmentItem[$shipping_id][$product_class_id . "_" . $product_index]['shipment_plg_productoptions_flg'] = serialize($arrProductOptions[$product_index]);
                } else {
                    $arrShipmentItem[$shipping_id][$product_class_id . "_" . $product_index][str_replace('multiple', 'shipment', $key)] = $val;
                }
            }
        }

        /*
         * フォームのお届け先ごとの配列を生成
         *
         * $arrShipmentForm['(key)'][$shipping_id][$item_index] = 値
         * $arrProductQuantity[$shipping_id] = お届け先ごとの配送商品数量
         */
        $arrShipmentForm = array();
        $arrProductQuantity = array();
        $arrShippingIds = $objFormParam->getValue('shipping_id');
        $arrQuantity = array();
        foreach ($arrShippingIds as $shipping_id) {
            $item_index = 0;
            foreach ($arrShipmentItem[$shipping_id] as $index => $shipment_item) {
                foreach ($shipment_item as $key => $val) {
                    $arrShipmentForm[$key][$shipping_id][$item_index] = $val;
                }
                // 受注商品の数量を設定
                $arrQuantity[$index] += $shipment_item['shipment_quantity'];
                $item_index++;
            }
            // お届け先ごとの配送商品数量を設定
            $arrProductQuantity[$shipping_id] = count($arrShipmentItem[$shipping_id]);
        }

        $objFormParam->setParam($arrShipmentForm);
        $objFormParam->setValue('shipping_product_quantity', $arrProductQuantity);

        // 受注商品の数量を変更
        $arrDest = array();
        foreach ($objFormParam->getValue('product_class_id') as $n => $order_product_class_id) {
            $arrDest['quantity'][$n] = 0;
        }
        foreach ($arrQuantity as $index => $quantity) {
            foreach ($objFormParam->getValue('product_class_id') as $n => $order_product_class_id) {
                if ($index == $order_product_class_id . "_" . $n) {
                    $arrDest['quantity'][$n] = $quantity;
                }
            }
        }
        $objFormParam->setParam($arrDest);
    }

}
