<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Template.php";

class plg_ProductOptions_LC_Template_Ex extends plg_ProductOptions_LC_Template
{

    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename)
    {
        parent::prefilterTransform($source, $objPage, $filename);

        $objTransform = new SC_Helper_Transform($source);
        $template_dir = PLUGIN_UPLOAD_REALDIR . 'ProductOptions/templates/';
        $template_ex_dir = PLUGIN_UPLOAD_REALDIR . 'ProductOptions/templates/213/';

        switch ($objPage->arrPageLayout['device_type_id']) {
            case DEVICE_TYPE_PC:
                $template_dir .= "default/";
                $template_ex_dir .= "default/";
                if (strpos($filename, 'products/list.tpl') !== false) {
                    $objTransform->select('div#undercolumn', 0, false)->insertBefore(file_get_contents($template_ex_dir . 'products/list_js.tpl'));
                }
                if (strpos($filename, 'products/detail.tpl') !== false) {
                    $objTransform->select('.quantity', 0, false)->insertBefore(file_get_contents($template_ex_dir . 'products/detail.tpl'));
                }
                if (strpos($filename, 'shopping/multiple.tpl') !== false) {
                    $objTransform->select('td', 1, false)->replaceElement(file_get_contents($template_ex_dir . 'shopping/multiple.tpl'));
                }
                if (strpos($filename, 'shopping/confirm.tpl') !== false || strpos($filename, 'quick_shopping_confirm.tpl' !== false)) {
                    $objTransform->select('table', 3, false)->find('td', 1, false)->appendChild(file_get_contents($template_dir . 'shopping/confirm_shipping.tpl'));
                }
                break;
            case DEVICE_TYPE_MOBILE:
                $template_dir .= "mobile/";
                $template_ex_dir .= "mobile/";
                if (strpos($filename, 'shopping/multiple.tpl') !== false) {
                    $objTransform->select('form', 0, false)->replaceElement(file_get_contents($template_ex_dir . 'shopping/multiple.tpl'));
                }
                if (strpos($filename, 'shopping/confirm.tpl') !== false || strpos($filename, 'quick_shopping_confirm.tpl' !== false)) {
                    $objTransform->select('form', 0, false)->replaceElement(file_get_contents($template_ex_dir . 'shopping/confirm.tpl'));
                }
                break;
            case DEVICE_TYPE_SMARTPHONE:
                $template_dir .= "sphone/";
                $template_ex_dir .= "sphone/";
                if (strpos($filename, 'products/detail.tpl') !== false) {
                    $objTransform->select('div.cartin_btn', 0, false)->insertBefore(file_get_contents($template_ex_dir . 'products/detail.tpl'));
                    $objTransform->select("div.review_btn", 0, false)->replaceElement(file_get_contents($template_dir . "products/detail_review_btn.tpl"));
                }
                if (strpos($filename, 'shopping/multiple.tpl') !== false) {
                    $objTransform->select('div.delivContents p', 0, false)->replaceElement(file_get_contents($template_ex_dir . 'shopping/multiple.tpl'));
                }
                break;
            case DEVICE_TYPE_ADMIN:
            default:
                $template_dir .= "admin/";
                $template_ex_dir .= "admin/";
                if (strpos($filename, 'order/edit.tpl') !== false) {
                    if (plg_ProductOptions_Util::getECCUBEVer() >= 2131) {
                        $objTransform->select('input', 65)->insertAfter(file_get_contents($template_ex_dir . 'order/edit_shipmentitem_hidden.tpl'));
                    } else {
                        $objTransform->select('input', 68)->insertAfter(file_get_contents($template_ex_dir . 'order/edit_shipmentitem_hidden.tpl'));
                    }
                    $objTransform->select('table.list td', 1, false)->replaceElement(file_get_contents($template_ex_dir . 'order/edit_detail.tpl'));
                    $objTransform->select('a.btn-normal', 11, false)->insertBefore(file_get_contents($template_ex_dir . 'order/edit_shipmentitem.tpl'));
                }
                // ペイジェント決済モジュール用
                if (strpos($filename, 'order_edit.tpl') !== false) {
                    $objTransform->select('input', 65, false)->insertAfter(file_get_contents($template_ex_dir . 'order/edit_shipmentitem_hidden.tpl'));
                    $objTransform->select('table.list td', 1, false)->replaceElement(file_get_contents($template_ex_dir . 'order/edit_detail.tpl'));
                    $objTransform->select('a.btn-normal', 11, false)->insertBefore(file_get_contents($template_ex_dir . 'order/edit_shipmentitem.tpl'));
                }
                if (strpos($filename, 'order/product_select.tpl') !== false) {
                    $objTransform->select('form#form1', 0)->insertBefore(file_get_contents($template_ex_dir . 'order/product_select_js.tpl'));
                }
                if (strpos($filename, 'order/disp.tpl') !== false) {
                    $objTransform->select('table.list', 1)->find('td', 1)->appendChild(file_get_contents($template_ex_dir . 'order/edit_shipmentitem.tpl'));
                }
                break;
        }
        $source = $objTransform->getHTML();
    }

}
