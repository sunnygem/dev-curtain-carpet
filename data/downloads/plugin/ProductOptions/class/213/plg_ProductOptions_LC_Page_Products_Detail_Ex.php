<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page_Products_Detail.php";

class plg_ProductOptions_LC_Page_Products_Detail_Ex extends plg_ProductOptions_LC_Page_Products_Detail
{

    /**
     * @param LC_Page_Products_Detail $objPage
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Products_Detail $objPage
     * @return void
     */
    function after($objPage)
    {
        parent::after($objPage);

        $arrTaxRule = SC_Helper_TaxRule_Ex::getTaxRule();
        $objPage->tpl_productoptions_js .= "tax_rate_base=" . SC_Utils_Ex::jsonEncode($arrTaxRule['tax_rate']) . ';';
        $objPage->tpl_productoptions_js .= "tax_rule_base=" . SC_Utils_Ex::jsonEncode($arrTaxRule['tax_rule']) . ';';
    }

}
