<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page_Products_List.php";

class plg_ProductOptions_LC_Page_Products_List_Ex extends plg_ProductOptions_LC_Page_Products_List
{

    /**
     * @param LC_Page_Products_List $objPage 商品一覧ページクラス
     * @return void
     */
    function before($objPage)
    {
        // パラメーター管理クラス
        $objFormParam = new SC_FormParam_Ex();

        // パラメーター情報の初期化
        $objPage->lfInitParam($objFormParam);
        plg_ProductOptions_Util::addProductOptionsParam($objFormParam);

        // 値の設定
        $objFormParam->setParam($_REQUEST);

        // 入力値の変換
        $objFormParam->convParam();

        // 値の取得
        $arrForm = $objFormParam->getHashArray();

        //カート処理
        $target_product_id = intval($arrForm['product_id']);
        if ($target_product_id > 0) {
            // 商品IDの正当性チェック
            if (!SC_Utils_Ex::sfIsInt($arrForm['product_id']) || !SC_Helper_DB_Ex::sfIsRecord('dtb_products', 'product_id', $arrForm['product_id'], 'del_flg = 0 AND status = 1')) {
                SC_Utils_Ex::sfDispSiteError(PRODUCT_NOT_FOUND);
            }

            // 入力内容のチェック
            $objPage->arrErr = $objPage->lfCheckError($objFormParam);

            $productoptions = $_REQUEST['plg_productoptions'];
            $objPage->plg_productoptions_Err[$target_product_id] = self::lfCheckErrorOptions($target_product_id, $productoptions);

            //if (count($objPage->plg_productoptions_Err[$target_product_id]) > 0) { // ishibashi
            if (isset($objPage->plg_productoptions_Err[$target_product_id])) {
                $objPage->plg_productoptions_Form[$target_product_id] = $arrForm['plg_productoptions'];
                $_POST['target_product_id'] = $target_product_id;
                $_REQUEST['product_id'] = $_GET['product_id'] = $_POST['product_id'] = "";
            }

            if (empty($objPage->arrErr) && empty($objPage->plg_productoptions_Err[$target_product_id])) {
                // 開いているカテゴリーツリーを維持するためのパラメーター
                $arrQueryString = array(
                    'category_id' => $arrForm['category_id'],
                );

                $objCartSess = new SC_CartSession_Ex();
                $product_class_id = $arrForm['product_class_id'];
                $productoptions = $arrForm['plg_productoptions'];
                $quantity = $arrForm['quantity'];
                foreach ((array) $productoptions as $option_id => $optioncategory_id) {
                    $default_id = plg_ProductOptions_Util::lfGetOptionDefaultCatId($option_id);
                    if (is_null($optioncategory_id) || $optioncategory_id == "" || $default_id == $optioncategory_id) {
                        if (isset($productoptions[$option_id]))
                            unset($productoptions[$option_id]);
                    }
                }

                $objCartSess = new SC_CartSession_Ex();
                $objCartSess->addProduct($product_class_id, $quantity, $productoptions);

                self::sendRedirect(CART_URLPATH, $arrQueryString);
                exit;
            }
        }
    }

    /**
     * @param LC_Page_Products_List $objPage 商品一覧ページクラス
     * @return void
     */
    function after($objPage)
    {

        $price01_base = array();
        $price02_base = array();
        $point_base = array();
        $tax_rate_base = array();
        $tax_rule_base = array();
        foreach ($objPage->arrProducts as $key => $arrProduct) {
            list($arrRet, $values) = plg_ProductOptions_Util::lfSetProductOptions($arrProduct['product_id']);
            $objPage->arrProducts[$key]['productoptions'] = $arrRet;
            $arrValues[$arrProduct['product_id']] = $values;
            if (!$objPage->tpl_classcat_find1[$arrProduct['product_id']]) {
                $price01_base[$arrProduct['product_id']] = $arrProduct['price01_min'];
                $price02_base[$arrProduct['product_id']] = $arrProduct['price02_min'];
                $point_base[$arrProduct['product_id']] = SC_Utils_Ex::sfPrePoint($arrProduct['price02_min'], $arrProduct['point_rate']);
                $arrTaxRule = SC_Helper_TaxRule_Ex::getTaxRule($arrProduct['product_id']);
                $tax_rate_base[$arrProduct['product_id']] = $arrTaxRule['tax_rate'];
                $tax_rule_base[$arrProduct['product_id']] = $arrTaxRule['tax_rule'];
            }
        }
        $objPage->tpl_productoptions_js = 'values=' . SC_Utils_Ex::jsonEncode($arrValues) . ';';
        $objPage->tpl_productoptions_js .= "price01_base=" . SC_Utils_Ex::jsonEncode($price01_base) . ';';
        $objPage->tpl_productoptions_js .= "price02_base=" . SC_Utils_Ex::jsonEncode($price02_base) . ';';
        $objPage->tpl_productoptions_js .= "point_base=" . SC_Utils_Ex::jsonEncode($point_base) . ';';
        $objPage->tpl_productoptions_js .= "tax_rate_base=" . SC_Utils_Ex::jsonEncode($tax_rate_base) . ';';
        $objPage->tpl_productoptions_js .= "tax_rule_base=" . SC_Utils_Ex::jsonEncode($tax_rule_base) . ';';

        $js_fnOnLoad = "";
        if (SC_Display_Ex::detectDevice() === DEVICE_TYPE_PC) {
            foreach ($objPage->arrProducts as $arrProduct) {
                if ($arrProduct['stock_unlimited_max'] || $arrProduct['stock_max'] > 0) {
                    $js_fnOnLoad .= "fnSetClassCategories(document.product_form{$arrProduct['product_id']});";
                }
            }
        }

        $target_product_id = intval($_POST['target_product_id']);
        if ($target_product_id > 0) {

            $objFormParam = new SC_FormParam_Ex();
            $objPage->lfInitParam($objFormParam);
            plg_ProductOptions_Util::addProductOptionsParam($objFormParam);
            $objFormParam->setParam($_REQUEST);
            $objFormParam->convParam();
            $arrErr = self::lfCheckError($objFormParam, $objPage);

            $js_fnOnLoad .= $objPage->lfSetSelectedData($objPage->arrProducts, $objPage->arrForm, $arrErr, $target_product_id);
        }
        $objPage->tpl_javascript .= 'function fnOnLoad(){' . $js_fnOnLoad . '}';
        $objPage->tpl_onload .= 'fnOnLoad(); ';

        //if (count($objPage->plg_productoptions_Form) > 0) {
        if (isset($objPage->plg_productoptions_Form)) {
            foreach ($objPage->plg_productoptions_Form as $product_id => $options) {
                foreach ($options as $option_id => $value) {
                    $value = str_replace('alert', '', $value);
                    $value = str_replace('document.cookie', '', $value);
                    $objPage->plg_productoptions_Form[$product_id][$option_id] = $value;
                }
            }
        }
    }

    /* 入力内容のチェック */

    public function lfCheckError($objFormParam, $objPage)
    {
        // 入力データを渡す。
        $arrForm = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrForm);
        $objErr->arrErr = $objFormParam->checkError();

        // 動的チェック
        if ($objPage->tpl_classcat_find1[$_POST['target_product_id']]) {
            $objErr->doFunc(array('規格1', 'classcategory_id1'), array('EXIST_CHECK'));
        }
        if ($objPage->tpl_classcat_find2[$_POST['target_product_id']]) {
            $objErr->doFunc(array('規格2', 'classcategory_id2'), array('EXIST_CHECK'));
        }

        return $objErr->arrErr;
    }

}
