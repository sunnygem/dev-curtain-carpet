<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Shopping_Confirm extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Shopping_Confirm $objPage 購入確認ページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Shopping_Confirm $objPage 購入確認ページクラス
     * @return void
     */
    function after($objPage)
    {
        list($total_inctax, $total_point, $deliv_free_cnt) = plg_ProductOptions_Util::lfSetCartProductOptions($objPage->arrCartItems);

        $objPage->tpl_total_inctax[$objPage->cartKey] = $total_inctax;
        $objPage->arrForm['payment_total'] += ($total_inctax - $objPage->arrForm['subtotal']);

        $add_point = SC_Helper_DB_Ex::sfGetAddPoint($total_point, $objPage->arrForm['use_point']);
        if ($objPage->arrForm['birth_point'] > 0) {
            $add_point += $objPage->arrForm['birth_point'];
        }
        if ($add_point < 0) {
            $add_point = 0;
        }

        $objPage->arrForm['add_point'] = $add_point;
        if ($deliv_free_cnt > 0) {
            $objPage->arrForm['payment_total'] -= $objPage->arrForm['deliv_fee'];
            $objPage->arrForm['deliv_fee'] = 0;
        }

        foreach ($objPage->arrShipping as $key => $shipping) {
            foreach ($shipping['shipment_item'] as $product_class_id => $cart_nos) {
                foreach ($cart_nos as $cart_no => $shipmentitem) {
                    foreach ($objPage->arrCartItems as $cartItem) {
                        if ($cartItem['id'] == $product_class_id && $cartItem['cart_no'] == $cart_no) {
                            $objPage->arrShipping[$key]['shipment_item'][$product_class_id][$cart_no]['plg_productoptions_detail'] = $cartItem['plg_productoptions_detail'];
                            break;
                        }
                    }
                    $objPage->arrShipping[$key]['shipment_item'][$product_class_id . "_" . $cart_no] = $objPage->arrShipping[$key]['shipment_item'][$product_class_id][$cart_no];
                }
                unset($objPage->arrShipping[$key]['shipment_item'][$product_class_id]);
            }
        }
    }

}
