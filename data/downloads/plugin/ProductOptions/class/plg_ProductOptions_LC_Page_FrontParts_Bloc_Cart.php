<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_FrontParts_Bloc_Cart extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Products_Detail $objPage 商品詳細ページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Frontparts_Bloc_Cart $objPage カートブロッククラス
     * @return void
     */
    function after($objPage)
    {
        $objCartSess = new SC_CartSession_Ex();
        $arrInfo = SC_Helper_DB_Ex::sfGetBasisData();
        $total_inctax = 0;
        $total_point = 0;
        $cartItems = $objCartSess->getAllCartList();
        $is_deliv_free = false;
        foreach ($cartItems as $cartkey => $item) {
            list($sub_total_inctax, $sub_total_point, $deliv_free_cnt) = plg_ProductOptions_Util::lfSetCartProductOptions($item);
            $total_inctax += $sub_total_inctax;
            $total_point += $sub_total_point;
            // 送料無料チェック
            if (!$objPage->isMultiple && !$objPage->hasDownload) {
                // 送料無料の購入数が設定されている場合
                if (DELIV_FREE_AMOUNT > 0) {
                    // 商品の合計数量
                    $total_quantity = $objCartSess->getTotalQuantity($cartkey);

                    if ($total_quantity >= DELIV_FREE_AMOUNT) {
                        $is_deliv_free = true;
                    }
                }

                // 送料無料条件が設定されている場合
                if ($arrInfo['free_rule'] > 0) {
                    // 小計が送料無料条件以上の場合
                    if ($total_inctax >= $arrInfo['free_rule']) {
                        $is_deliv_free = true;
                    }
                }

                if ($deliv_free_cnt > 0) {
                    $is_deliv_free = true;
                }
            }
        }
        $objPage->arrCartList[0]['ProductsTotal'] = $total_inctax;

        // 送料無料までの金額
        if ($is_deliv_free) {
            $objPage->arrCartList[0]['deliv_free'] = 0;
        } else {
            $deliv_free = $arrInfo['free_rule'] - $total_inctax;
            $objPage->arrCartList[0]['deliv_free'] = $deliv_free;
        }
    }

}
