<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Cart extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Cart $objPage カートページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Cart $objPage カートページクラス
     * @return void
     */
    function after($objPage)
    {
        $objCartSess = new SC_CartSession_Ex();
        $total_inctax = 0;
        $total_point = 0;
        foreach ($objPage->cartItems as $cartkey => $item) {
            list($sub_total_inctax, $sub_total_point, $deliv_free_cnt) = plg_ProductOptions_Util::lfSetCartProductOptions($item);
            $total_inctax += $sub_total_inctax;
            $total_point += $sub_total_point;
            $objPage->cartItems[$cartkey] = $item;

            $objPage->tpl_total_inctax[$cartkey] = $sub_total_inctax;
            $objPage->tpl_total_point[$cartkey] = $sub_total_point;
            $objPage->arrData[$cartkey]['total'] = $sub_total_inctax;
            $objPage->arrData[$cartkey]['add_point'] = $sub_total_point;

            $objPage->tpl_deliv_free[$cartkey] = $objPage->arrInfo['free_rule'] - $objPage->tpl_total_inctax[$cartkey];

            $objPage->arrData[$cartkey]['is_deliv_free'] = false;
            // 送料無料の購入数が設定されている場合
            if (DELIV_FREE_AMOUNT > 0) {
                // 商品の合計数量
                $total_quantity = $objCartSess->getTotalQuantity($cartkey);

                if ($total_quantity >= DELIV_FREE_AMOUNT) {
                    $objPage->arrData[$cartkey]['is_deliv_free'] = true;
                }
            }

            // 送料無料条件が設定されている場合
            if ($objPage->arrInfo['free_rule'] > 0) {
                // 小計が送料無料条件以上の場合
                if ($objPage->tpl_total_inctax[$cartkey] >= $objPage->arrInfo['free_rule']) {
                    $objPage->arrData[$cartkey]['is_deliv_free'] = true;
                }
            }

            if ($deliv_free_cnt > 0) {
                $objPage->arrData[$cartkey]['is_deliv_free'] = true;
            }
        }
        $objPage->tpl_all_total_inctax = $total_inctax;
    }

}
