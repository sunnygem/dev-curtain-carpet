<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

class LC_Page_Admin_Products_OptionCategory extends LC_Page_Admin_Ex
{
    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init()
    {
        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/admin/products/optioncategory.tpl";
        $this->tpl_subno = 'options';
        $this->tpl_maintitle = '商品管理';
        $this->tpl_subtitle = 'オプション管理＞選択肢登録';
        $this->tpl_mainno = 'products';

        $this->arrACTION = plg_ProductOptions_Util::getActionList();
        $this->arrDELIVFREE = plg_ProductOptions_Util::getDelivFreeSel();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action()
    {

        $objFormParam = new SC_FormParam_Ex();

        // アップロードファイル情報の初期化
        $objUpFile = new SC_UploadFile_Ex(IMAGE_TEMP_REALDIR, IMAGE_SAVE_REALDIR);
        $this->lfInitFile($objUpFile);
        $objUpFile->setHiddenFileList($_POST);

        $this->lfInitParam($objFormParam);
        $_REQUEST['value'] = mb_ereg_replace('[ー－]', '-', $_REQUEST['value']);
        $objFormParam->setParam($_REQUEST);
        $objFormParam->convParam();
        $option_id = $objFormParam->getValue('option_id');
        $optioncategory_id = $objFormParam->getValue('optioncategory_id');

        $mode = $this->getMode();
        switch ($mode) {
            // 登録ボタン押下
            // 新規作成 or 編集
            case 'edit':
                // パラメーター値の取得
                $arrForm = $objFormParam->getHashArray();
                $arrRet = $objUpFile->getDBFileList();
                $arrForm = array_merge($arrForm, $arrRet);

                // 入力パラメーターチェック
                $this->arrErr = $this->lfCheckError($objFormParam);
                if (SC_Utils_Ex::isBlank($this->arrErr)) {
                    //新規規格追加かどうかを判定する
                    $is_insert = $this->lfCheckInsert($optioncategory_id);
                    if ($is_insert) {
                        //新規追加
                        $this->lfInsertOption($arrForm);
                    } else {
                        //更新
                        $this->lfUpdateOption($arrForm);
                    }
                    $objUpFile->moveTempFile();

                    // 再表示
                    SC_Response_Ex::reload();
                }
                $this->arrForm = $arrForm;
                break;
            // 削除
            case 'delete':
                // ランク付きレコードの削除
                $this->lfDeleteOptionCat($option_id, $optioncategory_id);

                SC_Response_Ex::reload();
                break;
            // 編集前処理
            case 'pre_edit':
                // 選択肢を取得する。
                $this->arrForm = plg_ProductOptions_Util::lfGetOptionCat($optioncategory_id);
                $objUpFile->setDBFileList($this->arrForm);
                break;
            case 'down':
                //並び順を下げる
                $this->lfDownRank($option_id, $optioncategory_id);
                $optioncategory_id = '';
                $this->tpl_onload = $this->getAnchorHash("list_table");
                break;
            case 'up':
                //並び順を上げる
                $this->lfUpRank($option_id, $optioncategory_id);
                $optioncategory_id = '';
                $this->tpl_onload = $this->getAnchorHash("list_table");
                break;
            // 画像のアップロード
            case 'upload_image':
            case 'delete_image':
                // パラメーター初期化
                $arrForm = $objFormParam->getHashArray();

                switch ($mode) {
                    case 'upload_image':
                        // ファイルを一時ディレクトリにアップロード
                        $this->arrErr[$arrForm['image_key']] = $objUpFile->makeTempFile($arrForm['image_key'], IMAGE_RENAME);
                        break;
                    case 'delete_image':
                        // ファイル削除
                        $this->lfDeleteTempFile($objUpFile, $arrForm['image_key']);
                        break;
                    default:
                        break;
                }
                $this->arrForm = $arrForm;
                break;
            default:
                break;
        }
        //オプション名の取得
        $this->tpl_option_name = plg_ProductOptions_Util::lfGetOptionName($option_id);
        $this->tpl_option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
        //オプション選択肢情報の取得
        $this->arrOptionCat = plg_ProductOptions_Util::lfGetOptionCatList($option_id);
        // POSTデータを引き継ぐ
        $this->tpl_optioncategory_id = $optioncategory_id;

        $this->arrHidden = $objUpFile->getHiddenFileList();
        $this->arrForm['arrFile'] = $objUpFile->getFormFileList(IMAGE_TEMP_URLPATH, IMAGE_SAVE_URLPATH);
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy()
    {
        if (method_exists('LC_Page_Admin_Ex', 'destroy')) {
            parent::destroy();
        }
    }

    /**
     * パラメーターの初期化を行う.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam)
    {
        $objFormParam->addParam('オプションID', 'option_id', INT_LEN, 'n', array('NUM_CHECK'));
        $objFormParam->addParam('選択肢名', 'name', STEXT_LEN, 'KVa', array('EXIST_CHECK', 'SPTAB_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('選択肢ID', 'optioncategory_id', INT_LEN, 'n', array('NUM_CHECK'));
        $objFormParam->addParam('金額/ポイント', 'value', INT_LEN, 'a');
        $objFormParam->addParam('説明文', 'description', LTEXT_LEN, 'KVa', array('MAX_LENGTH_CHECK'));
        $objFormParam->addParam('未選択肢設定', 'default_flg', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('初期値設定', 'init_flg', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('image_key', 'image_key', '', '', array());
    }

    /**
     * アップロードファイルパラメーター情報の初期化
     * - 画像ファイル用
     *
     * @param object $objUpFile SC_UploadFileインスタンス
     * @return void
     */
    function lfInitFile(&$objUpFile)
    {
        $objUpFile->addFile('説明画像', 'option_image', array('jpg', 'gif', 'png'), IMAGE_SIZE, false, NORMAL_IMAGE_WIDTH, NORMAL_IMAGE_HEIGHT);
    }

    /**
     * オプション選択肢情報を新規登録
     *
     * @param array $arrForm フォームパラメータークラス
     * @return integer 更新件数
     */
    function lfInsertOption($arrForm)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        // 親規格IDの存在チェック
        $where = 'del_flg <> 1 AND option_id = ?';
        $option_id = $objQuery->get('option_id', 'plg_productoptions_dtb_option', $where, array($arrForm['option_id']));
        if (!SC_Utils_Ex::isBlank($option_id)) {
            // INSERTする値を作成する。
            $sqlval['name'] = $arrForm['name'];
            $sqlval['value'] = $arrForm['value'];
            $sqlval['option_id'] = $arrForm['option_id'];
            $sqlval['option_image'] = $arrForm['option_image'];
            $sqlval['description'] = $arrForm['description'];
            $sqlval['default_flg'] = $arrForm['default_flg'];
            $sqlval['init_flg'] = $arrForm['init_flg'];
            $sqlval['creator_id'] = $_SESSION['member_id'];
            $sqlval['rank'] = $objQuery->max('rank', 'plg_productoptions_dtb_optioncategory', $where, array($arrForm['option_id'])) + 1;
            $sqlval['create_date'] = 'CURRENT_TIMESTAMP';
            $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
            // INSERTの実行
            $max = $objQuery->max('optioncategory_id', 'plg_productoptions_dtb_optioncategory') + 1;
            $next = $objQuery->nextVal('plg_productoptions_dtb_optioncategory_optioncategory_id');
            if ($max > $next) {
                $optioncategory_id = $max;
            } else {
                $optioncategory_id = $next;
            }
            $sqlval['optioncategory_id'] = $optioncategory_id;
            $ret = $objQuery->insert('plg_productoptions_dtb_optioncategory', $sqlval);
        }
        $objQuery->commit();
        return $ret;
    }

    /**
     * オプション選択肢情報を更新
     *
     * @param array $arrForm フォームパラメータークラス
     * @return integer 更新件数
     */
    function lfUpdateOption($arrForm)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        // UPDATEする値を作成する。
        $sqlval['name'] = $arrForm['name'];
        $sqlval['value'] = $arrForm['value'];
        $sqlval['option_image'] = $arrForm['option_image'];
        $sqlval['description'] = $arrForm['description'];
        $sqlval['default_flg'] = $arrForm['default_flg'];
        $sqlval['init_flg'] = $arrForm['init_flg'];
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $where = 'optioncategory_id = ?';
        // UPDATEの実行
        $ret = $objQuery->update('plg_productoptions_dtb_optioncategory', $sqlval, $where, array($arrForm['optioncategory_id']));
        return $ret;
    }

    /**
     * エラーチェック
     *
     * @param array $objFormParam フォームパラメータークラス
     * @return array エラー配列
     */
    function lfCheckError(&$objFormParam)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $arrForm = $objFormParam->getHashArray();
        // パラメーターの基本チェック
        $arrErr = $objFormParam->checkError();
        if (!SC_Utils_Ex::isBlank($arrErr)) {
            return $arrErr;
        } else {
            $arrForm = $objFormParam->getHashArray();
        }

        $option_action = plg_ProductOptions_Util::lfGetOptionAction($arrForm['option_id']);
        if (!is_numeric($arrForm['value']) && $option_action > 0) {
            $arrErr['value'] = '数字で指定してください。<br>';
        }

        $where = 'option_id = ? AND name = ?';
        $arrRet = $objQuery->select('optioncategory_id, name', 'plg_productoptions_dtb_optioncategory', $where, array($arrForm['option_id'], $arrForm['name']));
        // 編集中のレコード以外に同じ名称が存在する場合
        if ($arrRet[0]['optioncategory_id'] != $arrForm['optioncategory_id'] && $arrRet[0]['name'] == $arrForm['name']) {
            $arrErr['name'] = '※ 既に同じ内容の登録が存在します。<br>';
        }

        if ($objFormParam->getValue('default_flg') == 1) {
            $where = "default_flg = ? AND option_id = ?";
            $arrWhere = array(1, $arrForm['option_id']);
            $optioncategory_id = $objFormParam->getValue('optioncategory_id');
            if ($optioncategory_id > 0) {
                $where .= " AND optioncategory_id <> ?";
                $arrWhere[] = $optioncategory_id;
            }
            $objQuery = & SC_Query_Ex::getSingletonInstance();
            $cnt = $objQuery->get("count(optioncategory_id)", "plg_productoptions_dtb_optioncategory", $where, $arrWhere);
            if ($cnt > 0)
                $arrErr['default_flg'] = "既に未選択肢設定された選択肢が存在します。<br>";
        }

        if ($objFormParam->getValue('init_flg') == 1) {
            $where = "init_flg = ? AND option_id = ?";
            $arrWhere = array(1, $arrForm['option_id']);
            $optioncategory_id = $objFormParam->getValue('optioncategory_id');
            if ($optioncategory_id > 0) {
                $where .= " AND optioncategory_id <> ?";
                $arrWhere[] = $optioncategory_id;
            }
            $objQuery = & SC_Query_Ex::getSingletonInstance();
            $cnt = $objQuery->get("count(optioncategory_id)", "plg_productoptions_dtb_optioncategory", $where, $arrWhere);
            if ($cnt > 0)
                $arrErr['init_flg'] = "既に初期値設定された選択肢が存在します。<br>";
        }


        return $arrErr;
    }

    /**
     * 新規オプション選択肢追加かどうかを判定する.
     *
     * @param integer $optioncategory_id
     * @return boolean 新規オプション選択肢追加の場合 true
     */
    function lfCheckInsert($optioncategory_id)
    {
        if (empty($optioncategory_id)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * オプション選択肢情報を削除する
     *
     * @param integer $option_id オプションID
     * @param integer $optioncategory_id オプション選択肢ID
     * @return void
     */
    function lfDeleteOptionCat($option_id, $optioncategory_id)
    {
        $objDb = new SC_Helper_DB_Ex();
        $where = 'option_id = ' . SC_Utils_Ex::sfQuoteSmart($option_id);
        $objDb->sfDeleteRankRecord('plg_productoptions_dtb_optioncategory', 'optioncategory_id', $optioncategory_id, $where, true);
    }

    /**
     * 並び順を上げる
     *
     * @param integer $option_id オプションID
     * @param integer $optioncategory_id オプション選択肢ID
     * @return void
     */
    function lfUpRank($option_id, $optioncategory_id)
    {
        $objDb = new SC_Helper_DB_Ex();
        $where = 'option_id = ' . SC_Utils_Ex::sfQuoteSmart($option_id);
        $objDb->sfRankUp('plg_productoptions_dtb_optioncategory', 'optioncategory_id', $optioncategory_id, $where);
    }

    /**
     * 並び順を下げる
     *
     * @param integer $option_id オプションID
     * @param integer $optioncategory_id オプション選択肢ID
     * @return void
     */
    function lfDownRank($option_id, $optioncategory_id)
    {
        $objDb = new SC_Helper_DB_Ex();
        $where = 'option_id = ' . SC_Utils_Ex::sfQuoteSmart($option_id);
        $objDb->sfRankDown('plg_productoptions_dtb_optioncategory', 'optioncategory_id', $optioncategory_id, $where);
    }

    /**
     * アップロードファイルパラメーター情報から削除
     * 一時ディレクトリに保存されている実ファイルも削除する
     *
     * @param object $objUpFile SC_UploadFileインスタンス
     * @param string $image_key 画像ファイルキー
     * @return void
     */
    function lfDeleteTempFile(&$objUpFile, $image_key)
    {
        // TODO: SC_UploadFile::deleteFileの画像削除条件見直し要
        $arrTempFile = $objUpFile->temp_file;
        $arrKeyName = $objUpFile->keyname;

        foreach ($arrKeyName as $key => $keyname) {
            if ($keyname != $image_key)
                continue;

            if (!empty($arrTempFile[$key])) {
                $temp_file = $arrTempFile[$key];
                $arrTempFile[$key] = '';

                if (!in_array($temp_file, $arrTempFile)) {
                    $objUpFile->deleteFile($image_key);
                } else {
                    $objUpFile->temp_file[$key] = '';
                    $objUpFile->save_file[$key] = '';
                }
            } else {
                $objUpFile->temp_file[$key] = '';
                $objUpFile->save_file[$key] = '';
            }
        }
    }

    /**
     * アンカーハッシュ文字列を取得する
     * アンカーキーをサニタイジングする
     *
     * @param string $anchor_key フォーム入力パラメーターで受け取ったアンカーキー
     * @return <type>
     */
    function getAnchorHash($anchor_key)
    {
        if ($anchor_key != '') {
            return "location.hash='#" . htmlspecialchars($anchor_key) . "'";
        } else {
            return '';
        }
    }

}
