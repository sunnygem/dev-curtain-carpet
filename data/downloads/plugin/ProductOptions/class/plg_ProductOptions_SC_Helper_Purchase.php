<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

class plg_ProductOptions_SC_Helper_Purchase extends SC_Helper_Purchase
{

    /**
     * 配送商品を登録する.
     *
     * @param integer $order_id 受注ID
     * @param integer $shipping_id 配送先ID
     * @param array $arrParams 配送商品の配列
     * @return void
     */
    function registerShipmentItem($order_id, $shipping_id, $arrParams)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $table = 'dtb_shipment_item';
        $where = 'order_id = ? AND shipping_id = ?';
        $objQuery->delete($table, $where, array($order_id, $shipping_id));

        $objProduct = new SC_Product_Ex();
        foreach ($arrParams as $arrCartNos) {
            foreach ($arrCartNos as $arrValues) {
                if (SC_Utils_Ex::isBlank($arrValues['product_class_id'])) {
                    continue;
                }
                $d = $objProduct->getDetailAndProductsClass($arrValues['product_class_id']);
                $name = SC_Utils_Ex::isBlank($arrValues['product_name']) ? $d['name'] : $arrValues['product_name'];

                $code = SC_Utils_Ex::isBlank($arrValues['product_code']) ? $d['product_code'] : $arrValues['product_code'];

                $cname1 = SC_Utils_Ex::isBlank($arrValues['classcategory_name1']) ? $d['classcategory_name1'] : $arrValues['classcategory_name1'];

                $cname2 = SC_Utils_Ex::isBlank($arrValues['classcategory_name2']) ? $d['classcategory_name2'] : $arrValues['classcategory_name2'];

                $price = SC_Utils_Ex::isBlank($arrValues['price']) ? $d['price'] : $arrValues['price'];

                $arrValues['order_id'] = $order_id;
                $arrValues['shipping_id'] = $shipping_id;
                $arrValues['product_name'] = $name;
                $arrValues['product_code'] = $code;
                $arrValues['classcategory_name1'] = $cname1;
                $arrValues['classcategory_name2'] = $cname2;
                if (isset($arrValues['plg_productoptions'])) {
                    if (!is_array($arrValues['plg_productoptions'])) {
                        $options = array();
                    } else {
                        $options = $arrValues['plg_productoptions'];
                    }
                    $arrValues['plg_productoptions_flg'] = serialize($options);
                }
                if (!is_array(@unserialize($arrValues['plg_productoptions_flg'])))
                    $arrValues['plg_productoptions_flg'] = serialize(array());

                $arrExtractValues = $objQuery->extractOnlyColsOf($table, $arrValues);
                $objQuery->insert($table, $arrExtractValues);
            }
        }
    }

    /**
     * 配送商品を設定する.
     *
     * @param  integer $shipping_id      配送先ID
     * @param  integer $product_class_id 商品規格ID
     * @param  integer $quantity         数量
     * @return void
     */
    function setShipmentItemTemp($shipping_id, $product_class_id, $quantity, $cart_no = NULL)
    {
        $objProduct = new SC_Product_Ex();
        $objCartSession = new SC_CartSession_Ex();
        $cartKey = $objCartSession->getKey();
        // 詳細情報を取得
        $cartItems_buff = $objCartSession->getCartList($cartKey);
        plg_ProductOptions_Util::lfSetCartProductOptions($cartItems_buff);
        $cartItems = $cartItems_buff;

        $arrItems = array();
        foreach ($cartItems as $cartitem) {
            if (isset($cartitem['cart_no'])) {
                if ($cartitem['id'] == $product_class_id && $cartitem['cart_no'] == $cart_no) {
                    $arrItems['shipping_id'] = $shipping_id;
                    $arrItems['product_class_id'] = $product_class_id;
                    $arrItems['quantity'] = $quantity;
                    $arrItems['plg_productoptions'] = $cartitem['plg_productoptions'];

                    $product = & $objProduct->getDetailAndProductsClass($product_class_id);
                    $arrItems['productsClass'] = $product;

                    $arrItems['price'] = $cartitem['price'];
                    if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                        $inctax = SC_Helper_TaxRule_Ex::sfCalcIncTax($arrItems['price'], $arrItems['productsClass']['product_id'], $arrItems['productsClass']['product_class_id']);
                    } else {
                        $inctax = SC_Helper_DB_Ex::sfCalcIncTax($arrItems['price']);
                    }
                    $arrItems['total_inctax'] = $inctax * $arrItems['quantity'];

                    $_SESSION['shipping'][$shipping_id]['shipment_item'][$product_class_id][$cartitem['cart_no']] = $arrItems;
                    break;
                }
            }
        }
    }

    /**
     * 単一配送指定用に配送商品を設定する
     *
     * @param SC_CartSession $objCartSession カート情報のインスタンス
     * @param integer $shipping_id 配送先ID
     * @return void
     */
    function setShipmentItemTempForSole(&$objCartSession, $shipping_id = 0)
    {
        $objCartSess = new SC_CartSession_Ex();

        $this->clearShipmentItemTemp();

        $arrCartList = & $objCartSession->getCartList($objCartSess->getKey());
        foreach ($arrCartList as $arrCartRow) {
            if ($arrCartRow['quantity'] == 0)
                continue;
            $this->setShipmentItemTemp($shipping_id, $arrCartRow['id'], $arrCartRow['quantity'], $arrCartRow['cart_no']);
        }
    }

    /**
     * 受注詳細情報を登録する.
     *
     * 既に, 該当の受注が存在する場合は, 受注情報を削除し, 登録する.
     *
     * @param  integer $order_id  受注ID
     * @param  array   $arrParams 受注情報の連想配列
     * @return void
     */
    public function registerOrderDetail($order_id, $arrParams)
    {
        $table = 'dtb_order_detail';
        $where = 'order_id = ?';
        $objQuery = SC_Query_Ex::getSingletonInstance();

        $objQuery->delete($table, $where, array($order_id));
        foreach ($arrParams as $arrDetail) {
            if (!is_array(@unserialize($arrDetail['plg_productoptions_flg']))) {
                $arrDetail['plg_productoptions_flg'] = serialize(array());
            }
            $arrValues = $objQuery->extractOnlyColsOf($table, $arrDetail);
            $arrValues['order_detail_id'] = $objQuery->nextVal('dtb_order_detail_order_detail_id');
            $arrValues['order_id'] = $order_id;
            $objQuery->insert($table, $arrValues);
        }
    }

}
