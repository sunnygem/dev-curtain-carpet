<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

class plg_ProductOptions_SC_Product extends SC_Product
{

    /**
     * 商品IDに紐づく商品規格を自分自身に設定する.
     *
     * 引数の商品IDの配列に紐づく商品規格を取得し, 自分自身のフィールドに
     * 設定する.
     *
     * @param  array   $arrProductId 商品ID の配列
     * @param  boolean $has_deleted  削除された商品規格も含む場合 true; 初期値 false
     * @return void
     */
    public function setProductsClassByProductIds($arrProductId, $has_deleted = false)
    {
        foreach ($arrProductId as $productId) {
            $arrProductClasses = $this->getProductsClassFullByProductId($productId, $has_deleted);

            $classCats1 = array();
            $classCats1['__unselected'] = '選択してください';

            // 規格1クラス名
            $this->className1[$productId] = isset($arrProductClasses[0]['class_name1']) ? $arrProductClasses[0]['class_name1'] : '';

            // 規格2クラス名
            $this->className2[$productId] = isset($arrProductClasses[0]['class_name2']) ? $arrProductClasses[0]['class_name2'] : '';

            // 規格1が設定されている
            $this->classCat1_find[$productId] = $arrProductClasses[0]['classcategory_id1'] > 0; // 要変更ただし、他にも改修が必要となる
            // 規格2が設定されている
            $this->classCat2_find[$productId] = $arrProductClasses[0]['classcategory_id2'] > 0; // 要変更ただし、他にも改修が必要となる

            $this->stock_find[$productId] = false;
            $classCategories = array();
            $classCategories['__unselected']['__unselected']['name'] = '選択してください';
            $classCategories['__unselected']['__unselected']['product_class_id'] = $arrProductClasses[0]['product_class_id'];
            // 商品種別
            $classCategories['__unselected']['__unselected']['product_type'] = $arrProductClasses[0]['product_type_id'];
            $this->product_class_id[$productId] = $arrProductClasses[0]['product_class_id'];
            // 商品種別
            $this->product_type[$productId] = $arrProductClasses[0]['product_type_id'];
            foreach ($arrProductClasses as $arrProductsClass) {
                $arrClassCats2 = array();
                $classcategory_id1 = $arrProductsClass['classcategory_id1'];
                $classcategory_id2 = $arrProductsClass['classcategory_id2'];
                // 在庫
                $stock_find_class = ($arrProductsClass['stock_unlimited'] || $arrProductsClass['stock'] > 0);

                $arrClassCats2['classcategory_id2'] = $classcategory_id2;
                $arrClassCats2['name'] = $arrProductsClass['classcategory_name2'] . ($stock_find_class ? '' : ' (品切れ中)');

                $arrClassCats2['stock_find'] = $stock_find_class;

                if ($stock_find_class) {
                    $this->stock_find[$productId] = true;
                }

                if (!array_key_exists($classcategory_id1, $classCats1)) {
                    $classCats1[$classcategory_id1] = $arrProductsClass['classcategory_name1']
                            . ($classcategory_id2 == 0 && !$stock_find_class ? ' (品切れ中)' : '');
                }

                // 価格
                // TODO: ここでprice01,price02を税込みにしてよいのか？ _inctax を付けるべき？要検証
                if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                    $arrClassCats2['price01'] = strlen($arrProductsClass['price01']) ? number_format(SC_Helper_TaxRule_Ex::sfCalcIncTax($arrProductsClass['price01'], $productId, $arrProductsClass['product_class_id'])) : '';

                    $arrClassCats2['price02'] = strlen($arrProductsClass['price02']) ? number_format(SC_Helper_TaxRule_Ex::sfCalcIncTax($arrProductsClass['price02'], $productId, $arrProductsClass['product_class_id'])) : '';

                    $arrTaxRule = SC_Helper_TaxRule_Ex::getTaxRule($productId, $arrProductsClass['product_class_id']);
                    $arrClassCats2['tax_rate'] = $arrTaxRule['tax_rate'];
                    $arrClassCats2['tax_rule'] = $arrTaxRule['tax_rule'];
                } else {
                    $arrClassCats2['price01'] = strlen($arrProductsClass['price01']) ? number_format(SC_Helper_DB_Ex::sfCalcIncTax($arrProductsClass['price01'])) : '';

                    $arrClassCats2['price02'] = strlen($arrProductsClass['price02']) ? number_format(SC_Helper_DB_Ex::sfCalcIncTax($arrProductsClass['price02'])) : '';
                }

                $arrClassCats2['price01_exctax'] = strlen($arrProductsClass['price01']) ? number_format($arrProductsClass['price01']) : '';

                $arrClassCats2['price02_exctax'] = strlen($arrProductsClass['price02']) ? number_format($arrProductsClass['price02']) : '';

                // ポイント
                $arrClassCats2['point'] = number_format(SC_Utils_Ex::sfPrePoint($arrProductsClass['price02'], $arrProductsClass['point_rate']));

                // 商品コード
                $arrClassCats2['product_code'] = $arrProductsClass['product_code'];
                // 商品規格ID
                $arrClassCats2['product_class_id'] = $arrProductsClass['product_class_id'];
                // 商品種別
                $arrClassCats2['product_type'] = $arrProductsClass['product_type_id'];

                // #929(GC8 規格のプルダウン順序表示不具合)対応のため、2次キーは「#」を前置
                if (!$this->classCat1_find[$productId]) {
                    $classcategory_id1 = '__unselected2';
                }
                $classCategories[$classcategory_id1]['#'] = array(
                    'classcategory_id2' => '',
                    'name' => '選択してください',
                );
                $classCategories[$classcategory_id1]['#' . $classcategory_id2] = $arrClassCats2;
            }

            $this->classCategories[$productId] = $classCategories;

            // 規格1
            $this->classCats1[$productId] = $classCats1;
        }
    }

}
