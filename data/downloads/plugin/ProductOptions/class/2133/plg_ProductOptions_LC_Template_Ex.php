<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Template.php";

class plg_ProductOptions_LC_Template_Ex extends plg_ProductOptions_LC_Template
{

    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename)
    {
        $objTransform = new SC_Helper_Transform($source);
        $template_dir = PLUGIN_UPLOAD_REALDIR . 'ProductOptions/templates/';
        $template_ex_dir = PLUGIN_UPLOAD_REALDIR . 'ProductOptions/templates/2133/';

        switch ($objPage->arrPageLayout['device_type_id']) {
            case DEVICE_TYPE_PC:
                $template_dir .= "default/";
                $template_ex_dir .= "default/";
                if (strpos($filename, 'cart/index.tpl') !== false || strpos($filename, 'quick_cart_index.tpl' !== false)) {
                    $objTransform->select('td', 2, false)->appendChild(file_get_contents($template_dir . 'cart/index.tpl'));
                }
                if (strpos($filename, 'mypage/history.tpl') !== false) {
                    $objTransform->select('td', 1, false)->appendChild(file_get_contents($template_dir . 'mypage/history.tpl'));
                    $objTransform->select('td', 15, false)->appendChild(file_get_contents($template_dir . 'mypage/history_shipping.tpl'));
                }
                if (strpos($filename, 'products/list.tpl') !== false) {
                    $objTransform->select('.cartin', 0, false)->insertBefore(file_get_contents($template_dir . 'products/list.tpl'));
                    $objTransform->select('div#undercolumn', 0, false)->insertBefore(file_get_contents($template_ex_dir . 'products/list_js.tpl'));
                }
                if (strpos($filename, 'products/detail.tpl') !== false) {
                    $objTransform->select('.quantity', 0, false)->insertBefore(file_get_contents($template_ex_dir . 'products/detail.tpl'));
                }
                if (strpos($filename, 'shopping/multiple.tpl') !== false) {
                    $objTransform->select('form input', 5, false)->replaceElement(file_get_contents($template_dir . 'shopping/multiple_cart_no.tpl'));
                    $objTransform->select('td', 1, false)->replaceElement(file_get_contents($template_ex_dir . 'shopping/multiple.tpl'));
                }
                if (strpos($filename, 'shopping/confirm.tpl') !== false || strpos($filename, 'quick_shopping_confirm.tpl' !== false)) {
                    $objTransform->select('td', 1, false)->find('ul', 0, false)->appendChild(file_get_contents($template_dir . 'shopping/confirm_order.tpl'));
                    $objTransform->select('table', 3, false)->find('td', 1, false)->appendChild(file_get_contents($template_dir . 'shopping/confirm_shipping.tpl'));
                }
                break;
            case DEVICE_TYPE_MOBILE:
                $template_dir .= "mobile/";
                $template_ex_dir .= "mobile/";
                if (strpos($filename, 'products/select_item.tpl') !== false) {
                    $objTransform->select('form', 0, false)->appendFirst(file_get_contents($template_dir . 'products/select_item.tpl'));
                    $objTransform->select('', 0, false)->replaceElement(file_get_contents($template_dir . 'products/select_item_replace.tpl'));
                }
                if (strpos($filename, 'cart/index.tpl') !== false || strpos($filename, 'quick_cart_index.tpl' !== false)) {
                    $objTransform->select('form', 0, false)->replaceElement(file_get_contents($template_ex_dir . 'cart/index.tpl'));
                }
                if (strpos($filename, 'mypage/history.tpl') !== false) {
                    $objTransform->select('', 0, false)->replaceElement(file_get_contents($template_ex_dir . 'mypage/history.tpl'));
                }
                if (strpos($filename, 'shopping/multiple.tpl') !== false) {
                    $objTransform->select('form', 0, false)->replaceElement(file_get_contents($template_ex_dir . 'shopping/multiple.tpl'));
                }
                if (strpos($filename, 'shopping/confirm.tpl') !== false || strpos($filename, 'quick_shopping_confirm.tpl' !== false)) {
                    $objTransform->select('form', 0, false)->replaceElement(file_get_contents($template_ex_dir . 'shopping/confirm.tpl'));
                }
                break;
            case DEVICE_TYPE_SMARTPHONE:
                $template_dir .= "sphone/";
                $template_ex_dir .= "sphone/";
                if (strpos($filename, 'cart/index.tpl') !== false || strpos($filename, 'quick_cart_index.tpl' !== false)) {
                    $objTransform->select('div.cartitemBox div.cartinContents div p span.mini', 2, false)->insertBefore(file_get_contents($template_dir . 'cart/index.tpl'));
                }
                if (strpos($filename, 'products/detail.tpl') !== false) {
                    $objTransform->select('div.cartin_btn', 0, false)->insertBefore(file_get_contents($template_ex_dir . 'products/detail.tpl'));
                    $objTransform->select("div.review_btn", 0, false)->replaceElement(file_get_contents($template_dir . "products/detail_review_btn.tpl"));
                }
                if (strpos($filename, 'shopping/multiple.tpl') !== false) {
                    $objTransform->select('form input', 5, false)->replaceElement(file_get_contents($template_dir . 'shopping/multiple_cart_no.tpl'));
                    $objTransform->select('div.delivContents p', 0, false)->replaceElement(file_get_contents($template_ex_dir . 'shopping/multiple.tpl'));
                }
                if (strpos($filename, 'shopping/confirm.tpl') !== false || strpos($filename, 'quick_shopping_confirm.tpl' !== false)) {
                    $objTransform->select('div.cartconfirmContents div p', 0, false)->appendChild(file_get_contents($template_dir . 'shopping/confirm.tpl'));
                    $objTransform->select('section.delivconfirm_area div.form_area div.formBox div.cartcartconfirmarea div.cartconfirmBox div.cartconfirmContents p', 0, false)->appendChild(file_get_contents($template_dir . 'shopping/confirm.tpl'));
                }
                break;
            case DEVICE_TYPE_ADMIN:
            default:
                $template_dir .= "admin/";
                $template_ex_dir .= "admin/";
                if (strpos($filename, 'products/subnavi.tpl') !== false) {
                    $objTransform->select('ul.level1', 0)->appendChild(file_get_contents($template_dir . 'products/subnavi.tpl'));
                }
                if (strpos($filename, 'products/index.tpl') !== false) {
                    $objTransform->select('table#products-search-result tr th', 10)->insertBefore(file_get_contents($template_dir . 'products/index_th.tpl'));
                    $objTransform->select('table#products-search-result tr td', 10)->insertBefore(file_get_contents($template_dir . 'products/index_td.tpl'));
                    $objTransform->select('table#products-search-result col', 6)->replaceElement(file_get_contents($template_dir . 'products/index_col.tpl'));
                    $objTransform->select('table#products-search-result col', 5)->replaceElement(file_get_contents($template_dir . 'products/index_col2.tpl'));
                }
                if (strpos($filename, 'order/edit.tpl') !== false || strpos($filename, 'order_edit.tpl') !== false) {
                    $objTransform->select('form#form1', 0, false)->appendFirst(file_get_contents($template_dir . 'order/edit_hidden.tpl'));
                    $objTransform->select('input', 65, false)->insertAfter(file_get_contents($template_ex_dir . 'order/edit_shipmentitem_hidden.tpl'));
                    $objTransform->select('table.list td', 1, false)->replaceElement(file_get_contents($template_ex_dir . 'order/edit_detail.tpl'));
                    $objTransform->select('a.btn-normal', 11, false)->insertBefore(file_get_contents($template_ex_dir . 'order/edit_shipmentitem.tpl'));
                }
                if (strpos($filename, 'order/multiple.tpl') !== false) {
                    $objTransform->select('', 0)->replaceElement(file_get_contents($template_dir . 'order/multiple.tpl'));
                }
                if (strpos($filename, 'order/product_select.tpl') !== false) {
                    $objTransform->select('table.list td', 2)->appendChild(file_get_contents($template_dir . 'order/product_select.tpl'));
                    $objTransform->select('table.list td.center', 1)->replaceElement(file_get_contents($template_dir . 'order/product_select_submit.tpl'));
                    $objTransform->select('form#form1', 0)->insertBefore(file_get_contents($template_ex_dir . 'order/product_select_js.tpl'));
                }
                if (strpos($filename, 'order/disp.tpl') !== false) {
                    $objTransform->select('table.list td', 1)->replaceElement(file_get_contents($template_dir . 'order/disp.tpl'));
                    $objTransform->select('table.list', 1)->find('td', 1)->appendChild(file_get_contents($template_ex_dir . 'order/edit_shipmentitem.tpl'));
                }
                break;
        }
        $source = $objTransform->getHTML();
    }

}
