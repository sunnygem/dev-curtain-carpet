<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

class plg_ProductOptions_SC_DB
{

    var $_arrRet = array();
    var $_arrRetOption = array();
    var $_arrRetOptionCategory = array();
    var $_arrRetOptionCatList = array();
    var $_arrRetOptionInitCatId = null;
    var $_arrRetOptionDefaultCatId = null;
    public static $arrPoolInstance;

    function lfGetOptionInfo($option_id)
    {
        if (isset($this->_arrRetOption[$option_id])) {
            return $this->_arrRetOption[$option_id];
        }

        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $where = 'option_id = ?';
        $ret = $objQuery->getRow('*', 'plg_productoptions_dtb_option', $where, array($option_id));

        $this->_arrRetOption[$option_id] = $ret;

        return $ret;
    }

    function lfGetOptionCategoryInfo($optioncategory_id)
    {
        if (isset($this->_arrRetOptionCategory[$optioncategory_id])) {
            return $this->_arrRetOptionCategory[$optioncategory_id];
        }

        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $where = 'optioncategory_id = ?';
        $ret = $objQuery->getRow('*', 'plg_productoptions_dtb_optioncategory', $where, array($optioncategory_id));

        $this->_arrRetOptionCategory[$optioncategory_id] = $ret;

        return $ret;
    }

    function lfGetOptionCatList($option_id)
    {
        if (isset($this->_arrRetOptionCatList[$option_id])) {
            return $this->_arrRetOptionCatList[$option_id];
        }
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $where = 'del_flg <> 1 AND option_id = ?';
        $objQuery->setOrder('rank DESC'); // XXX 降順
        $arrOptionCat = $objQuery->select('*', 'plg_productoptions_dtb_optioncategory', $where, array($option_id));

        $this->_arrRetOptionCatList[$option_id] = $arrOptionCat;

        return $arrOptionCat;
    }

    function lfGetOptionDefaultCatId($option_id)
    {
        if (!is_null($this->_arrRetOptionDefaultCatId[$option_id])) {
            return $this->_arrRetOptionDefaultCatId[$option_id];
        }

        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $where = 'del_flg <> 1 AND option_id = ? AND default_flg = 1';
        $optioncategory_id = $objQuery->get('optioncategory_id', 'plg_productoptions_dtb_optioncategory', $where, array($option_id));

        $this->_arrRetOptionDefaultCatId[$option_id] = $optioncategory_id;

        return $optioncategory_id;
    }

    function lfGetOptionInitCatId($option_id)
    {
        if (!is_null($this->_arrRetOptionInitCatId[$option_id])) {
            return $this->_arrRetOptionInitCatId[$option_id];
        }

        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $where = 'del_flg <> 1 AND option_id = ? AND init_flg = 1';
        $optioncategory_id = $objQuery->get('optioncategory_id', 'plg_productoptions_dtb_optioncategory', $where, array($option_id));

        $this->_arrRetOptionInitCatId[$option_id] = $optioncategory_id;

        return $optioncategory_id;
    }

    function lfGetOption($flg = false)
    {
        $key = $flg ? 1 : 0;
        if (isset($this->_arrRet[$key])) {
            return $this->_arrRet[$key];
        }

        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $where = 'del_flg <> 1';
        $objQuery->setOrder('rank DESC');
        $arrClass = $objQuery->select('*', 'plg_productoptions_dtb_option', $where);

        if ($flg) {
            $objQuery = & SC_Query_Ex::getSingletonInstance();
            foreach ($arrClass as $key => $class) {
                if ($class['type'] == 3 || $class['type'] == 4)
                    continue;
                $cnt = $objQuery->get('count(optioncategory_id)', 'plg_productoptions_dtb_optioncategory', 'option_id = ?', array($class['option_id']));
                if ($cnt == 0)
                    unset($arrClass[$key]);
            }
            $arrRet = array();
            foreach ($arrClass as $class) {
                $arrRet[] = $class;
            }
        } else {
            $arrRet = $arrClass;
        }

        $this->_arrRet[$key] = $arrRet;

        return $arrRet;
    }

    public static function getSingletonInstance()
    {
        $objThis = plg_ProductOptions_SC_DB::getPoolInstance();
        if (is_null($objThis)) {
            $objThis = plg_ProductOptions_SC_DB::setPoolInstance(new plg_ProductOptions_SC_DB());
        }

        return $objThis;
    }

    /**
     * インスタンスをプールする
     *
     */
    public static function setPoolInstance(&$objThis)
    {
        return plg_ProductOptions_SC_DB::$arrPoolInstance = $objThis;
    }

    /**
     * プールしているインスタンスを取得する
     *
     */
    public static function getPoolInstance()
    {
        if (isset(plg_ProductOptions_SC_DB::$arrPoolInstance)) {
            return plg_ProductOptions_SC_DB::$arrPoolInstance;
        }
    }

}
