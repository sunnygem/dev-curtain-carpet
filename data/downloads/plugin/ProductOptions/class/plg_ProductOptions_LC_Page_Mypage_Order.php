<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Mypage_Order extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Mypage_Order $objPage 再注文ページクラス
     * @return void
     */
    function before($objPage)
    {
        //受注詳細データの取得
        $arrOrderDetail = self::lfGetMypageOrderDetail($_POST['order_id']);

        //ログインしていない、またはDBに情報が無い場合
        if (empty($arrOrderDetail)) {
            SC_Utils_Ex::sfDispSiteError(CUSTOMER_ERROR);
        }

        $objCartSess = new SC_CartSession_Ex();
        foreach ($arrOrderDetail as $detail) {
            $product_class_id = $detail['product_class_id'];
            $quantity = $detail['quantity'];
            $plg_productoptions = @unserialize($detail['plg_productoptions_flg']);
            foreach ($plg_productoptions as $option_id => $optioncategory_id) {
                if (is_null($optioncategory_id) || $optioncategory_id == "")
                    unset($plg_productoptions[$key]);
            }
            $objCartSess->addProduct($detail['product_class_id'], $detail['quantity'], $plg_productoptions);
        }

        self::sendRedirect(CART_URLPATH);
        exit;
    }

    /**
     * @param LC_Page_Mypage_Order $objPage 再注文ページクラス
     * @return void
     */
    function after($objPage)
    {
        parent::after($objPage);
    }

    // 受注詳細データの取得
    function lfGetMypageOrderDetail($order_id)
    {
        $objQuery = SC_Query_Ex::getSingletonInstance();

        $objCustomer = new SC_Customer_Ex();
        //customer_idを検証
        $customer_id = $objCustomer->getValue('customer_id');
        $order_count = $objQuery->count('dtb_order', 'order_id = ? and customer_id = ?', array($order_id, $customer_id));
        if ($order_count != 1)
            return array();

        $col = 'dtb_order_detail.product_class_id, quantity,dtb_order_detail.plg_productoptions_flg';
        $table = 'dtb_order_detail LEFT JOIN dtb_products_class ON dtb_order_detail.product_class_id = dtb_products_class.product_class_id';
        $where = 'order_id = ?';
        $objQuery->setOrder('order_detail_id');
        $arrOrderDetail = $objQuery->select($col, $table, $where, array($order_id));
        return $arrOrderDetail;
    }

}
