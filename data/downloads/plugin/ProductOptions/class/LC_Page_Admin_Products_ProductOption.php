<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/admin/products/LC_Page_Admin_Products_Ex.php';
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

class LC_Page_Admin_Products_ProductOption extends LC_Page_Admin_Products_Ex
{
    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init()
    {
        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/admin/products/product_option.tpl";
        $this->tpl_subnavi = 'products/subnavi.tpl';
        $this->tpl_mainno = 'products';
        $this->tpl_subno = 'product';
        $this->tpl_maintitle = '商品管理';
        $this->tpl_subtitle = '商品登録(オプション設定)';

        $this->arrACTION = plg_ProductOptions_Util::getActionList();
        $this->arrTYPE = plg_ProductOptions_Util::getTypeList();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action()
    {
        $objFormParam = new SC_FormParam_Ex();

        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();
        $product_id = $objFormParam->getValue('product_id');
        $this->tpl_product_id = $product_id;
        $this->tpl_product_name = $this->lfGetProductName($product_id);

        // 検索パラメーター引き継ぎ
        $this->arrSearchHidden = $this->lfGetSearchParam($_POST);

        switch ($this->getMode()) {
            // 編集実行
            case 'edit':
                $this->arrErr = $objFormParam->checkError();
                // エラーの無い場合は確認画面を表示
                if (SC_Utils_Ex::isBlank($this->arrErr)) {
                    $arrForm = $objFormParam->getHashArray();
                    $this->lfUpdateData($product_id, $arrForm);
                    $this->arrForm = $arrForm;
                    $message = 'オプション設定が完了しました。';
                    $this->tpl_onload = "window.alert('" . $message . "');";
                }
                // エラーが発生した場合
                else {
                    $this->arrForm = $objFormParam->getHashArray();
                }
                break;

            // 初期表示
            case 'pre_edit':
                $this->arrForm['check'] = $this->lfGetProductOptionID($product_id);
                break;

            default:
                break;
        }

        $this->arrOption = plg_ProductOptions_Util::lfGetOption(true);
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy()
    {
        if (method_exists('LC_Page_Admin_Products_Ex', 'destroy')) {
            parent::destroy();
        }
    }

    /**
     * パラメーターの初期化を行う.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam)
    {
        $objFormParam->addParam('product_id', 'product_id', INT_LEN, 'n', array('EXIST_CHECK', 'NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('チェックボックス', 'check', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
    }

    /**
     * 商品名の取得
     *
     * @param integer $product_id 商品ID
     * @return string 商品名
     */
    function lfGetProductName($product_id)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $where = 'product_id = ?';
        $name = $objQuery->get('name', 'dtb_products', $where, array($product_id));
        return $name;
    }

    function lfUpdateData($product_id, $arrData)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $sqlval = array();
        $strCheck = "";
        foreach ($arrData['check'] as $key => $item) {
            $strCheck .= $key . ",";
        }
        $strCheck = rtrim($strCheck, ',');
        $sqlval['plg_productoptions_flg'] = $strCheck;

        $objQuery->update("dtb_products", $sqlval, "product_id = ?", array($product_id));
    }

    /**
     * 商品に設定されているオプション情報の取得
     *
     * @param integer $product_id 商品ID
     * @return array オプションID
     */
    function lfGetProductOptionID($product_id)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $strCheck = $objQuery->get("plg_productoptions_flg", "dtb_products", "product_id = ?", array($product_id));
        $arrCheck = explode(',', $strCheck);
        foreach ($arrCheck as $option_id) {
            $arrRet[$option_id] = 1;
        }
        return $arrRet;
    }

    /**
     * 検索パラメーター引き継ぎ用配列取得
     *
     * @param array $arrPost $_POSTデータ
     * @return array 検索パラメーター配列
     */
    function lfGetSearchParam($arrPost)
    {
        $arrSearchParam = array();
        $objFormParam = new SC_FormParam_Ex();

        parent::lfInitParam($objFormParam);
        $objFormParam->setParam($arrPost);
        $arrSearchParam = $objFormParam->getSearchArray();

        return $arrSearchParam;
    }

}
