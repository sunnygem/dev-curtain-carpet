<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * プラグイン のアップデート用クラス.
 *
 * @package ProductOptions
 * @author Bratech CO.,LTD.
 * @version $Id: $
 */
class plugin_update
{

    /**
     * アップデート
     * updateはアップデート時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function update($arrPlugin)
    {
        $version = floor(str_replace('.', '', $arrPlugin['plugin_version']));
        $plugin_dir_path = PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . '/';
        SC_Utils_Ex::copyDirectory(DOWNLOADS_TEMP_PLUGIN_UPDATE_DIR, $plugin_dir_path);

        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $sqlval_plugin = array();
        $sqlval_plugin['plugin_version'] = "2.3.7";
        $sqlval_plugin['update_date'] = 'CURRENT_TIMESTAMP';

        $objQuery->update('dtb_plugin', $sqlval_plugin, "plugin_code = ?", array($arrPlugin['plugin_code']));

        if ($version <= 101) {
            $objQuery->query("ALTER TABLE plg_productoptions_dtb_option ADD COLUMN action int DEFAULT 0");
            $objQuery->query("UPDATE plg_productoptions_dtb_option SET action = type");
            $objQuery->query("UPDATE plg_productoptions_dtb_option SET type = 1");
        }

        if ($version <= 111) {
            $objQuery->query("ALTER TABLE plg_productoptions_dtb_option ADD COLUMN manage_name text");
            $objQuery->query("ALTER TABLE plg_productoptions_dtb_optioncategory ADD COLUMN default_flg int DEFAULT 0");
            $objQuery->query("UPDATE plg_productoptions_dtb_option SET manage_name = name");
        }

        if ($version >= 120 && $version <= 124) {
            require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                if (copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/templates/213/default/mail_templates/plg_productoptions_order_mail.tpl", TEMPLATE_REALDIR . "mail_templates/plg_productoptions_order_mail.tpl") === false)
                    print_r("失敗");
                if (copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/templates/213/mobile/mail_templates/plg_productoptions_order_mail.tpl", MOBILE_TEMPLATE_REALDIR . "mail_templates/plg_productoptions_order_mail.tpl") === false)
                    print_r("失敗");
            }
        }

        if ($version <= 128) {
            $objQuery->query("ALTER TABLE plg_productoptions_dtb_option ADD COLUMN is_required smallint DEFAULT 0");
        }

        if ($version <= 130) {
            if (DB_TYPE == "pgsql") {
                $objQuery->query("ALTER TABLE dtb_shipment_item ADD COLUMN plg_productoptions_flg text");
            } elseif (DB_TYPE == "mysql") {
                $objQuery->query("ALTER TABLE dtb_shipment_item ADD COLUMN plg_productoptions_flg longtext");
            }

            $objQuery->update("dtb_plugin", array("free_field1" => '0'), "plugin_code = ?", array("ProductOptions"));

            if ($arrPlugin['enable'] == 1) {
                copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/admin/products/options.php", HTML_REALDIR . ADMIN_DIR . "products/options.php");
                copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/admin/products/optioncategory.php", HTML_REALDIR . ADMIN_DIR . "products/optioncategory.php");
                copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/admin/products/product_option.php", HTML_REALDIR . ADMIN_DIR . "products/product_option.php");
                copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/products/product_options.php", HTML_REALDIR . "products/product_options.php");
            }
        }

        if ($version <= 201) {
            $objQuery->query("ALTER TABLE plg_productoptions_dtb_option ADD COLUMN description text");
        }

        if ($version <= 210) {
            $objQuery->query("ALTER TABLE plg_productoptions_dtb_optioncategory ADD COLUMN init_flg smallint DEFAULT 0");
            $objQuery->update("plg_productoptions_dtb_optioncategory", array("init_flg" => 1), "default_flg = ?", array(1));
        }

        if ($version <= 226) {
            $objQuery->query("CREATE INDEX plg_productoptions_dtb_optioncategory_option_id ON plg_productoptions_dtb_optioncategory (option_id)");
        }

        $objQuery->commit();
    }

}

?>