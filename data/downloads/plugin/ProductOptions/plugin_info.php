<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * プラグイン の情報クラス.
 *
 * @package ProductOptions
 * @author Bratech CO.,LTD.
 * @version $Id: $
 */
class plugin_info
{

    /** プラグインコード(必須)：プラグインを識別する為キーで、他のプラグインと重複しない一意な値である必要がありま. */
    static $PLUGIN_CODE = "ProductOptions";

    /** プラグイン名(必須)：EC-CUBE上で表示されるプラグイン名. */
    static $PLUGIN_NAME = "商品オプションプラグイン";

    /** クラス名(必須)：プラグインのクラス（拡張子は含まない） */
    static $CLASS_NAME = "ProductOptions";

    /** プラグインバージョン(必須)：プラグインのバージョン. */
    static $PLUGIN_VERSION = "2.3.7";

    /** 対応バージョン(必須)：対応するEC-CUBEバージョン. */
    static $COMPLIANT_VERSION = "2.12, 2.13";

    /** 作者(必須)：プラグイン作者. */
    static $AUTHOR = "株式会社ブラテック";

    /** 説明(必須)：プラグインの説明. */
    static $DESCRIPTION = "商品注文時に規格とは別にオプションを付加できるようにします";

    /** プラグインURL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $PLUGIN_SITE_URL = "http://www.bratech.co.jp";
    static $AUTHOR_SITE_URL = "http://www.bratech.co.jp";

    /** フックポイント * */
    static $HOOK_POINTS = array(
        array('LC_Page_Products_List_action_before'),
        array('LC_Page_Products_List_action_after'),
        array('LC_Page_Products_Detail_action_before'),
        array('LC_Page_Products_Detail_action_after'),
        array('LC_Page_Cart_action_after'),
        array('LC_Page_FrontParts_Bloc_Cart_action_after'),
        array('LC_Page_Shopping_Multiple_action_before'),
        array('LC_Page_Shopping_Multiple_action_after'),
        array('LC_Page_Shopping_Confirm_action_after'),
        array('LC_Page_Mypage_History_action_after'),
        array('LC_Page_Mypage_Order_action_before'),
        array('LC_Page_Admin_Order_Edit_action_before'),
        array('LC_Page_Admin_Order_Edit_action_after'),
        array('LC_Page_Admin_Order_Disp_action_after'),
        array('LC_Page_Admin_Order_ProductSelect_action_after')
    );

    /** ライセンス */
    static $LICENSE = "独自ライセンス";

}

?>