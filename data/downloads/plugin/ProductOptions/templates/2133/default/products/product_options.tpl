<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_header.tpl" subtitle="$tpl_title"}-->

<style type="text/css">
    <!--
    body {
        background-color: #FFFFFF;
    }

    h3 {
        font-size:14px;
    }

    div.productoptionlist {
        margin-bottom: 10px;
        padding-bottom: 10px;
        background: url("<!--{$TPL_URLPATH}-->img/background/line_dot_02.gif") repeat-x bottom ;
    }

    p.description {
        background-color:#F3F3F3;
        padding:5px 5px 5px 5px;
    }
    -->
</style>


<div id="window_area">
    <h2 class="title"><!--{$tpl_option_name|h}--></h2>
    <p><!--{$tpl_option_description|h|nl2br}--></p>
    <!--{foreach from=$arrOptionsCatList item=item}-->
    <!--{if $item.default_flg != 1}-->
    <div class="productoptionlist">
        <!--{if $item.option_image|strlen > 0}-->
        <p><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.option_image}-->" alt="<!--{$item.name|h}-->" /></p>
        <!--{/if}-->
        <h3><!--{$item.name|h}--></h3>
        <!--{if $tpl_option_action == 1}--><!--{if $item.value > 0}--><p style="color:#FF0000;">金額：<!--{$item.value|sfCalcIncTax|n2s}--> 円</p><!--{elseif $item.value < 0}--><p style="color:#0000FF;">金額：<!--{$item.value|sfCalcIncTax|n2s}--> 円</p><!--{/if}-->
        <!--{elseif $tpl_option_action == 2}--><!--{if $item.value > 0}--><p style="color:#FF0000;"><!--{$item.value|n2s}--> ポイントプレゼント</p><!--{/if}-->
        <!--{elseif $tpl_option_action == 3}--><p style="color:#FF0000;"><!--{if $item.value == 1}-->送料無料になります<!--{else}-->送料無料にはなりません<!--{/if}--></p>
        <!--{/if}-->
        <!--{if $item.description|strlen > 0}-->
        <p class="description"><!--{$item.description}--></p>
        <!--{/if}-->
    </div>
    <!--{/if}-->
    <!--{/foreach}-->
</div>

<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_footer.tpl"}-->
