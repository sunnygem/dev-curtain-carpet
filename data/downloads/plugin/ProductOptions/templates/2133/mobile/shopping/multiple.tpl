<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<form method="post" action="?">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->">
    <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->">
    <input type="hidden" name="line_of_num" value="<!--{$arrForm.line_of_num.value}-->">
    <input type="hidden" name="mode" value="confirm">
    <!--{section name=line loop=$arrForm.line_of_num.value}-->
    <!--{assign var=index value=$smarty.section.line.index}-->
    <input type="hidden" name="cart_no[<!--{$index}-->]" value="<!--{$arrForm.cart_no.value[$index]|h}-->" />
    <!--{assign var=key value="product_class_id"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="name"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="class_name1"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="class_name2"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="classcategory_name1"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="classcategory_name2"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="main_image"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="main_list_image"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="price"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="price_inctax"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="plg_productoptions_option_name"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">
    <!--{assign var=key value="plg_productoptions_optioncategory_name"}-->
    <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->">

    <!--{* 商品名 *}-->◎<!--{$arrForm.name.value[$index]|h}--><br>
    <!--{* 規格名1 *}--><!--{if $arrForm.classcategory_name1.value[$index] != ""}--><!--{$arrForm.class_name1.value[$index]|h}-->：<!--{$arrForm.classcategory_name1.value[$index]|h}--><br><!--{/if}-->
    <!--{* 規格名2 *}--><!--{if $arrForm.classcategory_name2.value[$index] != ""}--><!--{$arrForm.class_name2.value[$index]|h}-->：<!--{$arrForm.classcategory_name2.value[$index]|h}--><br><!--{/if}-->
    <!--{if $arrForm.plg_productoptions_optioncategory_name.value[$index]|@count > 0}-->
    <!--{foreach from=$arrForm.plg_productoptions_option_name.value[$index] item=item name=plg_options_loop}-->
    <!--{assign var=option value=$arrForm.plg_productoptions_option_name.value[$index]}-->
    <!--{assign var=optioncategory value=$arrForm.plg_productoptions_optioncategory_name.value[$index]}-->
    <!--{assign var=index2 value=$smarty.foreach.plg_options_loop.index}-->
    <!--{$option[$index2]}-->:<!--{$optioncategory[$index2]|h|nl2br}--><br>
    <!--{/foreach}-->
    <!--{/if}-->
    <!--{* 販売価格 *}-->
    <!--{math assign=price equation="a+b" a=$arrForm.price_inctax.value[$index] b=$arrForm.add_price_inctax.value[$index]}-->
    <!--{$price|n2s}-->円<br>

    <!--{assign var=key value="quantity"}-->
    <!--{if $arrErr[$key][$index] != ''}-->
    <font color="#FF0000"><!--{$arrErr[$key][$index]}--></font>
    <!--{/if}-->
    数量：<input type="text" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" size="4">
    <br>

    <!--{assign var=key value="shipping"}-->
    <!--{if strlen($arrErr[$key][$index]) >= 1}-->
    <font color="#FF0000"><!--{$arrErr[$key][$index]}--></font>
    <!--{/if}-->
    お届け先：<br>
    <select name="<!--{$key}-->[<!--{$index}-->]">
        <!--{html_options options=$addrs selected=$arrForm[$key].value[$index]}-->
    </select>
    <br>
    <br>
    <!--{/section}-->
    <center><input type="submit" value="選択したお届け先に送る"></center>
</form>