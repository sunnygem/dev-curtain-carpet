<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->


<h2><!--{$tpl_option_name|h}--></h2>
<!--{$tpl_option_description|h|nl2br}--><br>
<!--{foreach from=$arrOptionsCatList item=item}-->
<!--{if $item.default_flg != 1}-->
<!--{if $item.option_image|strlen > 0}-->
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.option_image}-->" alt="<!--{$item.name|h}-->" /><br>
<!--{/if}-->
<!--{$item.name|h}--><br><br>
<!--{if $tpl_option_action == 1}--><!--{if $item.value > 0}--><font color="#FF0000">金額：<!--{$item.value|sfCalcIncTax|n2s}--> 円</font><br><!--{elseif $item.value < 0}--><font color="#0000FF">金額：<!--{$item.value|sfCalcIncTax|n2s}--> 円</font><br><!--{/if}-->
<!--{elseif $tpl_option_action == 2}--><!--{if $item.value > 0}--><font color="#FF0000"><!--{$item.value|n2s}--> ポイントプレゼント</font><br /><!--{/if}-->
<!--{elseif $tpl_option_action == 3}--><font color="#FF0000"><!--{if $item.value == 1}-->送料無料になります<!--{else}-->送料無料にはなりません<!--{/if}--></font><br />
<!--{/if}-->
<!--{if $item.description|strlen > 0}-->
<!--{$item.description}-->
<!--{/if}-->
<hr>
<!--{/if}-->
<!--{/foreach}-->
