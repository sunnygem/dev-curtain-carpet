<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<script type="text/javascript">
<!-- 
    function fnChangeMode(mode) {
    document.form1['mode'].value = mode;
    }
//-->
</script>

<form name="form1" id="form1" method="post" action="" enctype="multipart/form-data">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="image_key" value="" />
    <input type="hidden" name="optioncategory_id" value="<!--{$tpl_optioncategory_id}-->" />
    <!--{foreach key=key item=item from=$arrHidden}-->
    <input type="hidden" name="<!--{$key}-->" value="<!--{$item|h}-->" />
    <!--{/foreach}-->
    <div id="products" class="contents-main">
        <div style="float:right; margin-bottom:5px;"><a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('form1', '', 'optioncategory_id', '0'); return false;">新規登録画面</a></div>
        <table style="float:left;">
            <tr>
                <th>オプション名</th>
                <td><!--{$tpl_option_name}--></td>
                <th>アクション</th>
                <td><!--{$arrACTION[$tpl_option_action]}--></td>
            </tr>
            <tr>
                <th>選択肢名<span class="attention"> *</span></th>
                <td>
                    <!--{if $arrErr.name}-->
                    <span class="attention"><!--{$arrErr.name}--></span>
                    <!--{/if}-->
                    <input type="text" name="name" value="<!--{$arrForm.name|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="" size="30" class="box30" />
                    <span class="attention"> (上限<!--{$smarty.const.STEXT_LEN}-->文字)</span>
                </td>
                <th><!--{if $tpl_option_action == 1}-->金額<!--{elseif $tpl_option_action == 2 || $tpl_option_action == 4}-->ポイント<!--{elseif $tpl_option_action == 3}-->送料設定<!--{/if}--></th>
                <td>
                    <!--{if $arrErr.value}-->
                    <span class="attention"><!--{$arrErr.value}--></span>
                    <!--{/if}-->
                    <!--{if $tpl_option_action == 1 || $tpl_option_action == 2 || $tpl_option_action == 4}-->
                    <input type="text" name="value" value="<!--{$arrForm.value|h}-->" maxlength="<!--{$smarty.const.INT_LEN}-->" style="" size="15" class="box15" /><!--{if $tpl_option_action == 1}-->円<!--{elseif $tpl_option_action == 2 || $tpl_option_action == 4}-->Pt<!--{/if}-->
                    <!--{elseif $tpl_option_action == 3}-->
                    <!--{html_options name="value" options=$arrDELIVFREE selected=$arrForm.value}-->
                    <!--{else}-->
                    設定なし
                    <!--{/if}-->
                </td>
            </tr>
            <tr>
                <th>説明文</th>
                <td colspan="3">
                    <!--{if $arrErr.description}-->
                    <span class="attention"><!--{$arrErr.description}--></span>
                    <!--{/if}-->
                    <textarea name="description" maxlength="<!--{$smarty.const.LTEXT_LEN}-->" style="<!--{if $arrErr.description != ""}-->background-color: <!--{$smarty.const.ERR_COLOR}-->;<!--{/if}-->" cols="60" rows="8" class="area60"><!--{$arrForm.description|h}--></textarea><br />
                    <span class="attention"> (上限<!--{$smarty.const.LTEXT_LEN}-->文字)</span>
                </td>
            </tr>
            <tr>
                <th>説明画像<br />[<!--{$smarty.const.NORMAL_IMAGE_WIDTH}-->×<!--{$smarty.const.NORMAL_IMAGE_HEIGHT}-->]</th>
                <!--{assign var=key value="option_image"}-->
                <td colspan="3">
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{if $arrForm.arrFile[$key].filepath != ""}-->
                    <img src="<!--{$arrForm.arrFile[$key].filepath}-->" alt="" />　<a href="" onclick="fnModeSubmit('delete_image', 'image_key', '<!--{$key}-->'); return false;">[画像の取り消し]</a><br />
                    <!--{/if}-->
                    <input type="file" name="<!--{$key}-->" size="40" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" />
                    <a class="btn-normal" href="javascript:;" name="btn" onclick="fnModeSubmit('upload_image', 'image_key', '<!--{$key}-->'); return false;">アップロード</a>
                </td>            
            </tr>
            <tr>
                <th>未選択設定</th>
                <!--{assign var=key value="default_flg"}-->
                <td colspan="3">
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <input type="checkbox" name="<!--{$key}-->" value="1" <!--{if $arrForm[$key] == 1}-->checked<!--{/if}--> />この選択肢を未選択扱いとする
                </td>
            </tr>
            <tr>
                <th>初期値設定</th>
                <!--{assign var=key value="init_flg"}-->
                <td colspan="3">
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <input type="checkbox" name="<!--{$key}-->" value="1" <!--{if $arrForm[$key] == 1}-->checked<!--{/if}--> />この選択肢を初期値とする
                </td>
            </tr>
        </table>
        <div class="btn-area">
            <ul>
                <li><a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('form1', 'edit', '', ''); return false;"><span class="btn-next">この内容で登録する</span></a></li>
            </ul>
        </div>

        <a name="list_table"></a>
        <table class="list">
            <col/>
            <col width="30%" />
            <col width="15%" />
            <col width="7%" />
            <col width="5%" />
            <col width="5%" />
            <col width="8%" />
            <tr>
                <th>選択肢名</th>
                <th>説明文</th>
                <th>画像</th>
                <th><!--{if $tpl_option_action == 1}-->金額<!--{elseif $tpl_option_action == 2 || $tpl_option_action == 4}-->ポイント<!--{elseif $tpl_option_action == 3}-->送料設定<!--{/if}--></th>
                <th>編集</th>
                <th>削除</th>
                <th>移動</th>
            </tr>
            <!--{section name=cnt loop=$arrOptionCat}-->
            <tr style="background:<!--{if $tpl_optioncategory_id != $arrOptionCat[cnt].optioncategory_id}-->#ffffff<!--{else}--><!--{$smarty.const.SELECT_RGB}--><!--{/if}-->;">
                <td><!--{$arrOptionCat[cnt].name|h}--><!--{if $arrOptionCat[cnt].default_flg==1}-->（未選択扱い）<!--{/if}--><!--{if $arrOptionCat[cnt].init_flg==1}-->（初期値）<!--{/if}--></td>
                <td><!--{$arrOptionCat[cnt].description|h}--></td>
                <td class="center"><!--{if $arrOptionCat[cnt].option_image}--><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrOptionCat[cnt].option_image|h}-->" alt="" height="50"/><!--{/if}--></td>
                <td><!--{if $tpl_option_action == 1 || $tpl_option_action == 2 || $tpl_option_action == 4}--><!--{$arrOptionCat[cnt].value|h|default:0}--><!--{if $tpl_option_action == 1}-->円<!--{elseif $tpl_option_action == 2 || $tpl_option_action == 4}-->Pt<!--{/if}--><!--{elseif $tpl_option_action == 3}--><!--{assign var=key value=`$arrOptionCat[cnt].value`}--><!--{$arrDELIVFREE[$key]}--><!--{else}-->設定なし<!--{/if}--></td>
                <td class="center">
                    <!--{if $tpl_optioncategory_id != $arrOptionCat[cnt].optioncategory_id}-->
                    <a href="?" onclick="fnModeSubmit('pre_edit', 'optioncategory_id', <!--{$arrOptionCat[cnt].optioncategory_id}--> ); return false;">編集</a>
                    <!--{else}-->
                    編集中
                    <!--{/if}-->
                </td>
                <td class="center">
                    <a href="?" onclick="fnModeSubmit('delete', 'optioncategory_id', <!--{$arrOptionCat[cnt].optioncategory_id}--> ); return false;">削除</a>
                </td>
                <td class="center">
                    <!--{if $smarty.section.cnt.iteration != 1}-->
                    <a href="?" onclick="fnModeSubmit('up', 'optioncategory_id', <!--{$arrOptionCat[cnt].optioncategory_id}--> ); fnChangeMode('edit'); return false;">上へ</a>
                    <!--{/if}-->
                    <!--{if $smarty.section.cnt.iteration != $smarty.section.cnt.last}-->
                    <a href="?" onclick="fnModeSubmit('down', 'optioncategory_id', <!--{$arrOptionCat[cnt].optioncategory_id}--> ); fnChangeMode('edit'); return false;">下へ</a>
                    <!--{/if}-->
                </td>
            </tr>
            <!--{/section}-->
        </table>
        <div class="btn">
            <a class="btn-action" href="./options.php"><span class="btn-prev">オプション一覧に戻る</span></a>
        </div>
    </div>
</form>
