<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<script type="text/javascript">
<!-- 
    function fnOptionCatPage(option_id) {
        location.href = "./optioncategory.php?option_id=" + option_id;
    }
//-->
</script>

<form name="form1" id="form1" method="post" action="?">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
    <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->" />
    <!--{foreach key=key item=item from=$arrSearchHidden}-->
    <!--{if is_array($item)}-->
    <!--{foreach item=c_item from=$item}-->
    <input type="hidden" name="<!--{$key|h}-->[]" value="<!--{$c_item|h}-->" />
    <!--{/foreach}-->
    <!--{else}-->
    <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->" />
    <!--{/if}-->
    <!--{/foreach}-->    
    <input type="hidden" name="mode" value="edit" />
    <div id="products" class="contents-main">
        <table>
            <tr>
                <th>商品名</th>
                <td><!--{$tpl_product_name|h}--></td>
            </tr>
        </table>


        <table class="list">
            <col width="6%" />
            <col width="22%" />
            <col width="22%" />
            <col width="20%" />
            <col width="20%" />
            <col width="10%" />
            <tr>
                <th><input type="checkbox" onclick="fnAllCheck(this, 'input[name^=check]')" id="allCheck" /> <label for="allCheck"><br>登録</label></th>        
                <th>オプション管理名</th>
                <th>オプション表示タイトル</th>
                <th>タイプ</th>
                <th>アクション</th>
                <th>説明画面</th>
            </tr>
            <!--{section name=cnt loop=$arrOption}-->
            <!--{assign var=index value=$smarty.section.cnt.index}-->
            <tr style="background:<!--{if $tpl_option_id != $arrOption[cnt].option_id}-->#ffffff<!--{else}--><!--{$smarty.const.SELECT_RGB}--><!--{/if}-->;">
                <!--{assign var=option_id value=$arrOption[cnt].option_id}-->
                <td class="center">
                    <!--{assign var=key value="check"}-->
                    <!--{if $arrErr[$key][$option_id]}-->
                    <span class="attention"><!--{$arrErr[$key][$option_id]}--></span>
                    <!--{/if}-->
                    <input type="checkbox" name="<!--{$key}-->[<!--{$option_id}-->]" value="1" <!--{if $arrForm[$key][$option_id] == 1}-->checked="checked"<!--{/if}--> id="<!--{$key}-->_<!--{$index}-->" />
                </td>                
                <td><!--{$arrOption[cnt].manage_name|h}--></td>
                <td><!--{$arrOption[cnt].name|h}--></td>
                <td><!--{assign var=type value=`$arrOption[cnt].type`}--><!--{$arrTYPE[$type]|h}--></td>
                <td><!--{assign var=action value=`$arrOption[cnt].action`}--><!--{$arrACTION[$action]|h}--></td>
                <td class="center"><!--{if $arrOption[cnt].description_flg > 0}-->○<!--{/if}--></td>
            </tr>
            <!--{/section}-->
        </table>

        <div class="btn-area">
            <ul>
                <li><a class="btn-action" href="javascript:;" onclick="fnChangeAction('<!--{$smarty.const.ADMIN_PRODUCTS_URLPATH}-->');
                        fnModeSubmit('search', '', '');
                        return false;"><span class="btn-prev">検索画面に戻る</span></a></li>        
                <li><a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('form1', 'edit', '', '');
                        return false;"><span class="btn-next">登録</span></a></li>
            </ul>
        </div>    

    </div>
</form>
