<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<script type="text/javascript">
<!-- 
    function fnOptionCatPage(option_id) {
    location.href = "./optioncategory.php?option_id=" + option_id;
    }

    function onChangeType(){
    val = $("select[name='type']").val();
    if (val == 3 || val == 4){
    $("select[name='action']").attr("disabled", "disabled");
//		$("select[name='description_flg']").attr("disabled","disabled");
    $("select[name='pricedisp_flg']").attr("disabled", "disabled");
    } else{
    $("select[name='action']").removeAttr("disabled");
//		$("select[name='description_flg']").removeAttr("disabled");
    onChangeAction();
    }
    }

    function onChangeAction(){
    val = $("select[name='action']").val();
    if (val == 0){
    $("select[name='pricedisp_flg']").attr("disabled", "disabled");
    } else{
    $("select[name='pricedisp_flg']").removeAttr("disabled");
    }
    }

    function fnChangeMode(mode) {
    document.form1['mode'].value = mode;
    }
//-->
</script>

<script type="text/javascript">//<![CDATA[
    $(function() {
    $('#copy_from_manage').click(function() {
    var val = $('#manage_name').val();
    $('input[id=name]').val(val);
    });
    });
//]]></script>	

<form name="form1" id="form1" method="post" action="?">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="option_id" value="<!--{$tpl_option_id|h}-->" />
    <div id="products" class="contents-main">
        <div style="float:right; margin-bottom:5px;"><a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('form1', '', 'option_id', '0'); return false;">新規登録画面</a></div>
        <table style="float:left;">
            <tr>
                <th>オプション管理名<span class="attention"> *</span></th>
                <td colspan="3">
                    <!--{if $arrErr.manage_name}-->
                    <span class="attention"><!--{$arrErr.manage_name}--></span>
                    <!--{/if}-->
                    <input type="text" name="manage_name" value="<!--{$arrForm.manage_name|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="" size="30" class="box30" id="manage_name"/>
                    <span class="attention"> (上限<!--{$smarty.const.STEXT_LEN}-->文字)</span>
                </td>
            </tr>
            <tr>
                <th>オプション表示タイトル<span class="attention"> *</span></th>
                <td colspan="3">
                    <!--{if $arrErr.name}-->
                    <span class="attention"><!--{$arrErr.name}--></span>
                    <!--{/if}-->
                    <input type="text" name="name" value="<!--{$arrForm.name|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="" size="30" class="box30" id="name"/>
                    <span class="attention"> (上限<!--{$smarty.const.STEXT_LEN}-->文字)</span>
                    <a class="btn-normal" href="javascript:;" id="copy_from_manage"><span>管理名をコピー</span></a>
                </td>
            </tr>
            <tr>
                <th>説明文</th>
                <td colspan="3">
                    <!--{if $arrErr.description}-->
                    <span class="attention"><!--{$arrErr.description}--></span>
                    <!--{/if}-->
                    <textarea name="description" maxlength="<!--{$smarty.const.LTEXT_LEN}-->" style="<!--{if $arrErr.description != ""}-->background-color: <!--{$smarty.const.ERR_COLOR}-->;<!--{/if}-->" cols="60" rows="8" class="area60"><!--{$arrForm.description|h}--></textarea><br />
                    <span class="attention"> (上限<!--{$smarty.const.LTEXT_LEN}-->文字)</span>
                </td>
            </tr>
            <tr>
                <th>タイプ</th>
                <td>
                    <!--{if $arrErr.type}-->
                    <span class="attention"><!--{$arrErr.type}--></span>
                    <!--{/if}-->
                    <!--{html_options name="type" options=$arrTYPE selected=$arrForm.type|h onChange="onChangeType();"}-->
                </td>
                <th>アクション</th>
                <td>
                    <!--{if $arrErr.action}-->
                    <span class="attention"><!--{$arrErr.action}--></span>
                    <!--{/if}-->
                    <select name="action" onChange="onChangeAction();" <!--{if $arrForm.type == 3}-->disabled="disabled"<!--{/if}-->>
                            <!--{html_options options=$arrACTION selected=$arrForm.action|h}-->
                </select>
            </td>
        </tr>
        <tr>
            <th>オプション説明</th>            
            <td>
                <!--{if $arrErr.description_flg}-->
                <span class="attention"><!--{$arrErr.description_flg}--></span>
                <!--{/if}-->
                <select name="description_flg">
                    <!--{html_options options=$arrDESC_FLG selected=$arrForm.description_flg|default:0}-->
                </select>
            </td>
            <th>価格・ポイント等の表示</th>            
            <td>
                <!--{if $arrErr.pricedisp_flg}-->
                <span class="attention"><!--{$arrErr.pricedisp_flg}--></span>
                <!--{/if}-->
                <select name="pricedisp_flg" <!--{if $arrForm.action == 0 || $arrForm.type == 3}-->disabled="disabled"<!--{/if}-->>
                        <!--{html_options options=$arrDispFLG selected=$arrForm.pricedisp_flg|default:0}-->
            </select>
        </td>
    </tr>
    <tr>
        <th>必須チェック</th>
        <td colspan="3"><input type="checkbox" name="is_required" value="1" <!--{if $arrForm.is_required == 1}-->checked<!--{/if}-->>必須項目にする</td>
    </tr>
</table>
<div class="btn-area">
    <ul>
        <li><a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('form1', 'edit', '', ''); return false;"><span class="btn-next">この内容で登録する</span></a></li>
    </ul>
</div>

<a name="list_table"></a>
<table class="list">
    <col width="5%"/>
    <col width="11%"/>
    <col width="11%"/>
    <col width="10%" />
    <col width="10%" />
    <col width="10%" />
    <col width="4%" />
    <col width="10%" />
    <col width="6%" />    
    <col width="6%" />
    <col width="15%" />
    <tr>
        <th>オプションID</th>
        <th>オプション管理名 (登録数)</th>
        <th>オプション表示タイトル</th>
        <th>タイプ</th>
        <th>アクション</th>
        <th>説明表示</th>
        <th>必須</th>
        <th>選択肢登録</th>
        <th class="edit">編集</th>
        <th class="delete">削除</th>
        <th>移動</th>
    </tr>
    <!--{section name=cnt loop=$arrOption}-->
    <tr style="background:<!--{if $tpl_option_id != $arrOption[cnt].option_id}-->#ffffff<!--{else}--><!--{$smarty.const.SELECT_RGB}--><!--{/if}-->;">
        <!--{assign var=option_id value=$arrOption[cnt].option_id}-->
        <td class="center"><!--{$option_id|h}--></td>
        <td><!--{$arrOption[cnt].manage_name|h}--> (<!--{$arrOptionCatCount[$option_id]|default:0}-->)</td>
        <td><!--{$arrOption[cnt].name|h}--></td>
        <td class="center"><!--{assign var=type value=`$arrOption[cnt].type`}--><!--{$arrTYPE[$type]|h}--></td>
        <td class="center"><!--{assign var=action value=`$arrOption[cnt].action`}--><!--{$arrACTION[$action]|h}--></td>
        <td class="center"><!--{assign var=flg value=`$arrOption[cnt].description_flg`}--><!--{$arrDESC_FLG[$flg]|h}--></td>
        <td class="center"><!--{if $arrOption[cnt].is_required == 1}-->○<!--{/if}--></td>
        <td class="center"><!--{if $type != 3 && $type != 4}--><a href="<!--{$smarty.const.ROOT_URLPATH}-->" onclick="fnOptionCatPage( <!--{$arrOption[cnt].option_id}--> ); return false;">選択肢登録</a><!--{/if}--></td>
        <td class="center">
            <!--{if $tpl_option_id != $arrOption[cnt].option_id}-->
            <a href="?" onclick="fnModeSubmit('pre_edit', 'option_id', <!--{$arrOption[cnt].option_id}--> ); return false;">編集</a>
            <!--{else}-->
            編集中
            <!--{/if}-->
        </td>
        <td class="center">
            <!--{if $arrOptionCatCount[$option_id] > 0}-->
            -
            <!--{else}-->
            <a href="?" onclick="fnModeSubmit('delete', 'option_id', <!--{$arrOption[cnt].option_id}--> ); return false;">削除</a>
            <!--{/if}-->
        </td>
        <td class="center">
            <!--{if $smarty.section.cnt.iteration != 1}-->
            <a href="?" onclick="fnModeSubmit('up', 'option_id', <!--{$arrOption[cnt].option_id}--> ); fnChangeMode('edit'); return false;">上へ</a>
            <!--{/if}-->
            <!--{if $smarty.section.cnt.iteration != $smarty.section.cnt.last}-->
            <a href="?" onclick="fnModeSubmit('down', 'option_id', <!--{$arrOption[cnt].option_id}--> ); fnChangeMode('edit'); return false;">下へ</a>
            <!--{/if}-->
        </td>
    </tr>
    <!--{/section}-->
</table>

</div>
</form>
