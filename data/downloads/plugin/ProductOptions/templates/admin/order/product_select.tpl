<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrProductOptions[$id]|@count > 0}-->
<!--{foreach from=$arrProductOptions[$id] item=option}-->
<!--{assign var=type value=`$option.type`}-->
<br>
<!--{$option.name}-->:<br>
<!--{if $type == 1 || $type == 2}-->
<!--{html_options name="plg_productoptions[`$option.option_id`]" options=$option.options id="plg_productoptions`$id`_`$option.option_id`"}-->
<!--{elseif $type == 3}-->
<input type="text" name="plg_productoptions[<!--{$option.option_id}-->]" value="" class="box35" id="plg_productoptions<!--{$id}-->_<!--{$option.option_id}-->">
<!--{elseif $type == 4}-->
<textarea name="plg_productoptions[<!--{$option.option_id}-->]" id="plg_productoptions<!--{$id}-->_<!--{$option.option_id}-->" cols="40" rows="5"></textarea>
<!--{/if}-->
<!--{/foreach}-->
<!--{/if}-->