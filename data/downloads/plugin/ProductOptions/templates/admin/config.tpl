<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_header.tpl"}-->
<script type="text/javascript">
</script>

<h2><!--{$tpl_subtitle}--></h2>
<form name="form1" id="form1" method="post" action="<!--{$smarty.server.REQUEST_URI|h}-->">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
    <input type="hidden" name="mode" value="edit">

    <table border="0" cellspacing="1" cellpadding="8" summary=" ">
        <col width="30%" />
        <col width="70%" />
        <tr >
            <th>カート動作モード設定<span class="attention">※</span></th>
            <td>
                <span class="attention"><!--{$arrErr.update_flg}--></span><!--{html_radios options=$arrSelect name="update_flg" selected=$arrForm.update_flg|default:0 separator="<br>"}--><br>

                <div style="font-size:10px;">既にカートに入っている商品を追加でカートに入れた際の挙動を設定します。<br><br>
                    <span style="color:#00F;">【上書きモード】</span><br>
                    後から追加したオプション内容で上書きします。（ver.1と同様の動作になります）<br>
                    <span style="color:#00F;">【区別モード】</span><br>
                    「規格」と同様の動作になります。選択したオプションの内容が１つでも異なっていればカート内で別商品として扱われます。</div>

            </td>
        </tr>
        <tr >
            <th>一覧ページカートイン機能設定</th>
            <td>
                <span class="attention"><!--{$arrErr.list_cartin_flg}--></span><!--{html_radios options=$arrSelect2 name="list_cartin_flg" selected=$arrForm.list_cartin_flg|default:0 separator="<br>"}--><br>

                <div style="font-size:10px;">商品一覧画面でカートインの機能を使用されない場合は「OFF」に変更されてください。一覧ページの動作を軽くする事ができます。<br />

            </td>
        </tr>
    </table>

    <div class="btn-area">
        <ul>
            <li>
                <a class="btn-action" href="javascript:;" onclick="document.form1.submit();
                        return false;"><span class="btn-next">この内容で登録する</span></a>
            </li>
        </ul>
    </div>

</form>
<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_footer.tpl"}-->
