<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{strip}-->
<!--{assign var=option value=`$arrProductOptions[$tpl_option_id]`}-->
<!--{assign var=option_name value=plg_productoptions[`$tpl_option_id`]}-->
<!--{assign var=type value=`$option.type`}-->
<div align="center"><!--{$option.name}--></div>
<hr>

<!--{if $tpl_classcat_find1}-->
<p><!--{$tpl_class_name1}-->は「<!--{$arrClassCat1[$arrForm.classcategory_id1.value]|h}-->」を選択しています。</p>
<!--{if $tpl_classcat_find2}-->
<p><!--{$tpl_class_name2}-->は「<!--{$arrClassCat2[$arrForm.classcategory_id2.value]|h}-->」を選択しています。</p>
<!--{/if}-->
<!--{/if}-->

<!--{foreach from=$arrProductOptions key=key item=item}-->
<!--{if strlen($arrHiddenOptions[$key]) > 0}-->
<!--{assign var=value value=`$arrHiddenOptions[$key]`}-->
<!--{if $item.type == 3 || $item.type == 4}-->
<p><!--{$item.name}-->は「<!--{$value}-->」を入力しています。</p>
<!--{else}-->
<p><!--{$item.name}-->は「<!--{$item.options[$value]}-->」を選択しています。</p>
<!--{/if}-->
<!--{/if}-->
<!--{/foreach}-->

<!--{if $plg_productoptions_Err[$tpl_option_id] != ""}-->
<font color="#FF0000"><!--{$plg_productoptions_Err[$tpl_option_id]}--></font><br>
<!--{/if}-->
<form method="post" action="?">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->">
    <input type="hidden" name="mode" value="selectItem">
    <input type="hidden" name="classcategory_id1" value="<!--{$arrForm.classcategory_id1.value}-->">
    <input type="hidden" name="classcategory_id2" value="<!--{$arrForm.classcategory_id2.value}-->">
    <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->">
    <input type="hidden" name="product_class_id" value="<!--{$tpl_product_class_id}-->">
    <input type="hidden" name="product_type" value="<!--{$tpl_product_type}-->">
    <input type="hidden" name="option_id" value="<!--{$tpl_option_id}-->">
    <!--{foreach from=$arrHiddenOptions key=option_id item=value}-->
    <input type="hidden" name="plg_productoptions[<!--{$option_id}-->]" value="<!--{$value}-->">        
    <!--{/foreach}-->
    <!--{if $type == 1}-->
    <!--{html_options name=$option_name options=$option.options selected=$arrForm[$option_name]|default:$option.option_init}-->
    <!--{if $option.description_flg == 2 || $option.description_flg == 3}-->
    <!--{foreach from=$option.optioncategory item=optioncategory}-->
    <br />
    <!--{if $optioncategory.default_flg != 1}-->
    <!--{if strlen($optioncategory.option_image) > 0}-->
    <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$optioncategory.option_image}-->" alt="<!--{$optioncategory.name|h}-->" width="100" /><br />
    <!--{/if}-->
    <!--{$optioncategory.name|h}--><br />
    <br />
    <!--{/if}-->
    <!--{/foreach}-->
    <!--{/if}-->
    <!--{elseif $type == 2}-->
    <!--{if $option.description_flg == 2 || $option.description_flg == 3}-->
    <!--{foreach from=$option.optioncategory item=optioncategory}-->
    <!--{assign var=selected_value value=`$arrForm.plg_productoptions.value[$option_id]`}-->
    <!--{if $selected_value == ""}-->
    <!--{assign var=selected_value value=`$option.option_init`}-->
    <!--{/if}-->
    <label><input type="radio" name="<!--{$option_name|h}-->" value="<!--{$optioncategory.optioncategory_id|h}-->" onChange="setOptionPriceOnChangeRadio(this, <!--{$option_id}--> , <!--{$action}--> );" <!--{if $selected_value == $optioncategory.optioncategory_id}-->checked<!--{/if}-->/><!--{$optioncategory.name|h}--><!--{if $option.pricedisp_flg == 1}-->(
                  <!--{$optioncategory.value|number_format|h}--><!--{if $option.action==1}-->円<!--{elseif $option.action==2}-->Pt<!--{/if}-->)<!--{/if}--></label>
    <!--{if strlen($optioncategory.option_image) > 0}-->
    <br />
    <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$optioncategory.option_image}-->" alt="<!--{$optioncategory.name|h}-->" width="100" />
    <!--{/if}-->
    <br /><br />
    <!--{/foreach}-->
    <!--{else}-->
    <!--{html_radios name=$option_name options=$option.options selected=$arrForm[$option_name]|default:$option.option_init}-->
    <!--{/if}-->
    <!--{elseif $type == 3}-->
    <input type="text" name="<!--{$option_name}-->" value="<!--{$arrForm[$option_name]|h}-->" maxlength="<!--{$smarty.const.MTEXT_LEN}-->">
    <!--{elseif $type == 4}-->
    <textarea name="<!--{$option_name}-->" maxlength="<!--{$smarty.const.LLTEXT_LEN}-->" cols="20" rows="2"><!--{$arrForm[$option_name]|h}--></textarea>
    <!--{/if}-->
    <br>
    <!--{if $option.description_flg == 1 || $option.description_flg == 3}-->
    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/product_options.php?option_id=<!--{$tpl_option_id}-->" target="_blank">内容説明</a><br>
    <!--{/if}-->

    <center><input type="submit" name="submit" value="次へ"></center>
</form>
<!--{/strip}-->
