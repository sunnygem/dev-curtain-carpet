<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{foreach from=$arrHiddenOptions key=option_id item=value}-->
<input type="hidden" name="plg_productoptions[<!--{$option_id}-->]" value="<!--{$value|h}-->">        
<!--{/foreach}-->