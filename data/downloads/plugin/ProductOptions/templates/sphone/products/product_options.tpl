<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_header.tpl" subtitle="$tpl_title"}-->

<style type="text/css">
    <!--
    h3 {
        margin-left:10px;
        margin-top: 5px;
        margin-bottom: 5px;
    }

    p {
        margin-left:10px;
        margin-top: 5px;
        margin-bottom: 5px;	
    }

    p.description {
        background-color:#F3F3F3;
        padding:5px 5px 5px 5px;
    }
    -->
</style>


<section id="windowcolumn">
    <h2 class="title"><!--{$tpl_option_name|h}--></h2>
    <p><!--{$tpl_option_description|nl2br}--></p>
    <!--{foreach from=$arrOptionsCatList item=item}-->
    <!--{if $item.default_flg != 1}-->
    <!--{if $item.option_image|strlen > 0}-->
    <p style="text-align:center;"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.option_image}-->" alt="<!--{$item.name|h}-->" /></p>
    <!--{/if}-->
    <h3><!--{$item.name|h}--></h3>
    <!--{if $tpl_option_action == 1}--><!--{if $item.value > 0}--><p style="color:#FF0000;">金額：<!--{$item.value|sfCalcIncTax|number_format}--> 円</p><!--{elseif $item.value < 0}--><p style="color:#0000FF;">金額：<!--{$item.value|sfCalcIncTax|number_format}--> 円</p><!--{/if}-->
    <!--{elseif $tpl_option_action == 2}--><!--{if $item.value > 0}--><p style="color:#FF0000;"><!--{$item.value|number_format}--> ポイントプレゼント</p><!--{/if}-->
    <!--{elseif $tpl_option_action == 3}--><p style="color:#FF0000;"><!--{if $item.value == 1}-->送料無料になります<!--{else}-->送料無料にはなりません<!--{/if}--></p>
    <!--{/if}-->
    <!--{if $item.description|strlen > 0}-->
    <p class="description"><!--{$item.description|nl2br}--></p>
    <!--{/if}-->
    <hr>
    <!--{/if}-->
    <!--{/foreach}-->
</section>

<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_footer.tpl"}-->