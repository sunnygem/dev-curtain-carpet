<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $item.plg_productoptions|@count >=0}-->
<!--{foreach from=$item.plg_productoptions_detail item=option}-->
<li><!--{$option.option_name|h}-->:<!--{$option.optioncategory_name|h|nl2br}--></li>
<!--{/foreach}-->
<!--{/if}-->