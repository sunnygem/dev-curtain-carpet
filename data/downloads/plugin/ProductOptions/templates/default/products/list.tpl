<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<!--{if $arrProduct.productoptions|@count > 0}-->

<div class="classlist">
    <!--{assign var=plgErr value=$plg_productoptions_Err[$id]}-->
    <!--{assign var=plgForm value=$plg_productoptions_Form[$id]}-->
    <!--{foreach from=$arrProduct.productoptions item=option key=option_id}-->
    <!--{assign var=option_name value="plg_productoptions[`$option_id`]"}-->
    <!--{assign var=option_tag_id value=plg_productoptions_`$option_id`}-->
    <!--{assign var=type value=`$option.type`}-->
    <!--{assign var=action value=`$option.action`}-->
    <dl class="clearfix">
        <dt><!--{$option.name|h}-->:a</dt>
        <dd>
            <!--{if $type == 1}-->
            <select name="<!--{$option_name}-->" id="<!--{$option_tag_id}-->" onChange="setOptionPriceOnChange( <!--{$id}--> , this, <!--{$option_id}--> , <!--{$action}--> );" style="<!--{$plgErr[$option_id]|sfGetErrorColor}-->">
                <!--{html_options options=$option.options selected=$plgForm[$option_id]|default:$option.option_init}-->
            </select>
            <!--{elseif $type == 2}-->
            <!--{html_radios name=$option_name id=$option_tag_id options=$option.options selected=$plgForm[$option_id]|default:$option.option_init onChange="setOptionPriceOnChangeRadio($id,this,$option_id,$action);"}-->
            <!--{elseif $type == 3}-->
				<!--{if $option.name == "横幅"}-->
              <dl>
                <dt>横幅</dt>
                <dd>
                  <p>レールの長さ（ｃｍ）を入力してください。<br>
                    <small>（フック穴を基準にしてください。）</small></p>
                  <div class="railBox01"> <img src="<!--{$TPL_URLPATH}-->img/icon/rail01.png" alt="レール"><br>
                    <img src="<!--{$TPL_URLPATH}-->img/icon/arrow04.png" alt="長さ"><br>
            <input type="text" name="<!--{$option_name}-->" value="<!--{$plgForm[$option_id]|h}-->" class="box300" style="<!--{$plgErr[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->">
                    <label>cm</label>
                    <div class="sizeBox_bg01">
                      <p>仕上がりサイズ&nbsp;&nbsp;<span></span>cm</p>
                      <aside>生地のゆとり分を1.05倍した<br>
                        仕上がりサイズを自動で計算します。</aside>
                    </div>
                    
                    <dl class="checkBox01">
                    	<dt>
                      <label>
                    	<input type="checkbox" name="checkbox01[]" class="checkNone">
                     <span class="check01"></span> </label>
                      </dt>
                      <dd>横幅はフック穴を基準に測りました。<br><small>間違いない場合はチェックをつけてください。</small><br>（必須項目）</dd>
                   	</dl>
                  </div>
                </dd>
              </dl>
				<!--{else}-->
            <input type="text" name="<!--{$option_name}-->" value="<!--{$plgForm[$option_id]|h}-->" class="box300" style="<!--{$plgErr[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->">
				<!--{/if}-->
            <!--{elseif $type == 4}-->
            <textarea name="<!--{$option_name}-->" cols="50" rows="8" class="txtarea" wrap="hard" style="<!--{$plgErr[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->"><!--{$plgForm[$option_id]|h}--></textarea>
            <!--{/if}-->
            <!--{if $plgErr[$option_id] != ""}-->
            <p class="attention"><!--{$plgErr[$option_id]}--></p>
            <!--{/if}-->
        </dd>
        <!--{if $option.description_flg > 0}-->
　<a class="btn-action" href="javascript:;" onclick="win03('<!--{$smarty.const.ROOT_URLPATH}-->products/product_options.php?option_id=<!--{$option_id}-->', 'option_win', 650, 500); return false;">内容説明</a>
        <!--{/if}-->
    </dl>
    <!--{if $type != 3 && $type != 4}-->
    <script type="text/javascript">//<![CDATA[
        setOptionPrice( <!--{$id}--> , <!--{$plgForm[$option_id]|default:$option.option_init|h}--> , <!--{$option_id|h}--> , <!--{$action|h}--> );
//]]></script>
    <!--{/if}-->
    <!--{/foreach}-->
</div>
<!--{/if}-->
