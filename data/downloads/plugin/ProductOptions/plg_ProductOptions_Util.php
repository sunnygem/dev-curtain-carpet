<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_SC_DB.php";

/**
 * 共通関数
 *
 * @package ProductOptions
 * @author Bratech CO.,LTD.
 * @version $Id: $
 */
class plg_ProductOptions_Util
{

    function getECCUBEVer()
    {
        return floor(str_replace('.', '', ECCUBE_VERSION));
    }

    function getActionList()
    {
        $arrACTION[0] = "設定なし";
        $arrACTION[1] = "金額加/減算";
        $arrACTION[2] = "ポイントプレゼント";
        $arrACTION[3] = "送料無料";
        return $arrACTION;
    }

    function getTypeList()
    {
        $arrTYPE[1] = "セレクトメニュー";
        $arrTYPE[2] = "ラジオボタン";
        $arrTYPE[3] = "テキストボックス";
        $arrTYPE[4] = "テキストエリア";
        return $arrTYPE;
    }

    function getDelivFreeSel()
    {
        $arrDelivFree[0] = "無料にしない";
        $arrDelivFree[1] = "無料にする";
        return $arrDelivFree;
    }

    function getDescriptionFlg()
    {
        $arrDescriptionFlg[0] = "表示しない";
        $arrDescriptionFlg[1] = "別ウィンドウで表示する";
        $arrDescriptionFlg[2] = "詳細画面上で表示する";
        $arrDescriptionFlg[3] = "両方に表示する";
        return $arrDescriptionFlg;
    }

    function getDispFlg()
    {
        $arrDescriptionFlg[0] = "表示しない";
        $arrDescriptionFlg[1] = "表示する";
        return $arrDescriptionFlg;
    }

    /**
     * オプション表示タイトルの取得
     *
     * @param integer $option_id オプションID
     * @return string オプション表示タイトル
     */
    function lfGetOptionName($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        $ret = $objDB->lfGetOptionInfo($option_id);
        return $ret['name'];
    }

    /**
     * オプション管理名の取得
     *
     * @param integer $option_id オプションID
     * @return string オプション管理名
     */
    function lfGetOptionManageName($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        $ret = $objDB->lfGetOptionInfo($option_id);
        return $ret['manage_name'];
    }

    /**
     * オプション説明文の取得
     *
     * @param integer $option_id オプションID
     * @return string オプション説明文
     */
    function lfGetOptionDescription($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        $ret = $objDB->lfGetOptionInfo($option_id);
        return $ret['description'];
    }

    /**
     * オプションタイプを取得する
     *
     * @param integer $option_id オプションID
     * @return int タイプ
     */
    function lfGetOptionType($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        $ret = $objDB->lfGetOptionInfo($option_id);
        return $ret['type'];
    }

    /**
     * オプションアクションを取得する
     *
     * @param integer $option_id オプションID
     * @return int アクション
     */
    function lfGetOptionAction($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        $ret = $objDB->lfGetOptionInfo($option_id);
        return $ret['action'];
    }

    /**
     * オプション説明フラグを取得する
     *
     * @param integer $option_id オプションID
     * @return int 説明フラグ
     */
    function lfGetOptionDescriptionFlg($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        $ret = $objDB->lfGetOptionInfo($option_id);
        return $ret['description_flg'];
    }

    /**
     * 価格の表示・非表示フラグを取得する
     *
     * @param integer $option_id オプションID
     * @return int 表示フラグ
     */
    function lfGetOptionPricedispFlg($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        $ret = $objDB->lfGetOptionInfo($option_id);
        return $ret['pricedisp_flg'];
    }

    /**
     * 必須チェックフラグを取得する
     *
     * @param integer $option_id オプションID
     * @return int 必須フラグ
     */
    function lfGetOptionIsRequired($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        $ret = $objDB->lfGetOptionInfo($option_id);
        return $ret['is_required'];
    }

    /**
     * 有効なオプション選択肢情報の取得
     *
     * @param integer $option_id オプションID
     * @return array オプション選択肢情報リスト
     */
    function lfGetOptionCatList($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();
        return $objDB->lfGetOptionCatList($option_id);
    }

    /**
     * 未選択肢IDを取得
     *
     * @param integer $option_id オプションID
     * @return array 未選択肢ID
     */
    function lfGetOptionDefaultCatId($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();
        return $objDB->lfGetOptionDefaultCatId($option_id);
    }

    /**
     * 初期値選択肢IDを取得
     *
     * @param integer $option_id オプションID
     * @return array 初期値選択肢ID
     */
    function lfGetOptionInitCatId($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();
        return $objDB->lfGetOptionInitCatId($option_id);
    }

    /**
     * 有効なオプション情報の取得
     *
     * @return array オプション情報
     */
    function lfGetOption($flg = false)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();
        return $objDB->lfGetOption($flg);
    }

    function lfGetOptionInfo($option_id)
    {
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();
        return $objDB->lfGetOptionInfo($option_id);
    }

    /**
     * オプション選択肢名を取得する
     *
     * @param integer $optioncategory_id
     * @return string オプション選択肢名
     */
    function lfGetOptionCatName($optioncategory_id)
    {
        $ret = plg_ProductOptions_Util::lfGetOptionCat($optioncategory_id);
        return $ret['name'];
    }

    /**
     * オプション選択肢値を取得する
     *
     * @param integer $optioncategory_id
     * @return string オプション選択肢値
     */
    function lfGetOptionCatValue($optioncategory_id)
    {
        $ret = plg_ProductOptions_Util::lfGetOptionCat($optioncategory_id);
        return $ret['value'];
    }

    /**
     * オプション画像名を取得する
     *
     * @param integer $optioncategory_id
     * @return string オプション画像名
     */
    function lfGetOptionCatImage($optioncategory_id)
    {
        $ret = plg_ProductOptions_Util::lfGetOptionCat($optioncategory_id);
        return $ret['option_image'];
    }

    /**
     * オプション選択肢説明文を取得する
     *
     * @param integer $optioncategory_id
     * @return string 説明文
     */
    function lfGetOptionCatDescription($optioncategory_id)
    {
        $ret = plg_ProductOptions_Util::lfGetOptionCat($optioncategory_id);
        return $ret['description'];
    }

    /**
     * 選択肢情報を取得する
     *
     * @param integer $optioncategory_id
     * @return array 選択肢情報
     */
    function lfGetOptionCat($optioncategory_id)
    {
        if (is_array($optioncategory_id))
            return;
        if (!is_numeric($optioncategory_id))
            return;
        $objDB = & plg_ProductOptions_SC_DB::getSingletonInstance();

        return $objDB->lfGetOptionCategoryInfo($optioncategory_id);
    }

    /**
     * 商品に設定されているオプション情報の取得
     *
     * @param integer $product_id 商品ID
     * @return array オプションID
     */
    function lfGetProductOptions($product_id)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $strCheck = $objQuery->get("plg_productoptions_flg", "dtb_products", "product_id = ?", array($product_id));
        $arrCheck = explode(',', $strCheck);
        foreach ($arrCheck as $option_id) {
            $arrRet[$option_id] = 1;
        }
        return $arrRet;
    }

    function lfSetProductOptions($product_id)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $strCheck = $objQuery->get("plg_productoptions_flg", "dtb_products", "product_id = ?", array($product_id));
        $point_rate = $objQuery->get("point_rate", "dtb_products_class", "product_id = ?", array($product_id));
        if ($strCheck != "") {
            $arrCheck = explode(',', $strCheck);

            $arrRet = array();
            $values = array();
            foreach ($arrCheck as $option_id) {
                $option = plg_ProductOptions_Util::lfGetOptionInfo($option_id);
                if (empty($option))
                    continue;
                $arrRet[$option_id]['name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                $arrRet[$option_id]['action'] = $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                $arrRet[$option_id]['type'] = plg_ProductOptions_Util::lfGetOptionType($option_id);
                $arrRet[$option_id]['description_flg'] = plg_ProductOptions_Util::lfGetOptionDescriptionFlg($option_id);
                $arrRet[$option_id]['is_required'] = plg_ProductOptions_Util::lfGetOptionIsRequired($option_id);
                $arrRet[$option_id]['option_id'] = $option_id;

                $arrOptions = plg_ProductOptions_Util::lfGetOptionCatList($option_id);
                $options = array();

                $arrRet[$option_id]['pricedisp_flg'] = $pricedisp_flg = plg_ProductOptions_Util::lfGetOptionPricedispFlg($option_id);

                foreach ($arrOptions as $item) {
                    $values[$option_id][$item['optioncategory_id']]['price'] = $item['value'];
                    if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                        $values[$option_id][$item['optioncategory_id']]['price_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax($item['value'], $product_id, 0);
                    } else {
                        $values[$option_id][$item['optioncategory_id']]['price_inctax'] = SC_Helper_DB_Ex::sfCalcIncTax($item['value']);
                    }
                    $values[$option_id][$item['optioncategory_id']]['point'] = SC_Utils_Ex::sfPrePoint($item['value'], $point_rate);

                    $name = $item['name'];
                    if ($pricedisp_flg == 1 && $item['value'] != 0) {
                        if ($option_action == 1) {
                            $name .= "  (" . number_format($item['value']) . "円 )";
                        } elseif ($option_action == 2) {
                            $name .= "  (" . number_format($item['value']) . "Pt )";
                        } elseif ($option_action == 3 && $item['value'] == 1) {
                            $name .= "  (送料無料)";
                        }
                    }
                    $options[$item['optioncategory_id']] = $name;
                }
                $arrRet[$option_id]['options'] = $options;
                $arrRet[$option_id]['optioncategory'] = plg_ProductOptions_Util::lfGetOptionCatList($option_id);
                $default_id = plg_ProductOptions_Util::lfGetOptionDefaultCatId($option_id);
                $init_id = plg_ProductOptions_Util::lfGetOptionInitCatId($option_id);
                $arrRet[$option_id]['default_id'] = $default_id;
                if ($init_id > 0) {
                    $arrRet[$option_id]['option_init'] = $init_id;
                } else {
                    $arrRet[$option_id]['option_init'] = key($options);
                }
            }
        }
        return array($arrRet, $values, $arrCheck);
    }

    function lfSetCartProductOptions(&$arrCartItems, $pref_id = 0, $country_id = 0)
    {
        $total_inctax = 0;
        $total_point = 0;
        $total_tax = 0;
        $deliv_free_cnt = 0;
        foreach ($arrCartItems as $i => $cart_item) {
            $arrCartItems[$i]['add_price'] = 0;
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                $arrCartItems[$i]['price_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax($arrCartItems[$i]['price'], $arrCartItems[$i]['productsClass']['product_id'], $arrCartItems[$i]['id'][0]);
            } else {
                $arrCartItems[$i]['price_inctax'] = SC_Helper_DB_Ex::sfCalcIncTax($arrCartItems[$i]['price']);
            }

            if (is_array($cart_item['plg_productoptions'])) {
                foreach ($cart_item['plg_productoptions'] as $option_id => $optioncategory_id) {
                    $option = plg_ProductOptions_Util::lfGetOptionInfo($option_id);

                    if (is_null($optioncategory_id) || $optioncategory_id == "")
                        continue 2;
                    if ($option['option_id'] != $option_id)
                        continue;
                    $value = "";
                    $option_action = "";
                    if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['optioncategory_id'] = $optioncategory_id;
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['optioncategory_name'] = $optioncategory_id;
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['optioncategory_value'] = $value = $optioncategory_id;
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['option_action'] = $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                    } else {
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['optioncategory_id'] = $optioncategory_id;
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['optioncategory_name'] = plg_ProductOptions_Util::lfGetOptionCatName($optioncategory_id);
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['optioncategory_value'] = $value = plg_ProductOptions_Util::lfGetOptionCatValue($optioncategory_id);
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['option_action'] = $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                        $arrCartItems[$i]['plg_productoptions_detail'][$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                        if ($option_action == 1) {
                            $arrCartItems[$i]['add_price'] += $value;
                        } elseif ($option_action == 2) {
                            $total_point += $value;
                        } elseif ($option_action == 3 && $value == 1) {
                            $deliv_free_cnt++;
                        }
                    }
                }
            }

            $quantity = $arrCartItems[$i]['quantity'];
            $arrCartItems[$i]['price'] += $arrCartItems[$i]['add_price'];
            $price = $arrCartItems[$i]['price'];

            if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                $arrCartItems[$i]['price_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax($price, $arrCartItems[$i]['productsClass']['product_id'], $arrCartItems[$i]['id'][0]);
                $incTax = $arrCartItems[$i]['price_inctax'];
                $tax = SC_Helper_TaxRule_Ex::sfTax($price, $arrCartItems[$i]['productsClass']['product_id'], $arrCartItems[$i]['productsClass']['product_class_id'], $pref_id, $country_id);
            } else {
                $incTax = SC_Helper_DB_Ex::sfCalcIncTax($price);
                $tax = SC_Helper_DB_Ex::sfTax($price);
            }
            $total = $incTax * $quantity;
            $arrCartItems[$i]['total_inctax'] = $total;
            $total_inctax += $total;

            $total_tax += ($tax * $quantity);

            $point_rate = $arrCartItems[$i]['point_rate'];

            $point = SC_Utils_Ex::sfPrePoint($price, $point_rate);
            $total_point += ($point * $quantity);
        }

        return array($total_inctax, $total_point, $deliv_free_cnt, $total_tax);
    }

    function lfSetOrderDetailOptions(&$arrOrderDetail)
    {
        foreach ((array) $arrOrderDetail as $key => $detail) {
            $arrOrderDetail[$key]['add_price'] = 0;
            $arrOrderDetail[$key]['add_price_inctax'] = 0;
            $arrOrderDetail[$key]['plg_productoptions'] = NULL;
            $options = @unserialize($detail['plg_productoptions_flg']);
            if (is_array($options) && count($options) > 0) {
                foreach ($options as $option_id => $optioncategory_id) {
                    $option_action = 0;
                    $value = 0;
                    if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['optioncategory_name'] = $optioncategory_id;
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['optioncategory_id'] = $optioncategory_id;
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['optioncategory_value'] = $value = $optioncategory_id;
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['option_action'] = $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['option_type'] = plg_ProductOptions_Util::lfGetOptionType($option_id);
                    } else {
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['optioncategory_name'] = plg_ProductOptions_Util::lfGetOptionCatName($optioncategory_id);
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['optioncategory_id'] = $optioncategory_id;
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['optioncategory_value'] = $value = plg_ProductOptions_Util::lfGetOptionCatValue($optioncategory_id);
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['option_action'] = $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                        $arrOrderDetail[$key]['plg_productoptions'][$option_id]['option_type'] = plg_ProductOptions_Util::lfGetOptionType($option_id);
                        if ($option_action == 1) {
                            $arrOrderDetail[$key]['add_price']+=$value;
                        }
                    }
                }
                $arrOptions[$arrOrderDetail[$key]['product_class_id']] = $arrOrderDetail[$key]['plg_productoptions'];
            }
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                $arrOrderDetail[$key]['add_price_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax($arrOrderDetail[$key]['add_price'], $arrOrderDetail[$key]['product_id'], $arrOrderDetail[$key]['product_class_id']);
            } else {
                $arrOrderDetail[$key]['add_price_inctax'] = SC_Helper_DB_Ex::sfCalcIncTax($arrOrderDetail[$key]['add_price']);
            }
        }
        return $arrOptions;
    }

    function convProductOptions($option_flg)
    {
        $arrOption = array();
        $options = @unserialize($option_flg);
        if (count($options) > 0) {
            foreach ($options as $option_id => $optioncategory_id) {
                $option_action = 0;
                $value = 0;
                if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                    $arrOption[$option_id]['optioncategory_name'] = $optioncategory_id;
                    $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                    $arrOption[$option_id]['optioncategory_value'] = $value = $optioncategory_id;
                    $arrOption[$option_id]['option_action'] = $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                    $arrOption[$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                    $arrOption[$option_id]['option_type'] = plg_ProductOptions_Util::lfGetOptionType($option_id);
                } else {
                    $arrOption[$option_id]['optioncategory_name'] = plg_ProductOptions_Util::lfGetOptionCatName($optioncategory_id);
                    $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                    $arrOption[$option_id]['optioncategory_value'] = $value = plg_ProductOptions_Util::lfGetOptionCatValue($optioncategory_id);
                    $arrOption[$option_id]['option_action'] = $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                    $arrOption[$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                    $arrOption[$option_id]['option_type'] = plg_ProductOptions_Util::lfGetOptionType($option_id);
                }
            }
        }
        return $arrOption;
    }

    function getTotalOptionPrice($options)
    {
        list($option_price) = plg_ProductOptions_Util::getTotalOptionInfo($options);
        return $option_price;
    }

    function getTotalOptionPoint($options)
    {
        list($option_price, $option_point) = plg_ProductOptions_Util::getTotalOptionInfo($options);
        return $option_point;
    }

    function getTotalOptionInfo($options)
    {
        $arrOption = array();
        $add_price = 0;
        $add_point = 0;
        $deliv_free_cnt = 0;
        if (is_array($options)) {
            foreach ($options as $option_id => $optioncategory_id) {
                if (!is_array($optioncategory_id)) {
                    if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                        $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                        $arrOption[$option_id]['optioncategory_name'] = $optioncategory_id;
                        $arrOption[$option_id]['optioncategory_value'] = $value = $optioncategory_id;
                    } else {
                        $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                        $arrOption[$option_id]['optioncategory_name'] = plg_ProductOptions_Util::lfGetOptionCatName($optioncategory_id);
                        $arrOption[$option_id]['optioncategory_value'] = $value = plg_ProductOptions_Util::lfGetOptionCatValue($optioncategory_id);
                    }
                    $arrOption[$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                    $arrOption[$option_id]['option_action'] = $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                } else {
                    $value = $optioncategory_id['optioncategory_value'];
                    $option_action = $optioncategory_id['option_action'];
                }
                if ($option_action == 1) {
                    $add_price += $value;
                } elseif ($option_action == 2) {
                    $add_point += $value;
                } elseif ($option_action == 3 && $value == 1) {
                    $deliv_free_cnt++;
                }
            }
        }
        return array($add_price, $add_point, $deliv_free_cnt, $arrOption);
    }

    function addProductOptionsParam(&$objFormParam)
    {
        $objFormParam->addParam("オプション", 'plg_productoptions', LTEXT_LEN, '', array('MAX_LENGTH_CHECK'));
    }

    function getUpdateFlg()
    {
        if (!isset($GLOBALS['PLG_PRODUCTOPTIONS_UPDATE_FLG'])) {
            $objQuery = & SC_Query_Ex::getSingletonInstance();
            $flg = $objQuery->get('free_field1', 'dtb_plugin', 'plugin_code = ?', array('ProductOptions'));
            $GLOBALS['PLG_PRODUCTOPTIONS_UPDATE_FLG'] = $flg;
        }
        return $GLOBALS['PLG_PRODUCTOPTIONS_UPDATE_FLG'];
    }

    function getCartInFlg()
    {
        if (!isset($GLOBALS['PLG_PRODUCTOPTIONS_CARTIN_FLG'])) {
            $objQuery = & SC_Query_Ex::getSingletonInstance();
            $flg = $objQuery->get('free_field2', 'dtb_plugin', 'plugin_code = ?', array('ProductOptions'));
            $GLOBALS['PLG_PRODUCTOPTIONS_CARTIN_FLG'] = $flg;
        }
        return $GLOBALS['PLG_PRODUCTOPTIONS_CARTIN_FLG'];
    }

    function compareOptions($options1, $options2)
    {
        if (!is_array($options1))
            $options1 = array();
        if (!is_array($options2))
            $options2 = array();
        $cnt = count($options1) > count($options2) ? count($options1) : count($options2);
        if ((count(array_intersect_assoc($options1, $options2)) == $cnt && count($options1) > 0 && count($options2) > 0) || (count($options1) == 0 && count($options2) == 0) || $options1 == $options2) {
            return true;
        }
        return false;
    }

    function dropPkey()
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        // dtb_shipment_itemテーブルの制約があるかを確認
        $arrRet = $objQuery->getTableInfo("dtb_shipment_item");
        $exists_constraint = false;
        foreach ($arrRet as $field) {
            if ($field['name'] == 'shipping_id') {
                if (ereg('primary_key', $field['flags'])) {
                    $exists_constraint = true;
                }
            }
        }

        if ($exists_constraint) {
            if (DB_TYPE == "pgsql") {
                $objQuery->query("ALTER TABLE dtb_shipment_item DROP CONSTRAINT dtb_shipment_item_pkey");
            } elseif (DB_TYPE == "mysql") {
                $objQuery->query("ALTER TABLE dtb_shipment_item DROP PRIMARY KEY");
            }
            $objQuery->query("CREATE INDEX dtb_shipment_item_mkey ON dtb_shipment_item (shipping_id, product_class_id, order_id)");
        }
    }

    function createPkey()
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        // dtb_shipment_itemテーブルの制約があるかを確認
        $arrRet = $objQuery->getTableInfo("dtb_shipment_item");
        $exists_constraint = false;
        $exists_index = false;
        foreach ($arrRet as $field) {
            if ($field['name'] == 'shipping_id') {
                if (ereg('primary_key', $field['flags'])) {
                    $exists_constraint = true;
                }
                if (ereg('multiple_key', $field['flags'])) {
                    $exists_index = true;
                }
            }
        }

        if ($exists_index) {
            if (DB_TYPE == "pgsql") {
                $objQuery->query("ALTER TABLE dtb_shipment_item DROP CONSTRAINT dtb_shipment_item_mkey");
            } elseif (DB_TYPE == "mysql") {
                $objQuery->query("ALTER TABLE dtb_shipment_item DROP INDEX dtb_shipment_item_mkey");
            }
        }

        if (!$exists_constraint) {
            $objQuery->query("ALTER TABLE dtb_shipment_item ADD PRIMARY KEY (shipping_id, product_class_id, order_id)");
        }
    }

}
