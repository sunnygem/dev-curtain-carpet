<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'SC_Product.php';

class SC_Product_Ex extends SC_Product
{

    /**
     * 商品データから特徴を取得して返す
     *
     * @param arr $product_id
     * @param bool $include_hidden
     * @param bool $include_deleted
     * @return bool
     */
    public function getProductFunctionFeature($arr_product=null ) {
    //var_dump($arr_product);
        if( count( $arr_product ) > 0 )
        {
            $arr_flg = [];
            foreach( $arr_product as $key => $val )
            {
                if( preg_match( '/^function_feature[0-9]+$/', $key ) && $val == '1' )
                {
                    $arr_flg[] = $key;
                }
            }
            if( count( $arr_flg ) === 0 )
            {
                return false;
            }
            else
            {
                $objQuery =& SC_Query_Ex::getSingletonInstance();
                $cols = '*';
                $from = 'dtb_function_feature';
                $where = 'del_flg = 0 AND column_name IN (' . SC_Utils_Ex::repeatStrWithSeparator('?', count($arr_flg)) . ') ORDER BY id ASC';
                return $functionFeatures = $objQuery->select($cols, $from, $where, $arr_flg);
            }
        }
        else
        {
            return false;
        }
    }}
