<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->



<!--{assign var="key" value="plg_managecustomerstatus_product_disp"}-->
<tr>
    <th>会員ランク別購入不可設定<br><span class="attention">(購入不可にしたいランクにチェック)</span></th>
    <td>
        <span class="attention"><!--{$arrErr[$key]}--></span>
        <!--{html_checkboxes name=$key options=$arrPlgManageCustomerStatus selected=$arrForm[$key] separator='&nbsp;&nbsp;'}-->
    </td>
</tr>
