<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{foreach from=$arrRankPrices item=item}-->
<!--{assign var="key" value="plg_managecustomerstatus_price`$item.status_id`"}-->
<td class="right"><!--{$arrForm[$key].value[$index]|h}--></td>
<!--{/foreach}-->