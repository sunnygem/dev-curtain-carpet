<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<script type="text/javascript">
// URLの表示非表示切り替え
function lfnDispChange(){
    inner_id = 'switch';

    cnt = document.form1.item_cnt.value;

    if($('#disp_url1').css("display") == 'none'){
        for (i = 1; i <= cnt; i++) {
            disp_id = 'disp_url'+i;
            $('#' + disp_id).css("display", "");

            disp_id = 'disp_cat'+i;
            $('#' + disp_id).css("display", "none");

            $('#' + inner_id).html('    URL <a href="#" onclick="lfnDispChange();"> &gt;&gt; カテゴリ表示<\/a>');
        }
    }else{
        for (i = 1; i <= cnt; i++) {
            disp_id = 'disp_url'+i;
            $('#' + disp_id).css("display", "none");

            disp_id = 'disp_cat'+i;
            $('#' + disp_id).css("display", "");

            $('#' + inner_id).html('    カテゴリ <a href="#" onclick="lfnDispChange();"> &gt;&gt; URL表示<\/a>');
        }
    }

}

</script>


<div id="products" class="contents-main">
    <form name="search_form" id="search_form" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="search" />
        <h2>検索条件設定</h2>

        <!--検索条件設定テーブルここから-->
        <table>
            <tr>
                <th>期間選択</th>
                <td>
                    <!--{if $arrErr.search_startyear || $arrErr.search_endyear}-->
                        <span class="attention"><!--{$arrErr.search_startyear}--></span>
                        <span class="attention"><!--{$arrErr.search_endyear}--></span>
                    <!--{/if}-->
                    <select name="search_startyear" style="<!--{$arrErr.search_startyear|sfGetErrorColor}-->">
                    <option value="">----</option>
                    <!--{html_options options=$arrStartYear selected=$arrForm.search_startyear.value}-->
                    </select>年
                    <select name="search_startmonth" style="<!--{$arrErr.search_startyear|sfGetErrorColor}-->">
                    <option value="">--</option>
                    <!--{html_options options=$arrStartMonth selected=$arrForm.search_startmonth.value}-->
                    </select>月
                    <select name="search_startday" style="<!--{$arrErr.search_startyear|sfGetErrorColor}-->">
                    <option value="">--</option>
                    <!--{html_options options=$arrStartDay selected=$arrForm.search_startday.value}-->
                    </select>日～
                    <select name="search_endyear" style="<!--{$arrErr.search_endyear|sfGetErrorColor}-->">
                    <option value="">----</option>
                    <!--{html_options options=$arrEndYear selected=$arrForm.search_endyear.value}-->
                    </select>年
                    <select name="search_endmonth" style="<!--{$arrErr.search_endyear|sfGetErrorColor}-->">
                    <option value="">--</option>
                    <!--{html_options options=$arrEndMonth selected=$arrForm.search_endmonth.value}-->
                    </select>月
                    <select name="search_endday" style="<!--{$arrErr.search_endyear|sfGetErrorColor}-->">
                    <option value="">--</option>
                    <!--{html_options options=$arrEndDay selected=$arrForm.search_endday.value}-->
                    </select>日
                </td>
            </tr>
            <tr>
                <th>メーカー選択</th>
                <td>
                    <!--{assign var=key value="search_maker_id"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
                    <option value="">全て</option>
                    <!--{html_options options=$arrMakerList selected=$arrForm[$key].value}-->
                    </select>
                </td>
            </tr>
        </table>
        <div class="btn">
            <p class="page_rows">検索結果表示件数
            <!--{assign var=key value="search_page_max"}-->
            <!--{if $arrErr[$key]}-->
                <span class="attention"><!--{$arrErr[$key]}--></span>
            <!--{/if}-->
            <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
                <!--{html_options options=$arrPageMax selected=$arrForm.search_page_max.value}-->
            </select> 件</p>

            <div class="btn-area">
                <ul>
                    <li><a class="btn-action" href="javascript:;" onclick="eccube.fnFormModeSubmit('search_form', 'search', '', ''); return false;"><span class="btn-next">この条件で検索する</span></a></li>
                </ul>
            </div>

        </div>
        <!--検索条件設定テーブルここまで-->
    </form>


    <!--{if count($arrErr) == 0 and ($smarty.post.mode == 'search' or $smarty.post.mode == 'delete')}-->

        <!--★★検索結果一覧★★-->
        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="search" />
            <input type="hidden" name="maker_id" value="" />
            <input type="hidden" name="category_id" value="" />
            <!--{foreach key=key item=item from=$arrHidden}-->
                <!--{if is_array($item)}-->
                    <!--{foreach item=c_item from=$item}-->
                    <input type="hidden" name="<!--{$key|h}-->[]" value="<!--{$c_item|h}-->" />
                    <!--{/foreach}-->
                <!--{else}-->
                    <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->" />
                <!--{/if}-->
            <!--{/foreach}-->
            <h2>検索結果一覧</h2>
            <div class="btn">
                <span class="attention"><!--検索結果数--><!--{$tpl_linemax}-->件</span>&nbsp;が該当しました。
                <!--検索結果-->
<!--               <a class="btn-tool" href="javascript:;" onclick="eccube.setModeAndSubmit('csv','',''); return false;">CSV ダウンロード</a>-->
            </div>
            <!--{if count($arrAffiliateList) > 0}-->

                <!--{include file=$tpl_pager}-->

                <!--検索結果表示テーブル-->
                <table class="list" id="products-search-result">
                    <col width="10%" />
                    <col width="20%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="20%" />
                    <tr>
                        <th>アフィリエイトID</th>
                        <th>名前</th>
                        <th>アクセス数</th>
                        <th>アクセス人数</th>
                        <th>CV数</th>
                        <th>CV金額</th>
                    </tr>
                    <!--{section name=cnt loop=$arrAffiliateList}-->
                        <!--▼商品<!--{$smarty.section.cnt.iteration}-->-->
                        <tr>
                            <td><!--{if $arrAffiliateList[cnt].affiliate_id != '-1'}--><!--{$arrAffiliateList[cnt].affiliate_id}--><!--{/if}--></td>
                            <td><!--{$arrAffiliateList[cnt].name}--></td>
                            <td style="text-align:right;"><!--{$arrAffiliateList[cnt].accesscount}-->PV</td>
                            <td style="text-align:right;"><!--{$arrAffiliateList[cnt].accessperson}-->人</td>
                            <td style="text-align:right;"><!--{$arrAffiliateList[cnt].cv}-->回</td>
                            <td style="text-align:right;"><!--{$arrAffiliateList[cnt].cvprice}-->円</td>
                        </tr>
                        <!--▲商品<!--{$smarty.section.cnt.iteration}-->-->
                    <!--{/section}-->
                </table>
                <input type="hidden" name="item_cnt" value="<!--{$arrProducts|@count}-->" />
                <!--検索結果表示テーブル-->
            <!--{/if}-->

        </form>

        <!--★★検索結果一覧★★-->
    <!--{/if}-->
</div>
