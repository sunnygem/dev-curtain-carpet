<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->

	
<article class="contents_page product delivery abouts">
	<h1 class="ttl_01">運営会社</h1>
	<div class="contents_link_02_box">
		<dl class="table_02">
			<dt>会社名</dt>
			<dd>株式会社スモール・プラネット</dd>
			<dt>運営統括責任者</dt>
			<dd>土屋 世央</dd>
			<dt>所在地</dt>
			<dd>〒192-0904　　東京都八王子市子安町2-2-13</dd>
			<dt>電話番号</dt>
			<dd><a href="tel:0426432779">042-643-2779</a><br>（土・日・祝日・年末年始を除く10:00～18:00）<br>※通話料がかかります。あらかじめご了承ください。</dd>
	</dl>
	</div>	
	
</article>
<!--/.contents_page-->	
	
	
	
	
<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->	
