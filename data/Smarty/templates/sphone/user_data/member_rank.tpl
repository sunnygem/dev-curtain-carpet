<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->


<article class="contents_page product member_rank">
	<h1 class="ttl_01">会員ランクについて</h1>

	<p class="member_rank_text_01">お買い物金額の合計に応じて<span>会員ランク</span>が決定！<br><span>ランクに応じたお得な特典</span>が受けられます。</p>
	<ul class="contents_link_li_01 clearfix">
		<li><img src="<!--{$TPL_URLPATH}-->img/member_rank/point_01.png" alt="お買い物"></li>
		<li><img src="<!--{$TPL_URLPATH}-->img/member_rank/point_02.png" alt="今月の会員ランクが決定！"></li>
		<li><img src="<!--{$TPL_URLPATH}-->img/member_rank/point_03.png" alt="更にお得にお買い物！！"></li>
	</ul>


	<div class="contents_link_04 clear">
		<h2 class="rank_h2">会員ランクの種類</h2>
		<p class="member_rank_text_02">会員ランクは直近12ヵ月のご購入金額累計(税込)によって毎月決まります。<span>※</span></p>
		<ul class="contents_link_li_02 clearfix">
			<li><img src="<!--{$TPL_URLPATH}-->img/member_rank/nomal.png" alt="ノーマルチョコビ"></li>
			<li><img src="<!--{$TPL_URLPATH}-->img/member_rank/silver.png" alt="シルバーチョコビ"></li>
			<li><img src="<!--{$TPL_URLPATH}-->img/member_rank/gold.png" alt="ゴールドチョコビ"></li>
		</ul>
		<p class="member_rank_text_03">※毎月末までの出荷済のご注文まで対象とし、翌月1日にランクアップ。 ランクアップはマイページにてご確認いただけます。</p>
	</div>
	<!--/.contents_link_04-->

	<div class="contents_link_04">
		<h2 class="rank_h2">会員ランクの特典内容</h2>
		<div class="contents_link_04_wrap">
			<div class="contents_link_04_box">
				<img src="<!--{$TPL_URLPATH}-->img/member_rank/special_01.png" alt="ランクアップごとにボーナスポイントをプレゼント！">
				<p class="member_rank_text_04">※ボーナスポイントはランクアップに基づき、毎月末までの出荷済分を対象とし、翌月1日に付与いたします。</p>
			</div>
			<!--/.contents_link_04_wrap-->

			<div class="contents_link_04_box">
				<img src="<!--{$TPL_URLPATH}-->img/member_rank/special_02.png" alt="ランク別でお買い物ポイントが多くもらえちゃうスペシャルチョコビデーを開催！">
				<p class="member_rank_text_04">※スペシャルチョコビデーの開催は不定期で実施いたします。</p>				
			</div>
			<!--/.contents_link_04_wrap-->

			<div class="contents_link_04_box">
				<img src="<!--{$TPL_URLPATH}-->img/member_rank/special_03.png" alt="シークレットSALEや先行販売など限定・優先イベントにご招待！">
			</div>
			<!--/.contents_link_04_wrap-->

		</div>
	</div>
	<!--/.contents_link_04-->





</article>
<!--/.contents_page-->





<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->
