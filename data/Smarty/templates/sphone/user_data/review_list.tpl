<style>
.LC_Page_User h2 {
  margin-top: 0;
}
.LC_Page_User h2 > img {
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
}
.LC_Page_User h2 > span {
  display: inline-block;
  color: #00c1fc;
  font-size: 32px;
  vertical-align: middle;
}
.thanksComment {
  background-color: #ddf1b5;
  border-radius: 4px;
  padding: 40px 0 40px 30px;
  position: relative;
  margin-bottom: 60px;
}
.thanksComment p.comment__ {
  color: #fd9d03;
  font-size: 22px;
  font-family: 'hmarugo';
  font-weight: bold;
  line-height: 1.4;
}
.thanksComment .image__ {
  position: absolute;
  right: 5px;
  top: -22px;
}
.voiceproduct > .image{
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
}
.voiceproduct > .name{
  display: inline-block;
  vertical-align: middle;
}
</style>

<div class="review">
<h2><img src="<!--{$TPL_URLPATH}-->img/user_data/review_list.png" alt="レビュー一覧" /><span>レビュー一覧</span></h2>
<div class="thanksComment">
<p class="comment__">たくさんのレビューをいただいております★<br>ありがとうございます！</p>
<div class="image__"><img src="<!--{$TPL_URLPATH}-->img/user_data/thanksComment.png"></div>
</div>
    <!--{if count($arrReview) > 0}-->
        <ul>
            <!--{section name=cnt loop=$arrReview}-->
                <li>
                    <p class="voiceproduct">
                        <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrReview[cnt].product_id|u}-->" class="image"><img src="<!--{$smarty.const.ROOT_URLPATH}-->resize_image.php?image=<!--{$arrReview[cnt].main_list_image|sfNoImageMainList|h}-->&amp;width=40&amp;height=40" alt="<!--{$arrReview[cnt].name|h}-->" /></a>
                        <a href="<!--{$smarty.const.HTTP_URL}-->products/detail.php?product_id=<!--{$arrReview[cnt].product_id|u}-->" class="name"><!--{$arrReview[cnt].name|h}--></a>
                    </p>
                    <!-- .voiceproduct -->
                    <p class="voicetitle"><img src="<!--{$TPL_URLPATH}-->img/product/review_icon.png"><span><!--{$arrReview[cnt].title|h}--></span></p>
                    <p class="voicedate"><span class="user">投稿者：<!--{if $arrReview[cnt].reviewer_url}--><a href="<!--{$arrReview[cnt].reviewer_url}-->" target="_blank"><!--{$arrReview[cnt].reviewer_name|h}--></a><!--{else}--><!--{$arrReview[cnt].reviewer_name|h}--><!--{/if}--></span><span class="postData">投稿日：<!--{$arrReview[cnt].create_date|sfDispDBDate:false}--></span></p>
                    <p class="voicecomment"><!--{$arrReview[cnt].comment|h|nl2br}--></p>
                </li>
            <!--{/section}-->
        </ul>
    <!--{/if}-->
</div>
<!-- .review -->
