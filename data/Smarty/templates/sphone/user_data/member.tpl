<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->


<article class="contents_page product delivery member">
	<h1 class="ttl_01">会員について</h1>
	<section class="contents_box">
		<ul>
			<li><a href="#sec01">クレヨンしんちゃん公式オンラインショップ会員の特典</a></li>
			<li><a href="#sec02">ポイントの詳細について</a></li>
			<li><a href="#sec03">クーポンの詳細について</a></li>
		</ul>
	</section>
	<!--/.contents_box-->


	<div class="contents_link_02">
		<div class="contents_link_02_box" id="sec01">
			<h2>クレヨンしんちゃん公式オンラインショップ会員の特典</h2>
			<p>クレヨンしんちゃん公式オンラインショップ会員にご登録いただくとさらにお買い物をお楽しみいただけます。</p>
			<div class="contents_link_02_box02 clearfix">
				<h3 class="contents_link_02_border">お買い物ごとにポイントが貯まる！</h3>
				<div class="contents_link_02_textbox">
					<p>お買い物に応じてポイントが貯まり、貯まったポイントでまたお買い物が<br>楽しめる嬉しい還元制度です。<br>さらにキャンペーンやメルマガでお得なクーポンがもらえるチャンスも！</p>
					<ul class="arrow_list">
						<li><a href="#sec02">ポイントの詳細についてはこちら</a></li>
						<li><a href="#sec03">クーポンの詳細についてはこちら</a></li>
					</ul>
				</div>
				<img src="<!--{$TPL_URLPATH}-->img/member/img_01.png" class="member_img" alt="しんちゃん">
			</div>
			<!--./contents_link_02_box02-->


			<div class="contents_link_02_box02 clearfix clear">
				<h3 class="contents_link_02_border">マイページで便利にお買い物ができる！</h3>
				<div class="contents_link_02_textbox">
					<p>「配送先リスト」登録や、「ご注文履歴」の確認など、お買い物を<br>便利にする機能がいっぱいです。</p>
					<ul class="arrow_list">
						<li><a href="/user_data/mypage.php">マイページの詳細についてはこちら</a></li>
					</ul>
				</div>
				<img src="<!--{$TPL_URLPATH}-->img/member/img_02.png" class="member_img" alt="しんちゃん">
			</div>
			<!--./contents_link_02_box02-->

			<div class="contents_link_02_box02 clearfix clear">
				<h3 class="contents_link_02_border">メルマガで新商品やキャンペーン情報を配信！</h3>
				<div class="contents_link_02_textbox">
					<p>新商品の情報や、キャンペーンなど他では得られない情報が満載の<br>メールマガジンを配信！<br>欲しかった商品をひと足早くゲットできちゃうかも！</p>
				</div>
				<img src="<!--{$TPL_URLPATH}-->img/member/img_03.png" class="member_img" alt="しんちゃん">
			</div>
			<!--./contents_link_02_box02-->


			<div class="contents_link_02_box02 clearfix clear">
				<h3 class="contents_link_02_border">お誕生月にクーポンをプレゼント！！</h3>
				<div class="contents_link_02_textbox">
					<p>あなたのお誕生日の月には、クレヨンしんちゃん公式オンラインショップの<br>お買い物ですぐに使える割引クーポンをプレゼント♪</p>
					<ul class="arrow_list">
						<li><a href="#sec03">お誕生日月クーポンの詳細はこちら</a></li>
					</ul>
				</div>
				<img src="<!--{$TPL_URLPATH}-->img/member/img_04.png" class="member_img" alt="しんちゃん">
			</div>
			<!--./contents_link_02_box02-->

			<div class="contents_link_02_box02 clearfix clear">
				<h3 class="contents_link_02_border">再入荷をメールでお知らせ！</h3>
				<div class="contents_link_02_textbox">
					<p>品切れている商品の商品ページより再入荷お知らせメールの設定がご利用いただけます。<br>会員ログインした状態で「再入荷お知らせメール登録」ボタンでご登録頂くと、会員登録いただいたメールアドレス宛てに、ご登録いただいた商品が再入荷され次第メールでお知らせいたします。</p>
					<ul class="arrow_list">
						<li><a href="＃">再入荷お知らせの詳細はこちら</a></li>
					</ul>
				</div>
				<img src="<!--{$TPL_URLPATH}-->img/member/img_05.png" class="member_img" alt="しんちゃん">
			</div>
			<!--./contents_link_02_box02-->

			<div class="contents_link_02_box02 clearfix clear">
				<h3 class="contents_link_02_border">年間累計お買いもの金額に応じて会員ランクが決定！</h3>
				<div class="contents_link_02_textbox">
					<p>年間累計お買物金額に応じて会員ランクがつきます。会員ランクに応じて、<br>先行発売やポイントアップ、お得なクーポンがもらえるチャンスも！</p>
					<ul class="arrow_list">
						<li><a href="/user_data/member_rank.php">会員ランクについてはこちら</a></li>
					</ul>
				</div>
				<img src="<!--{$TPL_URLPATH}-->img/member/img_06.png" class="member_img" alt="しんちゃん">
			</div>
			<!--./contents_link_02_box02-->
		</div>
	</div>



	<div class="contents_link_02">
		<div class="contents_link_02_box" id="sec02">
			<h2>ポイントの詳細について</h2>
			<p>以下、テキストでの説明がどんどん入っていきます。</p>
			</div>
			<!--./contents_link_02_box02-->
	</div>

	<div class="contents_link_02">
		<div class="contents_link_02_box" id="sec03">
			<h2>クーポンの詳細について</h2>
			<p>以下、テキストでの説明がどんどん入っていきます。</p>
			</div>
			<!--./contents_link_02_box02-->
	</div>

</article>
<!--/.contents_page-->





<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->
