<style type="text/css">
ul,li {
	margin:0;
	padding:0;
}
a,
a:hover {
	text-decoration:none;
}
h2.title > span {
    display: inline-block;
    vertical-align: middle;
    color: #00c1fc;
    font-size: 100%;
}
.section h3 {
    margin: 0 0 16px;
    color: #555555;
    padding:0;
    border-bottom: solid 4px #f0f0f0;
}
.section ul {
	display:-webkit-flex;
	display:flex;
	-webkit-flex-wrap:wrap;
	flex-wrap:wrap;
}
.section ul li {
	list-style:none;
	width:48%;
	margin:0 1% 10px;
}

.section ul li span {
    display: block;
    color: #00c1fc;
    font-size: 13px;
    font-weight: bold;
}
</style>

<h2 class="title"><img src="<!--{$TPL_URLPATH}-->img/feature/featureHeading.png" alt=""><span>特集から探す</span></h2>

<div class="section">
<h3><img src="<!--{$TPL_URLPATH}-->img/user_data/star_ki.png" alt=""><span>注目の特集</span></h3>
<ul>
<li><a href="/products/list.php?category_id=1302"><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr01.jpg"><span>ここでしか手に入らない☆しんちゃんショップ限定アイテムが勢ぞろい</span></a></li>
<li><a href="/products/list.php?category_id=1302"><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr02.jpg"><span>新作映画がもうすぐ公開！映画関連グッズがたくさん登場！！</span></a></li>
<li><a href="/products/list.php?category_id=1302"><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr03.jpg"><span>おもわずクスッと笑っちゃう！楽しいぶりぶりざえもんのアイテムが揃ったよ☆</span></a></li>
</ul>
</div>
<!-- .section -->

<div class="section">
<h3><img src="<!--{$TPL_URLPATH}-->img/user_data/star_ki.png" alt=""><span>季節のオススメ特集</span></h3>
<ul>
<li><a href="/products/list.php?category_id=1302"><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr01.jpg"><span>ここでしか手に入らない☆しんちゃんショップ限定アイテムが勢ぞろい</span></a></li>
<li><a href="/products/list.php?category_id=1302"><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr02.jpg"><span>新作映画がもうすぐ公開！映画関連グッズがたくさん登場！！</span></a></li>
<li><a href="/products/list.php?category_id=1302"><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr03.jpg"><span>おもわずクスッと笑っちゃう！楽しいぶりぶりざえもんのアイテムが揃ったよ☆</span></a></li>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr01.jpg"><span>ここでしか手に入らない☆しんちゃんショップ限定アイテムが勢ぞろい</span></a></li>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr02.jpg"><span>新作映画がもうすぐ公開！映画関連グッズがたくさん登場！！</span></a></li>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr03.jpg"><span>おもわずクスッと笑っちゃう！楽しいぶりぶりざえもんのアイテムが揃ったよ☆</span></a></li>
</ul>
</div>
<!-- .section -->

<div class="section">
<h3><img src="<!--{$TPL_URLPATH}-->img/user_data/star_ki.png" alt=""><span>オススメ特集</span></h3>
<ul>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr01.jpg"><span>ここでしか手に入らない☆しんちゃんショップ限定アイテムが勢ぞろい</span></a></li>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr02.jpg"><span>新作映画がもうすぐ公開！映画関連グッズがたくさん登場！！</span></a></li>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr03.jpg"><span>おもわずクスッと笑っちゃう！楽しいぶりぶりざえもんのアイテムが揃ったよ☆</span></a></li>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr01.jpg"><span>ここでしか手に入らない☆しんちゃんショップ限定アイテムが勢ぞろい</span></a></li>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr02.jpg"><span>新作映画がもうすぐ公開！映画関連グッズがたくさん登場！！</span></a></li>
<li><a href=""><img src="<!--{$TPL_URLPATH}-->img/feature/featureBnr03.jpg"><span>おもわずクスッと笑っちゃう！楽しいぶりぶりざえもんのアイテムが揃ったよ☆</span></a></li>
</ul>
</div>
<!-- .section -->
