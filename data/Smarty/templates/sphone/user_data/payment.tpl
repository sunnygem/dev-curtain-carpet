<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->


<article class="contents_page product payment">
	<h1 class="ttl_01">お支払いについて</h1>
	<p class="contents_page_text_01">当ショップでは、以下のお支払方法をご利用いただけます。ご希望のお支払方法をお選びください。</p>
	<section class="contents_box">
		<ul>
			<li><a href="#sec01">クレジットカード決済</a></li>
			<li><a href="#sec02">代金引換</a></li>
			<li><a href="#sec03">【先払い】コンビニ決済</a></li>
			<li><a href="#sec04">【先払い】ネットバンキング</a></li>
			<li><a href="#sec05">【後払い】ニッセン後払い</a></li>
		</ul>
	</section>
	<!--/.contents_box-->


	<div class="contents_link_02">
		<div class="contents_link_02_box" id="sec01">
			<h2>クレジットカード決済</h2>
			<p>各種クレジットカードを利用し、お支払いいただけます。</p>
			<h3>【ご利用いただけるクレジットカード】</h3>
			<p><img src="<!--{$TPL_URLPATH}-->img/payment/card.png" alt="カード一覧"></p>
			<p>VISA、JCB、MASTER、DINERS、NICOS、OMC、セゾンカード、アメリカン・エキスプレスがご利用いただけます。</p>

			<h3>【ご利用にあたって】</h3>
			<p>・承認番号が取得され次第、発送を手配させていただきます。<br>
				・代金は各カード会社の会員規約に基づき、ご指定の口座からの自動引き落しとなります。<br>
				・お客様のカード情報はSSLというセキュリティシステムにより暗号化されて送信されます。安心してご利用ください。</p>
<!--
			<p><img src="<!--{$TPL_URLPATH}-->img/payment/site.png" alt="GMO"></p>
-->
<table width="135" border="0" cellpadding="2" cellspacing="0" title="このマークは、SSL/TLSで通信を保護している証です。">
<tr>
<td width="135" align="center" valign="top">
<!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. --> <SCRIPT LANGUAGE="JavaScript"  TYPE="text/javascript" SRC="//smarticon.geotrust.com/si.js"></SCRIPT>
<!-- end  GeoTrust Smart Icon tag -->
<a href="https://www.geotrust.co.jp/ssl_guideline/ssl_beginners/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 12px 'ＭＳ ゴシック',sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">SSLとは？</a></td>
</tr>
</table>

			<p>・クレジットカードでお支払いの場合はセキュリティコードの入力が必要です。 </p>

			<h3>【セキュリティコードとは】</h3>
			<p>セキュリティコードとは、クレジットカード番号とは別に、クレジットカードの裏面に印字されている3桁（または4桁）の数字です。<br>お客様自身がお手元のクレジットカードを確認してコードを入力することで、ご利用時のセキュリティが向上します。</p>
			<br>
			<p>・VISA、JCB、MASTER、DINERS、NICOS、OMC、セゾンカードの表示例 </p>
			<p><img src="<!--{$TPL_URLPATH}-->img/payment/card_01.png" alt="表示例"></p>
			<br><br>
			<p>・アメリカン・エキスプレスの表示例 </p>
			<p><img src="<!--{$TPL_URLPATH}-->img/payment/card_02.png" alt="表示例"></p>

			<h3>【注意事項】</h3>
			<ul class="kome">
				<li>お支払回数は、1回払い、分割3回/5回/6回/10回/12回、リボ払いより選べます。</li>
				<li>分割払い／リボ払いの場合、金利・手数料はお客様のご負担となります。</li>
				<li>分割払い／リボ払いはお客様とクレジットカード会社の間でご契約が必要です。</li>
				<li>当店では分割払い、リボ払いの金利・手数料に関するご質問にはご回答できません。金利・手数料に関してはご利用のクレジットカード会社様へ直接お問合せください。</li>
				<li>海外発行のカードはご利用いただけません。</li>
				<li>クレジット会社の審査基準等により、契約が成立しないことがございます。あらかじめご了承ください。</li>
			</ul>
			<p class="text_red">決済代行提供：GMOペイメントゲートウェイ株式会社 ※外部サイトにリンクします</p>

		</div>

		<p class="text_36">※同様に・代引き/コンビニ決済方法などすべて記載します。</p>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec02">
			<h2>代金引換</h2>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec03">
			<h2>【先払い】コンビニ決済</h2>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec04">
			<h2>【先払い】ネットバンキング</h2>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec05">
			<h2>【後払い】ニッセン後払い</h2>
		</div>
		<!--/.contents_link_02_box-->
		</div>




</article>
<!--/.contents_page-->





<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->
