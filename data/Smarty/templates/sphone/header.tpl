
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/user_data/packages/shinchan/css/slick.css" type="text/css" media="all" />
<link rel="stylesheet" href="/user_data/packages/shinchan/css/slick-theme.css" type="text/css" media="all" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/hiraku.css" type="text/css" media="all" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/hiraku.style.css" type="text/css" media="all" />
<script src="/user_data/packages/shinchan/js/slick.min.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/hiraku.js"></script>

<header class="container">
<div id="headerInner">
<h1 id="headerLogo"><a href="<!--{$smarty.const.HTTPS_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/common/logo.png" alt="クレヨンしんちゃん公式オンラインショップ"></a></h1>

<form name="header_login_form" id="header_login_form" method="post" action="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php" onsubmit="return fnCheckLogin('header_login_form')">
<input type="hidden" name="mode" value="login" />
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="url" value="<!--{$smarty.server.PHP_SELF|h}-->" />


<!--{if $tpl_login}-->
<ul class="headerMenu">
<li class="mypage"><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php" onclick="fnFormModeSubmit('header_login_form', 'logout', '', ''); return false;"><img src="<!--{$TPL_URLPATH}-->img/common/key_sp.png" alt="ショッピングカート"><span class="caption">ログアウト</span></a></li>


<li class="ecBtn_cart"><a href="<!--{$smarty.const.CART_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/common/cart_small.png" alt="ショッピングカート"><span class="cartValue"><!--{$tpl_cart_quantity_total|number_format|default:0}--></span></a></li>
</ul>
<!-- .ecBtn_cart -->
<!--{else}-->
<ul class="headerMenu">
<li class="mypage"><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php"><img src="<!--{$TPL_URLPATH}-->img/common/key_sp.png" alt="ログイン"><span class="caption">ログイン</span></a></li>
<li class="ecBtn_cart"><a href="<!--{$smarty.const.CART_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/common/cart_small.png" alt="ショッピングカート"><span class="cartValue"><!--{$tpl_cart_quantity_total|number_format|default:0}--></span></a></li>
</ul>
<!-- .ecBtn_cart -->
<!--{/if}-->

</form>

</div>
<div class="searchBox">
  <form name="search_form" id="search_form" method="get" action="/products/list.php">
  <input type="text" class="searchBox" name="name" maxlength="50" value="" placeholder="キーワードを入力してね！">
  <input type="image" class="searchBtn" src="/user_data/packages/shinchan/img/button/btn_bloc_search.jpg" alt="検索" name="search">
  </form>
</div>


</header>

<script type="text/javascript">
$(".js-offcanvas").hiraku({
btn: ".js-offcanvas-btn",
direction: "left",
breakpoint: 813
});
</script>
<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{strip}-->
    <header class="global_header clearfix">
        <div id="logo_area">
            <a rel="external" href="<!--{$smarty.const.TOP_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/header/logo.png" width="150" height="25" alt="<!--{$arrSiteInfo.shop_name|h}-->" /></a>
        </div>
        <div class="header_utility">
            <!--{* ▼HeaderInternal COLUMN *}-->
            <!--{if !empty($arrPageLayout.HeaderInternalNavi)}-->
                <!--{foreach key=HeaderInternalNaviKey item=HeaderInternalNaviItem from=$arrPageLayout.HeaderInternalNavi}-->
                    <!-- ▼<!--{$HeaderInternalNaviItem.bloc_name}--> -->
                    <!--{if $HeaderInternalNaviItem.php_path != ""}-->
                        <!--{include_php_ex file=$HeaderInternalNaviItem.php_path items=$HeaderInternalNaviItem}-->
                    <!--{else}-->
                        <!--{include file=$HeaderInternalNaviItem.tpl_path items=$HeaderInternalNaviItem}-->
                    <!--{/if}-->
                    <!-- ▲<!--{$HeaderInternalNaviItem.bloc_name}--> -->
                <!--{/foreach}-->
            <!--{/if}-->
            <!--{* ▲HeaderInternal COLUMN *}-->
        </div>
    </header>
<!--{/strip}-->
