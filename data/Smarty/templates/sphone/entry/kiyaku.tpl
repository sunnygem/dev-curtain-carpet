<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->


<article class="contents_page product delivery review_agreement">
	<h1 class="ttl_01">会員規約</h1>


	<div class="contents_link_02">
		<div class="contents_link_02_box">
			<h2>第1条 会員規約の適用範囲</h2>
			<p>この会員規約（以下「本規約」といいます。）は、株式会社スモール・プラネット（以下「当社」といいます）が「クレヨンしんちゃん公式オンラインショップ」（以下「本ショップ」といいます。）において販売する商品を購入又は提供するサービスを利用しようとするすべての利用者（以下「利用者」といいます。）及び第2条所定の会員（以下「会員」といいます。）に適用するものとします。なお､本ショップにおいて販売する商品又は提供するサービスを総称して、以下サービス等といいます。</p>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box">
			<h2>第2条 利用者・会員の定義</h2>
			<ol>
				<li>本規約において「利用者」とは、当社が利用者を特定できるか否かに関係なく、当社が本ショップにおいて提供するサービス等を利用しようとするすべての人をいうものとします。</li>
				<li>本規約において「会員」とは、本ショップが提供するサービス等を受けることのできる者として、当社が会員登録を認めた人をいうものとします。</li>
			</ol>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box">
			<h2>第3条 本規約の変更</h2>
			<ol>
				<li>当社は、利用者及び会員の承諾を得ることなく、本規約を変更することがございます。</li>
				<li>変更後の本規約は、別途定める場合を除き、利用者及び会員が本ショップ上で変更後の本規約を認識することが可能となった時点で効力を生じるものとし、これ以後の利用は、全て変更後の本規約が適用されるものとします。</li>
			</ol>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box">
			<h2>第4条 会員に対する通知</h2>
			<ol>
				<li>当社は、本規約に別段の定めのある場合を除き、会員に対して必要な事項の通知を行なうときは、会員が予め当社に通知したアドレス宛の電子メールの送信、本ショップ上での表示、若しくはこれに代わる当社が適当と判断する方法により、随時これを行なうものとします。</li>
				<li>前項の通知は、当社が当該通知の内容を本ショップ上に表示又はこれに代わる方法により会員に通知した時点より効力を有するものとします。</li>
			</ol>
		</div>
		<!--/.contents_link_02_box-->

		<p class="text_36">※規約が続きます</p>
	</div>
	<!--/.contents_link_02-->

<div class="entry">

    <div class="btn_area btn2">
        <ul>
            <li class="return"><a href="<!--{$smarty.const.TOP_URL}-->" class="btn_back" rel="external">同意しない</a></li>
            <li class="entry"><a href="<!--{$smarty.const.ENTRY_URL}-->" class="btn" rel="external">同意して会員登録へ</a></li>
        </ul>
    </div>

</article>
<!--/.contents_page-->





<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->
<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<section id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="information">
        <p><span class="attention">【重要】 会員登録をされる前に、下記ご利用規約をよくお読みください。</span></p>
        <p>規約には、本サービスを使用するに当たってのあなたの権利と義務が規定されております。<br />
            「同意して会員登録へ」ボタン をクリックすると、あなたが本規約の全ての条件に同意したことになります。</p>
    </div>

    <div class="btn_area">
        <ul>
            <li><a href="<!--{$smarty.const.ENTRY_URL}-->" class="btn" rel="external">同意して会員登録へ</a></li>
            <li><a href="<!--{$smarty.const.TOP_URL}-->" class="btn_back" rel="external">同意しない</a></li>
        </ul>
    </div>

    <div id="kiyaku_text"><!--{$tpl_kiyaku_text|nl2br}--></div>

    <div class="btn_area">
        <ul class="btn_btm">
            <li><a href="<!--{$smarty.const.ENTRY_URL}-->" class="btn" rel="external">同意して会員登録へ</a></li>
            <li><a href="<!--{$smarty.const.TOP_URL}-->" class="btn_back" rel="external">同意しない</a></li>
        </ul>
    </div>
</section>

<!--{include file= 'frontparts/search_area.tpl'}-->
