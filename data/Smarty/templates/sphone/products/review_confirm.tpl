<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/common.css" type="text/css" media="all" />
<!--{include file="`$smarty.const.SMARTPHONE_TEMPLATE_REALDIR`popup_header.tpl" subtitle="お客様の声書き込み（確認ページ）"}-->
<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<div id="window_area">
    <h2 class="title">お客様の声書き込み</h2>
    <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="complete" />
        <!--{foreach from=$arrForm key=key item=item}-->
            <!--{if $key ne "mode"}-->
                <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->" />
            <!--{/if}-->
        <!--{/foreach}-->

        <dl summary="お客様の声書き込み" class="entry">
                <dt>商品名</dt>
                <dd><!--{$arrForm.name|h}--></dd>
                <dt>投稿者名</dt>
                <dd><!--{$arrForm.reviewer_name|h}--></dd>
                <dt>投稿者URL</dt>
                <dd><!--{$arrForm.reviewer_url|h}--></dd>
                <dt>性別</dt>
                <dd><!--{if $arrForm.sex eq 1}-->男性<!--{elseif $arrForm.sex eq 2}-->女性<!--{/if}--></dd>
                <dt>おすすめレベル</dt>
                <dd><span class="recommend_level"><!--{$arrRECOMMEND[$arrForm.recommend_level]}--></span></dd>
                <dt>タイトル</dt>
                <dd><!--{$arrForm.title|h}--></dd>
                <dt>コメント</dt>
                <dd><!--{$arrForm.comment|h|nl2br}--></dd>
        </dl>
        <div class="btn_area btn2">
            <ul>
                <li class="return"><input type="submit" onclick="mode.value='return';" src="<!--{$TPL_URLPATH}-->img/button/btn_back.jpg" alt="戻る" name="back" id="back" value="戻る" /></li>
                <li class="entry"><input type="submit" src="<!--{$TPL_URLPATH}-->img/button/btn_complete.jpg" alt="送信" name="send" id="send" value="送信" /></li>
            </ul>
        </div>
    </form>
</div>

<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_footer.tpl"}-->
