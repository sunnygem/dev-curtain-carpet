<!--{*
 *
 * This file is part of the SmartRecommend
 *
 * Copyright (C) 2017 bitmop, Inc.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *}-->

<!--▼ SmartRecommend -->
<!--{if count($arrSmartRecommendItems) > 0}-->
    <link rel="stylesheet" type="text/css" href="<!--{$smarty.const.PLUGIN_HTML_URLPATH}-->SmartRecommend/media/sp/smartrecommend.css">
    <section id="SmartRecommend_area" class="mainImageInit">
        <!--{if $block_display_title_flg == 1}--><h2><!--{$block_display_title}--></h2><!--{/if}-->
        <!--▼ スマートリコメンドによるオススメ商品一覧 -->
        <div class="SmartRecommend_area clearfix">
            <!--{foreach from=$arrSmartRecommendItems item=key}-->
                <!--▼ 商品 -->
                <div class="SmartRecommendItemBox">
                    <a rel="external" href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$key.product_id|u}-->"><img class="photoL" src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$key.main_list_image|sfNoImageMainList|h}-->" style="max-width: 80px;max-height: 80px;" alt="<!--{$key.name|h}-->" /></a>
                    <div class="cartinContents">
                        <div style="padding-top: 10px;">
                            <h3>
                                <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$key.product_id|u}-->"><!--{$key.name|h}--></a>
                            </h3>
                            <p>
                                <!--{if $key.classcategory_name != ""}--><!--{$key.classcategory_name|h}--><!--{if $key.parent_classcategory_name != ""}--> / <!--{$key.parent_classcategory_name|h}--><!--{/if}--><!--{/if}-->
                            </p>
                            <div class="productContents">

                                <p class="sale_price">
                                    <!--{$smarty.const.SALE_PRICE_TITLE}-->(税込)： <span class="price">
                                    <!--{if $key.price02_min_inctax == $key.price02_max_inctax}-->
                                        <!--{$key.price02_min_inctax|number_format}-->
                                    <!--{else}-->
                                        <!--{$key.price02_min_inctax|number_format}-->～<!--{$key.price02_max_inctax|number_format}-->
                                    <!--{/if}-->
                                円</span>
                                </p>
                                <p class="mini comment"><!--{$key.comment|h|nl2br}--></p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!--▲ 商品 -->
            <!--{/foreach}-->
        </div>
        <!--▲ スマートリコメンドによるオススメ商品一覧ここまで -->
    </section>
<!--{/if}-->
<!--▲ SmartRecommend -->
