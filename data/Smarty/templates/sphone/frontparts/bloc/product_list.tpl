<!--{strip}-->
<!--{if count($arrProducts) > 0}-->
<div class="block_outer clearfix">
<div class="topListArea special">
<div class="heading">
<img src="<!--{$TPL_URLPATH}-->img/top/special.png" alt="" class="title_icon" /><h2><span class="title">特集・キャンペーン</span><span class="title_rb">SPECIAL・CAMPAIGN</span></h2>
</div>
<!-- .heading -->
<img src="<!--{$TPL_URLPATH}-->img/top/special_line.png" alt="" class="main_column_line" />

<div class="block_body clearfix">
                <!--{foreach from=$arrProducts item=arrProduct}-->
<div class="product_item clearfix">
<div class="productImage">
<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" />
</a>
</div>
<div class="productContents">
<h3>
<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->"><!--{$arrProduct.name|h}--></a>
</h3>
<p class="sale_price">
<span class="price"><!--{$arrProduct.price02_min|sfCalcIncTax:$arrInfo.tax:$arrInfo.tax_rule|number_format}-->円</span>
</p>
</div>
<!-- .productContents -->
</div>
<!--{/foreach}-->
</div>
<!-- .block_body -->
</div>
<!-- .topListArea -->
<div class="categoryBtn">
<a href="/products/list.php?category_id=17">特集・キャンペーン一覧</a>
</div>
<!-- .categoryBtn -->
</div>
<!-- .block_outer -->
<!--{/if}-->
<!--{/strip}-->
