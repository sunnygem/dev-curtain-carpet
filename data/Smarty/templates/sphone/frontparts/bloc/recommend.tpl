<!--{strip}-->
<!--{if count($arrBestProducts) > 0}-->
<div class="block_outer clearfix">
<div class="topListArea">
<div class="heading">
<img src="<!--{$TPL_URLPATH}-->img/top/recommend.png" alt="" class="title_icon" /><h2><span class="title">おすすめ商品</span><span class="title_rb">RECOMMENDED</span></h2>
</div>
<!-- .heading -->
<img src="<!--{$TPL_URLPATH}-->img/top/recommend_line.png" alt="" class="main_column_line" />

<div class="block_body clearfix">


<!--{foreach from=$arrBestProducts item=arrProduct name="recommend_products"}-->
<div class="product_item clearfix">
<div class="productImage">
<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" />
</a>
</div>
<div class="productContents">
<h3>
<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->"><!--{$arrProduct.name|h}--></a>
</h3>
<p class="sale_price">
<span class="price"><!--{$arrProduct.price02_min_inctax|n2s}--> 円</span>
</p>
</div>
<!-- .productContents -->
</div>
<!--{/foreach}-->

<!--
<div>
<div class="moreBtn">おすすめ商品をもっと見る</div>
</div>
-->

</div>
<!-- .block_body -->
</div>
<!-- .topListArea -->
</div>
<!-- .block_outer -->
<!--{/if}-->
<!--{/strip}-->

<script type='text/javascript'>
$(function () {
    $('.moreBtn').prevAll().hide();
    $('.moreBtn').click(function () {
        if ($(this).prevAll().is(':hidden')) {
            $(this).prevAll().slideDown();
            $(this).addClass('close');
        } else {
            $(this).prevAll().slideUp();
            $(this).removeClass('close');
        }
    });
});


$(function() {
    $('.product_item .productImage').matchHeight();
});

</script>
<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!-- ▼おすすめ商品 -->
<!--{if count($arrBestProducts) > 0}-->
    <section id="recommend_area" class="mainImageInit">
        <h2>おすすめ商品</h2>
        <ul>
            <!--{section name=cnt loop=$arrBestProducts}-->
                <li id="mainImage<!--{$smarty.section.cnt.index}-->">
                    <div class="recommendblock clearfix">
                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrBestProducts[cnt].main_list_image|sfNoImageMainList|h}-->" style="max-width: 80px;max-height: 80px;" alt="<!--{$arrBestProducts[cnt].name|h}-->" />
                        <div class="productContents">
                            <h3><a rel="external" href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrBestProducts[cnt].product_id|u}-->"><!--{$arrBestProducts[cnt].name|h}--></a></h3>
                            <p class="mini comment"><!--{$arrBestProducts[cnt].comment|h|nl2br}--></p>
                            <p class="sale_price">
                                <span class="mini"><!--{$smarty.const.SALE_PRICE_TITLE|h}-->(税込):</span><span class="price"><!--{$arrBestProducts[cnt].price02_min_inctax|n2s}--> 円</span>
                            </p>
                        </div>
                    </div>
                </li>
            <!--{/section}-->
        </ul>
    </section>
<!--{/if}-->
<!-- ▲おすすめ商品 -->

<script type="application/javascript">
    <!--//
    $(function(){
      $('#recommend_area ul').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
      });
    });
    //-->
</script>
