<ul class="categorymenu"><li><a>商品カテゴリ</a></li></ul>

<script type="text/javascript">//<![CDATA[
    $(function(){
        $('#category_area li.level1:last').css('border-bottom', 'none');
    });
//]]></script>

<script>
    var box = $('#topcolumn');
    $(function(){
        $('.categorymenu > li').on('click',function(){
            $('.block_outer.category > .block_body').slideToggle(500);
        });
    });
</script>
