<div class="sp_top_slider">
  <ul>
    <li><a href="#1"><img src="<!--{$TPL_URLPATH}-->img/slider/slider01.jpg"></a></li>
    <li><a href="#2"><img src="<!--{$TPL_URLPATH}-->img/slider/slider01.jpg"></a></li>
    <li><a href="#3"><img src="<!--{$TPL_URLPATH}-->img/slider/slider01.jpg"></a></li>
  </ul>
</div>
<!-- .wideslider -->

<script type='text/javascript'>
  $(document).ready(function() {
    $('.sp_top_slider > ul').slick({
      autoplay: true,
      autoplaySpeed: 1500,
      arrows: true,
      dots: true,
      prevArrow: '<div class="prevarrow"><img src="/user_data/packages/shinchan/img/slider/slider_arrow.png" alt=""></div>',
      nextArrow: '<div class="nextarrow"><img src="/user_data/packages/shinchan/img/slider/slider_arrow.png" alt=""></div>',
    });
  });
</script>
<img src="" alt="">
<style>
  .slick_slider {
    position: relative;
  }

  .prevarrow,
  .nextarrow {
    position: absolute;
    top: 0;
    bottom: 0;
    margin: auto;
    z-index: 1;
    width: 28px;
    height: 44px;
    background-color: rgba(255, 255, 255, .7);
    box-shadow: 0 0 8px rgba(0, 0, 0, 0.5);
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .prevarrow {
    left: 0;
    z-index: 1;
    transform: rotate(180deg);
  }

  .nextarrow {
    right: 0;
  }

  .slick-dots li.slick-active {
    background-color: none !important;
  }
</style>