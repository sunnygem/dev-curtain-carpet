<script>
    $(function(){
        var val = $('#select_search_taste').val();
        if ( val )
        {
            $('#search_taste').attr('name', val );
            $('#search_taste').val('1');
        }
        else
        {
            $('#search_taste').attr('name', '' );
            $('#search_taste').val('');
        }
        
        $('#select_search_taste').on('change',function(){
            var val = $(this).val();
            if ( val )
            {
                $('#search_taste').attr('name', val );
                $('#search_taste').val('1');
            }
            else
            {
                $('#search_taste').attr('name', '' );
                $('#search_taste').val('');
            }
        });
        $('#search_form_reset').on('click',function(){
            document.form1.reset();
            document.form2.reset();
        });
    });
</script>
<div class="page research">
    <div id="contents_wrapper">
        <!-- ▼タイトル -->
        <div class="ttl">
            <h2><span></span>こだわり検索</h2>
        </div>
        <!-- ▲タイトル -->
        <!-- ▼商品・キーワードで探す -->
        <div class="item_search">
            <h3 class="item_search__title">商品・キーワードで探す</h3>
            <form action="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?" method="get" name="form1">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="mode" value="<!--{$mode|h}-->" />
                <!--{* ▼検索条件 *}-->
                <input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" />
                <input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
                <input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" />
                <!--{* ▲検索条件 *}-->
                <!--{* ▼ページナビ関連 *}-->
                <input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
                <input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
                <input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
                <!--{* ▲ページナビ関連 *}-->
                <input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />
                <table class="item_search__tbl item_search__tbl_keyword">
                    <tbody>
                        <tr class="item_search__tbl__tr">
                            <th class="item_search__tbl__tr__th">キーワード検索</th>
                            <td class="item_search__tbl__tr__td">
                                <input class="item_search__tbl__tr__td_input" type="text" name="name" placeholder="入力してください">
                                <input class="item_search__tbl__tr__td_submit" type="submit" name="" value="検索">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <!-- ▲商品・キーワードで探す -->
        <!-- ▼カーテンを探す -->
        <div class="item_search">
            <h3 class="item_search__title">カーテンを探す</h3>
            <form action="/shop/products/list.php?" method="get" name="form2">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="mode" value="<!--{$mode|h}-->" />
                <!--{* ▼検索条件 *}-->
                <!-- input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" /-->
                <input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
                <!-- input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" / -->
                <!--{* ▲検索条件 *}-->
                <!--{* ▼ページナビ関連 *}-->
                <input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
                <input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
                <input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
                <input type="hidden" id="search_taste" name="" value="" />
                <input type="hidden" id="search_price" name="" value="" />
                <!--{* ▲ページナビ関連 *}-->
                <input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />
                <table class="item_search__tbl item_search__tbl_curtain">
                    <tbody>
                        <tr class="item_search__tbl__tr">
                            <th class="item_search__tbl__tr__th">種類</th>
                            <td class="item_search__tbl__tr__td">
                                <div class="item_search__tbl__tr__td__slc_wrap">
                                    <select class="item_search__tbl__tr__td__slc_wrap__slc" name="category_id">
                                        <option value="">選択してください</option>
                                        <option value="1792">ドレープカーテン</option>
                                        <option value="1793">レースカーテン</option>
                                        <option value="1794">既製カーテン</option>
                                        <option value="1795">オーダーカーテン</option>
                                        <option value="1796">イージーオーダーカーテン</option>
                                        <!--<option value="1797">横型ブラインド</option>-->
                                        <!--<option value="1798">縦型ブラインド</option>-->
                                       <!--<option value="1799">ロールスクリーン</option>-->
                                        <!--<option value="1800">プリーツスクリーン</option>-->
                                        <!--<option value="1801">アコーディオン間仕切り</option>-->
                                        <!--<option value="1802">カーテンレール、タッセル</option>-->
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr class="item_search__tbl__tr">
                            <th class="item_search__tbl__tr__th">カラー</th>
                            <td class="item_search__tbl__tr__td">
                            <ul>
                                <li>
                                    <input type="hidden" name="color_monotone" value="0" />
                                    <input type="checkbox" name="color_monotone" value="1" id="monotone" />
                                    <label for="monotone"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_monotone.png" alt="search_color_monotone">モノトーン系</label>
                                </li>
                                <li>
                                    <input type="hidden" name="color_brown" value="0" />
                                    <input type="checkbox" name="color_brown" value="1" id="brown" />
                                    <label for="brown"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_brown.png" alt="search_color_brown">ブラウン系</label>
                                </li>
                                <li>
                                    <input type="hidden" name="color_blue" value="0" />
                                    <input type="checkbox" name="color_blue" value="1" id="blue" />
                                    <label for="blue"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_blue.png" alt="search_color_blue">ブルー系</label>
                                </li>
                                <li>
                                    <input type="hidden" name="color_ivory" value="0" />
                                    <input type="checkbox" name="color_ivory" value="1" id="ivory" />
                                    <label for="ivory"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_ivory.png" alt="search_color_ivory">アイボリー系</label>
                                </li>
                                <li>
                                    <input type="hidden" name="color_yellow" value="0" />
                                    <input type="checkbox" name="color_yellow" value="1" id="yellow" />
                                    <label for="yellow"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_yellow.png" alt="search_color_yellow">イエロー系</label>
                                </li>
                                <li>
                                    <input type="hidden" name="color_red" value="0" />
                                    <input type="checkbox" name="color_red" value="1" id="red" />
                                    <label for="red"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_red.png" alt="search_color_red">レッド系</label>
                                </li>
                                <li>
                                    <input type="hidden" name="color_beige" value="0" />
                                    <input type="checkbox" name="color_beige" value="1" id="beige" />
                                    <label for="beige"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_beige.png" alt="search_color_beige">ベージュ系</label>
                                </li>
                                <li>
                                    <input type="hidden" name="color_green" value="0" />
                                    <input type="checkbox" name="color_green" value="1" id="green" />
                                    <label for="green"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_green.png" alt="search_color_green">グリーン系</label>
                                </li>
                                <li>
                                    <input type="hidden" name="color_gray" value="0" />
                                    <input type="checkbox" name="color_gray" value="1" id="gray" />
                                    <label for="gray"><img src="<!--{$TPL_URLPATH}-->img/search/search_color_gray.png" alt="search_color_gray">グレイ系</label>
                                </li>
                            </ul>
                            </td>
                        </tr>
                        <tr class="item_search__tbl__tr">
                            <th class="item_search__tbl__tr__th">テイスト</th>
                            <td class="item_search__tbl__tr__td">
                                <div class="item_search__tbl__tr__td__slc_wrap">
                                    <select class="item_search__tbl__tr__td__slc_wrap__slc" name="" id="select_search_taste">
                                        <option value="">選択してください</option>
                                        <option value="taste_modern">モダン</option>
                                        <option value="taste_natural">ナチュラル</option>
                                        <option value="taste_classic">クラシック</option>
                                        <option value="taste_elegance">エレガンス</option>
                                        <option value="taste_casual">カジュアル</option>
                                        <option value="taste_plain">無地</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr class="item_search__tbl__tr">
                            <th class="item_search__tbl__tr__th">機能</th>
                            <td class="item_search__tbl__tr__td status">
                                <ul>
                                    <li>
                                        <input type="hidden" name="function_shading" value="0" />
                                        <input type="checkbox" name="function_shading" value="1" id="shade" />
                                        <label for="shade">遮光</label>
                                    </li>
                                    <li>
                                        <input type="hidden" name="function_washable" value="0" />
                                        <input type="checkbox" name="function_washable" value="1" id="wash" />
                                        <label for="wash">洗える</label>
                                    </li>
                                    <li>
                                        <input type="hidden" name="function_privacy_measures" value="0" />
                                        <input type="checkbox" name="function_privacy_measures" value="1" id="privacy" />
                                        <label for="privacy">プライバシー対策</label>
                                    </li>
                                    <li>
                                        <input type="hidden" name="function_fire_retardant" value="0" />
                                        <input type="checkbox" name="function_fire_retardant" value="1" id="prevent_fire" />
                                        <label for="prevent_fire">防炎</label>
                                    </li>
                                    <li>
                                        <input type="hidden" name="function_energy_saving" value="0" />
                                        <input type="checkbox" name="function_energy_saving" value="1" id="energy_saving" />
                                        <label for="energy_saving">省エネ</label>
                                    </li>
                                    <li>
                                        <input type="hidden" name="function_soundproofing" value="0" />
                                        <input type="checkbox" name="function_soundproofing" value="1" id="prevent_sound" />
                                        <label for="prevent_sound">防音</label>
                                    </li>
                                    <li>
                                        <input type="hidden" name="function_shape_memory_processing" value="0" />
                                        <input type="checkbox" name="function_shape_memory_processing" value="1" id="shape_memory" />
                                        <label for="shape_memory">形状記憶</label>
                                    </li>
                                    <li>
                                        <input type="hidden" name="function_uv_cut" value="0" />
                                        <input type="checkbox" name="function_uv_cut" value="1" id="uv_cut" />
                                        <label for="uv_cut">UVカット</label>
                                    </li>
                                    <li>
                                        <input type="hidden" name="function_pollen_catch" value="0" />
                                        <input type="checkbox" name="function_pollen_catch" value="1" id="pollen_catch" />
                                        <label for="pollen_catch">花粉キャッチ</label>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr class="item_search__tbl__tr">
                            <th class="item_search__tbl__tr__th">価格ライン</th>
                            <td class="item_search__tbl__tr__td">
                                <div class="item_search__tbl__tr__td__slc_wrap">
                                    <select class="item_search__tbl__tr__td__slc_wrap__slc" name="search_price">
                                        <option value="">選択してください</option>
                                        <option value="1814">～10,000円</option>
                                        <option value="1815">～20,000円</option>
                                        <option value="1816">～30,000円</option>
                                        <option value="1817">～40,000円</option>
                                        <option value="1818">～50,000円</option>
                                        <option value="1819">～60,000円</option>
                                        <option value="1820">～70,000円</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="item_search__btn">
                    <div class="item_search__btn__item">
                        <button type="button" class="item_search__btn__item_btn" id="search_form_reset" name="">リセット</button>
                        <input class="item_search__btn__item_submit" type="submit" name="" value="検索">
                    </div>
                </div>
            </form>
        </div>
        <!-- ▲カーテンを探す -->
    </div>
</div>


