<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/view.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/publis.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/common.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/nw_header.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/lineup.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/lineup02.css" charset="UTF-8" />
<div id="mainArea" class="pbMainArea" style="max-width: 1000px;">
	<div id="area0" class="pbArea ">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock632515">
<div class="pbBlock pbBlockBase">
<div>
<h1>プリーツスクリーン</h1>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock632516">
		<div class="pbNested style2457">
			<div class="pbNested pbNestedWrapper" id="pbBlock632545">
		<div class="pbNested pbNestedHorizontalWrapper cjMB20">
					<div class="pbHorizontalNested" id="pbBlock632547" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv632547" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div class="cjMR20">
<div class="style2441">
<p><big><big><strong>和にも調和する、優しい採光</strong></big></big></p>
<p><br></p>
<div class="style2441">
<div class="style2441">
<p>障子のようなやわかな光を採り入れるプリーツスクリーン。繊細なプリーツがブラインドのようなシャープさを感じさせます。採光とプライバシーを1台で叶える「ペアタイプ」、スリット窓に最適な「小窓」タイプもあります。</p>
<p>&nbsp;</p>
<p>下記でご紹介している商品の詳細、価格はお近くの店舗へお尋ねください。</p>
</div>
</div>
</div>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock632546" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv632546" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<p><img alt="プリーツスクリーン.jpg" src="/shop/user_data/packages/CJoukoku/img/testlp/pscreen/201441175838.jpg"></p>
<p>ダイバー&nbsp;</p>
</div>
</div>
</div>

					</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock632548">
		<div class="pbNested pbNestedHorizontalWrapper cjMB20">
					<div class="pbHorizontalNested" id="pbBlock632550" style="overflow:hidden;clear:none;">
					
					<div id="pbNestDiv632550" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div class="cjMB6-2288">
<p><img src="/var/rev0/0001/7094/201432717424.jpg" alt="プリーツスクリーン和室.jpg" style="border: 0px solid;"></p>
<p>アスカ＜和＞プリーツスクリーン</p>
<p>&nbsp;</p>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock632549" style="overflow:hidden;clear:none;">
					
					<div id="pbNestDiv632549" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<p><img style="border: 0px solid;" alt="プリーツスクリーン.jpg" src="/shop/user_data/packages/CJoukoku/img/testlp/pscreen/20143271773.jpg"></p>
<p>シズク ＜和＞プリーツスクリーン</p>
</div>
</div>
</div>

					</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock632526">
		<div class="pbNested pbNestedHorizontalWrapper style2477">
					<div class="pbHorizontalNested" id="pbBlock632528" style="overflow:hidden;clear:none;">
					
					<div id="pbNestDiv632528" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="cjMB6-2288">
<p><img style="border: 0px solid;" alt="プリーツスクリーンピンク.jpg" src="/shop/user_data/packages/CJoukoku/img/testlp/pscreen/201432717146.jpg"></p>
<p>ジャスティ&nbsp;&lt;プレーンプリーツスクリーン＞</p>
</div>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock632527" style="overflow:hidden;clear:none;">
					
					<div id="pbNestDiv632527" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<p style="text-align: left;"><img style="border: 0px solid;" alt="遮光プリーツスクリーン.jpg" src="/shop/user_data/packages/CJoukoku/img/testlp/pscreen/201432717544.jpg"></p>
<p style="text-align: left;">セコイア&nbsp;＜遮光＞プリーツスクリーン</p>
</div>
</div>
</div>

					</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock632537">
		<div class="pbNested style2432-2485">
			<div class="pbNested pbNestedWrapper" id="pbBlock632538">
<div class="pbBlock pbBlockBase">
<div>
<div class="cjMB20"><span style="text-decoration:underline;"><span style="color: #ff0000;"><big><big><strong>＜Pick Up＞</strong></big></big></span><big><strong>採光とプライバシーを両立させたプリーツスクリーン</strong></big></span></div>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock632554">
		<div class="pbNested pbNestedHorizontalWrapper ">
					<div class="pbHorizontalNested" id="pbBlock632556" style="overflow:hidden;clear:none;">
					
					<div id="pbNestDiv632556" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div><img style="border-width: 0px; border-style: solid;" alt="preets.jpg" src="/shop/user_data/packages/CJoukoku/img/testlp/pscreen/preets.jpg"></div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock632555" style="overflow:hidden;clear:none;">
					
					<div id="pbNestDiv632555" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<p><strong>ドレープとレースを1台にした「ペアタイプ」</strong><br></p>
<p>1枚生地のスタンダードな「標準タイプ」の他に、2種類の生地を1台にしたコンパクトで機能的な「ペアタイプ」をご用意しました。「ペアタイプ」なら採光とプライバシーを上手に組み合わせることができ、同じ生地や薄手と薄手などの組み合わせも可能です。&nbsp;</p>
</div>
</div>
</div>

					</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock632551">
		<div class="pbNested pbNestedHorizontalWrapper style2468">
					<div class="pbHorizontalNested" id="pbBlock632552" style="overflow:hidden;clear:none;">
					
					<div id="pbNestDiv632552" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<p><img style="border: 1px solid;" alt="preets1.jpg" src="/shop/user_data/packages/CJoukoku/img/testlp/pscreen/preets1.jpg"></p>
<p>戸建てやマンションの1階の外からの視線をブロックしたいとき<br></p>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock632553" style="overflow:hidden;clear:none;">
					
					<div id="pbNestDiv632553" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<p><img style="border: 1px solid;" alt="preets2.jpg" src="/shop/user_data/packages/CJoukoku/img/testlp/pscreen/preets2.jpg"></p>
<p>上からの視線が気になるとき<br></p>
</div>
</div>
</div>

					</div>
		</div>

			</div>
		</div>

			</div>
		</div>

			</div>
		</div>
	</div>

</div>