<!-- cloth-sample -->
</div>
<!-- #main_column -->
<div class="cloth-sample">
<div class="view">
<h2><img src="<!--{$TPL_URLPATH}-->img/cloth-sample/mainView.jpg" alt="カーテンの生地サンプルお送りします。" /></h2>
</div>
<!-- .view -->
<div class="inner">
<div class="paragraph">
<p>カーテンの生地サンプルは10枚まで無料で送付させていただきます。<br>
「オンラインストアの画面だけではイメージしにくい」「実際の生地を手に取ってみて、お部屋に合わせてみたい」など、お気軽にご利用ください。お申込みから約1週間でお届けとなります。<br>
※一部商品は対象外となります。<br>
<br>


</p>
</div>
<!-- .paragraph -->
</div>
<!-- .inner -->

<main>

<section class="first">
<div class="inner">
<div class="bg-line">
<h3>生地サンプルのお申込み方法</h3>
</div>
<!-- .bg-line -->
</div>
<!-- .inner -->
</section>

<section class="step01">
<div class="inner">
<div class="content">

<div class="left">
<h3>STEP1:</h3>
<p class="heading">ご希望の生地の品名（カーテンの品名）をご確認ください。</p>
<p class="img"><img src="<!--{$TPL_URLPATH}-->img/cloth-sample/step1Img.jpg" alt="カーテンの品名" /></p>
<p class="bottom">カーテンの品名は商品詳細ページの右上部に記載されています｡<br>
ご希望の生地を最大10種類お選びいただき､自宅でご確認することが出来ます｡<br>
生地の欠品等で送付出来ない場合がございます。あらかじめご了承ください。
</p>
</div>
<!-- .left -->
<!-- .right
<div class="right">
<h3 class="color">※対象外商品</h3>
<div class="lane">
<div class="product01">
<h4>オーダー<br>厚地</h4>
<ul>
<li>SD27155</li>
<li>FA21326</li>
<li>YB22322</li>
<li>YB22323</li>
<li>YB22324</li>
<li>YB22325</li>
<li>YB22326</li>
<li>IH26301</li>
<li>IH26302</li>
<li>IH26303</li>
<li>IH26304</li>
<li>IH26305</li>
<li>IH26306</li>
<li>IH26307</li>
<li>IH26308</li>
<li>IH26309</li>
<li>IH26310</li>
<li>IH26311</li>
<li>IH26312</li>
</ul>
</div>
</div>
<div class="lane">
<div class="product02">
<h4>オーダー<br>レース</h4>
<ul>
<li>SC17253</li>
<li>SC17328</li>
<li>IH14301</li>
<li>IH14302</li>
<li>IH14303</li>
<li>IH14304</li>
<li>IH14305</li>
</ul>
</div>
<div class="product03">
<h4>既製<br>レース</h4>
<ul>
<li>フォルマⅣ</li>
<li>アブソルートⅣ</li>
<li>シャロットⅣ</li>
<li>モントルⅣ</li>
<li>アレーGN</li>
</ul>
</div>
</div>
<div class="lane">
<div class="product04">
<h4>既製<br>厚地</h4>
<ul>
<li>リヨンPI</li>
<li>リヨンGN</li>
<li>アゼリアPI</li>
<li>コルサⅡBK</li>
<li>ディラⅣ</li>
<li>ディラGN</li>
<li>レサンⅣ</li>
<li>レサンBR</li>
<li>タリスⅣ</li>
<li>タリスBK</li>
<li>アンティコPI</li>
<li>アンティコⅣ</li>
<li>ミンスクNV</li>
<li>ミンスクGRY</li>
<li>セシルⅣ</li>
<li>セシルOR</li>
<li>パルフェⅣ</li>
<li>パルフェGN</li>
<li>ジェナRO</li>
<li>ジェナBL</li>
</ul>
</div>
</div>
</div>
-->
<div style="clear:both;"></div>
</div>
<!-- .content -->
</div>
<!-- .inner -->
</section>
<!-- .step01 -->

<section class="step02">
<div class="inner">
<h3>STEP2:</h3>
<p>生地サンプルお申込みフォームよりお申込みください。<br>ご希望のカーテンの品名をお申込みフォームの生地サンプルの品名記入欄にご記入ください。</p>
</div>
<!-- .inner -->
</section>
<!-- .step02 -->

<section>
<div class="inner">
<div class="btn">
<a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->sample/">無料生地サンプルを申し込む</a>
</div>
<!-- .btn -->
</div>
<!-- .inner -->
</section>

</main>
</div>
<!-- .cloth-sample -->
