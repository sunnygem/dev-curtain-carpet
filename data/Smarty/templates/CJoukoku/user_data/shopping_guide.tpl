<div class="page guide">
    <div id="contents_wrapper">
        <!-- ▼タイトル -->
        <div class="ttl">
            <h2>ショッピングガイド</h2>
        </div>
        <p class="main_column_title_txt__detail">カーテンじゅうたん王国の商品はオンラインショップでいつでもお買い求めいただくことが出来ます。オンラインショップの会員に登録いただくと、メルマガにてお得な情報や特集、お店でのイベント・キャンペーン情報などをいち早くお伝えいたします。ぜひ、この機会にカーテンじゅうたん王国オンラインショップをご利用ください。</p>
        <!-- ▲タイトル -->
        <!-- ▼ショッピングガイド -->
        <div class="guide">
            <!-- ▼商品選びに困ったら -->
            <div class="guide__help">

                <div class="guide__help__parent">
                    <div class="guide__help__parent__txt">
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/advice.php" class="display_b">
                            <div class="guide__help__parent__txt__title">
                                <div class="guide__help__parent__txt__title_photo">
                                    <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu01.png" alt="">
                                </div>
                                <h3 class="guide__help__parent__txt__title_txt">商品選びに困ったら<span class="guide__help__parent__txt__title_txt__arrow"></span></h3>
                            </div>
                        </a>
                        <p class="guide__help__parent__txt__detail">インテリアを選ぶ上でまず、家の窓周辺の環境はどうなっているかを確認することは大切です。どの商品を選んだらいいか分からない方にむけて、ここではカーテンじゅうたん王国でご相談いただく代表的なニーズや選び方をご紹介します。</p>
                    </div>
                    <div class="guide__help__parent__photo">
                        <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu01_img.jpg" alt="">
                    </div>
                </div>

                <div class="guide__help__child">
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/features.php" class="display_b">
                        <div class="guide__help__child__wrap">
                            <div class="guide__help__child__wrap__photo">
                                <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu01_sub01.png" alt="">
                            </div>
                            <div class="guide__help__child__wrap__txt">
                                <h4 class="guide__help__child__wrap__txt__title">カーテンの機能や特徴<span class="guide__help__arrow"></span></h4>
                                <p class="guide__help__child__wrap__txt__detail">カーテンじゅうたん王国のカーテンは見た目の美しさやクオリティはもちろん、機能も充実しています。こだわりの機能や特徴をご紹介します。</p>
                            </div>
                        </div>
                    </a>
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/select.php" class="display_b">
                        <div class="guide__help__child__wrap">
                            <div class="guide__help__child__wrap__photo">
                                <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu01_sub02.png" alt="">
                            </div>
                            <div class="guide__help__child__wrap__txt">
                                <h4 class="guide__help__child__wrap__txt__title">商品の選び方<span class="guide__help__arrow"></span></h4>
                                <p class="guide__help__child__wrap__txt__detail">カーテンじゅうたん王国はカーテンだけでなく、ブラインドやカーペットも用意しております。ポイントを押さえておけば商品選びがさらにスムーズに。カーテン、ブラインド、カーペットの商品選びのポイントをご紹介します。</p>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <!-- ▲商品選びに困ったら -->

            <!-- ▼採寸方法 -->
            <div class="guide__help">
                <div class="guide__help__parent">
                    <div class="guide__help__parent__txt">
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/measurements.php" class="display_b">
                            <div class="guide__help__parent__txt__title">
                                <div class="guide__help__parent__txt__title_photo">
                                    <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu02.png" alt="">
                                </div>
                                <h3 class="guide__help__parent__txt__title_txt">採寸方法<span></span></h3>
                            </div>
                        </a>
                        <p class="guide__help__parent__txt__detail">お部屋の窓にピッタリなカーテンのサイズを測りましょう。<br>お客様が測られたサイズを元に、カーテンが美しく見えるようお仕立ていたします。</p>
                    </div>

                    <div class="guide__help__parent__photo">
                        <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu02_img.jpg" alt="">
                    </div>
                </div>
            </div>
            <!-- ▲採寸方法 -->

            <!-- ▼お手入れ方法 -->
            <div class="guide__help">
                <div class="guide__help__parent">
                    <div class="guide__help__parent__txt">
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/care.php" class="display_b">
                            <div class="guide__help__parent__txt__title">
                                <div class="guide__help__parent__txt__title_photo">
                                    <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu03.png" alt="">
                                </div>
                                <h3 class="guide__help__parent__txt__title_txt">お手入れ方法<span></span></h3>
                            </div>
                        </a>
                        <p class="guide__help__parent__txt__detail">カーテンのお手入れ(普段のお手入れ)やご家庭でカーテンを洗濯する際の注意点をご案内いたします。</p>
                    </div>

                    <div class="guide__help__parent__photo">
                        <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu03_img.jpg" alt="">
                    </div>
                </div>
            </div>
            <!-- ▲お手入れ方法 -->

            <!-- ▼商品をご注文いただくときに -->
            <div class="guide__help">
                <div class="guide__help__parent">
                    <div class="guide__help__parent__txt">
                        <div class="guide__help__parent__txt__title">
                            <div class="guide__help__parent__txt__title_photo">
                                <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu04.png" alt="">
                            </div>
                                <h3 class="guide__help__parent__txt__title_txt">商品をご注文いただくときに<span></span></h3>
                        </div>
                        <p class="guide__help__parent__txt__detail">カーテンじゅうたん王国オンラインショップの注文手順やご利用方法についてご案内いたします。</p>
                    </div>
                </div>

                <div class="guide__help__child">
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/order.php" class="display_b">
                        <div class="guide__help__child__wrap">
                            <div class="guide__help__child__wrap__photo">
                                <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu04_sub01.png" alt="">
                            </div>
                            <div class="guide__help__child__wrap__txt">
                                <h4 class="guide__help__child__wrap__txt__title">ご注文からお届けまでの流れ<span class="guide__help__arrow"></span></h4>
                                <p class="guide__help__child__wrap__txt__detail">商品のご注文からお届けまで、ご注文いただいた際の一通りの流れをご案内いたします。</p>
                            </div>
                        </div>
                    </a>
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/flow.php" class="display_b">
                        <div class="guide__help__child__wrap">
                            <div class="guide__help__child__wrap__photo">
                                <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu04_sub02.png" alt="">
                            </div>
                            <div class="guide__help__child__wrap__txt">
                                <h4 class="guide__help__child__wrap__txt__title">ご購入までの流れ<span class="guide__help__arrow"></span></h4>
                                <p class="guide__help__child__wrap__txt__detail">オンラインショップでの商品のご購入方法をご案内いたします。</p>
                            </div>
                        </div>
                    </a>
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php" class="display_b">
                        <div class="guide__help__child__wrap">
                            <div class="guide__help__child__wrap__photo">
                                <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu04_sub03.png" alt="">
                            </div>
                            <div class="guide__help__child__wrap__txt">
                                <h4 class="guide__help__child__wrap__txt__title">送料・お支払いについて</h4>
                                <p class="guide__help__child__wrap__txt__detail">配送・送料やお支払い方法、返品・交換についてご案内いたします。</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- ▲商品をご注文いただくときに -->

            <!-- ▼よくあるご質問 -->
            <div class="guide__help">
                <div class="guide__help__parent">
                    <div class="guide__help__parent__txt">
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/faq.php" class="display_b">
                            <div class="guide__help__parent__txt__title">
                                <div class="guide__help__parent__txt__title_photo">
                                    <img src="<!--{$TPL_URLPATH}-->img/guide/guide_menu05.png" alt="">
                                </div>
                                <h3 class="guide__help__parent__txt__title_txt">よくあるご質問<span class="guide__help__arrow"></span></h3>
                            </div>
                        </a>
                        <p class="guide__help__parent__txt__detail">お客様からよくいただくご質問を紹介しております。ご不明点がございましたら、お気軽にお問合わせください。</p>
                    </div>
                </div>
                <div class="guide__help__contact_btn">
                    <div class="guide__help__contact_btn__item">
                        <button class="guide__help__contact_btn__item_btn" name="" onClick="location.href='<!--{$smarty.const.ROOT_URLPATH}-->contact'"><span class="guide__help__arrow contact"></span>お問い合わせ</button>
                    </div>
                </div>
            </div>
            <!-- ▼よくあるご質問 -->

        </div>
    </div>
</div>
