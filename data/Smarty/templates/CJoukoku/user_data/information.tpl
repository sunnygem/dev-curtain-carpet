<style>
.informationInner2 li { vertical-align: top; }
</style>

<div class="page-body">
		<div class="inner info-area">
			<h2>インフォメーション</h2>
			
			<div class="img100"><img src="<!--{$TPL_URLPATH}-->img/common/info_title.png"></div>
			
			<section class="information2">
	<div class="informationInner2">
	<h3><span>INFORMATION</span></h3>
    	<ul class="clearfix">
        	<li><a href="/shop/user_data/measure-curtain.php" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/001.png" data-original="i<!--{$TPL_URLPATH}-->img/information/001.png" alt="" class="lazy"></dt>
                    <dd><h3>採寸方法<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>カーテンの採寸方法を動画など</dd>
                </dl>
            </li>
        	<li><a href="/shop/user_data/care.php" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/002.png" data-original="<!--{$TPL_URLPATH}-->img/information/002.png" alt="" class="lazy"></dt>
                    <dd><h3>お手入れ方法<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>カーテンの洗濯方法など</dd>
                </dl>
            </li>
<!--        	<li><a href="#" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/003.png" data-original="<!--{$TPL_URLPATH}-->img/information/003.png" alt="" class="lazy"></dt>
                    <dd><h3>採寸･取付サービス<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>採寸や取付工事もお任せください。</dd>
                </dl>
            </li>
        	<li><a href="#" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/004.png" data-original="<!--{$TPL_URLPATH}-->img/information/004.png" alt="" class="lazy"></dt>
                    <dd><h3>商品について<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>カーテンやラグなどをお取り扱いしています。</dd>
                </dl>
            </li>
-->
        	<li><a href="/shop/user_data/order.php" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/005.png" data-original="<!--{$TPL_URLPATH}-->img/information/005.png" alt="" class="lazy"></dt>
                    <dd><h3>購入方法<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>オーダーと既製の違い：当店ではオーダーカーテンを2倍ヒダ、既製カーテンを1.5倍ヒダでお作りしています。</dd>
                </dl>
            </li>
        	<li><a href="/shop/user_data/flow.php" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/006.png" data-original="<!--{$TPL_URLPATH}-->img/information/006.png" alt="" class="lazy"></dt>
                    <dd><h3>お届けまでの流れ<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>ご注文からお客様のお手元に届くまで。
</dd>
                </dl>
            </li>
        	<li><a href="/reserve/" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/007.png" data-original="<!--{$TPL_URLPATH}-->img/information/007.png" alt="" class="lazy"></dt>
                    <dd><h3>店舗予約について<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>カーテンのお見積りの際は来店予約をご利用いただくと便利です。
</dd>
                </dl>
            </li>
        	<li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpp-service/" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/008.png" data-original="<!--{$TPL_URLPATH}-->img/information/008.png" alt="" class="lazy"></dt>
                    <dd><h3>店舗サービス<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>商品のコーディネートのご相談やお見積りなど、専任のスタッフにお気軽にご相談ください。
</dd>
                </dl>
            </li>
 <!--       	<li><a href="#" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/009.png" data-original="<!--{$TPL_URLPATH}-->img/information/009.png" alt="" class="lazy"></dt>
                    <dd><h3>生地サンプル<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>一部商品を除き、カーテンの生地サンプルの送付サービスを行っております。
</dd>
                </dl>
            </li>
        	<li><a href="#" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/010.png" data-original="<!--{$TPL_URLPATH}-->img/information/010.png" alt="" class="lazy"></dt>
                    <dd><h3>オーダーと既成の違い<img src="<!--{$TPL_URLPATH}-->/img/common/c_contact_arrow_03.png"></h3></dd>
					<dd>ああああああああああああああああああああああああああああああああ</dd>
                </dl>
            </li>
-->
        </ul>
    </div>
    </section>
			
			<div class="btn-area">
				
				<a href="/shop/user_data/shopping_guide.php"><span class="ishop"><img src="<!--{$TPL_URLPATH}-->img/icon/s_guide_icon.png"></span>ショッピングガイド</a>
				<a href="/shop/user_data/faq.php"><span class="ifaq"><img src="<!--{$TPL_URLPATH}-->img/icon/que_icon02.png"></span>よくある質問</a>
				
				
			</div>
	
		
		</div><!-- page-body inner -->
	</div><!-- page-body -->
