<div class="page flow">
    <div class="ttl">
        <h2><span></span>ご購入までの流れ</h2>
    </div>
			
    <div class="section">
        <div class="step">
            <ul>
                <li>
                    <a href="#step1">
                        <div class="step_no"><span>STEP</span>1</div>
                        <div class="txt">商品を選ぶ</div>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="#step2">
                        <div class="step_no"><span>STEP</span>2</div>
                        <div class="txt">商品をカートに<br>入れる</div>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="#step3">
                        <div class="step_no"><span>STEP</span>3</div>
                        <div class="txt">お客様情報の<br>入力</div>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="#step4">
                        <div class="step_no"><span>STEP</span>4</div>
                        <div class="txt">お支払方法・<br>送付先の入力</div>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="#step5">
                        <div class="step_no"><span>STEP</span>5</div>
                        <div class="txt">注文完了</div>
                        <span class="arrow"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!--<div class="img02"><img src="<!--{$TPL_URLPATH}-->img/common/f19_img_16.png"></div>-->
			


    <div class="section" id="step1">
        <h3><span></span>商品を探して選んでください</h3>
        <div class="flex">
            <div class="txt">
                <h4>具体的に商品が決まっている場合</h4>
                <p>
                検索窓やこだわり検索から条件を入力して探してください｡
                検索結果一覧が表示されます｡
                </p>

                <h4>カテゴリから探す場合</h4>
                <p>
                ナビゲーションから探してください。
                </p>
            </div>
        </div>
    </div>

    <div class="section" id="step2">
        <h3><span></span>商品をカートに入れてください</h3>
        <div class="flex">
            <div class="txt">
                ●カラーに間違いないかご確認ください。<br>
                ●イージーオーダー、オーダーメイドのカーテンをご購入される際は、横幅や高さ、フックの種類をご確認ください。<br>
                欲しい商品の個数を確認のうえ、カートに入れます。
            </div>
        </div>
    </div>

    <div class="section" id="step3">
        <h3><span></span>お客様情報をご入力ください</h3>
        <div class="flex">
            <div class="txt">
                <p>
                    会員登録に進みまして、お客様情報の入力を行ってください。
                </p>
            </div>
        </div>
    </div>

    <div class="section" id="step4">
        <h3><span></span>お支払い情報や送付先をご入力ください</h3>
        <div class="flex">
            <div class="txt">
                <p class="mb20">ご注文内容をご確認の上、決済方法を下記よりお選びいただき、送付先をご入力ください。 </p>
                <p>
                    ・クレジットカード（1回払い・リボ払い・分割払い）<br>
                    ・コンビニ払い（セブンイレブンを除く）<br>
                    ・Pay-easy決済<br>
                    ・キャリア決済（auを除く予定）<br>
                    ※代引きは不可となります。
                </p>
                <p style="text-align:right;"><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php#2">お支払いについて＞</a></p>
            </div>
        </div>
    </div>

    <div class="section" id="step5">
        <h3><span></span>注文が完了しました</h3>
        <div class="flex">
            <div class="txt">
                <p>注文の確定を行いましたら自動返信メールが届きます。<br>
                ご注文内容に誤りがないかご確認ください。</p>
            </div>
        </div>
    </div>
    <div class="point_area">
        <h3 class="mb10">メールが届かない場合は以下の項目をご確認ください。</h3>
        <ul>
            <li>・迷惑メールのフォルダに振り分けられていないかご確認ください。</li>
            <li>・ドメイン指定受信の設定をご確認ください。</li>
            <li>・【ouoku.co.jp]ドメインからのメールを受信できるように設定してください。</li>
            <li>・メールアドレスを間違えてご登録されていないかご確認ください。</li>
        </ul>
    </div>


</div>
                    
                    
		
