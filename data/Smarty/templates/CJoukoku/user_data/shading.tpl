<div class="page-body">
		<div class="inner feature">
			
			<div class="title01"><img src="<!--{$TPL_URLPATH}-->img/common/n_title_01.png"></div>
			<div class="title02"><img src="<!--{$TPL_URLPATH}-->img/common/n_title_02.png"></div>
			
			<div class="sec01 clearfix">
				<dl>
					<dt><img src="<!--{$TPL_URLPATH}-->img/common/n_img_01.png"></dt>
					<dt>涼しさ全開！い草<span>特集</span></dt>
					<dd>暑い夏をい草のラグで快適に過ごそう！</dd>
				</dl>
				<dl>
					<dt><img src="<!--{$TPL_URLPATH}-->img/common/n_img_02.png"></dt>
					<dt>遮熱カーテン<span>特集</span></dt>
					<dd>これからの季節に最適！熱を遮断して冷暖房がよく効く！</dd>
				</dl>
				<dl>
					<dt><img src="<!--{$TPL_URLPATH}-->img/common/n_img_03.png"></dt>
					<dt>遮光カーテン<span>特集</span></dt>
					<dd>日よけや街灯などの光を遮断する遮光カーテン特集</dd>
				</dl>
			</div>
			
			
		
		</div><!-- page-body inner -->
	</div><!-- page-body -->