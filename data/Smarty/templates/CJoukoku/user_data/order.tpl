<div class="page order">
    <div class="ttl">
        <h2>
            <span></span>ご注文からお届けまでの流れ
        </h2>
    </div>
    <p class="mb30">商品のご注文からお届けまで、ご注文いただいた際の一通りの流れをご案内いたします。</p>
    <div class="section">
        <div class="step">
            <ul>
                <li><a href="#step1"><p class="step_no"><span>STEP</span>1</p><p class="txt">採寸</p><span class="arrow"></span></a></li>
                <li><a href="#step2"><p class="step_no"><span>STEP</span>2</p><p class="txt">ご注文</p><span class="arrow"></span></a></li>
                <li><a href="#step3"><p class="step_no"><span>STEP</span>3</p><p class="txt">注文確認メール<br>(自動配信)</p><span class="arrow"></span></a></li>
                <li><a href="#step4"><p class="step_no"><span>STEP</span>4</p><p class="txt">受注確認入金<br>確認メール</p><span class="arrow"></span></a></li>
                <li><a href="#step5"><p class="step_no"><span>STEP</span>5</p><p class="txt">制作・納期の<br>ご連絡</p><span class="arrow"></span></a></li>
                <li><a href="#step6"><p class="step_no"><span>STEP</span>6</p><p class="txt">出荷お知らせ<br>メール</p><span class="arrow"></span></a></li>
                <li><a href="#step7"><p class="step_no"><span>STEP</span>7</p><p class="txt">商品の<br>お届け</p><span class="arrow"></span></a></li>
            </ul>
        </div>
    </div>
    <div class="section" id="step1">
        <h3><span></span>採寸する</h3>
        <div>
            <p>カーテンを設置する箇所のサイズをお測りください。</p>
            <div style="text-align:right;margin-top:20px;"><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/measurements.php">カーテン採寸方法&gt;</a></div>
        </div>
    </div>
    <div class="section" id="step2">
        <h3><span></span>注文する</h3>
        <div>
            <p>ご希望の商品をお選びください。</p>
            <div style="text-align:right;margin-top:20px;"><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/flow.php">ご購入方法&gt;</a></div>
        </div>
    </div>
    <div class="section" id="step3">
        <h3><span></span>注文確認メール（自動配信）をご確認ください</h3>
        <div>
            <p>
                ご注文後すぐに当店より【ご注文確認メール（自動配信）】を送付させていただきます。<br>
                ご注文内容の確認として保存をお願いいたします。<br>
                なお、このご注文確認メールはご注文が正常に届いていることをお知らせする自動配信のメールとなっております。<br>
                ご注文が完了していない場合や迷惑メール対策などでドメイン指定などの受信制限をされているお客様については<br>
                oukoku.co.jpのドメインメールを受信できるように設定の変更をお願い致します。
            </p>

            <p>
                &#8251;ご注文内容に不備がある場合、メールにてご案内をさせていただきます。<br>
                &#8251;メールが届かない場合は、ご連絡いただけますようお願い致します。
            </p>
            <div class="form_btn"><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top/">お問い合わせフォーム&gt;</a></div>
        </div>
    </div>
    <div class="section" id="step4">
        <h3><span></span>受注確認、入金確認メールをご確認ください</h3>
        <div>
            <p>
            ご注文後の入金が確認できましたら、当店より受注確認及び入金確認メールをお送りいたします。<br>
            ご注文内容が確定し、ご入金の確認が取れましたら商品の作製に進ませていただきます。
            </p>
            <div style="text-align:right;margin-top:20px;"><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php#2">お支払いについて&gt;</a></div>
        </div>
    </div>
    <div class="section" id="step5">
        <h3><span></span>納期をご連絡いたします</h3>
        <div>
            <p>
            商品のお届けする時期が分かりましたら､カーテンじゅたん王国からお客様へご連絡いたします。
            </p>
        </div>
    </div>
    <div class="section" id="step6">
        <h3><span></span>出荷のお知らせメールをご確認ください</h3>
        <div>
            <p>
                商品の出荷後、出荷のお知らせメールをお送りいたします。<br>
                商品の到着までお待ちください。
            </p>
            <div style="text-align:right;margin-top:20px;"><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php#1">配送・送料について&gt;</a></div>
        </div>
    </div>
    <div class="section" id="step7">
        <h3><span></span>商品をお届けします</h3>
        <div>
            <p>
            ご到着した商品に間違いがないかご確認ください。
            </p>
            <div style="text-align:right;margin-top:20px;"><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php#3">返品・交換について&gt;</a></div>

        </div>
    </div>
</div>
