<div class="page measure">
    <div id="contents_wrapper">
    <!-- ▼タイトル -->
    <div class="ttl">
        <h2><span></span>採寸方法</h2>
    </div>
    <!-- ▲タイトル -->
    <!-- ▼採寸方法 -->
    <div class="measure">
    <!-- ▼切り替え -->
    <!-- <ul class="measure__nav">
    <li>カーテン</li>
    <li>シェード</li>
    <li>ロールスクリーン</li>
    <li>ブラインド</li>
    </ul> -->
    <div class="tab_wrap" style="display:none">
    <input id="tab1" type="radio" name="tab_btn" checked>
    <input id="tab2" type="radio" name="tab_btn">
    <input id="tab3" type="radio" name="tab_btn">
    <input id="tab4" type="radio" name="tab_btn">

    <div class="tab_area">
    <label class="tab1_label" for="tab1"><span class="arrow"></span>カーテン</label>
    <!--label class="tab2_label" for="tab2"><span class="arrow arrow02"></span>シェード</label-->
    <!--label class="tab3_label" for="tab3"><span class="arrow arrow03"></span>ロールスクリーン</label-->
    <!--label class="tab4_label" for="tab4"><span class="arrow arrow04"></span>ブラインド</label-->
    </div>
    </div>
    <!-- ▲切り替え -->
    <div class="measure__intro">
    <p class="measure__intro__txt">部屋の雰囲気や窓のサイズにあったカーテンは快適な空間を作り出します。以下の手順にしたがって適切なサイズを採寸してみましょう。</p>
    <div class="measure__intro__item">
        <div class="flex">
            <div class="">
                <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_character.png" alt="">
            </div>
        </div>
        <div class="">
            <a href="#movie" class="btn__item_btn" name=""><img src="<!--{$TPL_URLPATH}-->img/measure/measurements_play_btn.png" alt=""></a>
            <p class="btn_bottom">※動画の採寸方法はオンラインストアでのご注文方法と異なりますので、ご了承ください。</p>
        </div>
    </div>
    </div>

    <!-- ▼採寸方法STEP -->
    <div class="page measurement">
        <div class="section">
            <div class="step">
                <ul>
                    <li>
                        <a href="#step1">
                            <p class="step_no"><span>STEP</span>1</p>
                            <p class="txt">レールの<br>横幅を測る</p>
                            <span class="arrow"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#step2">
                            <p class="step_no"><span>STEP</span>2</p>
                            <p class="txt">高さを測る</p>
                            <span class="arrow"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#step3">
                            <p class="step_no"><span>STEP</span>3</p>
                            <p class="txt">フックとレール<br>の形式を確認</p>
                            <span class="arrow"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#step4">
                            <p class="step_no"><span>STEP</span>4</p>
                            <p class="txt">開き方と<br>窓の周辺を確認</p>
                            <span class="arrow"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#step5">
                            <p class="step_no"><span>STEP</span>5</p>
                            <p class="txt">つなぎ目と<br>柄合わせを確認</p>
                            <span class="arrow"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- ▲採寸方法STEP -->

    <!-- ▼STEP1 -->
    <div class="measure__step_col" id="step1">
    <div class="measure__step_col__step_title">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_icon_step01.png" alt="">
    <h3 class="measure__step_col__step_title__txt">レールの横幅を測ります</h3>
    </div>
    <p class="measure__step_col__title_bottom">巾は一番端の固定ランナーにメジャーを引っかけて測ります。</p>

    <ul class="measure__step_col__rail">
    <li>
    <h4 class="measure__step_col__title">一般的な機能レール</h4>
    <div class="measure__step_col__img_box">
        <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_width_a.png" alt="">
    </div>
    </li>
    <li>
    <h4 class="measure__step_col__title">装飾レール</h4>
    <div class="measure__step_col__img_box">
        <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_width_b.png" alt="">
    </div>
    </li>
    <li>
    <h4 class="measure__step_col__title">出窓</h4>
    <div class="measure__step_col__img_box">
        <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_width_c.png" alt="">
    </div>
    </li>
    </ul>

    <div class="measure__attention">
    <div class="measure__attention__box">
    <div class="measure__attention__box_title">
    <div>
    <img src="<!--{$TPL_URLPATH}-->img/icon/icon_attention.png" alt="">
    </div>
    <p class="measure__attention__box_title__txt">カーテンの仕上がり巾について</p>
    </div>
    <p class="measure__attention__box_txt">カーテンの仕上がり巾は計測した巾よりも５％程度プラス(注1)して作成となります。<br>(注1：計測したレール巾に上乗せがないと閉まりに余裕がないため)</p>
    </div>
    <div class="measure__attention__sample_box">
    <p class="measure__attention__sample_box__title">例えば 計測巾190cmの場合</p>
    <p class="measure__attention__sample_box__ruby">上乗せ分を合計した長さ(小数点以下 切り上げ)</p>
    <p class="measure__attention__sample_box__detail">
    190<span class="mini">cm</span>
    <span class="mini">×</span>
    1.05<span class="mini both">=</span>
    <span class="bold">199.5</span>
    <span class="both arrow"></span>
    <span class="bold">200</span>
    <span class="mini">cm</span>
    </p>
    </div>
    </div>

    </div>
    <!-- ▲STEP1 -->

    <!-- ▼STEP2 -->
    <div class="measure__step_col" id="step2">
    <div class="measure__step_col__step_title">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_icon_step02.png" alt="">
    <h3 class="measure__step_col__step_title__txt">高さを測ります</h3>
    </div>
    <p class="measure__step_col__title_bottom">メジャーをランナーにひっかけ、垂直に下ろして測ります。<br>機能レール、装飾レール共にフックの穴の下から測ります。Aフックの場合もBフックの場合も、測る位置は変わりません。</p>

    <ul class="measure__step_col__ul">
    <li class="measure__step_col__ul__li">
    <div class="measure__step_col__ul__li__left">
    <p class="measure__step_col__title">腰高窓</p>
    <p class="measure__step_col__title_bottom">腰高窓はランナーから窓枠の下まで測ります。光漏れなどを防ぐために窓枠下から15cm〜20cmプラスした長さが理想的です。但し、窓の下に机などの家具を置く場合は干渉しないようご注意下さい。</p>
    </div>
    <div class="measure__step_col__ul__li__right measure__step_col__img_box">
        <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_height_a.png" alt="">
    </div>
    </li>
    <li class="measure__step_col__ul__li">
    <div class="measure__step_col__ul__li__left">
    <p class="measure__step_col__title">掃き出し窓</p>
    <p class="measure__step_col__title_bottom">掃き出し窓はランナーから床までを測ります。<br>※床にすらないようにマイナス1cmします。レースカーテンは計測した丈からマイナス2cmします。</p>
    </div>
    <div class="measure__step_col__ul__li__right measure__step_col__img_box">
        <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_height_b.png" alt="">
    </div>
    </li>
    <li class="measure__step_col__ul__li">
        <div class="measure__step_col__ul__li__left">
            <p class="measure__step_col__title">出窓</p>
            <p class="measure__step_col__title_bottom">出窓は2か所測ります。<br>①ランナーから窓枠の下まで測ります。イラスト①のようにお部屋側についている場合は、腰高窓と同じ測り方です。</p>
        </div>
        <div class="measure__step_col__ul__li__right">
            <p class="measure__step_col__title_bottom upper_space">②ランナーから棚板の上までを測ります。出窓のカウンターにすらないようにマイナス1cmします。</p>
        </div>
    </li>
    <li class="measure__step_col__ul__li">
        <div class="measure__step_col__ul__li__right measure__step_col__img_box window">
            <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_height_c.png" alt="">
        </div>
        <div class="measure__step_col__ul__li__right measure__step_col__img_box window">
            <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_height_d.png" alt="">
        </div>
    </li>
    <li class="measure__step_col__ul__li">
    <div class="measure__step_col__ul__li__left">
    <p class="measure__step_col__title">立ち上がり窓</p>
    <p class="measure__step_col__title_bottom">立ち上がり窓はランナーから床までを測ります。<br>床にすらないようにマイナス1cmします。レースカーテンは計測した丈からマイナス2cmします。</p>
    </div>
    <div class="measure__step_col__ul__li__right measure__step_col__img_box">
        <img src="<!--{$TPL_URLPATH}-->img/measure/measurements_height_e.png" alt="">
    </div>
    </li>
    </ul>

    <div class="measure__attention">
    <div class="measure__attention__box lower_space">
    <div class="measure__attention__box_title">
    <div class="measure__attention__box_title__img">
    <img src="<!--{$TPL_URLPATH}-->img/icon/icon_attention.png" alt="">
    </div>
    <p class="measure__attention__box_txt left_space">窓のタイプによって計測を行って頂き、足し引きをしたサイズにて入力してください。</p>
    </div>
    </div>
    <div class="measure__attention__box_title">
    <div class="measure__attention__box_title__img">
    <img src="<!--{$TPL_URLPATH}-->img/icon/icon_attention.png" alt="">
    </div>
    <p class="measure__attention__box_txt left_space">カーテンの丈は生地ものである為、プラスマイナス1cmの誤差が発生する場合がございます。その場合は、アジャスターフックにて調整をして頂くようお願い致します。</p>
    </div>
    </div>

    </div>
    <!-- ▲STEP2 -->

    <!-- ▼STEP3 -->
    <div class="measure__step_col" id="step3">
    <div class="measure__step_col__step_title">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_icon_step03.png" alt="">
    <h3 class="measure__step_col__step_title__txt">フックとレールの形式を確認します</h3>
    </div>
    <p class="measure__step_col__title_bottom lower_space">カーテンのフックはレールを見せるタイプとレールを隠すタイプの2種類があります。</p>

    <ul class="measure__step_col__ul lower_space">
    <li class="measure__step_col__ul__li lower_space">
    <div class="measure__step_col__ul__li__left">
    <p class="measure__step_col__title">レールを見せるAフック</p>
    <p class="measure__step_col__title_bottom">フックの上に生地が1cm付け足されており、カーテンレールが見えます。</p>
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_fook_img01.png" alt="">
    </div>
    <div class="measure__step_col__ul__li__right">
    <p class="measure__step_col__title">レールを見せないBフック</p>
    <p class="measure__step_col__title_bottom">フックの上に生地が4cm付け足されており、カーテンレールを隠すようになっています。</p>
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_fook_img02.png" alt="">
    </div>
    </li>
    </ul>
    <p class="measure__step_col__txt measure__step_col__title_bottom">Aフック・Bフック共にアジャスターフックとなっておりますので、多少の丈調整ができます。</p>

    <p class="measure__step_col__title">レールの形式は4種類</p>
    <p class="measure__step_col__title_bottom">レールの形式によって、フックの形式が異なりますので、レールの形式もご確認ください。</p>
    <ul class="measure__step_col__ul">
    <li class="measure__step_col__ul__li lower_space">
    <div class="measure__step_col__ul__li__left">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_fook_img03.png" alt="">
    </div>
    <div class="measure__step_col__ul__li__right lower_space">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_fook_img04.png" alt="">
    </div>
    </li>
    <li class="measure__step_col__ul__li lower_space">
    <div class="measure__step_col__ul__li__left">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_fook_img05.png" alt="">
    </div>
    <div class="measure__step_col__ul__li__right lower_space">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_fook_img06.png" alt="">
    </div>
    </li>
    </ul>

    <div class="measure__attention">
    <div class="measure__attention__box">
    <div class="measure__attention__box_title">
    <div>
    <img src="<!--{$TPL_URLPATH}-->img/icon/icon_attention.png" alt="">
    </div>
    <p class="measure__attention__box_title__txt">既製品に関して</p>
    </div>
    <p class="measure__attention__box_txt">既製カーテンをご購入いただいた場合、厚地のカーテンはBフック、レースカーテンはAフックにてセットしております。</p>
    </div>
    <div class="measure__attention__box_title">
    <div>
    <img src="<!--{$TPL_URLPATH}-->img/icon/icon_attention.png" alt="">
    </div>
    <p class="measure__attention__box_title__txt">丈サイズについての注意点</p>
    </div>
    <p class="measure__attention__box_txt">フックから上の生地の付けたしについては、仕上がりサイズの算出の際に足して頂く必要はありません。全てランナーから計測したサイズをもとに、窓のタイプごとに足し引きを行ったサイズをご注文ください。</p>
    </div>

    </div>
    <!-- ▲STEP3 -->

    <!-- ▼STEP4 -->
    <div class="measure__step_col" id="step4">
    <div class="measure__step_col__step_title">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_icon_step04.png" alt="">
    <h3 class="measure__step_col__step_title__txt">カーテンの開き方と窓の周辺を確認しましょう</h3>
    </div>

    <ul class="measure__step_col__ul lower_space">
    <li class="measure__step_col__ul__li">
    <div class="measure__step_col__ul__li__left">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_opentype_img01.png" alt="">
    </div>
    <div class="measure__step_col__ul__li__right lower_space">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_opentype_img02.png" alt="">
    </div>
    </li>
    <li class="measure__step_col__ul__li under_window">
    <div class="measure__step_col__ul__li__left">
    <p class="measure__step_col__title">カーテンの下に干渉物がありませんか？</p>
    <p class="measure__step_col__title_bottom">右記の干渉物がある場合は、お客様のご希望のサイズをご指定ください。</p>
    </div>
    <div class="measure__step_col__ul__li__right lower_space">
    <div class="measure__step_col__under_window">
    <p class="measure__step_col__under_window__title">窓の下の干渉物例</p>
    <ul>
    <li class="measure__step_col__under_window__bottom"><span class="checkmark"></span>デスク</li>
    <li class="measure__step_col__under_window__bottom"><span class="checkmark"></span>チェスト</li>
    <li class="measure__step_col__under_window__bottom"><span class="checkmark"></span>ベッドのヘッド部分</li>
    <li class="measure__step_col__under_window__bottom"><span class="checkmark"></span>ソファの背もたれなど</li>
    <li class="measure__step_col__under_window__bottom"><span class="checkmark"></span>カーペットをカーテンの下に敷く予定の有無</li>
    </ul>
    </div>
    </div>
    </li>
    </ul>

    </div>
    <!-- ▲STEP4 -->

    <!-- ▼STEP5 -->
    <div class="measure__step_col" id="step5">
    <div class="measure__step_col__step_title lower_space">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_icon_step05.png" alt="">
    <h3 class="measure__step_col__step_title__txt">カーテンのつなぎ目と柄合わせを確認しましょう</h3>
    </div>
    <ul class="measure__step_col__ul lower_space02">
    <li class="measure__step_col__ul__li">
    <div class="">
    <p class="measure__step_col__title">つなぎ目について</p>
    <p class="measure__step_col__title_bottom">カーテンのつなぎ目は既製カーテン、オーダーカーテンにおいてもサイズにとって生じます。1.5 倍ヒダでは横幅100cm以上、2倍のヒダでは横幅70cm以上 (一部75cm以上) の場合にはつなぎ目が入ります。つなぎ目のないカーテンをご希望の場合は、お問合せよりご連絡ください。</p>
    </div>
    <div class="measure__step_col__ul__li__right lower_space" style="display:none;">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_joint_sampleimg.jpg" alt="">
    </div>
    </li>
    </ul>

    <div>
    <p class="measure__step_col__title">柄合わせについて</p>
    <p class="measure__step_col__title_bottom">1.5倍ヒダの既製カーテン横幅100cm以上の商品は柄合わせを行っておりません。その為、デザインがきっちり連続しませんのでご了承下さい。気にされる場合は、オーダーカーテンからお選び下さい。</p>
    </div>

    <div class="measure__attention lower_space">
    <div class="measure__attention__box_title">
    <div>
    <img src="<!--{$TPL_URLPATH}-->img/icon/icon_attention.png" alt="">
    </div>
    <p class="measure__attention__box_title__txt">カーテンのつなぎ合わせについて</p>
    </div>
    <p class="measure__attention__box_txt">カーテン(既製・カーテン)は、「仕上がりサイズ・使用する生地巾」によって、カーテンにつなぎ目が出ます。予めご了承ください。</p>
    </div>

    <div id="movie" class="measure__movie">
    <div class="measure__movie__title">
    <img src="<!--{$TPL_URLPATH}-->img/measure/measure_movie_play.png" alt="">
    <p class="measure__movie__title__txt">とっても簡単!カーテンの測り方</p>
    </div>
    <p class="measure__movie__title_bottom measure__step_col__title_bottom lower_space">測り方は動画でもご覧いただけます。<br>※動画の採寸方法はオンラインストアでのご注文方法と異なりますので、ご了承ください。</p>
    <div class="measure__movie__video">
        <div class="youtube">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/rNDDYOxnX-k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
    </div>

    </div>
    <!-- ▲STEP5 -->

    </div>
    <!-- ▲採寸方法 -->

    </div>
</div>

