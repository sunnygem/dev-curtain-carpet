<div class="page-head-under tenpo-service-head">
		<h2><img src="<!--{$TPL_URLPATH}-->img/common/tenpo_title.png"></h2>
		<h3><span>来店無料サービス</span></h3>
	</div>
	
	<div class="green-border">
		<div class="pankuzu">
			<div>店舗サービス</div><div>関東</div>
		</div>
	</div>
	
	<div class="page-white tenpo-service-page">
		<div class="inner">
			<div class="sec01">
				<h3>カーテンじゅうたん王国からの<br>来店時無料サービスのご紹介</h3>
				<p>カーテンじゅうたん王国では、「カーテンを買おうかな～？」とお考えの方に素敵なプレゼントをご用意いたしました。ご購入にあたり必要な採寸メジャーなどに加えて、うれしいプレゼントが無料でもらえるネット限定の特典です。</p>
				<div class="clearfix">
					<dl>
						<dt>特典１</dt>
						<dt class="img"><img src="<!--{$TPL_URLPATH}-->img/common/tenpo_free_img01.png"></dt>
						<dd>カーテンの採寸に便利なメジャーをプレゼント</dd>
					</dl>
					<dl>
						<dt>特典２</dt>
						<dt class="img"><img src="<!--{$TPL_URLPATH}-->img/common/tenpo_free_img02.png"></dt>
						<dd>サイズの測り方リーフレットをプレゼント</dd>
					</dl>
					<dl>
						<dt>特典３</dt>
						<dt class="img"><img src="<!--{$TPL_URLPATH}-->img/common/tenpo_free_img03.png" class="pad"></dt>
						<dd>うれしいプレゼントがもらえます（店舗で交換できる引換券）</dd>
					</dl>
				</div>
			</div>
			<div class="sec02">
				<h4><span><img src="<!--{$TPL_URLPATH}-->img/common/tenpo_icon01.png"></span>下記の手順を踏むだけで上記の無料特典が受けられます。</h4>
				<div class="clearfix">
				<div class="box01">
					<p>下記ボタンから<br>
					入力フォームに<br>
						情報を登録してください。</p>
					<div class="btn"><a href="#">無料プレゼントの申込み</a></div>
				</div>
					<div class="box02">
						<p>特典を発送します。<br>
							特典が届くまで<br>
							お待ちください。</p>
					</div>
					<div class="box03">
						<p>採寸メジャー、<br>
							リーフレット、<br>
							嬉しいプレゼント（引換券）<br>
							が届きます。</p>
					</div>
				</div>
				<p class="hosoku">メール便による送付の為、出荷後到着まで約2～4日ほどいただいております<br>
					<br>
					※特典が届くまで数日お待ちください（土日の発送は行っておりません）<br>
					<br>
					※引換対象商品は予告なく変更となる場合がございます。予めご了承ください。<br>個人情報保護方針に同意後、ボタンをクリックしてください。</p>
			</div>
			
			<div class="btn-area"><a href="#">無料のプレゼントの申込み</a></div>
			
		</div>
	</div>