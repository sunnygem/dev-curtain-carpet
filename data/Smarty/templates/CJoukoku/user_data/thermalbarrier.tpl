<style>
.thermal { width: 750px; padding: 0px 0 30px;}
.thermal img { max-width:100%; height:auto; }
.thermal dt, .thermal dd, .thermal ul{font-size:12px;}
.thermal p{ margin-bottom: 20px; font-size:12px;}
.thermal h2 { margin-bottom: 30px; }
.thermal__btn{ width:250px; text-align:center; margin:0 auto 30px auto;}
.thermal__btn  span a{ display:block; border:1px solid #242424; padding:10px 20px; font-size:13px; }

.thermal__intro{ display:flex; justify-content:space-between; margin-bottom:60px; }
.thermal__intro dt { width:50%; }  
.thermal__intro dt img { vertical-align:bottom; }
.thermal__intro dd { width:47%; display:flex; flex-direction:column;  justify-content:space-between; }
.thermal__intro dd p { font-weight:bold; }
.thermal__intro dd  .thermal__btn{ margin-bottom:0px; }

.thermal__product{ display:flex; justify-content:space-between; flex-wrap:wrap; margin-bottom:10px;}
.thermal__product ul{ width:31%; margin-bottom:30px;}
.thermal__product--title{ font-weight:bold; margin:3px 0px 10px 0px}
.thermal__product::after{ content:""; display: block; width:31%; }

@media screen and (max-width:960px) {
.thermal { width: 95%; margin: 0 auto; padding: 10px 0 0;}
.thermal__intro { flex-direction:column; }
.thermal__intro dt { width:100%; margin-bottom:10px;}
.thermal__intro dd { width:100%;}
.thermal__intro dd p { font-weight:bold;  }
.thermal__intro dd .thermal__btn{ margin-bottom:0px; }
}
</style>


<div class="page-body">
<div class="inner thermal">
            <h2><img src="<!--{$TPL_URLPATH}-->img/thermal/img_title.jpg" alt="遮熱断熱特集" /></h2>

<dl class="thermal__intro">
    <dt><img src="<!--{$TPL_URLPATH}-->img/thermal/img_main.jpg" alt="" /></dt>
    <dd>
        <p>冬場なかなかお部屋が温まらない、<br />帰宅時や起床時、とても部屋が寒い。<br />そんなお悩みにおすすめの寒さ対策をまとめました。</p>
        <div class="thermal__btn"><span><a href="/shop/products/list.php?category_id=1729">遮熱・断熱カーテン一覧</a></span></div>
   </dd>
</dl>

<div class="thermal__product">
    <ul><li><img src="<!--{$TPL_URLPATH}-->img/thermal/img_product01.jpg" alt="カーテンの開閉で室温を保つ" /></li><li class="thermal__product--title">カーテンの開閉で室温を保つ</li><li>お出かけ前にエアコンなどの空調機器を使用している場合、カーテンを閉め切っておくと、室内の空気が逃げにくくなりますので、室温の変動が抑えられます。</li></ul>
    <ul><li><img src="<!--{$TPL_URLPATH}-->img/thermal/img_product02.jpg" alt="カーテンやシェードを遮熱・断熱効果の高い生地に変える" /></li><li class="thermal__product--title">カーテンやシェードを遮熱・断熱効果の高い生地に変える</li><li>シェードは生地のみを交換することができます。窓辺に近いレースを断熱効果の高いものに変えるだけでも、遮熱断熱効果が高まります。<br />※シェードは店舗でのみお取り扱いしております。<br /><a href="/shop/products/list.php?category_id=1729">遮熱・断熱カーテンはこちら＞</a>
</li></ul>
    <ul><li><img src="<!--{$TPL_URLPATH}-->img/thermal/img_product03.jpg" alt="カーテンレールの上にカバートップを付ける" /></li><li class="thermal__product--title">カーテンレールの上にカバートップを付ける</li><li>お部屋の冷たい空気を逃さず、外からの熱い空気を室内に入れません。お掃除もラクになり、防音効果もあります。<br />※店舗にてご相談ください。オンラインストアではお取り扱いしておりません。</li></ul>
    <ul><li><img src="<!--{$TPL_URLPATH}-->img/thermal/img_product04.jpg" alt="リターン縫製でカーテンのサイドからの冷気を防ぐ" /></li><li class="thermal__product--title">リターン縫製でカーテンのサイドからの冷気を防ぐ</li><li>リターン縫製とはＬ字型にカーテンを縫製し、レールの脇までカーテンが行き届く縫製です。レールの脇までカーテンで覆うことで、お部屋の冷たい空気を逃さず、外からの熱い空気を室内に入れません。<br />※店舗にてご相談ください。オンラインストアではお取り扱いしておりません。</li></ul>
    <ul><li><img src="<!--{$TPL_URLPATH}-->img/thermal/img_product05.jpg" alt="カーテンに裏地を付ける" /></li><li class="thermal__product--title">カーテンに裏地を付ける</li><li>生地の厚みが増すことで断熱効果が高まります。外からの熱い空気を防ぎ、室内の冷たい空気を逃がしません。冷暖房効果アップで1年を通して節電にもつながります。<br />※店舗にてご相談ください。オンラインストアは一部の商品のみ裏地がついています。</li></ul>
</div>

<div class="thermal__btn"><span><a href="/shop/products/list.php?category_id=1729">遮熱・断熱カーテンはこちら</a></span></div>


</div><!-- page-body inner -->
</div><!-- page-body -->