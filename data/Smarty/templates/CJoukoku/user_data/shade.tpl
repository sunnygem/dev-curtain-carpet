<style>
.shade { width: 750px; padding: 0px 0 30px;}
.shade img { max-width:100%; height:auto; }
.shade dt, .shade dd, .shade ul{font-size:12px;}
.shade p{ margin-bottom: 20px; font-size:12px;}
.shade h2 { margin-bottom: 30px; }
.shade h3 { margin-bottom: 10px; }
.shade .txtcenter { text-align:center; }
.shade__btn{ width:250px; text-align:center; margin:0 auto 30px auto;}
.shade__btn  span a{ display:block; border:1px solid #242424; padding:10px 20px; font-size:13px; }

.shade__intro { width:100%; background-color:#194292; margin-bottom:20px; padding-bottom:2.73%; }
.shade__intro__title { text-align:center; }
.shade__intro__lead { text-align:center; color:#fff; margin:0px 2.73% 20px 2.73%; }
.shade__intro__body { background-color:#fff; padding:3.82%; width:96%; margin:0px auto; }
.shade__intro__body p { font-size:16px; font-weight:bold; }
.shade__intro__list li{ margin-bottom:19px; font-weight:bold; font-size:13px;}
.shade__intro__list li:last-child { margin-bottom:0; }
.shade__intro__list { position:relative; padding-left:2.5em;line-height:1.8em;}
.shade__intro__list li::before { position:absolute;left:0;content:"";display:inline-block;width:1.8em;height:1.8em;background:url("/shop/user_data/packages/CJoukoku/img/shade/icon_check.png") no-repeat;background-size:contain; }

.shade__productbox__wrapper { width: 100%; background-color:#f8f8f8; padding:30px; box-sizing:border-box; margin-bottom:2%;}
.shade__productbox { display:flex; justify-content: space-between; margin-bottom:5px; }
.shade__productbox__img { width:20%; }
.shade__productbox__txt { width:76%; font-weight:bold; }
.shade__productbox__grade { font-size:1em; margin-bottom:1.5em; }
.shade__productbox__title { font-size: 2em; margin-bottom: 3px; }

.shade__caution { width:100%; padding:15px; padding-bottom:2px; background-color:#ffefbe; margin-bottom:30px; }
.shade__caution__title { color:#b50708; margin-bottom:5px; font-size:12px; font-weight:bold; }

@media screen and (max-width:960px) {
.shade { width: 95%; margin: 0 auto; padding: 10px 0 0;}
.shade__productbox__wrapper { padding:2.73%; }
.shade__productbox__img { width:38%; }
.shade__productbox__txt { width:52%; }
.shade__productbox__title { font-size: 1.7em; margin-bottom: 3px; }
}
</style>


<div class="page-body">
<div class="inner shade">

<h2><img src="<!--{$TPL_URLPATH}-->img/shade/img_title.jpg" alt="夜間の街灯やお昼寝の時間の日光対策に遮光カーテン" /></h2>

<div class="shade__intro">
<h3 class="shade__intro__title"><img src="<!--{$TPL_URLPATH}-->img/shade/img_main.jpg" alt="日差しや街灯などの光に遮光カーテン" /></h3>
<h4 class="shade__intro__lead">遮光カーテンとはお部屋に入ってくる光を遮る機能を持ったカーテンです。</h4>
<div class="shade__intro__body">
    <p>遮光カーテンはこんな方におすすめです。</p>
    <ul class="shade__intro__list">
        <li>朝日や西日の強い日差しを抑えたい。</li>
        <li>夜勤などによる昼間の睡眠を快適にしたい。</li>
        <li>車のライトや、コンビニ、街灯などの光を防ぎたい。</li>
        <li>夜間の外からの視線を防ぎたい。（夜間電気を点けた時に、外に影が映るのが気になる）</li>
    </ul>
</div>
</div>

<h3 class="txtcenter">遮光カーテンには等級があります。</h3>

<div class="shade__productbox__wrapper">
<dl class="shade__productbox">
    <dt class="shade__productbox__img"><img src="<!--{$TPL_URLPATH}-->img/shade/img_product01.jpg" alt="99.99%の遮光率" /></dt>
    <dd class="shade__productbox__txt">
        <ul>
            <li class="shade__productbox__grade">遮光1級</li>
            <li class="shade__productbox__title">99.99%の遮光率</li>
            <li class="shade__productbox__body">シャッターや雨戸に一番近い<br />人の表情が判別できない暗さ、外の明るさをほぼ感じない。</li>
        </ul>
    </dd>
</dl>

<dl class="shade__productbox">
    <dt class="shade__productbox__img"><img src="<!--{$TPL_URLPATH}-->img/shade/img_product02.jpg" alt="99.80%の遮光率" /></dt>
    <dd class="shade__productbox__txt">
        <ul>
            <li class="shade__productbox__grade">遮光2級</li>
            <li class="shade__productbox__title">99.80%の遮光率</li>
            <li class="shade__productbox__body">本を読めない暗さ<br />人の表情が判別できる暗さ、外が明るいのがまだわかる。</li>
        </ul>
    </dd>
</dl>

<dl class="shade__productbox">
    <dt class="shade__productbox__img"><img src="<!--{$TPL_URLPATH}-->img/shade/img_product03.jpg" alt="99.40%の遮光率" /></dt>
    <dd class="shade__productbox__txt">
        <ul>
            <li class="shade__productbox__grade">遮光3級</li>
            <li class="shade__productbox__title">99.40%の遮光率</li>
            <li class="shade__productbox__body">眩しさは無いが朝が来たのはわかる。人の表情が判別できる暗さ、外が明るいことがわかる。事務作業には暗い。</li>
        </ul>
    </dd>
</dl>
</div>

<div class="shade__caution">
    <div class="shade__caution__title">注意事項</div>
    <p>同じ遮光等級でも表生地の色により光（明かり）の映り方に若干の違いがあります。<br />また、使用する窓の方角（日当たり）によっても効果に差がありますので、等級はあくまでも目安となります。</p>
    <p>また、遮光カーテンの場合、つなぎ目やミミ・裾の折り返しの縫い合わせ部分からの光漏れが気になる場合がございます。縫い針の通った穴からの光漏れについては縫製上どうしても生じてしまいますのであらかじめご了承下さい。</p>
</div>

<div class="shade__btn"><span><a href="/shop/products/list.php?category_id=1729">遮光カーテンの一覧はこちら</a></span></div>



</div>
</div>