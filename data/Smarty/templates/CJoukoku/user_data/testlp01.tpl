<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/view.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/publis.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/common.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/shop/user_data/packages/CJoukoku/css/testlp/nw_header.css" charset="UTF-8" />
<div id="mainArea" class="pbMainArea" style="max-width:1000px;">
	<div id="area0" class="pbArea ">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock17796">
<div style="width:100%;">					<div style="width:100%;margin:0px;">
												<div style="width:100%;float:left;">
							<div class="pbBlock pbTitleBlock "><h1>サポート＆サービス</h1></div>
							
							
							
						</div>
						
					</div></div>
			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock2052095">
		<div class="pbNested pbNestedHorizontalWrapper ">
					<div class="pbHorizontalNested" id="pbBlock2052096" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv2052096" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<p><a href="/support_service.html"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/117822131722.jpg" alt="ご来店前に店員.jpg" title="ご来店前に店員.jpg" style="width: 100%;" width="100%" class="imgHover"></a></p>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock2052097" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv2052097" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572">
<div class="style2572">専任のスタッフが、お部屋にあったインテリア探しをお手伝いさせていただきます。なんでもお気軽にご相談ください。カーテンが仕上がるまでの間の、目隠し用のカーテンの貸出や、採寸（※カーテンご成約の方が対象です）などの嬉しいサービスが充実していますので、ぜひご利用ください。</div>
<p><span style="color: #231815; font-family: 'Hiragino Kaku Gothic Pro', Meiryo, 'MS PGothic', 'MS Gothic', Osaka, sans-serif; font-size: 13.3333px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;</span></p>
</div>
<p><span style="color: #231815; font-family: 'Hiragino Kaku Gothic Pro', Meiryo, 'MS PGothic', 'MS Gothic', Osaka, sans-serif; font-size: 13.3333px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;</span></p>
</div>
</div>
</div>

					</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803066">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock803069">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock803067">
<div class="pbBlock pbBlockBase">
<div>
<div>
<div class="style2685-2733">【1】採寸メジャー・測り方ガイド 進呈</div>
</div>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803070">
		<div class="pbNested pbNestedHorizontalWrapper ">
					<div class="pbHorizontalNested" id="pbBlock803071" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803071" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div>
<div>
<div class="style2572"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/117824102611.jpg" alt="カーテン.jpg" title="カーテン.jpg" style="width: 100%;" width="100%"></div>
</div>
</div>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock803072" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803072" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2650">
<p><span style="font-size: 10pt;">カーテンの測り方がわからなくても安心です。誰でも簡単、正確にカーテンの採寸ができる”専用メジャー”と、”測り方ガイド”をプレゼントいたします。「カーテンの測り方」動画とパンフレットを連動して、ご覧いただくとよりわかりやすくなっております。</span></p>
</div>
</div>
</div>
</div>

					</div>
		</div>

			</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock2052094">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock2052474">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2706">
<div class="cjML10-2264-2292-2296"><a href="/free_present.html"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/1178249424.png" alt="プレゼントの申し込み.png" title="プレゼントの申し込み.png" style="float: right;" class="imgHover"></a></div>
</div>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock2052475">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2706">
<div class="cjML10-2264-2292-2296"><a href="https://www.youtube.com/watch?v=rNDDYOxnX-k" class="pbOpenNewWindow"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/11782494219.png" alt="測り方動画.png" title="測り方動画.png" style="float: right;" class="imgHover"></a></div>
</div>
</div>
</div>

			</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803073">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock803074">
<div class="pbBlock pbBlockBase">
<div>
<div>
<div class="style2685-2733">【2】カーテン採寸 承ります</div>
</div>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803075">
		<div class="pbNested pbNestedHorizontalWrapper ">
					<div class="pbHorizontalNested" id="pbBlock803076" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803076" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/117824103359.jpg" alt="メジャー.jpg" title="メジャー.jpg" style="width: 100%;" width="100%"></div>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock803077" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803077" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2650">
<p><span style="font-size: 10pt;">新築・改築に伴うお客様による複数窓の採寸が困難な場合、専門スタッフによる出張採寸を承ります。</span></p>
<p><span style="font-size: 8pt;">※カーテンご成約の方が対象です</span></p>
</div>
</div>
</div>
</div>

					</div>
		</div>

			</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803078">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock803079">
<div class="pbBlock pbBlockBase">
<div>
<div>
<div class="style2685-2733">【3】カーテン見本吊り貸出し</div>
</div>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803080">
		<div class="pbNested pbNestedHorizontalWrapper ">
					<div class="pbHorizontalNested" id="pbBlock803081" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803081" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/117824102723.jpg" alt="カーテン.jpg" title="カーテン.jpg" style="width: 100%;" width="100%"></div>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock803082" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803082" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572">
<p><span style="font-size: 10pt;">店内のカーテン実物サンプルをご自宅でお試しになれます。実際にお部屋の家具や床色などに合わせると、よりカーテンのコーディネイトがイメージしやすくなります。</span></p>
</div>
</div>
</div>
</div>

					</div>
		</div>

			</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803083">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock803084">
<div class="pbBlock pbBlockBase">
<div>
<div>
<div class="style2685-2733">【4】見本帳（カタログ）貸出し</div>
</div>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803085">
		<div class="pbNested pbNestedHorizontalWrapper ">
					<div class="pbHorizontalNested" id="pbBlock803086" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803086" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/BOOK.jpg" alt="BOOK.jpg" title="BOOK.jpg" style="width: 100%;" width="100%"></div>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock803087" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803087" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572">
<p><span style="font-size: 10pt;">店内のメーカーカタログを貸出し致します。</span><span style="font-size: 10pt;">ご自宅でゆっくりお選びになる時にご活用ください。</span></p>
</div>
</div>
</div>
</div>

					</div>
		</div>

			</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803088">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock803089">
<div class="pbBlock pbBlockBase">
<div>
<div>
<div class="style2685-2733">【5】代替えカーテン貸出し<span style="font-size: 12pt; color: #800000;"><strong><span style="background-color: #ffffff;"><span style="font-size: 14pt;"><br></span></span></strong></span></div>
</div>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803090">
		<div class="pbNested pbNestedHorizontalWrapper ">
					<div class="pbHorizontalNested" id="pbBlock803091" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803091" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/11782410300.jpg" alt="カーテン.jpg" title="カーテン.jpg" style="width: 100%;" width="100%"></div>
<p style="text-align: center;"></p>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock803092" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803092" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572">
<p><span style="font-size: 10pt;">カーテンが仕上がるまでの間、目隠し用のカーテンをご用意いたします。お引越しまでにオーダーカーテンが間に合わない時にぜひご利用ください。</span></p>
</div>
</div>
</div>
</div>

					</div>
		</div>

			</div>
		</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803233">
		<div class="pbNested ">
			<div class="pbNested pbNestedWrapper" id="pbBlock803234">
<div class="pbBlock pbBlockBase">
<div>
<div>
<div class="style2685-2733">【6】レール取付工事承ります<span style="font-size: 12pt; color: #800000;"><strong><span style="background-color: #ffffff;"><span style="font-size: 14pt;"><br></span></span></strong></span></div>
</div>
</div>
</div>

			</div>
			<div class="pbNested pbNestedWrapper" id="pbBlock803235">
		<div class="pbNested pbNestedHorizontalWrapper ">
					<div class="pbHorizontalNested" id="pbBlock803236" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803236" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572"><img src="/shop/user_data/packages/CJoukoku/img/testlp/supportservice/117824103326.jpg" alt="工事.jpg" title="工事.jpg" style="width: 100%;" width="100%"></div>
</div>
</div>
</div>

					</div>
					<div class="pbHorizontalNested" id="pbBlock803237" style="overflow:hidden;clear:right;">
					
					<div id="pbNestDiv803237" style="margin:0;padding:0;border-width:0;line-height:normal;text-indent:0;">
<div class="pbBlock pbBlockBase">
<div>
<div class="style2572">
<div class="style2481">
<p><span style="font-size: 10pt;">専門のスタッフによるレールの取付工事をご利用ください。</span></p>
<p><span style="font-size: 10pt;">年間2万棟以上の工事を承っております。</span></p>
</div>
</div>
</div>
</div>
</div>

					</div>
		</div>

			</div>
		</div>

			</div>
		</div>

			</div>
		</div>
	</div>

</div>
