<!--{*
/*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->





<!--{strip}-->
    <!--{assign var=errCnt value=0}-->
        <div class="member_regist_forms">
            <table>
                <tbody>
                    <tr>
                        <th>お名前
                            <span></span>
                        </th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`name01"}-->
                            <!--{assign var=key2 value="`$prefix`name02"}-->
                            <div class="name_form">
                                <input id="<!--{$key1}-->" type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: active;" placeholder="姓" />
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                                <div class="name_form">
                                <input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->; ime-mode: active;" placeholder="名" />
                                <p class="err"><!--{$arrErr[$key2]}--></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>お名前（フリガナ）
                            <span></span>
                        </th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`kana01"}-->
                            <!--{assign var=key2 value="`$prefix`kana02"}-->
                            <div class="name_form">
                                <input type="text" id="<!--{$key1}-->" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: active;" placeholder="セイ" />
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                            <div class="name_form">
                                <input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->; ime-mode: active;" placeholder="メイ" />
                                <p class="err"><!--{$arrErr[$key2]}--></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>会社名</th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`company_name"}-->
                            <p>法人で登録される場合は会社名をご入力ください。</p>
                            <div class="company_form">
                                <input type="text" id="<!--{$key1}-->" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: active;" placeholder="会社名　部署名" />
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>住所
                        <span></span>
                        </th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`zip01"}-->
                            <!--{assign var=key2 value="`$prefix`zip02"}-->
                            <!--{assign var=key3 value="`$prefix`pref"}-->
                            <!--{assign var=key4 value="`$prefix`addr01"}-->
                            <!--{assign var=key5 value="`$prefix`addr02"}-->
                            <!--{assign var=key6 value="`$prefix`country_id"}-->
                            <!--{assign var=key7 value="`$prefix`zipcode"}-->
                            <!--{if !$smarty.const.FORM_COUNTRY_ENABLE}-->
                            <!--{/if}-->
                            <p>郵便番号（ご入力いただき、ボタンを押すと住所が自動表示されます）</p>
                            <div class="postcode_form">
                                <input type="tel" class="zip1" id="<!--{$key1}-->" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: disabled;" placeholder="123" />
                                <span class="">ー</span>
                                <input type="tel" class="zip2" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->; ime-mode: disabled;" placeholder="4567" />
                                <a class="auto_address" href="javascript:eccube.getAddress('<!--{$smarty.const.INPUT_ZIP_URLPATH}-->', '<!--{$key1}-->', '<!--{$key2}-->', '<!--{$key3}-->', '<!--{$key4}-->');">住所自動入力</a>
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                            <div class="pref_select">
                                <select id="<!--{$key3}-->" name="<!--{$key3}-->" style="<!--{$arrErr[$key3]|sfGetErrorColor}-->">
                                    <option value="">都道府県を選択</option>
                                    <!--{html_options options=$arrPref selected=$arrForm[$key3].value|h}-->
                                </select>
                                <p class="err"><!--{$arrErr[$key3]}--></p>
                            </div>
                            <div class="address_form">
                                <input type="text" name="<!--{$key4}-->" value="<!--{$arrForm[$key4].value|h}-->" style="<!--{$arrErr[$key4]|sfGetErrorColor}-->; ime-mode: active;" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS1}-->" />
                                <p class="err"><!--{$arrErr[$key4]}--></p>
                            </div>
                            <div class="address_form">
                                <input type="text" name="<!--{$key5}-->" value="<!--{$arrForm[$key5].value|h}-->" style="<!--{$arrErr[$key5]|sfGetErrorColor}-->; ime-mode: active;" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS2}-->" />
                                <p class="err"><!--{$arrErr[$key5]}--></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>電話番号
                            <span></span>
                        </th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`tel01"}-->
                            <!--{assign var=key2 value="`$prefix`tel02"}-->
                            <!--{assign var=key3 value="`$prefix`tel03"}-->
                            <div class="phone_form">
                                <input type="tel" class="phone" id="<!--{$key1}-->" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: disabled;" />
                                <span>ー</span>
                                <input type="tel" class="phone" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->; ime-mode: disabled;" />
                                <span>ー</span>
                                <input type="tel" class="phone" name="<!--{$key3}-->" value="<!--{$arrForm[$key3].value|h}-->" maxlength="<!--{$arrForm[$key3].length}-->" style="<!--{$arrErr[$key3]|sfGetErrorColor}-->; ime-mode: disabled;" />
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>メールアドレス<span></span></th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`email"}-->
                            <!--{assign var=key2 value="`$prefix`email02"}-->
                            <div class="company_form">
                                <input type="email" id="<!--{$key1}-->" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: disabled;" placeholder="user@domain.com" />
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                            <p>確認のため2度入力してください。</p>
                            <div class="company_form">
                                <input type="email" id="<!--{$key2}-->" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->; ime-mode: disabled;" placeholder="user@domain.com" />
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>性別</th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`sex"}-->
                            <ul>
                                <div class="sex_radio">
                                    <li>
                                        <input type="radio" class="sex" value="2" id="female" name="sex">
                                        <label for="female">女性</label>
                                    </li>
                                    <li>
                                        <input type="radio" class="sex" value="1" id="men" name="sex">
                                        <label for="men">男性</label>
                                    </li>
                                </div>
                            </ul>
                            <p class="err"><!--{$arrErr[$key1]}--></p>
                        </td>
                    </tr>
                    <tr>
                        <th>職業</th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`job"}-->
                            <div class="job_select">
                                <select>
                                    <option value="" style="display: none;">入力してください</option>
                                    <!--{html_options options=$arrJob selected=$arrForm[$key1].value}-->
                                </select>
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>生年月日</th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`year"}-->
                            <!--{assign var=key2 value="`$prefix`month"}-->
                            <!--{assign var=key3 value="`$prefix`day"}-->
                            <!--{assign var=errBirth value="`$arrErr.$key1``$arrErr.$key2``$arrErr.$key3`"}-->
                            <div class="birth_select">
                                <!--                      <div class="year">-->
                                <label>
                                    <select id="<!--{$key1}-->" name="<!--{$key1}-->" style="<!--{$errBirth|sfGetErrorColor}-->">
                                        <option value="" style="display: none;">年</option>
                                        <!--{html_options options=$arrYear selected=$arrForm[$key1].value|default:''}-->
                                    </select>
                                </label>
                                <!--                      </div>-->
                                <span class="">ー</span>
                                <label>
                                    <select name="<!--{$key2}-->" style="<!--{$errBirth|sfGetErrorColor}-->">
                                        <option value="" style="display: none;">月</option>
                                        <!--{html_options options=$arrMonth selected=$arrForm[$key2].value|default:''}-->
                                    </select>
                                </label>
                                <span class="">ー</span>
                                <label>
                                    <select class="form-control" name="<!--{$key3}-->" style="<!--{$errBirth|sfGetErrorColor}-->">
                                        <option value="" style="display: none;">日</option>
                                        <!--{html_options options=$arrDay selected=$arrForm[$key3].value|default:''}-->
                                    </select>
                                </label>
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                                <p class="err"><!--{$arrErr[$key2]}--></p>
                                <p class="err"><!--{$arrErr[$key3]}--></p>
                            </div>
                        </td>
                    </tr>
                <!--{if $flgFields > 2}-->
                    <tr>
                        <th>
                            <span>希望するパスワード<br>（記号可）</span>
                            <span></span>
                        </th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`password"}-->
                            <!--{assign var=key2 value="`$prefix`password02"}-->
                            <div class="password_form">
                                <input type="password" id="<!--{$key1}-->" class="box380 top form-control" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" placeholder="半角英数字<!--{$smarty.const.PASSWORD_MIN_LEN}-->～<!--{$smarty.const.PASSWORD_MAX_LEN}-->文字" />
                                <p class="err"><!--{$arrErr[$key1]}--></p>
                            </div>
                            <p>確認のため2度入力してください。</p>
                            <div class="password_form">
                                <input type="password" class="box380 form-control" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" style="<!--{$arrErr[$key1]|cat:$arrErr[$key2]|sfGetErrorColor}-->" placeholder="パスワード確認用" />
                                <p class="err"><!--{$arrErr[$key2]}--></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <span>パスワードを<br>忘れた時のヒント</span>
                            <span></span>
                        </th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`reminder"}-->
                            <!--{assign var=key2 value="`$prefix`reminder_answer"}-->
                            <div class="password_hint_select">
                                <select id="<!--{$key1}-->" name="<!--{$key1}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->">
                                    <option value="" selected="selected">質問を選択してください</option>
                                    <!--{html_options options=$arrReminder selected=$arrForm[$key1].value}-->
                                </select>
                            </div>
                            <p class="err"><!--{$arrErr[$key1]}--></p>
                            <div class="password_hint_form">
                                <input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->; ime-mode: active;" placeholder="質問の答え" />
                            </div>
                            <p class="err"><!--{$arrErr[$key2]}--></p>
                        </td>
                    </tr>
                    <tr>
                        <th>メールマガジンの送付<span></span></th>
                        <td>
                            <!--{assign var=key1 value="`$prefix`mailmaga_flg"}-->
                            <div style="<!--{if strlen($arrErr[$key1] ) > 0}-->background-color:#ffe8e8;padding:10px;border:2px solid #ccc;<!--{/if}-->">
                                <ul class="">
                                    <div class="mailmagazine_checkbox">
                                        <li>
                                            <input type="radio" class="mailmagazine" name="mailmaga_flg" value="1" id="HTMLmail">
                                            <label for="HTMLmail">HTMLメール</label>
                                        </li>
                                    </div>
                                    <div class="mailmagazine_checkbox">
                                        <li>
                                            <input type="radio" class="mailmagazine" name="mailmaga_flg" value="2" id="textmail" >
                                            <label for="textmail">テキストメール</label>
                                        </li>
                                    </div>
                                    <div class="mailmagazine_checkbox">
                                        <li>
                                            <input type="radio" class="mailmagazine" name="mailmaga_flg" value="3" id="deny" >
                                            <label for="deny">希望しない</label>
                                        </li>
                                    </div>
                                </ul>
                            </div>
                            <p class="err"><!--{$arrErr[$key1]}--></p>
                        </td>
                    </tr>
                <!--{/if}-->
                </tbody>
            </table>
        </div>
<!--{/strip}-->
