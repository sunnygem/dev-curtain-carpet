<section class="voice">
	<h2><span><small>お客様の声</small>VOICE</span></h2>
    <div class="voiceContents">
    <div class="voiceLeft">
    <ul>
    	<li>
            <dl>
                <dt><img src="<!--{$TPL_URLPATH}-->img/voice/001.png" data-original="<!--{$TPL_URLPATH}-->img/voice/001.png" alt="" class="lazy"></dt>
                <dd>
                <ul>
                    <li><img src="<!--{$TPL_URLPATH}-->img/voice/002.png" data-original="<!--{$TPL_URLPATH}-->img/voice/002.png" alt="" class="lazy"></li>
                    <li><img src="<!--{$TPL_URLPATH}-->img/voice/003.png" data-original="<!--{$TPL_URLPATH}-->img/voice/003.png" alt="" class="lazy"></li>
                </ul>
                </dd>
            </dl>
        </li>
    	<li class="voiceText">
        	<dl>
                <dt><img src="<!--{$TPL_URLPATH}-->img/voice/w001.png" data-original="<!--{$TPL_URLPATH}-->img/voice/w001.png" alt="" class="lazy"></dt>
            	<dd><h3>イメージしていた通りになって大満足！</h3>
                <p class="name">東京都 A様</p>
                </dd>
            </dl>
            <p>購入された商品：<br>
ドレープカーテン、レースカーテン、シェード、ロールスクリーン</p>
<p>受けたサービス：カーテンレールの取付工事、採寸</p>
	<ol>
    	<li>Q1.購入した商品はいかがでしたか？<br>
イメージしていたインテリア通りになって本当に大満足です。</li>
		<li>Q2.購入の決め手は何ですか？<br>
カーテンのデザインがステキすぎたので。</li>
		<li>Q3.取付工事のスタッフの対応や仕上がりはいかがでしたか？<br>
ショールームのスタッフさんがひとつひとつ丁寧な説明と私たちがイメージしていたインテリアをしっかり汲んで提案してくれたので、”夫婦で本当に良かったね”と話していました。</li>
</ol>
<p>取付工事のスタッフさんも丁寧に親切にやっていただいたので良かったです。</p>
        </li>
    </ul>
    </div>
    <div class="voiceRight">
    <ul>
    	<li>
        	<dl>
            	<dt><img src="<!--{$TPL_URLPATH}-->img/voice/p001.png" data-original="<!--{$TPL_URLPATH}-->img/voice/p001.png" alt="モンタナ" class="lazy"></dt>
            	<dd>
                <h3>エキゾチックで格調高いデザインが気に入りました。</h3>
                <p class="name">G様</p>
                <h4>使用された商品：モンタナ（カーペット）</h4>
                <p class="voiceComent">エキゾチックで格調高いデザインが気に入りました♥光沢があり、見る角度によって色の濃さが変化！ アクリルと綿の混紡で、優しい手触りです。</p>
                </dd>
            </dl>
        </li>
    	<li>
        	<dl>
            	<dt><img src="<!--{$TPL_URLPATH}-->img/voice/p002.png" data-original="<!--{$TPL_URLPATH}-->img/voice/p002.png" alt="モンタナ" class="lazy"></dt>
            	<dd>
                <h3>豪華なデザインがお気に入りです。</h3>
                <p class="name">M様</p>
                <h4>使用された商品：エルマ（カーペット）</h4>
                <p class="voiceComent">豪華なデザインがお気に入りの輸入物カーペット（トルコ製）の「エルマ」を愛用しています。 この素材にしては珍しく、手触りが良好です！</p>
                </dd>
            </dl>
        </li>
    </ul>
    <!--<div class="btn001"><a href="#" target="_top">もっと見る</a></div>-->
    </div>
    </div>
</section>