<style>
	select.kazu {
		width:60px;
		padding-right:28px !important;
	}
	.consultationBtn01 a {
		cursor:pointer;
	}
	.favoriteBtn02 p {
		position: relative;
		display: block;
		width: 100%;
		font-size: 13px;
		text-align: center;
		border: solid 1px #aaa;
		-webkit-border-radius: 3px;
		border-radius: 3px;
		line-height: 2.5em;
	}
</style>


<div id="footerCartBox">
	<ul>
		<li><h3><!--{$arrProduct.name|h}--></h3>
			<!--<p></p>-->
		</li>
		<li>
			<dl>
			<dt>数量選択</dt>
			<dd>
				<ul>
					<li>
						<div class="selectBox02 numberSelect">
							<select class="kazu" name="quantity">
							<option value="" hidden></option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							</select>
						</div>
					</li>
					<li class="price02">
						&yen;<span id="price02_default_2">-</span><span id="price02_dynamic_2"></span>
						<small>(税込)</small>
					</li>
				</ul>
			</dd>
		</li>
		<li>
			<ul>
				<li>
					<div class="cartBtn01"><a href="#" onClick="form_submit();return false;">カートに入れる</a></div>
				</li>
				<li>
					<!--★お気に入り登録★-->
					<!--{if $smarty.const.OPTION_FAVORITE_PRODUCT == 1 && $tpl_login === true}-->
					<!--{assign var=add_favorite value="add_favorite`$product_id`"}-->
					<!--{if $arrErr[$add_favorite]}-->
					<div class="attention"><!--{$arrErr[$add_favorite]}--></div>
					<!--{/if}-->
					<!--{if !$is_favorite}-->
					<div class="favoriteBtn01 mb5 favorite_btn">
						<a href="javascript:eccube.changeAction('?product_id=<!--{$arrProduct.product_id|h}-->'); eccube.setModeAndSubmit('add_favorite','favorite_product_id','<!--{$arrProduct.product_id|h}-->');">お気に入りに登録する</a>
					</div>
						<!--{else}-->
						<div class="favoriteBtn02 mb5 favorite_btn">
							<p>お気に入りに登録済み</p>
						</div>
							<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/jquery.ui.core.min.js"></script>
							<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/jquery.ui.widget.min.js"></script>
							<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/jquery.ui.position.min.js"></script>
							<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/jquery.ui.tooltip.min.js"></script>
							<script type="text/javascript">
							var favoriteButton = $("#add_favorite_product");
							favoriteButton.tooltip();

							<!--{if $just_added_favorite == true}-->
							favoriteButton.load(function(){ $(this).tooltip("open") });
							$(function(){
							var tid = setTimeout('favoriteButton.tooltip("close")',5000);
							});
							<!--{/if}-->
							</script>
							<!--{/if}-->
							<!--{else}-->
							<div class="favoriteBtn02 mb5 favorite_btn">
								<a href="/shop/mypage/login.php">お気に入りに登録する</a>
							</div>
							<!--{/if}-->
							<div class="consultationBtn01">
								<form action="/reserve/" method="POST" name="form_f">
									<input type="hidden" name="product" value=""/>
									<a>この商品を店舗で相談する</a>
								</form>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</li>
	</ul>
</div>


<script>
    $(function(){
		$("select.kazu").on("change",function(){
			$("dl.quantity input.kazu").val($(this).val());
		});
	});

</script>


<script>
$(function() {
    $('.consultationBtn01 a').on('click', function() {
		putcookie($("#footerCartBox ul li h3").text());
		$(".consultationBtn01 input[name=product]").val($("#footerCartBox ul li h3").text());
		form_f.submit();
	});
});
    function putcookie(product) {
      if (('localStorage' in window) && (window.localStorage !== null)) {
          localStorage.setItem('oukoku_product', product);
      }
    }
var unicodeEscape = function(str) {
    var code, pref = {1:'\\u000',2:'\\u00', 3:'\\u0',4:'\\u'};
    return str.replace(/\W/g, function(c) {
        return pref[(code = c.charCodeAt(0).toString(16)).length] + code;
    });
};

</script>
