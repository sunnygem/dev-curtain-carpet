<section class="information">
	<div class="informationInner">
	<h2><span>INFORMATION</span></h2>
    	<ul>
        	<li><a href="<!--{$smarty.const.TOP_URL}-->user_data/measurements.php" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/001.png" data-original="i<!--{$TPL_URLPATH}-->img/information/001.png" alt="" class="lazy"></dt>
                    <dd><h3>採寸方法</h3></dd>
                </dl>
            </li>
        	<li><a href="<!--{$smarty.const.TOP_URL}-->user_data/care.php" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/002.png" data-original="<!--{$TPL_URLPATH}-->img/information/002.png" alt="" class="lazy"></dt>
                    <dd><h3>お手入れ方法</h3></dd>
                </dl>
            </li>
 <!--       	<li><a href="<!--{$smarty.const.TOP_URL}-->" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/003.png" data-original="<!--{$TPL_URLPATH}-->img/information/003.png" alt="" class="lazy"></dt>
                    <dd><h3>採寸･取付サービス</h3></dd>
                </dl>
            </li>
        	<li><a href="<!--{$smarty.const.TOP_URL}-->" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/004.png" data-original="<!--{$TPL_URLPATH}-->img/information/004.png" alt="" class="lazy"></dt>
                    <dd><h3>商品について</h3></dd>
                </dl>
            </li>
-->
        	<li><a href="<!--{$smarty.const.TOP_URL}-->user_data/order.php" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/005.png" data-original="<!--{$TPL_URLPATH}-->img/information/005.png" alt="" class="lazy"></dt>
                    <dd><h3>購入方法</h3></dd>
                </dl>
            </li>
        	<li><a href="<!--{$smarty.const.TOP_URL}-->user_data/flow.php" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/006.png" data-original="<!--{$TPL_URLPATH}-->img/information/006.png" alt="" class="lazy"></dt>
                    <dd><h3>お届けまでの流れ</h3></dd>
                </dl>
            </li>
        	<li><a href="/reserve/" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/007.png" data-original="<!--{$TPL_URLPATH}-->img/information/007.png" alt="" class="lazy"></dt>
                    <dd><h3>店舗予約について</h3></dd>
                </dl>
            </li>
        	<li><a href="/service/tenpo/" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/008.png" data-original="<!--{$TPL_URLPATH}-->img/information/008.png" alt="" class="lazy"></dt>
                    <dd><h3>店舗サービス</h3></dd>
                </dl>
            </li>
 <!--
       	<li><a href="<!--{$smarty.const.TOP_URL}-->" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/009.png" data-original="<!--{$TPL_URLPATH}-->img/information/009.png" alt="" class="lazy"></dt>
                    <dd><h3>生地サンプル</h3></dd>
                </dl>
            </li>
        	<li><a href="<!--{$smarty.const.TOP_URL}-->" class="linkA"></a>
            	<dl>
                	<dt><img src="<!--{$TPL_URLPATH}-->img/information/010.png" data-original="<!--{$TPL_URLPATH}-->img/information/010.png" alt="" class="lazy"></dt>
                    <dd><h3>オーダーと既成の違い</h3></dd>
                </dl>
            </li>
-->
        </ul>
    </div>
    </section>