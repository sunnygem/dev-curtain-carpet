<!--{if $arrCheckItems}-->
<div class="product_related check_item">
    <div class="ttl">
        <div><img src="<!--{$TPL_URLPATH}-->img/products/product_ttl_checked.png" alt="最近チェックした商品"></div>
        <h3>最近、チェックした商品</h3>
    </div>
    <div class="">
        <ul>
        <!--{section name=cnt loop=$arrCheckItems}-->
            <li>
                <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrCheckItems[cnt].product_id}-->">
                    <img src="<!--{$smarty.const.ROOT_URLPATH}-->resize_image.php?image=<!--{$arrCheckItems[cnt].main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrCheckItems[cnt].name|h}-->"/>
                </a>
                <h4><!--{$arrCheckItems[cnt].name}--></h4>
                <div class="pricebox sale_price">
                    <p class="price <!--{if $arrCheckItems[cnt].sale_flg === "1"}-->sale<!--{/if}-->">
                    <!--{if $arrCheckItems[cnt].sale_flg ===
                    "1"}--> <span class="normal_price"> <!--{$arrCheckItems[cnt].price01_min|n2s}--> 円～ </span> <!--{/if}-->
                    <span id="price02_default_<!--{$id}-->"><!--{strip}-->
                    <!--{if $arrCheckItems[cnt].price02_min == $arrCheckItems[cnt].price02_max}-->
                        <!--{$arrCheckItems[cnt].price02_min|n2s}-->円
                    <!--{else}-->
                        <!--{$arrCheckItems[cnt].price02_min|n2s}-->円～
                    <!--{/if}-->
                    </span>
                    <!--{/strip}--><span style="display: inline-block;">(税抜)</span>
                    </p>
                </div><!--/.pricebox-->
            </li>
        <!--{/section}-->
        </ul>
    </div>
</div> <!-- .product_related -->
<!--{/if}-->
