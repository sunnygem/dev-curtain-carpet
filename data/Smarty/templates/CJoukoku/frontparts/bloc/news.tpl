<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{strip}-->
<article id="news">
    <h3><img src="/shop/user_data/packages/CJoukoku/img/news/top_news_ttl.png" alt="NEWS"></h3>
	<ul>
        <!--{section name=data loop=$arrNews}-->
        <!--{assign var="date_array" value="-"|explode:$arrNews[data].cast_news_date}-->
    	<li><time><!--{$date_array[1]}-->.<!--{$date_array[2]}--></time>
            <a href="#news-collapse<!--{$smarty.section.data.index}-->" data-toggle="collapse" data-parent="#news-area-accordion">
                <!--{$arrNews[data].news_title|h|nl2br}-->
            </a>
      </li>
      
                <!--{/section}-->
    </ul>
</article>
<!--{/strip}-->
