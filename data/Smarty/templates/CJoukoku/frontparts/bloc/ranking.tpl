<section class="ranking">
	<h2>
        <img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_ttl.png">
    </h2>
    <div class="rankMenu">
        <ul class="tabs">
            <li class="tab active">
                カーテン
            </li>
            <li class="tab">
                カーペット
            </li>
        </ul>
        <section class="curtain">
            <ul class="rank">
                <li>
                    <div class="rank"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_rank1.png"></div>
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/detail.php?product_id=458"><img src="<!--{$smarty.const.ROOT_URLPATH}-->upload/save_image/samacBL/samacBL_1.jpg"></a>
                    <h4>サマク（ブルー）</h4>
                </li>
                <li>
                    <div class="rank"><img src="<!--{$smarty.const.ROOT_URLPATH}-->user_data/packages/CJoukoku/img/ranking/top_ranking_rank2.png"></div>
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/detail.php?product_id=452"><img src="<!--{$smarty.const.ROOT_URLPATH}-->upload/save_image/warfisBL/warfisBL_1.jpg"></a>
                    <h4>ワルフィス（ブルー）</h4>
                </li>
                <li>
                    <div class="rank"><img src="<!--{$smarty.const.ROOT_URLPATH}-->user_data/packages/CJoukoku/img/ranking/top_ranking_rank3.png"></div>
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/detail.php?product_id=477"><img src="<!--{$smarty.const.ROOT_URLPATH}-->upload/save_image/furonGRY/furonGRY_1.jpg"></a>
                    <h4>フロン (グレー)</h4>
                </li>
            </ul>
      </section>
      <section class="carpet">
        <ul class="rank">
            <li>
                <div class="rank"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_rank1.png"></div>
                <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_1.png"></a>
                <h4>遮光1級付き厚地オーダーカーテン</h4>
            </li>
            <li>
                <div class="rank"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_rank2.png"></div>
                <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_2.png"></a>
                <h4>遮光1級付き厚地オーダーカーテン</h4>
            </li>
            <li>
                <div class="rank"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_rank3.png"></div>
                <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_3.png"></a>
                <h4>遮光1級付き厚地オーダーカーテン</h4>
            </li>
        </ul>
      </section>
      </div>
      
<!--<div class="btn001"><a href="#" target="_top"><i><img src="<!--{$TPL_URLPATH}-->img/icon/crown_icon.png" alt="crown"></i>ランキング商品をもっと見る</a></div>-->
<br>
</section>
