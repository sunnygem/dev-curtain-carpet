    <div id="left">
      <section class="loginLeft">
		  <div class="guestLeft">
		  <!--{if $tpl_login}-->
		  ようこそ！<!--{$tpl_name1|h}--> <!--{$tpl_name2|h}--> 様
　　　<!--{else}-->
　　　ようこそ！ゲスト様
　　　<!--{/if}-->
        </div>
		   <!--{if $tpl_login }-->
		   <div class="btn002 logIcon01"> <a href="<!--{$smarty.const.TOP_URL}-->mypage/">マイページ</a> </div>
		   <!--{else}-->
        <div class="btn002 logIcon01"> <a href="<!--{$smarty.const.TOP_URL}-->mypage/login.php">ログイン</a> </div>
        <div class="btn002 newIcon01"> <a href="<!--{$smarty.const.TOP_URL}-->entry/kiyaku.php">新規登録</a> </div>
		  <!--{/if}-->
		 
        <h3><a href="<!--{$smarty.const.TOP_URL}-->user_data/merit.php"><span>会員特典はこちら</span></a></h3>
      </section>
      <section class="categoryLeft">
        <h2><span><i><img src="<!--{$TPL_URLPATH}-->img/icon/search01.png" alt="カテゴリから探す"></i>カテゴリから探す</span></h2>
        <h3><i><img src="<!--{$TPL_URLPATH}-->img/icon/curtain.png" alt="カーテン"></i>カーテン</h3>
        <dl>
          <dt>テイストから選ぶ</dt>
          <dd>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1708" target="_top"><span>無地</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1709" target="_top"><span>ナチュラル</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1710" target="_top"><span>モダン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1711" target="_top"><span>エレガンス</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1712" target="_top"><span>クラシック</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1713" target="_top"><span>カジュアル</span></a></h4>
          </dd>
          <dt>機能から選ぶ</dt>
          <dd>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1730" target="_top"><span>遮光カーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1729" target="_top"><span>遮熱・断熱・蓄熱カーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1728" target="_top"><span>形状記憶カーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1727" target="_top"><span>防炎カーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1726" target="_top"><span>丸洗いOKカーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1716" target="_top"><span>レースカーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1724" target="_top"><span>裏地付きカーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1723" target="_top"><span>UVカーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1722" target="_top"><span>遮音・防音カーテン</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1721" target="_top"><span>ミラーカーテン</span></a></h4>
          </dd>
        </dl>
<h3><i><img src="<!--{$TPL_URLPATH}-->img/icon/mado.png" alt="窓まわり" width="30px"></i>窓まわり</h3>
        <dl>
          <dt>オーダーメイド</dt>
          <dd>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1764" target="_top"><span>ブラインド</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1769" target="_top"><span>シェード</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1770" target="_top"><span>スクリーン</span></a></h4>

          </dd>
        
        </dl>
<h3><i><img src="<!--{$TPL_URLPATH}-->img/icon/carpet2.png" alt="カーペット" width="30px"></i>カーペット</h3>
        <dl>
          <dt><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1748" target="_top">用途から選ぶ</a></dt>
          <dd>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1761" target="_top"><span>ホットカーペット</span></a></h4>
            <h4><a href="<!--{$smarty.const.TOP_URL}-->products/list.php?category_id=1762" target="_top"><span>ラグ</span></a></h4>

          </dd>
        
        </dl>
      </section>
      <!--{*<section class="leftBnrBox"> <a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->active/order/" target="_top"> <img src="<!--{$TPL_URLPATH}-->img/banner/l001.png" alt="オーダーメイドソファーカバー" width="100%"> </a> </section>*}--><!-- 本番との差異 devの記載は一旦コメントアウト -->
      <section class="leftBnrBox"> <a href="/active/order/" target="_top"> <img src="<!--{$TPL_URLPATH}-->img/banner/l001.png" alt="オーダーメイドソファーカバー" width="100%"> </a> </section>
      <section class="leftbarNav">
        <ul>
          <li class="queIcon01"><a href="<!--{$smarty.const.TOP_URL}-->user_data/faq.php" target="_top">よくある質問</a></li>
          <li class="voiIcon01"><a href="<!--{$smarty.const.TOP_URL}-->user_data/voice.php" target="_top">お客様の声</a></li>
          <li class="guiIcon01"><a href="<!--{$smarty.const.TOP_URL}-->user_data/shopping_guide.php" target="_top">ショッピングガイド</a></li>
          <li class="infIcon01"><a href="<!--{$smarty.const.TOP_URL}-->user_data/information.php" target="_top">インフォメーション</a></li>
          <li class="inqIcon01"><a href="<!--{$smarty.const.TOP_URL}-->contact/index.php" target="_top">お問合せ</a></li>
        </ul>
      </section>
    </div>
