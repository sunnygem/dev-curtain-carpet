<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->
<style>
.contact-page p {
    margin-bottom: 5px; 
}
	.contact-page input,.contact-page select {
		margin-bottom:5px;
	}
</style>
<div id="contents_wrapper" class="contact">

    <div class="member_regist_wrapper" >
        <div class="member_regist_title" >
            <h2><!--{$tpl_title|h}--></h2>
        </div>

        <div class="member_regist_flow">
            <ul>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/contact/contact_step01_on.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/contact/contact_step02.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/contact/contact_step03.png">
                </li>
            </ul>
        </div>
        <!--table class="step">
            <tr>
                <th class="orange"><span>STEP1</span>入力</th>
                <th><span>STEP2</span>確認</th>
                <th><span>STEP3</span>送信完了</th>
            </tr>
        </table-->
            
        <div class="member_regist_remark">
            <p>
                必要事項をご入力ください｡<br>
                入力内容をご確認ください｡<br>
                後日､担当者からご連絡致します｡
            </p>
            <p>
                お問合せフォームの必須項目にご入力いただき「確認ページへ」ボタンを押してください。<br>
                お問い合わせ内容は、なるべく具体的にご記入頂ますようお願い致します<br>
                ※お問合せの前に必ず、<a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/faq.php">よくある質問</a> や<a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/shopping_guide.php">ご利用ガイド</a>を御覧ください。
            </p>
        </div>
        <h4 style="margin-bottom:30px;">お問合せ内容を入力してください｡</h4>

        <form name="form1" id="form1" class="form-horizontal" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />

                <div class="member_regist_forms">
                    <table>
                        <tbody>
                            <tr>
                                <th>お名前
                                    <span></span>
                                </th>
                                <td>
                                    <!--{assign var=key1 value="`$prefix`name01"}-->
                                    <!--{assign var=key2 value="`$prefix`name02"}-->
                                    <div class="name_form">
                                        <input id="<!--{$key1}-->" type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: active;" placeholder="姓" />
                                        <p class="err"><!--{$arrErr[$key1]}--></p>
                                    </div>
                                    <div class="name_form">
                                        <input type="text" class="" name="name02" value="<!--{$arrForm.name02.value|default:$arrData.name02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.name02|sfGetErrorColor}-->; ime-mode: active;" placeholder="名" />
                                        <p class="err"><!--{$arrErr[$key2]}--></p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>お名前（フリガナ）
                                    <span></span>
                                </th>
                                <td>
                                    <!--{assign var=key1 value="`$prefix`kana01"}-->
                                    <!--{assign var=key2 value="`$prefix`kana02"}-->
                                    <div class="name_form">
                                        <input type="text" id="kana01" class="" name="kana01" value="<!--{$arrForm.kana01.value|default:$arrData.kana01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.kana01|sfGetErrorColor}-->; ime-mode: active;" placeholder="セイ" />
                                        <p class="err"><!--{$arrErr[$key1]}--></p>
                                    </div>
                                    <div class="name_form">
                                        <input type="text" class="box120 form-control" name="kana02" value="<!--{$arrForm.kana02.value|default:$arrData.kana02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.kana02|sfGetErrorColor}-->; ime-mode: active;" placeholder="メイ" />
                                        <p class="err"><!--{$arrErr[$key2]}--></p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>住所
                                </th>
                                <td>
                                    <!--{assign var=key1 value="`$prefix`zip01"}-->
                                    <!--{assign var=key2 value="`$prefix`zip02"}-->
                                    <!--{assign var=key3 value="`$prefix`pref"}-->
                                    <!--{assign var=key4 value="`$prefix`addr01"}-->
                                    <!--{assign var=key5 value="`$prefix`addr02"}-->
                                    <!--{assign var=key6 value="`$prefix`country_id"}-->
                                    <!--{assign var=key7 value="`$prefix`zipcode"}-->
                                    <!--{if !$smarty.const.FORM_COUNTRY_ENABLE}-->
                                    <!--{/if}-->
                                    <p>郵便番号（ご入力いただき、ボタンを押すと住所が自動表示されます）</p>
                                    <div class="postcode_form">
                                        <input type="tel" name="zip01" id="zip01" class="zip1" value="<!--{$arrForm.zip01.value|default:$arrData.zip01|h}-->" maxlength="<!--{$smarty.const.ZIP01_LEN}-->" style="<!--{$arrErr.zip01|sfGetErrorColor}-->; ime-mode: disabled;" placeholder="123" />
                                        <span class="">ー</span>
                                        <input type="tel" name="zip02" class="zip2" value="<!--{$arrForm.zip02.value|default:$arrData.zip02|h}-->" maxlength="<!--{$smarty.const.ZIP02_LEN}-->" style="<!--{$arrErr.zip02|sfGetErrorColor}-->; ime-mode: disabled;" placeholder="4567" />
                                        <a class="auto_address" href="javascript:eccube.getAddress('<!--{$smarty.const.INPUT_ZIP_URLPATH}-->', '<!--{$key1}-->', '<!--{$key2}-->', '<!--{$key3}-->', '<!--{$key4}-->');">住所自動入力</a>
                                        <p class="err"><!--{$arrErr[$key1]}--></p>
                                    </div>
                                    <div class="pref_select">
                                        <select id="<!--{$key3}-->" name="<!--{$key3}-->" style="<!--{$arrErr[$key3]|sfGetErrorColor}-->">
                                            <option value="">都道府県を選択</option>
                                            <!--{html_options options=$arrPref selected=$arrForm[$key3].value|h}-->
                                        </select>
                                        <p class="err"><!--{$arrErr[$key3]}--></p>
                                    </div>
                                    <div class="address_form">
                                        <input type="text" name="<!--{$key4}-->" value="<!--{$arrForm[$key4].value|h}-->" style="<!--{$arrErr[$key4]|sfGetErrorColor}-->; ime-mode: active;" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS1}-->" />
                                        <p class="err"><!--{$arrErr[$key4]}--></p>
                                    </div>
                                    <div class="address_form">
                                        <input type="text" name="<!--{$key5}-->" value="<!--{$arrForm[$key5].value|h}-->" style="<!--{$arrErr[$key5]|sfGetErrorColor}-->; ime-mode: active;" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS2}-->" />
                                        <p class="err"><!--{$arrErr[$key5]}--></p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>電話番号
                                </th>
                                <td>
                                    <!--{assign var=key1 value="`$prefix`tel01"}-->
                                    <!--{assign var=key2 value="`$prefix`tel02"}-->
                                    <!--{assign var=key3 value="`$prefix`tel03"}-->
                                    <div class="phone_form">
                                        <input type="tel" class="phone" id="<!--{$key1}-->" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: disabled;" />
                                        <span>ー</span>
                                        <input type="tel" class="phone" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->; ime-mode: disabled;" />
                                        <span>ー</span>
                                        <input type="tel" class="phone" name="<!--{$key3}-->" value="<!--{$arrForm[$key3].value|h}-->" maxlength="<!--{$arrForm[$key3].length}-->" style="<!--{$arrErr[$key3]|sfGetErrorColor}-->; ime-mode: disabled;" />
                                        <p class="err"><!--{$arrErr[$key1]}--></p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>メールアドレス<span></span></th>
                                <td>
                                    <!--{assign var=key1 value="`$prefix`email"}-->
                                    <!--{assign var=key2 value="`$prefix`email02"}-->
                                    <div class="company_form">
                                        <input type="email" id="<!--{$key1}-->" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: disabled;" placeholder="user@domain.com" />
                                        <p class="err"><!--{$arrErr[$key1]}--></p>
                                    </div>
                                    <p>確認のため2度入力してください。</p>
                                    <div class="company_form">
                                        <input type="email" id="<!--{$key2}-->" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->; ime-mode: disabled;" placeholder="user@domain.com" />
                                        <p class="err"><!--{$arrErr[$key1]}--></p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>お問合せ内容<span></span></th>
                                <td class="form_content">
                                    <div>
                                        <textarea name="contents" id="contents" class="box380 form-control" rows="10" style="<!--{$arrErr.contents|h|sfGetErrorColor}-->; ime-mode: active;" placeholder="お問い合せ内容（全角<!--{$smarty.const.MLTEXT_LEN}-->字以下）"><!--{"\n"}--><!--{$arrForm.contents.value|h}--></textarea>
                                        <p class="err"><!--{$arrErr.contents}--></p>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="personal_check">
                    <span>
                        <input type="checkbox" class="personal" id="personal" name="personal">
                        <label for="personal"></label>
                    </span>
                    <span class="personal_text">
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->guide/privacy.php" target="_blank">個人情報の取り扱い</a>に同意する
                    </span>
                </div>
                <div class="member_submit_wrapper">
                    <div class="member_submit">
                        <div class="member_submit_buttons">
                            <button type="reset" class="reset">リセット</button>
                            <button class="confirm">入力内容を確認する</button>
                        </div>
                    </div>

                </div>
        </form>
    </div>
</div>

<script>
$(function() {
	var parm=location.search;
	if(parm!=""){
		if(parm.indexOf('&')!=-1){
			tmpparm=parm.split('&');
			$.each(tmpparm, function(index, value) {
				tmpary=value.split("=");
				if(tmpary[0].indexOf('pdname')!=-1){
					$('textarea#contents').val("お問い合わせ商品："+decodeURI(tmpary[1]));
				}
			})
		} else {
			tmpary=parm.split("=");
			if(tmpary[0].indexOf('pdname')!=-1){
				$('textarea#contents').val("お問い合わせ商品："+decodeURI(tmpary[1]));
			}
		}
	}
});
</script>
