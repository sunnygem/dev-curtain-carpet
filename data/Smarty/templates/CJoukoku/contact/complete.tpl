<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<div id="contents_wrapper" class="contact">

    <div class="member_regist_wrapper" >
        <div class="member_regist_title" >
            <h2><!--{$tpl_title|h}--></h2>
        </div>
					
        <div class="member_regist_remark">
            <p>
                お問い合わせいただきありがとうございます。
            </p>
            <p>
                お問い合わせの内容を確認の上、ご連絡させていただきますので、今しばらくお待ち下さい。<br>
                弊社よりご連絡が無い場合は、誠に恐縮ですが再度お問い合わせ頂きます様、何卒お願い申し上げます。<br>
            </p>
            <p>
                ※自動返信で確認のメールを送付させていただいております。お問い合わせについての返答は土日祝を除いた平日にご連絡させていただきます。予めご了承ください。
            </p>
        </div>

        <div class="member_submit_wrapper">
            <div class="member_submit">
                <div class="member_submit_buttons">
                    <button onClick="location.href='/';" class="reset">TOPに戻る</button>
                    <button onClick="location.href='<!--{$smarty.const.TOP_URL}-->';" class="confirm">買い物を続ける</button>
                </div>
            </div>
        </div>

    </div>
</div>
