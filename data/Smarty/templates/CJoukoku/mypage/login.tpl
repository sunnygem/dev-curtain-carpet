<!--{*
/*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<div id="undercolumn" class="login-page">
    <h2 class="title">ログイン</h2>
    <div id="undercolumn_login">
        <form class="form-signin" role="form" name="login_mypage" id="login_mypage" method="post" action="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php" onsubmit="return eccube.checkLoginFormInputted('login_mypage')">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="login" />
            <input type="hidden" name="url" value="<!--{$smarty.server.REQUEST_URI|h}-->" />
            <div class="row">
                <div class="login_area col-md-6">
					<div class="inner">
                    <h3>会員登録がお済みのお客様</h3>
                   
                    <div class="jumbotron padding-md">
                        <!--{assign var=key1 value="login_email"}-->
						<p>メールアドレス</p>
                        <input type="email"  class="box300 form-control" name="<!--{$key1}-->" value="<!--{$tpl_login_email|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->; ime-mode: disabled;" required="" autofocus="" placeholder="メールアドレス" /><br>
						<!--{assign var=key value="login_memory"}-->
                        <label class="checkbox">
                          <input type="checkbox" name="<!--{$key}-->" value="1"<!--{$tpl_login_memory|sfGetChecked:1}--> id="login_memory" /> ログイン情報を記憶する
                        </label>

                        <!--{assign var=key2 value="login_pass"}-->
						<p>パスワード</p>
                        <input type="password" class="form-control" name="<!--{$key2}-->" maxlength="<!--{$arrForm[$key2].length}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->" required="" placeholder="パスワード" />
                        <span class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></span>
						<button class="btn btn-lg btn-primary btn-block" type="submit">ログインする</button>
                        <small class="div">
                            <a href="<!--{$smarty.const.HTTPS_URL}-->forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->" onclick="eccube.openWindow('<!--{$smarty.const.HTTPS_URL}-->forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->','forget','600','460',{scrollbars:'no',resizable:'no'}); return false;" target="_blank">パスワードを忘れた方はこちら</a><br>
							<a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->contact/<!--{$smarty.const.DIR_INDEX_PATH}-->">メールアドレスを忘れた方はこちら</a>
                        </small>

                        
                        </div>
                    </div>
                   <p class="tyui">会員の方は、登録時に入力されたメールアドレスとパスワードでログインしてください。</p>
                </div>
                <div class="col-md-6 right">
					<div class="inner">
                    <h3>まだ会員登録されていないお客様</h3>
						<div class="img"><img src="<!--{$TPL_URLPATH}-->img/common/login_img2.png" width="100%"></div>
                    
                    <div class="jumbotron padding-md">
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->entry/kiyaku.php" class="btn btn-lg btn-danger btn-block">
                            会員登録はこちら
                        </a>
						
                    </div>
						</div>
					<p class="tyui">会員登録をすると便利なMyページをご利用いただけます。<br>
					また、ログインするだけで、毎回お名前や住所を入力することなくスムーズにお買い物をお楽しみいただけます。</p>
                </div>
            </div>
        </form>
    </div>
</div>
