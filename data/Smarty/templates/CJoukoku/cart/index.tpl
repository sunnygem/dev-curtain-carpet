<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<div id="undercolumn">
    <div id="undercolumn_cart">
       

       <table class="step">
			<tr>
				<th class="s01 orange"><span>STEP1</span><br>
				カートのご確認</th>
				<th class="s02"><span>STEP2</span><br>
				届け先の指定</th>
				<th class="s03"><span>STEP3</span><br>
				お支払方法等の確認</th>
				<th class="s04"><span>STEP4</span><br>
				ご注文の最終確認</th>
			</tr>
			<tr>
				<td>ご注文の内容確認を確認ください。</td>
				<td>届け先等をご指定ください。</td>
				<td>お支払方法・お届け時間等の指定ください。</td>
				<td>ご注文内容の確認をしてください。</td>
			</tr>
		</table>

        <p class="totalmoney_area">
            <!--{* カートの中に商品がある場合にのみ表示 *}-->
            <!--{if count($cartKeys) > 1}-->
                <div class="alert alert-danger">
                    <span class="fa fa-warning"></span>
                    <!--{foreach from=$cartKeys item=key name=cartKey}--><!--{$arrProductType[$key]|h}--><!--{if !$smarty.foreach.cartKey.last}-->、<!--{/if}--><!--{/foreach}-->は同時購入できません。
                    お手数ですが、個別に購入手続きをお願い致します。
                </div>
            <!--{/if}-->

            <!--{if strlen($tpl_error) != 0}-->
                <div class="alert alert-danger"><span class="fa fa-warning"></span><!--{$tpl_error|h}--></div>
            <!--{/if}-->

            <!--{if strlen($tpl_message) != 0}-->
                <div class="alert alert-danger"><span class="fa fa-warning"></span><!--{$tpl_message|h|nl2br}--></div>
            <!--{/if}-->
        </p>

        <!--{if count($cartItems) > 0}-->
            <!--{foreach from=$cartKeys item=key}-->
                <form name="form<!--{$key|h}-->" id="form<!--{$key|h}-->" method="post" action="?">
                    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME|h}-->" value="<!--{$transactionid|h}-->" />
                    <input type="hidden" name="mode" value="confirm" />
                    <input type="hidden" name="cart_no" value="" />
                    <input type="hidden" name="cartKey" value="<!--{$key|h}-->" />
                    <input type="hidden" name="category_id" value="<!--{$tpl_category_id|h}-->" />
                    <input type="hidden" name="product_id" value="<!--{$tpl_product_id|h}-->" />

                    <div class="form_area panel panel-default">
                        <!--{if count($cartKeys) > 1}-->
                            <div class="panel-heading">
                                <h3 class="margin-none padding-none"><!--{$arrProductType[$key]|h}--></h3>
                            </div>
                            <!--{assign var=purchasing_goods_name value=$arrProductType[$key]}-->
                        <!--{else}-->
                            <!--{assign var=purchasing_goods_name value="カートの中の商品"}-->
                        <!--{/if}-->
                        <div class="list-group cart-page">
                            <table>
								<tr class="first">
									<th>商品</th>
									<td>単価（税込）</td>
									<td>個数</td>
									<td>小計（税込）</td>
									<td></td>
								</tr>
                                <!--{foreach from=$cartItems[$key] item=item}-->
							
                                <tr>
                                <th>
                                    <!--{if $item.productsClass.main_image|strlen >= 1}-->
                                    <a class="expansion" target="_blank" href="<!--{$smarty.const.IMAGE_SAVE_URLPATH|h}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->">
                                    <!--{/if}-->
                                    <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_list_image|sfNoImageMainList|h}-->" class="img-responsive" alt="<!--{$item.productsClass.name|h}-->" />
                                    <!--{if $item.productsClass.main_image|strlen >= 1}-->
                                    </a>
                                    <!--{/if}-->
                                    <strong class="cart-item-title">
                                        <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->"><!--{$item.productsClass.name|h}--></a>
                                    </strong>
                                    <!--{if $item.productsClass.classcategory_name1 !== '雑貨1' }-->
                                        <!--{if $item.productsClass.class_id1 !== null }-->
                                            <li><small><!--{$item.productsClass.class_name1|h}-->：<!--{$item.productsClass.classcategory_name1|h}--></small></li>
                                        <!--{/if}-->

                                        <!--{* <!--{if $item.productsClass.classcategory_name2 !== "" && $item.productsClass.classcategory_name2|regex_replace:'/^order_/':'x' eq $item.productsClass.classcategory_name2 }-->
                                            <li><small>SIZE：<!--{$item.productsClass.classcategory_name2|h}--></small></li>
                                        <!--{/if}--> *}-->

                                        <!--{if $item.productsClass.class_id2 !== null }-->
                                            <!--{if count( $item.plg_productoptions_detail ) === 0 && $item.productsClass.plg_productoptions_flg !== null }-->
                                                <li><small>サイズ：<!--{$item.productsClass.classcategory_name2|h}--></small></li>
                                            <!--{elseif count( $item.plg_productoptions_detail ) === 0}-->
                                                <li><small><!--{$item.productsClass.class_name2|h}-->：<!--{$item.productsClass.classcategory_name2|h}--></small></li>
                                            <!--{/if}-->
                                        <!--{/if}-->

                                        <br/>
                                        <!--{if $item.plg_productoptions_detail[1].optioncategory_value}-->
                                            <!--{$item.plg_productoptions_detail[1].option_name}-->:<!--{$item.plg_productoptions_detail[1].optioncategory_value}-->cm<br/>
                                        <!--{/if}-->
                                        <!--{if $item.plg_productoptions_detail[2].optioncategory_value}-->
                                            <!--{$item.plg_productoptions_detail[2].option_name}-->:<!--{$item.plg_productoptions_detail[2].optioncategory_value}-->cm<br/>
                                        <!--{/if}-->
                                        <!--{if $item.plg_productoptions_detail[3].optioncategory_name}-->
                                            <!--{$item.plg_productoptions_detail[3].option_name}-->:<!--{$item.plg_productoptions_detail[3].optioncategory_name}--><br/>
                                        <!--{/if}-->
                                        <!--{if $item.plg_productoptions_detail[4].optioncategory_name}-->
                                            <!--{$item.plg_productoptions_detail[4].option_name}-->:<!--{$item.plg_productoptions_detail[4].optioncategory_name}--><br/>
                                        <!--{/if}-->
                                        <!--{if $item.plg_productoptions_detail[5].optioncategory_name}-->
                                            <!--{$item.plg_productoptions_detail[5].option_name}-->:<!--{$item.plg_productoptions_detail[5].optioncategory_name}--><br/>
                                        <!--{/if}-->	
                                        <!--{if $item.plg_productoptions_detail[6].optioncategory_name}-->
                                            <!--{$item.plg_productoptions_detail[6].option_name}-->:<!--{$item.plg_productoptions_detail[6].optioncategory_name}-->
                                        <!--{/if}-->	
                                    <!--{/if}-->
								</th>
								<td>¥<!--{$item.price_inctax|number_format|h}-->円</td>
								<td>
                                        <div id="quantity_level" class="btn-group btn-group-sm">
                                            <a href="?" class="btn btn-default btn" onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->','up','cart_no','<!--{$item.cart_no|h}-->'); return false">
                                                <span class="fa fa-plus"></span>
                                            </a>
											<div class="block-a"><!--{$item.quantity|h}--></div>
                                            <a href="?" class="btn btn-default btn" onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->','down','cart_no','<!--{$item.cart_no|h}-->'); return false" <!--{if $item.quantity == 1}-->disabled="disabled"<!--{/if}-->>
                                                <span class="fa fa-minus"></span>
                                            </a>
                                        </div></td>
								<td>¥<!--{$item.total_inctax|number_format|h}-->円</td>
								<td><a class="btn-delete" href="?" onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->', 'delete', 'cart_no', '<!--{$item.cart_no|h}-->'); return false;">
                                           <span class="hidden-xs"> × <small>削除</small></span>
                                        </a></td>
							</tr>

                            
                            
                            <!--{/foreach}-->
						</table>
						
                        <div class="gray-area01">
                            <div class="small-text">小計　：　<!--{$arrData[$key].total-$arrData[$key].deliv_fee|number_format|h}-->円</div>
                            <div class="big-text">合計金額　：　<!--{$arrData[$key].total-$arrData[$key].deliv_fee|number_format|h}-->円</div>
                        </div>
						
						<!--{if strlen($tpl_error) == 0}-->
                        <div class="btn-area">
                            <!--{if $tpl_url_back}-->
                                <a href="<!--{$tpl_url_back}-->" class="btn_back">ショッピングを続ける</a>
                            <!--{/if}-->
                            <span class="cartbtn"><button class="" name="confirm">ご購入手続きに進む</button></span>
                        </div>
                        <!--{/if}-->

                        </div><!--{* list-group *}-->
                    </div>
                </form>
            <!--{/foreach}-->

        <!--{else}-->
            <p class="empty alert alert-warning">※ 現在カート内に商品はございません。</p>
        <!--{/if}-->



    </div>
</div>
