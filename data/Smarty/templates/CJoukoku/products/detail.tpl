<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<script type="text/javascript">//<![CDATA[
    // 規格2に選択肢を割り当てる。
    function fnSetClassCategories(form, classcat_id2_selected) {
        var $form = $(form);
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('[name=classcategory_id1]');
        var $sele2 = $form.find('select[name=classcategory_id2]');
        eccube.setClassCategories($form, product_id, $sele1, $sele2, classcat_id2_selected);
    }

    function form_submit(){
        // オーダーものはチェック;
    	if($("#order_flg_disp").val()==='2'){
    		if($('input[name=checkwidth]').prop("checked")){
    			if($('input[name=checkheight]').prop("checked")){
    				if($.isNumeric($('input.orderwidth').val())){
    					if($.isNumeric($('input.orderheight').val())){
    						if($.isNumeric($('input[name="plg_productoptions[3]"]:checked').val())){
    							if($.isNumeric($('input[name="plg_productoptions[4]"]:checked').val())){
    								document.form1.submit();
    							} else {
    								alert("フックを選択してください");
    							}
    						} else {
    							alert("両開き片開きを選択してください");
    						}
    					} else {
    						alert("高さを入力してください");
    					}
    				} else {
    					alert("幅を入力してください");
    				}
    			} else {
    				alert("高さを測りましたにチェックを入れてください");
    			}
    		} else {
    			alert("幅を測りましたにチェックを入れてください");
    		}
    	} else {
    		document.form1.submit();
    	}
	}	
//]]></script>
<script>
$(function(){
    //var imgname="/shop/upload/save_image/<!--{$arrProduct[$ckey]|nl2br_html}-->";
    //var imgkakucho=".jpg";
    //$(".productColor ul li").on("click",function(){
    //    var tmpcolor=$(this).data("colorname");
    //    var tmpccolor=$("input#ccolor").val();
    //    if(tmpccolor=="undefined"){
    //    	tmpccolor="";
    //    } else {
    //    	tmpccolor="_"+tmpccolor;
    //    }
    //    $("#mainImg").attr("src",imgname+"_1"+tmpccolor+tmpcolor+imgkakucho);
    //    $(".mainPleft ul.thumbnail li img").eq(0).attr("src",imgname+"_1"+tmpccolor+tmpcolor+imgkakucho);
    //    $(".mainPleft ul.thumbnail li img").eq(1).attr("src",imgname+"_2"+tmpccolor+tmpcolor+imgkakucho);
    //    $(".mainPleft ul.thumbnail li img").eq(2).attr("src",imgname+"_3"+tmpccolor+tmpcolor+imgkakucho);
    //    $(".mainPleft ul.thumbnail li img").eq(3).attr("src",imgname+"_4"+tmpccolor+tmpcolor+imgkakucho);
    //    $(".mainPleft ul.thumbnail li img").eq(4).attr("src",imgname+"_5"+tmpccolor+tmpcolor+imgkakucho);
    //    $(".mainPleft ul.thumbnail li img").eq(5).attr("src",imgname+"_6"+tmpccolor+tmpcolor+imgkakucho);
    //    $(".mainPleft ul.thumbnail li img").eq(6).attr("src",imgname+"_7"+tmpccolor+tmpcolor+imgkakucho);
    //    $(".mainPleft ul.thumbnail li img").eq(7).attr("src",imgname+"_8"+tmpccolor+tmpcolor+imgkakucho);
    //});

    $("[name=classcategory_id1]").on("change",function(){
    	classcategory1click($(this));
    });
    // 規格2選択時
    $('select[name=classcategory_id2]').on("change",function(){
    	var $form = $('#form1');
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('select[name=classcategory_id1], [name=classcategory_id1]:checked');
        var $sele2 = $(this);
        eccube.checkStock($form, product_id, $sele1.val(), $sele2.val());
    });

    $(window).on("load",function(){
        if($("dt[name='classcategory_id1_name']").text()=="dummy"){
        	$("dt[name='classcategory_id1_name']").parents('dl').css("display","none");
        	$("select[name='classcategory_id1']").val('126');
        	classcategory1click($("select[name='classcategory_id1']"));
        }
    });

    function classcategory1click($this){
        $("ul.colorSelect01 li").removeClass("selected");
        var tmpval = $($this).val();
        $("ul.colorSelect01 li[data-color="+tmpval+"]").addClass("selected");
        var $form = $('#form1');
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $($this);
        var $sele2 = $form.find('select[name=classcategory_id2]');

        // 規格1のみの場合
        if (!$sele2.length) {
            eccube.checkStock($form, product_id, $sele1.val(), '0');
            // 規格2ありの場合
        } else {
            eccube.setClassCategories($form, product_id, $sele1, $sele2);
        }
    }

    // 規格1選択時
    $('[name=classcategory_id1]').on('change', function() {
        var $form = $(this).parents('form');
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $(this);
        var $sele2 = $form.find('select[name=classcategory_id2]');

        // 規格1のみの場合
        if (!$sele2.length) {
            eccube.checkStock($form, product_id, $sele1.val(), '0');
        // 規格2ありの場合
        } else {
            eccube.setClassCategories($form, product_id, $sele1, $sele2);
        }
    });

});

</script>

<main>

<section class="mainProduct">
    <h2><!--{$arrProduct.name|h}--></h2>
    <div class="product_overview">
        <div class="product_feature">
            <!--{section name=cnt loop=$smarty.const.PRODUCTSUB_MAX}-->
                <!--{assign var=ikey value="sub_image`$smarty.section.cnt.index+1`"}-->
                <!--{if $arrProduct[$ikey]|strlen >= 1}-->
                    <!--{assign var=key value="sub_title`$smarty.section.cnt.index+1`"}-->
                    <!--{assign var=ckey value="sub_comment`$smarty.section.cnt.index+1`"}-->
                    <!--{if $arrProduct[$ikey]|strlen >= 1}-->
                        <div class="product_feature_img">
                        <img src="<!--{$arrFile[$ikey].filepath}-->" data-original="<!--{$arrFile[$ikey].filepath}-->" alt="<!--{$arrProduct[$key]|h}-->" class="thumbnail lazy"/>
                        <!--{if $arrProduct[$ckey]|strlen >= 1}-->
                        <p class="sub_comment"><!--{$arrProduct[$ckey]|nl2br_html}--></p>
                        <!--{/if}-->
                        </div>
                    <!--{/if}-->
                <!--{/if}-->
            <!--{/section}-->
        </div><!---./product_feature}-->

        <!--{if count( $arrFunctionFeatures ) > 0 && $arrFunctionFeatures !== false }-->
        <div class="product_status">
            <ul>
                <!--{foreach from=$arrFunctionFeatures key=key item=val}-->
                <li>
                    <div class="status_img">
                        <img src="/shop/user_data/packages/CJoukoku/<!--{$val.image_path}-->" alt="<!--{$val.title|h}-->" class="lazy">
                    </div>
                    <div class="status_txt">
                        <dl>
                            <dt> <!--{$val.title|h}--> </dt>
                            <dd class="status_detail">
                                <!--{$val.contents|nl2br}-->
                            </dd>
                        </dl>
                    </div>
                </li>
                <!--{/foreach}-->
            </ul>
        </div><!--/.product_status-->
        <!--{/if}-->

        <!--{if $outlet_flg === false }-->
            <!--{if (int)$arrProduct.sample_flg === 1}-->
            <div class="product_free_sample">
                <a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->sample/?sample=<!--{$arrProduct.name|h}-->(品番:<!--{$arrProduct.product_code_min|h}-->)">
                    <img src="/shop/user_data/packages/CJoukoku/img/products/product_free_sample.png">
                </a>
            </div><!--/.product_free_sample-->
            <!--{/if}-->
            <div class="product_shop_search">
                <a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top">
                    <img src="/shop/user_data/packages/CJoukoku/img/products/product_shop_search.png">
                </a>
            </div>
        <!--{/if}-->

        <div class="product_main_comment"><!--{$arrProduct.main_comment|nl2br_html}--></div>

        <!--[*<img src="/shop/user_data/packages/CJoukoku/img/products/product_gallery_slider_sample.jpg">*]-->
        <div class="product_gallery">
            <div class="product_ga_main">
                <!--{assign var=key value="main_image"}-->
                <!--★画像★-->
                <!--{if $arrProduct.main_large_image|strlen >= 1}-->
                <a
                href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_large_image|h}-->"
                class="expansion"
                target="_blank"
                >
                    <!--{/if}-->
                    <img src="<!--{$arrFile[$key].filepath|h}-->"  data-original="<!--{$arrFile[$key].filepath|h}-->" alt="<!--{$arrProduct.name|h}-->" class="picture lazy" id="mainImg" />
                    <!--{if $arrProduct.main_large_image|strlen >= 1}-->
                </a>
                <!--{/if}-->
            </div>
            <div class="product_ga_thumb_wrap">
                <div class="product_ga_thumb">
                    <!--{section name=cnt loop=$smarty.const.PRODUCTSUB_MAX}-->
                        <!--{assign var=ikey value="sub_image`$smarty.section.cnt.index+1`"}-->
                        <!--{if $arrProduct[$ikey]|strlen >= 1}-->
                            <!--{assign var=key value="sub_title`$smarty.section.cnt.index+1`"}-->
                            <!--{assign var=lkey value="sub_large_image`$smarty.section.cnt.index+1`"}-->
                            <!--{if $arrProduct[$ikey]|strlen >= 1}-->
                                <div class="sp_thumbnail"><img src="<!--{$arrFile[$ikey].filepath}-->" data-original="<!--{$arrFile[$ikey].filepath}-->" alt="<!--{$arrProduct[$ckey]|h}-->" class="thumbnail lazy"/></div>
                            <!--{/if}-->
                        <!--{/if}-->
                    <!--{/section}-->
                </div>
                <!--
                <div class="product_ga_thumb_arrow">
                    <div class="slick-next"></div>
                    <div class="slick-prev"></div>
                </div>
                -->
            </div>
        </div><!--/.product_gallery-->

        <div class="product_tag">
            <!--▼商品ステータス-->
            <!--{assign var=ps value=$productStatus[$tpl_product_id]}-->
            <!--{if count($ps) > 0}-->
                <ul>
                    <!--{foreach from=$ps item=status}-->
                    <li><span class="icon<!--{$status}-->"><!--{$arrSTATUS[$status]}--></span></li>
                    <!--{/foreach}-->
                </ul>
            <!--{/if}-->
            <!--▲商品ステータス-->
        </div><!--/.product_tag-->
    </div><!--/.product_overview -->

    <div class="product_detail">
        <div id="undercolumn">
            <form name="form1" id="form1" method="post" action="?">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="order_flg" id="order_flg" value="<!--{$arrProduct.order_flg}-->" />
                <h3><!--{$arrProduct.name|h}--></h3>
                <div class="product_color">
                    <dl>
                        <!--{*
                        <!--{if $arrProduct.color_monotone === '1' }-->
                        <dt>カラー：</dt> <dd>モノトーン系</dd>
                        <!--{elseif $arrProduct.color_green === '1' }-->
                        <dt>カラー：</dt> <dd>グリーン系</dd>
                        <!--{elseif $arrProduct.color_beige === '1' }-->
                        <dt>カラー：</dt> <dd>ベージュ系</dd>
                        <!--{elseif $arrProduct.color_blue === '1' }-->
                        <dt>カラー：</dt> <dd>ブルー系</dd>
                        <!--{elseif $arrProduct.color_brown === '1' }-->
                        <dt>カラー：</dt> <dd>ブラウン系</dd>
                        <!--{elseif $arrProduct.color_red === '1' }-->
                        <dt>カラー：</dt> <dd>レッド系</dd>
                        <!--{elseif $arrProduct.color_yellow === '1' }-->
                        <dt>カラー：</dt> <dd>イエロー系</dd>
                        <!--{elseif $arrProduct.color_gray === '1' }-->
                        <dt>カラー：</dt> <dd>グレー系</dd>
                        <!--{elseif $arrProduct.color_ivory === '1' }-->
                        <dt>カラー：</dt> <dd>アイボリー系</dd>
                        <!--{/if}-->
                        *}-->
                        <dt>型番：</dt>
                        <dd><!--{$arrProduct.product_code_max|h}--></dd>
                    </dl>
                </div>
                <!--{*<dl class="productNo">
                  <dt>商品番号</dt>
                  <dd><span id="product_code_default">
                      <!--{if $arrProduct.product_code_min == $arrProduct.product_code_max}-->
                          <!--{$arrProduct.product_code_min|h}-->
                      <!--{else}-->
                          <!--{$arrProduct.product_code_min|h}-->～<!--{$arrProduct.product_code_max|h}-->
                      <!--{/if}-->
                      </span><span id="product_code_dynamic"></span>
                  </dd>
                </dl>*}-->
                <div class="spec product_price">
                    <!--★販売価格★-->
                    <dl class="sale_price price01">
                        <dt><!--{$smarty.const.SALE_PRICE_TITLE}--></dt>
                        <dd class="price <!--{if $arrProduct.sale_flg === "1"}-->sale<!--{/if}-->">
                            <!--{if $arrProduct.sale_flg === "1"}-->
                                <span class="normal_price">
                                    <span class="price_sale_default"><!--{$arrProduct.price01_min|n2s}--></span><span class="price_sale_dynamic"></span> 円～
                                </span>
                            <!--{/if}-->

                            <span id="price02_default"><!--{strip}-->
                            <!--{if $arrProduct.price02_min === $arrProduct.price02_max}-->
                                <!--{$arrProduct.price02_min|n2s}--> <small>円</small>
                            <!--{else}-->
                                <!--{$arrProduct.price02_min|n2s}--> <small>円</small> ～
                            <!--{/if}-->
                            <!--{/strip}--></span><span id="price02_dynamic"></span>
                            <small>(税抜)</small>
                            <p class="inctax">
                                <small>(消費税込み：<span id="price02_inctax_default"><!--{strip}-->
                                <!--{if $arrProduct.price02_min_inctax === $arrProduct.price02_max_inctax}-->
                                    <!--{$arrProduct.price02_min_inctax|n2s}--><small>円</small>
                                <!--{else}-->
                                    <!--{$arrProduct.price02_min_inctax|n2s}--><small>円</small> ～
                                <!--{/if}-->
                                <!--{/strip}--></span><span id="price02_inctax_dynamic"></span>)</small>
                            </p>
                        </dd>
                    </dl>
                    <!--{if $arrProduct.stock_unlimited_min == 1}--><!--{*無制限*}-->
                    <!--{else}-->
                    <dl class="price01 stock">
                        <dt>
                            在庫数
                        </dt>
                        <dd>
                            <!--{if $arrProduct.stock_min != $arrProduct.stock_max}-->
                                <!--{$arrProduct.stock_min|escape}-->点～<!--{$arrProduct.stock_max|escape}-->点
                            <!--{else}-->
                                <!--{$arrProduct.stock_min|escape}-->点
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <!--{/if}-->

                </div>

                <!--{*<dl class="revew01">
                <dt><span>★★★★</span><span>(4.0)</span></dt>
                <dd><a href=""><span>商品レビューを見る(99件)</span></a></dd>
                </dl>*}-->


                <div class="cart_area">
                    <input type="hidden" name="mode" value="cart" />
                    <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->" />
                    <input type="hidden" name="product_class_id" value="<!--{$tpl_product_class_id}-->" id="product_class_id" />
                    <input type="hidden" name="favorite_product_id" value="" />
                    <input type="hidden" name="sample_product_id" value="" />
                    <input type="hidden" name="classcategory_id" value="<!--{$tpl_classcategory_id}-->" id="classcategory_id"/>
                    <input type="hidden" name="classcategory_id1" value="<!--{$tpl_classcategory_id1}-->" id="classcategory_id1" />
                    <input type="hidden" name="classcategory_id2" value="<!--{$tpl_classcategory_id2}-->" id="classcategory_id2" />
  
                    <!--{if $tpl_stock_find}-->
                         <!--{***** プラグインの中身を抜き出してしています *****}-->
                         <!--{if $easy_order_flg || $full_order_flg }-->

                         <div class="check_wrap">
                         <div class="check"><a href="#pc"> <p>巾のつなぎ目や仕上がり丈など注意事項をご確認ください。</p></a> </div>
                         </div>

                         <!--{foreach from=$arrProductOptions|@array_reverse:true item=option key=option_id}-->
                             <!--{assign var=option_tag_id value="plg_productoptions_`$option_id`"}-->
                             <!--{if $option_id === 3}--><!--{*開き方にチェックがある場合*}-->
                             <div class="spec open_type">
                                 <dl class="">
                                     <dt>開き方</dt>
                                     <dd id="hiraki">
                                         <ul class="radio_box">
                                             <li>
                                                 <input type="radio" id="hiraki01" name="plg_productoptions[<!--{$option_id}-->]" id="<!--{$option_tag_id}-->" value="1" onChange="setOptionPriceOnChangeRadio(this,<!--{$option_id}-->,<!--{$option.action}-->);" checked="checked">
                                                 <label for="hiraki01">
                                                     <div class="txt"> 両開き </div>
                                                     <div class="img"> <img src="<!--{$TPL_URLPATH}-->img/products/product_status_hiraki_double.png" alt="両開き"> </div>
                                                 </label>
                                             </li>
                                             <li>
                                                 <input type="radio" id="hiraki02" name="plg_productoptions[<!--{$option_id}-->]" id="<!--{$option_tag_id}-->" value="2" onChange="setOptionPriceOnChangeRadio(this,<!--{$option_id}-->,<!--{$option.action}-->);">
                                                 <label for="hiraki02">
                                                     <div class="txt"> 片開き </div>
                                                     <div class="img"> <img src="<!--{$TPL_URLPATH}-->img/products/product_status_hiraki_single.png" alt="片開き"> </div>
                                                 </label>
                                             </li>
                                         </ul>
                                     </dd>
                                 </dl>
                             </div>
                             <!--{/if}-->
                         <!--{/foreach}-->
                         <!--{/if}-->

                        <!--{if $established_flg === false && $easy_order_flg === false && $full_order_flg === false }-->
                             <input type="hidden" name="order_flg_disp" id="order_flg_disp" value="0"/>
                             <!--{if $tpl_classcat_find1}-->
                                 <div class="classlist">
                                    <div class="spec" <!--{if $tpl_class_name1 === 'dummy'}-->style="display:none;"<!--{/if}-->>
                                    <!--▼規格1-->
                                    <dl class="">
                                        <dt><!--{$tpl_class_name1|h}-->：</dt>
                                        <dd>
                                            <div class="select_wrap">
                                                <select name="classcategory_id1" style="<!--{$arrErr.classcategory_id1|sfGetErrorColor}-->">
                                                <!--{html_options options=$arrClassCat1 selected="$arrForm.classcategory_id1.value"}-->
                                                </select>
                                                <!--{if $arrErr.classcategory_id1 != ""}-->
                                                <br /><span class="attention">※ <!--{$tpl_class_name1}-->を入力して下さい。</span>
                                                <!--{/if}-->
                                            </div>
                                        </dd>
                                    </dl>
                                    <!--▲規格1-->
                                    </div>
                                    <!--{if $tpl_class_name1 === 'dummy'}-->
                                        <script>
                                            $(function(){
                                                 $(window).on('load', function(){
                                                     var $target = $('[name=classcategory_id1]');
                                                     $target.val('126');
                                                     $form = $('[name=form1]')[0];
                                                     fnSetClassCategories( $form, '' );
                                                 });
                                            });
                                        </script>
                                    <!--{/if}-->

                                    <!--{if $tpl_classcat_find2}-->
                                    <!--▼規格2-->
                                    <div class="spec size">
                                    <dl class="">
                                        <dt>
                                            <!--{if $tpl_class_name1 === 'dummy'}-->
                                                サイズ
                                            <!--{else}-->
                                                <!--{$tpl_class_name2|h}-->：
                                            <!--{/if}-->
                                        </dt>
                                        <dd>
                                            <div class="select_wrap">
                                                <select name="classcategory_id2" style="<!--{$arrErr.classcategory_id2|sfGetErrorColor}-->">
                                                </select>
                                                <!--{if $arrErr.classcategory_id2 != ""}-->
                                                <br /><span class="attention">※ <!--{$tpl_class_name2}-->を入力して下さい。</span>
                                                <!--{/if}-->
                                            </div>
                                        </dd>
                                    </dl>
                                    </div>
                                    <!--▲規格2-->
                                    <!--{/if}-->
                                </div>
                            <!--{/if}-->
                        <!--{else}-->
                            <!--{*プラグイン内に記述**}-->
                        <!--{/if}-->

                        <!--★数量★-->
                        <div class="spec">
                            <dl class="quantity">
                                <dt>数量：</dt>
                                <dd>
                                    <div class="quantity_wrap">
                                        <ul>
                                            <li>
                                                <input type="text" class="kazu box60" name="quantity" value="<!--{$arrForm.quantity.value|default:1|h}-->" maxlength="<!--{$smarty.const.INT_LEN}-->" style="<!--{$arrErr.quantity|sfGetErrorColor}-->" />
                                            </li>
                                            <li><input type="button" name="spinner_up" class="spinner_up" value="＋" onclick="javascript:spinner('up');"></li>
                                            <li><input type="button" name="spinner_up" class="spinner_down" value="－" onclick="javascript:spinner('down');"></li>
                                        </ul>
                                        <!--{if $arrErr.quantity != ""}-->
                                            <br /><span class="attention"><!--{$arrErr.quantity}--></span>
                                        <!--{/if}-->
                                    </div><!--/.quantity_wrap-->
                                </dd>
                            </dl>
                            <script>
                                function spinner(mode){
                                    $this=$("dl.quantity input[name=quantity].kazu");
                                    if(mode==="up"){
                                        $this.val(parseInt($this.val())+1);
                                    } else if(mode==="down"){
                                        if($this.val()>0){
                                            $this.val(parseInt($this.val())-1);
                                        }
                                    }
                                }
                            </script>
                        </div>

                        <div class="cartin">
                            <div class="cartin_btn">
                                <div id="cartbtn_default">
                                    <!--★カゴに入れる★-->
                                    <!--{*
                                    <a href="javascript:void(document.form1.submit())">
                                        <img class="hover_change_image" src="<!--{$TPL_URLPATH}-->img/button/btn_cartin.jpg" alt="カゴに入れる" />
                                    </a>
                                    *}-->
                                    <button onclick="form_submit();return false;">カートに入れる</button>
                                </div>
                            </div>
                        </div>
                        <div class="attention" id="cartbtn_dynamic"></div>
                    <!--{else}-->
                        <div class="attention">申し訳ございませんが、只今品切れ中です。</div>
                    <!--{/if}-->

                    <!--{if $outlet_flg === false }-->
                    <div class="shop_search">
                        <a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top">
                            <div class="inner"> お店で商品を見る </div>
                        </a>
                        <p>※商品の店頭在庫については、お手数ですが各店舗にお問い合わせください。</p>
                    </div>
                    <!--{/if}-->

                </div><!--/.cart_area -->

                <div id="widthlimit" style="display:none;"><!--{assign var=ckey value="sub_title2"}-->
                <!--{$arrProduct[$ckey]|nl2br_html}--></div>

                <div class="product_menu">
                    <ul>
                        <!--<li><a href="#" target="_top"><span><img src="<!--{$TPL_URLPATH}-->img/icon/price_icon.png" alt="価格表"></span>価格表を見る</a></li>-->
                        <li><a href="javascript:eccube.changeAction('?product_id=<!--{$arrProduct.product_id|h}-->'); eccube.setModeAndSubmit('add_favorite','favorite_product_id','<!--{$arrProduct.product_id|h}-->');">お気に入りに登録</a></li>
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/favorite.php">お気に入りを見る</a></li>
                        <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->contact/?pdname=<!--{$arrProduct.name|h}-->" target="_top">この商品について問合せる</a></li>
                        <!--{*<li><a href="#">レビューを書く</a></li> *}-->
                        <!--{if $outlet_flg === false }-->
                            <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top">お近くの店舗を探す</a></li>
                        <!--{/if}-->
                        <!--{if (int)$arrProduct.sample_flg === 1 && $outlet_flg === false }-->
                            <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->sample/?sample=<!--{$arrProduct.name|h}-->(品番:<!--{$arrProduct.product_code_min|h}-->)" target="_blank">この商品の生地サンプルを申し込む</a></li>
                            <li><a href="javascript:eccube.changeAction('?product_id=<!--{$arrProduct.product_id|h}-->'); eccube.setModeAndSubmit('add_sample','sample_product_id','<!--{$arrProduct.product_id|h}-->');">生地サンプルに登録</a></li>
                        <!--{/if}-->
                     </ul>
                    <!--{if (int)$arrProduct.sample_flg === 1 && $outlet_flg === false }-->
                        <div class="sample_attention">
                            <p>
                                生地サンプル（無料）のお申込は10枚までとなっております。<br>
                                <a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/sample.php">生地サンプルリスト</a>で管理できます  　<a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/sample.php">生地サンプルリスト画面に移動する</a>
                            </p>
                        </div>
                    <!--{/if}-->
                </div><!--/. product_menu -->

                <!--{*カーテンの場合のみ表示*}-->
                <!--{if $curtain_flg === true }-->
                    <div class="product_caution" id="pc">
                        <dl>
                            <dt class="open">巾のつなぎ目について</dt>
                            <dd>
                                <div>
                                    カーテンのつなぎ目は既製カーテン、オーダーカーテンにおいてもサイズによって生じます。 <br>
                                    1.5倍ヒダでは横幅100cm以上、2倍ヒダでは横幅70cm以上（一部75cm以上）の場合 にはつなぎ目が入ります。つなぎ目のないカーテンをご希望の場合は、『お問い合わせ』よりご連絡ください。メールにてご案内いたします。<br>
                                    また、1.5倍ヒダの既製カーテンで、横幅100cm以上の商品は柄合わせを行っておりません。その為、デザインがきっちり連続しませんのでご了承下さい。 気にされる場合は、オーダーカーテンからお選び下さい。
                                </div>
                            </dd>
                            <dt class="open">仕上がり丈について</dt>
                            <dd>
                                <div>
                                    カーテンの丈は生地ものであるため、プラスマイナス1cmの誤差が発生する場合がございます。その場合は、アジャスターフックにて調整して頂くようお願いいたします。
                                </div>
                                <div id="curtain_fook"></div>
                            </dd>
                            <dt class="open">フックとレールの形式について</dt>
                            <dd>
                                <div>
                                    レールの形式によって、適したフックが異なります。レールの形式をご確認の上、 フックをお選びください。
                                    <ul class="rail_type">
                                        <li><img src="<!--{$TPL_URLPATH}-->img/products/product_rail_type01.png" class="lazy"></li>
                                        <li><img src="<!--{$TPL_URLPATH}-->img/products/product_rail_type02.png" class="lazy"></li>
                                        <li><img src="<!--{$TPL_URLPATH}-->img/products/product_rail_type03.png" class="lazy"></li>
                                        <li><img src="<!--{$TPL_URLPATH}-->img/products/product_rail_type04.png" class="lazy"></li>
                                        <li><img src="<!--{$TPL_URLPATH}-->img/products/product_rail_type05.png" class="lazy"></li>
                                        <li><img src="<!--{$TPL_URLPATH}-->img/products/product_rail_type06.png" class="lazy"></li>
                                    </ul>
                                </div>
                            </dd>
                        </dl>
                    </div>
                <!--{/if}-->

<!--{*
                <div class="product_voice">
                    <p class="ttl">この商品に寄せられたお客様の声</p>
                    <div class="voice_list">
                        <dl>
                            <dt>防虫効果に驚きました</dt>
                            <dd>
                                <ul>
                                    <li class="time">2019年3月20日</li>
                                    <li class="name">カーテン王国さん</li>
                                    <li class="value"><span>★★★★</span>★</li>
                                </ul>
                                <p class="comment">
                                    部屋の雰囲気も変わってとてもいい感じになりました。簡単包装で届いたのでゴミ捨ても楽でした。
                                </p>
                            </dd>
                            <dt>防虫効果に驚きました</dt>
                            <dd>
                                <ul>
                                    <li class="time">2019年3月20日</li>
                                    <li class="name">カーテン王国さん</li>
                                    <li class="value"><span>★★★★</span>★</li>
                                </ul>
                                <p class="comment">
                                    部屋の雰囲気も変わってとてもいい感じになりました。簡単包装で届いたのでゴミ捨ても楽でした。
                                </p>
                            </dd>
                            <dt>防虫効果に驚きました</dt>
                            <dd>
                                <ul>
                                    <li class="time">2019年3月20日</li>
                                    <li class="name">カーテン王国さん</li>
                                    <li class="value"><span>★★★★</span>★</li>
                                </ul>
                                <p class="comment">
                                    部屋の雰囲気も変わってとてもいい感じになりました。簡単包装で届いたのでゴミ捨ても楽でした。
                                </p>
                            </dd>
                            <dt>防虫効果に驚きました</dt>
                            <dd>
                                <ul>
                                    <li class="time">2019年3月20日</li>
                                    <li class="name">カーテン王国さん</li>
                                    <li class="value"><span>★★★★</span>★</li>
                                </ul>
                                <p class="comment">
                                    部屋の雰囲気も変わってとてもいい感じになりました。簡単包装で届いたのでゴミ捨ても楽でした。
                                </p>
                            </dd>
                        </dl>
                    </div>
                </div><!--/.product_voice-->
*}-->

                <!--▼関連商品-->
                <!--{if $arrRecommend}-->

                <div class="product_related">
                    <div class="ttl">
                        <div><img src="<!--{$TPL_URLPATH}-->img/products/product_ttl_related.png" alt="関連商品"></div>
                        <h3>関連商品</h3>
                    </div>
                    <div class="">
                        <ul class="">
                            <!--{foreach from=$arrRecommend item=arrItem name="arrRecommend"}-->
                            <!--{assign var=id value=$arrItem.recommend_product_id}-->
                            <li>
                                <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrItem.product_id|u}-->">
                                    <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrItem.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrItem.name|h}-->" class="lazy"></a>
                                <h4><!--{$arrItem.name|h}--></h4>
                                <div class="pricebox sale_price">
                                    <p class="price <!--{if $arrItem.sale_flg === "1"}-->sale<!--{/if}-->">
                                    <!--{if $arrItem.sale_flg === "1"}--> <span class="normal_price"> <!--{$arrItem.price01_min|n2s}--> 円～ </span> <!--{/if}-->
                                    <span id="price02_default_<!--{$id}-->"><!--{strip}-->
                                    <!--{if $arrItem.price02_min == $arrItem.price02_max}-->
                                        <!--{$arrItem.price02_min|n2s}-->円
                                    <!--{else}-->
                                        <!--{$arrItem.price02_min|n2s}-->円～
                                    <!--{/if}-->
                                    </span>
                                    <!--{/strip}--><span style="display: inline-block;">(税抜)</span>
                                    </p>
                                </div><!--/.pricebox-->

                            </li>
                            <!--{/foreach}-->
                        </ul>
                    </div>
                </div>
                <!--{/if}-->
                <!--▲関連商品-->

                <!--{* 最近チェックした商品ブロックを編集 *}-->

            </form>
        </div><!--/#undercolumn -->
    </div><!--/.product_detail-->
</section><!--/.mainProduct-->

    <div class="mainPunder">
    <!--{assign var=ckey value="note"}-->
    <!--{$arrProduct[$ckey]|nl2br_html}-->
    </div>

<!--{*
        <!--この商品に対するお客様の声-->
        <!--{if count($arrReview) > 0}-->
        <div id="customervoice_area">
            <h2>この商品に対するお客様の声</h2>
            <ul>
            <!--{section name=cnt loop=$arrReview}-->
                <li>
                    <p class="voicetitle"><!--{$arrReview[cnt].title|h}--></p>
                    <p class="voicedate"><!--{$arrReview[cnt].create_date|sfDispDBDate:false}-->　投稿者：<!--{if $arrReview[cnt].reviewer_url}--><a href="<!--{$arrReview[cnt].reviewer_url}-->" target="_blank"><!--{$arrReview[cnt].reviewer_name|h}--></a><!--{else}--><!--{$arrReview[cnt].reviewer_name|h}--><!--{/if}-->　<span class="osusumeLv">おすすめレベル：<span class="recommend_level"><!--{assign var=level value=$arrReview[cnt].recommend_level}--><!--{$arrRECOMMEND[$level]|h}--></span></span></p>
                    <p class="voicecomment"><!--{$arrReview[cnt].comment|h|nl2br}--></p>
                </li>
            <!--{/section}-->
            </ul>
       </div>
       <!--{/if}-->
       <!--お客様の声ここまで-->
*}-->

</main>
