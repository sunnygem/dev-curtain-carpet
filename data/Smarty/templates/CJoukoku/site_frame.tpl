<!DOCTYPE html>
<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<html lang="ja">
<head>
<!--{if $env_pro_flg === true}-->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5GM9G6M');</script>
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async
    src="https://www.googletagmanager.com/gtag/js?id=UA-39695785-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-39695785-1');
    </script>
<!--{/if}-->
<meta charset="<!--{$smarty.const.CHAR_CODE}-->" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<meta http-equiv="Content-Type" content="text/html; charset=<!--{$smarty.const.CHAR_CODE}-->" />
<title><!--{$arrSiteInfo.shop_name|h}--><!--{if $tpl_subtitle|strlen >= 1}--> / <!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--> / <!--{$tpl_title|h}--><!--{/if}--></title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<!--{if $arrPageLayout.author|strlen >= 1}-->
<meta name="author" content="<!--{$arrPageLayout.author|h}-->" />
<!--{/if}-->
<meta name="description" content="<!--{if $arrPageLayout.description}--><!--{$arrPageLayout.description|h}--><!--{else}--><!--{$arrSiteInfo.shop_name|h}--><!--{/if}--><!--{if $tpl_subtitle|strlen >= 1}-->,<!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}-->,<!--{$tpl_title|h}--><!--{/if}-->" />

<!--{if $arrPageLayout.keyword|strlen >= 1}-->
<meta name="keywords" content="<!--{if $arrProduct.comment3}--><!--{$arrProduct.comment3|escape}--><!--{else}--><!--{$arrSiteInfo.shop_name|h}--><!--{if $tpl_subtitle|strlen >= 1}-->,<!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}-->,<!--{$tpl_title|h}--><!--{/if}--><!--{/if}-->,<!--{$arrPageLayout.keyword|h}-->" />
<!--{else}-->
 <meta name="keywords" content="<!--{if $arrProduct.comment3}--><!--{$arrProduct.comment3|escape}--><!--{else}--><!--{$arrSiteInfo.shop_name|h}--><!--{if $tpl_subtitle|strlen >= 1}-->,<!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}-->,<!--{$tpl_title|h}--><!--{/if}--><!--{/if}-->" />
<!--{/if}-->
	
<!--{if $arrPageLayout.meta_robots|strlen >= 1}-->
    <meta name="robots" content="<!--{$arrPageLayout.meta_robots|h}-->" />
<!--{/if}-->
<link rel="shortcut icon" href="<!--{$TPL_URLPATH}-->img/common/favicon.ico" />
<link rel="apple-touch-icon" href="<!--{$TPL_URLPATH}-->img/common/apple-touch-icon.png?20190712" />
<link rel="icon" type="image/vnd.microsoft.icon" href="<!--{$TPL_URLPATH}-->img/common/favicon.ico" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="<!--{$smarty.const.HTTP_URL}-->rss/<!--{$smarty.const.DIR_INDEX_PATH}-->" />
<link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.colorbox/colorbox.css" type="text/css" media="all" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/import.css" type="text/css" media="all" />

<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.js"></script>
<!-- #2342 次期メジャーバージョン(2.14)にてeccube.legacy.jsは削除予定.モジュール、プラグインの互換性を考慮して2.13では残します. -->
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.legacy.js"></script>
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.colorbox/jquery.colorbox-min.js"></script>
<!--{if $tpl_page_class_name === "LC_Page_Abouts"}-->
    <!--{if ($smarty.server.HTTPS != "") && ($smarty.server.HTTPS != "off")}-->
        <script type="text/javascript" src="https://maps-api-ssl.google.com/maps/api/js?sensor=false"></script>
    <!--{else}-->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!--{/if}-->
<!--{/if}-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/slick/slick.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.lazyload.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/trunk8.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/script.js?<!--{$smarty.now}-->"></script>
<script type="text/javascript">//<![CDATA[
<!--{$tpl_javascript}-->
$(function(){
    <!--{$tpl_onload}-->
    // off canvas button
    $(document).on('click', '.toggle-offcanvas', function(){
        $('.row-offcanvas').toggleClass('active');
        return false;
    });

    // swipe event
    $("#main_column, #rightcolumn").on("touchstart", TouchStart);
    $("#main_column, #rightcolumn").on("touchmove" , TouchMove);

    function Position(e){
        var x = e.originalEvent.touches[0].pageX;
        var y = e.originalEvent.touches[0].pageY;
        x = Math.floor(x);
        y = Math.floor(y);
        var pos = {'x':x , 'y':y};
        return pos;
    }
    function TouchStart( event ) {
        var pos = Position(event);
        $("#main_column").data("memory",pos.x);
    }
    function TouchMove( event ) {
        var pos = Position(event); //X,Yを得る
        var start = $("#main_column").data("memory");
        var range = start - pos.x;
        if( range > 50){
            // 左に移動
            $('.row-offcanvas').removeClass('active');
        } else if (start < 30 && range < -10){
            // 右に移動
            $('.row-offcanvas').addClass('active');
        }
    }

    // pagetop
    var pageTop = function(){
        $((navigator.userAgent.indexOf("Opera") != -1) ? document.compatMode == 'BackCompat' ? 'body' : 'html' :'html,body').animate({scrollTop:0}, 'slow');
        return false;
    };
    var pageBottom = function(){
        $((navigator.userAgent.indexOf("Opera") != -1) ? document.compatMode == 'BackCompat' ? 'body' : 'html' :'html,body').animate({scrollTop: $(document).height()-$(window).height()}, 'slow');
        return false;
    };
    $("a[href^=#top]").click(pageTop);

});
//]]></script>

<!--{strip}-->
    <!--{* ▼Head COLUMN*}-->
    <!--{if !empty($arrPageLayout.HeadNavi)}-->
        <!--{* ▼上ナビ *}-->
        <!--{foreach key=HeadNaviKey item=HeadNaviItem from=$arrPageLayout.HeadNavi}-->
            <!--{* ▼<!--{$HeadNaviItem.bloc_name}--> *}-->
            <!--{if $HeadNaviItem.php_path != ""}-->
                <!--{include_php file=$HeadNaviItem.php_path}-->
            <!--{else}-->
                <!--{include file=$HeadNaviItem.tpl_path}-->
            <!--{/if}-->
            <!--{* ▲<!--{$HeadNaviItem.bloc_name}--> *}-->
        <!--{/foreach}-->
        <!--{* ▲上ナビ *}-->
    <!--{/if}-->
    <!--{* ▲Head COLUMN*}-->
<!--{/strip}-->
</head>

<!-- ▼BODY部 スタート -->
<!--{include file='./site_main.tpl'}-->
<!-- ▲BODY部 エンド -->

</html>
