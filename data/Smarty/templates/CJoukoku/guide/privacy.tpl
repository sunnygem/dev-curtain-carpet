<!--{*
/*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

       
       <div class="page-body">
		<div class="inner privacy">
			<h2><!--{$tpl_title|h}--></h2>
		<p>株式会社カーテンじゅうたん王国(以下「当社」といいます。)は、個人情報の保護を社会的責務であると考え、お客様に安心してご利用いただけるウェブサイトの管理・運営を行うため、「カーテンじゅうたん王国個人情報保護方針」に基づき、以下のとおり「ウェブサイトにおける個人情報の取扱いについて」を定めました。</p>
				
					<dl>
						<dt>1.個人情報の定義</dt>
						<dd>「個人情報」とは、生存する個人に関する情報であって、当該情報に含まれる氏名、生年月日その他の記述等により特定の個人を識別することができるもの、及び他の情報と容易に照合することができ、それにより特定の個人を識別することができることとなるものをいいます。</dd>
					</dl>
					<dl>
						<dt>2.個人情報の収集</dt>
						<dd>当ショップでは商品のご購入、お問合せをされた際にお客様の個人情報を収集することがございます。</dd>
						<dd>収集するにあたっては利用目的を明記の上、適法かつ公正な手段によります。</dd>
						<dd>当ショップで収集する個人情報は以下の通りです。</dd>
						<dd class="space">a)お名前、フリガナ<br>
						b)ご住所<br>
						c)お電話番号<br>
						d)メールアドレス<br>
						e)パスワード<br>
						f)配送先情報<br>
						g)当ショップとのお取引履歴及びその内容<br>
						h)上記を組み合わせることで特定の個人が識別できる情報</dd>
					</dl>
					<dl>
						<dt>3.個人情報の利用</dt>
						<dd>当ショップではお客様からお預かりした個人情報の利用目的は以下の通りです。</dd>
						<dd class="space">a)ご注文の確認、照会<br>
						b)商品発送の確認、照会<br>
						c)お問合せの返信時<br>
						当ショップでは、下記の場合を除いてはお客様の断りなく第三者に個人情報を開示・提供することはいたしません。<br>
						a)法令に基づく場合、及び国の機関若しくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合<br>
						b)人の生命、身体又は財産の保護のために必要がある場合であって、本人の同意を得ることが困難である場合<br>
						c)当ショップを運営する会社の関連会社で個人データを交換する場合</dd>
					</dl>
					<dl>
						<dt>4.個人情報の安全管理</dt>
						<dd>お客様よりお預かりした個人情報の安全管理はサービス提供会社によって合理的、組織的、物理的、人的、技術的施策を講じるとともに、当ショップでは関連法令に準じた適切な取扱いを行うことで個人データへの不正な侵入、個人情報の紛失、改ざん、漏えい等の危険防止に努めます。</dd>
					</dl>
					<dl>
						<dt>5.個人情報の訂正、削除</dt>
						<dd>お客様からお預かりした個人情報の訂正・削除は下記の問合せ先よりお知らせ下さい。</dd>
						<dd>また、ユーザー登録された場合、当サイトのメニュー「マイアカウント」より個人情報の訂正が出来ます。</dd>
					</dl>
					<dl>
						<dt>6.cookie(クッキー)の使用について</dt>
						<dd>当社は、お客様によりよいサービスを提供するため、cookie （クッキー）を使用することがありますが、これにより個人を特定できる情報の収集を行えるものではなく、お客様のプライバシーを侵害することはございません。</dd>
						<dd>また、cookie （クッキー）の受け入れを希望されない場合は、ブラウザの設定で変更することができます。</dd>
						<dd>※cookie （クッキー）とは、サーバーコンピュータからお客様のブラウザに送信され、お客様が使用しているコンピュータのハードディスクに蓄積される情報です。</dd>
					</dl>
					<dl>
						<dt>7.SSLの使用について</dt>
						<dd>個人情報の入力時には、セキュリティ確保のため、これらの情報が傍受、妨害または改ざんされることを防ぐ目的でSSL（Secure Sockets Layer）技術を使用しております。</dd>
						<dd>※ SSLは情報を暗号化することで、盗聴防止やデータの改ざん防止送受信する機能のことです。SSLを利用する事でより安全に情報を送信する事が可能となります。</dd>
					</dl>
					<dl>
						<dt>8.お問合せ先</dt>
						<dd>「個人情報保護方針」に関するお問い合せ先</dd>
						<dd class="big">カーテン・じゅうたん王国　　<a href="mailto:oukoku-onlineshop@oukoku.biz">E-mail：oukoku-onlineshop@oukoku.biz</a></dd>
					</dl>
					<dl>
						<dt>9.プライバシーポリシーの変更</dt>
						<dd>当ショップでは、収集する個人情報の変更、利用目的の変更、またはその他プライバシーポリシーの変更を行う際は、当ページへの変更をもって公表とさせていただきます。</dd>
					</dl>
			
			
		
		
		</div><!-- page-body inner -->
	</div><!-- page-body -->