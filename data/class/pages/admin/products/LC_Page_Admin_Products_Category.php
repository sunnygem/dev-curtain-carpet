<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * カテゴリ管理 のページクラス.
 *
 * @package Page
 * @author EC-CUBE CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Products_Category extends LC_Page_Admin_Ex
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->tpl_maintitle = '商品管理';
        $this->tpl_subtitle = 'カテゴリ登録';
        $this->tpl_mainpage = 'products/category.tpl';
        $this->tpl_mainno = 'products';
        $this->tpl_subno  = 'category';
        $this->tpl_onload = " eccube.setFocus('category_name'); ";

        // キャプションにHTMLタグ許可 20190708 nakagawa add.
        $masterData = new SC_DB_MasterData_Ex();
        $this->arrAllowedTag = $masterData->getMasterData('mtb_allowed_tag');
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $objFormParam = new SC_FormParam_Ex();
        $objCategory = new SC_Helper_Category_Ex();

        // 2019.4.4 add
        // アップロードファイル情報の初期化
        $objUpFile = new SC_UploadFile_Ex(IMAGE_TEMP_REALDIR, IMAGE_SAVE_REALDIR);
        $this->lfInitFile($objUpFile);
        // 2019.4.4 add ここまで

        // 入力パラメーター初期化
        $this->initParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();

        switch ($this->getMode()) {
            // カテゴリ登録/編集実行
            case 'edit':
                // 2019.4.11 edit 
                $this->doEdit($objFormParam, $objUpFile);
                break;
            // 入力ボックスへ編集対象のカテゴリ名をセット
            case 'pre_edit':
                //$this->doPreEdit($objFormParam);
                // 2019.4.11 edit 
                $this->doPreEdit($objFormParam,$objUpFile);
                break;
            // カテゴリ削除
            case 'delete':
                $this->doDelete($objFormParam);
                break;
            // 表示順を上へ
            case 'up':
                $this->doUp($objFormParam);
                break;
            // 表示順を下へ
            case 'down':
                $this->doDown($objFormParam);
                break;
            // FIXME r19909 によってテンプレートが削除されている
            case 'moveByDnD':
                // DnDしたカテゴリと移動先のセットを分解する
                $keys = explode('-', $_POST['keySet']);
                if ($keys[0] && $keys[1]) {
                    $objQuery = SC_Query_Ex::getSingletonInstance();
                    $objQuery->begin();

                    // 移動したデータのrank、level、parent_category_idを取得
                    $rank   = $objQuery->get('rank', 'dtb_category', 'category_id = ?', array($keys[0]));
                    $level  = $objQuery->get('level', 'dtb_category', 'category_id = ?', array($keys[0]));
                    $parent = $objQuery->get('parent_category_id', 'dtb_category', 'category_id = ?', array($keys[0]));

                    // 同一level内のrank配列を作成
                    $objQuery->setOption('ORDER BY rank DESC');
                    if ($level == 1) {
                        // 第1階層の時
                        $arrRet = $objQuery->select('rank', 'dtb_category', 'level = ?', array($level));
                    } else {
                        // 第2階層以下の時
                        $arrRet = $objQuery->select('rank', 'dtb_category', 'level = ? AND parent_category_id = ?', array($level, $parent));
                    }
                    for ($i = 0; $i < sizeof($arrRet); $i++) {
                        $rankAry[$i + 1] = $arrRet[$i]['rank'];
                    }

                    // 移動したデータのグループ内データ数
                    $my_count = $this->lfCountChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $keys[0]);
                    if ($rankAry[$keys[1]] > $rank) {
                        // データが今の位置より上がった時
                        $up_count = $rankAry[$keys[1]] - $rank;
                        $decAry   = $objQuery->select('category_id', 'dtb_category', 'level = ? AND rank > ? AND rank <= ?', array($level, $rank, $rankAry[$keys[1]]));
                        foreach ($decAry as $value) {
                            // 上のグループから減算
                            $this->lfDownRankChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $value['category_id'], $my_count);
                        }
                        // 自分のグループに加算
                        $this->lfUpRankChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $keys[0], $up_count);
                    } elseif ($rankAry[$keys[1]] < $rank) {
                        // データが今の位置より下がった時
                        $down_count = 0;
                        $incAry     = $objQuery->select('category_id', 'dtb_category', 'level = ? AND rank < ? AND rank >= ?', array($level, $rank, $rankAry[$keys[1]]));
                        foreach ($incAry as $value) {
                            // 下のグループに加算
                            $this->lfUpRankChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $value['category_id'], $my_count);
                            // 合計減算値
                            $down_count += $this->lfCountChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $value['category_id']);
                        }
                        // 自分のグループから減算
                        $this->lfDownRankChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $keys[0], $down_count);
                    }
                    $objQuery->commit();
                }
                break;
            // カテゴリツリークリック時
            case 'tree':
                break;
            // CSVダウンロード
            case 'csv':
                // CSVを送信する
                $objCSV = new SC_Helper_CSV_Ex();

                $objCSV->sfDownloadCsv('5', '', array(), '', true);
                SC_Response_Ex::actionExit();
                break;
            // 2019.4.4 add
            // 画像のアップロード
            case 'upload_image':
            case 'delete_image':
                // パラメーター初期化
                //$this->lfInitFormParam_UploadImage($objFormParam);
                // 2019.04.04 edit
                //$this->lfInitFormParam($objFormParam, $_POST);
                //$this->initParam($objFormParam, $_POST);
                $arrForm = $objFormParam->getHashArray();

                switch ($this->getMode()) {
                    case 'upload_image':
                        // ファイルを一時ディレクトリにアップロード
                        $this->arrErr[$arrForm['image_key']] = $objUpFile->makeTempFile($arrForm['image_key'], IMAGE_RENAME);

                        //if ( $_FILES['thumbnail_path']['error'] === 0 && $_FILES['thumbnail_path']['size'] !== 0 )
                        //{
                        //    $this->arrErr[$arrForm['thumbnail_path']] = $objUpFile->makeTempFile($arrForm['thumbnail_path'], IMAGE_RENAME);
                        //}
                        //if ( $_FILES['image_path']['error'] === 0 && $_FILES['image_path']['size'] !== 0 )
                        //{
                        //    $this->arrErr[$arrForm['image_path']] = $objUpFile->makeTempFile($arrForm['image_path'], IMAGE_RENAME);
                        //}
                        // 一時ファイルを本番ディレクトリに移動する
                        $this->lfSaveUploadFiles($objUpFile, $arrForm['category_id']);
                        //if ($this->arrErr[$arrForm['image_key']] == '') {
                        //    // 縮小画像作成
                        //    $this->lfSetScaleImage($objUpFile, $arrForm['image_key']);
                        //}
                        break;
                    case 'delete_image':
                        // ファイル削除
                        $this->lfDeleteFile($objUpFile, $arrForm['image_key'], $arrForm['category_id']);
                        break;
                    default:
                        break;
                }
                // 入力パラメーター初期化
                $objUpFile = new SC_UploadFile_Ex(IMAGE_TEMP_REALDIR, IMAGE_SAVE_REALDIR);
                $this->lfInitFile($objUpFile);
                $this->initParam($objFormParam);
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                $this->doPreEdit($objFormParam,$objUpFile);

                // 入力画面表示設定
                //$this->arrForm = $this->lfSetViewParam_InputPage($objUpFile, $objDownFile, $arrForm);
                break;
            default:
                break;
        }

        $parent_category_id = $objFormParam->getValue('parent_category_id');
        // 空の場合は親カテゴリを0にする
        if (empty($parent_category_id)) {
            $parent_category_id = 0;
        }
        // 親カテゴリIDの保持
        $this->arrForm['parent_category_id'] = $parent_category_id;
        // カテゴリ一覧を取得
        $this->arrList = $this->findCategoiesByParentCategoryId($parent_category_id);
        // カテゴリツリーを取得
        $this->arrTree = $objCategory->getTree();
        $this->arrParentID = $objCategory->getTreeTrail($parent_category_id);
        // ぱんくずの生成
        $arrBread = $objCategory->getTreeTrail($this->arrForm['parent_category_id'], FALSE);
        $this->tpl_bread_crumbs = SC_Utils_Ex::jsonEncode(array_reverse($arrBread));
    }

    /**
     * カテゴリの削除を実行する.
     *
     * 下記の場合は削除を実施せず、エラーメッセージを表示する.
     *
     * - 削除対象のカテゴリに、子カテゴリが1つ以上ある場合
     * - 削除対象のカテゴリを、登録商品が使用している場合
     *
     * カテゴリの削除は、物理削除で行う.
     *
     * @param  SC_FormParam $objFormParam
     * @return void
     */
    public function doDelete(&$objFormParam)
    {
        $objCategory = new SC_Helper_Category_Ex(false);
        $category_id = $objFormParam->getValue('category_id');

        // 子カテゴリのチェック
        $arrBranch = $objCategory->getTreeBranch($category_id);
        if (count($arrBranch) > 0) {
            $this->arrErr['category_name'] = '※ 子カテゴリが存在するため削除できません。<br/>';
            return;
        }
        // 登録商品のチェック
        $arrCategory = $objCategory->get($category_id);
        if ($arrCategory['product_count'] > 0) {
            $this->arrErr['category_name'] = '※ カテゴリ内に商品が存在するため削除できません。<br/>';
            return;
        }

        // ランク付きレコードの削除(※処理負荷を考慮してレコードごと削除する。)
        $objCategory->delete($category_id);
    }

    /**
     * 編集対象のカテゴリ名を, 入力ボックスへ表示する.
     *
     * @param  SC_FormParam $objFormParam
     * @param  SC_UploadFile $objUpFile // 2019.4.11 add
     * @return void
     */
    public function doPreEdit(&$objFormParam, &$objUpFile )
    {
        $category_id = $objFormParam->getValue('category_id');

        $objCategory = new SC_Helper_Category_Ex();
        $arrRes = $objCategory->get($category_id);

        $objFormParam->setParam($arrRes);

        $this->arrForm = $objFormParam->getHashArray();
        // 2019.4.11 add 
        $objUpFile->setDBFileList($this->arrForm); // 2019.4.11 add
        // 画像ファイル表示用データ取得
        $this->arrForm['arrFile'] = $objUpFile->getFormFileList(IMAGE_TEMP_URLPATH, IMAGE_SAVE_URLPATH);
        // 2019.4.11 add ここまで
 
    }

    /**
     * カテゴリの登録・編集を実行する.
     *
     * 下記の場合は, 登録・編集を実行せず、エラーメッセージを表示する
     *
     * - カテゴリ名がすでに使用されている場合
     * - 階層登録数の上限を超える場合 (登録時のみ評価)
     * - カテゴリ名がすでに使用されている場合 (登録時のみ評価)
     *
     * @param  SC_FormParam $objFormParam
     * @param  SC_UploadFile $objUpFile // 2019.4.11 add
     * @return void
     */
    public function doEdit(&$objFormParam, &$objUpFile )
    {
        $category_id = $objFormParam->getValue('category_id');

        // 追加か
        $add = strlen($category_id) === 0;

        // エラーチェック
        $this->arrErr = $this->checkError($objFormParam, $add);

        // キャプションにHTMLタグ許可 20190708 nakagawa add.
        $objErr = new SC_CheckError_Ex($objFormParam->getHashArray());
        $objErr->doFunc(array('キャプション', 'image_caption', $this->arrAllowedTag), array('HTML_TAG_CHECK'));
        $this->arrErr = array_merge( $this->arrErr, $objErr->arrErr );

        // エラーがない場合、追加・更新処理
        if (empty($this->arrErr)) {
            $arrCategory = $objFormParam->getDbArray();

            if ( isset( $arrCategory['thumbnail_path'] ) && strlen( $arrCategory['thumbnail_path'] ) === 0 ) unset( $arrCategory['thumbnail_path'] );
            if ( isset( $arrCategory['image_path'] )     && strlen( $arrCategory['image_path'] )     === 0 ) unset( $arrCategory['image_path'] );

            // 2019.4.11 add 
            unset( $arrCategory['image_key'] );

            // 追加
            if ($add) {
                $this->registerCategory($arrCategory);
            }
            // 更新
            else {
                unset($arrCategory['category_id']);
                $this->updateCategory($category_id, $arrCategory);
            }

            $this->doPreEdit( $objFormParam, $objUpFile );

        // エラーがある場合、入力値の再表示
        } else {
            $this->arrForm = $objFormParam->getHashArray();
        }
        //// 2019.4.11 add 
        //$objUpFile->setDBFileList($this->arrForm); // 2019.4.11 add
        //// 画像ファイル表示用データ取得
        //$this->arrForm['arrFile'] = $objUpFile->getFormFileList(IMAGE_TEMP_URLPATH, IMAGE_SAVE_URLPATH);
        //// 2019.4.11 add ここまで
        // 2019.4.11 add 
        $objUpFile->setDBFileList($this->arrForm); // 2019.4.11 add
        // 画像ファイル表示用データ取得
        $this->arrForm['arrFile'] = $objUpFile->getFormFileList(IMAGE_TEMP_URLPATH, IMAGE_SAVE_URLPATH);
        // 2019.4.11 add ここまで
    }

    /**
     * エラーチェック
     *
     * @param  SC_FormParam $objFormParam
     * @param  boolean      $add          追加か
     * @return void
     */
    public function checkError(&$objFormParam, $add)
    {
        $objQuery = SC_Query_Ex::getSingletonInstance();

        // 入力項目チェック
        $arrErr = $objFormParam->checkError();
        if (!empty($arrErr)) {
            return $arrErr;
        }

        $category_id = $objFormParam->getValue('category_id');
        $parent_category_id = $objFormParam->getValue('parent_category_id');
        $category_name = $objFormParam->getValue('category_name');

        // 追加の場合に固有のチェック
        if ($add) {
            // 登録数上限チェック
            $where = 'del_flg = 0';
            $count = $objQuery->count('dtb_category', $where);
            if ($count >= CATEGORY_MAX) {
                $arrErr['category_name'] = '※ カテゴリの登録最大数を超えました。<br/>';

                return $arrErr;
            }

            // 階層上限チェック
            if ($this->isOverLevel($parent_category_id)) {
                $arrErr['category_name'] = '※ ' . LEVEL_MAX . '階層以上の登録はできません。<br/>';

                return $arrErr;
            }
        }

        // 重複チェック
        $arrWhereVal = array();
        $where = 'del_flg = 0 AND parent_category_id = ? AND category_name = ?';
        $arrWhereVal[] = $parent_category_id;
        $arrWhereVal[] = $category_name;
        // 更新の場合、抽出対象から自己を除外する
        if (!$add) {
            $where .= ' AND category_id <> ?';
            $arrWhereVal[] = $category_id;
        }
        $exists = $objQuery->exists('dtb_category', $where, $arrWhereVal);
        if ($exists) {
            $arrErr['category_name'] = '※ 既に同じ内容の登録が存在します。<br/>';

            return $arrErr;
        }

        return $arrErr;
    }

    /**
     * カテゴリの表示順序を上へ移動する.
     *
     * @param  SC_FormParam $objFormParam
     * @return void
     */
    public function doUp(&$objFormParam)
    {
        $category_id = $objFormParam->getValue('category_id');

        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $up_id = $this->lfGetUpRankID($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $category_id);
        if ($up_id != '') {
            // 上のグループのrankから減算する数
            $my_count = $this->lfCountChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $category_id);
                // 自分のグループのrankに加算する数
                $up_count = $this->lfCountChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $up_id);
                if ($my_count > 0 && $up_count > 0) {
                    // 自分のグループに加算
                    $this->lfUpRankChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $category_id, $up_count);
                    // 上のグループから減算
                    $this->lfDownRankChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $up_id, $my_count);
                }
        }
        $objQuery->commit();
    }

    /**
     * カテゴリの表示順序を下へ移動する.
     *
     * @param  SC_FormParam $objFormParam
     * @return void
     */
    public function doDown(&$objFormParam)
    {
        $category_id = $objFormParam->getValue('category_id');

        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $down_id = $this->lfGetDownRankID($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $category_id);
        if ($down_id != '') {
            // 下のグループのrankに加算する数
            $my_count = $this->lfCountChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $category_id);
            // 自分のグループのrankから減算する数
            $down_count = $this->lfCountChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $down_id);
            if ($my_count > 0 && $down_count > 0) {
                // 自分のグループから減算
                $this->lfUpRankChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $down_id, $my_count);
                // 下のグループに加算
                $this->lfDownRankChilds($objQuery, 'dtb_category', 'parent_category_id', 'category_id', $category_id, $down_count);
            }
        }
        $objQuery->commit();
    }

    /**
     * パラメーターの初期化を行う
     *
     * @param  SC_FormParam $objFormParam
     * @return void
     */
    public function initParam(&$objFormParam)
    {
        $objFormParam->addParam('親カテゴリID', 'parent_category_id', null, null, array());
        $objFormParam->addParam('カテゴリID', 'category_id', null, null, array());
        $objFormParam->addParam('カテゴリ名', 'category_name', STEXT_LEN, 'KVa', array('EXIST_CHECK', 'SPTAB_CHECK', 'MAX_LENGTH_CHECK'));
        // 2019.4.4 add
        $objFormParam->addParam('画像', 'image_key', '', '', array());
        $objFormParam->addParam('サムネイル', 'thumbnail_path', '', '', array());
        $objFormParam->addParam('メイン画像', 'image_path', '', '', array());
        $objFormParam->addParam('キャプション', 'image_caption', LTEXT_LEN, 'KVa', array('SPTAB_CHECK', 'MAX_LENGTH_CHECK'));
    }
    // 2019.4.4 add
    /**
     * アップロードファイルパラメーター情報の初期化
     * - 画像ファイル用
     *
     * @param  SC_UploadFile_Ex $objUpFile SC_UploadFileインスタンス
     * @return void
     */
    public function lfInitFile(&$objUpFile)
    {
        //$objUpFile->addFile('image_key', 'image_key', array('jpg', 'gif', 'png'), IMAGE_SIZE, false, NORMAL_IMAGE_WIDTH, NORMAL_IMAGE_HEIGHT);
        $objUpFile->addFile('thumbnail_path', 'thumbnail_path', array('jpg', 'gif', 'png'), IMAGE_SIZE, false, CATEGORY_THUMBNAIL_IMAGE_WIDTH, CATEGORY_THUMBNAIL_IMAGE_HEIGHT);
        $objUpFile->addFile('image_path', 'image_path', array('jpg', 'gif', 'png'), IMAGE_SIZE, false, CATEGORY_MAIN_IMAGE_WIDTH, CATEGORY_MAIN_IMAGE_HEIGHT);
    }
    /**
     * パラメーター情報の初期化
     * - 画像ファイルアップロードモード
     *
     * @param  SC_FormParam_Ex $objFormParam SC_FormParamインスタンス
     * @return void
     */
    public function lfInitFormParam_UploadImage(&$objFormParam)
    {
        $objFormParam->addParam('image_key', 'thumbnail_path', '', '', array());
        $objFormParam->addParam('image_key', 'image_path', '', '', array());
    }
    /**
     * 表示用フォームパラメーター取得
     * - 入力画面
     *
     * @param  SC_UploadFile_Ex $objUpFile   SC_UploadFileインスタンス
     * @param  array  $arrForm     フォーム入力パラメーター配列
     * @return array  表示用フォームパラメーター配列
     */
    public function lfSetViewParam_InputPage(&$objUpFile, &$arrForm)
    {
        // カテゴリマスターデータ取得
        $objDb = new SC_Helper_DB_Ex();
        list($this->arrCatVal, $this->arrCatOut) = $objDb->sfGetLevelCatList(false);

        if (isset($arrForm['category_id']) && !is_array($arrForm['category_id'])) {
            $arrForm['category_id'] = SC_Utils_Ex::jsonDecode($arrForm['category_id']);
        }
        $this->tpl_json_category_id = !empty($arrForm['category_id']) ? SC_Utils_Ex::jsonEncode($arrForm['category_id']) : SC_Utils_Ex::jsonEncode(array());
        if ($arrForm['status'] == '') {
            $arrForm['status'] = DEFAULT_PRODUCT_DISP;
        }
        if ($arrForm['product_type_id'] == '') {
            $arrForm['product_type_id'] = DEFAULT_PRODUCT_DOWN;
        }
        if (OPTION_PRODUCT_TAX_RULE) {
            // 編集の場合は設定された税率、新規の場合はデフォルトの税率を取得
            if ($arrForm['product_id'] == '') {
                $arrRet = SC_Helper_TaxRule_Ex::getTaxRule();
            } else {
                $arrRet = SC_Helper_TaxRule_Ex::getTaxRule($arrForm['product_id'], $arrForm['product_class_id']);
            }
            $arrForm['tax_rate'] = $arrRet['tax_rate'];
        }
        // アップロードファイル情報取得(Hidden用)
        $arrForm['arrHidden'] = $arrHidden = $objUpFile->getHiddenFileList();

        // 画像ファイル表示用データ取得
        $arrForm['arrFile'] = $objUpFile->getFormFileList(IMAGE_TEMP_URLPATH, IMAGE_SAVE_URLPATH);

        // 基本情報(デフォルトポイントレート用)
        $arrForm['arrInfo'] = SC_Helper_DB_Ex::sfGetBasisData();

        return $arrForm;
    }
    /**
     * アップロードファイルを保存する
     *
     * @param  object  $objUpFile   SC_UploadFileインスタンス
     * @param  integer $product_id  商品ID
     * @return void
     */
    public function lfSaveUploadFiles(&$objUpFile, $category_id )
    {
        // TODO: SC_UploadFile::moveTempFileの画像削除条件見直し要
        $objImage = new SC_Image_Ex($objUpFile->temp_dir);
        $arrKeyName = $objUpFile->keyname;
        $arrTempFile = $objUpFile->temp_file;
        $arrSaveFile = $objUpFile->save_file;
        $arrImageKey = array();
        foreach ($arrTempFile as $key => $temp_file) {
            if ($temp_file) {
                $objImage->moveTempImage($temp_file, $objUpFile->save_dir);
                $arrImageKey[] = $arrKeyName[$key];
                if (!empty($arrSaveFile[$key])
                    && !$this->lfHasSameCategoryImage($category_id, $arrImageKey, $arrSaveFile[$key])
                    && !in_array($temp_file, $arrSaveFile)
                ) {
                    $objImage->deleteImage($arrSaveFile[$key], $objUpFile->save_dir);
                }
                else
                {
                    $objQuery =& SC_Query_Ex::getSingletonInstance();
                    $sqlval = array( $arrKeyName[$key] => $temp_file );
                    $where  = 'category_id=?';
                    $objQuery->update( 'dtb_category', $sqlval, $where, array( $category_id ) );
                }
            }
        }
    }
    /**
     * アップロードファイルパラメーター情報から削除
     * 一時ディレクトリに保存されている実ファイルも削除する
     *
     * @param  SC_UploadFile_Ex $objUpFile SC_UploadFileインスタンス
     * @param  string $image_key 画像ファイルキー
     * @return void
     */
    public function lfDeleteFile(&$objUpFile, $image_key, $category_id)
    {
        // TODO: SC_UploadFile::deleteFileの画像削除条件見直し要
        $arrTempFile = $objUpFile->temp_file;
        $arrKeyName = $objUpFile->keyname;

        foreach ($arrKeyName as $key => $keyname) {
            if ($keyname != $image_key) continue;

            if (!empty($arrTempFile[$key])) {
                $temp_file = $arrTempFile[$key];
                $arrTempFile[$key] = '';

                if (!in_array($temp_file, $arrTempFile)) {
                    $objUpFile->deleteFile($image_key);
                } else {
                    $objUpFile->temp_file[$key] = '';
                    $objUpFile->save_file[$key] = '';
                }
            } else {
                $objUpFile->temp_file[$key] = '';
                $objUpFile->save_file[$key] = '';
            }
            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $sqlval = array( $arrKeyName[$key] => '' );
            $where  = 'category_id=?';
            $objQuery->update( 'dtb_category', $sqlval, $where, array( $category_id ) );
        }
    }
     /*
     * 画像ファイルの削除可否判定用。
     * 同名ファイルの登録がある場合には画像ファイルの削除を行わない。
     * 戻り値： 同名ファイル有り(true) 同名ファイル無し(false)
     *
     * @param  string  $category_id カテゴリID
     * @param  string  $arrImageKey     対象としない画像カラム名
     * @param  string  $image_file_name 画像ファイル名
     * @return boolean
     */
    public function lfHasSameCategoryImage($category_id, $arrImageKey, $image_file_name)
    {
        if (!SC_Utils_Ex::sfIsInt($category_id)) return false;
        if (!$arrImageKey) return false;
        if (!$image_file_name) return false;

        $arrWhere = array();
        $sqlval = array('0', $category_id);
        foreach ($arrImageKey as $image_key) {
            $arrWhere[] = "{$image_key} = ?";
            $sqlval[] = $image_file_name;
        }
        $where = implode(' OR ', $arrWhere);
        $where = "del_flg = ? AND ((category_id<> ? AND ({$where}))";

        $arrKeyName = $this->objUpFile->keyname;
        foreach ($arrKeyName as $key => $keyname) {
            if (in_array($keyname, $arrImageKey)) continue;
            $where .= " OR {$keyname} = ?";
            $sqlval[] = $image_file_name;
        }
        $where .= ')';

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $exists = $objQuery->exists('dtb_category', $where, $sqlval);

        return $exists;
    }

    // 2019.4.4 add ここまで


    /**
     * 親カテゴリIDでカテゴリを検索する.
     *
     * - 表示順の降順でソートする
     * - 有効なカテゴリを返す(del_flag = 0)
     *
     * @param  SC_Query $objQuery
     * @param  int      $parent_category_id 親カテゴリID
     * @return array    カテゴリの配列
     */
    public function findCategoiesByParentCategoryId($parent_category_id)
    {
        if (!$parent_category_id) {
            $parent_category_id = 0;
        }
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $col   = 'category_id, category_name, level, rank';
        $where = 'del_flg = 0 AND parent_category_id = ?';
        $objQuery->setOption('ORDER BY rank DESC');

        return $objQuery->select($col, 'dtb_category', $where, array($parent_category_id));
    }

    /**
     * カテゴリを更新する
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function updateCategory($category_id, $arrCategory)
    {
        $objQuery = SC_Query_Ex::getSingletonInstance();

        $arrCategory['update_date']   = 'CURRENT_TIMESTAMP';

        $objQuery->begin();
        $where = 'category_id = ?';
        $objQuery->update('dtb_category', $arrCategory, $where, array($category_id));
        $objQuery->commit();
    }

    /**
     * カテゴリを登録する
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function registerCategory($arrCategory)
    {
        $objQuery = SC_Query_Ex::getSingletonInstance();

        $parent_category_id = $arrCategory['parent_category_id'];

        $objQuery->begin();

        $rank = null;
        if ($parent_category_id == 0) {
            // ROOT階層で最大のランクを取得する。
            $where = 'parent_category_id = ?';
            $rank = $objQuery->max('rank', 'dtb_category', $where, array($parent_category_id)) + 1;
        } else {
            // 親のランクを自分のランクとする。
            $where = 'category_id = ?';
            $rank = $objQuery->get('rank', 'dtb_category', $where, array($parent_category_id));
            // 追加レコードのランク以上のレコードを一つあげる。
            $where = 'rank >= ?';
            $arrRawSql = array(
                'rank' => '(rank + 1)',
            );
            $objQuery->update('dtb_category', array(), $where, array($rank), $arrRawSql);
        }

        $where = 'category_id = ?';
        // 自分のレベルを取得する(親のレベル + 1)
        $level = $objQuery->get('level', 'dtb_category', $where, array($parent_category_id)) + 1;

        $arrCategory['create_date'] = 'CURRENT_TIMESTAMP';
        $arrCategory['update_date'] = 'CURRENT_TIMESTAMP';
        $arrCategory['creator_id']  = $_SESSION['member_id'];
        $arrCategory['rank']        = $rank;
        $arrCategory['level']       = $level;
        $arrCategory['category_id'] = $objQuery->nextVal('dtb_category_category_id');

        $objQuery->insert('dtb_category', $arrCategory);

        $objQuery->commit();    // トランザクションの終了
    }

    /**
     * カテゴリの階層が上限を超えているかを判定する
     *
     * @param integer 親カテゴリID
     * @param 超えている場合 true
     */
    public function isOverLevel($parent_category_id)
    {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $level = $objQuery->get('level', 'dtb_category', 'category_id = ?', array($parent_category_id));

        return $level >= LEVEL_MAX;
    }

    // 並びが1つ下のIDを取得する。
    public function lfGetDownRankID($objQuery, $table, $pid_name, $id_name, $id)
    {
        // 親IDを取得する。
        $col = "$pid_name";
        $where = "$id_name = ?";
        $pid = $objQuery->get($col, $table, $where, $id);
        // 全ての子を取得する。
        $col = "$id_name";
        $where = "del_flg = 0 AND $pid_name = ? ORDER BY rank DESC";
        $arrRet = $objQuery->select($col, $table, $where, array($pid));
        $max = count($arrRet);
        $down_id = '';
        for ($cnt = 0; $cnt < $max; $cnt++) {
            if ($arrRet[$cnt][$id_name] == $id) {
                $down_id = $arrRet[($cnt + 1)][$id_name];
                break;
            }
        }

        return $down_id;
    }

    // 並びが1つ上のIDを取得する。
    public function lfGetUpRankID($objQuery, $table, $pid_name, $id_name, $id)
    {
        // 親IDを取得する。
        $col = "$pid_name";
        $where = "$id_name = ?";
        $pid = $objQuery->get($col, $table, $where, $id);
        // 全ての子を取得する。
        $col = "$id_name";
        $where = "del_flg = 0 AND $pid_name = ? ORDER BY rank DESC";
        $arrRet = $objQuery->select($col, $table, $where, array($pid));
        $max = count($arrRet);
        $up_id = '';
        for ($cnt = 0; $cnt < $max; $cnt++) {
            if ($arrRet[$cnt][$id_name] == $id) {
                $up_id = $arrRet[($cnt - 1)][$id_name];
                break;
            }
        }

        return $up_id;
    }

    public function lfCountChilds($objQuery, $table, $pid_name, $id_name, $id)
    {
        $objDb = new SC_Helper_DB_Ex();
        // 子ID一覧を取得
        $arrRet = $objDb->sfGetChildrenArray($table, $pid_name, $id_name, $id);

        return count($arrRet);
    }

    /**
     * @param SC_Query $objQuery
     * @param string $table
     * @param string $pid_name
     * @param string $id_name
     */
    public function lfUpRankChilds($objQuery, $table, $pid_name, $id_name, $id, $count)
    {
        $objDb = new SC_Helper_DB_Ex();
        // 子ID一覧を取得
        $arrRet = $objDb->sfGetChildrenArray($table, $pid_name, $id_name, $id);
        $line = SC_Utils_Ex::sfGetCommaList($arrRet);
        $where = "$id_name IN ($line) AND del_flg = 0";
        $arrRawVal = array(
            'rank' => "(rank + $count)",
        );

        return $objQuery->update($table, array(), $where, array(), $arrRawVal);
    }

    /**
     * @param SC_Query $objQuery
     * @param string $table
     * @param string $pid_name
     * @param string $id_name
     * @param integer $count
     */
    public function lfDownRankChilds($objQuery, $table, $pid_name, $id_name, $id, $count)
    {
        $objDb = new SC_Helper_DB_Ex();
        // 子ID一覧を取得
        $arrRet = $objDb->sfGetChildrenArray($table, $pid_name, $id_name, $id);
        $line = SC_Utils_Ex::sfGetCommaList($arrRet);
        $where = "$id_name IN ($line) AND del_flg = 0";
        $arrRawVal = array(
            'rank' => "(rank - $count)",
        );

        return $objQuery->update($table, array(), $where, array(), $arrRawVal);
    }
}
