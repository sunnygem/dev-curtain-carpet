<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * 配送方法設定 のページクラス.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Affiliate_Id extends LC_Page_Admin_Ex
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->tpl_mainpage = 'affiliate/affiliateid.tpl';
        $this->tpl_subno = 'affiliateid';
        $this->tpl_mainno = 'affiliate';
        $this->tpl_maintitle = 'アフィリエイト管理';
        $this->tpl_subtitle = 'アフィリエイトID管理';
        $masterData = new SC_DB_MasterData_Ex();
        $this->mode = $this->getMode();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $objAffiliate = new SC_Helper_Affiliate_Ex();
        $mode = $this->getMode();
        if (!empty($_POST)) {
            $objFormParam = new SC_FormParam_Ex();
            $objFormParam->setParam($_POST);

            $this->arrErr = $this->lfCheckError($mode, $objFormParam);
            if (!empty($this->arrErr['affiliate_id'])) {
                trigger_error('', E_USER_ERROR);

                return;
            }
        }

        switch ($mode) {
            case 'delete':
                // ランク付きレコードの削除
                $objAffiliate->delete($_POST['affiliate_id']);
                $this->objDisplay->reload(); // PRG pattern
                break;
            default:
                break;
        }
        $this->arrAffiliateList = $objAffiliate->getList();
    }

    /**
     * 入力エラーチェック
     *
     * @param  string $mode
     * @param SC_FormParam_Ex $objFormParam
     * @return array
     */
    public function lfCheckError($mode, &$objFormParam)
    {
        $arrErr = array();
        switch ($mode) {
            case 'delete':
                $objFormParam->addParam('ID', 'affiliate_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));

                $objFormParam->convParam();

                $arrErr = $objFormParam->checkError();
                break;
            default:
                break;
        }

        return $arrErr;
    }
}
