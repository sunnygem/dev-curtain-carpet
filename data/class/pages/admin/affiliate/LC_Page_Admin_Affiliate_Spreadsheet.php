<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * 商品管理 のページクラス.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Affiliate_Spreadsheet extends LC_Page_Admin_Ex
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->tpl_mainpage = 'affiliate/spreadsheet.tpl';
        $this->tpl_mainno = 'affiliate';
        $this->tpl_subno = 'spreadsheet';
        $this->tpl_pager = 'pager.tpl';
        $this->tpl_maintitle = 'アフィリエイト管理';
        $this->tpl_subtitle = '集計表';

        $masterData = new SC_DB_MasterData_Ex();
        $this->arrPageMax = $masterData->getMasterData('mtb_page_max');
        $this->arrDISP = $masterData->getMasterData('mtb_disp');
        $this->arrSTATUS = $masterData->getMasterData('mtb_status');
        $this->arrPRODUCTSTATUS_COLOR = $masterData->getMasterData('mtb_product_status_color');

        $objDate = new SC_Date_Ex();
        // 登録・更新検索開始年
        $objDate->setStartYear(RELEASE_YEAR);
        $objDate->setEndYear(DATE('Y'));
        $this->arrStartYear = $objDate->getYear();
        $this->arrStartMonth = $objDate->getMonth();
        $this->arrStartDay = $objDate->getDay();
        // 登録・更新検索終了年
        $objDate->setStartYear(RELEASE_YEAR);
        $objDate->setEndYear(DATE('Y'));
        $this->arrEndYear = $objDate->getYear();
        $this->arrEndMonth = $objDate->getMonth();
        $this->arrEndDay = $objDate->getDay();
        // メーカーリスト作成
        $this->arrMakerList = $this->sfGetMakerList();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $objDb = new SC_Helper_DB_Ex();
        $objFormParam = new SC_FormParam_Ex();
        $objProduct = new SC_Product_Ex();
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // パラメーター情報の初期化
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $this->arrHidden = $objFormParam->getSearchArray();
        $this->arrForm = $objFormParam->getFormParamList();

        switch ($this->getMode()) {
            case 'delete':

            // 検索パラメーター生成後に処理実行するため breakしない
            case 'csv':
            case 'delete_all':

            case 'search':
                $objFormParam->convParam();
                $objFormParam->trimParam();
                $this->arrErr = $this->lfCheckError($objFormParam);
                $arrParam = $objFormParam->getHashArray();

                if (count($this->arrErr) == 0) {
                    $where = 'del_flg = 0';
                    $arrWhereVal = array();
                    foreach ($arrParam as $key => $val) {
                        if ($val == '') {
                            continue;
                        }
                        $this->buildQuery($key, $where, $arrWhereVal, $objFormParam, $objDb);
                    }

                    $order = 'update_date DESC';

                    /* -----------------------------------------------
                     * 処理を実行
                     * ----------------------------------------------- */
                    switch ($this->getMode()) {
                        // CSVを送信する。
                        case 'csv':
                            $objCSV = new SC_Helper_CSV_Ex();
                            // CSVを送信する。正常終了の場合、終了。
                            $objCSV->sfDownloadCsv(1, $where, $arrWhereVal, $order, true);
 var_dump($objCSV);
                            SC_Response_Ex::actionExit();

                        // 検索実行
                        default:
							$this->arrAffiliateList = $this->sfGetAffiliateList($objFormParam);
                            // 行数の取得
                            $this->tpl_linemax = count($this->arrAffiliateList);
                            // ページ送りの処理
                            $page_max = SC_Utils_Ex::sfGetSearchPageMax($objFormParam->getValue('search_page_max'));
                            // ページ送りの取得
                            $objNavi = new SC_PageNavi_Ex($this->arrHidden['search_pageno'],
                                                          $this->tpl_linemax, $page_max,
                                                          'eccube.moveNaviPage', NAVI_PMAX);
                            $this->arrPagenavi = $objNavi->arrPagenavi;

 /*                           // 検索結果の取得
                            $this->arrAffiliatelog = $this->findAffiliatelog($where, $arrWhereVal, $page_max, $objNavi->start_row,
                                                                     $order, $objProduct);
*/
 /*                           if (count($this->arrAffiliatelog) > 0) {
                                foreach ($this->arrProducts as $key => $val) {
                                    $this->arrProducts[$key]['categories'] = $objProduct->getCategoryIds($val['product_id'], true);
                                    $objDb->g_category_on = false;
                                }
                            }
 */
					}
                }
                break;
        }

    }

    /**
     * パラメーター情報の初期化を行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function lfInitParam(&$objFormParam)
    {
        // POSTされる値
        $objFormParam->addParam('ページ送り番号', 'search_pageno', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('表示件数', 'search_page_max', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));

        // 検索条件
        $objFormParam->addParam('メーカー', 'search_maker_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        // 登録・更新日
        $objFormParam->addParam('開始年', 'search_startyear', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('開始月', 'search_startmonth', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('開始日', 'search_startday', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('終了年', 'search_endyear', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('終了月', 'search_endmonth', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('終了日', 'search_endday', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));

    }

    /**
     * 入力内容のチェックを行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function lfCheckError(&$objFormParam)
    {
        $objErr = new SC_CheckError_Ex($objFormParam->getHashArray());
        $objErr->arrErr = $objFormParam->checkError();

        $objErr->doFunc(array('開始日', '終了日', 'search_startyear', 'search_startmonth', 'search_startday', 'search_endyear', 'search_endmonth', 'search_endday'), array('CHECK_SET_TERM'));

        return $objErr->arrErr;
    }

    // カテゴリIDをキー、カテゴリ名を値にする配列を返す。
    public function lfGetIDName($arrCatKey, $arrCatVal)
    {
        $max = count($arrCatKey);
        for ($cnt = 0; $cnt < $max; $cnt++) {
            $key = isset($arrCatKey[$cnt]) ? $arrCatKey[$cnt] : '';
            $val = isset($arrCatVal[$cnt]) ? $arrCatVal[$cnt] : '';
            $arrRet[$key] = $val;
        }

        return $arrRet;
    }


    /**
     * クエリを構築する.
     *
     * 検索条件のキーに応じた WHERE 句と, クエリパラメーターを構築する.
     * クエリパラメーターは, SC_FormParam の入力値から取得する.
     *
     * 構築内容は, 引数の $where 及び $arrValues にそれぞれ追加される.
     *
     * @param  string       $key          検索条件のキー
     * @param  string       $where        構築する WHERE 句
     * @param  array        $arrValues    構築するクエリパラメーター
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @param  SC_FormParam $objDb        SC_Helper_DB_Ex インスタンス
     * @return void
     */
    public function buildQuery($key, &$where, &$arrValues, &$objFormParam, &$objDb)
    {
        $dbFactory = SC_DB_DBFactory_Ex::getInstance();
        switch ($key) {
            // メーカー
            case 'search_maker_id':
                $where .= ' AND maker_id = ?';
                $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
                break;

            // 登録・更新日(開始)
            case 'search_startyear':
                $date = SC_Utils_Ex::sfGetTimestamp($objFormParam->getValue('search_startyear'),
                                                    $objFormParam->getValue('search_startmonth'),
                                                    $objFormParam->getValue('search_startday'));
                $where.= ' AND update_date >= ?';
                $arrValues[] = $date;
                break;
            // 登録・更新日(終了)
            case 'search_endyear':
                $date = SC_Utils_Ex::sfGetTimestamp($objFormParam->getValue('search_endyear'),
                                                    $objFormParam->getValue('search_endmonth'),
                                                    $objFormParam->getValue('search_endday'), true);
                $where.= ' AND update_date <= ?';
                $arrValues[] = $date;
                break;
            default:
                break;
        }
    }

    /**
     * 検索結果の行数を取得する.
     *
     * @param  string  $where     検索条件の WHERE 句
     * @param  array   $arrValues 検索条件のパラメーター
     * @return integer 検索結果の行数
     */
    public function getNumberOfLines($where, $arrValues)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        return $objQuery->count('dtb_affiliate', $where, $arrValues);
    }


/*	
    public function findAffiliatelog($where, $arrValues, $limit, $offset, $order, &$objProduct)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 読み込む列とテーブルの指定
        $col = 'product_id, name, main_list_image, status, product_code_min, product_code_max, price02_min, price02_max, stock_min, stock_max, stock_unlimited_min, stock_unlimited_max, update_date';
        $from = $objProduct->alldtlSQL();

        $objQuery->setLimitOffset($limit, $offset);
        $objQuery->setOrder($order);

        return $objQuery->select($col, $from, $where, $arrValues);
    }
*/
    public function sfGetMakerList()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // カテゴリ名リストを取得
        $col = 'maker_id, name';
        $where = 'del_flg = 0';
        $objQuery->setOption('ORDER BY maker_id');
        $arrRet = $objQuery->select($col, 'dtb_maker', $where);
		$arrRes=array();
		for($i=0;$i<count($arrRet);$i++){
			$arrRes[$arrRet[$i]['maker_id']]=$arrRet[$i]['name'];
		}
        return $arrRes;
    }

    public function sfGetAffiliateList(&$objFormParam)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // カテゴリ名リストを取得
        $col = 'affiliate_id, name';
        $where = 'del_flg = 0';
        $objQuery->setOption('ORDER BY affiliate_id');
        $arrRet = $objQuery->select($col, 'dtb_affiliate', $where);
		for($i=0;$i<count($arrRet);$i++){
			$arrRet[$i]['accesscount']=number_format($this->sfGetAffiliateDetail(1,$objFormParam,$arrRet[$i]['affiliate_id']));
			$arrRet[$i]['accessperson']=number_format($this->sfGetAffiliateDetail(2,$objFormParam,$arrRet[$i]['affiliate_id']));
			$arrRet[$i]['cv']=number_format($this->sfGetAffiliateDetail(3,$objFormParam,$arrRet[$i]['affiliate_id']));
			$arrRet[$i]['cvprice']=number_format($this->sfGetAffiliateDetail(4,$objFormParam,$arrRet[$i]['affiliate_id']));
		}
		$arrRet[$i]['affiliate_id']=0;
		$arrRet[$i]['name']="その他";
		$arrRet[$i]['accesscount']=number_format($this->sfGetAffiliateDetail(1,$objFormParam,0));
		$arrRet[$i]['accessperson']=number_format($this->sfGetAffiliateDetail(2,$objFormParam,0));
		$arrRet[$i]['cv']=number_format($this->sfGetAffiliateDetail(3,$objFormParam,0));
		$arrRet[$i]['cvprice']=number_format($this->sfGetAffiliateDetail(4,$objFormParam,0));
		$arrRet[$i+1]['affiliate_id']=-1;
		$arrRet[$i+1]['name']="合計";
		$arrRet[$i+1]['accesscount']=number_format($this->sfGetAffiliateDetail(1,$objFormParam,-1));
		$arrRet[$i+1]['accessperson']=number_format($this->sfGetAffiliateDetail(2,$objFormParam,-1));
		$arrRet[$i+1]['cv']=number_format($this->sfGetAffiliateDetail(3,$objFormParam,-1));
		$arrRet[$i+1]['cvprice']=number_format($this->sfGetAffiliateDetail(4,$objFormParam,-1));
        return $arrRet;
    }

    public function sfGetAffiliateDetail($mode,&$objFormParam,$affiliate_id)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
		$where="";
		if($objFormParam->getValue('search_maker_id')!=""){
			$where.=sprintf(' maker_id=%d',$objFormParam->getValue('search_maker_id'));
		} 
		if($objFormParam->getValue('search_startyear')!="" && $objFormParam->getValue('search_startmonth')!="" && $objFormParam->getValue('search_startday')!="") {
			if($where!=""){ $where.=" and "; }
			$where.=sprintf(" create_date>='%d/%d/%d'",$objFormParam->getValue('search_startyear'),$objFormParam->getValue('search_startmonth'),$objFormParam->getValue('search_startday'));
		}
		if($objFormParam->getValue('search_endyear')!="" && $objFormParam->getValue('search_endmonth')!="" && $objFormParam->getValue('search_endday')!="") {
			if($where!=""){ $where.=" and "; }
			$where.=sprintf(" create_date<='%d/%d/%d'",$objFormParam->getValue('search_endyear'),$objFormParam->getValue('search_endmonth'),$objFormParam->getValue('search_endday'));
		}
		if($affiliate_id!=-1){
			if($where!=""){ $where.=" and "; }
			$where.=sprintf(' affiliate_id=%d',$affiliate_id);
		} 
		if($where!=""){ $where.=" and "; }
		$where.= 'del_flg = 0';
		if($mode==1){
			$col = 'count(*) as cnt';
			$arrRet = $objQuery->select($col, 'dtb_affiliate_log', $where);
			return $arrRet[0]['cnt'];
		} elseif($mode==2){
			$col = 'count(DISTINCT affiliate_user_id) as cnt';
			$arrRet = $objQuery->select($col, 'dtb_affiliate_log', $where);
			return $arrRet[0]['cnt'];
		} elseif($mode==3){
			if($where!=""){ $where.=" and "; }
			$where.= 'del_flg = 0 and conversion=1';
			$col = 'count(*) as cnt';
			$arrRet = $objQuery->select($col, 'dtb_affiliate_log', $where);
			return $arrRet[0]['cnt'];
		} elseif($mode==4){
			if($where!=""){ $where.=" and "; }
			$where.= 'del_flg = 0 and conversion=1';
			$col = 'sum(totalprice) as price';
			$arrRet = $objQuery->select($col, 'dtb_affiliate_log', $where);
			return $arrRet[0]['price'];
		}
	}

    public function AffiliateDownloadCsv($csv_id, $where = '', $arrVal = array(), $order = '', $is_download = false)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // CSV出力タイトル行の作成
        $arrOutput = SC_Utils_Ex::sfSwapArray($this->sfGetCsvOutput($csv_id, 'status = ' . CSV_COLUMN_STATUS_FLG_ENABLE));
        if (count($arrOutput) <= 0) return false; // 失敗終了
        $arrOutputCols = $arrOutput['col'];

        $cols = SC_Utils_Ex::sfGetCommaList($arrOutputCols, true);

        // 商品の場合
        if (true) {
           $from = 'dtb_category';
        }

        $objQuery->setOrder($order);
        $sql = $objQuery->getSql($cols, $from, $where);

        return $this->sfDownloadCsvFromSql($sql, $arrVal, $this->arrSubnavi[$csv_id], $arrOutput['disp_name'], $is_download);
    }

}
