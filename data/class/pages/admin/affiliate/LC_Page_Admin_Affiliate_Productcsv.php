<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * 商品管理 のページクラス.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Affiliate_Productcsv extends LC_Page_Admin_Ex
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->tpl_mainpage = 'affiliate/productcsv.tpl';
        $this->tpl_mainno = 'affiliate';
        $this->tpl_subno = 'productcsv';
        $this->tpl_pager = 'pager.tpl';
        $this->tpl_maintitle = 'アフィリエイト管理';
        $this->tpl_subtitle = '商品リストCSV';

        $masterData = new SC_DB_MasterData_Ex();
        $this->arrPageMax = $masterData->getMasterData('mtb_page_max');
        $this->arrDISP = $masterData->getMasterData('mtb_disp');
        $this->arrSTATUS = $masterData->getMasterData('mtb_status');
        $this->arrPRODUCTSTATUS_COLOR = $masterData->getMasterData('mtb_product_status_color');

        // アフィリエイトID作成
        $this->arrAffiliate = $this->sfGetAffiliateName();
        $this->arrAffiliateID = $this->sfGetAffiliateID();
   }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $objDb = new SC_Helper_DB_Ex();
        $objFormParam = new SC_FormParam_Ex();
        $objProduct = new SC_Product_Ex();
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // パラメーター情報の初期化
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $this->arrHidden = $objFormParam->getSearchArray();
        $this->arrForm = $objFormParam->getFormParamList();

        switch ($this->getMode()) {

            case 'csv':
 
            case 'search':
                $objFormParam->convParam();
                $objFormParam->trimParam();
                $this->arrErr = $this->lfCheckError($objFormParam);
                $arrParam = $objFormParam->getHashArray();

                if (count($this->arrErr) == 0) {
                    $where = 'del_flg = 0';
                    $arrWhereVal = array();
                    foreach ($arrParam as $key => $val) {
                        if ($val == '') {
                            continue;
                        }
                        $this->buildQuery($key, $where, $arrWhereVal, $objFormParam, $objDb);
                    }

                    $order = 'product_id ASC';

                    /* -----------------------------------------------
                     * 処理を実行
                     * ----------------------------------------------- */
                    switch ($this->getMode()) {
                        // CSVを送信する。
                        case 'csv':
                            $objCSV = new SC_Helper_CSV_Ex();
                            // CSVを送信する。正常終了の場合、終了。
							$objCSV->sfDownloadCsv(7, $where, $arrWhereVal, $order, true,$this->arrAffiliateID[$_POST['search_affiliate_id']]);
                            SC_Response_Ex::actionExit();

                        // 検索実行
                        default:
                            $this->arrProductCSV = $this->findProducts($where, $arrWhereVal, '', '',$order);

							// 行数の取得
                            $this->tpl_linemax = count($this->arrProductCSV);
                            // ページ送りの処理
                            $page_max = SC_Utils_Ex::sfGetSearchPageMax($objFormParam->getValue('search_page_max'));
                            // ページ送りの取得
                            $objNavi = new SC_PageNavi_Ex($this->arrHidden['search_pageno'],
                                                          $this->tpl_linemax, $page_max,
                                                          'eccube.moveNaviPage', NAVI_PMAX);
                            $this->arrPagenavi = $objNavi->arrPagenavi;

                            $this->arrProductCSV = $this->findProducts($where, $arrWhereVal, $page_max, $objNavi->start_row,$order);

							if (count($this->arrProductCSV) > 0) {
                                foreach ($this->arrProductCSV as $key => $val) {
                                    $this->arrProductCSV[$key]['URL'] = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"].'/products/detail.php?product_id='.$val['product_id'].'&aid='.$this->arrAffiliateID[$_POST['search_affiliate_id']];
                                }
                            }
                    }
                }
                break;
        }
    }

    /**
     * パラメーター情報の初期化を行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function lfInitParam(&$objFormParam)
    {
        // POSTされる値
        $objFormParam->addParam('ページ送り番号', 'search_pageno', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('表示件数', 'search_page_max', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));

        // 検索条件
        $objFormParam->addParam('アフィリエイトID', 'search_affiliate_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('開始商品ID', 'start_product_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('終了商品ID', 'end_product_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
    }

    /**
     * 入力内容のチェックを行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function lfCheckError(&$objFormParam)
    {
        $objErr = new SC_CheckError_Ex($objFormParam->getHashArray());
        $objErr->arrErr = $objFormParam->checkError();

        $objErr->doFunc(array('開始日', '終了日', 'search_startyear', 'search_startmonth', 'search_startday', 'search_endyear', 'search_endmonth', 'search_endday'), array('CHECK_SET_TERM'));

        return $objErr->arrErr;
    }

    // カテゴリIDをキー、カテゴリ名を値にする配列を返す。
    public function lfGetIDName($arrCatKey, $arrCatVal)
    {
        $max = count($arrCatKey);
        for ($cnt = 0; $cnt < $max; $cnt++) {
            $key = isset($arrCatKey[$cnt]) ? $arrCatKey[$cnt] : '';
            $val = isset($arrCatVal[$cnt]) ? $arrCatVal[$cnt] : '';
            $arrRet[$key] = $val;
        }

        return $arrRet;
    }


    /**
     * クエリを構築する.
     *
     * 検索条件のキーに応じた WHERE 句と, クエリパラメーターを構築する.
     * クエリパラメーターは, SC_FormParam の入力値から取得する.
     *
     * 構築内容は, 引数の $where 及び $arrValues にそれぞれ追加される.
     *
     * @param  string       $key          検索条件のキー
     * @param  string       $where        構築する WHERE 句
     * @param  array        $arrValues    構築するクエリパラメーター
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @param  SC_FormParam $objDb        SC_Helper_DB_Ex インスタンス
     * @return void
     */
    public function buildQuery($key, &$where, &$arrValues, &$objFormParam, &$objDb)
    {
        $dbFactory = SC_DB_DBFactory_Ex::getInstance();
        switch ($key) {
            // 商品ID
            case 'start_product_id':
                $where .= ' AND product_id >= ?';
                $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
                break;
            case 'end_product_id':
                $where .= ' AND product_id <= ?';
                $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
                break;

            default:
                break;
        }
    }

    /**
     * 検索結果の行数を取得する.
     *
     * @param  string  $where     検索条件の WHERE 句
     * @param  array   $arrValues 検索条件のパラメーター
     * @return integer 検索結果の行数
     */
    public function getNumberOfLines($where, $arrValues)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        return $objQuery->count('dtb_products', $where, $arrValues);
    }

    /**
     * 商品を検索する.
     *
     * @param  string     $where      検索条件の WHERE 句
     * @param  array      $arrValues  検索条件のパラメーター
     * @param  integer    $limit      表示件数
     * @param  integer    $offset     開始件数
     * @param  string     $order      検索結果の並び順
     * @param  SC_Product $objProduct SC_Product インスタンス
     * @return array      商品の検索結果
     */
    public function findProducts($where, $arrValues, $limit, $offset, $order)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 読み込む列とテーブルの指定
        $col = 'product_id, name';
        //$from = $objProduct->alldtlSQL();
		$from = 'dtb_products';

        $objQuery->setLimitOffset($limit, $offset);
        $objQuery->setOrder($order);

        return $objQuery->select($col, $from, $where, $arrValues);
    }

    public function sfGetAffiliateName()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // カテゴリ名リストを取得
        $col = 'affiliate_id, name,affiliateid';
        $where = 'del_flg = 0';
        $objQuery->setOption('ORDER BY affiliate_id');
        $arrRet = $objQuery->select($col, 'dtb_affiliate', $where);
		$arrRes=array();
		for($i=0;$i<count($arrRet);$i++){
			$arrRes[$arrRet[$i]['affiliate_id']]=$arrRet[$i]['name'];
		}
        return $arrRes;
    }

    public function sfGetAffiliateID()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // カテゴリ名リストを取得
        $col = 'affiliate_id, name,affiliateid';
        $where = 'del_flg = 0';
        $objQuery->setOption('ORDER BY affiliate_id');
        $arrRet = $objQuery->select($col, 'dtb_affiliate', $where);
		$arrRes=array();
		for($i=0;$i<count($arrRet);$i++){
			$arrRes[$arrRet[$i]['affiliate_id']]=$arrRet[$i]['affiliateid'];
		}
        return $arrRes;
    }

}
