<?php

	$today=strtotime(postget("today",date("Y/m/d")));	
	$filename="block".date("Ymd",$today).".csv";
	$s_id = getsession('s_logid',0);
	$s_authority = getsession('s_authority',0);
	$s_acategory = getsession('s_acategory',999);
	$s_section = getsession('s_section',999);
	if($s_section!=0){
		$memberrow=db_get_record('section',$s_section);
	}

	$a_blood_id=db_get_array(1,'blood','id',0);
	$a_blood_name=db_get_array(1,'blood','name',0);
	$a_blood_color1=db_get_array(1,'blood','color1',0);
	$a_blood_color2=db_get_array(1,'blood','color2',0);
	$a_blood_textcolor=db_get_array(1,'blood','textcolor',0);
	
	$a_unit_id=db_get_array(1,'unit','id',0);
	$a_unit_name=db_get_array(1,'unit','name',0);
	$a_unit_sname=db_get_array(1,'unit','sname',0);
	
	if($s_authority==1){
		$section_condition="";
		$room_condition="";
		$center_condition="";
		$factory_condition="";
	} elseif($s_authority==2){
		$section_condition=sprintf(" and id=%d ORDER BY id",$s_section);
		$room_condition=sprintf(" and id=%d ORDER BY id",$memberrow['room']);
		$center_condition=sprintf(" and id=%d ORDER BY id",$memberrow['center']);
		$factory_condition=sprintf(" and id=%d ORDER BY id",$memberrow['factory']);
	} elseif($s_authority==3){
		$section_condition=sprintf(" and id=%d ORDER BY id",$s_section);
		$room_condition=sprintf(" and id=%d ORDER BY id",$memberrow['room']);
		$center_condition=sprintf(" and id=%d ORDER BY id",$memberrow['center']);
		$factory_condition=sprintf(" and id=%d ORDER BY id",$memberrow['factory']);
	} elseif($s_authority==4){
		$section_condition=sprintf(" and block=%d ORDER BY id",$memberrow['block']);
		$room_condition=sprintf(" and block=%d ORDER BY id",$memberrow['block']);
		$center_condition=sprintf(" and block=%d ORDER BY id",$memberrow['block']);
		$factory_condition=sprintf(" and block=%d ORDER BY id",$memberrow['block']);
	} elseif($s_authority==5){
		$section_condition=sprintf(" and factory=%d ORDER BY id",$memberrow['factory']);
		$room_condition=sprintf(" and factory=%d ORDER BY id",$memberrow['factory']);
		$center_condition=sprintf(" and factory=%d ORDER BY id",$memberrow['factory']);
		$factory_condition=sprintf(" and id=%d ORDER BY id",$memberrow['factory']);
	} elseif($s_authority==6){
		$section_condition=sprintf(" and center=%d ORDER BY id",$memberrow['center']);
		$room_condition=sprintf(" and center=%d ORDER BY id",$memberrow['center']);
		$center_condition=sprintf(" and id=%d ORDER BY id",$memberrow['center']);
		$factory_condition=sprintf(" and id=%d ORDER BY id",$memberrow['factory']);
	} elseif($s_authority==7){
		$section_condition=sprintf(" and room=%d ORDER BY id",$memberrow['room']);
		$room_condition=sprintf(" and id=%d ORDER BY id",$memberrow['room']);
		$center_condition=sprintf(" and id=%d ORDER BY id",$memberrow['center']);
		$factory_condition=sprintf(" and id=%d ORDER BY id",$memberrow['factory']);
	} else {
		$condition=sprintf(" and deleteflag=999");
	}
	
	$a_section_id=db_get_array_condition(1,'section','id',$section_condition);
	$a_section_name=db_get_array_condition(1,'section','name',$section_condition);

	$a_room_id=db_get_array_condition(1,'room','id',$room_condition);
	$a_room_name=db_get_array_condition(1,'room','name',$room_condition);
	$a_room_factory=db_get_array_condition(1,'room','factory',$room_condition);

	$a_center_id=db_get_array_condition(1,'center','id',$center_condition);
	$a_center_name=db_get_array_condition(1,'center','name',$center_condition);

	$a_factory_id=db_get_array_condition(1,'factory','id',$factory_condition);
	$a_factory_name=db_get_array_condition(1,'factory','name',$factory_condition);

	$usum=array();
	$out="日付,センターID,ブロックID,名称,";
    for($i=0;$i<count($a_blood_id);$i++){ 
		if($i!=0){ $out.=","; }
		 for($j=0;$j<count($a_unit_id);$j++){ 
			if($j!=0){ $out.=","; }
			$out.=$a_blood_name[$i].$a_unit_name[$j];
		 }
		 $out.=",小計";
     } 
	$out.="\n";

	for($i=0;$i<count($a_blood_id);$i++){ 
		$tmp_donationblood[$i]= blooddonation_get(3,date('Y/m/d',$today),$memberrow,$a_blood_id[$i]);
	}
	$out.=date("Y/m/d",$today).",";
	$out.=$memberrow['block'].",";
	$out.=$memberrow['block'].",";
	$out.="ブロック依頼,";
	for($i=0;$i<count($a_blood_id);$i++){
		$bsum=0;
		if($i!=0){ $out.=","; }
		for($j=0;$j<count($a_unit_id);$j++){ 
			if($j!=0){ $out.=","; }
			$out.=$tmp_donationblood[$i]['blkreq'.$a_unit_id[$j]];
			$bsum+=$tmp_donationblood[$i]['blkreq'.$a_unit_id[$j]];
		}
		$out.=",".$bsum;
	} 
	$out.="\n";
	$out.=date("Y/m/d",$today).",";
	$out.=$memberrow['block'].",";
	$out.=$memberrow['block'].",";
	$out.="センター依頼,";
	for($i=0;$i<count($a_blood_id);$i++){
		$bsum=0;
		if($i!=0){ $out.=","; }
		for($j=0;$j<count($a_unit_id);$j++){ 
			if($j!=0){ $out.=","; }
			$out.=$tmp_donationblood[$i]['req'.$a_unit_id[$j]];
			$bsum+=$tmp_donationblood[$i]['req'.$a_unit_id[$j]];
		}
		$out.=",".$bsum;
	} 
	$out.="\n";

	for($k=0;$k<count($a_center_id);$k++){ 
		$tmprow['center']=$a_center_id[$k];
		for($i=0;$i<count($a_blood_id);$i++){ 
			$tmp_donationblood[$i]= blooddonation_get(1,date('Y/m/d',$today),$tmprow,$a_blood_id[$i]);
		} 
		$out.=date("Y/m/d",$today).",";
		$out.=$a_center_id[$k].",";
		$out.=$memberrow['block'].",";
		$out.=$a_center_name[$k].",";
		for($i=0;$i<count($a_blood_id);$i++){
			$bsum=0;
			if($i!=0){ $out.=","; }
			for($j=0;$j<count($a_unit_id);$j++){ 
				if($j!=0){ $out.=","; }
				$out.=$tmp_donationblood[$i]['don'.$a_unit_id[$j]];
				$bsum+=$tmp_donationblood[$i]['don'.$a_unit_id[$j]];
				$usum[$i][$j]+=$tmp_donationblood[$i]['don'.$a_unit_id[$j]];
			}
			$out.=",".$bsum;
		} 
		$out.="\n";
	}
	
	$out.=",,,合計,";
	for($i=0;$i<count($a_blood_id);$i++){
		$bsum=0;
		if($i!=0){ $out.=","; }
		for($j=0;$j<count($a_unit_id);$j++){ 
			if($j!=0){ $out.=","; }
			$out.=$usum[$i][$j];
			$bsum+=$usum[$i][$j];
		}
		$out.=",".$bsum;
	}
	$out.="\n";

    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=$filename");
    echo mb_convert_encoding($out,"SJIS", "UTF-8");


function getpost($obj,$default){
	$result = isset($_GET[$obj]) ? $_GET[$obj] : $default ;
	if($result==$default){
		$result = isset($_POST[$obj]) ? $_POST[$obj] : $default ;
	}
    $rpword = array("\"","'");
    $result = trim(str_replace($rpword,"",$result));
	return $result;
}
?>
