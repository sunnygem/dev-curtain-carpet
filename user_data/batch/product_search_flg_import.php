<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    //$query = 'select product_id from dtb_products limit 1';
    $res = $objQuery->select( 'product_id', 'dtb_products', array(), array( 'limit' => 1 ) );

    // CSVを読み込む
    $target_files = 'product_search_flg2.csv';
    if ( file_exists( $target_files ) )
    {
        $files = new SplFileObject( $target_files );
        $files->setFlags( SplFileObject::READ_CSV );
        foreach( $files as $key => $val )
        {
            if ( $key === 0 ) continue; // header行
            if ( is_null( $val[0] ) === false )
            {
                $product_id                         = $val[0];                      // 商品ID
                $product_code                       = $val[1];                      // 商品コード
                $color_monotone                     = $val[2];      // 検索_カラー_モノトーン系            
                $color_beige                        = $val[3];      // 検索_カラー_ベージュ系              
                $color_brown                        = $val[4];      // 検索_カラー_ブラウン系              
                $color_yellow                       = $val[5];      // 検索_カラー_イエロー系              
                $color_green                        = $val[6];      // 検索_カラー_グリーン系              
                $color_blue                         = $val[7];      // 検索_カラー_ブルー系                
                $color_red                          = $val[8];      // 検索_カラー_レッド系                
                $color_gray                         = $val[9];      // 検索_カラー_グレイ系                
                $color_ivory                        = $val[10];     // 検索_カラー_アイボリー系            
                $function_feature17                 = $val[11];     // 検索_機能_遮熱                      
                $function_feature28                 = $val[12];     // 検索_機能_ウェーブロン              
                $function_shading                   = $val[13];     // 検索_機能_遮光                      
                $function_fire_retardant            = $val[14];     // 検索_機能_防炎                      
                $function_soundproofing             = $val[15];     // 検索_機能_防音                      
                $function_uv_cut                    = $val[16];     // 検索_機能_UVカット                  
                $function_washable                  = $val[17];     // 検索_機能_洗える                    
                $function_energy_saving             = $val[18];     // 検索_機能_省エネ                    
                $function_shape_memory_processing   = $val[19];     // 検索_機能_形状記憶加工              
                $function_pollen_catch              = $val[20];     // 検索_機能_花粉キャッチ              
                $function_privacy_measures          = $val[21];     // 検索_機能_プライバシー対策          
                $function_feature1                  = $val[22];     // 機能と特徴_裏地付き                 
                $function_feature2                  = $val[23];     // 機能と特徴_2倍ヒダ                  
                $function_feature3                  = $val[24];     // 機能と特徴_柄出し縫製               
                $function_feature4                  = $val[25];     // 機能と特徴_形状記憶加工             
                $function_feature5                  = $val[26];     // 機能と特徴_お洗濯OK                 
                $function_feature6                  = $val[27];     // 機能と特徴_防炎                     
                $function_feature7                  = $val[28];     // 機能と特徴_遮光1級                  
                $function_feature8                  = $val[29];     // 機能と特徴_遮光2級                  
                $function_feature9                  = $val[30];     // 機能と特徴_遮光3級                  
                $function_feature10                 = $val[31];     // 機能と特徴_UVカット                 
                $function_feature11                 = $val[32];     // 機能と特徴_ミラーレース             
                $function_feature12                 = $val[33];     // 機能と特徴_遮熱・断熱             
                $function_feature13                 = $val[34];     // 機能と特徴_ウェーブロン+(プラス)  
                $function_feature14                 = $val[35];     // 機能と特徴_採光                   
                $function_feature15                 = $val[36];     // 機能と特徴_遮像                   
                $function_feature16                 = $val[37];     // 機能と特徴_防音
                $function_feature18                 = $val[38];     // 機能と特徴_シームレス             
                $function_feature19                 = $val[39];     // 機能と特徴_抗菌                   
                $function_feature20                 = $val[40];     // 機能と特徴_消臭                   
                $function_feature21                 = $val[41];     // 機能と特徴_防臭                   
                $function_feature22                 = $val[42];     // 機能と特徴_防汚                   
                $function_feature23                 = $val[43];     // 機能と特徴_透けにくい             
                $function_feature24                 = $val[44];     // 機能と特徴_花粉キャッチ           
                $function_feature25                 = $val[45];     // 機能と特徴_黒ずみ防止             
                $function_feature26                 = $val[46];     // 機能と特徴_防虫                    
                $function_feature27                 = $val[47];     // 機能と特徴_アレルゲン抑制          
                $categories                         = $val[48];     // カテゴリー                         
                $main_list_comment                  = $val[49];     // 画像1_コメント                      
                $sub_comment1                       = $val[50];     // 画像2_コメント                      
                $sub_comment2                       = $val[51];     // 画像3_コメント                      
                $sub_comment3                       = $val[52];     // 画像4_コメント                      
                $sub_comment4                       = $val[53];     // 画像5_コメント                      
                $sub_comment5                       = $val[54];     // 画像6_コメント                      
                $sub_comment6                       = $val[55];     // 画像7_コメント                      
                $sub_comment7                       = $val[56];     // 画像8_コメント                     
                $sub_comment8                       = $val[57];     // 画像9_コメント
                $sample_flg                         = $val[58];     // 生地サンプル

                $main_comment = $main_list_comment;

                $columns = compact( 'color_monotone' ,'color_beige' ,'color_brown' ,'color_yellow' ,'color_green' ,'color_blue' ,'color_red' ,'color_gray' ,'color_ivory' ,'function_shading' ,'function_fire_retardant' ,'function_soundproofing' ,'function_uv_cut' ,'function_washable' ,'function_energy_saving' ,'function_shape_memory_processing' ,'function_pollen_catch' ,'function_privacy_measures' ,'function_feature1' ,'function_feature2' ,'function_feature3' ,'function_feature4' ,'function_feature5' ,'function_feature6' ,'function_feature7' ,'function_feature8' ,'function_feature9' ,'function_feature10' ,'function_feature11' ,'function_feature12' ,'function_feature13' ,'function_feature14' ,'function_feature15' ,'function_feature16', 'function_feature17' ,'function_feature18' ,'function_feature19' ,'function_feature20' ,'function_feature21' ,'function_feature22' ,'function_feature23' ,'function_feature24' ,'function_feature25' ,'function_feature26' ,'function_feature27', 'function_feature28' ,'main_list_comment', 'main_comment' ,'sub_comment1' ,'sub_comment2' ,'sub_comment3' ,'sub_comment4' ,'sub_comment5' ,'sub_comment6' ,'sub_comment7' ,'sub_comment8' ,'sample_flg' ) ;
                foreach( $columns as $key2 => $val2 )
                {
                    if ( strlen( $val2 ) === 0 ) unset( $columns[$key2] );
                }
                // product_codeから更新をかける
                $res = $objQuery->select( 'product_id', 'dtb_products_class', 'product_code = ? AND del_flg = ?', array( $product_code, 0 ) );
                if ( isset( $res[0]['product_id'] ) )
                {
                    $product_id = $res[0]['product_id'];
                    // カテゴリーが設定されている場合
                    if ( strlen( trim( $categories ) ) > 0 )
                    {
                        $categoryArr = explode( ',', $categories );
                        foreach( $categoryArr as $key2 => $val2 )
                        {
                            $val2 = trim( $val2 );

                            if ( strlen( $val2 ) === 0 ) continue;
                            if ( preg_match( '/^[\d]+$/', $val2 ) === 1 )
                            {
                                $category_id = $val2;
                            }
                            else
                            {
                                // category_nameからidを取得
                                $val2 = ( preg_match( '/0000/', $val2 ) === 1 ) ? preg_replace( '/0000/', '0,000', $val2 ) : $val2;
                                //$val2 = ( preg_match( '/～/', $val2 ) === 1 ) ? preg_replace( '/～/', '', $val2 ) : $val2;
                                //$res = $objQuery->select( 'category_id', 'dtb_category', 'category_name LIKE ? and del_flg = 0', array( trim( '%'.$val2.'%' ) ) );
                                $res = $objQuery->select( 'category_id', 'dtb_category', 'category_name = ? and del_flg = 0', array( trim( $val2 ) ) );
                                if ( count( $res ) > 1 )
                                {
                                    // TODO 1件以上の場合、どちらのカテゴリーに属するかを判断させる必要がある
                                    var_dump('一件以上');
                                    //var_dump($categories);
                                    var_dump($val2);
                                    var_dump($res);
                                }
                                else if ( count( $res ) === 0 )
                                {
                                    if ( preg_match( '/\_/', $val2 ) === 0 )
                                    {
                                        var_dump('0件');
                                        var_dump($categories);
                                        var_dump($val2);
                                        var_dump($res);
                                    }
                                    else
                                    {
                                        $tmp = explode( '_', $val2 );
                                        $res2 = $objQuery->select( 'category_id', 'dtb_category', 'category_name = ? and del_flg = 0', array( trim( $tmp[0] ) ) );
                                        $parent_category_id = $res2[0]['category_id'];
                                        $res3 = $objQuery->select( 'category_id', 'dtb_category', 'category_name = ? and parent_category_id = ? and del_flg = 0', array( trim( $tmp[1] ), $parent_category_id ) );
                                        $category_id = $res3[0]['category_id'];
                                    }
                                }
                                // 1件の場合
                                else
                                {
                                    $category_id = $res[0]['category_id'];
                                }
                            }
                            // dtb_product_categoriesの登録
                            $table = 'dtb_product_categories';
                            $res = $objQuery->count( $table, 'product_id=? and category_id=?', array( $product_id,$category_id) );
                            if ( (int)$res === 0 )
                            {
                                $rank = $objQuery->max( 'rank', $table ) + 1;
                                $sqlval['product_id']  = $product_id;
                                $sqlval['category_id'] = $category_id;
                                $sqlval['rank']        = $rank;
                                $objQuery->insert( $table, $sqlval );
                            }
                        }
                    }
                    // 商品情報の更新
                    $objQuery->update( 'dtb_products', $columns, 'product_id=?', array( $product_id ) );
                    //$sqlval['product_id']  = $product_id;
                    //$sqlval['category_id'] = $category_id;
                    //$where = 'product_id=?';
                    //$objQuery->insert($table, $sqlval);
                    //$objQuery->update( 'dtb_product_categories', $sqlval, $where, array( $id ) );
                    // $linemax = $objQuery->count($table, $where, array($category_id));
                    // max($col, $table, $where = '', $arrWhereVal = array())
                }
                else
                {
//                    var_dump('---------------------------------------------');
//                    var_dump('product_idがありません。product_code:'.$product_code);
//                    var_dump('---------------------------------------------');
                }
            }
        }
    }
    else
    {
        throw new Exception( 'file not exists' );
    }
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}
