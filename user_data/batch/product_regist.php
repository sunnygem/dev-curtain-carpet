<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    //$query = 'select product_id from dtb_products limit 1';
    //$res = $objQuery->select( 'product_id', 'dtb_products', array(), array( 'limit' => 1 ) );

    // CSVを読み込む
    $target_files = 'product_regist.csv';
    if ( file_exists( $target_files ) )
    {
        $files = new SplFileObject( $target_files );
        $files->setFlags( SplFileObject::READ_CSV );
        foreach( $files as $key => $val )
        {
            if ( $key === 0 ) continue; // header行
            if ( is_null( $val[0] ) === false )
            {
                $product_code      = mb_convert_kana( $val[0], 'KVas' ); // 商品コード
                $name              = $val[1]; // 商品名
                $main_list_comment = $val[2]; // メインコメント
                $main_comment      = $val[3]; // メインコメント
                $comment3          = $val[4]; // 検索ワード
                $category_id       = $val[5]; // カテゴリーID

                if ( strlen( trim( $name ) ) === 0  )
                {
                    $name = $product_code;
                }

                $columns = compact( 'name', 'main_list_comment', 'main_comment', 'comment3' ) ;

                // product_codeから更新をかける
                $res = $objQuery->select( 'product_id', 'dtb_products_class', 'product_code = ? AND del_flg = ?', array( $product_code, 0 ) );
                if ( isset( $res[0]['product_id'] ) )
                {
                    $product_id = $res[0]['product_id'];

                    // 商品情報の更新
                    $columns['status'] = 1;
                    $objQuery->update( 'dtb_products', $columns, 'product_id=?', array( $product_id ) );
                }
                else
                {
                    $res = $objQuery->select( 'product_id', 'dtb_products', 'name = ? AND del_flg = ?', array( $product_code, 0 ) );
                    if ( isset( $res[0]['product_id'] ) )
                    {
                        $product_id = $res[0]['product_id'];
                    }
                    else
                    {
                        $product_id = $objQuery->nextVal( 'dtb_products_product_id' );
                        $columns['product_id'] = $product_id;
                        // 新規登録
                        $objQuery->insert('dtb_products', $columns );
                    }
                    $sqlval = array(
                        'product_class_id' => $objQuery->nextVal( 'dtb_products_class_product_class_id' )
                        ,'product_id'      => $product_id
                        ,'product_code'    => $product_code
                    );
                    $objQuery->insert( 'dtb_products_class', $sqlval );


                }
                // dtb_product_categories
                $tmp = explode( ',', $category_id );
                foreach( $tmp as $key2 => $val2 )
                {
                    if ( trim( $val2 ) === 0 ) continue;
                    $res = $objQuery->count( 'dtb_product_categories', 'product_id=? and category_id=?', array( $product_id,$val2 ) );

                    if ( (int)trim($val2) === 1796 )
                    {
                        $objQuery->delete( 'dtb_product_categories', 'product_id=? AND category_id=?', array( $product_id, 1795 ) );
                    }
                    if ( (int)$res === 0 )
                    {
                        $rank = $objQuery->max( 'rank', 'dtb_product_categories' ) + 1;
                        $sqlval = array(); 
                        $sqlval['product_id']  = $product_id;
                        $sqlval['category_id'] = $val2;
                        $sqlval['rank']        = $rank;
                        $objQuery->insert( 'dtb_product_categories', $sqlval );
                    }

                }
            }
        }
    }
    else
    {
        throw new Exception( 'file not exists' );
    }
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}
