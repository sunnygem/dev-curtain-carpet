<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

$save_path = '/home/apache/htdocs/dev-curtain-carpet/shop/upload/save_image/';

try
{
    $res = $objQuery->select( 'b.product_code', 'dtb_products a LEFT JOIN dtb_products_class b ON a.product_id = b.product_id', '', array() );

    // product_idを取得
    $res = $objQuery->select( 'product_id', 'dtb_products', 'del_flg=?', array( 0 ) );
    foreach( $res as $key => $val )
    {
        $product_id = $val['product_id'];
        $objQuery->setLimit( 1 );
        $res = $objQuery->select( 'product_code', 'dtb_products_class', 'product_id=?', array( $product_id ) );
        $product_code = $res[0]['product_code'];
        // 画像フォルダがある場合
        $target_dir = $save_path . $product_code;
        if ( file_exists( $target_dir ) )
        {
            $res = glob( $target_dir . '/*.jpg' );
            foreach( $res as $key2 => $val2 )
            {
                $res[$key2] = preg_replace( '/' . preg_quote( $save_path, '/' ) . '/', '', $val2 );

            }
            $data = [];
            if ( isset( $res[0] ) )
            {
                $data['main_list_image']  = $res[0];
                $data['main_image']       = $res[0];
                $data['main_large_image'] = $res[0];
                $data['sub_image1']       = $res[0];
                $data['sub_large_image1'] = $res[0];
            }
            for( $i = 1; $i < 8; $i++ )
            {
                $j = $i + 1;
                if ( isset( $res[$i] ) )
                {
                    $data['sub_image'.$j]       = $res[$i];
                    $data['sub_large_image'.$j] = $res[$i];
                }
                else
                {
                    $data['sub_image'.$j]       = '';
                    $data['sub_large_image'.$j] = '';
                }
            }

            $objQuery->update( 'dtb_products', $data, 'product_id=?', array( $product_id ) );
        }
    }

    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}

