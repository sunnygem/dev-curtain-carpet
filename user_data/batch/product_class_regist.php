<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    $target_files = 'product_class_matrix.csv';

    $files = new SplFileObject( $target_files );
    $files->setFlags( SplFileObject::READ_CSV );
    foreach( $files as $key => $val )
    {
        if ( $key === 0 || strlen( $val[0] ) === 0 ) continue;

        $product_code = $val[0];
        $class_name   = $val[1];
        $price01      = $val[2];

        $table = 'dtb_products_class';
        $res = $objQuery->select( 'product_id', 'dtb_products_class', 'product_code = ? AND del_flg = ?', array( $product_code, 0 ) );
        //$res = $objQuery->select( 'product_id', 'dtb_products_class', 'product_code = ? ', array( $product_code ) );
        if ( isset( $res[0]['product_id'] ) )
        {
            $product_id = $res[0]['product_id'];
            $classcategory_id1 = 127;
            // カテゴリーを取得
            $categories = $objQuery->select( 'category_id', 'dtb_product_categories', 'product_id=?', array( $product_id ) );
            if ( count( $categories ) > 0 )
            {
                foreach( $categories as $key2 => $val2 )
                {
                    if ( in_array( $val2['category_id'], array( 1793, 1828, 1829, 1830, 1831, 1832, 1833, 1834, 1835 ) ) )
                    {
                        $classcategory_id1 = 128;
                        break;
                    }
                }
            }

            $sqlval = array(
                'product_id'      => $product_id
                ,'classcategory_id1' => $classcategory_id1
                ,'classcategory_id2' => 0
                ,'product_type_id'   => 1
                ,'product_code'      => $product_code
                ,'stock_unlimited'   => 1
                ,'price01'           => $price01
                ,'price02'           => 0
            );

            $objQuery->delete( $table, 'product_id=?', array( $product_id ) );

            $res = $objQuery->select( '*', 'dtb_class', 'name=?', array( $class_name ) );
            $class_id = $res[0]['class_id'];

            $matrix = $objQuery->select( '*', 'matrix', 'title=?', array( $class_name ) );
            $matrix = $matrix[0];
            $title = $matrix['title'];

            if ( preg_match( '/^(レK|厚K)/', $title ) === 1 )
            {
                execute( $matrix, 0, $class_id, $objQuery, $sqlval );
            }
            else if ( preg_match( '/^(レE|E)/', $title ) === 1 )
            {
                execute( $matrix, 1, $class_id, $objQuery, $sqlval );
            }
            else
            {
                execute( $matrix, 2, $class_id, $objQuery, $sqlval );
            }


        }
        else
        {
            //var_dump('---------------------------------------------');
            //var_dump('product_idがありません。product_code:'.$product_code);
            //var_dump('---------------------------------------------');
        }

    }


    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}
function execute( $matrix, $flg, $class_id, $objQuery, $sqlval )
{
    if ( $flg === 0 )
    {
        foreach( $matrix as $key => $val )
        {
            if ( preg_match( '/length(\d)/', $key, $match ) === 1 )
            {
                if ( strlen( $val ) === 0 ) continue;
                $length_num = $match[1];
                foreach( $matrix as $key2 => $val2 )
                {
                    if ( preg_match( '/width(\d)/', $key2, $match2 ) === 1 )
                    {
                        if ( strlen( $val2 ) === 0 ) continue;
                        $width_num = $match2[1];

                        if ( (int)$val2 === 100 )
                        {
                            $name = '巾'.$val2.'cm×丈'.$val.'cm (2枚入り)';
                        }
                        else if ( (int)$val2 === 150 )
                        {
                            $name = '巾'.$val2.'cm×丈'.$val.'cm (1枚入り)';
                        }

                        $res = $objQuery->select( 'classcategory_id', 'dtb_classcategory', 'name=? AND class_id=?', array( $name, $class_id ) );
                        $classcategory_id2 = $res[0]['classcategory_id'];
                        $price02 = $matrix['price'.$length_num.'_'.$width_num];

                        if ( strlen( $price02 ) > 0 )
                        {
                            $sqlval['product_class_id']  = $objQuery->nextVal( 'dtb_products_class_product_class_id' );
                            $sqlval['price02']           = $price02;
                            $sqlval['classcategory_id2'] = $classcategory_id2;
                            $objQuery->insert( 'dtb_products_class', $sqlval );
                        }
                    }
                }
            }
        }
    }
    else
    {
        $min = ( $flg === 1 ) ? 50 : 40;
        $length_min = 0;
        $length_max = 0;
        foreach( $matrix as $key => $val )
        {
            if ( preg_match( '/length(\d)/', $key, $match ) === 1 )
            {
                if ( strlen( $val ) === 0 ) continue;
                $length_num = $match[1];

                $length_min = ( $length_min === 0 ) ? $min : $length_max + 1;
                $length_max = $val;

                $width_min  = 0;
                $width_max  = 0;
                foreach( $matrix as $key2 => $val2 )
                {
                    if ( preg_match( '/width(\d)/', $key2, $match2 ) === 1 )
                    {
                        if ( strlen( $val2 ) === 0 ) continue;
                        $width_num = $match2[1];

                        $width_min  = ( $width_min  === 0 ) ? $min : $width_max  + 1;
                        $width_max  = $val2;

                        $name = 'order_' . $width_min .'_'.$width_max.'_'.$length_min.'_'.$length_max;
                        $res = $objQuery->select( 'classcategory_id', 'dtb_classcategory', 'name=? AND class_id=?', array( $name, $class_id ) );
                        $classcategory_id2 = $res[0]['classcategory_id'];
                        $price02 = $matrix['price'.$length_num.'_'.$width_num];
                        if ( strlen( $price02 ) > 0 )
                        {
                            $sqlval['product_class_id']  = $objQuery->nextVal( 'dtb_products_class_product_class_id' );
                            $sqlval['price02']           = $price02;
                            $sqlval['classcategory_id2'] = $classcategory_id2;
                            $objQuery->insert( 'dtb_products_class', $sqlval );
                        }
                    }
                }
            }
        }
    }

}
