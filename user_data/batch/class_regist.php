<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{
    //$query = 'select product_id from dtb_products limit 1';
    //$res = $objQuery->select( 'product_id', 'dtb_products', array(), array( 'limit' => 1 ) );

    // CSVを読み込む
    $target_files = 'class_regist.csv';
    if ( file_exists( $target_files ) )
    {
        $files = new SplFileObject( $target_files );
        $files->setFlags( SplFileObject::READ_CSV );
        foreach( $files as $key => $val )
        {
            if ( $key === 0 ) continue; // header行
            if ( is_null( $val[0] ) === false )
            {
                $table    = 'dtb_class';
                $name     = $val[0];
                $rank     = $objQuery->max( 'rank', $table ) + 1;
                $class_id = $objQuery->nextVal( 'dtb_class_class_id' );

                $sqlval = compact( 'name', 'rank', 'class_id' );
                
                $objQuery->insert( $table, $sqlval );
            }
        }
    }
    else
    {
        throw new Exception( 'file not exists' );
    }
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}

