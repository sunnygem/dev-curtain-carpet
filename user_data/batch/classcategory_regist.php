<?php

require '../require.php';

$objQuery =& SC_Query_Ex::getSingletonInstance();
$objQuery->begin();

try
{

    $class = $objQuery->select( 'class_id,name', 'dtb_class', 'class_id >= ?', array( 17 ) );

var_dump($class);
    foreach( $class as $key => $val )
    {
        $class_id = $val['class_id'];
        $matrix = $objQuery->select( '*', 'matrix', 'title=?', array( $val['name'] ) );
        $matrix = $matrix[0];

        $title = $matrix['title'];

        if ( preg_match( '/^(レK|厚K)/', $title ) === 1 )
        {
            var_dump('既製品');
            execute( $matrix, 0, $class_id, $objQuery );
        }
        else if ( preg_match( '/^(レE|E)/', $title ) === 1 )
        {
            var_dump('イージーオーダー');
            execute( $matrix, 1, $class_id, $objQuery );
        }
        else
        {
            var_dump('その他');
            execute( $matrix, 2, $class_id, $objQuery );
        }
    }


    //// CSVを読み込む
    //$target_files = 'class_regist.csv';
    //if ( file_exists( $target_files ) )
    //{
    //    $files = new SplFileObject( $target_files );
    //    $files->setFlags( SplFileObject::READ_CSV );
    //    foreach( $files as $key => $val )
    //    {
    //        if ( $key === 0 ) continue; // header行
    //        if ( is_null( $val[0] ) === false )
    //        {
    //            $table    = 'dtb_class';
    //            $name     = $val[0];
    //            $rank     = $objQuery->max( 'rank', $table ) + 1;
    //            $class_id = $objQuery->nextVal( 'dtb_class_class_id' );

    //            $sqlval = compact( 'name', 'rank', 'class_id' );
    //            
    //            $objQuery->insert( $table, $sqlval );
    //        }
    //    }
    //}
    //else
    //{
    //    throw new Exception( 'file not exists' );
    //}
    $objQuery->commit();
}
catch( Exception $e )
{
    var_dump( $e->getMessage() );
    $objQuery->rollback();
}
function execute( $matrix, $flg, $class_id, $objQuery )
{
    if ( $flg === 0 )
    {
        foreach( $matrix as $key => $val )
        {
            if ( preg_match( '/length(\d)/', $key, $match ) === 1 )
            {
                if ( strlen( $val ) === 0 ) continue;
                foreach( $matrix as $key2 => $val2 )
                {
                    if ( preg_match( '/width(\d)/', $key2, $match2 ) === 1 )
                    {
                        if ( strlen( $val2 ) === 0 ) continue;
                        $width_num = $match2[1];

                        if ( (int)$val2 === 100 )
                        {
                            $name = '巾'.$val2.'cm×丈'.$val.'cm (2枚入り)';
                        }
                        else if ( (int)$val2 === 150 )
                        {
                            $name = '巾'.$val2.'cm×丈'.$val.'cm (1枚入り)';
                        }
                        //var_dump($name);


                        $sqlval['classcategory_id'] = $objQuery->nextVal( 'dtb_classcategory_classcategory_id' );
                        $sqlval['name']             = $name;
                        $sqlval['class_id']         = $class_id;
                        $sqlval['rank']             = $objQuery->max( 'rank', 'dtb_classcategory', 'class_id=?', array( $class_id ) ) + 1;
                        $objQuery->insert( 'dtb_classcategory', $sqlval );

                    }
                }
            }
        }
    }
    else
    {
        $min = ( $flg === 1 ) ? 50 : 40;
        $length_min = 0;
        $length_max = 0;
        foreach( $matrix as $key => $val )
        {
            if ( preg_match( '/length(\d)/', $key, $match ) === 1 )
            {
                if ( strlen( $val ) === 0 ) continue;
                $length_num = $match[1];

                $length_min = ( $length_min === 0 ) ? $min : $length_max + 1;
                $length_max = $val;

                $width_min  = 0;
                $width_max  = 0;
                foreach( $matrix as $key2 => $val2 )
                {
                    if ( preg_match( '/width(\d)/', $key2, $match2 ) === 1 )
                    {
                        if ( strlen( $val2 ) === 0 ) continue;
                        $width_num = $match2[1];

                        $width_min  = ( $width_min  === 0 ) ? $min : $width_max  + 1;
                        $width_max  = $val2;

                        $name = 'order_' . $width_min .'_'.$width_max.'_'.$length_min.'_'.$length_max;

                        $sqlval['classcategory_id'] = $objQuery->nextVal( 'dtb_classcategory_classcategory_id' );
                        $sqlval['name']             = $name;
                        $sqlval['class_id']         = $class_id;
                        $sqlval['rank']             = $objQuery->max( 'rank', 'dtb_classcategory', 'class_id=?', array( $class_id ) ) + 1;
                        $objQuery->insert( 'dtb_classcategory', $sqlval );

                    }
                }
            }
        }
    }

}
