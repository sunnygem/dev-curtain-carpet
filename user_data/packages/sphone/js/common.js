// JavaScript Document

//クリックして展開する FAQページ
	$(function(){
		$(".acMenu dt").on("click", function() {
			$(this).next().slideToggle(100);
			$(this).toggleClass("acMenu_clicked");
		});
	});

//PCでTELリンク無効
    $(function setTelDisable(){
        var $tel = $('[href^="tel:"]');
        $tel.attr('tabIndex', -1);
        $tel.on('click', function(e){
            if($(window).width() > 768){
                e.preventDefault();
            }
        });
     });setTelDisable();