<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*  [名称] SC_FunctionFeature
 *  [概要] 機能と特徴
 */
class SC_FunctionFeature
{
    public $objQuery;
    public $_table = 'dtb_function_feature';

    public function __construct()
    {
        $this->objQuery =& SC_Query_Ex::getSingletonInstance();
    }
    public function getData()
    {
        $sql = 'SELECT * FROM ' . $this->_table . ' WHERE del_flg = 0';
        $result = $this->objQuery->getAll($sql, $arrValues);
        if ( count( $result ) > 0 )
        {
            $data = [];
            foreach( $result as $key => $val )
            {
                $data[$val['id']] = $val;
            }
            return $data;
        }
        return false;
    }
}
