<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * 配送方法設定 のページクラス.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Affiliate_IdInput extends LC_Page_Admin_Ex
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->tpl_mainpage = 'affiliate/affiliate_idinput.tpl';
        $this->tpl_subno = 'affiliateid';
        $this->tpl_mainno = 'affiliate';
        $this->tpl_maintitle = 'アフィリエイト管理';
        $this->tpl_subtitle = 'アフィリエイトID管理';
        $masterData = new SC_DB_MasterData_Ex();
        $this->mode = $this->getMode();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($this->mode, $objFormParam);
        $objFormParam->setParam($_POST);
        // 入力値の変換
        $objFormParam->convParam();
		if($this->affiliate_id==''){
			$this->arrErr = $this->lfCheckError($objFormParam);
		}

        switch ($this->mode) {
            case 'edit':
                if (count($this->arrErr) == 0) {
                    $objFormParam->setValue('affiliate_id', $this->lfRegistData($objFormParam->getHashArray(), $_SESSION['member_id']));
                    $this->tpl_onload = "window.alert('アフィリエイトID登録が完了しました。');";
                }
                break;
            case 'pre_edit':
                if (count($this->arrErr) > 0) {
                    trigger_error('', E_USER_ERROR);
                }
                $this->lfGetAffiliateData($objFormParam);
                break;
            default:
                break;
        }
        $this->arrForm = $objFormParam->getFormParamList();
    }

    /* パラメーター情報の初期化 */
    public function lfInitParam($mode, &$objFormParam)
    {
        $objFormParam = new SC_FormParam_Ex();

        switch ($mode) {
            case 'edit':
                $objFormParam->addParam('ID', 'affiliate_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
                $objFormParam->addParam('名前', 'name', STEXT_LEN, 'KVa', array('EXIST_CHECK', 'MAX_LENGTH_CHECK'));
                $objFormParam->addParam('アフィリエイトID', 'affiliateid', STEXT_LEN, 'KVa', array('EXIST_CHECK', 'MAX_LENGTH_CHECK'));
                $objFormParam->addParam('説明', 'comment', LLTEXT_LEN, 'KVa', array('MAX_LENGTH_CHECK'));
                $objFormParam->addParam('ステータス', 'status', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));

                break;

            case 'pre_edit':
                $objFormParam->addParam('ID', 'affiliate_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
                break;

            default:
                break;
        }
    }

    /**
     * 配送情報を登録する
     *
     * @return $affiliate_id
     */
    public function lfRegistData($arrRet, $member_id)
    {
        $objAffiliate = new SC_Helper_Affiliate_Ex();

        // 入力データを渡す。
        $sqlval['affiliate_id'] = $arrRet['affiliate_id'];
        $sqlval['name'] = $arrRet['name'];
        $sqlval['affiliateid'] = $arrRet['affiliateid'];
        $sqlval['comment'] = $arrRet['comment'];
        $sqlval['status'] = $arrRet['status'];
        $sqlval['creator_id'] = $member_id;
        $sqlval['update_id'] = $member_id;

        $affiliate_id = $objAffiliate->save($sqlval);

        return $affiliate_id;
    }

    /* 配送業者情報の取得 */
    public function lfGetAffiliateData(&$objFormParam)
    {
        $objAffiliate = new SC_Helper_Affiliate_Ex();

        $affiliate_id = $objFormParam->getValue('affiliate_id');
        // パラメーター情報の初期化
        $this->lfInitParam('edit', $objFormParam);

        $arrAffiliate = $objAffiliate->get($affiliate_id);
        $objFormParam->setParam($arrAffiliate);
    }

    /* 入力内容のチェック */
    public function lfCheckError(&$objFormParam)
    {
        // 入力データを渡す。
        $arrRet =  $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrRet);
        $objErr->arrErr = $objFormParam->checkError();

        if (!isset($objErr->arrErr['affiliateid'])) {
            // 既存チェック
            $objAffiliate = new SC_Helper_Affiliate_Ex();
            if ($objAffiliate->checkExist($arrRet)) {
                $objErr->arrErr['affiliateid'] = '※ 同じ名称のアフィリエイトIDは登録できません。<br>';
            }
        }

        return $objErr->arrErr;
    }
}
