<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * 商品管理 のページクラス.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Affiliate_Log extends LC_Page_Admin_Ex
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->tpl_mainpage = 'affiliate/log.tpl';
        $this->tpl_mainno = 'affiliate';
        $this->tpl_subno = 'loglist';
        $this->tpl_pager = 'pager.tpl';
        $this->tpl_maintitle = 'アフィリエイト管理';
        $this->tpl_subtitle = 'ログリスト';

        $masterData = new SC_DB_MasterData_Ex();
        $this->arrPageMax = $masterData->getMasterData('mtb_page_max');

        $objDate = new SC_Date_Ex();
        // 登録・更新検索開始年
        $objDate->setStartYear(RELEASE_YEAR);
        $objDate->setEndYear(DATE('Y'));
        $this->arrStartYear = $objDate->getYear();
        $this->arrStartMonth = $objDate->getMonth();
        $this->arrStartDay = $objDate->getDay();
        // 登録・更新検索終了年
        $objDate->setStartYear(RELEASE_YEAR);
        $objDate->setEndYear(DATE('Y'));
        $this->arrEndYear = $objDate->getYear();
        $this->arrEndMonth = $objDate->getMonth();
        $this->arrEndDay = $objDate->getDay();
        // メーカーリスト作成
        $this->arrMakerList = $this->sfGetMakerList();
        // アフィリエイトID作成
        $this->arrAffiliate = $this->sfGetAffiliateList();

    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $objDb = new SC_Helper_DB_Ex();
        $objFormParam = new SC_FormParam_Ex();
        $objAffiliateLog = new SC_Helper_AffiliateLog_Ex();
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // パラメーター情報の初期化
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $this->arrHidden = $objFormParam->getSearchArray();
        $this->arrForm = $objFormParam->getFormParamList();

        switch ($this->getMode()) {
           // 検索パラメーター生成後に処理実行するため breakしない
            case 'csv':

            case 'search':
                $objFormParam->convParam();
                $objFormParam->trimParam();
                $this->arrErr = $this->lfCheckError($objFormParam);
                $arrParam = $objFormParam->getHashArray();

                if (count($this->arrErr) == 0) {
                    $where = 'del_flg = 0';
                    $arrWhereVal = array();
                    foreach ($arrParam as $key => $val) {
                        if ($val == '') {
                            continue;
                        }
                        $this->buildQuery($key, $where, $arrWhereVal, $objFormParam, $objDb);
                    }

                    $order = 'update_date DESC';

                    /* -----------------------------------------------
                     * 処理を実行
                     * ----------------------------------------------- */
                    switch ($this->getMode()) {
                        // CSVを送信する。
                        case 'csv':
                            $objCSV = new SC_Helper_CSV_Ex();
                            // CSVを送信する。正常終了の場合、終了。
                            $objCSV->sfDownloadCsv(6, $where, $arrWhereVal, $order, true);
                            SC_Response_Ex::actionExit();

                        // 検索実行
                        default:
                            // 行数の取得
                            $this->tpl_linemax = $this->getNumberOfLines($where, $arrWhereVal);
                            // ページ送りの処理
                            $page_max = SC_Utils_Ex::sfGetSearchPageMax($objFormParam->getValue('search_page_max'));
                            // ページ送りの取得
                            $objNavi = new SC_PageNavi_Ex($this->arrHidden['search_pageno'],
                                                          $this->tpl_linemax, $page_max,
                                                          'eccube.moveNaviPage', NAVI_PMAX);
                            $this->arrPagenavi = $objNavi->arrPagenavi;

                            // 検索結果の取得
							$arrRet = array();
                            $arrRet = $this->findAffiliateLog($where, $arrWhereVal, $page_max, $objNavi->start_row,$order, $objAffiliateLog);
							for($i=0;$i<count($arrRet);$i++){
								$this->arrAffiliateLogList[$i]=$arrRet[$i];
								$this->arrAffiliateLogList[$i]['affiliate_id']=$this->arrAffiliate[$arrRet[$i]['affiliate_id']];
								$this->arrAffiliateLogList[$i]['totalprice']=number_format($arrRet[$i]['totalprice']);
								$this->arrAffiliateLogList[$i]['maker_id']=$this->arrMakerList[$arrRet[$i]['maker_id']];
							}
                    }
                }
                break;
        }
    }

    /**
     * パラメーター情報の初期化を行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function lfInitParam(&$objFormParam)
    {
        // POSTされる値
        $objFormParam->addParam('ページ送り番号', 'search_pageno', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('表示件数', 'search_page_max', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));

        // 検索条件
        $objFormParam->addParam('アフィリエイトID', 'search_affiliate_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('メーカー', 'search_maker_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('開始注文番号', 'order_id_start', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('終了注文番号', 'order_id_end', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('種別', 'search_conversion', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        // 登録・更新日
        $objFormParam->addParam('開始年', 'search_startyear', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('開始月', 'search_startmonth', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('開始日', 'search_startday', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('終了年', 'search_endyear', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('終了月', 'search_endmonth', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('終了日', 'search_endday', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));

    }

    /**
     * 入力内容のチェックを行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function lfCheckError(&$objFormParam)
    {
        $objErr = new SC_CheckError_Ex($objFormParam->getHashArray());
        $objErr->arrErr = $objFormParam->checkError();

        $objErr->doFunc(array('開始日', '終了日', 'search_startyear', 'search_startmonth', 'search_startday', 'search_endyear', 'search_endmonth', 'search_endday'), array('CHECK_SET_TERM'));

        return $objErr->arrErr;
    }

    // カテゴリIDをキー、カテゴリ名を値にする配列を返す。
    public function lfGetIDName($arrCatKey, $arrCatVal)
    {
        $max = count($arrCatKey);
        for ($cnt = 0; $cnt < $max; $cnt++) {
            $key = isset($arrCatKey[$cnt]) ? $arrCatKey[$cnt] : '';
            $val = isset($arrCatVal[$cnt]) ? $arrCatVal[$cnt] : '';
            $arrRet[$key] = $val;
        }

        return $arrRet;
    }


    /**
     * クエリを構築する.
     *
     * 検索条件のキーに応じた WHERE 句と, クエリパラメーターを構築する.
     * クエリパラメーターは, SC_FormParam の入力値から取得する.
     *
     * 構築内容は, 引数の $where 及び $arrValues にそれぞれ追加される.
     *
     * @param  string       $key          検索条件のキー
     * @param  string       $where        構築する WHERE 句
     * @param  array        $arrValues    構築するクエリパラメーター
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @param  SC_FormParam $objDb        SC_Helper_DB_Ex インスタンス
     * @return void
     */
    public function buildQuery($key, &$where, &$arrValues, &$objFormParam, &$objDb)
    {
        $dbFactory = SC_DB_DBFactory_Ex::getInstance();
        switch ($key) {
            // アフィリエイトID
            case 'search_affiliate_id':
                $where .= ' AND affiliate_id = ?';
                $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
                break;

            // メーカー
            case 'search_maker_id':
                $where .= ' AND maker_id = ?';
                $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
                break;

            // 種別
            case 'search_conversion':
                $where .= ' AND conversion = ?';
                $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
                break;

            // 開始注文番号
            case 'order_id_start':
                $where .= ' AND order_id >= ?';
                $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
                break;

            // 終了注文番号
            case 'order_id_end':
                $where .= ' AND order_id <= ?';
                $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
                break;

           // 登録・更新日(開始)
            case 'search_startyear':
                $date = SC_Utils_Ex::sfGetTimestamp($objFormParam->getValue('search_startyear'),
                                                    $objFormParam->getValue('search_startmonth'),
                                                    $objFormParam->getValue('search_startday'));
                $where.= ' AND update_date >= ?';
                $arrValues[] = $date;
                break;
            // 登録・更新日(終了)
            case 'search_endyear':
                $date = SC_Utils_Ex::sfGetTimestamp($objFormParam->getValue('search_endyear'),
                                                    $objFormParam->getValue('search_endmonth'),
                                                    $objFormParam->getValue('search_endday'), true);
                $where.= ' AND update_date <= ?';
                $arrValues[] = $date;
                break;
            default:
                break;
        }
    }

    /**
     * 検索結果の行数を取得する.
     *
     * @param  string  $where     検索条件の WHERE 句
     * @param  array   $arrValues 検索条件のパラメーター
     * @return integer 検索結果の行数
     */
    public function getNumberOfLines($where, $arrValues)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        return $objQuery->count('dtb_affiliate_log', $where, $arrValues);
    }

    /**
     * 商品を検索する.
     *
     * @param  string     $where      検索条件の WHERE 句
     * @param  array      $arrValues  検索条件のパラメーター
     * @param  integer    $limit      表示件数
     * @param  integer    $offset     開始件数
     * @param  string     $order      検索結果の並び順
     * @param  SC_Product $objAffiliateLog SC_Product インスタンス
     * @return array      商品の検索結果
     */

    public function findAffiliatelog($where, $arrValues, $limit, $offset, $order, &$objAffiliateLog)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
		
        // 読み込む列とテーブルの指定
        $col = 'log_id,affiliate_id,affiliate_user_id,customer_id,conversion,order_id,product_id,product_class_id,maker_id,quantity,totalprice,create_date';
        $from = "dtb_affiliate_log";
		
//        $from = $objAffiliateLog->alldtlSQL();

        $objQuery->setLimitOffset($limit, $offset);
        $objQuery->setOrder($order);

        return $objQuery->select($col, $from, $where, $arrValues);
    }

    public function sfGetMakerList()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // カテゴリ名リストを取得
        $col = 'maker_id, name';
        $where = 'del_flg = 0';
        $objQuery->setOption('ORDER BY maker_id');
        $arrRet = $objQuery->select($col, 'dtb_maker', $where);
		$arrRes=array();
		for($i=0;$i<count($arrRet);$i++){
			$arrRes[$arrRet[$i]['maker_id']]=$arrRet[$i]['name'];
		}
        return $arrRes;
    }

    public function sfGetAffiliateList()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // カテゴリ名リストを取得
        $col = 'affiliate_id, name';
        $where = 'del_flg = 0';
        $objQuery->setOption('ORDER BY affiliate_id');
        $arrRet = $objQuery->select($col, 'dtb_affiliate', $where);
		$arrRes=array();
		for($i=0;$i<count($arrRet);$i++){
			$arrRes[$arrRet[$i]['affiliate_id']]=$arrRet[$i]['name'];
		}
        return $arrRes;
    }

    public function GetAffiliateLogList()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // カテゴリ名リストを取得
        $col = '*';
        $where = 'del_flg = 0';
        $objQuery->setOption('ORDER BY log_id');
        $arrRet = $objQuery->select($col, 'dtb_affiliate_log', $where);
		$arrRes=array();
		for($i=0;$i<count($arrRet);$i++){
			$arrRes[$arrRet[$i]['affiliate_id']]=$arrRet[$i]['name'];
		}
        return $arrRes;
    }
	
}

