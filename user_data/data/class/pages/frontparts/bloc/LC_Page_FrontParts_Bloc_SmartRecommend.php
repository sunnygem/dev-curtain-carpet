<?php
/*
 * SmartRecommend
 * Copyright (C) 2017 bitmop, Inc. All Rights Reserved.
 * http://www.bitmop.co.jp/
 * 
 * This library is NOT free software; you can NOT redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// {{{ requires
require_once CLASS_REALDIR . 'pages/frontparts/bloc/LC_Page_FrontParts_Bloc.php';

/**
 * スマートリコメンドプラグインのブロッククラス
 *
 * @package SmartRecommend
 * @author bitmop, Inc.
 * @version $Id: $
 */
class LC_Page_FrontParts_Bloc_SmartRecommend extends LC_Page_FrontParts_Bloc {

    public $SmartRecommend;
    public $arrProductsBySmartRecommend = array();
    public $plugin_code;

    /**
     * 初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->httpCacheControl('nocache');
        $this->plugin_code  = basename(dirname(__FILE__));
    }

    /**
     * プロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {

        $smartrecommend_available_flg = FALSE;

        // クッキー管理クラス
        $objCookie = new SC_Cookie_Ex();
        $cookie_key = $this->getSaveKey();

        // Cookieがあるかチェック
        $item = $objCookie->getCookie($cookie_key);
        $customer_profile = !empty($item) ? $item : null;
        $arrCustomerProfile = explode(",", $customer_profile);
        $user_birthday = $arrCustomerProfile[0];
        $user_sex      = $arrCustomerProfile[1];

        if ($_SESSION['customer']['customer_id']) {
            // ログインしている
            $user_birthday = $_SESSION['customer']['birth']; // Cookieの値よりもログインデータを優先で上書き
            $user_sex      = $_SESSION['customer']['sex'];   // Cookieの値よりもログインデータを優先で上書き
        }

        list($user_birthday_date, $user_birthday_time) = explode(' ', $user_birthday);
        list($Y, $m, $d) = explode('-', $user_birthday_date);
        if (checkdate($m, $d, $Y) === true) {
            // このユーザーの誕生日は有効な日付が設定されている

            $smartrecommend_available_flg = TRUE;

            //// プラグインの設定情報を取得する
            $plugin = SC_Plugin_Util_Ex::getPluginByPluginCode($this->plugin_code);
            $arrPluginData = unserialize($plugin['free_field1']);

            // どの区切りで「同世代」と呼ぶかのモード(1: 誕生日から±N年で区切る / 2: 何十代(30代とか40代のように)として、10歳ごとに区切る)
            $peers_mode = $arrPluginData['peers_mode'];

            switch($peers_mode) {
                case '1': // 誕生日から±N年で区切る
                    // 誕生日から前後何年を「同世代」と呼ぶかの定義値
                    $peers_years = $arrPluginData['peers_mode_1_year'];
                    $peers_date_from = date("Y/m/d 00:00:00", strtotime($user_birthday . ' -' . $peers_years . ' year'));
                    $peers_date_to   = date("Y/m/d 23:59:59", strtotime($user_birthday . ' +' . $peers_years . ' year'));
                    break;
                case '2': // 何十代(30代とか40代のように)として、10歳ごとに区切る
                    $now = date("Ymd");
                    $age = floor(($now - date("Ymd", strtotime($user_birthday))) / 10000);

                    if ($age < 10) {
                        // 10歳未満
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -10 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -0 year'));
                    } elseif ($age >= 10 && $age < 20) {
                        // 10代
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -20 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -10 year'));
                    } elseif ($age >= 20 && $age < 30) {
                        // 20代
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -30 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -20 year'));
                    } elseif ($age >= 30 && $age < 40) {
                        // 30代
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -40 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -30 year'));
                    } elseif ($age >= 40 && $age < 50) {
                        // 40代
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -50 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -40 year'));
                    } elseif ($age >= 50 && $age < 60) {
                        // 50代
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -60 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -50 year'));
                    } elseif ($age >= 60 && $age < 70) {
                        // 60代
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -70 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -60 year'));
                    } elseif ($age >= 70 && $age < 80) {
                        // 70代
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -80 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -70 year'));
                    } elseif ($age >= 80 && $age < 90) {
                        // 80代
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -90 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -80 year'));
                    } else {
                        // 90代以上
                        $peers_date_from = date("Y/m/d 00:00:00", strtotime($now . ' -100 year + 1 day'));
                        $peers_date_to   = date("Y/m/d 23:59:59", strtotime($now . ' -90 year'));
                    }
                    break;
                default:
            }

            $searchData = array(
                'birth_start' => $peers_date_from,
                'birth_end' => $peers_date_to,
            );

            if ($_SESSION['customer']['customer_id']) {
                if ($arrPluginData['classification_by_sex'] == 1) {
                    // (設定では)性別による分類をする
                    if ($user_sex == 1 || $user_sex == 2) {
                        $where  = 'customer_id != ? AND birth >= ? AND birth <= ? AND sex = ? AND del_flg = 0 AND status = 2';
                        $arrVal = array($_SESSION['customer']['customer_id'], $searchData['birth_start'], $searchData['birth_end'], $user_sex);
                    } else {
                        // でもこのユーザーは性別を登録してないので、仕方ないのでせめて年齢での絞り込みのみ
                        $where  = 'customer_id != ? AND birth >= ? AND birth <= ? AND del_flg = 0 AND status = 2';
                        $arrVal = array($_SESSION['customer']['customer_id'], $searchData['birth_start'], $searchData['birth_end']);
                    }
                } elseif ($arrPluginData['classification_by_sex'] == 2) {
                    // 性別による分類はしない
                    $where  = 'customer_id != ? AND birth >= ? AND birth <= ? AND del_flg = 0 AND status = 2';
                    $arrVal = array($_SESSION['customer']['customer_id'], $searchData['birth_start'], $searchData['birth_end']);
                }
            } else {
                if ($arrPluginData['classification_by_sex'] == 1) {
                    // (設定では)性別による分類をする
                    if ($user_sex == 1 || $user_sex == 2) {
                        $where  = 'birth >= ? AND birth <= ? AND sex = ? AND del_flg = 0 AND status = 2';
                        $arrVal = array($searchData['birth_start'], $searchData['birth_end'], $user_sex);
                    } else {
                        // でもこのユーザーは性別を登録してないので、仕方ないのでせめて年齢での絞り込みのみ
                        $where  = 'birth >= ? AND birth <= ? AND del_flg = 0 AND status = 2';
                        $arrVal = array($searchData['birth_start'], $searchData['birth_end']);
                    }
                } elseif ($arrPluginData['classification_by_sex'] == 2) {
                    // 性別による分類はしない
                    $where  = 'birth >= ? AND birth <= ? AND del_flg = 0 AND status = 2';
                    $arrVal = array($searchData['birth_start'], $searchData['birth_end']);
                }
            }

            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $col    = '*';
            $from   = 'dtb_customer';
            $arrCustomer = $objQuery->select($col, $from, $where, $arrVal);
            $arrPeersCustomers = array();
            foreach ($arrCustomer as $key => $value) {
                $arrPeersCustomers[$value['customer_id']] = $value['customer_id'];
            }

            if (count($arrPeersCustomers)) {
                // 同年代の会員が存在するので、この人たちの過去の受注リストを取得する
                $col    = 'dtb_order_detail.*';
                $from   = 'dtb_order LEFT JOIN dtb_order_detail ON dtb_order.order_id = dtb_order_detail.order_id';
                $where  = 'dtb_order.customer_id  IN (' . implode(',', $arrPeersCustomers) . ')';
                $arrPeersCustomersOrders = $objQuery->select($col, $from, $where);

                $arrProductIdsInOrderDetailsByPeers = array();
                foreach ($arrPeersCustomersOrders as $key2 => $value2) {
                    $product_id = $value2['product_id'];
                    if (!array_key_exists($product_id, $arrProductIdsInOrderDetailsByPeers)) {
                        $arrOrderDetailProducts = array(
                            'product_id'      => $product_id,
                            'quantity'        => $value2['quantity'],
                            'total_amount'    => $value2['price'] * $value2['quantity'],
                            'count'           => 1,
                        );
                        $arrProductIdsInOrderDetailsByPeers[$product_id] = $arrOrderDetailProducts;
                    } else {
                        $arrProductIdsInOrderDetailsByPeers[$product_id]['quantity'] += $value2['quantity'];
                        $arrProductIdsInOrderDetailsByPeers[$product_id]['total_amount'] += ($value2['price'] * $value2['quantity']);
                        $arrProductIdsInOrderDetailsByPeers[$product_id]['count'] ++;
                    }
                }
            } else {
                // 同年代の会員が存在しない
                $smartrecommend_available_flg = FALSE;
                $arrProductIdsInOrderDetailsByPeers = array();
                $arrProductIDs = array();
            }

            if ($smartrecommend_available_flg) {
                $recommend_type = $arrPluginData['recommend_type'];
                switch($recommend_type) {
                    case '1': // 個数をキーとして、降順ソートでリコメンド商品を決める
                        foreach ($arrProductIdsInOrderDetailsByPeers as $key3 => $value3) {
                          $id[$key3] = $value3['quantity'];
                        }
                        break;
                    case '2': // 合計販売金額(売価×個数)をキーとして、降順ソートでリコメンド商品を決める
                        foreach ($arrProductIdsInOrderDetailsByPeers as $key3 => $value3) {
                          $id[$key3] = $value3['total_amount'];
                        }
                        break;
                    case '3': // 購入された件数(回数)をキーとして、降順ソートでリコメンド商品を決める
                        foreach ($arrProductIdsInOrderDetailsByPeers as $key3 => $value3) {
                          $id[$key3] = $value3['count'];
                        }
                        break;
                    default:
                }

                // array_multisortで'recommend_type'の列を降順に並び替える
                array_multisort($id, SORT_DESC, $arrProductIdsInOrderDetailsByPeers);

                $display_counter = 0;
                foreach ($arrProductIdsInOrderDetailsByPeers as $key4 => $value4) {
                    if ($display_counter < $arrPluginData['display_count']) {
                        $arrProductIDs[] = $value4['product_id'];
                        $display_counter ++;
                    } else {
                        break;
                    }
                }
            }

            $objProduct = new SC_Product_Ex();

            $this->display_smartrecommend_bloc_flg = FALSE;
            $arrSmartRecommendItems = array();
            foreach ($arrProductIDs as $key3 => $value3) {
                $arrSmartRecommendItems[$key3] = $this->findProducts(array($value3), $objProduct);
                $this->display_smartrecommend_bloc_flg = TRUE;
            }

            $this->arrSmartRecommendItems  = $arrSmartRecommendItems;
            $this->block_display_title_flg = $arrPluginData['block_display_title_flg'];
            $this->block_display_title     = $arrPluginData['block_display_title'];

        } else {
            // このユーザーの誕生日は有効な日付が設定されていない
            $this->display_smartrecommend_bloc_flg = FALSE;
            $arrSmartRecommendItems = array();
            $this->arrSmartRecommendItems  = $arrSmartRecommendItems;
        }
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
        parent::destroy();
    }

    private function getSaveKey()
    {
        return 'plg_SmartRecommend';
    }

    /**
     * 商品を検索する.
     *
     * @param array $arrValues 検索条件のパラメーター
     * @param SC_Product $objProduct SC_Product インスタンス
     * @return array 商品の検索結果
     */
    function findProducts($arrValues, &$objProduct) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 読み込む列とテーブルの指定
        $col = 'product_id, name, main_list_image, status, product_code_min, product_code_max, price02_min, price02_max, stock_min, stock_max, stock_unlimited_min, stock_unlimited_max, update_date';
        $from = $objProduct->alldtlSQL();
        $where  = 'product_id = ?';
        $arrResult = $objQuery->select($col, $from, $where, $arrValues);

        $arrResult[0]['price02_min_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax($arrResult[0]['price02_min']);
        $arrResult[0]['price02_max_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax($arrResult[0]['price02_max']);

        return $arrResult[0];
    }
}
