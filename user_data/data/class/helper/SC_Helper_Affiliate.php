<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * 配送方法を管理するヘルパークラス.
 *
 * @package Helper
 * @author pineray
 * @version $Id:$
 */
class SC_Helper_Affiliate
{
    /**
     * 配送方法の情報を取得.
     *
     * @param  integer $affiliate_id    配送方法ID
     * @param  boolean $has_deleted 削除された支払方法も含む場合 true; 初期値 false
     * @return array
     */
    public function get($affiliate_id, $has_deleted = false)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 配送業者一覧の取得
        $col = '*';
        $where = 'affiliate_id = ?';
        if (!$has_deleted) {
            $where .= ' AND del_flg = 0';
        }
        $table = 'dtb_affiliate';
        $arrRet = $objQuery->select($col, $table, $where, array($affiliate_id));

        if (empty($arrRet)) {
            return array();
        }
        $arrAffiliate = $arrRet[0];

        return $arrAffiliate;
    }

    /**
     * 配送方法一覧の取得.
     *
     * @param  integer $product_type_id 商品種別ID
     * @param  boolean $has_deleted     削除された支払方法も含む場合 true; 初期値 false
     * @return array
     */
    public function getList($product_type_id = null, $has_deleted = false)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $col = '*';
        $where = '';
        $arrVal = array();
        if (!$has_deleted) {
            $where .= 'del_flg = 0';
        }
        $table = 'dtb_affiliate';
        $objQuery->setOrder('affiliate_id DESC');
        $arrRet = $objQuery->select($col, $table, $where, $arrVal);

        return $arrRet;
    }

    /**
     * 配送方法の登録.
     *
     * @param  array   $sqlval
     * @return integer $affiliate_id
     */
    public function save($sqlval)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();


        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';

        // affiliate_id が決まっていた場合
        if ($sqlval['affiliate_id'] != '') {
            unset($sqlval['creator_id']);
            unset($sqlval['create_date']);
            $affiliate_id = $sqlval['affiliate_id'];
            $where = 'affiliate_id = ?';
            $objQuery->update('dtb_affiliate', $sqlval, $where, array($affiliate_id));

        } else {
            // 登録する配送業者IDの取得
            $affiliate_id = $objQuery->nextVal('dtb_affiliate_id');
            $sqlval['affiliate_id'] = $affiliate_id;
            $sqlval['create_date'] = 'CURRENT_TIMESTAMP';
            // INSERTの実行
            $objQuery->insert('dtb_affiliate', $sqlval);

        }

        $objQuery->commit();

        return $affiliate_id;
    }

    /**
     * 配送方法の削除.
     *
     * @param  integer $affiliate_id 配送方法ID
     * @return void
     */
    public function delete($affiliate_id)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

		$sqlval['del_flg']=1;
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $sqlval['update_id'] = $_SESSION['member_id'];
        unset($sqlval['name']);
        unset($sqlval['affiliateid']);
        unset($sqlval['comment']);
        unset($sqlval['status']);
        unset($sqlval['creator_id']);
		
        // affiliate_id が決まっていた場合
        if (!empty($affiliate_id)) {
            $where = 'affiliate_id = ?';
            $objQuery->update('dtb_affiliate', $sqlval, $where, array($affiliate_id));
		}
        $objQuery->commit();

    }


    /**
     * 同じ内容の配送方法が存在するか確認.
     *
     * @param  array   $arrAffiliate
     * @return boolean
     */
    public function checkExist($arrAffiliate)
    {
        $objDb = new SC_Helper_DB_Ex();
        if ($arrAffiliate['affiliate_id'] == '') {
            $ret = $objDb->sfIsRecord('dtb_affiliate', 'affiliateid', array($arrAffiliate['affiliateid']));
        } else {
            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $ret = (($objQuery->count('dtb_affiliate', 'affiliate_id != ? AND affiliateid = ? AND del_flg = 0', array($arrAffiliate['affiliate_id'], $arrAffiliate['affiliateid'])) > 0) ? true : false);
        }

        return $ret;
    }

    /**
     * 配送方法IDをキー, 名前を値とする配列を取得.
     *
     * @param  string $type 値のタイプ
     * @return array
     */
    public static function getIDValueList($type = 'name')
    {
        return SC_Helper_DB_Ex::sfGetIDValueList('dtb_affiliate', 'affiliate_id', $type);
    }

}
