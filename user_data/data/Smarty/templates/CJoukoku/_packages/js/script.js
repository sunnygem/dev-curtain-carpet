// JavaScript Document

$(function(){
    var moveElm = $('nav > ul > li');
    moveTimer = 0;
    hideTimer = 0;
 
    moveElm.hover(function(){
        var self = $(this),
        selfMdd = self.find('.navContents');
 
        self.on('mousemove', function(){
 
            clearTimeout(moveTimer);
            clearTimeout(hideTimer);
 
            moveTimer = setTimeout(function(){
                self.addClass('mddActive');
                selfMdd.css({display:'block'}).stop().animate({height:'280px',opacity:'1'},200,'swing');
 
                self.siblings('li').removeClass('mddActive');
                self.siblings('li').find('.navContents').stop().animate({height:'0',opacity:'0'},function(){
                    $(this).css({display:'none'});
                });
            }, 200);
        });
    },function(){
        var self = $(this),
        selfMdd = self.find('.navContents');
 
        clearTimeout(moveTimer);
 
        hideTimer = setTimeout(function(){
            self.removeClass('mddActive');
            selfMdd.css({display:'none'});
        }, 200);
    });
 
    $('nav > ul').hover(function(e){
        e.preventDefault();
    },function(){
        $('nav > ul > li').removeClass('mddActive').find('.navContents').stop().animate({height:'0',opacity:'0'});
    });
});

$(function()
{
	$( 'img.lazy' ).lazyload( {
		threshold : 100 ,
		effect : 'fadeIn' ,
		effect_speed: 2000 ,
		failure_limit: 2,
	} ) ;
} ) ;

$(function(){
	$(".curtain,.carpet").addClass("module");
	var Module = $(".module");
	Module.hide();
	Module.first().show();
	Module.find().hide();
	$(".tab").click(function(){
		var index = $(this).index();
		$(".tab").removeClass("active");
		$(this).addClass("active");
		$(Module).hide();
		$(Module).eq(index).show();
	});
});

$(function() {
    var count = 68;
 $('.voiceComent').each(function() {
     var thisText = $(this).text();
      var textLength = thisText.length;
       if (textLength > count) {
            var showText = thisText.substring(0, count);
            var insertText = showText;
          insertText += '<span class="omit">…</span>';
            $(this).html(insertText);
       };
  });
});

$(function(){
	$(window).load(function(){
		var $setElm = $('.ticker');
		var effectSpeed = 1000;
		var switchDelay = 3000;
		var easing = 'swing';

		$setElm.each(function(){
			var effectFilter = $(this).attr('rel'); // 'fade' or 'roll' or 'slide'

			var $targetObj = $(this);
			var $targetUl = $targetObj.children('ul');
			var $targetLi = $targetObj.find('li');
			var $setList = $targetObj.find('li:first');

			var ulWidth = $targetUl.width();
			var listHeight = $targetLi.height();
			$targetObj.css({height:(listHeight)});
			$targetLi.css({top:'0',left:'0',position:'absolute'});

			if(effectFilter == 'fade') {
				$setList.css({display:'block',opacity:'0',zIndex:'98'}).stop().animate({opacity:'1'},effectSpeed,easing).addClass('showlist');
				setInterval(function(){
					var $activeShow = $targetObj.find('.showlist');
					$activeShow.animate({opacity:'0'},effectSpeed,easing,function(){
						$(this).next().css({display:'block',opacity:'0',zIndex:'99'}).animate({opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({display:'none',zIndex:'98'}).removeClass('showlist');
					});
				},switchDelay);
				
			} else if(effectFilter == 'roll') {
				$setList.css({top:'3em',display:'block',opacity:'0',zIndex:'98'}).stop().animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist');
				setInterval(function(){
					var $activeShow = $targetObj.find('.showlist');
					$activeShow.animate({top:'-3em',opacity:'0'},effectSpeed,easing).next().css({top:'3em',display:'block',opacity:'0',zIndex:'99'}).animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist');
				},switchDelay);
				
			} else if(effectFilter == 'slide') {
				$setList.css({left:(ulWidth),display:'block',opacity:'0',zIndex:'98'}).stop().animate({left:'0',opacity:'1'},effectSpeed,easing).addClass('showlist');
				setInterval(function(){
					var $activeShow = $targetObj.find('.showlist');
					$activeShow.animate({left:(-(ulWidth)),opacity:'0'},effectSpeed,easing).next().css({left:(ulWidth),display:'block',opacity:'0',zIndex:'99'}).animate({left:'0',opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist');
				},switchDelay);
			}
		});
	});
});

$(function(){
    $(document).on('ready', function() {
			  $(".mainBanner").slick({
				autoplay: true,
				autoplaySpeed: 3000,
				dots: true,
				infinite: true,
				centerMode: true,
				variableWidth: true,
				responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		  ]
			});	  
		  $(".center").slick({
			dots: true,
			infinite: true,
			centerMode: true,
			slidesToShow: 3,
			slidesToScroll: 3
		  });
		  
		  $(".productsSlide").slick({
			autoplay: true,
			autoplaySpeed: 3000,
			dots: false,
			infinite: true,
			centerMode: true,
			slidesToShow: 4,
			slidesToScroll: 1
		  });
	});
});


  
$(function() {
	var topBtn = $('#footerCartBox');
	topBtn.hide();
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
});
/*
$(function(){
	$("#headerNav").append("<li id='magic-line'></li>");
		
    var $magicLine = $("#magic-line");
    
    $magicLine
        .width($(".current_page_item").width())
        .css("left", $(".current_page_item a").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());
        
    $("#headerNav li").find("a").hover(function() {
        $el = $(this);
        leftPos = $el.position().left;
        newWidth = $el.parent().width();
        
        $magicLine.stop().animate({
            left: leftPos,
            width: newWidth
        });
    }, function() {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft"),
            width: $magicLine.data("origWidth")
        });    
    });
	
});
*/