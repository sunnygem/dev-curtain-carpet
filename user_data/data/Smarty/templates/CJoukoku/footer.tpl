<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--▼FOOTER-->
<!--{strip}-->

<!--{assign var=index value="`$smarty.const.ROOT_URLPATH`index.php"}-->
<!--{if $smarty.server.PHP_SELF==$index}-->
<div id="for_buisiness">
    <img src="/shop/user_data/packages/CJoukoku/img/banner/bnr_buisiness.png">
</div>
<!--{/if}-->
<footer>
    <div id="footerTop">
        <div class="footer_wrap">
            <div class="footer_inner">
                <div class="calender">
                    <div class="ttl">オンラインショップ・カレンダー</div>
                    <div style="text-align:left; padding-left:10px; padding-right:10px; ">※ オンラインショップのカレンダーです。店舗の営業日に関しては各店の営業日をご確認ください。</div>
                    <div style="text-align:left; padding-left:10px; padding-right:10px; margin-top:7px; ">お近くのお店は、こちらでお探しください &gt;&gt; <a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top/" style="color:#ffffff; "><b>店舗検索</b></a></div>
                    <div class="calender_list">
                        <!--{foreach from=$arrCalendar item=val}-->
                            <!--{foreach from=$val item=val2 name=calender1}-->
                            <div>
                                <!--{if $smarty.foreach.calender1.first}-->
                                    <table>
                                    <caption class="month"><!--{$val2.month}--> 月</caption>
                                    <thead><tr><th>日</th><th>月</th><th>火</th><th>水</th><th>木</th><th>金</th><th>土</th></tr></thead>
                                <!--{/if}-->
                                <!--{if $val2.first}-->
                                    <tr>
                                <!--{/if}-->
                                <!--{if !$val2.in_month}-->
                                    <td></td>
                                <!--{elseif $val2.holiday}-->
                                    <td class="<!--{if $val2.today}--> today<!--{/if}-->"><span class="off"><!--{$val2.day}--></span></td>
                                <!--{else}-->
                                    <td<!--{if $val2.today}--> class="today"<!--{/if}-->><!--{$val2.day}--></td>
                                <!--{/if}-->
                                <!--{if $val2.last}-->
                                    </tr>
                                <!--{/if}-->
                                <!--{if $smarty.foreach.calender1.last}-->
                                    </table>
                                <!--{/if}-->
                            </div>
                            <!--{/foreach}-->
                        <!--{/foreach}-->
                        <!--
                         <ul>
                             <li><img src="/shop/user_data/packages/CJoukoku/img/footer/footer_calender_sample01.png" alt="calender_april"></li>
                             <li><img src="/shop/user_data/packages/CJoukoku/img/footer/footer_calender_sample02.png" alt="calender_may"></li>
                         </ul>
                         -->
                    </div>
                    <p class="footer_attention">黄色は休業日です。<br>営業日時：平日　午前10時～午後17時00分<br>※オンラインショップからのご注文は24時間受け付けいたしております。<br><br>※既製品のカーテンは、ご入金確認後、３?５日営業日で出荷となります。<br>※オーダーカーテン（イージーオーダー含む）は、ご入金確認後、約２週間後の出荷となります。<br>※ご注文・ご入金確定後に、納期のご案内メールを送らせていただきます。<br></p>
                </div>
                <div class="shopping_guide">
                    <div class="ttl">ショッピングガイド</div>
                    <ul class="footer_menu">
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/advice.php">商品選びに困ったら</a></li>
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/flow.php">ご購入までの流れ</a></li>
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php">注文からお届けまでの流れ</a></li>
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php">送料やお支払い、返品・交換について</a></li>        
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/measurements.php">採寸方法</a></li>
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/care.php">お手入れ方法</a></li>
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/faq.php">よくある質問</a></li>
                    </ul>
                    <div class="ttl">店舗のご案内</div>
                    　<ul class="footer_menu">
                        <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->reserve/">店舗でのご相談</a></li>
                    </ul>
                    <div class="recruit"><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->recruit"><img src="<!--{$TPL_URLPATH}-->img/footer/footer_bnr_recruit.png"></a></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="footerBottom">
        <div class="footer_wrap">
            <div class="footer_menu_ttl">Menu</div>
            <div class="footer_inner">
                <div>
                  <div class="ttl"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1771" target="_top">テイストで選ぶ</a></div>
                    <ul class="footer_menu">
                        <!--{foreach from=$arrCategoryTaste item=val}-->
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->"><!--{$val.category_name}--></a></li>
                        <!--{/foreach}-->
                    </ul>
                    <div class="ttl"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1772" target="_top">色で選ぶ</a></div>
                    <ul class="footer_menu">
                        <!--{foreach from=$arrCategoryColor item=val}-->
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->"><!--{$val.category_name}--></a></li>
                        <!--{/foreach}-->
                    </ul>
                </div>
                <div>
                    <div class="ttl"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1773" target="_top">機能から選ぶ</a></div>
                    <ul class="footer_menu">
                        <li><span>ドレープ</span></li>
                        <!--{foreach from=$arrCategoryFunctionDrape item=val}-->
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->"><!--{$val.category_name}--></a></li>
                        <!--{/foreach}-->
                        <li><span>レース</span></li>
                        <!--{foreach from=$arrCategoryFunctionLace item=val}-->
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->"><!--{$val.category_name}--></a></li>
                        <!--{/foreach}-->
                    </ul>
                </div>
                <div>
                    <div class="ttl"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1774" target="_top">種類から選ぶ</a></div>
                    <ul class="footer_menu">
                        <!--{foreach from=$arrCategoryType item=val}-->
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->"><!--{$val.category_name}--></a></li>
                        <!--{/foreach}-->
                    </ul>
                    <div class="ttl"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1776" target="_top">価格で選ぶ</a></div>
                    <ul class="footer_menu">
                        <!--{foreach from=$arrCategoryPrice item=val}-->
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->"><!--{$val.category_name}--></a></li>
                        <!--{/foreach}-->
                    </ul>
                </div>
                <div>
                    <div class="ttl"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1775" target="_top">ブランドで選ぶ</a></div>
                    <ul class="footer_menu">
                        <!--{foreach from=$arrCategoryBrand item=val}-->
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->"><!--{$val.category_name}--></a></li>
                        <!--{/foreach}-->
                    </ul>
                </div>
               <!--<div>
                    <div class="ttl"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1714" target="_top">カーペット</a></div>
                </div>-->
               <!--<div>
                    <div class="ttl"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1858" target="_top">雑貨</a></div>
                </div>-->
            </div>
        </div>
    </div>
    <div class="copy">
        <div class="copy_inner">
            <div class="company_menu">
                <ul>
                    <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->">会社概要</a></li>
                    <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->recruit/">採用情報</a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->guide/privacy.php">プライバシーポリシー</a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->order/index.php">特定商取引法に基づく表記</a></li>
                </ul>
            </div>
            <div>
                Copyright &copy; 2005-2019 CurtainJuutanOukoku OnlineShop All rights reserved.
            </div>
        </div>
    </div>
</footer>

<!--{/strip}-->
<!--▲FOOTER-->
