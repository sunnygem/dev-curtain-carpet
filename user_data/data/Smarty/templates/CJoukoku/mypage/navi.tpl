<!--{*
/*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<style>
	//.mypagemenus li:last-child,.mypagemenus li:nth-last-child(2) {
	//	display:none;
	//}
	//#mynavi_area .point_announce {
	//	border:none;
	//}
</style>
<div id="mynavi_area">
    <!--{strip}-->
        <div class="mynavi_list btn-group btn-group-justified margin-bottom-lg">
          <a href="<!--{if $tpl_login}-->./<!--{$smarty.const.DIR_INDEX_PATH}--><!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-shopping-cart"></span> 購入履歴を見る</a>
          <a href="<!--{if $tpl_login}-->favorite.php<!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-heart"></span> お気に入りを見る</a>
          <a href="<!--{if $tpl_login}-->sample.php<!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-file"></span> 生地サンプルの登録を見る</a>
          <a href="<!--{if $tpl_login}-->delivery.php<!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-truck"></span> お届け先追加・変更</a>
		  <a href="<!--{if $tpl_login}-->change.php<!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-wrench"></span> 会員登録内容変更</a>
 		  <a href="<!--{if $tpl_login}-->refusal.php<!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-ban"></span> 退会の手続き</a>
   		  <a href="index.php?mode=logout" class="<!--{if $tpl_mypageno == 'refusal'}--> selected<!--{/if}-->"><span class="fa fa-sign-out"> ログアウト</a>
          <!--
          <div class="btn-group">
            <ul class="mypagemenus" role="menu">
              <li><a href="<!--{if $tpl_login}-->delivery.php<!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-truck"></span> お届け先追加・変更</a></li>
			  <li><a href="<!--{if $tpl_login}-->change.php<!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-wrench"></span> 会員登録内容変更</a></li>
 			  <li><a href="<!--{if $tpl_login}-->refusal.php<!--{else}--><!--{$smarty.const.TOP_URL}--><!--{/if}-->"><span class="fa fa-ban"></span> 退会の手続き</a></li>
   		      <li><a href="index.php?mode=logout" class="<!--{if $tpl_mypageno == 'refusal'}--> selected<!--{/if}-->">ログアウト</a></li>

			  </ul>
          </div>
          -->
        </div>

    <!--{/strip}-->

</div>
<!--▲NAVI-->
