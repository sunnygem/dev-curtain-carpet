<section class="campaign">
    <div class="campaign_wrap">
        <ul>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1829">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner08.jpg" alt="UVカット">
                    </div>
                    <div class="campaign_txt">
                        <span>UVカット</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1827">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner09.jpg" alt="洗える">
                    </div>
                    <div class="campaign_txt">
                        <span>洗える</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1823">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner10.jpg" alt="省エネ">
                    </div>
                    <div class="campaign_txt">
                        <span>省エネ</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1824">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner11.jpg" alt="裏地付き">
                    </div>
                    <div class="campaign_txt">
                        <span>裏地付き</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1833">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner12.jpg" alt="防炎">
                    </div>
                    <div class="campaign_txt">
                        <span>防炎</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1832">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner13.jpg" alt="花粉キャッチ">
                    </div>
                    <div class="campaign_txt">
                        <span>花粉キャッチ</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1826">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner14.jpg" alt="防音">
                    </div>
                    <div class="campaign_txt">
                        <span>防音</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1831">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner15.jpg" alt="防虫">
                    </div>
                    <div class="campaign_txt">
                        <span>防虫</span>
                    </div>
                </a>
            </li>
            <li class="column1">
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1714">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner16.jpg" alt="カーペット">
                    </div>
                </a>
            </li>
            <li class="column1">
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1858">
                    <div class="campaign_img">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_banner17.jpg" alt="雑貨">
                    </div>
                </a>
            </li>
        </ul>
    </div>
</section>
