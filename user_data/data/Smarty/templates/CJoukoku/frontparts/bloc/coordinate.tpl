<section class="coordinate">
    <div class="oukoku_style_img">
        <img src="<!--{$TPL_URLPATH}-->img/top/top_coodinate_main.jpg" alt="">
    </div>
    <div class="oukoku_style">
        <ul>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1851">
                    <div class="o_style_wrap">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_pastel.jpg" alt="">
                    </div>
                    <div class="color">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_pastel_color.jpg" alt="">
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1852">
                    <div class="o_style_wrap">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_colorful.jpg" alt="">
                    </div>
                    <div class="color">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_colorful_color.jpg" alt="">
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1853">
                    <div class="o_style_wrap">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_blue.jpg" alt="">
                    </div>
                    <div class="color">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_blue_color.jpg" alt="">
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1854">
                    <div class="o_style_wrap">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_modern.jpg" alt="">
                    </div>
                    <div class="color">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_modern_color.jpg" alt="">
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1855">
                    <div class="o_style_wrap">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_soft.jpg" alt="">
                    </div>
                    <div class="color">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_soft_color.jpg" alt="">
                    </div>
                </a>
            </li>
            <li>
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1856">
                    <div class="o_style_wrap">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_luxury.jpg" alt="">
                    </div>
                    <div class="color">
                        <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_luxury_color.jpg" alt="">
                    </div>
                </a>
            </li>
        </ul>
    </div>
</section>
