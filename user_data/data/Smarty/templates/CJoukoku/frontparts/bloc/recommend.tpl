<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{strip}-->
<!--{if count($arrBestProducts) > 0}-->
    <div class="block_outer recommend">
        <h2><span>おすすめ商品</span></h2>
        <div class="product_list category">
            <ul>
            <!--{foreach from=$arrBestProducts item=arrProduct name="recommend_products"}-->
                <li>
                    <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" />
                    </a>
                    <h4> <!--{$arrProduct.name|h}--> </h4>
                    <div class="pricebox sale_price">
                        <p class="price <!--{if $arrProduct.sale_flg === "1"}-->sale<!--{/if}-->">
                            <!--{if $arrProduct.sale_flg === "1"}-->
                                <span class="normal_price"> <!--{$arrProduct.price01_min|n2s}--> 円～ </span>
                            <!--{/if}-->
                            <span class="price">
                                 <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                                     <!--{$arrProduct.price02_min|n2s}-->円
                                 <!--{else}-->
                                     <!--{$arrProduct.price02_min|n2s}-->円～
                                 <!--{/if}-->
                            </span>
                            <!--{*$arrProduct.comment|h|nl2br*}-->
                            <span style="display: inline-block;">(税抜)</span>
                        </p>
                    </div>
                </li>
            <!--{/foreach}-->
            <ul>
        </div>
    </div>
<!--{/if}-->
<!--{/strip}-->
