<div id="right_navi">
    <div class="shop">
        <a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_campaign03.png" alt="店舗検索"></a>
    </div>
    <div class="side_sns">
        <ul>
            <li><a href="https://www.instagram.com/oukoku_official/" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/icon/side_icon_instagram.png" alt="instagram"></a></li>
            <li><a href="https://www.facebook.com/oukoku1/" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/icon/side_icon_facebook.png" alt="facebook"></a></li>
            <li><a href="https://twitter.com/oukoku_official" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/icon/side_icon_twitter.png" alt="twitter"></a></li>
        </ul>
    </div>
    <div class="side_search">
        <form role="search" name="search_form" id="header_search_form" method="get" action="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="search" />
            <div class="input_wrap">
                <input type="text" id="" class="" name="name" maxlength="50" value="<!--{$smarty.get.name|h}-->" placeholder="キーワードを入力">
                <button type="submit"></button>
            </div>
        </form>
        <p><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/research.php"><span class="search_icon"></span>こだわり検索はこちらから</a></p>
    </div>
    <div class="side_menu">
        <div class="first">
            <div class="first_ttl">
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/first.php">はじめての方へ</a>
            </div>
            <div class="shopping_guide">
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/shopping_guide.php">
                    ショッピングガイド
                </a>
            </div>
            <div class="sub">
                <ul>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/advice.php">商品選びに困ったら</a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/flow.php">ご購入までの流れ</a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/order.php">注文からお届けまでの流れ</a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php">送料お支払いについて</a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/measurements.php">採寸方法</a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/care.php">お手入れ方法</a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/faq.php">よくある質問</a></li>
                </ul>
            </div>
        </div>
        <div class="side_feature">
            <ul>
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_hokuo.php"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_feature_sample01.jpg" alt="北欧"></a></li>
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_petit_flower.php"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_feature_sample02.jpg" alt="プチフラワー"></a></li>
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_botanical.php"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_feature_sample03.jpg" alt="ボタニカル"></a></li>
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_otokomae.php"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_feature_sample04.jpg" alt="男前"></a></li>
            </ul>
            <div class="plain_order" style="display:none;">
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1796">プレーンオーダー</a>
            </div>
        </div>
        <div class="side_category">
            <div class="side_category01">
                <div class="category_ttl"><span>カーテン</span>を選ぶ</div>
                <div class="category_img"><img src="<!--{$TPL_URLPATH}-->img/banner/side_curtain_img.jpg"></div>
                <div class="category_select">
                    <dl>
                        <dt class=""><span>機能</span>で選ぶ</dt>
                        <dd style="display:none;">
                            <div class="func_ttl ">
                                ドレープ
                            </div>
                            <div class="func_list" style="display:none;">
                                <div class="img"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1792"><img src="<!--{$TPL_URLPATH}-->img/banner/side_drape_img.jpg"></a></div>
                                <ul>
                                <!--{foreach from=$arrCategoryFunctionDrape item=val}-->
                                    <li>
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <!--{if strlen( $val.thumbnail_path ) > 0}-->
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <!--{/if}-->
                                            <div class="category_name"><!--{$val.category_name}--></div>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                                </ul>
                            </div>
                            <div class="func_ttl ">
                                レース
                            </div>
                            <div class="func_list" style="display:none;">
                                <div class="img"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1793"><img src="<!--{$TPL_URLPATH}-->img/banner/side_lace_img.jpg"></a></div>
                                <ul>
                                <!--{foreach from=$arrCategoryFunctionLace item=val}-->
                                    <li>
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <!--{if strlen( $val.thumbnail_path ) > 0}-->
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <!--{/if}-->
                                            <div class="category_name"><!--{$val.category_name}--></div>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                                </ul>
                            </div>
                        </dd>
                        <dt class=""><span>色</span>で選ぶ</dt>
                        <dd class="category_color" style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryColor item=val}-->
                                    <li>
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <!--{if strlen( $val.thumbnail_path ) > 0}-->
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <!--{/if}-->
                                            <div class="category_name"><!--{$val.category_name}--></div>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                        <dt class=""><span>テイスト</span>で選ぶ</dt>
                        <dd style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryTaste item=val}-->
                                    <li>
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <div class="category_name"><!--{$val.category_name}--></div>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                        <dt class=""><span>種類</span>から選ぶ</dt>
                        <dd style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryType item=val}-->
                                    <li class="txt">
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <!--{*if strlen( $val.thumbnail_path ) > 0}-->
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <!--{/if*}-->
                                            <span><!--{$val.category_name}--></span>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                        <dt class=""><span>価格</span>で選ぶ</dt>
                        <dd style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryPrice item=val}-->
                                    <li class="txt">
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <span><!--{$val.category_name}--></span>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                        <dt><span>ブランド</span>から選ぶ</dt>
                        <dd style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryBrand item=val}-->
                                    <li class="txt">
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <span><!--{$val.category_name}--></span>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="bnr_area">
                <ul>
                    <li style="display:none;"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1812"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_campaign01.png" alt="遮光1級キャンペーン"></a></li>
                    <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpp-service/free/"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_campaign02.png" alt="採寸メジャープレゼント"></a></li>
                    <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_campaign03.png" alt="店舗検索"></a></li>
                    <li><a href="/recruit"><img src="<!--{$TPL_URLPATH}-->img/footer/footer_bnr_recruit.png" alt="採用情報"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="other_category">
        <div class="side_category">
            <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1714">
            <div class="category_ttl"><span>カーペット</span>を選ぶ</div>
            <div class="category_img"><img src="<!--{$TPL_URLPATH}-->img/banner/side_carpet_img.jpg"></div>
            </a>
        </div>
        <div class="side_category">
            <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1858">
            <div class="category_ttl"><span>雑貨</span>を選ぶ</div>
            <div class="category_img"><img src="<!--{$TPL_URLPATH}-->img/banner/side_goods_img.jpg"></div>
            </a>
        </div>
    </div>
</div>


