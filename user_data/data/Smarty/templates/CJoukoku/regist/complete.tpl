<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

    <div class="member_regist_wrapper" >
        <div class="member_regist_title" >
            <h2><!--{$tpl_title|h}--></h2>
        </div>
        <div class="member_regist_flow">
            <ul>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step01.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step02.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step03.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step04_on.png">
                </li>
            </ul>
        </div>
			
			
        
        <div class="logo" style="text-align:center;"><img src="<!--{$TPL_URLPATH}-->img/common/logo_01.png"></div>
			
        <div class="gray-area" style="text-align:center;margin:30px 0;">
            <div class="big">本登録完了</div>
            <div class="tyu">本登録が完了しました。</div>
            <div class="small">引き続きお買い物をお楽しみください。</div>	
        </div>

        <div class="member_submit_wrapper">
            <div class="member_submit_buttons">
                <button onclick="location.href='<!--{$smarty.const.TOP_URL}-->'" class="reset">TOPに戻る</button>
            </div>
        </div>
    </div>
