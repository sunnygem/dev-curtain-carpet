<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{strip}-->
<!--
    <div id="main_image">
        <a href="<!--{$smarty.const.P_DETAIL_URLPATH}-->1">
            <img class="hover_change_image img-responsive" src="<!--{$TPL_URLPATH}-->img/banner/bnr_top_main.jpg" alt="詳細はこちら"  width="100%" />
        </a>
    </div>-->


<section id="mainBanner">
    <ul class="mainBanner slider">
        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_hokuo.php"><img src="<!--{$TPL_URLPATH}-->img/top/slider_hokuo.jpg"         alt="" height="auto" class="lazy"></a></li>
        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_petit_flower.php"><img src="<!--{$TPL_URLPATH}-->img/top/slider_petit_flower.jpg"  alt="" height="auto" class="lazy" /></a></li>
        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_botanical.php"><img src="<!--{$TPL_URLPATH}-->img/top/slider_botanical.jpg"     alt="" height="auto" class="lazy" /></a></li>
        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_otokomae.php"><img src="<!--{$TPL_URLPATH}-->img/top/slider_otokomae.jpg"      alt="" height="auto" class="lazy" /></a></li>
        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1821"><img src="<!--{$TPL_URLPATH}-->img/top/slider_shade.jpg"         alt="" height="auto" class="lazy" /></a></li>
        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1823"><img src="<!--{$TPL_URLPATH}-->img/top/slider_energy_saving.jpg" alt="" height="auto" class="lazy" /></a></li>
    </ul>
</section>
<!--{/strip}-->
