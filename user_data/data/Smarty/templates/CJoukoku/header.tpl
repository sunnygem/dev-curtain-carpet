<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA    02111-1307, USA.
 *}-->

<!--▼HEADER-->
<!--{strip}-->
<style>
	.input-group button {
		cursor:pointer;
	}
	.input-group button:hover {
		opacity:0.6;
	}
</style>
<header>
    <div id="topBanner">
        <a href="#">
            全国87店舗･創業51年で育てたクオリティと安心をお届けします
        </a>
    </div>
	<div id="headerInner">
        <div class="logo">
            <h1><a href="<!--{$smarty.const.TOP_URL}-->" target="_top">カーテンじゅうたん王国</a></h1>
        </div>
        <div class="menu">
            <ul>
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/first.php"><img src="<!--{$TPL_URLPATH}-->img/header/header_menu01.png" alt="はじめての方へ"></a></li>
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/list_curtain.php" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_menu02.png" alt="カーテン"></a></li>
                <!--<li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1706" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_menu02.png" alt="カーテン"></a></li>-->
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1714" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_menu03.png" alt="カーペット"></a></li>
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1858" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_menu04.png" alt="雑貨"></a></li>
                <!-- /*
                <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/research.php" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_menu05.png"></a></li>
                */ -->
                <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_menu06.png" alt="店舗検索"></a></li>
            </ul>
        </div>
        <div class="login">
            <ul>
                <!--{if $tpl_login === true}-->
                    <li><i class="logIcon"></i><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_login01b.png"></a></li>
                <!--{else}-->
                    <li><i class="logIcon"></i><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_login01.png"></a></li>
                <!--{/if}-->
                <li><a href="<!--{$smarty.const.CART_URL}-->" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/header_login02.png"></a></li>
            </ul>
        </div>
    </div>
</header>

<div id="spNav">

    <div id="spHeader">
        <div class="flex">
            <div class="navBtn"></div>
            <div class="flex">
                <div class="sp_shop_search">
                    <a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top/" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/sp_header_shopsearch.png" alt="店舗検索"></a>
                </div>
                <div id="cartBtnSp">
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->cart/index.php" target="_top"><img src="<!--{$TPL_URLPATH}-->img/header/sp_header_cart.png" alt="カートボタン"></a>
                </div>
            </div>
        </div>
        <div id="splogo">
            <h1><a href="<!--{$smarty.const.ROOT_URLPATH}-->"></a></h1>
        </div>
    </div>  
    <div class="sp_category_menu">
        <div class="flex">
            <div class="sp_category_curtain"><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/list_curtain.php">カーテン</a></div>
            <div class="sp_category_carpet"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1714">カーペット</a></div>
            <div class="sp_category_goods"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1858">雑貨</a></div>
        </div>
    </div>

    <nav role="navigation">
        <div id="spNavi">
            <div class="sp_search">
                <div class="search_wrap">
                    <form role="search" name="search_form" id="header_search_form" method="get" action="<!--{$smarty.const.ROOT_URLPATH}-->
                    products/list.php">
                        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                        <input type="hidden" name="mode" value="search" />
                        <input type="text" id="header-search" class="" name="name" maxlength="50" value="<!--{$smarty.get.name|h}-->" placeholder="キーワードを入力">
                        <button type="submit" class=""><img src="<!--{$TPL_URLPATH}-->img/icon/icon_search.png"></button>
                    </form>
                </div>
                <div class="search_menu">
                    <ul class="flex">
                        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/research.php">こだわり検索</a></li>
                        <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->contact/index.php">お問合せ</a></li>
                    </ul>
                </div>
            </div>
            <div class="sp_shop_search">
                <a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_campaign04.jpg" alt="店舗検索"></a>
            </div>
            <div class="spmenu_login">
                <ul class="flex">
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->entry/kiyaku.php" class="regist">会員登録</a></li>
                    <li><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/" class="login">ログイン</a></li>
                    <li><a href="<!--{$smarty.const.CART_URL}-->" class="cart">カート</a></li>
                </ul>
            </div>
            <div class="spmenu_guide">
                <dl>
                    <dt style="padding-bottom: 10px;"><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/first.php">はじめての方へ</a></dt>
                    <dt><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/shopping_guide.php">ショッピングガイド</a></dt>
                    <dd>
                        <ul>
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/advice.php">商品選びに困ったら</a></li>
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/flow.php">ご購入までの流れ</a></li>
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/order.php">注文からお届けまでの流れ</a></li>
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php">送料お支払いについて</a></li>
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/measurements.php">採寸方法</a></li>
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/care.php">お手入れ方法</a></li>
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/faq.php">よくある質問</a></li>
                        </ul>
                    </dd>
                </dl>
            </div>
            <div class="spmenu_feature">
                <ul class="flex">
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_hokuo.php"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_feature_sample01.jpg" alt="北欧"></a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_petit_flower.php"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_feature_sample02.jpg" alt="プチフラワー"></a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_botanical.php"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_feature_sample03.jpg" alt="ボタニカル"></a></li>
                    <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/feature_otokomae.php"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_feature_sample04.jpg" alt="男前"></a></li>
                </ul>
            </div>
            <div class="spmenu_product">
                <div class="ttl">
                    <span>カーテン</span>を選ぶ
                </div>
                <div class="img">
                    <img src="<!--{$TPL_URLPATH}-->img/banner/side_curtain_img.jpg">
                </div>
                <div class="select">
                    <dl>
                        <dt class=""><span>機能</span>で選ぶ</dt>
                        <dd style="display:none;">
                            <div class="func_ttl ">
                                ドレープ
                            </div>
                            <div class="func_list" style="display:none;">
                                <div class="img"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1792"><img src="<!--{$TPL_URLPATH}-->img/banner/side_drape_img.jpg"></a></div>
                                <ul>
                                <!--{foreach from=$arrCategoryFunctionDrape item=val}-->
                                    <li>
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <!--{if strlen( $val.thumbnail_path ) > 0}-->
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <!--{/if}-->
                                            <div class="category_name"><!--{$val.category_name}--></div>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                                </ul>
                            </div>
                            <div class="func_ttl ">
                                レース
                            </div>
                            <div class="func_list" style="display:none;">
                                <div class="img"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1793"><img src="<!--{$TPL_URLPATH}-->img/banner/side_lace_img.jpg"></a></div>
                                <ul>
                                <!--{foreach from=$arrCategoryFunctionLace item=val}-->
                                    <li>
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <!--{if strlen( $val.thumbnail_path ) > 0}-->
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <!--{/if}-->
                                            <div class="category_name"><!--{$val.category_name}--></div>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                                </ul>
                            </div>
                        </dd>
                        <dt class=""><span>色</span>で選ぶ</dt>
                        <dd class="category_color" style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryColor item=val}-->
                                    <li>
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <!--{if strlen( $val.thumbnail_path ) > 0}-->
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <!--{/if}-->
                                            <div class="category_name"><!--{$val.category_name}--></div>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                        <dt class=""><span>テイスト</span>で選ぶ</dt>
                        <dd style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryTaste item=val}-->
                                    <li>
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <div class="category_name"><!--{$val.category_name}--></div>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                        <dt class=""><span>種類</span>から選ぶ</dt>
                        <dd style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryType item=val}-->
                                    <li class="txt">
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <!--{if strlen( $val.thumbnail_path ) > 0}-->
                                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                                            <!--{/if}-->
                                            <span><!--{$val.category_name}--></span>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                        <dt class=""><span>価格</span>で選ぶ</dt>
                        <dd style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryPrice item=val}-->
                                    <li class="txt">
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <span><!--{$val.category_name}--></span>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                        <dt><span>ブランド</span>から選ぶ</dt>
                        <dd style="display:none;">
                            <ul>
                                <!--{foreach from=$arrCategoryBrand item=val}-->
                                    <li class="txt">
                                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                                            <span><!--{$val.category_name}--></span>
                                        </a>
                                    </li>
                                <!--{/foreach}-->
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="spmenu_bnr">
                <div class="main_bnr">
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1812"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_campaign01.png" alt="遮光1級キャンペーン"></a>
                </div>
                <ul class="flex">
                    <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpp-service/free/"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_campaign02.png" alt="採寸メジャープレゼント"></a></li>
                    <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->recruit"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_recruit.png" alt="採用情報"></a></li>
                    <li><a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top"><img src="<!--{$TPL_URLPATH}-->img/banner/side_bnr_campaign03.png" alt="店舗検索"></a></li>
                </ul>
            </div>
            <div class="spmenu_product">
                <div class="ttl">
                    <span>カーペット</span>を選ぶ
                </div>
                <div class="img"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1714"><img src="<!--{$TPL_URLPATH}-->img/banner/side_carpet_img.jpg"></a></div>
            </div>
            <div class="spmenu_product">
                <div class="ttl">
                    <span>雑貨</span>を選ぶ
                </div>
                <div class="img"><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1858"><img src="<!--{$TPL_URLPATH}-->img/banner/side_goods_img.jpg"></a></div>
            </div>
            <div class="spmenu_other">
                <ul>
                    <li>
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1792">ドレープカーテン</a>
                    </li>
                    <li>
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1793">
                            レースカーテン
                        </a>
                    </li>
                </ul>
            </div>
            <div class="spmenu_sns">
                <ul class="flex">
                    <li><a href="https://www.instagram.com/oukoku_official/" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/icon/side_icon_instagram.png" alt="instagram"></a></li>
                    <li><a href="https://www.facebook.com/oukoku1/" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/icon/side_icon_facebook.png" alt="facebook"></a></li>
                    <li><a href="https://twitter.com/oukoku_official" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/icon/side_icon_twitter.png" alt="twitter"></a></li>
                </ul>
            </div>
            <!--<div class="telSp">03-5649-3000</div>-->
        </div>
    </nav>
</div>
<div class="overlay"></div>
<!--{/strip}-->
<!--▲HEADER-->
