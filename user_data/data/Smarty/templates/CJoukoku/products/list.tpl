<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<script type="text/javascript">//<![CDATA[
    function fnSetClassCategories(form, classcat_id2_selected) {
        var $form = $(form);
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('select[name=classcategory_id1]');
        var $sele2 = $form.find('select[name=classcategory_id2]');
        eccube.setClassCategories($form, product_id, $sele1, $sele2, classcat_id2_selected);
    }

    // 並び順を変更
    function fnChangeOrderby(orderby) {
        eccube.setValue('orderby', orderby);
        eccube.setValue('pageno', 1);
        eccube.submitForm();
    }
    // 表示件数を変更
    function fnChangeDispNumber(dispNumber) {
        eccube.setValue('disp_number', dispNumber);
        eccube.setValue('pageno', 1);
        eccube.submitForm();
    }

//]]></script>
<style>
@media screen and (max-width:768px) {
	.list-head .left {
		width:inherit;
	}
	.change {
		width: 100%;
		margin-left: 2%;
	}
}
</style>
<main>
<div id="undercolumn">
    
    <!--▼特集-->
    <!--{if strlen( $arrCategory.image_path ) > 0}-->
        <div class="category_feature">
            <!-- カテゴリー画像 -->
            <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrCategory.image_path}-->" alt="<!--{$arrCategory.category_name}-->">
            <!-- カテゴリー画像キャプション -->
            <p><!--{$arrCategory.image_caption|nl2br_html}--></p>
        </div>
    <!--{/if}-->
    <!--▲特集-->
    <form name="form1" id="form1" method="get" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="<!--{$mode|h}-->" />
        <!--{* ▼検索条件 *}-->
        <!--{foreach from=$arrSearchData key=key item=val}-->
            <input type="hidden" name="<!--{$key}-->" value="<!--{$val|h}-->" />
        <!--{/foreach}-->

        <input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" />
        <!-- input type="hidden" name="category" value="<!--{$arrSearchData.category|h}-->" / -->
        <input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
        <input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" />
        <!--{* ▲検索条件 *}-->
        <!--{* ▼ページナビ関連 *}-->
        <input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
        <input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
        <input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
        <!--{* ▲ページナビ関連 *}-->
        <input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />
    </form>

    <!--▼検索条件-->
    <!--{if $tpl_subtitle == "検索結果"}-->
        <ul class="pagecond_area">
            <li><strong>商品カテゴリ：</strong><!--{$arrSearch.category|h}--></li>
        <!--{if $arrSearch.maker|strlen >= 1}--><li><strong>メーカー：</strong><!--{$arrSearch.maker|h}--></li><!--{/if}-->
            <li><strong>商品名：</strong><!--{$arrSearch.name|h}--></li>
        </ul>
    <!--{/if}-->
    <!--▲検索条件-->

    <!--▼ページナビ(本文)-->
    <!--{capture name=page_navi_body}-->
    <div class="pagenumber_area number-head">
        <div class="change">
            <!--{if $orderby != 'date'}-->
                <a href="javascript:fnChangeOrderby('date');">新着順</a>
            <!--{else}-->
                <span>新着順</span>
            <!--{/if}-->&nbsp;
            <!--{if $orderby != "price_h"}-->
                    <a href="javascript:fnChangeOrderby('price_h');">価格の高い順</a>
            <!--{else}-->
                <span>価格の高い順</span>
            <!--{/if}-->&nbsp;
            <!--{if $orderby != "price"}-->
                    <a href="javascript:fnChangeOrderby('price');">価格の低い順</a>
            <!--{else}-->
                <span>価格の低い順</span>
            <!--{/if}-->
        </div>
        <div class="disp_result">
            <label>表示件数</label>
            <div class="select_wrap">
                <select name="disp_number" id="disp_number" onchange="javascript:fnChangeDispNumber(this.value);">
                    <!--{foreach from=$arrPRODUCTLISTMAX item="dispnum" key="num"}-->
                        <!--{if $num == $disp_number}-->
                            <option value="<!--{$num}-->" selected="selected" ><!--{$dispnum}--></option>
                        <!--{else}-->
                            <option value="<!--{$num}-->" ><!--{$dispnum}--></option>
                        <!--{/if}-->
                    <!--{/foreach}-->
                </select>
            </div>
        </div>
    </div>
    <div class="navi">
        <div class="product_pager">
            <ul>
                <!--{$tpl_strnavi}-->
            </ul>
        </div>
    </div>

    <!--{/capture}-->
    <!--▲ページナビ(本文)-->

	 <!--▼ページナビ(本文)-->
    <!--{capture name=page_navi_bodys}-->
        <div class="pagenumber_area number-head bord-head">
            <div class="navi">
                <div class="product_pager">
                    <ul>
                        <!--{$tpl_strnavi}-->
                    </ul>
                </div>
            </div>
        </div>

    <!--{/capture}-->
    <!--▲ページナビ(本文)-->





            <!--▼ページナビ(上部)-->
            <form name="page_navi_top" id="page_navi_top" action="?">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <!--{if $tpl_linemax > 0}--><!--{$smarty.capture.page_navi_body|smarty:nodefaults}--><!--{/if}-->
            </form>

            <!--▲ページナビ(上部)-->

<div class="product_list category">


<ul>
    <!--{foreach from=$arrProducts item=arrProduct name=arrProducts}-->

        <!--{if $smarty.foreach.arrProducts.first}-->

        <!--{/if}-->
    <!--{if $arrProduct.product_id !== null}-->
    <li>
        <!--{assign var=id value=$arrProduct.product_id}-->
        <!--{assign var=arrErr value=$arrProduct.arrErr}-->
        <!--▼商品-->
        <form name="product_form<!--{$id|h}-->" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="product_id" value="<!--{$id|h}-->" />
            <input type="hidden" name="product_class_id" id="product_class_id<!--{$id|h}-->" value="<!--{$tpl_product_class_id[$id]}-->" />
            <!--★画像★-->
            <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" class="picture" />
            </a>

            <!--★商品名★-->
            <h4>
                <!--{$arrProduct.name|h}-->
            </h4>
            <!--★価格★-->
            <div class="pricebox sale_price">
                <p class="price <!--{if $arrProduct.sale_flg === "1"}-->sale<!--{/if}-->">
                    <!--{if $arrProduct.sale_flg === "1"}-->
                        <span class="normal_price"> <!--{$arrProduct.price01_min|n2s}--> 円～ </span>
                    <!--{/if}-->
                    <span id="price02_default_<!--{$id}-->"><!--{strip}-->
                        <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                            <!--{$arrProduct.price02_min|n2s}-->円
                        <!--{else}-->
                            <!--{$arrProduct.price02_min|n2s}-->円～
                        <!--{/if}-->
                    </span>
                    <!--{/strip}--><span style="display: inline-block;">(税抜)</span>
                </p>
            </div>
        </form>
        <!--▲商品-->
	</li>
    <!--{/if}-->




    <!--{foreachelse}-->
        <!--{include file="frontparts/search_zero.tpl"}-->
    <!--{/foreach}-->

</div>

	<!--{if $smarty.foreach.arrProducts.last}-->
            <!--▼ページナビ(下部)-->
            <form name="page_navi_bottom" id="page_navi_bottom" action="?">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <!--{if $tpl_linemax > 0}--><!--{$smarty.capture.page_navi_bodys|smarty:nodefaults}--><!--{/if}-->
            </form>
            <!--▲ページナビ(下部)-->
        <!--{/if}-->

        <!--{*
        <div class="style product_list category">
            <div class="ttl">
                <img src="/shop/user_data/packages/CJoukoku/img/products/category_style_ttl.png">
            </div>
            <ul class="rank">
                <li>
                    <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_1.png"></a>
                    <h4>遮光1級付き厚地オーダーカーテン</h4>
                </li>
                <li>
                    <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_2.png"></a>
                    <h4>遮光1級付き厚地オーダーカーテン</h4>
                </li>
                <li>
                    <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_3.png"></a>
                    <h4>遮光1級付き厚地オーダーカーテン</h4>
                </li>
            </ul>
        </div>
        <div class="recenty_checked product_list category">
            <div class="ttl">
                <img src="/shop/user_data/packages/CJoukoku/img/products/category_recenty_ttl.png">
            </div>
            <ul class="rank">
                <li>
                    <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_1.png"></a>
                    <h4>遮光1級付き厚地オーダーカーテン</h4>
                </li>
                <li>
                    <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_2.png"></a>
                    <h4>遮光1級付き厚地オーダーカーテン</h4>
                </li>
                <li>
                    <a href="#"><img src="/shop/user_data/packages/CJoukoku/img/ranking/top_ranking_curtain_3.png"></a>
                    <h4>遮光1級付き厚地オーダーカーテン</h4>
                </li>
            </ul>
        </div>
        *}-->


</div>
</main>

<script type="text/javascript">
  var maxHeight = 0;
  $(".matchHeight").each(function(){
     if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
  });
  $(".matchHeight").height(maxHeight);
$(window).resize(function(){
  $(".matchHeight").each(function(){
     if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
  });
  $(".matchHeight").height(maxHeight);
});
</script>
