<div class="page-body">
		<div class="inner howpay">
			<h2>お客様各位</h2>
			<div class="img100"><img src="<!--{$TPL_URLPATH}-->img/common/f19_img_01.png"></div>

			<p style="text-align: right">2019/2/12（火）</p>
			<h3>遮光率表記誤りのお詫びとお知らせ</h3>
                        <p>平素は、株式会社カーテン・じゅうたん王国をご愛顧賜り誠にありがとうございます。
                          <br>
                          この度、弊社にて販売しております「オーダーカーテン」の一部商品におきまして遮光等級の表示に誤りがございました。
                          </p>
                        <p>お客様には多大なご迷惑をお掛けしました事を心よりお詫び申し上げます。<br>
                        今後より一層の品質管理徹底を行い、再発防止に取り組んでまいります。<br>
                        何卒、ご理解賜りますよう、お願い申し上げます。</p>
			<br><br>
                        <h3 style="text-align: center">記</h3>
<br>
			<div class="inners">

			  <h3>対象商品</h3>
                <table width="96%" cellpadding="0" cellspacing="0" border="1" style="border-collapse: collapse; padding: 10px;">
				  <col span="2">
				  <col>
				  <tr>
				    <th style="background: #e7e7e7; border: solid 1px; padding: 5px;">品番（品名）</th>
				    <th style="background: #e7e7e7; border: solid 1px; padding: 5px;">誤　遮光等級</th>
				    <th style="background: #e7e7e7; border: solid 1px; padding: 5px;">正　遮光等級</th>
			      </tr>
				  <tr>
				    <td style="border: solid 1px; padding: 5px;"><strong>SD27038</strong></td>
				    <td style="border: solid 1px; padding: 5px;">２級</td>
				    <td style="border: solid 1px; padding: 5px;">３級</td>
			      </tr>
				  <tr>
				    <td style="border: solid 1px; padding: 5px;"><strong>SD27039</strong></td>
				    <td style="border: solid 1px; padding: 5px;">２級</td>
				    <td style="border: solid 1px; padding: 5px;">３級</td>
			      </tr>
			  </table>
                <p>&nbsp;</p>

		      <h3>お問い合わせ先</h3>
				<p><strong>株式会社カーテン・じゅうたん王国　お問い合わせフォーム</strong><br>
			    <a style="color: blue" href="https://www.oukoku.co.jp/shop/contact/">https://www.oukoku.co.jp/shop/contact/</a></p>
				<p><strong>株式会社カーテン・じゅうたん王国　商品部</strong><br>
				TEL：03－5649－3571（直通）<br>
				受付時間：10：00～18：00</p>
				<p>上記窓口にてご提供頂くお客様個人情報は、本件についてのみ使用し、その他目的には、一切使用致しません。<br>
			  </p>



		  </div>



  </div><!-- page-body inner -->
</div><!-- page-body -->
