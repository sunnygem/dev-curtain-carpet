<div class="page-body">
		<div class="inner voice2">
			<h2>お客様の声</h2>
			
			<div class="img100"><img src="<!--{$TPL_URLPATH}-->img/common/e1_header_bg.png"></div>
			
			<!-- <div class="img02"><img src="<!--{$TPL_URLPATH}-->img/common/e1_img_01.png"></div> -->
			<div class="cen"><h3>お客様の声 PICKUP</h3></div>
			
			<div class="brown-area">
				<div class="inner clearfix">
					<div class="left">
						<img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_1226.jpg">
					</div>
					<div class="right">
						<h4><img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_S1226.png"></h4>
						<p>購入された商品：<br>
							アルミブラインド、バーチカルブラインド（縦型ブラインド）、ロールスクリーン</p>
							<p>受けたサービス：採寸、取付工事</p><br>
<p>Q1.購入の決め手となった事はどんなところですか？<br>
							商品が気に入ったことと、引っ越し日が間近に迫っている中で、なるべく間に合わせようと配慮してくれたところです。<br>
							<br>
							Q2.思い描いた空間となりましたでしょうか？<br>
							素敵で快適な空間になりました。打ち合わせの時間も楽しく、取付工事の方も感じが良かったです。最初から最後まで満足です。ありがとうございました。<br>
							</p>
					</div>
				</div>
			</div>


			<div class="brown-area">
				<div class="inner clearfix">
					<div class="left">
						<img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_001.jpg">
					</div>
					<div class="right">
						<h4><img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_wood.png"></h4>
						<p>購入された商品：<br>
							ウッドブラインド<br>
							<br>
				お客様の声：<br>
				家具同様大きな買い物のカーテンに、こちらの気持ちに全力で寄り添ってくださり、本当に良かったです。<br>
<br>
							スタッフのコメント：<br>									
				今はやりの「ビンテージアメリカン」を大人の雰囲気でまとめた室内空間に。ご夫婦の仲の良さも感じられました。</p>
					</div>
				</div>
			</div>

<div class="brown-area">
				<div class="inner clearfix">
					<div class="left">
						<img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_002.jpg">
					</div>
					<div class="right">
						<h4><img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_002_1.png"></h4>
						<p>購入された商品：<br>
							ドレープカーテン、レースカーテン<br>
							<br>
							お客様の声：<br>									
				本当に素敵なカーテンで気に入っています。ありがとうございました。</p>
					</div>
				</div>
			</div>

<div class="brown-area">
				<div class="inner clearfix">
					<div class="left">
						<img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_003.jpg">
					</div>
					<div class="right">
						<h4><img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_003_1.png"></h4>
						<p>購入された商品：<br>
							ドレープカーテン、レースカーテン、シェード<br>
							<br>
							お客様の声：<br>									
				カーテン取付工事が終わり、カーテンを付けると一気に家らしくなり、入居が益々楽しみになりました。素敵なカーテン選びができたのも、アドバイスあってこそです。本当にありがとうございました。リビングの装飾レールも、やりたかった雰囲気が現実になっていて、とても嬉しいです。</p>
					</div>
				</div>
			</div>

<div class="brown-area">
				<div class="inner clearfix">
					<div class="left">
						<img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_004.jpg">
					</div>
					<div class="right">
						<h4><img src="<!--{$TPL_URLPATH}-->img/common/okyakusama_004_1.png"></h4>
						<p>購入された商品：<br>
							ドレープカーテン、レースカーテン、シェード、ウッドブラインド<br>
							<br>
							お客様の声：<br>									
				先日のウッドブラインドの件、素早い対応をしていただきありがとうございました。家具などがきれいに揃えられたら、また部屋のグレードが上がるだろうなぁと思っています。とても部屋の雰囲気にピッタリのカーテンをつけられたこと、とてもうれしく思っています(^^)</p>
					</div>
				</div>
			</div>
			

<div class="brown-area">
				<div class="inner clearfix">
					<div class="left">
						<img src="<!--{$TPL_URLPATH}-->img/common/e1_img_02.png">
					</div>
					<div class="right">
						<h4><img src="<!--{$TPL_URLPATH}-->img/common/e1_img_04.png"></h4>
						<p>購入された商品：<br>
							ドレープカーテン、レースカーテン、シェード、ロールスクリーン<br>
							<br>
							受けたサービス：カーテンレールの取付工事、採寸Q1.購入した商品はいかがでしたか？<br>
							イメージしていたインテリア通りになって本当に大満足です。<br>
							<br>
							Q2.購入の決め手は何ですか？<br>
							カーテンのデザインがステキすぎたので。<br>
							<br>
							Q3.取付工事のスタッフの対応や仕上がりはいかがでしたか？<br>
							ショールームのスタッフさんがひとつひとつ丁寧な説明と私たちがイメージしていたインテリアをしっかり汲んで提案してくれたので、”夫婦で本当に良かったね”と話していました。<br>
							<br>
							取付工事のスタッフさんも丁寧に親切にやっていただいたので良かったです。</p>
					</div>
				</div>
			</div>


			<div class="sec01 clearfix">
				<div class="left">
					<dl>
						<dt><img src="<!--{$TPL_URLPATH}-->img/common/e1_img_05.png"></dt>
						<dd><h4>エキゾチックで格調高いデザインが気に入りました。</h4>
						<p class="name">G様</p>
						<p>使用された商品：モンタナ（カーペット）</p>
						<p>エキゾチックで格調高いデザインが気に入りました?光沢があり、見る角度によって色の濃さが変化！ アクリルと綿の混紡で、優しい手触りです。毛....</p></dd>
					</dl>
				</div>
				<div class="right">
					<dl>
						<dt><img src="<!--{$TPL_URLPATH}-->img/common/e1_img_06.png"></dt>
						<dd><h4>豪華なデザインがお気に入りです。</h4>
						<p class="name">G様</p>
						<p>使用された商品：モンタナ（カーペット）</p>
						<p>エキゾチックで格調高いデザインが気に入りました?光沢があり、見る角度によって色の濃さが変化！ アクリルと綿の混紡で、優しい手触りです。毛....</p></dd>
					</dl>
				</div>
			</div>
			
			<!--<div class="more"><a href="#"><img src="<!--{$TPL_URLPATH}-->img/common/e1_btn_01.png"></a></div>-->
			
			<!--<h5><img src="<!--{$TPL_URLPATH}-->img/common/e1_title_02.png"></h5>-->
			
				
				
			</div>
			
			
		

<div id="customervoice_area">
 <!--<h2>お客様の声</h2>-->
    <!--{if count($arrReview) > 0}-->
        <ul>
            <!--{section name=cnt loop=$arrReview}-->
                <li>
                    <p class="voicetitle"><!--{$arrReview[cnt].title|h}--></p>
                    <p class="voicedate"><!--{$arrReview[cnt].create_date|sfDispDBDate:false}-->　投稿者：<!--{if $arrReview[cnt].reviewer_url}--><a href="<!--{$arrReview[cnt].reviewer_url}-->" target="_blank"><!--{$arrReview[cnt].reviewer_name|h}--></a><!--{else}--><!--{$arrReview[cnt].reviewer_name|h}--><!--{/if}-->　<span class="osusumeLv">おすすめレベル：<span class="recommend_level"><!--{assign var=level value=$arrReview[cnt].recommend_level}--><!--{$arrRECOMMEND[$level]|h}--></span></span></p>
                    <p class="voicecomment"><!--{$arrReview[cnt].comment|h|nl2br}--></p>
                </li>
            <!--{/section}-->
        </ul>
    <!--{/if}-->
</div>
		</div><!-- page-body inner -->
	</div><!-- page-body -->