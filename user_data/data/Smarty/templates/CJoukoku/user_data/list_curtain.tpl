<div class="list_curtain">

    <section id="slider_area">
        <div class="slider">
            <div><a href="/shop/user_data/feature_hokuo.php"><img src="<!--{$TPL_URLPATH}-->img/top/slider_hokuo.jpg"                alt="" height="auto" class="lazy"></a></div>
            <div><a href="/shop/user_data/feature_petit_flower.php"><img src="<!--{$TPL_URLPATH}-->img/top/slider_petit_flower.jpg"  alt="" height="auto" class="lazy"></a></div>
            <div><a href="/shop/user_data/feature_botanical.php"><img src="<!--{$TPL_URLPATH}-->img/top/slider_botanical.jpg"        alt="" height="auto" class="lazy"></a></div>
            <div><a href="/shop/user_data/feature_otokomae.php"><img src="<!--{$TPL_URLPATH}-->img/top/slider_otokomae.jpg"          alt="" height="auto" class="lazy"></a></div>
            <div><a href="/shop/products/list.php?category_id=1821"><img src="<!--{$TPL_URLPATH}-->img/top/slider_shade.jpg"         alt="" height="auto" class="lazy"></a></div>
            <div><a href="/shop/products/list.php?category_id=1823"><img src="<!--{$TPL_URLPATH}-->img/top/slider_energy_saving.jpg" alt="" height="auto" class="lazy"></a></div>
        </div><!--/.mainBanner -->

        <p class="description">カーテンじゅうたん王国は、お手頃価格で豊富なデザインのカーテンがそろっています。<br>テイストやカラー、機能、種類などからカーテンをお選びください。</p>

    </section><!--/#slider_area -->

    <section class="kodawari">
        <a href="/shop/user_data/research.php">
            <p class="search_button"><img src="<!--{$TPL_URLPATH}-->img/banner/curtain_search_banner.png" alt="こだわり検索はこちらから"></p>
        </a>
        <p class="description"><span>『ブルー系の遮光カーテン』<span>『ブラウン系でクラシックなデザイン』など、<br>こだわりの複合検索が可能です！</p>
    </section><!--/.kodawari -->

    <section class="function">
        <h2><span>機能で選ぶ</span></h2>
        <div class="flex_wrap">
            <div class="fn_drape">
                <h3>
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1792"><img src="<!--{$TPL_URLPATH}-->img/banner/side_drape_img.jpg" alt="ドレープ"></a>
                <!--{*<p class="description">王国のドレープ（厚地）カーテンは遮光や防炎といった機能性はもちろん、独自の形状記憶加工により美しいドレープを保ちます。</p>*}-->
                </h3>
                <ul>
                <!--{foreach from=$arrCategoryFunctionDrape item=val}-->
                    <li>
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                            <div class="category_thumb" style="<!--{if strlen( $val.thumbnail_path ) > 0}--> background-image: url(<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->); <!--{/if}-->"></div>
                            <span class="category_name"><!--{$val.category_name}--></span>
                        </a>
                    </li>
                <!--{/foreach}-->
                </ul>
            </div>
            <div class="fn_lace">
                <h3>
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=1793"><img src="<!--{$TPL_URLPATH}-->img/banner/side_lace_img.jpg" alt="レース"></a>
                </h3>
                <!--{*<p class="description">王国のレースカーテンは遮熱やUVカット、プライバシー対策など機能に優れたレースやデザインにこだわったレースなど豊富にそろっています。 </p>*}-->
                <ul>
                <!--{foreach from=$arrCategoryFunctionLace item=val}-->
                    <li>
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                            <div class="category_thumb" style="<!--{if strlen( $val.thumbnail_path ) > 0}--> background-image: url(<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->); <!--{/if}-->"></div>
                            <div class="category_name"><!--{$val.category_name}--></div>
                        </a>
                    </li>
                <!--{/foreach}-->
                </ul>
            </div>
        </div>
    </section><!--/.function -->

    <section class="color_type">
        <h2><span>色で選ぶ</span></h2>
        <div>
            <ul>
                <!--{foreach from=$arrCategoryColor item=val}-->
                    <li>
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                            <!--{if strlen( $val.thumbnail_path ) > 0}-->
                                <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                            <!--{/if}-->
                            <span class="category_name"><!--{$val.category_name}--></span>
                        </a>
                    </li>
                <!--{/foreach}-->
            </ul>
        </div>
    </section><!--/.color_type -->

    <section class="taste">
        <h2><span>テイストで選ぶ</span></h2>
        <div>
            <ul>
                <!--{foreach from=$arrCategoryTaste item=val}-->
                    <li>
                        <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                            <div class="category_name"><!--{$val.category_name}--></div>
                        </a>
                    </li>
                <!--{/foreach}-->
            </ul>
        </div>
    </section><!--/.taste -->

    <section class="coordinate">
        <h2>
            <img src="<!--{$TPL_URLPATH}-->img/top/top_coodinate_main.jpg" alt="王国スタイル" style="width: 100%;">
        </h2>
        <ul class="oukoku_style">
            <li>
                <a href="/shop/products/list.php?category_id=1851">
                    <div class="o_style_wrap"> <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_pastel.jpg" alt=""> </div>
                    <div class="color"> <img src="<!--{$TPL_URLPATH}-->img/curtain/curtain_coordinate_pastel_color.png" alt=""> </div>

                </a>
            </li>
            <li>
                <a href="/shop/products/list.php?category_id=1851">
                    <div class="o_style_wrap"> <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_colorful.jpg" alt=""> </div>
                    <div class="color"> <img src="<!--{$TPL_URLPATH}-->img/curtain/curtain_coordinate_colorful_color.png" alt=""> </div>
                </a>
            </li>
            <li>
                <a href="/shop/products/list.php?category_id=1853">
                    <div class="o_style_wrap"> <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_blue.jpg" alt=""> </div>
                    <div class="color"> <img src="<!--{$TPL_URLPATH}-->img/curtain/curtain_coordinate_blue_color.png" alt=""> </div>
                </a>
            </li>
            <li>
                <a href="/shop/products/list.php?category_id=1854">
                    <div class="o_style_wrap"> <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_modern.jpg" alt=""> </div>
                    <div class="color"> <img src="<!--{$TPL_URLPATH}-->img/curtain/curtain_coordinate_modern_color.png" alt=""> </div>
                </a>
            </li>
            <li>
                <a href="/shop/products/list.php?category_id=1855">
                    <div class="o_style_wrap"> <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_soft.jpg" alt=""> </div>
                    <div class="color"> <img src="<!--{$TPL_URLPATH}-->img/curtain/curtain_coordinate_soft_color.png" alt=""> </div>
                </a>
            </li>
            <li>
                <a href="/shop/products/list.php?category_id=1856">
                    <div class="o_style_wrap"> <img src="<!--{$TPL_URLPATH}-->img/top/top_coordinate_luxury.jpg" alt=""> </div>
                    <div class="color"> <img src="<!--{$TPL_URLPATH}-->img/curtain/curtain_coordinate_luxury_color.png" alt=""> </div>
                </a>
            </li>
        </ul><!--/.oukoku_style -->
    </section><!--/.cordinate -->

    <section class="curtain_type">
        <h2><span>種類から選ぶ</span></h2>
        <ul>
            <!--{foreach from=$arrCategoryType item=val}-->
                <li class="">
                    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$val.category_id}-->">
                        <!--{if strlen( $val.thumbnail_path ) > 0}-->
                            <div class="category_thumb"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$val.thumbnail_path}-->" alt="<!--{$val.category_name}-->"></div>
                        <!--{/if}-->
                        <div class="category_name"> <!--{$val.category_name}--> </div>
                    </a>
                </li>
            <!--{/foreach}-->
        </ul>
    </section><!--/.curtain_type -->

    <!--{* 商品一覧は非表示

    <form name="form1" id="form1" method="get" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="<!--{$mode|h}-->" />
        <!--{ ▼検索条件 }-->
        <input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" />
        <!-- input type="hidden" name="category" value="<!--{$arrSearchData.category|h}-->" / -->
        <input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
        <input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" />
        <!--{ ▲検索条件 }-->
        <!--{ ▼ページナビ関連 }-->
        <input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
        <input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
        <input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
        <!--{ ▲ページナビ関連 }-->
        <input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />
    </form>

    <!--▼ページナビ(本文)-->
    <!--{capture name=page_navi_body}-->
    <div class="pagenumber_area number-head">
        <div class="change">
            <!--{if $orderby != 'date'}-->
                <a href="javascript:fnChangeOrderby('date');">新着順</a>
            <!--{else}-->
                <span>新着順</span>
            <!--{/if}-->&nbsp;
            <!--{if $orderby != "price_h"}-->
                    <a href="javascript:fnChangeOrderby('price_h');">価格の高い順</a>
            <!--{else}-->
                <span>価格の高い順</span>
            <!--{/if}-->&nbsp;
            <!--{if $orderby != "price"}-->
                    <a href="javascript:fnChangeOrderby('price');">価格の低い順</a>
            <!--{else}-->
                <span>価格の低い順</span>
            <!--{/if}-->
        </div>
        <div class="disp_result">
            <label>表示件数</label>
            <div class="select_wrap">
                <select name="disp_number" id="disp_number" onchange="javascript:fnChangeDispNumber(this.value);">
                    <!--{foreach from=$arrPRODUCTLISTMAX item="dispnum" key="num"}-->
                        <!--{if $num == $disp_number}-->
                            <option value="<!--{$num}-->" selected="selected" ><!--{$dispnum}--></option>
                        <!--{else}-->
                            <option value="<!--{$num}-->" ><!--{$dispnum}--></option>
                        <!--{/if}-->
                    <!--{/foreach}-->
                </select>
            </div>
        </div>
    </div><!--/.pagenumber_area -->

    <div class="navi">
        <div class="product_pager">
            <ul>
                <!--{$tpl_strnavi}-->
            </ul>
        </div>
    </div><!--/.navi -->

    <!--{/capture}-->
    <!--▲ページナビ(本文)-->

    <!--▼ページナビ(本文)-->
    <!--{capture name=page_navi_bodys}-->
        <div class="pagenumber_area number-head bord-head">
            <div class="navi">
                <div class="product_pager">
                    <ul>
                        <!--{$tpl_strnavi}-->
                    </ul>
                </div>
            </div>
        </div>
    <!--{/capture}-->
    <!--▲ページナビ(本文)-->


    <!--▼ページナビ(上部)-->
    <form name="page_navi_top" id="page_navi_top" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <!--{if $tpl_linemax > 0}--><!--{$smarty.capture.page_navi_body|smarty:nodefaults}--><!--{/if}-->
    </form>
    <!--▲ページナビ(上部)-->

    <div class="product_list category">

        <ul>
        <!--{foreach from=$arrProducts item=arrProduct name=arrProducts}-->

            <!--{if $smarty.foreach.arrProducts.first}-->

            <!--{/if}-->
            <li>
                <!--{assign var=id value=$arrProduct.product_id}-->
                <!--{assign var=arrErr value=$arrProduct.arrErr}-->
                <!--▼商品-->
                <form name="product_form<!--{$id|h}-->" action="?">
                    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                    <input type="hidden" name="product_id" value="<!--{$id|h}-->" />
                    <input type="hidden" name="product_class_id" id="product_class_id<!--{$id|h}-->" value="<!--{$tpl_product_class_id[$id]}-->" />
                    <!--★画像★-->
                    <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" class="picture" />
                    </a>

                    <!--★商品名★-->
                    <h4><!--{$arrProduct.name|h}--></h4>
                    <!--★価格★-->
                    <div class="pricebox sale_price">
                        <p class="price">
                            <span id="price02_default_<!--{$id}-->"><!--{strip}-->
                                <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                                    <!--{$arrProduct.price02_min|n2s}-->
                                <!--{else}-->
                                    <!--{$arrProduct.price02_min|n2s}-->円～
                                <!--{/if}-->
                            </span>
                            <!--{/strip}-->(税別)
                        </p>
                    </div>
                </form>
                <!--▲商品-->
            </li>

        <!--{foreachelse}-->
            <!--{include file="frontparts/search_zero.tpl"}-->
        <!--{/foreach}-->
        </ul>

    </div><!--/.product_list -->

    <!--{if $smarty.foreach.arrProducts.last}-->
        <!--▼ページナビ(下部)-->
        <form name="page_navi_bottom" id="page_navi_bottom" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <!--{if $tpl_linemax > 0}--><!--{$smarty.capture.page_navi_bodys|smarty:nodefaults}--><!--{/if}-->
        </form>
        <!--▲ページナビ(下部)-->
    <!--{/if}-->

    商品非表示ここまで *}-->

<!--{strip}-->
    <!--{if count($arrBestProducts) > 0}-->
    <section class="block_outer recommend">
        <h2><span>おすすめ商品</span></h2>
        <div class="product_list category">
            <ul>
            <!--{foreach from=$arrBestProducts item=arrProduct name="recommend_products"}-->
                <li>
                    <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" />
                    </a>
                    <h4> <!--{$arrProduct.name|h}--> </h4>
                    <div class="pricebox sale_price">
                        <p class="price <!--{if $arrProduct.sale_flg === "1"}-->sale<!--{/if}-->">
                            <!--{if $arrProduct.sale_flg === "1"}-->
                                <span class="normal_price"> <!--{$arrProduct.price01_min|n2s}--> 円～ </span>
                            <!--{/if}-->
                            <span class="price">
                                 <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                                     <!--{$arrProduct.price02_min|n2s}-->円
                                 <!--{else}-->
                                     <!--{$arrProduct.price02_min|n2s}-->円～
                                 <!--{/if}-->
                            </span>
                            <!--{*$arrProduct.comment|h|nl2br*}-->
                            <span style="display: inline-block;">(税抜)</span>
                        </p>
                    </div>
                </li>
            <!--{/foreach}-->
            <ul>
        </div>
    </section>
    <!--{/if}-->
<!--{/strip}-->

<!--{strip}-->
    <section class="block_outer new_item">
        <h2><span>新着商品</span></h2>
        <div class="product_list category">
            <ul>
            <!--{foreach from=$arrNewItems item=arrProduct name="recommend_products" key="key"}-->
            <!--{if $key > 0 }-->
                <li>
                    <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" />
                    </a>
                    <h4> <!--{$arrProduct.name|h}--> </h4>
                    <div class="pricebox sale_price">
                        <p class="price <!--{if $arrProduct.sale_flg === "1"}-->sale<!--{/if}-->">
                            <!--{if $arrProduct.sale_flg === "1"}-->
                                <span class="normal_price"> <!--{$arrProduct.price01_min|n2s}--> 円～ </span>
                            <!--{/if}-->
                            <span class="price">
                                 <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                                     <!--{$arrProduct.price02_min|n2s}-->円
                                 <!--{else}-->
                                     <!--{$arrProduct.price02_min|n2s}-->円～
                                 <!--{/if}-->
                            </span>
                            <!--{*$arrProduct.comment|h|nl2br*}-->
                            <span style="display: inline-block;">(税抜)</span>
                        </p>
                    </div>
                </li>
            <!--{/if}-->
            <!--{/foreach}-->

            <ul>
        </div>
    </section>
<!--{/strip}-->

    <!--{if $arrCheckItems}-->
    <section class="product_related check_item">
        <h2><span>最近、チェックした商品</span></h2>
        <div class="">
            <ul>
            <!--{section name=cnt loop=$arrCheckItems}-->
                <li>
                    <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrCheckItems[cnt].product_id}-->">
                        <img src="<!--{$smarty.const.ROOT_URLPATH}-->resize_image.php?image=<!--{$arrCheckItems[cnt].main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrCheckItems[cnt].name|h}-->"/>
                    </a>
                    <h4><!--{$arrCheckItems[cnt].name}--></h4>
                    <div class="pricebox sale_price">
                        <p class="price <!--{if $arrCheckItems[cnt].sale_flg === "1"}-->sale<!--{/if}-->">
                        <!--{if $arrCheckItems[cnt].sale_flg ===
                        "1"}--> <span class="normal_price"> <!--{$arrCheckItems[cnt].price01_min|n2s}--> 円～ </span> <!--{/if}-->
                        <span id="price02_default_<!--{$id}-->"><!--{strip}-->
                        <!--{if $arrCheckItems[cnt].price02_min == $arrCheckItems[cnt].price02_max}-->
                            <!--{$arrCheckItems[cnt].price02_min|n2s}-->円
                        <!--{else}-->
                            <!--{$arrCheckItems[cnt].price02_min|n2s}-->円～
                        <!--{/if}-->
                        </span>
                        <!--{/strip}--><span style="display: inline-block;">(税抜)</span>
                        </p>
                    </div><!--/.pricebox-->
                </li>
            <!--{/section}-->
            </ul>
        </div>
    </section> <!-- .product_related -->
    <!--{/if}-->

<!--{*
    <section class="manufacture">
        <h2><span>ブランドから選ぶ</span></h2>
        <div class="">
            <img src="<!--{$TPL_URLPATH}-->img/common/n_logo_01.png">
        </div>

    </section><!--/.manufacture -->
*}-->

</div><!--/.list_curtain -->
