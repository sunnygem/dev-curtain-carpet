<div class="page sub_contents advice">
    <div class="ttl">
        <h2><span></span>商品選びに困ったら</h2>
    </div>
    <p class="mb30">
    インテリアを選ぶ上でまず、家の窓周辺の環境はどうなっているかを確認することは大切です。昼間と夜間では、窓からの影響はどんな違いがあるでしょうか。例えば、日中は気にならなくても、夜になると街灯がちらつく、朝方が眩しい、など、1日・季節を通して考えてみる必要があります。そこで、カーテンじゅうたん王国でよくお問い合わせ・ご相談いただく代表的なニーズをご紹介します。
    </p>
        <div class="section">
            <div class="step">
                <ul>
                    <li><a href="#step1"><p class="step_no"><span>Step</span>1</p><p class="txt">部屋の用途を<br>考える</p><span class="arrow"></span></a></li>
                    <li><a href="#step2"><p class="step_no"><span>Step</span>2</p><p class="txt">窓の形状を<br>確認</p><span class="arrow"></span></a></li>
                    <li><a href="#step3"><p class="step_no"><span>Step</span>3</p><p class="txt">窓の特徴から<br>絞り込み</p><span class="arrow"></span></a></li>
                    <li><a href="#step4"><p class="step_no"><span>Step</span>4</p><p class="txt">デザインから<br>選ぶ</p><span class="arrow"></span></a></li>
                    <li><a href="#step5"><p class="step_no"><span>Step</span>5</p><p class="txt">機能から<br>選ぶ</p><span class="arrow"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="section advice_step1" id="step1">
            <h3><span></span>部屋の用途を考える</h3>
            <p class="mb20">
            部屋によって用途は違います。例えば代表的な例を見てみましょう。
            </p>
            <div class="flex mb10">
                <div>
                    <h4>リビング</h4>
                    <p>
                    人が集まるリビングは、お家の顔になります。<br>
                    流行ものを取り入れるより、長くあきのこないデザインを選びましょう。
                    </p>
                </div>
                <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_livingroom.jpg"></div>
            </div>
            <div class="flex mb10">
                <div>
                    <h4>寝室</h4>
                    <p>
                    リラックスできる空間づくりには、好きなカラーを取り入れて。
                    </p>
                </div>
                <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_bedroom.jpg"></div>
            </div>
            <div class="flex mb10">
                <div>
                    <h4>子供部屋</h4>
                    <p>
                        成長に合せてカラー・模様替えをしていくことも念頭に置き、先ずはスタイル（カーテン・ブラインド・ロールスクリーン）を決めましょう。
                    </p>
                </div>
                <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_childroom.jpg"></div>
            </div>
            <div class="flex mb10">
                <div>
                    <h4>書斎・プライベートルーム</h4>
                    <p>
                    どんなことをするお部屋か（音楽を聴く・シアタールームなど）を思い浮かべてください。遮光や防音機能など、必要な機能を考えておくとよいでしょう。

                    </p>
                </div>
                <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_privateroom.jpg"></div>
            </div>
            <div class="point_area">
                <!-- ポイントエリア -->

                <p>【ポイント】こうしたい！という要望をリスト化してみましょう！</p>
                <p>
                カーテンの役割を決めてください。こうしたい！この機能も欲しい！などたくさんの夢が膨らむと思います。その思いを一つひとつリスト化して書き出しましょう。
                </p>
            </div>
        </div>
        <div class="section" id="step2">
            <h3><span></span>窓の形状を確認する</h3>
            <div>
                <p class="mb20">
                窓の形状によってつける事ができるカーテンや用途を絞ることが出来ます。
                </p>    
            </div>
            <div class="w_list">
                <div>
                    <h4>縦スリット窓</h4>
                    <div class="flex">
                        <div>
                            <p>幅の狭い細長い窓</p>
                        </div>
                        <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_shapewin01.jpg"></div>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <th>おすすめ度</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>カーテン</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>シェード</th>
                                <td>△</td>
                            </tr>
                            <tr>
                                <th>ブラインド</th>
                                <td>○</td>
                            </tr> 
                            <tr>
                                <th>ロールスクリーン</th>
                                <td>○</td>
                            </tr> 
                            <tr>
                                <th>バーチカルブラインド</th>
                                <td></td>
                            </tr> 
                        </table>
                    </div>
                </div>


                <div>
                    <h4>腰高2連窓</h4>
                    <div class="flex">
                        <div>
                            <p>              
                            同じサイズで並んでいる腰高の窓。3連などもある。
                            </p>
                        </div>
                        <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_shapewin02.jpg"></div>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <th>おすすめ度</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>カーテン</th>
                                <td>○</td>
                            </tr>
                            <tr>
                                <th>シェード</th>
                                <td>○</td>
                            </tr>
                            <tr>
                                <th>ブラインド</th>
                                <td>○</td>
                            </tr> 
                            <tr>
                                <th>ロールスクリーン</th>
                                <td>○</td>
                            </tr> 
                            <tr>
                                <th>バーチカルブラインド</th>
                                <td>○</td>
                            </tr> 
                        </table>
                    </div>
                </div>
                <div>
                    <h4>横長窓</h4>
                    <div class="flex">
                        <div>
                            <p>              
                            掃出し窓と同じ位の高さからあるが、丈が30cm前後と短い窓。
                            </p>
                        </div>
                        <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_shapewin03.jpg"></div>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <th>おすすめ度</th>
                                <td></td>
                                </tr>
                                <tr>
                                <th>カーテン</th>
                                <td>○</td>
                                </tr>
                                <tr>
                                <th>シェード</th>
                                <td>○</td>
                                </tr>
                                <tr>
                                <th>ブラインド</th>
                                <td>○</td>
                                </tr> 
                                <tr>
                                <th>ロールスクリーン</th>
                                <td>○</td>
                                </tr> 
                                <tr>
                                <th>バーチカルブラインド</th>
                                <td></td>
                            </tr> 
                        </table>
                    </div>
                </div>
                <div>
                    <h4>掃出し窓・欄間</h4>
                    <div class="flex">
                        <div>
                            <p>              
                            ベランダへの出入りができる、床まである窓
                            </p>
                        </div>
                        <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_shapewin04.jpg"></div>
                    </div>
                    <div>
                        <table>
                        <tr>
                        <th>おすすめ度</th>
                        <td></td>
                        </tr>
                        <tr>
                        <th>カーテン</th>
                        <td>○</td>
                        </tr>
                        <tr>
                        <th>シェード</th>
                        <td>○</td>
                        </tr>
                        <tr>
                        <th>ブラインド</th>
                        <td>○</td>
                        </tr> 
                        <tr>
                        <th>ロールスクリーン</th>
                        <td>○</td>
                        </tr> 
                        <tr>
                        <th>バーチカルブラインド</th>
                        <td>○</td>
                        </tr> 
                        </table>
                    </div>
                </div>
                <div>
                    <h4>腰高窓</h4>
                    <div class="flex">
                        <div>
                            <p>              
                            腰くらいの高さまである窓
                            </p>
                        </div>
                        <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_shapewin05.jpg"></div>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <th>おすすめ度</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>カーテン</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>シェード</th>
                                <td>○</td>
                            </tr>
                            <tr>
                                <th>ブラインド</th>
                                <td>○</td>
                            </tr> 
                            <tr>
                                <th>ロールスクリーン</th>
                                <td>○</td>
                            </tr> 
                            <tr>
                                <th>バーチカルブラインド</th>
                                <td></td>
                            </tr> 
                        </table>
                    </div>
                </div>
                <div>
                    <h4>出窓</h4>
                    <div class="flex">
                        <div>
                            <p>              
                            壁よりも室外に出っ張っている窓
                            </p>
                        </div>
                        <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_shapewin06.jpg"></div>
                    </div>
                    <div>
                        <table>
                            <tr>
                            <th>おすすめ度</th>
                            <td></td>
                            </tr>
                            <tr>
                            <th>カーテン</th>
                            <td>○</td>
                            </tr>
                            <tr>
                            <th>シェード</th>
                            <td>○</td>
                            </tr>
                            <tr>
                            <th>ブラインド</th>
                            <td>○</td>
                            </tr> 
                            <tr>
                            <th>ロールスクリーン</th>
                            <td>○</td>
                            </tr> 
                            <tr>
                            <th>バーチカルブラインド</th>
                            <td>△</td>
                            </tr> 
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="step3">
            <h3><span></span>窓の特徴から商品を絞り込みます</h3>
            <div>
                <p>部屋の窓に合うカテゴリからさらに要望に合っているカテゴリの目星をつけます</p>
            </div>    
            <div class="flex mb10">
                <div>
                    <h4>カーテン</h4>
                    <ul>
                        <li>レールに吊るすだけなので一番手軽。ほとんどの生地が自宅で洗える。生地は取り外して洗える。</li>
                    </ul>
                </div>
            </div>
            <div>
                <h4>シェード</h4>
                <div>
                    <ul>
                    <li>カーテン生地を使い、上にたくしあげていく。降ろしておくと、カーテンの絵柄が映え、タペストリーのようなフラットさ。</li>
                    </ul>
                </div>
            </div>
            <div>
                <h4>ブラインド</h4>
                <div>
                    <ul>
                    <li>ヨコ型の羽を角度調節して光量をコントロールする。アルミ、ウッド素材などがある。無機質な印象がある。</li>
                    <li>浴室、キッチンなどの水場・火元の近くでも使用できる</li>
                    </ul>
                </div>
            </div>
            <div>
                <h4>ロールスクリーン</h4>
                <div>
                    <ul>
                        <li>上に巻き上がっていくので、シェードに比べて上部のたまり（たまりしろ）が少なく、すっきり納まる。</li>
                        <li>シェード同様、降ろしている時はタペストリーのようなフラットさ。操作も比較的シンプルで扱いやすい</li>
                    </ul>
                </div>
            </div>
            <div class="mb30">
                <h4>バーチカルブラインド</h4>
                <div>
                    <ul>
                        <li>縦型の羽でスタイリッシュに空間を見せることができる。カーテンのような開け方ができる（真ん中分け、片開きなど）</li>
                        <li>開けた時にたたみしろが多くなるので、巾広の窓におすすめ。羽が揺れやすいので、はめ殺しの窓や締切りの窓におすすめ。</li>
                    </ul>
                </div>
            </div>
            <p class="fs12 mb10">
                ※上記は、それぞれ「窓一つ」に対してのおすすめアイテム目安になります。連窓になっていれば、窓枠を覆うように大きく取付（正面付）をすることで、たくさんの中からお選びが可能になります。
            </p>

            <div class="point_area">
                <!-- ポイントエリア -->

                <p>【ポイント】</p>
                <p>
                    Step1で出した要望とStep2の窓の形状からあう商品から絞りましょう。選びにくい場合は要望に順位をつけておくと後で選びやすくなります。
                </p>
            </div>
        </div>
        <div class="section" id="step4">
            <h3><span></span>デザインから選びましょう</h3>
            <p>
                お部屋全体のイメージはお持ちでしょうか？色や柄からピンポイントでカーテンを探す前に、最初はお部屋のテイスト（モダン・カジュアル・エレガンス etc）を決め、次にスタイル（カーテン・シェード・ロールスクリーン etc）とアイテムをセレクトしていくと選択が挟まれずに上手にインテリアを選ぶことができます。ここでは、一番イメージしやすいように、カーテンをメインに、その他メカ物をご紹介します。
            </p>
            <div>
                <h4>モダン</h4>
                <div class="flex">
                    <div>
                        <p>
                            スタイリッシュさを感じさせるならモダンテイストがおすすめです。クールな印象を与えるので男性にも人気のテイストです。
                            シャープな印象のバーチカルブラインドもおすすめです
                        </p>
                        <p style="text-align:right"><a href="/shop/products/list.php?category_id=1779">&rarr;商品一覧</a></p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_modern01.jpg"></div>
                </div>
            </div>
            <div>
                <h4>カジュアル</h4>
                <div class="flex">
                    <div>
                        <p>
                            明るくポップな色使いが特徴です。お部屋全体の主役になる色柄になります。
                            色が豊富にあるバーチカルブラインドやロールスクリーンがおすすめです。
                        </p>
                        <p style="text-align:right"><a href="/shop/products/list.php?category_id=1782">&rarr;商品一覧</a></p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_casual01.jpg"></div>
                </div>
            </div>
            <div>
                <h4>エレガンス</h4>
                <div class="flex">
                    <div>
                        <p>
                            光沢のある生地や刺繍を使うことが多く、華やかかつ高級感のある雰囲気になります。<br>
                            生地でスタイルを変えてあげることをおすすめします。（クロスオーバーやバルーンシェードなど）
                        </p>
                        <p style="text-align:right"><a href="/shop/products/list.php?category_id=1780">&rarr;商品一覧</a></p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_elegance01.jpg"></div>
                </div>
            </div>
            <div>
                <h4>クラシック</h4>
                <div class="flex">
                    <div>
                        <p>
                        伝統的でデザインと重厚感のある色が特徴です。床材が濃いめのダーク系によく合います。<br>
                        落ち着いたダークカラーのウッドブラインドも家具との相性抜群です。
                        </p>
                        <p style="text-align:right"><a href="/shop/products/list.php?category_id=1781">&rarr;商品一覧</a></p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_classic01.jpg"></div>
                </div>
            </div>
            <div>
                <h4>ナチュラル</h4>
                <div class="flex">
                    <div>
                        <p>
                        天然目の床材・家具をお使いでしたら、こういったシンプルで素材感のあるテイストがおすすめです。<br>
                        天然目を使ったウッドブラインドや、素材感のあるプリーツスクリーンがおすすめです。
                        </p>
                        <p style="text-align:right"><a href="/shop/products/list.php?category_id=1778">&rarr;商品一覧</a></p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_natural01.jpg"></div>
                </div>
            </div>
        </div>  


        <div class="section" id="step5">
            <img src="">
            <h3><span></span>機能から選びましょう</h3>
            <div class="p_link">
            <!-- tab? -->
                <ul>
                <li><a href="#insulation"><p class="func">断熱性</p><p class="cap">冬の寒さや夏の暑さをどうにかしたい</p></a></li>
                <li><a href="#privacy"><p class="func">プライバシー保護</span><p class="cap">昼間は明るく過ごしたい</p></a></li>
                <li><a href="#shading"><p class="func">遮光性</span><p class="cap">光漏れや外の光かりをどうにかしたい</p></a></li>
                <li><a href="#maintenance"><p class="func">メンテナンス性</span><p class="cap">お手入れメンテナンスは楽にしたい</p></a></li>
                </ul>
            </div>
            <div id="insulation">
                <div>
                    <h4>断熱性にすぐれているカーテン</h4>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;ハニカムスクリーン</h5>
                        <p>ハチの巣構造(ハニカム)で、外からの外気に左右されにくい窓辺になります。<br>
                        カーテン以外でお探しの方におすすめです。</p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_heat01.jpg"></div>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;遮熱・断熱カーテン</h5>
                        <p>カーテンを選ぶ際に、こちらのアイコンをご参照下さい。夏は熱気の侵入を防ぎ、冬は室内の暖を逃がしにくい糸を使ったカーテンになります。</p>
                   </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_heat02.jpg"></div>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;裏地付きカーテン</h5>
                        <p>
                            気に入った生地に遮光や遮熱機能がなくても、オプションで遮光付の裏地をおつけすることもできます。
                            2重にすることでこちらも外気に左右されにくくなります。
                        </p>
                       <p style="text-align:right"><a href="/shop/products/list.php?category_id=1824">&rarr;商品一覧</a></p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_heat03.jpg"></div>
                </div>
            </div>
            <div id="privacy">
                <div>
                    <h4>プライバシー保護にすぐれているカーテン</h4>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;採光レース</h5>
                        <p>外からの光を広範囲に拡散する特殊糸を使用したレースです。</p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_privacy01.jpg"></div>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;調光ロールスクリーン</h5>
                        <p>厚地とレースを組合わせたボーダー生地のスクリーンなら、優しく光とプライバシーをコントロールできます。</p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_privacy02.jpg"></div>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;プリーツスクリーン（ペアタイプ）</h5>
                        <p>
                        プリーツ状のスクリーン。レースと厚地の切り替えを好きな位置で自由に設定できます。上からの視線が気になる場合は上部を厚地にするのがおすすめです。
                        </p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_privacy03.jpg"></div>
                </div>
            </div>


            <div id="shading">
                <div>
                    <h4>遮光性にすぐれているカーテン</h4>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;厚地カーテン</h5>
                        <p>
                        12色から選べるシームレス遮光1級カーテン。巾の大きな窓でも、生地につなぎ目が入らないので、針穴からの光漏れの心配がありません。
                        </p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_shade01.jpg"></div>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;遮光ロールスクリーン</h5>
                        <p>
                        遮光機能が付いたロールスクリーン。窓枠を覆う正面付きで取付けをすれば、左右の隙間からの光漏れも少なくなります。
                        </p> 
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_shade02.jpg"></div>
                </div>
            </div>

            <div id="maintenance">
                <div>
                    <h4>メンテナンス性に優れているカーテン</h4>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;アルミブラインド</h5>
                        <p>
                        浴室やキッチンなどの水場に適した素材です。「フッ素コート」を付けることで、さらに汚れが落としやすい・メンテナンスがしやすくなります。
                        </p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_maintenance01.jpg"></div>
                </div>
                <div class="flex">
                    <div>
                        <h5>&sdot;カーテン</h5>
                        <p>洗濯機で洗えます</p> 
                        <p style="text-align:right"><a href="/shop/products/list.php?category_id=1706">&rarr;商品一覧</a></p>
                    </div>
                    <div class="img"><img src="<!--{$TPL_URLPATH}-->img/advice/img_maintenance02.jpg"></div>
                </div>
            </div>
        </div>
    </div>
