<div class="page first">
		<div class="inner first">
            <div class="ttl_img">
                <img src="<!--{$TPL_URLPATH}-->img/first/kodawari_img_01.jpg" alt="初めての方へ">
                <h2>初めての方へ</h2>
            </div>
			
			<div class="section">
                <h3>カーテンじゅうたん王国とは？</h3>
                <p>わたしたちカーテン・じゅうたん王国は、創業51年を越える老舗のカーテン、じゅうたんの専門店です。<br>
                地域の皆様の信頼の実績として全国87店舗を構え、半世紀にわたってサービスをご提供させていただく、まさにお客様にとっての「王国」でございます。<br>
                カーテン・じゅうたん王国オンラインショップでは、店舗と同じクオリティの商品をいつでもお買い求めいただくことが出来ます。おすすめの季節商品や、コーディネートなど様々な情報をご提供し、お客様にとって「他にはないお部屋作り」をご提供いたします。</p>
			</div>

            <div class="first_line">
                <img src="<!--{$TPL_URLPATH}-->img/first/pc_kodawari_img_02.jpg">
            </div>

			<div class="section">
                <h3>品質へのこだわり</h3>
                <div class="flex">
                    <div class="txt">
                        カーテンを永くお使いいただけるよう、こだわりの品質でお作りしております。特に、お客様に好評なのが当店の形状記憶加工です。形状記憶加工とは熱処理を行うことで、美しいS字状のプリーツを保つ加工です。当店では大型の真空釜を使用することで繊維内部の空気を抜き、深部まで熱を均一に加えることで生地本来の柔らかさを活かした美しいウェーブを生み出します。ご利用になる環境によって異なりますが、5回程度の洗濯にも対応しております。
                    </div>
                    <div class="img">
                        <img src="<!--{$TPL_URLPATH}-->img/first/first_img01.jpg" alt="品質へのこだわり">
                    </div>
                </div>
			</div>

            <div class="first_line">
                <img src="<!--{$TPL_URLPATH}-->img/first/pc_kodawari_img_03.jpg">
			</div>

			<div class="section">
                <h3>お買い求めいただきやすい価格でのご提供</h3>
                <p>カーテン・じゅうたん王国は創業以来、「カーテン」と「じゅうたん」でみなさまのインテリアライフを提案している国内最大級の専門店チェーンです。店舗でもオンラインショップでも大量仕入れのメリットを活かした価格にてご提供させて頂いております。</p>
			</div>

            <div class="first_line">
                <img src="<!--{$TPL_URLPATH}-->img/first/pc_kodawari_img_04.jpg">
			</div>

			<div class="section">
                <h3>店舗でもお待ちしております</h3>
                <div class="flex">
                    <div class="txt">
                        カーテン・じゅうたん王国は全国に87店舗展開しております。店舗では実物サンプルを見ながらのご提案やお見積り、販売を行っております。カーテンやブラインド、ロールスクリーン、じゅうたんやラグのことまで何でもお気軽に相談できるお店です。
                    </div>
                    <div class="img">
                        <img src="<!--{$TPL_URLPATH}-->img/first/first_img02.jpg" alt="店舗でもお待ちしております">
                    </div>
                </div>
			</div>

            <div class="first_line">
                <img src="<!--{$TPL_URLPATH}-->img/first/pc_kodawari_img_5.jpg">
			</div>

			<div class="section">
                <h3>カーテンの生地サンプルをお送りします</h3>
                <div class="flex">
                    <div class="txt">
                        「オンラインショップの画面だけではイメージしにくい」「実際の生地を手に取って、お部屋に合わせてみたい」など、不安な点を感じる方にお気軽にご利用頂ける生地サンプルの送付を行っております。カーテンの生地サンプルは10枚まで無料でご利用いただけます。<br>
                        ※お申し込みから約1週間程度でお届けとなります。<br>
                        ※一部商品は対象外となります。
                    </div>
                    <div class="img">
                        <img src="<!--{$TPL_URLPATH}-->img/first/first_img03.jpg" alt="カーテンの生地サンプルをお送りします">
                    </div>
                </div>
			</div>
        </div><!--/.innner-->
</div><!--/.page-->
