<div class="page features">
    <div id="contents_wrapper">
        <div class="ttl">
            <h2><span></span>カーテンの機能と特徴</h2>
        </div>
        <p>部屋のカーテンじゅうたん王国のカーテンは見た目の美しさはもちろん、機能も充実。こだわりのカーテンの機能と特徴をご案内いたします。</p>
        </div>

        <div class="link_in_page">
            <div class="link_in_page_click">
                <div class="link_in_page_click_1">
                    <a href="#common">ドレープカーテン、レースカーテン共通</a>
                </div>
                <div class="link_in_page_click_2">
                    <a href="#lace_only">レースカーテンのみ</a>
                </div>
            </div>
        </div>

        <div class="curtains_features_wrapper">
            <h3>ドレープカーテン、レースカーテン共通</h3>
            <div class="drape_lace_curtains" id="common">
                <!--ここから-->
                <!--{foreach from=$arrFunctionFeature item=val}-->
                    <!--{if (int)$val.id !== 11 && (int)$val.id !== 13}-->
                    <div class="curtain_feature">
                        <div class="drape_lace_img">
                            <img src="<!--{$TPL_URLPATH}--><!--{$val.image_path}-->">
                            <div class="feature_remark">
                                <h4><!--{$val.title}--></h4>
                                <div>
                                    <!--{$val.contents}-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--{/if}-->
                <!--{/foreach}-->
                <!--ここまで-->
            </div>


            <h3>レースカーテンのみ</h3>
            <div class="lace_only" id="lace_only">
                <!--ここから-->
                <!--{foreach from=$arrFunctionFeature item=val}-->
                    <!--{if (int)$val.id === 11 || (int)$val.id === 13}-->
                    <div class="curtain_feature">
                        <div class="drape_lace_img">
                            <img src="<!--{$TPL_URLPATH}--><!--{$val.image_path}-->">
                            <div class="feature_remark">
                                <h4><!--{$val.title}--></h4>
                                <div>
                                    <!--{$val.contents}-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--{/if}-->
                <!--{/foreach}-->
                <!--ここまで-->
            </div>
        </div>
    </div>

