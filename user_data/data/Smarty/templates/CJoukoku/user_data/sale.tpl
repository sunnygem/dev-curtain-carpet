<style>
@media screen and (max-width: 960px) {
	 .tenpo-campaign-sale .targetItem li .deta .value {
		 font-size:20px;
	 }
}
</style>	


<div class="green-border">

	</div>

	<div class="page-white tenpo-campaign-sale">
		<div class="inner">

			<div class="eyecatch">
        <!--<p class="heading">歳末セール！</p>-->
				<img src="/wp/wp-content/themes/theme/images/campaign-sale-eyecatch50.png">
				<p class="paragraph">
      人気の商品が数量限定のお買い得商品になって再登場！！<br>
        
        </p>
			</div>
      <!-- .eyecatch -->
			<div class="targetItem">
				<h3>対象商品はこちら</h3>
				<div class="targetList">
					<ul class="clearfix">
<li>
							<a href="/shop/products/detail.php?product_id=512&admin=on">
								<h4>遮光3級機能付き、クラシック柄既製カーテンジェナジェナ (ブルー)</h4>
								<img src="/shop/upload/save_image/jenaBL_2.jpg">
							</a>
							<div class="deta">
								<p>巾100㎝×丈135㎝（2枚）が<br>定価8,618円（税込）↓</p>
								<p class="value">4,309円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>遮光3級機能付き、1.5倍ヒダ仕様の既製カーテンです。裏地とタッセルが付いています。形状記憶加工が施されているので、美しいヒダが長持ちします。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=512&admin=on">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=513">
								<h4>遮光3級機能付き、クラシック柄既製カーテンジェナ (ローズ)</h4>
								<img src="/shop/upload/save_image/jenaRO_2.jpg">
							</a>
							<div class="deta">
								<p>巾100㎝×丈135㎝（2枚）が<br>定価8,618円（税込）↓</p>
								<p class="value">4,309円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>遮光3級機能付き、1.5倍ヒダ仕様の既製カーテンです。裏地とタッセルが付いています。形状記憶加工が施されているので、美しいヒダが長持ちします。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=513">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=487">
								<h4>形状記憶付厚地カーテンレンク (グレー)</h4>
								<img src="/shop/upload/save_image/renku_GRY.jpg">
							</a>
							<div class="deta">
								<p>巾100㎝×丈135㎝（2枚）が<br>定価5,378円（税込）↓</p>
								<p class="value">3,764円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">30%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>形状記憶加工が施された厚地の既製カーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=487">詳細はこちら<i></i></a>
							</div>
						</li>
<li>
							<a href="/shop/products/detail.php?product_id=486">
								<h4>形状記憶付厚地カーテンレンク (イエロー)</h4>
								<img src="/shop/upload/save_image/renkYE_3.jpg">
							</a>
							<div class="deta">
								<p>巾100㎝×丈135㎝（2枚）が<br>定価5,378円（税込）↓</p>
								<p class="value">3,764円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">30%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>形状記憶加工が施された厚地の既製カーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=486">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=485">
								<h4>形状記憶付厚地カーテンレンク (アイボリー)</h4>
								<img src="/shop/upload/save_image/renkIV_2.jpg">
							</a>
							<div class="deta">
								<p>巾100㎝×丈135㎝（2枚）が<br>定価5,378円（税込）↓</p>
								<p class="value">3,764円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">30%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>形状記憶加工が施された厚地の既製カーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=485">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=488">
								<h4>形状記憶付厚地カーテンレンク (ブルー)</h4>
								<img src="/shop/upload/save_image/renkBL_3.jpg">
							</a>
							<div class="deta">
								<p>巾100㎝×丈135㎝（2枚）が<br>定価5,378円（税込）↓</p>
								<p class="value">3,764円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">30%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>形状記憶加工が施された厚地の既製カーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=488">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=550">
								<h4>レースカーテンジャミール (ベージュ)</h4>
								<img src="/shop/upload/save_image/jamiruBE_1.jpg">
							</a>
							<div class="deta">
								<p>巾100㎝×丈133㎝（2枚）が<br>定価4,298円（税込）↓</p>
								<p class="value">3,008円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">30%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>刺繍入り1.5倍ヒダ仕様の既製レースカーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=550">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=559">
								<h4>ポルト （ピンク）</h4>
								<img src="/shop/upload/save_image/porutoPI/porutoPI_1.jpg">
							</a>
							<div class="deta">
								<!-- <p>巾225×丈220㎝</p> -->
								<!-- <p>メーカー希望小売価格<br>定価39,636円（税込）↓</p> -->
								<p>巾100㎝×丈135㎝（2枚）が<br>定価4,298円（税込）↓</p>
								<p class="value">2,149円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>遮光2級形状記憶付1.5倍ヒダカーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=559">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=560">
								<h4>ザフラ （イエロー）</h4>
								<img src="/shop/upload/save_image/zafraYE/zafraYE_1.jpg">
							</a>
							<div class="deta">
								<!-- <p>巾225×丈220㎝</p> -->
								<!-- <p>メーカー希望小売価格<br>定価64,260円（税込）↓</p> -->
								<p>巾100㎝×丈135㎝（2枚）が<br>定価5,378円（税込）↓</p>
								<p class="value">2,689円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>遮光2級形状記憶付1.5倍ヒダカーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=560">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=561">
								<h4>パトリシア （パープル）</h4>
								<img src="/shop/upload/save_image/patriciaPU/patriciaPU_1.jpg">
							</a>
							<div class="deta">
								<!-- <p>巾225×丈220㎝</p> -->
								<!-- <p>メーカー希望小売価格<br>定価47,628円（税込）↓</p> -->
								<p>巾100㎝×丈135㎝（2枚）が<br>定価6,458円（税込）↓</p>
								<p class="value">3,229円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>遮光2級裏地付形状記憶付1.5倍ヒダカーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=561">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=562">
								<h4>パトリシア （ベージュ）</h4>
								<img src="/shop/upload/save_image/patriciaBE/patriciaBE_1.jpg">
							</a>
							<div class="deta">
								<!-- <p>巾225×丈220㎝</p> -->
								<!-- <p>メーカー希望小売価格<br>定価141,804円（税込）↓</p> -->
								<p>巾100㎝×丈135㎝（2枚）が<br>定価6,458円（税込）↓</p>
								<p class="value">3,229円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>遮光2級裏地付形状記憶付1.5倍ヒダカーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=562">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=563">
								<h4>カウラ （グレー）</h4>
								<img src="/shop/upload/save_image/kauraGRY/kauraGRY_1.jpg">
							</a>
							<div class="deta">
								<!-- <p>巾200×丈220㎝</p> -->
								<!-- <p>メーカー希望小売価格<br>定価75,924円（税込）↓</p> -->
								<p>巾100㎝×丈133㎝（2枚）が<br>定価3,218円（税込）↓</p>
								<p class="value">1,609円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>1.5倍ヒダレースカーテンです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=563">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=564">
								<h4>キュイール（ﾍﾞｰｼﾞｭ）</h4>
								<img src="/shop/upload/save_image/kyuierBE/kyuierBE_1.jpg">
							</a>
							<div class="deta">
								<!-- <p>巾200×丈220㎝</p> -->
								<!-- <p>メーカー希望小売価格<br>定価85,536円（税込）↓</p> -->
								<p>約185㎝x185㎝が<br>定価6,458円（税込）↓</p>
								<p class="value">3,229円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>起毛した手触りの良い薄手のラグです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=564">詳細はこちら<i></i></a>
							</div>
						</li>
						<li>
							<a href="/shop/products/detail.php?product_id=565">
								<h4>キュイール（ﾌﾞﾗｳﾝ）</h4>
								<img src="/shop/upload/save_image/kyuierBR/kyuierBR_1.jpg">
							</a>
							<div class="deta">
								<!-- <p>巾225×丈220㎝</p> -->
								<!-- <p>メーカー希望小売価格<br>定価74,196円（税込）↓</p> -->
								<p>約185㎝x185㎝が<br>定価6,458円（税込）↓</p>
								<p class="value">3,229円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>起毛した手触りの良い薄手のラグです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=565">詳細はこちら<i></i></a>
							</div>
						</li>
						<!-- <li>
							<a href="/shop/products/detail.php?product_id=566">
								<h4>ラディエ（ｺｰﾗﾙ）</h4>
								<img src="/shop/upload/save_image/radhieCO/radhieCO_1.jpg">
							</a>
							<div class="deta">
								<p>メーカー希望小売価格<br>定価1,990円（税込）↓</p>
								<p class="value">995円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>素材感のある毛足が特徴のラグです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=566">詳細はこちら<i></i></a>
							</div>
						</li> -->
						<!-- <li>
							<a href="/shop/products/detail.php?product_id=567">
								<h4>ラディエ（ﾍﾞｰｼﾞｭ）</h4>
								<img src="/shop/upload/save_image/radhieBE/radhieBE_1.jpg">
							</a>
							<div class="deta">
								<p>メーカー希望小売価格<br>定価1,990円（税込）↓</p>
								<p class="value">995円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>素材感のある毛足が特徴のラグです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=567">詳細はこちら<i></i></a>
							</div>
						</li> -->
						<!-- <li>
							<a href="/shop/products/detail.php?product_id=568">
								<h4>ラディエ（ｸﾞﾘｰﾝ）</h4>
								<img src="/shop/upload/save_image/radhieG/radhieG_1.jpg">
							</a>
							<div class="deta">
								<p>メーカー希望小売価格<br>定価1,990円（税込）↓</p>
								<p class="value">995円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>素材感のある毛足が特徴のラグです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=568">詳細はこちら<i></i></a>
							</div>
						</li> -->
						<!-- <li>
							<a href="/shop/products/detail.php?product_id=569">
								<h4>ラディエ（ﾊﾟｰﾌﾟﾙ）</h4>
								<img src="/shop/upload/save_image/radhiePU/radhiePU_1.jpg">
							</a>
							<div class="deta">
								<p>メーカー希望小売価格<br>定価1,990円（税込）↓</p>
								<p class="value">995円<span>（税込）</span>～</p>
                <div class="campaign-sale-persent">50%<br>OFF</div>
							</div>
							<div class="detaTxt">
								<p>素材感のある毛足が特徴のラグです。</p>
							</div>
							<div class="detaBtn">
								<a href="/shop/products/detail.php?product_id=569">詳細はこちら<i></i></a>
							</div>
						

			<!--<div class="checkItem">
				<h3 class="title-border">最近チェックした商品</h3>
				<ul>
					<li>
						<img src="/wp/wp-content/themes/theme/images/checkItem-img1.png">
						<p>カルボス</p>
						<p>￥1,980～￥2,980</p>
					</li>
					<li>
						<img src="/wp/wp-content/themes/theme/images/checkItem-img1.png">
						<p>カルボス</p>
						<p>￥1,980～￥2,980</p>
					</li>
					<li>
						<img src="/wp/wp-content/themes/theme/images/checkItem-img1.png">
						<p>カルボス</p>
						<p>￥1,980～￥2,980</p>
					</li>
					<li>
						<img src="/wp/wp-content/themes/theme/images/checkItem-img1.png">
						<p>カルボス</p>
						<p>￥1,980～￥2,980</p>
					</li>
				</ul>
			</div>

			<div class="search clearfix">
				<h3 class="title-border">商品を探す&nbsp;<img src="/wp/wp-content/themes/theme/images/icon-search.png">SEARCH</h3>
				<div class="catCurtain">
					<h4>カーテン</h4>
					<ul>
						<li><a href="/shop/products/list.php?category_id=1768"><img src="/wp/wp-content/themes/theme/images/search-catcurtain-01.png"><p>既成カーテン</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1768"><img src="/wp/wp-content/themes/theme/images/search-catcurtain-02.png"><p>ロールスクリーン</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1768"><img src="/wp/wp-content/themes/theme/images/search-catcurtain-03.png"><p>プリーツスクリーン</p></p></a></li>
						<li><a href="/shop/products/list.php?category_id=1768"><img src="/wp/wp-content/themes/theme/images/search-catcurtain-04.png"><p>上飾りバランス</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1768"><img src="/wp/wp-content/themes/theme/images/search-catcurtain-05.png"><p>シェード</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1764"><img src="/wp/wp-content/themes/theme/images/search-catcurtain-06.png"><p>ブラインド</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1768"><img src="/wp/wp-content/themes/theme/images/search-catcurtain-07.png"><p>カーテンレール</p></a></li>
					</ul>
				</div>
				<div class="catOrder">
					<h4>窓まわり（オーダーメイド）</h4>
					<ul>
						<li><a href="/shop/products/list.php?category_id=1719"><img src="/wp/wp-content/themes/theme/images/search-catorder-01.png"><p>オーダーカーテン</p></a></li>
					</ul>
				</div>
				<div class="catCarpet">
					<h4>カーペット</h4>
					<ul>
						<li><a href="/shop/products/list.php?category_id=1759"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-01.png"><p>オーダーラグ＆カーペット</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1714"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-02.png"><p>モケット＆グブラン織ラグ<br>（敷詰めタイプ）</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1714"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-03.png"><p>スペースラグ</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1763"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-04.png"><p>ウィルトンカーペット</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1758"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-05.png"><p>ダイニングラグ</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1714"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-06.png"><p>綿・綿混ラグ</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1760"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-07.png"><p>い草＆竹ラグ</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1746"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-08.png"><p>オーダーラグ（円形・四角形<br>・敷詰めカーペット）</p></a></li>
						<li><a href="/shop/products/list.php?category_id=1757"><img src="/wp/wp-content/themes/theme/images/search-catcarpet-09.png"><p>玄関マット</p></a></li>
					</ul>
				</div>
			</div>-->

		</div><!--inner-->
	</div>
