<div class="page-body">
		<div class="inner howpay">
			<h2>お支払い方法</h2>
                        <p>掲載しているお支払方法はオンラインストアのみでご利用できます。<br>
店舗で使用できるお支払方法とは異なりますのでご注意下さい。</p>

			<div class="img100"><img src="<!--{$TPL_URLPATH}-->img/common/f19_img_01.png"></div>
			<div class="inners">

				<h4>クレジットカード決済</h4>
				<p>・VISA/MASTER/AMEX/DINERSがご利用できます。<br>
・1回払い・分割払い・リボルビング払いがご利用できます。<br>
・分割払いをご利用の場合はカード会社様によってはクレジット会社様へお支払いただく手数料が発生する可能性がございます。事前にカード会社様へご契約内容をご確認ください。</p>

				<h4>コンビニ決済</h4>
				<div class="img02">
					<img src="<!--{$TPL_URLPATH}-->img/common/f19_img_18.png">
				</div>
				<p>※会計処理によりご入金の確認に5日程度かかる場合がございます。</p>

				<h4>Pay-easy決済</h4>
				<p>・Pay-easy対応の金融機関でのお支払いが可能です。<br>
					・インターネットバンキング利用でのお支払いとATM利用でのお支払い、どちらかお好きな手段を選択することが出来ます。</p>

				<h4>キャリア決済</h4>
				<div class="img02">
					<img src="<!--{$TPL_URLPATH}-->img/common/payment_img.png">
				</div>
				<p>・ご利用の携帯電話キャリア決済がご利用できます。<br>
					・お客様のご契約キャリアによって、通話料との合算・キャリア口座等お好きな手段を選択することができます。</p>


				<h4>商品代引き</h4>
				<p>・商品の代引きについては承っておりません。</p>



			</div>



		</div><!-- page-body inner -->
	</div><!-- page-body -->
