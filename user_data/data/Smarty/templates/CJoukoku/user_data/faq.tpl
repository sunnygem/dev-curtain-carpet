<div class="page faq">
    <div class="ttl">
        <h2><span></span>よくある質問</h2>
    </div>
    <section class="sec01">
        <div class="bloc">
            <h3>ご注文について</h3>
            <div>
                <ul>
                    <li><a href="#o1"><span class="tab_title">Q.注文確認メールが届きません。</span></a></li>
                    <li><a href="#o2"><span class="tab_title">Q.品切れしていますが、取り寄せはできますか？</span></a></li>
                    <li><a href="#o3"><span class="tab_title">Q.追加注文をして同梱は可能ですか？</span></a></li>
                    <li><a href="#o4"><span class="tab_title">Q.問合せメールをしましたが、返信が来ません。</span></a></li>
                    <li><a href="#o5"><span class="tab_title">Q.引っ越し先に届けてほしいのですが、現住所のまま注文してしまったので、届け先を変更したいです。</span></a></li>
                </ul>
            </div>
        </div>
        <div class="bloc">
            <h3>配送について</h3>
            <div>
                <ul>
                    <li><a href="#d1"><span class="tab_title">Q.配送会社の指定はできますか？</span></a></li>
                    <li><a href="#d2"><span class="tab_title">Q.配送番号は教えてもらえますか？</span></a></li>
                    <li><a href="#d3"><span class="tab_title">Q.注文したのに、まだ届きません。</span></a></li>
                    <li><a href="#d4"><span class="tab_title">Q.複数注文したのに、一部の品物が届きません。</span></a></li>
                </ul>
            </div>
        </div>
        <div class="bloc">
            <h3>支払いについて</h3>
            <div>
                <ul>
                    <li><a href="#p1"><span class="tab_title">Q.支払方法は何が選べますか？</span></a></li>
                    <li><a href="#p2"><span class="tab_title">Q.領収書は発行できますか？</span></a></li>
                </ul>
            </div>
        </div>
        <div class="bloc">
            <h3>返品・交換について</h3>
            <div>
                <ul>
                    <li><a href="#r1"><span class="tab_title">Q.注文のキャンセルはできますか？</span></a></li>
                    <li><a href="#r2"><span class="tab_title">Q.商品がイメージと違うので返品できますか？</span></a></li>
                    <li><a href="#r3"><span class="tab_title">Q.届いた商品が不良品だった場合はどうしたらいいですか？（破損・汚れ・不良品 等）</span></a></li>
                    <li><a href="#r4"><span class="tab_title">Q.返品・交換はできますか？</span></a></li>
                </ul>
            </div>
        </div>
        <div class="bloc">
            <h3>店舗について</h3>
            <div>
                <ul>
                    <li><a href="#s"><span class="tab_title">Q.店舗はどの地域にありますか？</span></a></li>
                </ul>
            </div>
        </div>
        <div class="bloc">
            <h3>カーテンのお手入れについて</h3>
            <div>
                <ul>
                    <li><a href="#c1"><span class="tab_title">Q.カーテンを自宅の洗濯機で洗っていいのでしょうか？</span></a></li>
                    <li><a href="#c2"><span class="tab_title">Q.カーテンの適度なお洗濯回数は年何回ぐらいですか？</span></a></li>
                    <li><a href="#c3"><span class="tab_title">Q.カーテンの普段のお手入れはどうしたらよいですか？</span></a></li>
                    <li><a href="#c4"><span class="tab_title">Q.カーテンのカビ防止対策を教えてください。</span></a></li>
                    <li><a href="#c5"><span class="tab_title">Q.カーテンに伸縮はありますか？</span></a></li>
                    <li><a href="#c6"><span class="tab_title">Q.カーテンは何年ぐらい持ちますか？</span></a></li>
                </ul>
            </div>
        </div>
        <div class="bloc">
            <h3>カーテンの計り方について</h3>
            <div>
                <ul>
                    <li><a href="#c_m"><span class="tab_title">Q.カーテンのサイズの計り方はどうなりますか？</span></a></li>
                </ul>
            </div>
        </div>
        <div class="bloc">
            <h3>カーテンの選び方について</h3>
            <div>
                <ul>
                    <li><a href="#c_c"><span class="tab_title">Q.カーテンを購入する場合、どんなことに注意すればよいでしょうか？</span></a></li>
                    <li><a href="#c_c"><span class="tab_title">Q.カーテンはどんなお店で買うのがよいでしょうか？</span></a></li>
                </ul>
            </div>
        </div>
        <div class="bloc">
            <h3>よくある間違いについて</h3>
            <div>
                <ul>
                    <li><a href="#m1"><span class="tab_title">Q.開き方について</span></a></li>
                    <li><a href="#m2"><span class="tab_title">Q.横幅のつなぎ目について</span></a></li>
                    <li><a href="#m3"><span class="tab_title">Q.仕上がりの高さについて</span></a></li>
                    <li><a href="#m4"><span class="tab_title">Q.フックとレールの形式について</span></a></li>
                </ul>
            </div>
        </div>
    </section>
    
    <section class="sec02">
        <h3>ご注文について</h3>
        <p id="o1" class="question">Q.注文確認メールが届きません。</p>
        <div class="answer">
            <p>
                A.ご注文が混み合っている、もしくは当店からのメールが受け取れない状態が考えらえます。<br>
                「oukoku-onlineshop@oukoku.biz」のドメインを受信許可に設定していただき、お問い合わせフォームにてご連絡ください。
            </p>
        </div>
        <p id="o2" class="question">Q.品切れしていますが、取り寄せはできますか？</p>
        <div class="answer">
            <p>
                A.可能な場合がございますのでお問い合わせフォームからおたずねください。ただし、廃番品・完売品などは、取り寄せる事はできません。
            </p>
        </div>
        <p id="o3" class="question">Q.追加注文をして同梱は可能ですか？</p>
        <div class="answer">
            <p>
                A.出荷手配前であれば、お荷物をおまとめさせていただくことが可能な場合がございます。<br>
                状況を確認いたしますので、メールにてお問い合わせください。<br>
                なお、出荷完了後の追加・変更はいたしかねます。ご了承ください。
            </p>
        </div>
        <p id="o4" class="question">Q.問合せメールをしましたが、返信が来ません。</p>
        <div class="answer">
            <p>
                A.お問い合わせメールが混み合っている、もしくは当店からのメールが届かない状態が考えられます。<br>
               「oukoku-onlineshop@oukoku.biz」のドメインを受信許可に設定していただき、メールにてご連絡ください。
            </p>
        </div>
        <p id="o5" class="question">Q.引っ越し先に届けてほしいのですが、現住所のまま注文してしまったので、届け先を変更したいです。</p>
        <div class="answer">
            <p>
                A.出荷前の場合は変更が可能ですので、お問い合わせフォームにて、当店まで正しいお届け先のご連絡をお願いいたします。<br>
                なお、会員のお客様は、次回以降のご注文のため、「マイページ」ページよりご登録のご住所をご変更いただきますようお願いいたします。<br>
                出荷後の場合は、配送業者と連絡を取らせていただき、届け先の変更を依頼いたします。
            </p>
        </div>
        <p id="o6" class="question">Q.メール・電話での注文はできますか？</p>
        <div class="answer">
            <p>
                A.メールやお電話でのご注文は承っておりません。オンラインショップの注文フォームからのご注文のみとさせて頂いております。ご了承ください。
            </p>
        </div>
        <p id="o7" class="question">Q.購入履歴はどこで確認できますか？</p>
        <div class="answer">
            <p>
                A.購入履歴はマイページから確認をすることができます。<br>
                ただし、ご購入時に会員登録されていないお客様は申し訳ございませんが確認をすることができませんので事前の会員登録をおすすめ致します。
            </p>
        </div>
    </section>
    <section class="sec02">
        <h3>配送について</h3>
        <p id="d1" class="question">Q.配送会社の指定はできますか？</p>
        <div class="answer">
            <p>
                A.基本的には佐川急便にてお届けいたします。<br>
                ですが、商品の一部におきましてはメーカー直送にて配達をしております。その場合は、メーカー指定の配送業者となりますのでご了承ください。<br>
                なお、配送会社のご指定については、お受けいたしかねます。ご了承ください。<br>
            </p>
        </div>
        <p id="d2" class="question">Q.配送番号は教えてもらえますか？</p>
        <div class="answer">
            <p>
                A.出荷完了時にお送りする「発送のお知らせ」メールにて配送番号をご案内いたします。
            </p>
        </div>
        <p id="d3" class="question">Q.注文したのに、まだ届きません。</p>
        <div class="answer">
            <p>
                A.お荷物の状況は配送会社のホームページにてご確認いただけます。出荷のお知らせメールに配送会社のお荷物検索URLから配送番号を入力してください。<br>
                ご注文が集中している場合や、地域の交通事情によって、お届けが多少遅れている場合がございますので、その場合は何卒ご理解くださいますよう、お願い申し上げます。
            </p>
        </div>
        <p id="d4" class="question">Q.複数注文したのに、一部の品物が届きません。</p>
        <div class="answer">
            <p>
                A.お荷物の状況は配送会社のホームページにてご確認いただけます。出荷のお知らせメールに配送会社のお荷物検索URLから配送番号を入力してください。<br>
                複数口でのお届けとなる際に別々の倉庫から発送する場合がございます。その場合、配送番号が異なる場合がございますので、ご確認の際にはご注意ください。<br>
                なお、複数口でのお届けとなる場合は、「発送のお知らせ」メールにてその旨をお知らせいたします。<br>
                商品が異なる場合、お届けまでの納期がそれぞれ異なる場合ががございます。その場合、別便・別日でのお届けとなりますのでご了承ください。<br>
                ご注文が集中している場合や、地域の交通事情によって、お届けが多少遅れている場合がございますので、その場合は何卒ご理解くださいますよう、お願い申し上げます。
            </p>
        </div>
    </section>
    <section class="sec02">
        <h3>支払いについて</h3>
        <p id="p1" class="question">Q.支払方法は何が選べますか？</p>
        <div class="answer">
            <p>
                A.下記の決済方法からお選びいただけます。<br>
                ・クレジットカード決済、コンビニ決済、Pay-easy決済、携帯キャリア決済<br>
                詳しくは<a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/delivery.php">【送料やお支払いについて】</a>ページをご確認ください。
            </p>
        </div>
        <p id="p2" class="question">Q.領収書は発行できますか？</p>
        <div class="answer">
            <p>
                A.可能です。<br>
                納品書(お買い上げ明細書)は、商品と同梱しお送りいたします。<br>
                領収書が必要な場合は、お問い合わせフォームからご連絡ください。
            </p>
        </div>
    </section>
    <section class="sec02">
        <h3>返品・交換について</h3>
        <p id="r1" class="question">Q.注文のキャンセルはできますか？</p>
        <div class="answer">
            <p>
                A.『【カーテン・じゅうたん王国公式オンラインショップ】 ご入金確認のメール』がお客様へ届いた後ですと、ご注文内容のご変更・キャンセルは承ることができませんのでご注意ください。<br>
                また、お客さまの迷惑メールの設定でご注文の自動返信メールがお届けできていない時でも、商品はお手配させて頂いている場合がございますのでご了承ください。
            </p>
        </div>
        <p id="r2" class="question">Q.商品がイメージと違うので返品できますか？</p>
        <div class="answer">
            <p>
                A.原則として、良品の返品・交換は一切お受けしておりません。<br> 
                イメージ違い、ご注文サイズのご指定間違いなど、お客様都合によるご返品・ご交換はお断りしております。ご確認の上、ご注文ください。<br>
                掲載商品の写真はご覧のモニターにより色が多少異なって見える場合がございます。　又、照明環境などによってお使いになる場所でも色が違って見えてまいりますのでご了承ください。 <br>
                ご購入前に実際の生地をご確認されたい方は、「生地サンプル請求（無料）」をご利用ください。
            </p>
        </div>
        <p id="r3" class="question">Q.届いた商品が不良品だった場合はどうしたらいいですか？（破損・汚れ・不良品 等）</p>
        <div class="answer">
            <p>
                A.申し訳ございません。大変お手数ですが、内容確認のため、オンラインショップまで受注番号・お客様名・不良商品名・状態などをお問い合わせフォームからご連絡ください。<br>
                詳しい状況を確認する為、不良箇所のお写真をお願いすることがございますがご了承ください。<br>
                当店では返品や返金はいたしておりません。良品交換にてご対応をさせていただきます。<br>
                但し、お届け日より1週間以内にご連絡いただいた場合に限ります。<br>
            </p>
        </div>
        <p id="r4" class="question">Q.返品・交換はできますか？</p>
        <div class="answer">
            <p>
                A.ご注文後（商品お届け後）のキャンセル・交換・返品などは一切お断りしております。<br> 
                詳しくは<a href="<!--{$smarty.const.ROOT_URLPATH}-->order/index.php">【特定商取引法に基づく表記】</a>をご確認ください。
            </p>
        </div>
    </section>
    <section class="sec02">
        <h3>店舗について</h3>
        <p id="s" class="question">Q.店舗はどの地域にありますか？</p>
        <div class="answer">
            <p>
                A.カーテンじゅうたん王国は全国に87店舗あります。<br>
                カーテンじゅうたん王国の<a href="<!--{$smarty.const.WP_COMPANY_URLPATH}-->tenpo-top/">店舗紹介・検索</a>をご覧ください。
            </p>
        </div>
    </section>
    <section class="sec02">
        <h3>カーテンのお手入れについて</h3>
        <p id="c1" class="question">Q.カーテンを自宅の洗濯機で洗っていいのでしょうか？</p>
        <div class="answer">
            <p>
                A.商品により異なりますが、基本的には洗濯機で洗えます。<br>
                カーテンには洗濯表示（取扱絵表示）がついていますので、ご確認のうえ洗濯をお願いします。<br>
                カーテンの洗い方の手順や注意点については、お手入れ方法をご覧ください。<br>
                ※カーテンを洗う際は【アジャスターフックを必ず外して】頂くようご注意ください。
            </p>
        </div>
        <p id="c2" class="question">Q.カーテンの適度なお洗濯回数は年何回ぐらいですか？</p>
        <div class="answer">
            <p>
                A.目安として、ドレープカーテンは年に1回程度、レースカーテンはよく汚れますので、半年に1回程度のお洗濯をお勧めします。<br>
                商品と状況により異なりますが、数年間洗濯をしなかった場合、１回の洗濯でカーテンが劣化により破れることもありますので、ご注意ください。
            </p>
        </div>
        <p id="c3" class="question">Q.カーテンの普段のお手入れはどうしたらよいですか？</p>
        <div class="answer">
            <p>
                A.カーテンはカーペットとちがって生地が薄いので、掃除機で吸い込む等の扱いをしないでください。生地が傷む原因になります。毎日の 開け閉めをすることがカーテンの1番のメンテナンスです（ホコリのたまり防止・動かすことで生地の硬化を防ぎ、劣化を遅らせることができる）。ハタキではたくという人がいますが、ハタキも強くたたくと生地が傷みます。開け閉めすればホコリが取れるので、特別なお手入れはありません。<br>
                レースは窓側にある為、厚地カーテンよりもほこりの吸着があります。意識して開け閉めをしてください。雨や窓の結露などはカビの原因となるので、室内の湿度を調整する・洗濯は汚れたらすぐ洗うことを心がけてください。
            </p>
        </div>
        <p id="c4" class="question">Q.カーテンのカビ防止対策を教えてください。</p>
        <div>
        <div class="answer">
            <p>
                A.カビの原因はほとんどが冬の季節の結露です。窓の外側にあるレースが一番カビが発生しやすくなります。加湿器を使うとさらにカビが生えやすくなります。日に当てて乾かして湿気を取るのが最も効果的です。<br>
                カビがついてしまったら早めに洗濯をして除去します。洗濯してもカビ菌は死にません。洗濯して乾かしたらドライヤーを当ててカビ菌を熱殺菌するのが良いでしょう。 他に予防法として、防カビ加工をしてあるカーテンを選ぶことをおすすめします。
            </p>
        </div>
        <p id="c5" class="question">Q.カーテンに伸縮はありますか？</p>
        <div class="answer">
            <p>
                A.カーテンは、部屋の温度・湿度が最も影響を与え、伸びたり縮んだりすることがあります。<br>
                これは、カーテンに使用されている繊維の特徴です。また、カーテンの自重で伸びる場合もあります。ポリエステル以外の特に、綿・麻・絹などの天然繊維やレーヨン素材のカーテンでは、伸縮の大きいものもあります。
            </p>
        </div>
        <p id="c6" class="question">Q.カーテンは何年ぐらい持ちますか？</p>
        <div class="answer">
            <p>
                A.ご使用状況（日差しの強さや開け閉め・お洗濯の頻度など）とカーテン自体の機能（非遮光・遮光生地、UVカット機能）によって、変わりますが、一般的には年2回程度お洗濯をしていて約4年～5年は持つものと考えられています。オーダーカーテンですと、6年～12年とかなり長くお使いいただけることもございます。
            </p>
        </div>
    </section>
    <section class="sec02">
        <h3>カーテンの計り方について</h3>
        <p id="c_m" class="question">Q.カーテンのサイズの計り方はどうなりますか？</p>
        <div class="answer">
            <p>
                A.横の長さは、カーテンレールのカーテンを引っかける左隅の穴から右隅の穴まで（ほぼレールの全長）。 縦の長さは、床まである窓にはカーテンを引っかける固定された穴から床までの長さを計ります。<br>
                カーテンにとってサイズは命なので、詳しくは採寸方法についてをご確認のうえ、お計りください。<br>
                ※木枠や窓枠の長さはカーテンサイズと異なります。<a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/measurements.php">【採寸方法について】</a>をご覧ください。
            </p>
        </div>
    </section>
    <section class="sec02">
        <h3>カーテンの選び方について</h3>
        <p id="c_c1" class="question">Q.カーテンを購入する場合、どんなことに注意すればよいでしょうか？</p>
        <div class="answer">
            <p>
                A.①品質・縫製仕様はどうか<br>
                耳（端）・裾の折り返しが真っすぐに縫製されているか。生地自体の手持ち感はどうか（薄すぎる・やぼったくないか）などをチェックしてみてください。<br>
                ヒダ山（カーテンの上部）がきれいに縫製されているかも見た目に関わってきます。<br>
                パッケージされている既製カーテンでもお店にかかっているサンプルを見て、カーテンがよれていないか、左右の耳の部分がまっすぐに下りているか、左右のカーテンの長さが同じかどうか、斜めになっていないかを見るだけでも仕様の判断ができます。<br>
                ②デザイン・色柄・機能<br>
                使用する窓に、どんな機能が必要かを事前に洗い出しておくとよいでしょう。例）日差しや西日を防ぐために遮光生地が必要・隣の家と近いので、プライバシー性の高いレースが必要…等）。また、インテリアブック・サイト等を参考に、どんなテイストにしたいか・今ある部屋の写真を何枚か撮ってから、カーテンの色を決めていくとよいでしょう。
            </p>
        </div>
        <p id="c_c2" class="question">Q.カーテンはどんなお店で買うのがよいでしょうか？</p>
        <div class="answer">
            <p>
                A.選択を狭めることがないよう、価格帯・品数が充実したショップをおすすめします（リーズナブルな既製カーテン～メーカーオーダーカーテンまで）。実物サンプルがあるお店・生地サンプルを用意してもらえるお店は安心感があります。
            </p>
        </div>
    </section>
    <section class="sec02">
        <h3>よくある間違いについて</h3>
        <p id="m1" class="question">Q.開き方について</p>
        <div class="answer">
            <p>
                A.両開きと片開きで、カーテンのサイズと枚数が異なります。<br>
                例）1窓、横幅200×高さ150cmでオーダーする場合<br>
                「両開き」を選ぶと、横幅100×高さ丈150cmのカーテンが2枚届きます。<br>
                「片開き」を選ぶと、横幅200×高さ150cmのカーテンが1枚届きます。
            </p>
        </div>
        <p id="m2" class="question">Q.横幅のつなぎ目について</p>
        <div class="answer">
            <p>
                A.カーテンのつなぎ目は既製カーテン、オーダーカーテンにおいてもサイズによって生じます。<br>
                1.5倍ヒダでは横幅101㎝以上、2倍ヒダでは横幅70㎝以上（一部75㎝以上）の場合にはつなぎ目が入ります。<br>
                つなぎ目のないカーテンをご希望の場合は、「シームレス」機能のカーテンにて検索してください。<br>
                また、1.5倍ヒダの既製カーテンで、横幅が100㎝以上の商品は柄合わせを行っておりません。<br>
                その為、デザインがきっちり連続しませんのでご了承ください。<br>
                気にされる場合は、オーダーカーテンからお選びください。
            </p>
        </div>
        <p id="m3" class="question">Q.仕上がりの高さについて</p>
        <div class="answer">
            <p>
                A.カーテンの高さは生地ものであるため、プラスマイナス1㎝の誤差が発生する場合がございます。その為当店では、高さの調整が出来るアジャスターフックを採用しております。高さに誤差が出た場合はアジャスターフックにて調整して頂くようお願いいたします。
            </p>
        </div>
        <p id="m4" class="question">Q.フックとレールの形式について</p>
        <div class="answer">
            <p>
                A.レールの形式によって、適したフックが異なります。レールの形式をご確認の上、フックをお選びください。<br>
                詳しくは<a href="<!--{$smarty.const.ROOT_URLPATH}-->user_data/measurements.php">【採寸方法について】</a>をご覧ください。
            </p>
        </div>
    </section>
</div><!--/.page-->
