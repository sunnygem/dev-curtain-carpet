<div class="page_feature botanical">
    <h2><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_ttl.jpg" alt="特集 ボタニカルスタイル"></h2>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_img01.jpg" alt="特集 ボタニカルスタイル"></div>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_img02.jpg" alt="特集 ボタニカルスタイル"></div>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_img03.jpg" alt="特集 ボタニカルスタイル"></div>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_img04.jpg" alt="特集 ボタニカルスタイル"></div>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_img05.jpg" alt="特集 ボタニカルスタイル"></div>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_img06.jpg" alt="特集 ボタニカルスタイル"></div>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_img07.jpg" alt="特集 ボタニカルスタイル"></div>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_img08.jpg" alt="特集 ボタニカルスタイル"></div>


</div>
<div class="page_feature botanical">
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/subttl_coordinate.jpg" alt="コーディネート"></div>
    <div class="feature_slider">
        <div class="feature_slider_main_area">
            <div class="feature_slider_main">
                <img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_01.jpg">
            </div>
            <div class="feature_slider_info">
                <h3>掲載カーテン</h3>
                <dl>
                    <dt>ドレープ</dt>
                    <dd>
                        <h4>SD27176</h4>
                        <p><a href="#">>> 商品詳細ページへ</a></p>
                    </dd>
                    <dt>レース</dt>
                    <dd>
                        <h4>PSL3006</h4>
                        <p><a href="#">>> 商品詳細ページへ</a></p>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="feature_slider_thumbnail">
            <ul>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_01.jpg"></li>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_02.jpg"></li>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_03.jpg"></li>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_04.jpg"></li>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_05.jpg"></li>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_06.jpg"></li>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_07.jpg"></li>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_08.jpg"></li>
                <li><img src="<!--{$TPL_URLPATH}-->img/feature/coordinate_slider/botanical_09.jpg"></li>
            </ul>
        </div>
    </div>
    <div><img src="<!--{$TPL_URLPATH}-->img/feature/feature_botanical_zakka_banner.jpg" alt="雑貨バナー"></div>
</div>

    <form name="form1" id="form1" method="get" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="<!--{$mode|h}-->" />
        <!--{* ▼検索条件 *}-->
        <input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" />
        <!-- input type="hidden" name="category" value="<!--{$arrSearchData.category|h}-->" / -->
        <input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
        <input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" />
        <!--{* ▲検索条件 *}-->
        <!--{* ▼ページナビ関連 *}-->
        <input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
        <input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
        <input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
        <!--{* ▲ページナビ関連 *}-->
        <input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />
    </form>

    <!--▼検索条件-->
    <!--{if $tpl_subtitle == "検索結果"}-->
        <ul class="pagecond_area">
            <li><strong>商品カテゴリ：</strong><!--{$arrSearch.category|h}--></li>
        <!--{if $arrSearch.maker|strlen >= 1}--><li><strong>メーカー：</strong><!--{$arrSearch.maker|h}--></li><!--{/if}-->
            <li><strong>商品名：</strong><!--{$arrSearch.name|h}--></li>
        </ul>
    <!--{/if}-->
    <!--▲検索条件-->

    <!--▼ページナビ(本文)-->
    <!--{capture name=page_navi_body}-->
    <div class="pagenumber_area number-head">
        <div class="change">
            <!--{if $orderby != 'date'}-->
                <a href="javascript:fnChangeOrderby('date');">新着順</a>
            <!--{else}-->
                <span>新着順</span>
            <!--{/if}-->&nbsp;
            <!--{if $orderby != "price_h"}-->
                    <a href="javascript:fnChangeOrderby('price_h');">価格の高い順</a>
            <!--{else}-->
                <span>価格の高い順</span>
            <!--{/if}-->&nbsp;
            <!--{if $orderby != "price"}-->
                    <a href="javascript:fnChangeOrderby('price');">価格の低い順</a>
            <!--{else}-->
                <span>価格の低い順</span>
            <!--{/if}-->
        </div>
        <div class="disp_result">
            <label>表示件数</label>
            <div class="select_wrap">
                <select name="disp_number" id="disp_number" onchange="javascript:fnChangeDispNumber(this.value);">
                    <!--{foreach from=$arrPRODUCTLISTMAX item="dispnum" key="num"}-->
                        <!--{if $num == $disp_number}-->
                            <option value="<!--{$num}-->" selected="selected" ><!--{$dispnum}--></option>
                        <!--{else}-->
                            <option value="<!--{$num}-->" ><!--{$dispnum}--></option>
                        <!--{/if}-->
                    <!--{/foreach}-->
                </select>
            </div>
        </div>
    </div>
    <div class="navi">
        <div class="product_pager">
            <ul>
                <!--{$tpl_strnavi}-->
            </ul>
        </div>
    </div>

    <!--{/capture}-->
    <!--▲ページナビ(本文)-->

	 <!--▼ページナビ(本文)-->
    <!--{capture name=page_navi_bodys}-->
        <div class="pagenumber_area number-head bord-head">
            <div class="navi">
                <div class="product_pager">
                    <ul>
                        <!--{$tpl_strnavi}-->
                    </ul>
                </div>
            </div>
        </div>

    <!--{/capture}-->
    <!--▲ページナビ(本文)-->





            <!--▼ページナビ(上部)-->
            <form name="page_navi_top" id="page_navi_top" action="?">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <!--{if $tpl_linemax > 0}--><!--{$smarty.capture.page_navi_body|smarty:nodefaults}--><!--{/if}-->
            </form>

            <!--▲ページナビ(上部)-->

<div class="product_list category">


<ul>
    <!--{foreach from=$arrProducts item=arrProduct name=arrProducts}-->

        <!--{if $smarty.foreach.arrProducts.first}-->

        <!--{/if}-->
    <li>
        <!--{assign var=id value=$arrProduct.product_id}-->
        <!--{assign var=arrErr value=$arrProduct.arrErr}-->
        <!--▼商品-->
        <form name="product_form<!--{$id|h}-->" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="product_id" value="<!--{$id|h}-->" />
            <input type="hidden" name="product_class_id" id="product_class_id<!--{$id|h}-->" value="<!--{$tpl_product_class_id[$id]}-->" />
            <!--★画像★-->
            <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" class="picture" />
            </a>

            <!--★商品名★-->
            <h4>
                <!--{$arrProduct.name|h}-->
            </h4>
            <!--★価格★-->
            <div class="pricebox sale_price">
                <p class="price">
                    <span id="price02_default_<!--{$id}-->"><!--{strip}-->
                        <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                            <!--{$arrProduct.price02_min|n2s}-->
                        <!--{else}-->
                            <!--{$arrProduct.price02_min|n2s}-->円～
                        <!--{/if}-->
                    </span>
                    <!--{/strip}-->(税別)
                </p>
            </div>
        </form>
        <!--▲商品-->
	</li>




    <!--{foreachelse}-->
        <!--{include file="frontparts/search_zero.tpl"}-->
    <!--{/foreach}-->

</div>
	<!--{if $smarty.foreach.arrProducts.last}-->
            <!--▼ページナビ(下部)-->
            <form name="page_navi_bottom" id="page_navi_bottom" action="?">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <!--{if $tpl_linemax > 0}--><!--{$smarty.capture.page_navi_bodys|smarty:nodefaults}--><!--{/if}-->
            </form>
            <!--▲ページナビ(下部)-->
    <!--{/if}-->
