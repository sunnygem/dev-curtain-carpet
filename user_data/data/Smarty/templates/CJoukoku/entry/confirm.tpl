<!--{*
 * EC-CUBE on Bootstrap3. This file is part of EC-CUBE
 *
 * Copyright(c) 2014 clicktx. All Rights Reserved.
 *
 * http://perl.no-tubo.net/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<div id="contents_wrapper" >

    <div class="member_regist_wrapper" >
        <div class="member_regist_title" >
            <h2>会員登録</h2>
        </div>
        <!--{if $smarty.const.CUSTOMER_CONFIRM_MAIL}-->
        <div class="member_regist_flow">
            <ul>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step01.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step02_on.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step03.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step04.png">
                </li>
            </ul>
        </div>

        <p class="mb30">下記の内容で送信してもよろしいでしょうか？<br />
            よろしければ、一番下の「会員登録をする」ボタンをクリックしてください。</p>

        <!--{/if}-->

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="complete">
            <!--{foreach from=$arrForm key=key item=item}-->
                <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item.value|h}-->" />
            <!--{/foreach}-->

            <div class="col-sm-12">
                <div class="member_regist_forms confirm">
                    <table>
                        <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_confirm.tpl" flgFields=3 emailMobile=false prefix=""}-->
                    </table>
                </div>
            </div>
            <div class="member_submit_wrapper">
                <div class="member_submit_buttons">
                    <button onclick="eccube.setModeAndSubmit('return', '', ''); return false;" class="reset">前のページに戻る</button>
                    <button id="send_button" name="send_button" class="confirm">
                        <div class="arrow"></div>会員登録する
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>
