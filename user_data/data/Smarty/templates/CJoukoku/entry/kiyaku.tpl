<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<div id="undercolumn">
    <div id="undercolumn_entry">
      <div class="term">新規会員登録</div>
      <p class="heading-paragraph">下記、ご利用規約をご覧の上、ご同意いただける場合は「同意して会員登録へ」ボタンを押下してください。</p>
      <div class="kiyakuBox">
        <div class="term"><!--{$tpl_title|h}--></div>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <div class="tarm">
			<h3>1.目的</h3>
			<p>この度は、カーテンじゅうたん王国の公式ネット販売「王国ネット」をご利用いただきまして誠にありがとうございます。</p>

			<h3>2.同意</h3>
			<p>王国ネット（https://www.oukoku.co.jp/shop/）」（以下「当社サイト」といいます）を適正かつ円滑に運営し、お客さまにそのサービスを快適にご利用いただくため、本規約を定めます。<br>
			お客さまが当社サイトを利用されるにあたっては、「本規約」及び「特定商取引上に関する表示」、「お支払い・送料・納期」をよくご確認ください。お客さまが当社サイトを利用された場合、「本規約」及び「特定商取引上に関する表示」、「お支払い・送料・納期」の内容に同意されたものとみなします。</p>
			<h3>3.規約の遵守</h3>
			<p>お客さまは当社サイトのご利用にあたり、「本規約」及び「特定商取引上に関する表示」、「お支払い・送料・納期」の内容を遵守するものとします。万一「本規約」および「ご利用の手引き」の内容に違反された場合、当社はお客さまによるお取引の停止や、以後のご利用をお断りできるものとします。</p>
			<h3>4.個別の規定</h3>
			<p>当社サイトの各サービスのご利用にあたり、個別の規定を定める場合がございます。その場合は各規定への同意がご利用の条件になります。</p>
			<h3>5.禁止事項</h3>
			<ul>
				<li>当社サイトのご利用に関し、次の各号の行為を行うことを禁止します。<li>
				<li>当社、他のお客さま、その他の第三者の権利、利益、名誉等を損ねること<li>
				<li>当社サイトの運営を妨げ、サービスの提供に支障をきたす恐れのある行為<li>
				<li>購入する意思なく商品を注文すること<li>
				<li>他人になりすまして取引を行うこと、虚偽の情報を入力すること<li>
				<li>法令に違反すること、公序良俗に反すること<li>
				<li>当社が定める各種規約・規定に違反すること<li>
				<li>その他当社が不正と定めること<li>

			</ul>
			<h3>6.規約の改定</h3>
			<p>当社は「本規約」および「ご利用の手引き」の内容を適宜改定できるものとします。「本規約」および「ご利用の手引き」の内容の改定は、当社サイト上への掲示をもって効力を発生するものとし、以降の本サイトの利用については改定後の「本規約」及び「特定商取引上に関する表示」、「お支払い・送料・納期」の内容が適用されるものとします。</p>
			<h3>7.著作権</h3>
			<p>当社サイト上にて提供している全ての情報および画像の著作権は、当社または情報提供者に帰属します。</p>
			<h3>8.免責</h3>

			<ul>
				<li>当社は、当社サイト上のサービスのうち、全部または一部を適宜変更・廃止できるものとし、これによりお客さまに生じた損害について、一切責任を負わないものとします。<li>
				<li>当社は、当社サイト上で無償にて提供する情報の内容について、その真偽、正確性、最新性、有用性、信頼性、適法性、第三者の権利を侵害していないこと等について一切保証いたしません。<li>
				<li>当社は、通信回線やコンピュータ等の障害によるシステムの中断・遅滞・中止・データの消失、データへの不正アクセスにより生じた損害、その他当社サイトのサービスに関してお客さまに生じた損害について、一切責任を負わないものとします。<li>
</ul>
				<h3>9.レビューについて</h3>
			<p>お客様が投稿した商品レビューが5.禁止事項のいずれかに該当することが判明した場合、その他当社が不適切と認めた場合、当社は、お客様への予告なしに当該商品レビューの全部又は一部を非表示又は削除することができるものとします。
なお、当社は、当該商品レビューを投稿されたお客様に対して、当該商品レビューの非表示又は削除の理由を開示いたしませんので、予めご了承ください。
</p>

				<h3>10.その他</h3>
			<p>お客さまと当社との関係につきましては日本法が適用されるものとします。お客さまと当社との間に万一紛争が発生した場合、両者誠意を持ってその解決に努めるものとしますが、やむを得ず訴訟を必要とする場合は第一審の専属的合意管轄裁判所は当社で決めさせていただくものとします。</p>


			</ul>
			</div>

      </div>
      <!-- .kiyakuBox -->

            <div class="btn_area tarm-btn">
                <ul>
                    <li>
                        <a href="<!--{$smarty.const.TOP_URL}-->">
                            <img class="" src="<!--{$TPL_URLPATH}-->img/common/btn_entry_cannot.png" alt="同意しない" />
                        </a>
                    </li>
                    <li>
                        <a href="<!--{$smarty.const.ENTRY_URL}-->">
                            <img class="" src="<!--{$TPL_URLPATH}-->img/common/btn_entry_agree.png" alt="同意して会員登録へ" />
                        </a>
                    </li>
                </ul>
            </div>

        </form>

    </div>
</div>
