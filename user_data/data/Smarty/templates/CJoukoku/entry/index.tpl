<div id="contents_wrapper" >

    <div class="member_regist_wrapper" >
        <div class="member_regist_title" >
            <h2>会員登録</h2>
        </div>
        <!--{if $smarty.const.CUSTOMER_CONFIRM_MAIL}-->
        <div class="member_regist_flow">
            <ul>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step01_on.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step02.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step03.png">
                </li>
                <li>
                    <img src="<!--{$TPL_URLPATH}-->img/registration/registration_step04.png">
                </li>
            </ul>
        </div>
        <div class="member_regist_remark">
            <p>項目にご入力いただき、「確認ページへ進む」ボタンを押してください。</p>
            <br>
            <p class="required">マークは必須の入力項目となります。</p>
        </div>
        <!--{/if}-->
    </div>

    <form name="form1" id="form1" class="form-horizontal" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="confirm" />
        <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_input.tpl" flgFields=3 emailMobile=false prefix=""}-->
        <div class="personal_check">
            <span>
                <input type="checkbox" class="personal" id="personal" name="personal">
                <label for="personal"></label>
            </span>
            <span class="personal_text">
                <a href="<!--{$smarty.const.ROOT_URLPATH}-->guide/privacy.php" target="_blank">個人情報の取り扱い</a>に同意する
            </span>
        </div>

        <div class="member_submit_wrapper">
            <div class="member_submit">
                <div class="member_submit_buttons">
                    <button type="reset" class="reset">リセット</button>
                    <button id="next" class="confirm">
                        <div class="arrow"></div>確認ページへ進む
                    </button>
                </div>
                <p>ご入力内容は個人情報法方針に基づきSSLという暗号化技術で保護されています。</p>
            </div>
        </div>
    </form>

</div>
