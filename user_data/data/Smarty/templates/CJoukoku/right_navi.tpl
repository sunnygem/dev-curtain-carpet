<div class="side_sns">
    <ul>
        <li><img src="/shop/user_data/packages/CJoukoku/img/icon/side_icon_instagram.png" alt="instagram"></li>
        <li><img src="/shop/user_data/packages/CJoukoku/img/icon/side_icon_facebook.png" alt="facebook"></li>
        <li><img src="/shop/user_data/packages/CJoukoku/img/icon/side_icon_twitter.png" alt="twitter"></li>
    </ul>
</div>
<div class="side_search">
    <div class="input_wrap">
        <input type="text">
        <button type="submit"></button>
    </div>
    <p><span class="search_icon"></span>こだわり検索はこちらから</p>
</div>
<div class="side_menu">
    <div class="first">
        <div class="first_ttl">
            はじめての方へ
        </div>
        <div class="shopping_guide">
            <span>
                ショッピングガイド
            </span>
        </div>
        <div class="sub">
            <ul>
                <li><a href="#">購入の流れ</a></li>
                <li><a href="#">お届けまでの流れ</a></li>
                <li><a href="#">採寸方法</a></li>
                <li><a href="#">お手入れ方法</a></li>
                <li><a href="#">ご利用ガイド</a></li>
                <li><a href="#">商品選びに迷われたら</a></li>
                <li><a href="#">よくある質問</a></li>
            </ul>
        </div>
    </div>
    <div class="side_feature">
        <ul>
            <li><a href="#"><img src="/shop/user_data/packages/CJoukoku/img/banner/side_bnr_feature_sample01.png" alt="北欧"></a></li>
            <li><a href="#"><img src="/shop/user_data/packages/CJoukoku/img/banner/side_bnr_feature_sample02.png" alt="ボタニカル"></a></li>
            <li><a href="#"><img src="/shop/user_data/packages/CJoukoku/img/banner/side_bnr_feature_sample03.png" alt="男前"></a></li>
            <li><a href="#"><img src="/shop/user_data/packages/CJoukoku/img/banner/side_bnr_feature_sample04.png" alt="フレンチカントリー"></a></li>
        </ul>
        <div class="plain_order">
            <a href="#">プレーンオーダー</a>
        </div>
    </div>
    <div class="side_category">
        <div class="side_category01">
            <div class="category_ttl"><span>カーテン</span>を選ぶ</div>
            <div class="category_img"></div>
            <div class="category_select">
                <dl>
                    <dt class="open"><span>テイスト</span>で選ぶ</dt>
                    <dd>
                        <ul>
                            <li>
                                <a href="#">
                                    <div class="category_thumb"><img src="/shop/user_data/packages/CJoukoku/img/sample/side_category_img_sample01.png" alt="モダン"></div>
                                    <div class="category_name">モダン</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="category_thumb"><img src="/shop/user_data/packages/CJoukoku/img/sample/side_category_img_sample01.png" alt="モダン"></div>
                                    <div class="category_name">モダン</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="category_thumb"><img src="/shop/user_data/packages/CJoukoku/img/sample/side_category_img_sample01.png" alt="モダン"></div>
                                    <div class="category_name">モダン</div>
                                </a>
                            </li>
                            <li class="txt">
                                <a href="#">
                                    <span>ナチュラル</span>
                                </a>
                            </li>
                            <li class="txt">
                                <a href="#">
                                    <span>ナチュラル</span>
                                </a>
                            </li>
                            <li class="txt">
                                <a href="#">
                                    <span>ナチュラル</span>
                                </a>
                            </li>
                        </ul>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="bnr_area">
            <ul>
                <li><img src="/shop/user_data/packages/CJoukoku/img/banner/side_bnr_campaign01.png" alt="遮光1級キャンペーン"></li>
                <li><img src="/shop/user_data/packages/CJoukoku/img/banner/side_bnr_campaign02.png" alt="採寸メジャープレゼント"></li>
            </ul>
        </div>
    </div>
</div>
<div class="other_category">
    <div class="side_category">
        <div class="category_ttl"><span>カーペット</span>を選ぶ</div>
        <div class="category_img"></div>
    </div>
    <div class="side_category">
        <div class="category_ttl"><span>雑貨</span>を選ぶ</div>
        <div class="category_img"></div>
    </div>
</div>
