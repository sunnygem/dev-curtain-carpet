
<!--{* こちらはお客様ごとに編集してください*}-->


<!--{if $arrCheckItems}-->
<!-- CheckedItems -->
<section id="arrCheckItems">
<div class="near">
<!--▼関連商品-->
<h2><img src="<!--{$TPL_URLPATH}-->img/product/infoIcon04.png"><span>最近チェックした商品</span></h2>
<!--{section name=cnt loop=$arrCheckItems}-->
<div class="product_item nth5">
<div class="productImage">
<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrCheckItems[cnt].product_id}-->">
<img src="<!--{$smarty.const.ROOT_URLPATH}-->resize_image.php?image=<!--{$arrCheckItems[cnt].main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrCheckItems[cnt].name|h}-->" /></a>
</div>
<div class="productContents">
<h3><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrCheckItems[cnt].product_id}-->"><!--{$arrCheckItems[cnt].name}--></a></h3>
<p class="sale_price"><span class="price"><!--{if $arrCheckItems[cnt].price02_min_inctax == $arrCheckItems[cnt].price02_max_inctax}--><!--{$arrCheckItems[cnt].price02_min_inctax|number_format}--><!--{else}--><!--{$arrCheckItems[cnt].price02_min_inctax|number_format}-->〜<!--{$arrCheckItems[cnt].price02_max_inctax|number_format}--><!--{/if}-->円</span></p>
</div>
</div>
<!-- .product_item -->
<!--{/section}-->
<!-- .near -->
</section>
<!-- / CheckedItems END -->
<!--{/if}-->
