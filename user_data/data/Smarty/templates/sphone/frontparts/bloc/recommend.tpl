
<!--{strip}-->
<!--{if count($arrBestProducts) > 0}-->
<div class="block_outer clearfix">
<div class="topListArea">
<div class="heading">
<img src="<!--{$TPL_URLPATH}-->img/top/recommend.png" alt="" class="title_icon" /><h2><span class="title">おすすめ商品</span><span class="title_rb">RECOMMENDED</span></h2>
</div>
<!-- .heading -->
<img src="<!--{$TPL_URLPATH}-->img/top/recommend_line.png" alt="" class="main_column_line" />

<div class="block_body clearfix">


<!--{foreach from=$arrBestProducts item=arrProduct name="recommend_products"}-->
<div class="product_item clearfix">
<div class="productImage">
<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" />
</a>
</div>
<div class="productContents">
<h3>
<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->"><!--{$arrProduct.name|h}--></a>
</h3>
<p class="sale_price">
<span class="price"><!--{$arrProduct.price02_min_inctax|n2s}--> 円</span>
</p>
</div>
<!-- .productContents -->
</div>
<!--{/foreach}-->

<!--
<div>
<div class="moreBtn">おすすめ商品をもっと見る</div>
</div>
-->

</div>
<!-- .block_body -->
</div>
<!-- .topListArea -->
</div>
<!-- .block_outer -->
<!--{/if}-->
<!--{/strip}-->

<script type='text/javascript'>
$(function () {
    $('.moreBtn').prevAll().hide();
    $('.moreBtn').click(function () {
        if ($(this).prevAll().is(':hidden')) {
            $(this).prevAll().slideDown();
            $(this).addClass('close');
        } else {
            $(this).prevAll().slideUp();
            $(this).removeClass('close');
        }
    });
});


$(function() {
    $('.product_item .productImage').matchHeight();
});

</script>
