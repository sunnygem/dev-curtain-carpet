<script src="<!--{$smarty.const.TOP_URL}-->js/jquery.cookie.js"></script>
<!--{php}-->
    $new_userid=makeuserid();

	$tmpstr=sprintf("<script>");
	if(!empty($_GET['aid'])){
	    $tmpstr.=sprintf('var aid="%s";',$_GET['aid']);
		$tmpstr.=sprintf('$.cookie("Shinaid",aid, { expires: 1 });');
		$_SESSION["affiliate"]["affiliateid"]=$_GET['aid'];
		if(!empty($_GET['uid'])){
			$tmpstr.=sprintf('var uid="%s";',$_GET['uid']);
			$tmpstr.=sprintf('$.cookie("Shinaidu",uid, { expires: 1 });');
			$_SESSION["affiliate"]["affiliate_user_id"]=$_GET['uid'];
		} else {
			$tmpstr.=sprintf('var uid="%s";',$new_userid);
			$tmpstr.=sprintf('$.cookie("Shinaidu",uid, { expires: 1 });');
			$_SESSION["affiliate"]["affiliate_user_id"]=$new_userid;
		}
    } else {
		if(empty($_SESSION["affiliate"]["affiliateid"])){
			$tmpstr.=<<<EOD
    if($.cookie("Shinaid")){
	    var aid=$.cookie("Shinaid");
		$.cookie("Shinaid",aid, { expires: 1 });
	    var uid=$.cookie("Shinaidu");
		$.cookie("Shinaidu",uid, { expires: 1 });
		var url = location.href ;
		var req = new XMLHttpRequest();
		req.open('GET', url+'a.php?aid='+aid+'&uid='+uid, true);
		req.send(null);
	}
EOD;
		} else {
			$tmpstr.=sprintf('var aid="%s";',$_SESSION["affiliate"]["affiliateid"]);
			$tmpstr.=sprintf('$.cookie("Shinaid",aid, { expires: 1 });');
			if(!empty($_SESSION["affiliate"]["affiliate_user_id"])){
				$tmpstr.=sprintf('var uid="%s";',$_SESSION["affiliate"]["affiliate_user_id"]);
				$tmpstr.=sprintf('$.cookie("Shinaidu",uid, { expires: 1 });');
			} else {
				$tmpstr.=sprintf('var uid="%s";',$new_userid);
				$tmpstr.=sprintf('$.cookie("Shinaidu",uid, { expires: 1 });');
				$_SESSION["affiliate"]["affiliate_user_id"]=$new_userid;
			}
		}
	}
$tmpstr.=<<<EOD
	if(aid!==undefined && aid!=""){
		var path = location.pathname;
		var currentState = history.state;
		if(location.search){
			var tmpparam=location.search+'&aid='+aid+'&uid='+uid;
		} else {
			var tmpparam='?aid='+aid+'&uid='+uid;
		}
		history.pushState(currentState,null,path+tmpparam);
	}
</script>
EOD;
    print $tmpstr;

	if(strpos($_SERVER["REQUEST_URI"],'shopping/complete.php')===false){
        $objAffiliateLog = new SC_Helper_AffiliateLog_Ex();

        // 入力データを渡す。
        $sqlval['affiliate_id'] = getaffiliateid($_SESSION["affiliate"]["affiliateid"]);
        $sqlval['affiliate_user_id'] = $_SESSION["affiliate"]["affiliate_user_id"];
        $sqlval['customer_id'] = $_SESSION["customer"]["customer_id"];
        $sqlval['conversion'] = 0;
        $sqlval['ip'] = $_SERVER["REMOTE_ADDR"];
        $sqlval['browser'] = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
        $sqlval['url'] = $_SERVER['PHP_SELF'];
        $sqlval['afrom'] = 1;
        $sqlval['apost'] = arraydump($_POST);
        $sqlval['aget'] = arraydump($_GET);
        $sqlval['del_flg'] = 0;
        $sqlval['create_date'] = date("Y/m/d H:i:s",$_SERVER["REQUEST_TIME"]);
        $sqlval['update_date'] = date("Y/m/d H:i:s",$_SERVER["REQUEST_TIME"]);
        $sqlval['creator_id'] = $member_id;
        $sqlval['update_id'] = $member_id;

        $affiliate_log_id = $objAffiliateLog->save($sqlval);

        return $affiliate_log_id;

	} else {
		$objQuery =& SC_Query_Ex::getSingletonInstance();
		$col = '*';
		$where = sprintf("order_id='%s'",$_SESSION["order_id"]);
		$table = 'dtb_order_detail';
		$objQuery->setOrder('order_detail_id ASC');
		$arrRet = $objQuery->select($col, $table, $where);

        $objAffiliateLog = new SC_Helper_AffiliateLog_Ex();

		for($i=0;$i<count($arrRet);$i++){
			// 入力データを渡す。
			$sqlval['affiliate_id'] = getaffiliateid($_SESSION["affiliate"]["affiliateid"]);
			$sqlval['affiliate_user_id'] = $_SESSION["affiliate"]["affiliate_user_id"];
			$sqlval['customer_id'] = $_SESSION["customer"]["customer_id"];
			$sqlval['conversion'] = 1;
			$sqlval['ip'] = $_SERVER["REMOTE_ADDR"];
			$sqlval['browser'] = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
			$sqlval['url'] = $_SERVER['PHP_SELF'];
			$sqlval['afrom'] = 1;
			$sqlval['apost'] = arraydump($_POST);
			$sqlval['aget'] = arraydump($_GET);
			$sqlval['del_flg'] = 0;
			$sqlval['create_date'] = date("Y/m/d H:i:s",$_SERVER["REQUEST_TIME"]);
			$sqlval['update_date'] = date("Y/m/d H:i:s",$_SERVER["REQUEST_TIME"]);
			$sqlval['creator_id'] = $member_id;
			$sqlval['update_id'] = $member_id;
			$sqlval['order_id'] = $_SESSION["order_id"];
			$sqlval['product_id'] = $arrRet[$i]["product_id"];
			$sqlval['product_class_id'] = $arrRet[$i]["product_class_id"];
			$sqlval['maker_id'] = getmakerid($arrRet[$i]["product_id"]);
			$sqlval['totalprice'] = $arrRet[$i]["price"]*$arrRet[$i]["quantity"];
			$sqlval['quantity'] = $arrRet[$i]["quantity"];

			$affiliate_log_id = $objAffiliateLog->save($sqlval);
		}
        return $affiliate_log_id;	
	}

function arraydump($array){
	if(is_array($array)){
		$result=(string)"";
		foreach($array as $key=>$val) {
			if(is_array($key)){
				$result.=arraydump($key);
			} elseif(is_array($val)){
				$result.=arraydump($val);
			} else {
				$result.=sprintf("[%s]=>(%s) ",$key,$val);
			}
		}
		return $result;
	} else {
		return "";
	}
}
function makeuserid(){
	for($flag=0,$i=0;$flag==0 && $i<100;$i++){
		$newaccessid=generate_password(1,16);
		if(checkuserid($newaccessid)){
			$flag=1;
			return $newaccessid;
		}
	}
	return FALSE;
}
function generate_password($mode,$size) {
	if($mode==0){
		$password_chars = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-<>_()#$%&';
	} elseif($mode==1) {
		$password_chars = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	} else {
		$password_chars = '1234567890';
	}
	$password_chars_count = strlen($password_chars);
	
	$data = substr(str_shuffle(str_repeat($password_chars, $size)), 0, $size);
    $pin = '';
    for ($n = 0; $n < $size; $n ++) {
        $pin .= substr($password_chars, ord(substr($data, $n, 1)) % $password_chars_count, 1);
    }
    return $pin;
}
function checkuserid($userid) {
	$objQuery =& SC_Query_Ex::getSingletonInstance();
	$col = '*';
	$where = sprintf("affiliate_user_id='%s' and del_flg=0",$userid);
	$table = 'dtb_affiliate_log';
	$objQuery->setOrder('log_id ASC');
	$arrRet = $objQuery->select($col, $table, $where);
	if(empty($arrRet[0])){
		return TRUE;
	} else {
		return FALSE;
	}
}
function getaffiliateid($affiliateid) {
	$objQuery =& SC_Query_Ex::getSingletonInstance();
	$col = 'affiliate_id';
	$where = sprintf("affiliateid='%s' and del_flg=0 and status=1",$affiliateid);
	$table = 'dtb_affiliate';
	$objQuery->setOrder('affiliate_id ASC');
	$arrRet = $objQuery->select($col, $table, $where);
	if(empty($arrRet[0])){
		return 0;
	} else {
		return $arrRet[0]['affiliate_id'];
	}
}
function getmakerid($product_id) {
	$objQuery =& SC_Query_Ex::getSingletonInstance();
	$col = 'maker_id';
	$where = sprintf("product_id=%d and del_flg=0",$product_id);
	$table = 'dtb_products';
	$objQuery->setOrder('product_id ASC');
	$arrRet = $objQuery->select($col, $table, $where);
	if(empty($arrRet[0])){
		return 0;
	} else {
		return $arrRet[0]['maker_id'];
	}
}

<!--{/php}-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113293380-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113293380-1');
</script>
