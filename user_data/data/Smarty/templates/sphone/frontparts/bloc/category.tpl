
<nav class="navbar navbar-fixed-top navbar-inverse js-fixed-header">
<div class="container">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed js-offcanvas-btn" >
<span class="hiraku-open-btn-line"></span>
<p class="caption">メニュー</p>
</button>
</div>
</div><!-- /.container -->
</nav><!-- /.navbar -->

<div class="container">

<div class="row row-offcanvas row-offcanvas-right">

<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
<div class="list-group js-offcanvas">
<!-- ドロワーメニュー中身// -->

<div class="block_outer category">
<div class="block_body">
<ul>
<li><a href="/products/list.php?category_id=1"><img src="<!--{$TPL_URLPATH}-->img/common/sp_gnav01.png"><span class="gnav_title">新商品</span></a></li>
<li><a href="/products/list.php?category_id=2"><img src="<!--{$TPL_URLPATH}-->img/common/sp_gnav02.png"><span class="gnav_title">再入荷</span></a></li>
<li><a href="/products/list.php"><img src="<!--{$TPL_URLPATH}-->img/common/sp_gnav03.png"><span class="gnav_title">商品カテゴリから探す</span></a></li>
<li><a href="/products/list.php?category_id=17"><img src="<!--{$TPL_URLPATH}-->img/common/sp_gnav04.png"><span class="gnav_title">特集から探す</span></a></li>
</ul>
<!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/bloc/category_tree_fork.tpl" children=$arrTree treeID="" display=1}-->
</div>
<!-- .block_body -->
</div>
<!-- .block_outer category -->

<div class="site_guide">
<ul>
<li><a href="/user_data/howto_guide.php"><img src="<!--{$TPL_URLPATH}-->img/common/icon01_small.png">ご利用ガイド</a></li>
<li><a href="/user_data/faq.php"><img src="<!--{$TPL_URLPATH}-->img/common/icon01_small.png">よくある質問</a></li>
<li><a href="/user_data/contact.php"><img src="<!--{$TPL_URLPATH}-->img/common/icon01_small.png">お問い合わせ</a></li>
</ul>
</div>
<!-- .site_guide -->

<div class="drawer_sns">
<ul>
<li><a href="https://twitter.com/shinchan_ec"><img src="<!--{$TPL_URLPATH}-->img/common/twitter.png"></a></li>
<!-- <li><a href=""><img src="<!--{$TPL_URLPATH}-->img/common/twitter.png"></a></li> -->
<!-- <li><a href=""><img src="<!--{$TPL_URLPATH}-->img/common/twitter.png"></a></li> -->
</ul>
</div>
<!-- .drawer_sns -->

<div class="payment">
<img src="<!--{$TPL_URLPATH}-->img/common/top_payment.png">
</div>

<div class="memberShip">
<a href="/user_data/member_rank.php"><img src="<!--{$TPL_URLPATH}-->img/common/mamberShip.png"></a>
</div>

<div class="closeBtn">
<!--<a href="#" onClick="window.close(); return false;">
<button type="button" class="closeBtn"><img src="<!--{$TPL_URLPATH}-->img/common/spmenuBottom_close.png">とじる</button>
</a>-->
</div>
<!-- //ドロワーメニュー中身 -->
<button type="button" class="close2 js-offcanvas-btn"><img src="<!--{$TPL_URLPATH}-->img/common/closeBtn.png"><span>とじる</span></button>

</div>

</div><!--/.sidebar-offcanvas-->
</div><!--/row-->

<hr>

</div><!--/.container-->
