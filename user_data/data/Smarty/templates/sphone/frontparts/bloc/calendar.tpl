
<!--{strip}-->
<div class="footerLine_img"><img src="<!--{$TPL_URLPATH}-->img/footer/footer_line.png" alt="" /></div>
<div class="block_outer">
<div class="shoping_guide container">
<h2><img src="<!--{$TPL_URLPATH}-->img/footer/guide_heading.png" alt="ショッピングガイド" /></h2>

<div class="column3">
<div class="columnbox left">
<h3><img src="<!--{$TPL_URLPATH}-->img/footer/heading_icon.png" alt="" />送料について</h3>
<p class="footer_trader">配送業者：佐川急便</p>
<p class="about value">〇全国一律<span>540円（税込）</span><br>※北海道の離島・沖縄⇒地域料660円を加算<br>※個別送料指定商品を除く</p>
<p class="about"><span>〇5,400円(税込)以上送料無料</span><br>※一部メーカー直送品は、各メーカーの配送業者に準じます。</p>
</div>
<!-- .columnbox left -->
<div class="columnbox center">
<h3><img src="<!--{$TPL_URLPATH}-->img/footer/heading_icon.png" alt="" />お支払い方法</h3>
<p>クレジット決済(一括払い)・代金引換・銀行振込・NP後払い・PayPal・Allpayご利用になれます。<br>詳しくは<a href="/user_data/payment.php">お支払い</a>についてをご参照ください。</p>
<ul>
<li>クレジットカード</li>
<li class="img margin"><img src="<!--{$TPL_URLPATH}-->img/footer/payment01.png" width="100%" alt=""></li>
<li class="float bank margin">銀行振込</li><li class="float paypal">PayPal</li><li class="float alipay">Alipay</li>
<li class="img margin"><img src="<!--{$TPL_URLPATH}-->img/footer/payment02.png" width="100%" alt=""></li>
<li class="margin">代金引換（現金決済のみ・決済手数料324円）</li>
<li>NP後払い（後払い手数料216円）</li>
<li class="img"><img src="<!--{$TPL_URLPATH}-->img/footer/payment03.png" width="100%" alt=""></li>
</ul>
</div>
<!-- .columnbox center -->
<div class="columnbox right">
<h3><img src="<!--{$TPL_URLPATH}-->img/footer/heading_icon.png" alt="" />配送について</h3>
<p>ご注文から1週間～10日ほどお届けに要する場合があります。余裕を持ってご注文ください。</p>
<p>※詳しくは<a href="/user_data/delivery.php">配送および送料について</a>をご参照ください。<br>※ご注文の混雑状況などにより、多少前後する場合がございます。</p>
</div>
<!-- .columnbox right -->
</div>
<!-- .column3 -->

<div class="column2">

<div class="column2">
<div class="columnbox left">
<div id="calender_area">
<h3><img src="/user_data/packages/shinchan/img/footer/heading_icon.png" alt="" />営業日カレンダー</h3>

<!--{strip}-->
    <div class="block_outer">
        <div id="calender_area">
            <div class="block_body">
                <!--{section name=num loop=$arrCalendar}-->
                    <!--{assign var=arrCal value=`$arrCalendar[num]`}-->
                    <!--{section name=cnt loop=$arrCal}-->
                        <!--{if $smarty.section.cnt.first}-->
                            <table>
                                <caption class="month"><!--{$arrCal[cnt].year}-->年<!--{$arrCal[cnt].month}-->月の定休日</caption>
                                <thead><tr><th>日</th><th>月</th><th>火</th><th>水</th><th>木</th><th>金</th><th>土</th></tr></thead>
                        <!--{/if}-->
                        <!--{if $arrCal[cnt].first}-->
                            <tr>
                            <!--{/if}-->
                            <!--{if !$arrCal[cnt].in_month}-->
                                <td></td>
                            <!--{elseif $arrCal[cnt].holiday}-->
                                <td class="off<!--{if $arrCal[cnt].today}--> today<!--{/if}-->"><!--{$arrCal[cnt].day}--></td>
                            <!--{else}-->
                                <td<!--{if $arrCal[cnt].today}--> class="today"<!--{/if}-->><!--{$arrCal[cnt].day}--></td>
                            <!--{/if}-->
                            <!--{if $arrCal[cnt].last}-->
                                </tr>
                        <!--{/if}-->
                    <!--{/section}-->
                    <!--{if $smarty.section.cnt.last}-->
                        </table>
                    <!--{/if}-->
                <!--{/section}-->
                <p class="information">※赤字は休業日です</p>
            </div>

        </div>
    </div>
<!--{/strip}-->


<p class="information"><span class="off"></span>出荷業務のみ</p>
<p class="information"><span class="flag"></span>休業日</p>

</div>
<!-- #calender_area -->
</div>
<!-- .columnbox left -->


<div class="columnbox right">
<h3><img src="<!--{$TPL_URLPATH}-->img/footer/heading_icon.png" alt="" />お問い合わせ</h3>
<div class="footer_mail">
<a href="mailto:info@aaa.co.jp"><img src="<!--{$TPL_URLPATH}-->img/footer/mail_icon.png" alt="" />info@aaa.co.jp</a>
</div>
<!-- .footer_mail -->
<div class="footer_tel">
<a href="&#116;&#101;&#108;&#58;&#48;&#52;&#50;&#54;&#52;&#51;&#50;&#55;&#55;&#57;"><img src="<!--{$TPL_URLPATH}-->img/footer/tel_icon.png" alt="" />042-643-2779</a>(受付時間:10～18時)
</div>
<!-- .footer_mail -->
<p>※17時以降・休業日のメール受信につきましては、翌営業日に返信させていただきますのでご了承ください。</p>
<p>※ご注文はご注文フォームからのみとさせていただいております。メール・電話でのご注文は承っておりませんのでご了承ください。</p>
</div>
<!-- .columnbox right -->
</div>
<!-- .column2 -->

</div>
<!-- .shoping_guide -->




<!--{/strip}-->
