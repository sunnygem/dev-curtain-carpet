<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->


<article class="contents_page product faq">
	<h1 class="ttl_01">よくあるご質問</h1>
	<section class="contents_box_01">
		<p>お客様からよくお問い合わせいただく<br>ご質問をまとめました。</p>
		<img src="<!--{$TPL_URLPATH}-->img/faq/faq.png" alt="?">
	</section>
	<!--/.contents_box_01-->


	<div class="contents_link_02">
		<div class="contents_link_03_box" id="sec01">
			<h2>送料について</h2>
			<dl class="acMenu">
				<dt>プレゼント用に包装できますか？</dt>
				<dd>ラッピングをご希望の場合、有料432円（税込）にて申し受けております。詳しくは、「ラッピングについて」をご確認下さい。</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>注文した後に商品の包装は出来ますか？</dt>
				<dd>申し訳ございません。残念ながら商品の包装はご注文時のみにて承っております。<br>商品の包装をご希望の場合は、包装したい商品のご注文をキャンセルしていただき、「ラッピングを希望する」にチェックをして再度ご注文いただくことで承ることができます。<br>尚、商品の包装は有料432円（税込）にて承っております。</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>メッセージカードは付きますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>のしは付けてもらえますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->
		</div>
		<div class="contents_link_03_box" id="sec02">
			<h2>ご注文について</h2>
			<dl class="acMenu">
				<dt>カートに入れると途中でエラーになってしまいます。</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>電話による注文はできますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->
		</div>
		<div class="contents_link_03_box" id="sec03">
			<h2>お支払いについて</h2>
			<dl class="acMenu">
				<dt>クレジットカード会社からの請求はいつになりますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>クレジットカードのセキュリティコードとは何ですか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->
		</div>
		<div class="contents_link_03_box" id="sec04">
			<h2>配送について</h2>
			<dl class="acMenu">
				<dt>注文した商品は、いつ届きますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>送料について教えてください</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->
		</div>
		<div class="contents_link_03_box" id="sec05">
			<h2>交換・返品について</h2>

			<dl class="acMenu">
				<dt>届いた商品が不良品だったのですが、交換できますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>届いた商品が注文した商品と違うのですが、交換できますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->
		</div>
		<div class="contents_link_03_box" id="sec06">
			<h2>会員サービスについて</h2>


			<dl class="acMenu">
				<dt>ポイントはいつ付きますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>メールアドレスやパスワードなどを変更するにはどうしたらいいですか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->
		</div>

		<div class="contents_link_03_box" id="sec07">
			<h2>会員サービスについて</h2>


			<dl class="acMenu">
				<dt>カタログはありますか？</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->

			<dl class="acMenu">
				<dt>店舗に関する問い合わせ先を教えてください。</dt>
				<dd>テキストテキスト</dd>
			</dl>
			<!--/.acMenu-->
		</div>

	</div>
		<!--/.contents_link_02-->



</article>
<!--/.contents_page-->



<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->
