<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->


<article class="contents_page product order">
	<h1 class="ttl_01">ご注文の手順について</h1>

	<section class="contents_box">
		<ul>
			<li><a href="#sec01">欲しい商品を探す</a></li>
			<li><a href="#sec02">ショッピングカートに追加する</a></li>
			<li><a href="#sec03">ご注文手続きを行う</a></li>
			<li><a href="#sec04">ご注文の際の注意事項</a></li>
			<li><a href="#sec05">ご注文後の変更・キャンセルについて</a></li>
		</ul>
	</section>
	<!--/.contents_box-->


	<div class="contents_link_02">
		<div class="contents_link_02_box" id="sec01">
			<h2>欲しい商品を探す</h2>
			<p>商品カテゴリや、お探しの商品名・キーワードなどから商品をお探しいただけます。</p>
			<div><img src="<!--{$TPL_URLPATH}-->img/common/dmy.png" alt="キャプチャ入りの説明が入ります"></div>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec02">
			<h2>ショッピングカートに追加する</h2>
			<div><img src="<!--{$TPL_URLPATH}-->img/common/dmy.png" alt="キャプチャ入りの説明が入ります"></div>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec03">
			<h2>ご注文手続きを行う</h2>
			<div><img src="<!--{$TPL_URLPATH}-->img/common/dmy.png" alt="キャプチャ入りの説明が入ります"></div>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec04">
			<h2>ご注文の際の注意事項</h2>
			<div><img src="<!--{$TPL_URLPATH}-->img/common/dmy.png" alt="キャプチャ入りの説明が入ります"></div>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec05">
			<h2>ご注文後の変更・キャンセルについて</h2>
			<div><img src="<!--{$TPL_URLPATH}-->img/common/dmy.png" alt="キャプチャ入りの説明が入ります"></div>
		</div>
		<!--/.contents_link_02_box-->
	</div>




</article>
<!--/.contents_page-->





<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->
