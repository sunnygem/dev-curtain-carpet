<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->

	
<article class="contents_page product delivery">
	<h1 class="ttl_01">配送・送料について</h1>
	<section class="contents_box">
		<ul>
			<li><a href="#sec01">送料について</a></li>
			<li><a href="#sec02">お届けまでにかかる日数</a></li>
			<li><a href="#sec03">お届け日時の指定について</a></li>
			<li><a href="#sec04">配送業者について</a></li>
			<li><a href="#sec05">お届け時ご不在の場合</a></li>
		</ul>
	</section>
	<!--/.contents_box-->
	
	
	<div class="contents_link_02" id="l01">
		<div class="contents_link_02_box" id="sec01">
			<h2>送料について</h2>
			<p>クレヨンしんちゃん公式オンラインショップのご注文は、「佐川急便」でお届けいたします。<br>ご注文1件あたり、下記の送料を頂いております。</p>
			<dl class="table_01">
				<dt class="table_01_bg">ご注文金額</dt>
				<dd class="table_01_bg">送料（梱包料込）</dd>
				<dt>5,400円（税込）未満の場合</dt>
				<dd>540円（税込）</dd>
				<dt>5,400円（税込）以上の場合</dt>
				<dd>無　料</dd>
			</dl>
			
			<ul class="kome">
				<li>特殊な商品につきましては別途送料を頂戴する場合がございます。その場合は各商品ページ上に表記いたします。</li>
				<li>配送先は日本国内に限ります。</li>
				<li>メーカー直送品は、出荷場所や商品ごとに送料を頂戴いたします。</li>
			</ul>

		</div>

		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec02">
			<h2>お届けまでにかかる日数</h2>
			<p>特に表記のない場合、商品のお届け日は以下の通りとなります。</p>
			<dl class="table_02">
				<dt>クレジットカード決済</dt>
				<dd>ご注文受付後、約3～5日で商品お届け</dd>
				<dt>代金引換決済</dt>
				<dd>ご注文受付後、約3～5日で商品お届け</dd>
				<dt>【先払い】コンビニ決済</dt>
				<dd>ご注文受付後、約3～5日で商品お届け</dd>
			</dl>
			
			<ul class="kome">
				<li>当社休業日を挟む場合はその分お届けが遅くなりますので、あらかじめご了承ください。</li>
				<li>あくまでも目安ですので、お届けが前後する場合がございます。</li>
				<li>オーダーメイド商品の場合は時間を要する場合がございます（2週間～1か月）。<br>その旨ページ上にも表示いたしますので、あらかじめご了承ください。</li>
				<li>ご注文後の売切れやお届けが遅れる場合は、メール、電話などでご連絡いたしますので、あらかじめご了承ください。</li>
				<li>配送先は、ご注文者ご本人、または別の方の住所も可能です。（代金引換決済を除く）</li>
				<li>配送業者の営業所留めでのご注文は受け付けておりません。</li>
				<li>ゴールデンウィークや年末年始等、長期休業時には、お届けまでに上記日数よりもかかる場合がございます。</li>
				<li>一部地域や繁忙期などご希望に添えない場合もございますので、あらかじめご了承ください。</li>
			</ul>
			
			<p class="text_36">※同様に残りの項目もすべて記載します。</p>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec03">
			<h2>お届け日時の指定について</h2>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec04">
			<h2>配送業者について</h2>
		</div>
		<!--/.contents_link_02_box-->
		<div class="contents_link_02_box" id="sec05">
			<h2>お届け時ご不在の場合</h2>
		</div>
		<!--/.contents_link_02_box-->
		</div>
	
	
	
	
</article>
<!--/.contents_page-->	
	
	
	
	
	
<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->	
