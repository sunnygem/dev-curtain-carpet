<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->


<article class="contents_page contact faq">
	<h1 class="ttl_01">お問い合わせ</h1>
	<section class="contents_box_01">
		<p>クレヨンしんちゃん公式オンラインショップの<br>商品やサービスに関するお問い合わせ先を<br>ご案内いたします。</p>
		<img src="<!--{$TPL_URLPATH}-->img/contact/contact.png" alt="?">
	</section>
	<!--/.contents_box_01-->

<div class="contents_link_01">
<div class="contents_link_01_box" id="sec01">
<h2>こちらのお問い合わせはクレヨンしんちゃん公式オンラインショップ専用窓口です。</h2>
<p>クレヨンしんちゃん公式オンラインショップでのお買い物や、お取り扱い商品以外に関するお問い合わせは<a href="">こちら</a>(コーポレートサイトにリンクします)をご確認ください。</p>
</div>
</div>

<div class="contents_link_02">
		<div class="contents_link_02_box" id="sec02">
			<h2>ウェブからのお問い合わせ</h2>
			<p>当ウェブサイトからのお問い合わせを希望されるお客様は、下の「お問い合わせフォームへ」ボタンをクリックして、お問い合わせフォームからお問い合わせください。</p>
      <p>※お問い合わせの内容により、ご返信を差し上げるまでにしばらくお時間をいただく場合がございます。<br>※土日祝日の場合は翌営業日以降となります。また、平日の場合でも時間帯によっては回答が翌営業日以降となる場合がございますあらかじめご了承ください。</p>
			</div>
			<!--./contents_link_02_box02-->
	</div>


<div class="btn_area contact">
    <ul>
        <li class="entry">
            <a href="/contact/">お問い合わせフォームへ</a>
        </li>
    </ul>
</div>
<!-- .btn_area -->

<div class="contents_link_03">
		<div class="contents_link_03_box" id="sec03">
			<h2>お電話でのお問い合わせ</h2>
      <p class="callcenter">【クレヨンしんちゃん公式オンラインショップコールセンター】</p>
      <p><a href="&#116;&#101;&#108;&#58;&#48;&#52;&#50;&#54;&#52;&#51;&#50;&#55;&#55;&#57;" class="tel"><img src="<!--{$TPL_URLPATH}-->img/contact/contactTel.png" alt="">042-643-2779</a></p>
      <p class="acceptance">受付時間(土・日・祝日・年末年始を除く9:30～18:30)<br>※通話料がかかります。あらかじめご了承ください。<br>※おかけ間違いには十分ご注意ください。<br>※コールセンターの営業時間外は、<a href="/contact">お問い合わせフォーム</a>よりお願いいたします。<br>(ただし、回答は翌営業日以降となる場合がございます。あらかじめご了承ください。)</p>
			</div>
			<!--./contents_link_02_box02-->
	</div>


</article>
<!--/.contents_page-->



<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->
