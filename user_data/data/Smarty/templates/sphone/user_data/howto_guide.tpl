<!--▼▼▼▼▼▼▼▼ここから新規作成コンテンツ▼▼▼▼▼▼▼▼-->

	
<article class="contents_page">
	<h1 class="ttl_01">ご利用ガイド</h1>
	
	<section class="contents_box">
		<p>クレヨンしんちゃん公式オンラインショップのご利用にあたり<br>
お困りのことや、疑問は下記よりご確認ください。</p>
		<p><a href="/abouts/">クレヨンしんちゃん公式オンラインショップについて</a></p>
	</section>
	<!--/.contents_box-->
	
	
	<div class="contents_link_01">
		<div class="contents_link_01_box">
			<h2><a href="/user_data/product.php">商品について</a></h2><!---->
			<ul>
				<li><a href="/user_data/product.php#sec01">価格表示について</a></li>
				<li><a href="/user_data/product.php#sec02">商品サイズについて</a></li>
				<li><a href="/user_data/product.php#sec03">商品画像について</a></li>
				<li><a href="/user_data/product.php#sec04">品切れ商品について</a></li>
				<li><a href="/user_data/product.php#sec05">ラッピングについて</a></li>
			</ul>
		</div>
		<!--/.contents_link_01_box-->
		<div class="contents_link_01_box">
			<h2><a href="/user_data/order.php">ご注文の手順について</a></h2><!---->
			<ul>
				<li><a href="/user_data/order.php#sec01">欲しい商品を探す</a></li>
				<li><a href="/user_data/order.php#sec02">ショッピングカートに商品を追加する</a></li>
				<li><a href="/user_data/order.php#sec03">ご注文手続きを行う</a></li>
				<li><a href="/user_data/order.php#sec04">ご注文の際の注意事項</a></li>
				<li><a href="/user_data/order.php#sec05">ご注文後の変更・キャンセルについて</a></li>
			</ul>
		</div>
		<!--/.contents_link_01_box-->
		<div class="contents_link_01_box">
			<h2><a href="/user_data/payment.php">お支払いについて</a></h2><!---->
			<ul>
				<li><a href="/user_data/payment.php#sec01">クレジットカード決済</a></li>
				<li><a href="/user_data/payment.php#sec02">代金引換</a></li>
				<li><a href="/user_data/payment.php#sec03">【先払い】コンビニ決済</a></li>
				<li><a href="/user_data/payment.php#sec04">【先払い】ネットバンキング</a></li>
				<li><a href="/user_data/payment.php#sec05">【後払い】ニッセン後払い</a></li>
			</ul>
		</div>
		<!--/.contents_link_01_box-->
		<div class="contents_link_01_box">
			<h2><a href="/user_data/delivery.php">配送・送料について</a></h2><!---->
			<ul>
				<li><a href="/user_data/delivery.php#sec01">送料について</a></li>
				<li><a href="/user_data/delivery.php#sec02">お届けまでに掛かる日数</a></li>
				<li><a href="/user_data/delivery.php#sec03">お届け日時の指定について</a></li>
				<li><a href="/user_data/delivery.php#sec04">配送業者について</a></li>
				<li><a href="/user_data/delivery.php#sec05">お届け時ご不在の場合</a></li>
			</ul>
		</div>
		<!--/.contents_link_01_box-->
		<div class="contents_link_01_box">
			<h2><a href="/user_data/exchange.php">返品・交換について</a></h2><!---->
			<ul>
				<li><a href="/user_data/exchange.php#sec01">返品・交換をご希望の場合</a></li>
				<li><a href="/user_data/exchange.php#sec02">返品時の送料について</a></li>
			</ul>
		</div>
		<!--/.contents_link_01_box-->
		<div class="contents_link_01_box">
			<h2><a href="/user_data/member.php">会員について</a></h2>
			<ul>
				<li><a href="/user_data/member.php#sec01">会員特典</a></li>
				<li><a href="/user_data/member.php#sec02">ポイントの詳細</a></li>
				<li><a href="/user_data/member.php#sec03">クーポンの詳細</a></li>
				<li><a href="/user_data/member_rank.php">会員ランクについて</a></li>
			</ul>
		</div>
		<!--/.contents_link_01_box-->
		<div class="contents_link_01_box">
			<h2><a href="/user_data/mypage.php">マイページについて</a></h2>
			<ul>
				<li><a href="/user_data/mypage.php#sec01">マイページとは</a></li>
				<li><a href="/user_data/mypage.php#sec02">マイページの主な機能</a></li>
			</ul>
		</div>
		<!--/.contents_link_01_box-->

	</div>
	
	
	
	
</article>
<!--/.contents_page-->	
	
	
	
	
	
<!--▲▲▲▲▲▲▲▲ここまで新規作成コンテンツ▼▼▼▼▼▼▼▼-->	
