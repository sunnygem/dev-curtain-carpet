
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/user_data/packages/shinchan/css/slick.css" type="text/css" media="all" />
<link rel="stylesheet" href="/user_data/packages/shinchan/css/slick-theme.css" type="text/css" media="all" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/hiraku.css" type="text/css" media="all" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/hiraku.style.css" type="text/css" media="all" />
<script src="/user_data/packages/shinchan/js/slick.min.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/hiraku.js"></script>

<header class="container">
<div id="headerInner">
<h1 id="headerLogo"><a href="<!--{$smarty.const.HTTPS_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/common/logo.png" alt="クレヨンしんちゃん公式オンラインショップ"></a></h1>

<form name="header_login_form" id="header_login_form" method="post" action="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php" onsubmit="return fnCheckLogin('header_login_form')">
<input type="hidden" name="mode" value="login" />
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="url" value="<!--{$smarty.server.PHP_SELF|h}-->" />


<!--{if $tpl_login}-->
<ul class="headerMenu">
<li class="mypage"><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php" onclick="fnFormModeSubmit('header_login_form', 'logout', '', ''); return false;"><img src="<!--{$TPL_URLPATH}-->img/common/key_sp.png" alt="ショッピングカート"><span class="caption">ログアウト</span></a></li>


<li class="ecBtn_cart"><a href="<!--{$smarty.const.CART_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/common/cart_small.png" alt="ショッピングカート"><span class="cartValue"><!--{$tpl_cart_quantity_total|number_format|default:0}--></span></a></li>
</ul>
<!-- .ecBtn_cart -->
<!--{else}-->
<ul class="headerMenu">
<li class="mypage"><a href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php"><img src="<!--{$TPL_URLPATH}-->img/common/key_sp.png" alt="ログイン"><span class="caption">ログイン</span></a></li>
<li class="ecBtn_cart"><a href="<!--{$smarty.const.CART_URL}-->"><img src="<!--{$TPL_URLPATH}-->img/common/cart_small.png" alt="ショッピングカート"><span class="cartValue"><!--{$tpl_cart_quantity_total|number_format|default:0}--></span></a></li>
</ul>
<!-- .ecBtn_cart -->
<!--{/if}-->

</form>

</div>
<div class="searchBox">
  <form name="search_form" id="search_form" method="get" action="/products/list.php">
  <input type="text" class="searchBox" name="name" maxlength="50" value="" placeholder="キーワードを入力してね！">
  <input type="image" class="searchBtn" src="/user_data/packages/shinchan/img/button/btn_bloc_search.jpg" alt="検索" name="search">
  </form>
</div>


</header>

<script type="text/javascript">
$(".js-offcanvas").hiraku({
btn: ".js-offcanvas-btn",
direction: "left",
breakpoint: 813
});
</script>
