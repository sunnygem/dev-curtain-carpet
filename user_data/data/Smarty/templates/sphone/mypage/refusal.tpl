<div id="mypagecolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--{include file=$tpl_navi}-->
    <div id="mycontents_area">
        <h3>退会手続き</h3>
        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <div id="complete_area">
                <div class="message">会員を退会された場合には、現在保存されている購入履歴や、<br />
                お届け先などの情報は、全て削除されますがよろしいでしょうか？</div>
                <div class="message_area">
                    <p>退会手続きが完了した時点で、現在保存されている購入履歴や、<br />
                    お届け先等の情報は全てなくなりますのでご注意ください。</p>
                    <div class="btn_area">
                        <ul>
                            <li>
                                <input type="submit" alt="会員退会を行う" name="refusal" id="refusal" value="会員退会手続きへ" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
