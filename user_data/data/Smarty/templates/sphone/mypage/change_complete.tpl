<div id="mypagecolumn">
    <h2 class="title">会員登録内容変更</h2>
    <!--{include file=$tpl_navi}-->
    <div id="mycontents_area">
        <h3>会員登録内容変更</h3>

        <div id="complete_area">
            <div class="message">
                会員登録内容の変更が完了いたしました。<br />
            </div>
            <p>今後ともご愛顧賜りますようよろしくお願い申し上げます。</p>
        </div>
    </div>
</div>
