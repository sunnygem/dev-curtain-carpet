<script>
    function ajaxLogin() {
        var postData = new Object;
        postData['<!--{$smarty.const.TRANSACTION_ID_NAME}-->'] = "<!--{$transactionid}-->";
        postData['mode'] = 'login';
        postData['login_email'] = $('input[type=email]').val();
        postData['login_pass'] = $('input[type=password]').val();
        postData['url'] = $('input[name=url]').val();

        $.ajax({
            type: "POST",
            url: "<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php",
            data: postData,
            cache: false,
            dataType: "json",
            error: function(XMLHttpRequest, textStatus, errorThrown){
                alert(textStatus);
            },
            success: function(result){
                if (result.success) {
                    location.href = result.success;
                } else {
                    alert(result.login_error);
                }
            }
        });
    }
</script>

<div class="mypageLogin">
<h1><img src="<!--{$TPL_URLPATH}-->img/mypage/mypageLogin_h.png"><span>マイページログイン</span></h1>

<div class="container mypage login">
    <form name="login_mypage" id="login_mypage" method="post" action="javascript:;" onsubmit="return ajaxLogin();">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="login" />
        <input type="hidden" name="url" value="<!--{$smarty.server.REQUEST_URI|h}-->" />

<div class="login_area left">
<h2><img src="<!--{$TPL_URLPATH}-->img/mypage/star.png"><span>会員の方</span></h2>
<p class="login_paragraph">ログインID（またはご登録のメールアドレス）とパスワードをご入力の上、ログインしてください。</p>

<div class="inputbox">
  <dl class="formlist clearfix">
    <!--{assign var=key value="login_email"}-->
    <dt>IDもしくはメールアドレス</dt>
    <dd>
      <span class="attention"><!--{$arrErr[$key]}--></span>
      <input type="email" name="<!--{$key}-->" value="<!--{$tpl_login_email|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" class="mailtextBox data-role-none" placeholder="メールアドレス" />
      <p class="login_memory">
    </dd>
  </dl>
  <!-- .formlist -->
  <dl class="formlist clearfix">
    <!--{assign var=key value="login_pass"}-->
    <dt>パスワード</dt>
    <dd>
      <span class="attention"><!--{$arrErr[$key]}--></span>
      <input type="password" name="<!--{$key}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" class="box300" placeholder="パスワード" />
    </dd>
    </dl>
  <!-- .formlist -->
  <div class="btn_area login">
    <ul>
      <li>
        <input type="submit" alt="ログイン" name="log" id="log" value="ログイン" />
      </li>
    </ul>
  </div>
</div>
<!-- .inputbox -->

<p class="login_forget">登録したID(メールアドレス)・パスワードをお忘れの方は<a href="<!--{$smarty.const.HTTPS_URL}-->forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->" onclick="eccube.openWindow('<!--{$smarty.const.HTTPS_URL}-->forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->','forget','600','460',{scrollbars:'no',resizable:'no'}); return false;" target="_blank">こちら</a><br />
※メールアドレスを忘れた方は、お手数ですが、<a href="<!--{$smarty.const.ROOT_URLPATH}-->contact/<!--{$smarty.const.DIR_INDEX_PATH}-->">お問い合わせページ</a>からお問い合わせください。</p>

<div class="globalsign">
<!--
<a href=""><img src="<!--{$TPL_URLPATH}-->img/mypage/globalsign.jpg"></a>-->

<table width="135" border="0" cellpadding="2" cellspacing="0" title="このマークは、SSL/TLSで通信を保護している証です。">
<tr>
<td width="135" align="center" valign="top">
<!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. --> <SCRIPT LANGUAGE="JavaScript"  TYPE="text/javascript" SRC="//smarticon.geotrust.com/si.js"></SCRIPT>
<!-- end  GeoTrust Smart Icon tag -->
<a href="https://www.geotrust.co.jp/ssl_guideline/ssl_beginners/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 12px 'ＭＳ ゴシック',sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">SSLとは？</a></td>
</tr>
</table>

</div>
</div>
<!-- .login_area -->
</form>


<form name="member_form2" id="member_form2" method="post" action="?">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="nonmember" />

<div class="login_area right">
<h2><img src="<!--{$TPL_URLPATH}-->img/mypage/star.png"><span>はじめてご利用される方</span></h2>
<p class="login_paragraph">クレヨンしんちゃん公式オンラインショップの会員になると特典いっぱい！<br>登録すれば次回のお買い物も簡単になります。年会費・登録料・更新料は一切かかりませんので、ぜひご登録してくださいね！</p>

<div class="benefits">
<div class="contentBox">
<h3><div class="frame"><img src="<!--{$TPL_URLPATH}-->img/mypage/deals.png"><img src="<!--{$TPL_URLPATH}-->img/mypage/benefits_h.png"></div></h3>
<div class="imgArea">
<ul class="above">
<li><img src="<!--{$TPL_URLPATH}-->img/mypage/illustration01.png"></li>
<li><img src="<!--{$TPL_URLPATH}-->img/mypage/benefitsImg01.png"></li>
<li><img src="<!--{$TPL_URLPATH}-->img/mypage/illustration02.png"></li>
</ul>
<ul class="bottom">
<li><img src="<!--{$TPL_URLPATH}-->img/mypage/benefitsImg02.png"></li>
<li><img src="<!--{$TPL_URLPATH}-->img/mypage/illustration03.png"></li>
<li><img src="<!--{$TPL_URLPATH}-->img/mypage/benefitsImg03.png"></li>
</ul>
</div>
<!-- .imgArea -->
<div class="entry">
<a rel="external" href="<!--{$smarty.const.ROOT_URLPATH}-->entry/kiyaku.php">
<img src="<!--{$TPL_URLPATH}-->img/mypage/benefitsBtn.png">
</a>
</div>
</div>
<!-- .contentBox -->

<div class="memberRank">
<a href="/user_data/member_rank.php">
<img src="<!--{$TPL_URLPATH}-->img/mypage/memberRank.png">
</a>
</div>

</div>
<!-- .benefits -->
</div>

<!-- .login_area -->
</form>

</div>
<!-- .container -->




</div>
<!-- .mypageLogin -->



<script type="text/javascript">
$(function() {
    $('.login_area > p.login_paragraph').matchHeight();
});
</script>
