<div id="mypagecolumn">
    <h2 class="title">会員登録内容変更</h2>
    <!--{include file=$tpl_navi}-->
    <div id="mycontents_area">
        <h3>会員登録内容変更</h3>
        <p class="prompt">下記の内容で送信してもよろしいでしょうか？<br />
            よろしければ、一番下の「完了ページへ」ボタンをクリックしてください。</p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="complete" />
            <input type="hidden" name="customer_id" value="<!--{$arrForm.customer_id.value|h}-->" />
            <!--{foreach from=$arrForm key=key item=item}-->
                <!--{if $key ne "mode" && $key ne "subm"}-->
                <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item.value|h}-->" />
                <!--{/if}-->
            <!--{/foreach}-->
            <table summary=" " class="delivname">
                <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_confirm.tpl" flgFields=3 emailMobile=true prefix=""}-->
            </table>

            <div class="btn_area btn2 margin">
                <ul>
                    <li class="return">
                        <a href="?" onclick="eccube.setModeAndSubmit('return', '', ''); return false;">
                            戻る</a>
                    </li>
                    <li class="entry">
                        <input type="submit" class="hover_change_image" src="<!--{$TPL_URLPATH}-->img/button/btn_complete.jpg" alt="送信" name="complete" id="complete" value="完了ページへ"/>
                    </li>
                </ul>
            </div>
        </form>
    </div>
</div>
