
<div id="mypagecolumn">
    <h2 class="title">会員登録内容変更</h2>
    <!--{include file=$tpl_navi}-->
    <div id="mycontents_area">
        <h3>会員登録内容変更</h3>
        <p class="prompt">下記項目にご入力ください。「<span class="attention">※</span>」印は入力必須項目です。<br />
            入力後、一番下の「確認ページへ」ボタンをクリックしてください。</p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="customer_id" value="<!--{$arrForm.customer_id.value|h}-->" />
            <dl summary="会員登録内容変更 " class="delivname entry">
                <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_input.tpl" flgFields=3 emailMobile=true prefix=""}-->
            </dl>
            <div class="btn_area">
                <ul>
                    <li>
                        <span>></span><input type="submit" class="hover_change_image" alt="確認ページへ" name="refusal" id="refusal" value="確認ページへ"/>
                    </li>
                </ul>
            </div>
        </form>
    </div>
</div>
