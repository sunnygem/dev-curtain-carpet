
<div id="undercolumn">

<div class="contactStatus">
<img src="<!--{$TPL_URLPATH}-->img/contact/contactStatus01.png">
</div>
<!-- .contactStatus -->
    <h2 class="title">お問い合わせ内容の入力</h2>

    <div id="undercolumn_contact">

        <p class="prompt">必須項目をご入力いただき、「内容を確認する」ボタンを押してください。<br />お問い合わせ内容は、なるべく具体的にご記入いただきますようお願いいたします。</p>
        <p class="prompt">※お問い合わせについてはメールでご返信を差し上げております。ご連絡がつながらない場合やメールではわかりにくい場合には、お電話でお問い合わせをさせていただく場合がございます。<br>※お問い合わせの内容により、ご返信にしばらくお時間をいただく場合がございます。あらかじめご了承ください。</p>

        <div class="section contactMany">
        <h3><img src="/user_data/packages/shinchan/img/user_data/star_ki.png" alt=""><span>お問い合わせの多いご質問</span></h3>
        <ul>
        <li><a href="/products/list.php?category_id=1302"><span>パスワードを忘れてしまったのですが…</span></a></li>
        <li><a href="/products/list.php?category_id=1302"><span>パスワードを忘れた時の質問を忘れました</span></a></li>
        <li><a href="/products/list.php?category_id=1302"><span>メールアドレスやパスワードなどを変更するにはどうしたらいいですか？</span></a></li>
        <li><a href="/products/list.php?category_id=1302"><span>商品の追加・変更・キャンセルはできますか？</span></a></li>
        <li><a href="/products/list.php?category_id=1302"><span>注文した後に商品の梱包はできますか？</span></a></li>
        <li><a href="/products/list.php?category_id=1302"><span>注文したらいつ頃届きますか？</span></a></li>
        </ul>
        </div>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />

            <dl summary="お問い合わせ" class="entry">
                    <dt>お問い合わせ内容<span class="attention">必須</span><br />
                    <!-- <span class="mini">（全角<!--{$smarty.const.MLTEXT_LEN}-->字以下）</span></dt> -->
                    <dd>
                        <span class="attention"><!--{$arrErr.contents}--></span>
                        <textarea name="contents" class="box380" cols="60" rows="20" style="<!--{$arrErr.contents.value|h|sfGetErrorColor}-->; ime-mode: active;"><!--{"\n"}--><!--{$arrForm.contents.value|h}--></textarea>
                    </dd>
                    <dt>お名前<span class="attention">必須</span></dt>
                    <dd>
                        <span class="attention"><!--{$arrErr.name01}--><!--{$arrErr.name02}--></span>
                        姓&nbsp;<input type="text" class="box120" name="name01" value="<!--{$arrForm.name01.value|default:$arrData.name01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.name01|sfGetErrorColor}-->; ime-mode: active;" />　
                        名&nbsp;<input type="text" class="box120" name="name02" value="<!--{$arrForm.name02.value|default:$arrData.name02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.name02|sfGetErrorColor}-->; ime-mode: active;" />
                    </dd>
                    <dt>お名前(カナ)<span class="attention">必須</span></dt>
                    <dd>
                        <span class="attention"><!--{$arrErr.kana01}--><!--{$arrErr.kana02}--></span>
                        セイ&nbsp;<input type="text" class="box120" name="kana01" value="<!--{$arrForm.kana01.value|default:$arrData.kana01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.kana01|sfGetErrorColor}-->; ime-mode: active;" />　
                        メイ&nbsp;<input type="text" class="box120" name="kana02" value="<!--{$arrForm.kana02.value|default:$arrData.kana02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.kana02|sfGetErrorColor}-->; ime-mode: active;" />
                    </dd>
                    <dt>メールアドレス(半角)<span class="attention">必須</span></dt>
                    <dd>
                        <span class="attention"><!--{$arrErr.email}--><!--{$arrErr.email02}--></span>
                        <p class="caption">※クレヨンしんちゃん公式オンラインショップ会員の方は、登録されているメールアドレスをご記入ください。</p>
                        <input type="text" class="box380 top" name="email" value="<!--{$arrForm.email.value|default:$arrData.email|h}-->" style="<!--{$arrErr.email|sfGetErrorColor}-->; ime-mode: disabled;" /><br />
                        <p class="cation">※携帯電話のアドレスでも登録が可能です<br>携帯電話をご利用の場合は、「@crayonstore.co.jp」のドメイン指定受信の設定を行ってください。<br>ただし、お客様のメールアドレスが以下に該当している場合ご登録がいただけません。<br>「.」(ドット)が連続しているメールアドレス<br>@マークの直前に「.」(ドット)があるメールアドレス</p>
                        <!--{* ログインしていれば入力済みにする *}-->
                        <!--{if $smarty.session.customer}-->
                        <!--{assign var=email02 value=$arrData.email}-->
                        <!--{/if}-->
                        <p class="mini"><span class="attention">確認のため、もう一度入力してください。</span></p>
                        <input type="text" class="box380" name="email02" value="<!--{$arrForm.email02.value|default:$email02|h}-->" style="<!--{$arrErr.email02|sfGetErrorColor}-->; ime-mode: disabled;" /><br />
                    </dd>
                    <!-- <dt>郵便番号</dt>
                    <dd>
                        <span class="attention"><!--{$arrErr.zip01}--><!--{$arrErr.zip02}--></span>
                        <p class="top">
                            〒&nbsp;
                            <input type="text" name="zip01" class="box60" value="<!--{$arrForm.zip01.value|default:$arrData.zip01|h}-->" maxlength="<!--{$smarty.const.ZIP01_LEN}-->" style="<!--{$arrErr.zip01|sfGetErrorColor}-->; ime-mode: disabled;" />&nbsp;-&nbsp;
                            <input type="text" name="zip02" class="box60" value="<!--{$arrForm.zip02.value|default:$arrData.zip02|h}-->" maxlength="<!--{$smarty.const.ZIP02_LEN}-->" style="<!--{$arrErr.zip02|sfGetErrorColor}-->; ime-mode: disabled;" />　
                            <a href="http://www.post.japanpost.jp/zipcode/" target="_blank"><span class="mini">郵便番号検索</span></a>
                        </p>
                        <p class="zipimg">
                            <a href="javascript:eccube.getAddress('<!--{$smarty.const.INPUT_ZIP_URLPATH}-->', 'zip01', 'zip02', 'pref', 'addr01');">
                                <img src="<!--{$TPL_URLPATH}-->img/button/btn_address_input.jpg" alt="住所自動入力" /></a>
                            <span class="mini">&nbsp;郵便番号を入力後、クリックしてください。</span>
                        </p>
                    </dd> -->
                    <!-- <dt>住所</dt>
                    <dd>
                        <span class="attention"><!--{$arrErr.pref}--><!--{$arrErr.addr01}--><!--{$arrErr.addr02}--></span>

                        <select name="pref" style="<!--{$arrErr.pref|sfGetErrorColor}-->">
                        <option value="">都道府県を選択</option><!--{html_options options=$arrPref selected=$arrForm.pref.value|default:$arrData.pref|h}--></select>

                        <p>
                            <input type="text" class="box380" name="addr01" value="<!--{$arrForm.addr01.value|default:$arrData.addr01|h}-->" style="<!--{$arrErr.addr01|sfGetErrorColor}-->; ime-mode: active;" /><br />
                            <!--{$smarty.const.SAMPLE_ADDRESS1}-->
                        </p>

                        <p>
                            <input type="text" class="box380" name="addr02" value="<!--{$arrForm.addr02.value|default:$arrData.addr02|h}-->" style="<!--{$arrErr.addr02|sfGetErrorColor}-->; ime-mode: active;" /><br />
                            <!--{$smarty.const.SAMPLE_ADDRESS2}-->
                        </p>

                        <p class="mini"><span class="attention">住所は2つに分けてご記入ください。マンション名は必ず記入してください。</span></p>
                    </dd> -->
                    <dt>電話番号</dt>
                    <dd>
                        <span class="attention"><!--{$arrErr.tel01}--><!--{$arrErr.tel02}--><!--{$arrErr.tel03}--></span>
                        <input type="text" class="box60" name="tel01" value="<!--{$arrForm.tel01.value|default:$arrData.tel01|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="<!--{$arrErr.tel01|sfGetErrorColor}-->; ime-mode: disabled;" />&nbsp;-&nbsp;
                        <input type="text" class="box60" name="tel02" value="<!--{$arrForm.tel02.value|default:$arrData.tel02|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="<!--{$arrErr.tel02|sfGetErrorColor}-->; ime-mode: disabled;" />&nbsp;-&nbsp;
                        <input type="text" class="box60" name="tel03" value="<!--{$arrForm.tel03.value|default:$arrData.tel03|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="<!--{$arrErr.tel03|sfGetErrorColor}-->; ime-mode: disabled;" />
                    </dd>
            </dl>

            <div class="btn_area">
                <ul>
                    <li class="entry">
                        <input type="submit" src="<!--{$TPL_URLPATH}-->img/button/btn_confirm.jpg" alt="確認ページへ" name="confirm" value="確認ページへ" />
                    </li>
                </ul>
            </div>

        </form>
    </div>
</div>
