<div id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div id="undercolumn_contact">
        <p class="prompt">下記入力内容で送信してもよろしいでしょうか？<br />
            よろしければ、一番下の「完了ページへ」ボタンをクリックしてください。</p>
        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="complete" />
            <!--{foreach key=key item=item from=$arrForm}-->
                <!--{if $key ne 'mode'}-->
                    <input type="hidden" name="<!--{$key}-->" value="<!--{$item.value|h}-->" />
                <!--{/if}-->
            <!--{/foreach}-->
            <dl summary="お問い合わせ内容確認" class="entry">
                    <dt>お名前</dt>
                    <dd><!--{$arrForm.name01.value|h}-->　<!--{$arrForm.name02.value|h}--></dd>
                    <dt>お名前(フリガナ)</dt>
                    <dd><!--{$arrForm.kana01.value|h}-->　<!--{$arrForm.kana02.value|h}--></dd>
                    <dt>郵便番号</dt>
                    <dd>
                        <!--{if strlen($arrForm.zip01.value) > 0 && strlen($arrForm.zip02.value) > 0}-->
                            〒<!--{$arrForm.zip01.value|h}-->-<!--{$arrForm.zip02.value|h}-->
                        <!--{/if}-->
                    </dd>
                    <dt>住所</dt>
                    <dd><!--{$arrPref[$arrForm.pref.value]}--><!--{$arrForm.addr01.value|h}--><!--{$arrForm.addr02.value|h}--></dd>
                    <dt>電話番号</dt>
                    <dd>
                        <!--{if strlen($arrForm.tel01.value) > 0 && strlen($arrForm.tel02.value) > 0 && strlen($arrForm.tel03.value) > 0}-->
                            <!--{$arrForm.tel01.value|h}-->-<!--{$arrForm.tel02.value|h}-->-<!--{$arrForm.tel03.value|h}-->
                        <!--{/if}-->
                    </dd>
                    <dt>メールアドレス</dt>
                    <dd><a href="mailto:<!--{$arrForm.email.value|escape:'hex'}-->"><!--{$arrForm.email.value|escape:'hexentity'}--></a></dd>
                    <dt>お問い合わせ内容</dt>
                    <dd><!--{$arrForm.contents.value|h|nl2br}--></dd>
            </dl>
            <div class="btn_area btn2">
                <ul>
                    <li class="return">
                        <a href="?" onclick="eccube.setModeAndSubmit('return', '', ''); return false;">戻る</a>
                    </li>
                    <li class="entry">
                        <input type="submit" class="hover_change_image" src="<!--{$TPL_URLPATH}-->img/button/btn_complete.jpg" alt="送信" name="send" id="send" value="送信" />
                    </li>
                </ul>
            </div>

        </form>
    </div>
</div>
