<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=<!--{$smarty.const.CHAR_CODE}-->" />

<title><!--{$arrSiteInfo.shop_name|h}--><!--{if $tpl_subtitle|strlen >= 1}--> / <!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--> / <!--{$tpl_title|h}--><!--{/if}--></title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
<meta name="format-detection" content="telephone=no">
<!--{if $arrPageLayout.author|strlen >= 1}-->
<meta name="author" content="<!--{$arrPageLayout.author|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.description|strlen >= 1}-->
<meta name="description" content="<!--{$arrPageLayout.description|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.keyword|strlen >= 1}-->
<meta name="keywords" content="<!--{$arrPageLayout.keyword|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.meta_robots|strlen >= 1}-->
<meta name="robots" content="<!--{$arrPageLayout.meta_robots|h}-->" />
<!--{/if}-->

<link rel="shortcut icon" href="<!--{$TPL_URLPATH}-->img/common/favicon.ico" />
<link rel="icon" type="image/vnd.microsoft.icon" href="<!--{$TPL_URLPATH}-->img/common/favicon.ico" />
<!--{* 共通CSS *}-->
<link rel="stylesheet" media="only screen" href="<!--{$TPL_URLPATH}-->css/import.css" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/diff-slider.css" type="text/css" media="all" />
<link rel="stylesheet" media="only screen" href="<!--{$TPL_URLPATH}-->css/common.css" />
<script src="<!--{$TPL_URLPATH}-->js/jquery-1.7.min.js"></script>
<script src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/eccube.sphone.js"></script>
<!-- #2342 次期メジャーバージョン(2.14)にてeccube.legacy.jsは削除予定.モジュール、プラグインの互換性を考慮して2.13では残します. -->
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.legacy.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/eccube.sphone.legacy.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/jquery.biggerlink.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/diff-slider.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/jquery.matchHeight-min.js"></script>
<script>//<![CDATA[
$(function(){
$('.header_navi li, .recommendblock, .list_area, .newslist li, .bubbleBox, .arrowBox, .category_body, .navBox li,#mypagecolumn .cartitemBox').biggerlink();
});
//]]></script>

<!--{* jQuery Mobile *}-->
<script src="<!--{$TPL_URLPATH}-->js/config.js"></script>

<!--{* スマートフォンカスタマイズ用CSS *}-->
<link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.colorbox/colorbox.css" type="text/css" media="all" />

<link rel="stylesheet" media="only screen" href="<!--{$TPL_URLPATH}-->css/common.css" />

<!--{* スマートフォンカスタマイズ用JS *}-->
<script src="<!--{$TPL_URLPATH}-->js/jquery.autoResizeTextAreaQ-0.1.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/jquery.flickslide.js"></script>
<script src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.cookie.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/common.js"></script>

<!--{* iPhone用アイコン画像 *}-->
<link rel="apple-touch-icon" href="<!--{$TPL_URLPATH}-->img/common/apple-touch-icon.png" />

<script type="text/javascript">//<![CDATA[
<!--{$tpl_javascript}-->
$(function(){
<!--{$tpl_onload}-->
});
//]]></script>

<!--{* ▼Head COLUMN *}-->
<!--{if $arrPageLayout.HeadNavi|@count > 0}-->
<!--{foreach key=HeadNaviKey item=HeadNaviItem from=$arrPageLayout.HeadNavi}-->
<!--{* ▼<!--{$HeadNaviItem.bloc_name}--> *}-->
<!--{if $HeadNaviItem.php_path != ""}-->
<!--{include_php file=$HeadNaviItem.php_path items=$HeadNaviItem}-->
<!--{else}-->
<!--{include file=$HeadNaviItem.tpl_path items=$HeadNaviItem}-->
<!--{/if}-->
<!--{* ▲<!--{$HeadNaviItem.bloc_name}--> *}-->
<!--{/foreach}-->
<!--{/if}-->
<!--{* ▲Head COLUMN *}-->
</head>

<!-- ▼BODY部 -->
<!--{include file='./site_main.tpl'}-->
<!-- ▲BODY部 -->

</html>
