<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/common.css" type="text/css" media="all" />

<div id="window_area">
    <h2 class="title">お客様の声書き込み</h2>
    <p class="prompt">以下の商品について、お客様のご意見、ご感想をどしどしお寄せください。<br />
        「<span class="attention">※</span>」印は入力必須項目です。<br />
        ご入力後、一番下の「確認ページへ」ボタンをクリックしてください。</p>
    <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="confirm" />
        <input type="hidden" name="product_id" value="<!--{$arrForm.product_id|h}-->" />
        <dl summary="お客様の声書き込み" class="entry">
                <dt>商品名</dt>
                <dd><!--{$arrForm.name|h}--></dd>
                <dt>投稿者名<span class="attention">必須</span></dt>
                <dd><span class="attention"><!--{$arrErr.reviewer_name}--></span><input type="text" name="reviewer_name" value="<!--{$arrForm.reviewer_name|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.reviewer_name|sfGetErrorColor}-->" class="box350" /></dd>
                <dt>投稿者URL</dt>
                <dd><span class="attention"><!--{$arrErr.reviewer_url}--></span><input type="text" name="reviewer_url" value="<!--{$arrForm.reviewer_url|h}-->" maxlength="<!--{$smarty.const.MTEXT_LEN}-->" style="<!--{$arrErr.reviewer_url|sfGetErrorColor}-->" class="box350" /></dd>
                <dt>性別</dt>
                <dd>
                    <input type="radio" name="sex" id="man" value="1" <!--{if $arrForm.sex eq 1}--> checked="checked"<!--{/if}--> /><label for="man">男性</label>&nbsp;
                    <input type="radio" name="sex" id="woman" value="2" <!--{if $arrForm.sex eq 2}--> checked="checked"<!--{/if}--> /><label for="woman">女性</label>
                </dd>
                <dt>おすすめレベル<span class="attention">必須</span></dt>
                <dd>
                    <span class="attention"><!--{$arrErr.recommend_level}--></span>
                    <select name="recommend_level" style="<!--{$arrErr.recommend_level|sfGetErrorColor}-->">
                        <option value="" selected="selected">選択してください</option>
                            <!--{html_options options=$arrRECOMMEND selected=$arrForm.recommend_level}-->
                    </select>
                </dd>
                <dt>タイトル<span class="attention">必須</span></dt>
                <dd>
                    <span class="attention"><!--{$arrErr.title}--></span>
                    <input type="text" name="title" value="<!--{$arrForm.title|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.title|sfGetErrorColor}-->" class="box350" />
                </dd>
                <dt>コメント<span class="attention">必須</span></dt>
                <dd>
                    <span class="attention"><!--{$arrErr.comment}--></span>
                    <textarea name="comment" cols="50" rows="10" style="<!--{$arrErr.comment|sfGetErrorColor}-->" class="area350"><!--{"\n"}--><!--{$arrForm.comment|h}--></textarea>
                </dd>
        </dl>
        <div class="btn_area review">
            <ul>
                <li class="entry"><input type="submit" src="<!--{$TPL_URLPATH}-->img/button/btn_confirm.jpg" alt="確認ページへ" name="conf" id="conf" value="確認ページへ" /></li>
            </ul>
        </div>
    </form>
</div>

<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_footer.tpl"}-->
