<script>//<![CDATA[
    // 規格2に選択肢を割り当てる。
    function fnSetClassCategories(form, classcat_id2_selected) {
        var $form = $(form);
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('select[name=classcategory_id1]');
        var $sele2 = $form.find('select[name=classcategory_id2]');
        eccube.setClassCategories($form, product_id, $sele1, $sele2, classcat_id2_selected);
    }
    $(function(){
        $('#detailphotoblock ul li').flickSlide({target:'#detailphotoblock>ul', duration:5000, parentArea:'#detailphotoblock', height: 200});
        $('#whobought_area ul li').flickSlide({target:'#whobought_area>ul', duration:5000, parentArea:'#whobought_area', height: 80});

        //お勧め商品のリンクを張り直し(フリックスライドによるエレメント生成後)
        $('#whobought_area li').biggerlink();
    });
    //サブエリアの表示/非表示
    var speed = 500;
    var stateSub = 0;
    function fnSubToggle(areaEl, imgEl) {
        areaEl.slideToggle(speed);
        if (stateSub == 0) {
            $(imgEl).attr("src", "<!--{$TPL_URLPATH}-->img/button/btn_plus.png");
            stateSub = 1;
        } else {
            $(imgEl).attr("src", "<!--{$TPL_URLPATH}-->img/button/btn_minus.png");
            stateSub = 0
        }
    }
    //この商品に対するお客様の声エリアの表示/非表示
    var stateReview = 0;
    function fnReviewToggle(areaEl, imgEl) {
        areaEl.slideToggle(speed);
        if (stateReview == 0) {
            $(imgEl).attr("src", "<!--{$TPL_URLPATH}-->img/button/btn_plus.png");
            stateReview = 1;
        } else {
            $(imgEl).attr("src", "<!--{$TPL_URLPATH}-->img/button/btn_minus.png");
            stateReview = 0
        }
    }
    //お勧めエリアの表示/非表示
    var statewhobought = 0;
    function fnWhoboughtToggle(areaEl, imgEl) {
        areaEl.slideToggle(speed);
        if (statewhobought == 0) {
            $(imgEl).attr("src", "<!--{$TPL_URLPATH}-->img/button/btn_plus.png");
            statewhobought = 1;
        } else {
            $(imgEl).attr("src", "<!--{$TPL_URLPATH}-->img/button/btn_minus.png");
            statewhobought = 0
        }
    }
//]]></script>

<section id="product_detail">
    <!--★画像★-->

    <!-- <div id="detailphotoblock" class="mainImageInit"> -->
<div class="itemContent">

<div class="itemContent_left">
<div class="mainImage">

<ul class="slider thumb-item">
<li>
<!--{assign var=ikey value="main_large_image"}-->
<!--{if $arrProduct[$ikey]|strlen >= 1}-->
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_large_image|h}-->" alt="<!--{$arrProduct.name|h}-->" />
<!--{/if}-->
</li>
<li>
<!--{assign var=ikey value="sub_large_image1"}-->
<!--{if $arrProduct[$ikey]|strlen >= 1}-->
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.sub_large_image1|h}-->" alt="<!--{$arrProduct.name|h}-->" />
<!--{/if}-->
</li>
<li>
<!--{assign var=ikey value="sub_large_image2"}-->
<!--{if $arrProduct[$ikey]|strlen >= 1}-->
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.sub_large_image2|h}-->" alt="<!--{$arrProduct.name|h}-->" />
<!--{/if}-->
</li>
<li>
<!--{assign var=ikey value="sub_large_image3"}-->
<!--{if $arrProduct[$ikey]|strlen >= 1}-->
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.sub_large_image3|h}-->" alt="<!--{$arrProduct.name|h}-->" />
<!--{/if}-->
</li>
</ul>

</div>
<!-- .mainImage -->
<div class="accessories">
<div class="snsBtn">
<a class="twitter" href="https://twitter.com/shinchan_ec?ref_src=twsrc%5Etfw"><img src="<!--{$TPL_URLPATH}-->img/product/snsBtn_twitter.png"><span>シェア</span></a>
</div>
<!-- .snsBtn -->
<div class="review">
<a href="#review"><img src="<!--{$TPL_URLPATH}-->img/product/review_icon.png"><span>レビューを見る</span></a>
</div>
<!-- .review -->
</div>
<!-- .accessories -->
</div>
<!-- .itemContent_left -->



</div>
<!-- .itemContent -->

        <!--★詳細メインコメント★-->
<div class="itemInfo">
<span class="heading"><!--{assign var=ckey value="sub_comment2"}--><!--{$arrProduct[$ckey]|nl2br_html}-->
</span>
<!-- .heading -->
<h1><!--{$arrProduct.name|h}--></h1>
</div>
<!-- .itemInfo -->

        <form name="form1" id="form1" method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->products/detail.php">
            <div id="detailrightblock">
                <!--▼商品ステータス-->
                <!--{assign var=ps value=$productStatus[$tpl_product_id]}-->
                <!--{if count($ps) > 0}-->
                    <ul class="status_icon">
                    <!--{foreach from=$ps item=status}-->
                        <li class="status<!--{$status}-->"><!--{$arrSTATUS[$status]}--></li>
                    <!--{/foreach}-->
                    </ul>
                <!--{/if}-->
                <!--▲商品ステータス-->

                <!--★詳細メインコメント★-->
                <div class="caption">
                <p><!--{$arrProduct.main_comment|nl2br_html}--></p>
                </div>
                <!-- .caption -->

                <!--★販売価格★-->
                <div class="price">
                <p><!--{strip}-->
                    <!--{if $arrProduct.price02_min_inctax == $arrProduct.price02_max_inctax}-->
                        <!--{$arrProduct.price02_min_inctax|n2s}-->
                    <!--{else}-->
                        <!--{$arrProduct.price02_min_inctax|n2s}-->～<!--{$arrProduct.price02_max_inctax|n2s}-->
                    <!--{/if}-->
                <!--{/strip}-->円<span>(税込)</span></p>
                </div>
                <!-- .price -->

                <!--★ポイント★-->
                <!--{if $smarty.const.USE_POINT !== false}-->
                <div class="point">
                <p>ポイント<!--{strip}-->
                    <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                        <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|n2s}-->
                    <!--{else}-->
                        <!--{if $arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate == $arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate}-->
                            <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|n2s}-->
                        <!--{else}-->
                            <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|n2s}-->～<!--{$arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate|n2s}-->
                        <!--{/if}-->
                    <!--{/if}-->
                <!--{/strip}--></p>
                </div>
                <!-- .point -->
                <!--{/if}-->


                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="mode" value="cart" />
                <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->" />
                <input type="hidden" name="product_class_id" value="<!--{$tpl_product_class_id}-->" id="product_class_id" />
                <input type="hidden" name="favorite_product_id" value="" />

                <!--▼買い物カゴ-->
                <!--{if $tpl_stock_find}-->

                    <!--{if $tpl_classcat_find1}-->
                        <div class="classlist">
                            <ul>
                                <!--▼規格1-->
                                <li><!--{$tpl_class_name1|h}--></li>
                                <li>
                                    <select name="classcategory_id1"
                                        style="<!--{$arrErr.classcategory_id1|sfGetErrorColor}-->"
                                        class="data-role-none">
                                        <!--{html_options options=$arrClassCat1 selected=$arrForm.classcategory_id1.value}-->
                                    </select>
                                    <!--{if $arrErr.classcategory_id1 != ""}-->
                                        <br /><span class="attention">※ <!--{$tpl_class_name1}-->を入力して下さい。</span>
                                    <!--{/if}-->
                                </li>
                                <!--▲規格1-->

                                <!--{if $tpl_classcat_find2}-->
                                    <!--▼規格2-->
                                    <li><!--{$tpl_class_name2|h}--></li>
                                    <li>
                                        <select name="classcategory_id2"
                                            style="<!--{$arrErr.classcategory_id2|sfGetErrorColor}-->"
                                            class="data-role-none">
                                        </select>
                                        <!--{if $arrErr.classcategory_id2 != ""}-->
                                            <br /><span class="attention">※ <!--{$tpl_class_name2}-->を入力して下さい。</span>
                                        <!--{/if}-->
                                    </li>
                                    <!--▲規格2-->
                                <!--{/if}-->
                            </ul>
                        </div>
                    <!--{/if}-->

                        <!-- 数量スピンボタン// -->
                        <script type="text/javascript">
                        function spinner(counter){
                            var step = 1;
                            var min = 0;
                            var max = <!--{$smarty.const.INT_LEN}-->;

                            var kazu = $(".kazu").val();
                            var kazu = parseInt(kazu);
                              if (counter == 'up') { kazu += step; };
                              if (counter == 'down') { kazu -= step; };

                            if ( kazu < min ) { kazu = min; };
                            if ( max < kazu ) { kazu = max; };

                            $(".kazu").val(kazu);
                        }
                        </script>

                    <div class="cartin_btn">
                        <dl class="quantity">
                            <dt>数量</dt>
                            <dd>
                                  <div style="overflow:hidden;">
                                  <input type="number" name="quantity" class="quantitybox kazu box60" value="<!--{$arrForm.quantity.value|default:1|h}-->" max="<!--{9|str_repeat:$smarty.const.INT_LEN}-->" style="<!--{$arrErr.quantity|sfGetErrorColor}-->" />
                                  <div class="button">
                                  <input type="button" name="spinner_up" class="spinner_up" value="" onclick="javascript:spinner('up');">
                                  <input type="button" name="spinner_up" class="spinner_down" value="" onclick="javascript:spinner('down');">
                                  </div>
                                  </div>
                                <!--{if $arrErr.quantity != ""}-->
                                    <br /><span class="attention"><!--{$arrErr.quantity}--></span>
                                <!--{/if}-->
                            </dd>
                        </dl>

                        <p class="stock">在庫：
                            <!--{if $arrProduct.stock_unlimited_min == 1}--><!--{*無制限*}-->
                                あり
                            <!--{else}-->
                                <!--{if $arrProduct.stock_min != $arrProduct.stock_max}-->
                                    <!--{$arrProduct.stock_min|escape}-->～<!--{$arrProduct.stock_max|escape}-->
                                <!--{else}-->
                                    <!--{$arrProduct.stock_min|escape}-->
                                <!--{/if}-->個
                            <!--{/if}-->
                        </p>

                        <!--★カートに入れる★-->
                        <div class="cartin">
                            <div class="cartin_btn">
                        <div id="cartbtn_default">
                            <a class="external" href="javascript:void(document.form1.submit())">
                                <img src="<!--{$TPL_URLPATH}-->img/common/cart_small.png"><span>カートに入れる</span>
                            </a>
                        </div>
                            </div>
                        </div>
                        <div class="attention" id="cartbtn_dynamic"></div>
                    </div>
                <!--{else}-->
                    <div class="cartin_btn largeBtn gray">
                      <img src="<!--{$TPL_URLPATH}-->img/common/cart_small_gray.png"><span>カートに入れる</span>
                    </div>
                <!--{/if}-->
                <!--▲買い物カゴ-->

                <!--{if $tpl_login}-->
                    <!--{if !$is_favorite}-->
                        <div class="btn_favorite favorite">
                            <p><a rel="external" href="javascript:void(0);" onclick="eccube.addFavoriteSphone(<!--{$arrProduct.product_id|h}-->); return false;" class="btn_sub"><img src="<!--{$TPL_URLPATH}-->img/product/heart_icon.png"><span>お気に入りに追加</span></a></p>
                        </div>
                    <!--{else}-->
                        <div class="btn_favorite favorite">
                            <a title="お気に入りに登録済み"><img src="<!--{$TPL_URLPATH}-->img/product/heart_icon.png"><span>お気に入りに登録済み</span></a>
                        </div>
                    <!--{/if}-->
                <!--{/if}-->
            </div>
        </form>
    </section>
    <!--詳細ここまで-->





<div class="productInfo">
<h2><img src="<!--{$TPL_URLPATH}-->img/product/infoIcon01.png">
<span>
<!--{assign var=ckey value="sub_title1"}-->
<!--{$arrProduct[$ckey]|nl2br_html}-->

</span>
</h2>
<div class="productInfo_data">
<!--★商品コード★-->
<div class="row1">商品コード</div>
<div class="row2">
<!--{if $arrProduct.product_code_min == $arrProduct.product_code_max}-->
<!--{$arrProduct.product_code_min|h}-->
<!--{else}-->
<!--{$arrProduct.product_code_min|h}-->～<!--{$arrProduct.product_code_max|h}-->
<!--{/if}-->
</div>
<!--{assign var=ckey value="sub_comment1"}-->
<!--{$arrProduct[$ckey]|nl2br_html}-->
</div>
<!-- .productInfo_data -->
</div>
<!-- .productInfo -->

<div class="review" id="review">
<h2><img src="<!--{$TPL_URLPATH}-->img/product/infoIcon02.png"><span>商品レビュー</span></h2>

<!--{if count($arrReview) > 0}-->
<ul>
<!--{section name=cnt loop=$arrReview}-->
<li>
<p class="voicetitle"><img src="<!--{$TPL_URLPATH}-->img/product/review_icon.png"><span><!--{$arrReview[cnt].title|h}--></span></p>
<p class="voicedate"><span class="user">投稿者：<!--{if $arrReview[cnt].reviewer_url}--><a href="<!--{$arrReview[cnt].reviewer_url}-->" target="_blank"><!--{$arrReview[cnt].reviewer_name|h}--></a></span><!--{else}--><span class="user"><!--{$arrReview[cnt].reviewer_name|h}--></span><!--{/if}--><span class="postData">投稿日：<!--{$arrReview[cnt].create_date|sfDispDBDate:false}--></span></p>
<p class="voicecomment"><!--{$arrReview[cnt].comment|h|nl2br}--></p>
</li>
<!--{/section}-->
</ul>
<!--{/if}-->

<!--{if $tpl_login}-->
<div class="review_btn">
<!--{if count($arrReview) < $smarty.const.REVIEW_REGIST_MAX}-->
<!--★新規コメントを書き込む★-->
<a href="./review.php"
onclick="eccube.openWindow('./review.php?product_id=<!--{$arrProduct.product_id}-->','review','600','640'); return false;"
target="_blank">
<img class="hover_change_image" src="<!--{$TPL_URLPATH}-->img/button/btn_comment.jpg" alt="新規コメントを書き込む" />
</a>
<!--{/if}-->
</div>
<!--{else}-->
<p class="no_login">新規商品レビューを書き込むにはログインが必要です。</p>
<!--{/if}-->

</div>
<!-- .review -->

<div class="analogy">
<!--▼関連商品-->
<!--{if $arrRecommend}-->
<h2><img src="<!--{$TPL_URLPATH}-->img/product/infoIcon03.png"><span>この商品をチェックされた方が購入した商品</span></h2>
<!--{foreach from=$arrRecommend item=arrItem name="arrRecommend"}-->
<div class="product_item nth5">
<div class="productImage">
<a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrItem.product_id|u}-->">
<img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrItem.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrItem.name|h}-->" /></a>
</div>
<!--{assign var=price02_min value=`$arrItem.price02_min_inctax`}-->
<!--{assign var=price02_max value=`$arrItem.price02_max_inctax`}-->
<div class="productContents">
<h3><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrItem.product_id|u}-->"><!--{$arrItem.name|h}--></a></h3>
<p class="sale_price"><!--{$smarty.const.SALE_PRICE_TITLE}-->(税込)：<span class="price">
<!--{if $price02_min == $price02_max}-->
<!--{$price02_min|n2s}-->
<!--{else}-->
<!--{$price02_min|n2s}-->～<!--{$price02_max|n2s}-->
<!--{/if}-->円</span></p>
<p class="mini"><!--{$arrItem.comment|h|nl2br}--></p>
</div>
</div>
<!--{* /.item *}-->
<!--{if $smarty.foreach.arrRecommend.iteration % 2 === 0}-->
<!--{/if}-->
<!--{/foreach}-->
<!--{/if}-->
<!--▲関連商品-->
<div class="clear"></div>
</div>
<!-- .analogy -->

<!--    <div class="btn_area">
        <p><a href="javascript:void(0);" class="btn_more" data-rel="back">戻る</a></p>
    </div>
-->
</section>

<!--{include file= 'frontparts/search_area.tpl'}-->


<script type="text/javascript">
$(function() {
$('.thumb-item').slick({
infinite: true,
slidesToShow: 1,
slidesToScroll: 1,
arrows: true,
fade: true,
prevArrow: '<div class="prevarrow"><img src="/user_data/packages/shinchan/img/slider/slider_arrow_big.png" class="arrow"></div>',
nextArrow: '<div class="nextarrow"><img src="/user_data/packages/shinchan/img/slider/slider_arrow_big.png" class="arrow"></div>',
});
});

$(function() {
    $(".productImg_large").colorbox({
    opacity: 0.7,
  });
});

</script>
