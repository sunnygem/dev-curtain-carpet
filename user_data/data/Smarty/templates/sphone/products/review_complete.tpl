<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/common.css" type="text/css" media="all" />

<div id="window_area">
    <h2 class="title">お客様の声書き込み</h2>
    <div id="completebox">
        <p class="prompt">登録が完了しました。ご利用ありがとうございました。</p>
        <p class="prompt">弊社にて登録内容を確認後、ホームページに反映させていただきます。<br />
            今しばらくお待ちくださいませ。</p>
    </div>
    <div class="btn_area">
        <ul>
            <li class="entry">
                <a href="javascript:window.close()">
                    閉じる
                </a>
            </li>
        </ul>
    </div>
</div>

<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_footer.tpl"}-->
