
<script type="text/javascript">//<![CDATA[
    var sent = false;

    function fnCheckSubmit() {
        if (sent) {
            alert("只今、処理中です。しばらくお待ち下さい。");
            return false;
        }
        sent = true;
        return true;
    }
//]]></script>

<!--CONTENTS-->
<div id="undercolumn">
    <div id="undercolumn_shopping">
        <h2 class="title"><!--{$tpl_title|h}--></h2>

        <p class="information">下記ご注文内容で送信してもよろしいでしょうか？<br />
            よろしければ、「<!--{if $use_module}-->次へ<!--{else}-->ご注文完了ページへ<!--{/if}-->」ボタンをクリックしてください。</p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />

            <table summary="ご注文内容確認">
                <col width="10%" />
                <col width="40%" />
                <col width="20%" />
                <col width="10%" />
                <col width="20%" />
                <tr>
                    <th scope="col">商品写真</th>
                    <th scope="col">商品名</th>
                    <th scope="col">単価</th>
                    <th scope="col">数量</th>
                    <th scope="col">小計</th>
                </tr>
                <!--{foreach from=$arrCartItems item=item}-->
                    <tr>
                        <td class="alignC">
                            <a
                                <!--{if $item.productsClass.main_image|strlen >= 1}--> href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" class="expansion" target="_blank"
                                <!--{/if}-->
                            >
                                <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_list_image|sfNoImageMainList|h}-->" style="max-width: 65px;max-height: 65px;" alt="<!--{$item.productsClass.name|h}-->" /></a>
                        </td>
                        <td>
                            <ul>
                                <li><strong><!--{$item.productsClass.name|h}--></strong></li>
                                <!--{if $item.productsClass.classcategory_name1 != ""}-->
                                <li><!--{$item.productsClass.class_name1|h}-->：<!--{$item.productsClass.classcategory_name1|h}--></li>
                                <!--{/if}-->
                                <!--{if $item.productsClass.classcategory_name2 != ""}-->
                                <li><!--{$item.productsClass.class_name2|h}-->：<!--{$item.productsClass.classcategory_name2|h}--></li>
                                <!--{/if}-->
                            </ul>
                        </td>
                        <td class="alignR">
                            <!--{$item.price_inctax|n2s}-->円
                        </td>
                        <td class="alignR"><!--{$item.quantity|n2s}--></td>
                        <td class="alignR"><!--{$item.total_inctax|n2s}-->円</td>
                    </tr>
                <!--{/foreach}-->
                <tr>
                    <th colspan="4" class="alignR" scope="row">小計</th>
                    <td class="alignR"><!--{$tpl_total_inctax[$cartKey]|n2s}-->円</td>
                </tr>
                <!--{if $smarty.const.USE_POINT !== false}-->
                    <!--{if $arrForm.use_point > 0}-->
                    <tr>
                        <th colspan="4" class="alignR" scope="row">値引き（ポイントご使用時）</th>
                        <td class="alignR">
                            <!--{assign var=discount value=`$arrForm.use_point*$smarty.const.POINT_VALUE`}-->
                            -<!--{$discount|n2s|default:0}-->円</td>
                    </tr>
                    <!--{/if}-->
                <!--{/if}-->
                <tr>
                    <th colspan="4" class="alignR" scope="row">送料</th>
                    <td class="alignR"><!--{$arrForm.deliv_fee|n2s}-->円</td>
                </tr>
                <tr>
                    <th colspan="4" class="alignR" scope="row">手数料</th>
                    <td class="alignR"><!--{$arrForm.charge|n2s}-->円</td>
                </tr>
                <tr>
                    <th colspan="4" class="alignR" scope="row">合計</th>
                    <td class="alignR"><span class="price"><!--{$arrForm.payment_total|n2s}-->円</span></td>
                </tr>
            </table>

            <!--{* ログイン済みの会員のみ *}-->
            <!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false}-->
                <dl summary="ポイント確認" class="delivname entry">
                        <dt scope="row">ご注文前のポイント</dt>
                        <dd><!--{$tpl_user_point|n2s|default:0}-->Pt</dd>
                        <dt scope="row">ご使用ポイント</dt>
                        <dd>-<!--{$arrForm.use_point|n2s|default:0}-->Pt</dd>
                    <!--{if $arrForm.birth_point > 0}-->
                        <dt scope="row">お誕生月ポイント</dt>
                        <dd>+<!--{$arrForm.birth_point|n2s|default:0}-->Pt</dd>
                    <!--{/if}-->
                        <dt scope="row">今回加算予定のポイント</dt>
                        <dd>+<!--{$arrForm.add_point|n2s|default:0}-->Pt</dd>
                    <!--{assign var=total_point value=`$tpl_user_point-$arrForm.use_point+$arrForm.add_point`}-->
                        <dt scope="row">加算後のポイント</dt>
                        <dd><!--{$total_point|n2s}-->Pt</dd>
                </dl>
            <!--{/if}-->
            <!--{* ログイン済みの会員のみ *}-->

            <!--{* ▼注文者 *}-->
            <h3>ご注文者</h3>
            <dl summary="ご注文者" class="customer entry">
                        <dt scope="row">お名前</dt>
                        <dd><!--{$arrForm.order_name01|h}--> <!--{$arrForm.order_name02|h}--></dd>
                        <dt scope="row">お名前(フリガナ)</dt>
                        <dd><!--{$arrForm.order_kana01|h}--> <!--{$arrForm.order_kana02|h}--></dd>
                        <dt scope="row">会社名</dt>
                        <dd><!--{$arrForm.order_company_name|h}--></dd>
                    <!--{if $smarty.const.FORM_COUNTRY_ENABLE}-->
                        <dt scope="row">国</dt>
                        <dd><!--{$arrCountry[$arrForm.order_country_id]|h}--></dd>
                        <dt scope="row">ZIPCODE</dt>
                        <dd><!--{$arrForm.order_zipcode|h}--></dd>
                    <!--{/if}-->
                        <dt scope="row">郵便番号</dt>
                        <dd>〒<!--{$arrForm.order_zip01|h}-->-<!--{$arrForm.order_zip02|h}--></dd>
                        <dt scope="row">住所</dt>
                        <dd><!--{$arrPref[$arrForm.order_pref]}--><!--{$arrForm.order_addr01|h}--><!--{$arrForm.order_addr02|h}--></dd>
                        <dt scope="row">電話番号</dt>
                        <dd><!--{$arrForm.order_tel01}-->-<!--{$arrForm.order_tel02}-->-<!--{$arrForm.order_tel03}--></dd>
                        <dt scope="row">FAX番号</dt>
                        <dd>
                            <!--{if $arrForm.order_fax01 > 0}-->
                                <!--{$arrForm.order_fax01}-->-<!--{$arrForm.order_fax02}-->-<!--{$arrForm.order_fax03}-->
                            <!--{/if}-->
                        </dd>
                        <dt scope="row">メールアドレス</dt>
                        <dd><!--{$arrForm.order_email|h}--></dd>
                        <dt scope="row">性別</dt>
                        <dd><!--{$arrSex[$arrForm.order_sex]|h}--></dd>
                        <dt scope="row">職業</dt>
                        <dd><!--{$arrJob[$arrForm.order_job]|default:'(未登録)'|h}--></dd>
                        <dt scope="row">生年月日</dt>
                        <dd>
                            <!--{$arrForm.order_birth|regex_replace:"/ .+/":""|regex_replace:"/-/":"/"|default:'(未登録)'|h}-->
                        </dd>
            </dl>

            <!--{* ▼お届け先 *}-->
            <!--{foreach item=shippingItem from=$arrShipping name=shippingItem}-->
                <h3>お届け先<!--{if $is_multiple}--><!--{$smarty.foreach.shippingItem.iteration}--><!--{/if}--></h3>
                <!--{if $is_multiple}-->
                    <dl summary="ご注文内容確認" class="entry">
                            <dt scope="col">商品写真</dt>
                            <dt scope="col">商品名</dt>
                            <dt scope="col">数量</dt>
                            <dt scope="col">小計</dt>
                        <!--{foreach item=item from=$shippingItem.shipment_item}-->
                                <dd class="alignC">
                                    <a
                                        <!--{if $item.productsClass.main_image|strlen >= 1}--> href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" class="expansion" target="_blank"
                                        <!--{/if}-->
                                    >
                                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_list_image|sfNoImageMainList|h}-->" style="max-width: 65px;max-height: 65px;" alt="<!--{$item.productsClass.name|h}-->" /></a>
                                </dd>
                                <dd><!--{* 商品名 *}--><strong><!--{$item.productsClass.name|h}--></strong><br />
                                    <!--{if $item.productsClass.classcategory_name1 != ""}-->
                                        <!--{$item.productsClass.class_name1}-->：<!--{$item.productsClass.classcategory_name1}--><br />
                                    <!--{/if}-->
                                    <!--{if $item.productsClass.classcategory_name2 != ""}-->
                                        <!--{$item.productsClass.class_name2}-->：<!--{$item.productsClass.classcategory_name2}-->
                                    <!--{/if}-->
                                </dd>
                                <dd class="alignC"><!--{$item.quantity}--></dd>
                                <dd class="alignR">
                                    <!--{$item.total_inctax|n2s}-->円
                                </dd>
                        <!--{/foreach}-->
                    </dl>
                <!--{/if}-->

                <dl summary="お届け先確認" class="delivname entry">
                            <dt scope="row">お名前</dt>
                            <dd><!--{$shippingItem.shipping_name01|h}--> <!--{$shippingItem.shipping_name02|h}--></dd>
                            <dt scope="row">お名前(フリガナ)</dt>
                            <dd><!--{$shippingItem.shipping_kana01|h}--> <!--{$shippingItem.shipping_kana02|h}--></dd>
                            <dt scope="row">会社名</dt>
                            <dd><!--{$shippingItem.shipping_company_name|h}--></dd>
                        <!--{if $smarty.const.FORM_COUNTRY_ENABLE}-->
                            <dt scope="row">国</dt>
                            <dd><!--{$arrCountry[$shippingItem.shipping_country_id]|h}--></dd>
                            <dt scope="row">ZIPCODE</dt>
                            <dd><!--{$shippingItem.shipping_zipcode|h}--></dd>
                        <!--{/if}-->
                            <dt scope="row">郵便番号</dt>
                            <dd>〒<!--{$shippingItem.shipping_zip01|h}-->-<!--{$shippingItem.shipping_zip02|h}--></dd>
                            <dt scope="row">住所</dt>
                            <dd><!--{$arrPref[$shippingItem.shipping_pref]}--><!--{$shippingItem.shipping_addr01|h}--><!--{$shippingItem.shipping_addr02|h}--></dd>
                            <dt scope="row">電話番号</dt>
                            <dd><!--{$shippingItem.shipping_tel01}-->-<!--{$shippingItem.shipping_tel02}-->-<!--{$shippingItem.shipping_tel03}--></dd>
                            <dt scope="row">FAX番号</dt>
                            <dd>
                                <!--{if $shippingItem.shipping_fax01 > 0}-->
                                    <!--{$shippingItem.shipping_fax01}-->-<!--{$shippingItem.shipping_fax02}-->-<!--{$shippingItem.shipping_fax03}-->
                                <!--{/if}-->
                            </dd>
                        <!--{if $cartKey != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                                <dt scope="row">お届け日</dt>
                                <dd><!--{$shippingItem.shipping_date|default:"指定なし"|h}--></dd>
                                <dt scope="row">お届け時間</dt>
                                <dd><!--{$shippingItem.shipping_time|default:"指定なし"|h}--></dd>
                        <!--{/if}-->
                </dl>
            <!--{/foreach}-->
            <!--{* ▲お届け先 *}-->

            <h3>配送方法・お支払方法・その他お問い合わせ</h3>
            <dl summary="配送方法・お支払方法・その他お問い合わせ" class="delivname entry">
                    <dt scope="row">配送方法</dt>
                    <dd><!--{$arrDeliv[$arrForm.deliv_id]|h}--></dd>
                    <dt scope="row">お支払方法</dt>
                    <dd><!--{$arrForm.payment_method|h}--></dd>
                    <dt scope="row">その他お問い合わせ</dt>
                    <dd><!--{$arrForm.message|h|nl2br}--></dd>
            </dl>

            <div class="btn_area btn2 margin">
                <ul>
                    <li class="return"><a href="./payment.php">戻る</a></li>
                    <!--{if $use_module}-->
                    <li class="entry">
                        <input type="submit" onclick="return fnCheckSubmit();" value="次へ" name="next" id="next" />
                    </li>
                    <!--{else}-->
                    <li class="entry">
                        <input type="submit" onclick="return fnCheckSubmit();" value="ご注文完了ページへ"  name="next" id="next" />
                    </li>
                    <!--{/if}-->
                </ul>
            </div>
        </form>
    </div>
</div>
