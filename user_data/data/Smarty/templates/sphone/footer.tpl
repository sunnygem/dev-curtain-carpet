
<!--{strip}-->
<div class="middleArea">
<div class="container">
<div class="middleArea_left">
<h2>ご利用ガイド</h2>
<ul>
<li><a href="/user_data/product.php">商品について</a></li>
<li><a href="/user_data/delivery.php">配送および送料について</a></li>
<li><a href="/user_data/howto_guide.php">サイトのご利用にあたって</a></li>
<li><a href="/user_data/member.php">会員について</a></li>
<li><a href="/user_data/order.php">ご注文の手順について</a></li>
<li><a href="/user_data/exchange.php">返品・交換について</a></li>
<li><a href="/user_data/payment.php">お支払いについて</a></li>
<li><a href="/user_data/member_rank.php">会員ランクについて</a></li>
</ul>
</div>
<!-- .middleArea_left -->
<div class="middleArea_right">
<h2>お問い合わせ</h2>
<ul>
<li><a href="/user_data/faq.php">よくあるご質問</a></li>
<li><a href="/contact/">総合お問い合わせ</a></li>
</ul>
</div>
<!-- .middleArea_left -->
</div>
<!-- .container -->
</div>
<!-- .middleArea -->

<div class="bottomArea">
<div class="container">
<div class="listArea">
<ul>
<li><a href="/user_data/operating.php">運営会社</a></li>
<li><a href="/guide/privacy.php">プライバシーポリシー</a></li>
<li><a href="/user_data/faq.php">よくあるご質問</a></li>
<li><a href="/user_data/sitemap.php">サイトマップ</a></li>
<li><a href="/contact/">お問い合わせ</a></li>
</ul>
</div>
<!-- .listArea -->
<p>このホームページに掲載のイラストおよびページレイアウト、文章の無断転載を禁じます。<br>すべての著作権は株式会社双葉社に帰属します。</p>
</div>
<!-- .container -->
</div>
<!-- .bottomArea -->
<div class="copylight">
<p>©臼井儀人／双葉社　©臼井儀人／双葉社・シンエイ・テレビ朝日・ADK</p>
</div>
<!-- .copylight -->

<div id="pageto" style="display: block;"><a href="#"><img src="/user_data/packages/shinchan/img/common/pagetop.png"></a></div>

<!--{/strip}-->
<script type="text/javascript">
$(function() {
    $('.product_item .productContents h3 a').matchHeight();
});
$(function() {
    $('.product_item .productContents .sale_price').matchHeight();
});
$(function() {
    $('.product_item .productImage').matchHeight();
});
$(function() {
    $('.LC_Page_Products_List .list_area.grid .status_icon').matchHeight();
});
<!-- スムーススクロール// -->

$(function(){
  var topBtn = $('#pageto');
  topBtn.hide();
  $(window).scroll(function() {
    if ($(this).scrollTop() > 200) {
        topBtn.fadeIn();
    } else {
        topBtn.fadeOut();
    }
  });
  $('a[href^="#"]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});

</script>
