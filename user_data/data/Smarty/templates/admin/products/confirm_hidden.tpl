<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if is_array($item)}-->
<!--{foreach item=c_item from=$item key=key2}-->
<input type="hidden" name="<!--{$key}-->[<!--{$key2}-->]" value="<!--{$c_item|h}-->" />
<!--{/foreach}-->
<!--{else}-->
<input type="hidden" name="<!--{$key}-->" value="<!--{$item|h}-->" />
<!--{/if}-->