<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{foreach from=$arrRankPrices item=item}-->
<!--{assign var="key" value="plg_managecustomerstatus_price`$item.status_id`"}-->
<td class="center">
    <!--{if $arrErr[$key][$index]}-->
    <span class="attention"><!--{$arrErr[$key][$index]}--></span>
    <!--{/if}-->
    <input type="text" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" size="6" class="box6" maxlength="<!--{$arrForm[$key].length}-->" <!--{if $arrErr[$key][$index] != ""}--><!--{sfSetErrorStyle}--><!--{/if}--> id="<!--{$key}-->_<!--{$index}-->" />
</td>
<!--{/foreach}-->