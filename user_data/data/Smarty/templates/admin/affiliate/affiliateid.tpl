<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<script type="text/javascript">
<!--

    function fnDelete(customer_id) {
        if (confirm('この会員情報を削除しても宜しいですか？')) {
            document.form1.mode.value = "delete"
            document.form1['edit_customer_id'].value = customer_id;
            document.form1.submit();
            return false;
        }
    }

    function fnEdit(customer_id) {
        document.form1.action = './edit.php';
        document.form1.mode.value = "edit_search"
        document.form1['edit_customer_id'].value = customer_id;
        document.form1.search_pageno.value = 1;
        document.form1.submit();
        return false;
    }

    function fnReSendMail(customer_id) {
        if (confirm('仮登録メールを再送しても宜しいですか？')) {
            document.form1.mode.value = "resend_mail"
            document.form1['edit_customer_id'].value = customer_id;
            document.form1.submit();
            return false;
        }
    }
//-->
</script>


<div id="affiliate" class="contents-main">
        <h2>アフィリエイトID管理</h2>

<form name="form1" id="form1" method="post" action="?">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
    <input type="hidden" name="mode" value="edit" />
    <input type="hidden" name="affiliate_id" value="" />
    <div id="basis" class="contents-main">
        <div class="btn">
            <ul>
                <li><a class="btn-action" href="javascript:;" name="subm2" onclick="eccube.changeAction('./affiliate_idinput.php'); eccube.setModeAndSubmit('pre_edit','',''); return false;">
                    <span class="btn-next">アフィリエイトIDを新規入力</span></a></li>
            </ul>
        </div>
        <table class="list">
            <col width="5%" />
            <col width="40%" />
            <col width="25%" />
            <col width="10%" />
            <col width="10%" />
            <col width="10%" />
            <tr>
                <th>ID</th>
                <th>名前</th>
                <th>アフィリエイトID</th>
                <th>ステータス</th>
                <th>編集</th>
                <th>削除</th>
            </tr>
            <!--{section name=cnt loop=$arrAffiliateList}-->
                <tr>
                    <td><!--{$arrAffiliateList[cnt].affiliate_id|h}--></td>
                    <td><!--{$arrAffiliateList[cnt].name|h}--></td>
                    <td><!--{$arrAffiliateList[cnt].affiliateid|h}--></td>
                    <td><!--{if $arrAffiliateList[cnt].status == 1}-->使用中<!--{else}-->停止中<!--{/if}--></td>
                   <td align="center"><a href="?" onclick="eccube.changeAction('./affiliate_idinput.php'); eccube.setModeAndSubmit('pre_edit', 'affiliate_id', <!--{$arrAffiliateList[cnt].affiliate_id}-->); return false;">
                        編集</a></td>
                    <td align="center"><a href="?" onclick="eccube.setModeAndSubmit('delete', 'affiliate_id', <!--{$arrAffiliateList[cnt].affiliate_id}-->); return false;">
                        削除</a></td>
                </tr>
            <!--{/section}-->
        </table>
    </div>
</form>
</div>