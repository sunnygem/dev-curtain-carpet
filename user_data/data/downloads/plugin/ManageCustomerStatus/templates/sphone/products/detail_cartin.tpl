<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrProduct.plg_managecustomerstatus_hidden_flg > 0}-->
<div class="cartin_btn">
    <div class="attention">
        <!--{if $arrProduct.plg_managecustomerstatus_hidden_flg == 2}-->
        この商品はランクによる購入制限がかかっております。<br>
        ログイン後にもう1度ご確認下さい。
        <!--{else}-->
        この商品は現在のランクではご購入頂けません。
        <!--{/if}-->
    </div>
</div>
<!--{else}-->
<div class="cartin_btn">
    <dl class="quantity">
        <dt>数量</dt>
        <dd>
            <input type="number" name="quantity" class="quantitybox kazu box60" value="<!--{$arrForm.quantity.value|default:1|h}-->" max="<!--{$smarty.const.INT_LEN}-->" style="<!--{$arrErr.quantity|sfGetErrorColor}-->" />
            <div class="button">
            <input type="button" name="spinner_up" class="spinner_up" value="" onclick="javascript:spinner('up');">
            <input type="button" name="spinner_up" class="spinner_down" value="" onclick="javascript:spinner('down');">
            </div>
            </div>
            <!--{if $arrErr.quantity != ""}-->
            <br /><span class="attention"><!--{$arrErr.quantity}--></span>
            <!--{/if}-->
        </dd>
    </dl>

    <!--★カートに入れる★-->
    <div id="cartbtn_default" class="cartin">
        <a rel="external" href="javascript:void(document.form1.submit());" class="btn cartbtn_default"><img src="<!--{$TPL_URLPATH}-->img/common/cart_small.png">カートに入れる</a>
    </div>
    <div class="attention" id="cartbtn_dynamic"></div>
</div>
<!--{/if}-->
