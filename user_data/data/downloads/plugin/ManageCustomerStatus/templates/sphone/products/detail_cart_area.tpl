<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrProduct.plg_managecustomerstatus_hidden_flg > 0}-->

<!--{else}-->
<div class="cart_area">
    <dl>
        <!--▼規格1-->
        <dt><!--{$tpl_class_name1|h}--></dt>
        <dd>
            <select name="classcategory_id1"
                    style="<!--{$arrErr.classcategory_id1|sfGetErrorColor}-->"
                    class="data-role-none">
                <!--{html_options options=$arrClassCat1 selected=$arrForm.classcategory_id1.value}-->
            </select>
            <!--{if $arrErr.classcategory_id1 != ""}-->
            <br /><span class="attention">※ <!--{$tpl_class_name1}-->を入力して下さい。</span>fff
            <!--{/if}-->
        </dd>
        <!--▲規格1-->

        <!--{if $tpl_classcat_find2}-->
        <!--▼規格2-->
        <dt><!--{$tpl_class_name2|h}--></dt>
        <dd>
            <select name="classcategory_id2"
                    style="<!--{$arrErr.classcategory_id2|sfGetErrorColor}-->"
                    class="data-role-none">
            </select>
            <!--{if $arrErr.classcategory_id2 != ""}-->
            <br /><span class="attention">※ <!--{$tpl_class_name2}-->を入力して下さい。</span>
            <!--{/if}-->
        </dd>
        <!--▲規格2-->
        <!--{/if}-->
    </dl>
</div>
<!--{/if}-->

<!-- 数量スピンボタン// -->
<script type="text/javascript">
function spinner(counter){
    var step = 1;
    var min = 0;
    var max = <!--{$smarty.const.INT_LEN}-->;

    var kazu = $(".kazu").val();
    var kazu = parseInt(kazu);
      if (counter == 'up') { kazu += step; };
      if (counter == 'down') { kazu -= step; };

    if ( kazu < min ) { kazu = min; };
    if ( max < kazu ) { kazu = max; };

    $(".kazu").val(kazu);
}
</script>

<dl class="quantity">
    <dd>
        <div style="overflow:hidden;">
        <input type="text" class="kazu box60" name="quantity" value="0" style="<!--{$arrErr.quantity|sfGetErrorColor}-->" />
        <!--★ボタン★-->
        <div class="button">
        <input type="button" name="spinner_up" class="spinner_up" value="" onclick="javascript:spinner('up');">
        <input type="button" name="spinner_up" class="spinner_down" value="" onclick="javascript:spinner('down');">
        </div>
        </div>
        <!--{if $arrErr.quantity != ""}-->
        <br /><span class="attention"><!--{$arrErr.quantity}--></span>
        <!--{/if}-->
    </dd>
</dl>


<div class="cartin">
    <div class="cartin_btn">
        <div id="cartbtn_default">
            <!--★カゴに入れる★-->
            <div class="largeBtn">
            <a href="javascript:void(document.form1.submit())">
            <img src="<!--{$TPL_URLPATH}-->img/common/cart_small.png" alt="カートに入れる" name="cart" id="cart" /><span>カートに入れる</span></a>
            </div>
            <!-- .largeBtn -->
        </div>
    </div>
</div>
