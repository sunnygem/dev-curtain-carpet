<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<div id="products" class="contents-main">
    <div class="message">
        <span>会員ランク登録を実行しました。</span>
    </div>
    <!--{if $arrRowErr}-->
    <table class="form">
        <tr>
            <td>
                <!--{foreach item=err from=$arrRowErr}-->
                <span class="attention"><!--{$err}--></span><br/>
                <!--{/foreach}-->
            </td>
        </tr>
    </table>
    <!--{/if}-->
    <!--{if $arrRowResult}-->
    <table class="form">
        <tr>
            <td>
                <!--{foreach item=result from=$arrRowResult}-->
                <span><!--{$result|h}--><br/></span>
                <!--{/foreach}-->
            </td>
        </tr>
    </table>
    <!--{/if}-->
    <div class="btn-area">
        <ul>
            <li><a class="btn-action" href="?"><span class="btn-prev">戻る</span></a></li>
        </ul>
    </div>
</div>
