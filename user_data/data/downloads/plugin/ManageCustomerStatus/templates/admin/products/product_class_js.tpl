<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<script type="text/javascript">//<![CDATA[
    $(function() {
    // 1行目をコピーボタン
    $('#copy_from_first').click(function() {
    <!--{foreach from=$arrRankPrices item=item}-->
            <!--{assign var="key" value="plg_managecustomerstatus_price`$item.status_id`"}-->
            var price = $('#<!--{$key}-->_0').val();
    $('input[id^=<!--{$key}-->_]').val(price);
    <!--{/foreach}-->
    });
    });
//]]></script>		