<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrForm.has_product_class == false}-->
<!--{foreach from=$arrRankPrices item=item}-->
<!--{assign var="key" value="plg_managecustomerstatus_price`$item.status_id`"}-->
<tr>
    <th><!--{$item.name}-->価格</th>
    <td>
        <!--{if strlen($arrForm[$key]) >= 1}--><!--{$arrForm[$key]|h}--> 円<!--{/if}-->
    </td>
</tr>
<!--{/foreach}-->
<!--{/if}-->