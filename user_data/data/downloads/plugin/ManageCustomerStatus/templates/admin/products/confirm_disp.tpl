<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->


<!--{assign var="key" value="plg_managecustomerstatus_product_disp"}-->
<tr>
    <th>会員ランク別購入不可設定</th>
    <td>
        <!--{foreach from=$arrForm[$key] item=status_id}-->
        <!--{$arrPlgManageCustomerStatus[$status_id]}-->
        <!--{/foreach}-->
    </td>
</tr>
