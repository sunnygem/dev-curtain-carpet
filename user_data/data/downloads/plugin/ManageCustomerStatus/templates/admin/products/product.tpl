<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrForm.has_product_class == false}-->
<!--{foreach from=$arrRankPrices item=item}-->
<!--{assign var="key" value="plg_managecustomerstatus_price`$item.status_id`"}-->
<tr>
    <th><!--{$item.name}-->価格</th>
    <td>
        <span class="attention"><!--{$arrErr[$key]}--></span>
        <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key]|h}-->" size="6" class="box6" maxlength="<!--{$smarty.const.PRICE_LEN}-->" style="<!--{if $arrErr[$key] != ""}-->background-color: <!--{$smarty.const.ERR_COLOR}-->;<!--{/if}-->"/>円
        <span class="attention"> (半角数字で入力)</span>
    </td>
</tr>
<!--{/foreach}-->
<!--{/if}-->