<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<th>本文<span class="attention"> *</span><br>
    【差し込み時の記法】<br>
    &nbsp;{name}    : 会員名<br>
    &nbsp;{point}   : ポイント（数字のみ）<br>
    &nbsp;{rank}    : 会員ランク名<br>
    &nbsp;{expired} : ポイント有効期限（YYYY/MM/DD）
</th>

