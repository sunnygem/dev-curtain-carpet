<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<tr>
    <th>会員ランク</th>
    <td>
        <!--{assign var=key value="search_plg_managecustomerstatus_status"}-->
        <!--{if is_array($arrSearchData[$key])}-->
        <!--{foreach item=item from=$arrSearchData[$key]}-->
        <!--{$arrPlgManageCustomerStatus[$item]|h}-->
        <!--{/foreach}-->
        <!--{else}-->(未指定)<!--{/if}-->    
    </td>
</tr>
