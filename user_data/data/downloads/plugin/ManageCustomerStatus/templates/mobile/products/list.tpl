<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if strlen($arrProduct.plg_managecustomerstatus_price_min) > 0 && (($smarty.session.customer.customer_id && $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 1) || $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 0)}-->
<!--{if $smarty.const.MEMBER_RANK_PRICE_TITLE_MODE == 1}--><!--{$arrCustomerRank[$customer_rank_id]}--><!--{/if}--><!--{$smarty.const.MEMBER_RANK_PRICE_TITLE}-->：
<!--{if $arrProduct.plg_managecustomerstatus_price_min_inctax == $arrProduct.plg_managecustomerstatus_price_max_inctax}-->
<!--{$arrProduct.plg_managecustomerstatus_price_min_inctax|number_format}-->円
<!--{else}-->
<!--{$arrProduct.plg_managecustomerstatus_price_min_inctax|number_format}-->円～<!--{$arrProduct.plg_managecustomerstatus_price_max_inctax|number_format}-->円
<!--{/if}-->
<br>
<!--{/if}-->