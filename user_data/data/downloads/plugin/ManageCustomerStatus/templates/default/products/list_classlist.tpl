<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrProduct.plg_managecustomerstatus_hidden_flg > 0}-->

<!--{else}-->
<div class="classlist">
    <dl class="size01 clearfix">
        <!--▼規格1-->
        <dt><!--{$tpl_class_name1[$id]|h}-->：</dt>
        <dd>
            <select name="classcategory_id1" style="<!--{$arrErr.classcategory_id1|sfGetErrorColor}-->">
                <!--{html_options options=$arrClassCat1[$id] selected=$arrProduct.classcategory_id1}-->
            </select>
            <!--{if $arrErr.classcategory_id1 != ""}-->
            <p class="attention">※ <!--{$tpl_class_name1[$id]}-->を入力して下さい。</p>
            <!--{/if}-->
        </dd>
        <!--▲規格1-->
    </dl>
    <!--{if $tpl_classcat_find2[$id]}-->
    <dl class="size02 clearfix">
        <!--▼規格2-->
        <dt><!--{$tpl_class_name2[$id]|h}-->：</dt>
        <dd>
            <select name="classcategory_id2" style="<!--{$arrErr.classcategory_id2|sfGetErrorColor}-->">
            </select>
            <!--{if $arrErr.classcategory_id2 != ""}-->
            <p class="attention">※ <!--{$tpl_class_name2[$id]}-->を入力して下さい。</p>
            <!--{/if}-->
        </dd>
        <!--▲規格2-->
    </dl>
    <!--{/if}-->
</div>
<!--{/if}-->