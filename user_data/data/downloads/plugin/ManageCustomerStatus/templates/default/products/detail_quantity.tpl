<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrProduct.plg_managecustomerstatus_hidden_flg > 0}-->

<!--{else}-->
<div class="spec">
    <dl class="quantity">
        <dt>数量</dt>
        <dd>
            <div class="quantity_wrap">
                <ul>
                    <li><input type="text" class="kazu box60" name="quantity" value="<!--{$arrForm.quantity.value|default:1|h}-->" style="<!--{$arrErr.quantity|sfGetErrorColor}-->" /></li>
                    <!--★ボタン★-->
                    <li><input type="button" name="spinner_up" class="spinner_up" value="＋" onclick="javascript:spinner('up');"></li>
                    <li><input type="button" name="spinner_up" class="spinner_down" value="－" onclick="javascript:spinner('down');"></li>
                </ul>
                <!--{if $arrErr.quantity != ""}-->
                <span class="attention"><!--{$arrErr.quantity}--></span>
                <!--{/if}-->
            </div>
        </dd>
    </dl>
</div>
<script>
function spinner(mode){
	$this=$("dl.quantity input.kazu");
	if(mode=="up"){
		$this.val(parseInt($this.val())+1);
		$("#footerCartBox select.kazu").val($this.val());
	} else if(mode=="down"){
		if($this.val()>0){
			$this.val(parseInt($this.val())-1);
			$("#footerCartBox select.kazu").val($this.val());
		}
	}
}
    $(function(){
		$("input.kazu").on("change",function(){
			$("select.kazu").val($(this).val());
		});
	});
</script>
<!--{/if}-->
