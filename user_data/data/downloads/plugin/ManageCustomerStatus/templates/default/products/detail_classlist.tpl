<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if $arrProduct.plg_managecustomerstatus_hidden_flg > 0}-->

<!--{else}-->


    <!--{*
    <!--{assign var="tmpname1" value=$tpl_class_name1}-->
	<dl class="clearfix">
        <dt name="classcategory_id1_name"><!--{if $tmpname1 != "COLOR"}--><!--{$tmpname1}--><!--{/if}--></dt>
        <dd>
			<div class="selectBox01 colorSelect">
            <select name="classcategory_id1" style="<!--{$arrErr.classcategory_id1|sfGetErrorColor}-->">
                <!--{html_options options=$arrClassCat1 selected=$arrForm.classcategory_id1.value}-->
            </select>
            <!--{if $arrErr.classcategory_id1 != ""}-->
            <br /><span class="attention">※ <!--{$tmpname1}-->を入力して下さい。</span>
            <!--{/if}-->
			</div>
        </dd>
    </dl>

    <!--▲規格1-->

    <!--{if $tpl_classcat_find2}-->
    <!--▼規格2-->
    <!--{assign var="tmpname2" value=$tpl_class_name2}-->
    <dl class="clearfix">
        <dt name="classcategory_id2_name">サイズ</dt>
        <dd>
			<div class="selectBox01 colorSelect">
			<select name="classcategory_id2" style="<!--{$arrErr.classcategory_id2|sfGetErrorColor}-->">
           </select>
            <!--{if $arrErr.classcategory_id2 != ""}-->
            <br /><span class="attention">※ <!--{$tmpname2}-->を入力して下さい。</span>
            <!--{/if}-->
			</div>
        </dd>
    </dl>
    <!--▲規格2-->
    <!--{/if}-->
    *}-->

    <!--{*
    <!--{if $arrProduct.order_flg==1}-->
        <a href="#" onClick="order_type_change();return false;"><div class="sizeInput open">お客様指定のサイズでお直しする</div></a>
        <input type="hidden" name="order_flg_disp" id="order_flg_disp" value="0"/>
        <script>
            function order_type_change(){
                if($("#order_flg_disp").val()==0){
                    $('div.sizeInput').html("既製品から選ぶ");
                    $('div.sizeInput').removeClass("open");
                    //sessionStorage['order_flg_disp']=2;
                    $("#order_flg_disp").val('2');
                } else {
                    $('div.sizeInput').html("お客様指定のサイズでお直しする");
                    $('div.sizeInput').addClass("open");
                    //sessionStorage['order_flg_disp']=0;
                    $("#order_flg_disp").val('0');
                }
                $("select[name=classcategory_id2]").val("");
                $form=$('#form1');
                $sele1=$("select[name=classcategory_id1]");
                $sele2=$("select[name=classcategory_id2]");
                product_id=$form.find('input[name=product_id]').val();
                eccube.setClassCategories($form, product_id, $sele1, $sele2, '');
                //$('.ordertype').slideToggle('slow');
                //eccube.footercart_set();
            }
        </script>
    <!--{elseif  $arrProduct.order_flg==0}-->
        <input type="hidden" name="order_flg_disp" id="order_flg_disp" value="0"/>
    <!--{else}-->
        <input type="hidden" name="order_flg_disp" id="order_flg_disp" value="2"/>
    <!--{/if}-->
    *}-->

<!--{/if}-->
<script>
	//$("select[name^=classcategory]").on("change",function(){
	//	eccube.footercart_set();
	//});
	//$(window).on("load",function(){
	//	eccube.footercart_set();
	//});
	
	//eccube.footercart_set = function(){
	//	classcat_id1=$("select[name=classcategory_id1]").val();
	//	classcat_id2=$("select[name=classcategory_id2]").val();
	//	
    //    classcat_id2 = classcat_id2 ? classcat_id2 : '';

    //    var classcat2;

    //    // 商品一覧時
    //    if (eccube.hasOwnProperty('productsClassCategories')) {
    //        classcat2 = eccube.productsClassCategories[product_id][classcat_id1]['#' + classcat_id2];
    //    }
    //    // 詳細表示時
    //    else {
    //        classcat2 = eccube.classCategories[classcat_id1]['#' + classcat_id2];
    //    }
	//	$form=$("#footerCartBox");
    //    // 通常価格
    //    var $price02_default = $form.find('[id^=price02_default_2]');
    //    var $price02_dynamic = $form.find('[id^=price02_dynamic_2]');
    //    if (classcat2 && typeof classcat2.price02 !== 'undefined' && String(classcat2.price02).length >= 1) {

    //        $price02_dynamic.text(classcat2.price02).show();
    //        $price02_default.hide();
	//	} else {
	//		$price02_dynamic.hide();
	//		$price02_default.show();
	//	}
	//	$form.find('select.kazu').val($("div.cart_area dl.quantity input.kazu").val());
	//}
</script>
