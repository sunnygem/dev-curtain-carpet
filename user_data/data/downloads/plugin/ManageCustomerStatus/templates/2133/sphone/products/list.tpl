<!--{*
*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{if (($smarty.session.customer.customer_id && $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 1) || $smarty.const.PLG_MANAGECUSTOMER_LOGIN_DISP == 0)}-->
<br>
<div class="pricebox sale_price" style="color:#0000FF;"><span class="mini"><!--{if $smarty.const.MEMBER_RANK_PRICE_TITLE_MODE == 1}--><!--{$arrCustomerRank[$customer_rank_id]}--><!--{/if}--><!--{$smarty.const.MEMBER_RANK_PRICE_TITLE}-->(税込):</span>
    <span class="price2" style="color:#0000FF; font-weight:bold;">
        <!--{if strlen($arrProduct.plg_managecustomerstatus_price_min) > 0}-->
        <span id="price03_default_<!--{$id}-->">
            <!--{if $arrProduct.plg_managecustomerstatus_price_min_inctax == $arrProduct.plg_managecustomerstatus_price_max_inctax}-->
            <!--{$arrProduct.plg_managecustomerstatus_price_min_inctax|n2s}-->
            <!--{else}-->
            <!--{$arrProduct.plg_managecustomerstatus_price_min_inctax|n2s}-->～<!--{$arrProduct.plg_managecustomerstatus_price_max_inctax|n2s}-->
            <!--{/if}-->
        </span><span id="price03_dynamic_<!--{$id}-->">
        </span>円
        <!--{/if}-->
    </span>
</div>
<!--{/if}-->