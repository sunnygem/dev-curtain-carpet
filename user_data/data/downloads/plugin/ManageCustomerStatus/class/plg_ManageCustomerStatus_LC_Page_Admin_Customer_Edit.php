<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/plg_ManageCustomerStatus_Utils.php";
require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/class/plg_ManageCustomerStatus_LC_Page.php";

class plg_ManageCustomerStatus_LC_Page_Admin_Customer_Edit extends plg_ManageCustomerStatus_LC_Page
{

    /**
     * @param LC_Page_Admin_Customer_Edit $objPage 会員編集のページクラス
     * @return void
     */
    function after($objPage)
    {
        $objPage->arrPlgManageCustomerStatus = plg_ManageCustomerStatus_Utils::getStatusRankList();
        $objPage->arrPlgManageCustomerStatus[0] = "ランクなし";

        if ($objPage->getMode() == 'edit_search') {
            $objFormSearchParam = new SC_FormParam_Ex();
            $objPage->lfInitSearchParam($objFormSearchParam);
            plg_ManageCustomerStatus_Utils::addSearchManageCustomerStatusParam($objFormSearchParam);
            $objFormSearchParam->setParam($_REQUEST);
            $objPage->arrSearchData = $objFormSearchParam->getSearchArray();
        }
    }

}
