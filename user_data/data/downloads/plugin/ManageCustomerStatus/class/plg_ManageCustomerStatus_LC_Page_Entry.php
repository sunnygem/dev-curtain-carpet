<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/plg_ManageCustomerStatus_Utils.php";
require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/class/plg_ManageCustomerStatus_LC_Page.php";

class plg_ManageCustomerStatus_LC_Page_Entry extends plg_ManageCustomerStatus_LC_Page
{

    /**
     * @param LC_Page_Entry $objPage 会員登録のページクラス
     * @return void
     */
    function complete($objPage)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $status_id = $objQuery->get("status_id", "plg_managecustomerstatus_dtb_customer_status", "initial_rank = ?", array(1));
        if ($status_id > 0) {
            $objQuery->update("dtb_customer", array("plg_managecustomerstatus_status" => $status_id), "email = ? AND del_flg <> 1", array($_POST['email']));

            $objCustomer = new SC_Customer_Ex();
            if ($objCustomer->isLoginSuccess(true)) {
                $objCustomer->setValue('plg_managecustomerstatus_status', $status_id);
            }
        }
    }

}
