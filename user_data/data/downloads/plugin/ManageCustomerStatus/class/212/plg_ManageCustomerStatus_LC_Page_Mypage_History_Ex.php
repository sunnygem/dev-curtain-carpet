<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/class/plg_ManageCustomerStatus_LC_Page_Mypage_History.php";

class plg_ManageCustomerStatus_LC_Page_Mypage_History_Ex extends plg_ManageCustomerStatus_LC_Page_Mypage_History
{

    /**
     * @param LC_Page_Mypage_History $objPage MYページ購入履歴のページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Mypage_History $objPage MYページ購入履歴のページクラス
     * @return void
     */
    function after($objPage)
    {
        parent::after($objPage);
    }

}
