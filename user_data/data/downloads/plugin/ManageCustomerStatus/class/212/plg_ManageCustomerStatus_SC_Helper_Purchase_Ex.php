<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/


require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/class/plg_ManageCustomerStatus_SC_Helper_Purchase.php";

class plg_ManageCustomerStatus_SC_Helper_Purchase_Ex extends plg_ManageCustomerStatus_SC_Helper_Purchase
{
    
}
