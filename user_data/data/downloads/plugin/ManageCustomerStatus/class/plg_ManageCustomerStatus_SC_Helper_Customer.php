<?php

/*
* Plugin Code : ManageCustomerStatus
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ManageCustomerStatus/plg_ManageCustomerStatus_Utils.php";

class plg_ManageCustomerStatus_SC_Helper_Customer extends SC_Helper_Customer
{

    /**
     * 会員一覧検索をする処理（ページング処理付き、管理画面用共通処理）
     *
     * @param array $arrParam 検索パラメーター連想配列
     * @param string $limitMode ページングを利用するか判定用フラグ
     * @return array( integer 全体件数, mixed 会員データ一覧配列, mixed SC_PageNaviオブジェクト)
     */
    function sfGetSearchData($arrParam, $limitMode = '')
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $objSelect = new SC_CustomerList_Ex($arrParam, 'customer');
        $page_max = SC_Utils_Ex::sfGetSearchPageMax($arrParam['search_page_max']);
        $disp_pageno = $arrParam['search_pageno'];
        if ($disp_pageno == 0) {
            $disp_pageno = 1;
        }
        $offset = intval($page_max) * (intval($disp_pageno) - 1);
        if ($limitMode == '') {
            $objQuery->setLimitOffset($page_max, $offset);
        }
        $where = $objSelect->getList();
        if (strpos($where, 'mailmaga_flg FROM') > 0) {
            $where = str_replace('mailmaga_flg FROM', 'mailmaga_flg,plg_managecustomerstatus_status FROM', $where);
        }
        $arrWhere = $objSelect->arrVal;
        $count_where = $objSelect->getListCount();

        $add_where = "";
        // 会員ランク
        if (plg_ManageCustomerStatus_Utils::getECCUBEVer() >= 2130) {
            if (!isset($arrParam['search_plg_managecustomerstatus_status']))
                $arrParam['search_plg_managecustomerstatus_status'] = '';
            if (is_array($arrParam['search_plg_managecustomerstatus_status'])) {
                $add_where .= " AND (";
                foreach ($arrParam['search_plg_managecustomerstatus_status'] as $key => $data) {
                    if ($key > 0)
                        $add_where .= " OR ";
                    $add_where .= "plg_managecustomerstatus_status = ?";
                    $arrWhere[] = $data;
                }
                $add_where .= ")";
            }
            if (strpos($where, 'ORDER BY') > 0) {
                $where = str_replace('ORDER BY', $add_where . ' ORDER BY', $where);
            }
            $count_where .= $add_where;
        } else {
            if (!isset($arrParam['search_plg_managecustomerstatus_status']))
                $arrParam['search_plg_managecustomerstatus_status'] = '';
            if (is_array($arrParam['search_plg_managecustomerstatus_status'])) {
                $add_where .= " AND (";
                foreach ($arrParam['search_plg_managecustomerstatus_status'] as $key => $data) {
                    if ($key > 0)
                        $add_where .= " OR ";
                    $add_where .= "plg_managecustomerstatus_status = ?";
                    $arrWhere[] = $data;
                }
                $add_where .= ")";
            }
            $where .= $add_where;
            $count_where .= $add_where;
        }

        $arrData = $objQuery->getAll($where, $arrWhere);

        // 該当全体件数の取得
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $linemax = $objQuery->getOne($count_where, $arrWhere);

        // ページ送りの取得
        if (plg_ManageCustomerStatus_Utils::getECCUBEVer() >= 2130) {
            $objNavi = new SC_PageNavi_Ex($arrParam['search_pageno'], $linemax, $page_max, 'eccube.moveSearchPage', NAVI_PMAX);
        } else {
            $objNavi = new SC_PageNavi_Ex($arrParam['search_pageno'], $linemax, $page_max, 'fnNaviSearchOnlyPage', NAVI_PMAX);
        }
        return array($linemax, $arrData, $objNavi);
    }

    function getListMailMagazine($is_mobile = false)
    {

        $colomn = $this->getMailMagazineColumn($is_mobile);
        $this->select = "
            SELECT
                $colomn
            FROM
                dtb_customer";
        return $this->getSql(0);
    }

}
