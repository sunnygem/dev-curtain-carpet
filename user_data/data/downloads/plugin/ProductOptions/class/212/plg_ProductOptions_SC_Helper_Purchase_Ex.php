<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_SC_Helper_Purchase.php";

class plg_ProductOptions_SC_Helper_Purchase_Ex extends plg_ProductOptions_SC_Helper_Purchase
{

    /**
     * 受注登録を完了する.
     *
     * 引数の受注情報を受注テーブル及び受注詳細テーブルに登録する.
     * 登録後, 受注一時テーブルに削除フラグを立てる.
     *
     * @param array $orderParams 登録する受注情報の配列
     * @param SC_CartSession $objCartSession カート情報のインスタンス
     * @param integer $cartKey 登録を行うカート情報のキー
     * @param integer 受注ID
     */
    function registerOrderComplete($orderParams, &$objCartSession, $cartKey)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        // 不要な変数を unset
        $unsets = array('mailmaga_flg', 'deliv_check', 'point_check', 'password',
            'reminder', 'reminder_answer', 'mail_flag', 'session');
        foreach ($unsets as $unset) {
            unset($orderParams[$unset]);
        }

        // 対応状況の指定が無い場合は新規受付
        if (SC_Utils_Ex::isBlank($orderParams['status'])) {
            $orderParams['status'] = ORDER_NEW;
        }

        $orderParams['create_date'] = 'CURRENT_TIMESTAMP';
        $orderParams['update_date'] = 'CURRENT_TIMESTAMP';

        // 詳細情報を取得
        $cartItems = $objCartSession->getCartList($cartKey);


        // 詳細情報を生成
        $objProduct = new SC_Product_Ex();
        $i = 0;
        $arrDetail = array();
        $total_inctax = 0;
        $total_point = 0;
        $total_tax = 0;
        $deliv_free_cnt = 0;
        foreach ($cartItems as $item) {
            $p = & $item['productsClass'];
            $arrDetail[$i]['order_id'] = $orderParams['order_id'];
            $arrDetail[$i]['product_id'] = $p['product_id'];
            $arrDetail[$i]['product_class_id'] = $p['product_class_id'];
            $arrDetail[$i]['product_name'] = $p['name'];
            $arrDetail[$i]['product_code'] = $p['product_code'];
            $arrDetail[$i]['classcategory_name1'] = $p['classcategory_name1'];
            $arrDetail[$i]['classcategory_name2'] = $p['classcategory_name2'];
            $arrDetail[$i]['point_rate'] = $item['point_rate'];
            $arrDetail[$i]['price'] = $item['price'];
            $arrDetail[$i]['quantity'] = $item['quantity'];
            if (count($item['plg_productoptions']) > 0) {
                $arrDetail[$i]['plg_productoptions_flg'] = serialize($item['plg_productoptions']);
                $arrDetail[$i]['plg_productoptions'] = $item['plg_productoptions'];
            }

            // 在庫の減少処理
            if (!$objProduct->reduceStock($p['product_class_id'], $item['quantity'])) {
                $objQuery->rollback();
                SC_Utils_Ex::sfDispSiteError(SOLD_OUT, '', true);
            }
            $i++;
        }

        list($total_inctax, $total_point, $deliv_free_cnt, $total_tax) = plg_ProductOptions_Util::lfSetCartProductOptions($arrDetail);

        $add_point = SC_Helper_DB_Ex::sfGetAddPoint($total_point, $orderParams['use_point']);
        if ($orderParams['birth_point'] > 0) {
            $add_point += $orderParams['birth_point'];
        }
        if ($add_point < 0) {
            $add_point = 0;
        }

        //オプションによる変動分で上書き
        $orderParams['add_point'] = $add_point;
        $orderParams['payment_total'] += ($total_inctax - $orderParams['subtotal']);
        $orderParams['total'] += ($total_inctax - $orderParams['subtotal']);
        $orderParams['tax'] = $total_tax;
        $orderParams['subtotal'] = $total_inctax;
        if ($deliv_free_cnt > 0) {
            $orderParams['total'] -= $orderParams['deliv_fee'];
            $orderParams['payment_total'] -= $orderParams['deliv_fee'];
            $orderParams['deliv_fee'] = 0;
        }
        $this->registerOrder($orderParams['order_id'], $orderParams);
        $this->registerOrderDetail($orderParams['order_id'], $arrDetail);

        $objQuery->update('dtb_order_temp', array('del_flg' => 1), 'order_temp_id = ?', array(SC_SiteSession_Ex::getUniqId()));



        return $orderParams['order_id'];
    }

    /**
     * 受注詳細を取得する.
     *
     * @param integer $order_id 受注ID
     * @param boolean $has_order_status 対応状況, 入金日も含める場合 true
     * @return array 受注詳細の配列
     */
    function getOrderDetail($order_id, $has_order_status = true)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $dbFactory = SC_DB_DBFactory_Ex::getInstance();
        $col = <<< __EOS__
            T3.product_id,
            T3.product_class_id as product_class_id,
            T3.product_type_id AS product_type_id,
            T2.product_code,
            T2.product_name,
            T2.classcategory_name1 AS classcategory_name1,
            T2.classcategory_name2 AS classcategory_name2,
            T2.price,
            T2.quantity,
            T2.point_rate,
            T2.plg_productoptions_flg,
__EOS__;
        if ($has_order_status) {
            $col .= 'T1.status AS status, T1.payment_date AS payment_date,';
        }
        $col .= <<< __EOS__
            CASE WHEN
                EXISTS(
                    SELECT * FROM dtb_products
                    WHERE product_id = T3.product_id
                        AND del_flg = 0
                        AND status = 1
                )
                THEN '1'
                ELSE '0'
            END AS enable,
__EOS__;
        $col .= $dbFactory->getDownloadableDaysWhereSql('T1') . ' AS effective';
        $from = <<< __EOS__
            dtb_order T1
            JOIN dtb_order_detail T2
                ON T1.order_id = T2.order_id
            LEFT JOIN dtb_products_class T3
                ON T2.product_class_id = T3.product_class_id
__EOS__;
        $objQuery->setOrder('T2.order_detail_id');
        $ret = $objQuery->select($col, $from, 'T1.order_id = ?', array($order_id));
        plg_ProductOptions_Util::lfSetOrderDetailOptions($ret);
        return $ret;
    }

}
