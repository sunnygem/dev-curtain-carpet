<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page_Admin_Order_ProductSelect.php";

class plg_ProductOptions_LC_Page_Admin_Order_ProductSelect_Ex extends plg_ProductOptions_LC_Page_Admin_Order_ProductSelect
{

    /**
     * @param LC_Page_Admin_Order_ProductSelect $objPage
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Admin_Order_ProductSelect $objPage 商品選択ページクラス
     * @return void
     */
    function after($objPage)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $arrOptionIDs = array();
        foreach ($objPage->arrProducts as $arrProduct) {
            list($arrRet, $values, $arrCheck) = plg_ProductOptions_Util::lfSetProductOptions($arrProduct['product_id']);

            $arrProductOptions[$arrProduct['product_id']] = $arrRet;
            $arrOptionIDs[$arrProduct['product_id']] = $arrCheck;
        }
        $objPage->arrProductOptions = $arrProductOptions;
        $objPage->tpl_productoptions_js = "arrOptionIDs=" . SC_Utils_Ex::jsonEncode($arrOptionIDs) . ';';
    }

}
