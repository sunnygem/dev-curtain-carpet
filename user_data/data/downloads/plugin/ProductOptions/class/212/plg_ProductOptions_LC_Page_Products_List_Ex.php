<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page_Products_List.php";

class plg_ProductOptions_LC_Page_Products_List_Ex extends plg_ProductOptions_LC_Page_Products_List
{

    /**
     * @param LC_Page_Products_List $objPage 商品一覧ページクラス
     * @return void
     */
    function before($objPage)
    {
        //カート処理
        $arrForm = $_REQUEST;

        $objProduct = new SC_Product_Ex();

        $mode = $objPage->getMode();
        //表示条件の取得
        $arrSearchData = array(
            'category_id' => $objPage->lfGetCategoryId(intval($arrForm['category_id'])),
            'maker_id' => intval($arrForm['maker_id']),
            'name' => $arrForm['name']
        );
        $orderby = $arrForm['orderby'];

        //ページング設定
        $tpl_pageno = $arrForm['pageno'];
        $disp_number = self::lfGetDisplayNum($arrForm['disp_number']);

        // 商品一覧データの取得
        $arrSearchCondition = $objPage->lfGetSearchCondition($arrSearchData);
        $tpl_linemax = $objPage->lfGetProductAllNum($arrSearchCondition);
        $urlParam = "category_id={$arrSearchData['category_id']}&pageno=#page#";
        // モバイルの場合に検索条件をURLの引数に追加
        if (SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE) {
            $searchNameUrl = urlencode(mb_convert_encoding($arrSearchData['name'], 'SJIS-win', 'UTF-8'));
            $urlParam .= "&mode={$mode}&name={$searchNameUrl}&orderby={$orderby}";
        }

        $objNavi = new SC_PageNavi_Ex($tpl_pageno, $tpl_linemax, $disp_number, 'fnNaviPage', NAVI_PMAX, $urlParam, SC_Display_Ex::detectDevice() !== DEVICE_TYPE_MOBILE);
        $objPage->lfGetProductsList($arrSearchCondition, $disp_number, $objNavi->start_row, $tpl_linemax, $objProduct);

        // 規格1が設定されている
        $tpl_classcat_find1 = $objProduct->classCat1_find;
        // 規格2が設定されている
        $tpl_classcat_find2 = $objProduct->classCat2_find;

        // カート処理
        $target_product_id = intval($arrForm['product_id']);
        if ($target_product_id > 0) {
            // 商品IDの正当性チェック
            if (!SC_Utils_Ex::sfIsInt($arrForm['product_id']) || !SC_Helper_DB_Ex::sfIsRecord('dtb_products', 'product_id', $arrForm['product_id'], 'del_flg = 0 AND status = 1')) {
                SC_Utils_Ex::sfDispSiteError(PRODUCT_NOT_FOUND);
            }

            // 入力内容のチェック
            $objPage->arrErr = $objPage->lfCheckError($target_product_id, $arrForm, $tpl_classcat_find1, $tpl_classcat_find2);

            $productoptions = $_REQUEST['plg_productoptions'];
            $objPage->plg_productoptions_Err[$target_product_id] = self::lfCheckErrorOptions($target_product_id, $productoptions);

            if (count($objPage->plg_productoptions_Err[$target_product_id]) > 0) {
                $objPage->plg_productoptions_Form[$target_product_id] = $arrForm['plg_productoptions'];
                $_POST['target_product_id'] = $target_product_id;
                $_REQUEST['product_id'] = $_GET['product_id'] = $_POST['product_id'] = "";
            }

            if (empty($objPage->arrErr) && empty($objPage->plg_productoptions_Err[$target_product_id])) {

                $objCartSess = new SC_CartSession_Ex();
                $product_class_id = $arrForm['product_class_id'];
                $productoptions = $arrForm['plg_productoptions'];
                $quantity = $arrForm['quantity'];
                foreach ((array) $productoptions as $option_id => $optioncategory_id) {
                    $default_id = plg_ProductOptions_Util::lfGetOptionDefaultCatId($option_id);
                    if (is_null($optioncategory_id) || $optioncategory_id == "" || $default_id == $optioncategory_id) {
                        if (isset($productoptions[$option_id]))
                            unset($productoptions[$option_id]);
                    }
                }

                $objCartSess = new SC_CartSession_Ex();
                $objCartSess->addProduct($product_class_id, $quantity, $productoptions);

                self::sendRedirect(CART_URLPATH);
                exit;
            }
        }
    }

    /**
     * @param LC_Page_Products_List $objPage 商品一覧ページクラス
     * @return void
     */
    function after($objPage)
    {
        parent::after($objPage);
        $CONF = SC_Helper_DB_Ex::sfGetBasisData();

        $objPage->tpl_productoptions_js .= "tax_rate_base=" . SC_Utils_Ex::jsonEncode($CONF['tax']) . ';';
        $objPage->tpl_productoptions_js .= "tax_rule_base=" . SC_Utils_Ex::jsonEncode($CONF['tax_rule']) . ';';

        $js_fnOnLoad = "";
        foreach ($objPage->arrProducts as $arrProduct) {
            if ($arrProduct['stock_unlimited_max'] || $arrProduct['stock_max'] > 0) {
                $js_fnOnLoad .= "fnSetClassCategories(document.product_form{$arrProduct['product_id']});";
            }
        }

        $target_product_id = intval($_POST['target_product_id']);
        if ($target_product_id > 0) {

            $js_fnOnLoad .= $objPage->lfSetSelectedData($objPage->arrProducts, $objPage->arrForm, $objPage->arrErr, $target_product_id);
        }
        $objPage->tpl_javascript .= 'function fnOnLoad(){' . $js_fnOnLoad . '}';
        $objPage->tpl_onload .= 'fnOnLoad(); ';
    }

}
