<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Shopping_Multiple extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Shopping_Multiple $objPage 複数配送先ページクラス
     * @return void
     */
    function after($objPage)
    {
        if ($objPage->getMode() != 'confirm') {
            $objFormParam = new SC_FormParam_Ex();
            $objCartSess = new SC_CartSession_Ex();

            $objPage->lfInitParam($objFormParam);
            self::setParamToSplitItems($objFormParam, $objCartSess);

            // 前のページから戻ってきた場合
            if ($_GET['from'] == 'multiple') {
                $objFormParam->setParam($_SESSION['multiple_temp']);
            }
            $objPage->arrForm = $objFormParam->getFormParamList();
        }

        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $objCartSess = new SC_CartSession_Ex();
        $cartLists = & $objCartSess->getCartList($objCartSess->getKey());
        foreach ($objPage->arrForm['cart_no']['value'] as $key => $cart_no) {
            $add_price = 0;
            foreach ($cartLists as $cartitem) {
                if ($cartitem['cart_no'] == $cart_no) {
                    foreach ($cartitem['plg_productoptions'] as $option_id => $optioncategory_id) {
                        if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                            $objPage->arrForm['plg_productoptions_optioncategory_name']['value'][$key][] = $optioncategory_id;
                            $value = $optioncategory_id;
                            $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                            $objPage->arrForm['plg_productoptions_option_name']['value'][$key][] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                        } else {
                            $objPage->arrForm['plg_productoptions_optioncategory_name']['value'][$key][] = plg_ProductOptions_Util::lfGetOptionCatName($optioncategory_id);
                            $value = plg_ProductOptions_Util::lfGetOptionCatValue($optioncategory_id);
                            $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                            $objPage->arrForm['plg_productoptions_option_name']['value'][$key][] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                            if ($option_action == 1) {
                                $add_price += $value;
                            }
                        }
                    }
                }
            }
            $objPage->arrForm['add_price']['value'][$key] = $add_price;
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                $product_id = $objQuery->get('product_id', 'dtb_products_class', 'product_class_id = ?', array($objPage->arrForm['product_class_id']['value'][$key]));
                $objPage->arrForm['add_price_inctax']['value'][$key] = SC_Helper_TaxRule_Ex::sfCalcIncTax($add_price, $product_id, $objPage->arrForm['product_class_id']['value'][$key]);
            }
        }
    }

    /**
     * カートの商品を数量ごとに分割し, フォームに設定する.
     *
     * @param  SC_FormParam   $objFormParam SC_FormParam インスタンス
     * @param  SC_CartSession $objCartSess  SC_CartSession インスタンス
     * @return void
     */
    public function setParamToSplitItems(&$objFormParam, &$objCartSess)
    {
        $cartLists = & $objCartSess->getCartList($objCartSess->getKey());
        $arrItems = array();
        $index = 0;
        foreach (array_keys($cartLists) as $key) {
            $arrProductsClass = $cartLists[$key]['productsClass'];
            $quantity = (int) $cartLists[$key]['quantity'];
            for ($i = 0; $i < $quantity; $i++) {
                foreach ($arrProductsClass as $key2 => $val) {
                    $arrItems[$key2][$index] = $val;
                }
                $arrItems['cart_no'][$index] = $cartLists[$key]['cart_no'];
                $arrItems['quantity'][$index] = 1;
                $arrItems['price'][$index] = $cartLists[$key]['price'];
                $arrItems['price_inctax'][$index] = $cartLists[$key]['price_inctax'];
                $index++;
            }
        }
        $objFormParam->setParam($arrItems);
        $objFormParam->setValue('line_of_num', $index);
    }

    /**
     * 入力チェックを行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array        エラー情報の配列
     */
    public function lfCheckError(&$objFormParam)
    {
        $objCartSess = new SC_CartSession_Ex();

        $objFormParam->convParam();
        // 数量未入力は0に置換
        $objFormParam->setValue('quantity', $objFormParam->getValue('quantity', 0));

        $arrErr = $objFormParam->checkError();

        $arrParams = $objFormParam->getSwapArray();

        if (empty($arrErr)) {
            foreach ($arrParams as $index => $arrParam) {
                // 数量0で、お届け先を選択している場合
                if ($arrParam['quantity'] == 0 && !SC_Utils_Ex::isBlank($arrParam['shipping'])) {
                    $arrErr['shipping'][$index] = '※ 数量が0の場合、お届け先を入力できません。<br />';
                    ;
                }
                // 数量の入力があり、お届け先を選択していない場合
                if ($arrParam['quantity'] > 0 && SC_Utils_Ex::isBlank($arrParam['shipping'])) {
                    $arrErr['shipping'][$index] = '※ お届け先が入力されていません。<br />';
                }
            }
        }

        // 入力エラーが無い場合、カゴの中身との数量の整合を確認
        if (empty($arrErr)) {
            $arrQuantity = array();
            // 入力内容を集計
            foreach ($arrParams as $arrParam) {
                $product_class_id = $arrParam['product_class_id'];
                $cart_no = $arrParam['cart_no'];
                $arrQuantity[$product_class_id][$cart_no] += $arrParam['quantity'];
            }
            // カゴの中身と突き合わせ
            $cartLists = & $objCartSess->getCartList($objCartSess->getKey());
            foreach ($cartLists as $arrCartRow) {
                $product_class_id = $arrCartRow['id'];
                $cart_no = $arrCartRow['cart_no'];
                // 差異がある場合、エラーを記録
                if ($arrCartRow['quantity'] != $arrQuantity[$product_class_id][$cart_no]) {
                    foreach ($arrParams as $index => $arrParam) {
                        if ($arrParam['product_class_id'] == $product_class_id && $arrParam['cart_no'] == $cart_no) {
                            $arrErr['quantity'][$index] = '※ 数量合計を「' . $arrCartRow['quantity'] . '」にしてください。<br />';
                        }
                    }
                }
            }
        }

        return $arrErr;
    }

}
