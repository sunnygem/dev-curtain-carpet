<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Mypage_History extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Mypage_History $objPage 購入履歴ページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Mypage_History $objPage 購入履歴ページクラス
     * @return void
     */
    function after($objPage)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        foreach ($objPage->arrShipping as $key => $shipping) {
            foreach ($shipping["shipment_item"] as $key2 => $item) {
                $objPage->arrShipping[$key]["shipment_item"][$key2]["plg_productoptions_flg"] = $objQuery->get("plg_productoptions_flg", "dtb_order_detail", "product_class_id = ? AND order_id = ?", array($item['product_class_id'], $item['order_id']));
            }
            plg_ProductOptions_Util::lfSetOrderDetailOptions($objPage->arrShipping[$key]['shipment_item']);
        }

        $objPage->is_price_change = false;
        foreach ($objPage->tpl_arrOrderDetail as $key => $arrOrderDetail) {
            $objPage->tpl_arrOrderDetail[$key]['product_price_inctax'] += $objPage->tpl_arrOrderDetail[$key]['add_price_inctax'];
            if ($objPage->tpl_arrOrderDetail[$key]['product_price_inctax'] != $objPage->tpl_arrOrderDetail[$key]['price_inctax']) {
                $objPage->is_price_change = true;
            }
        }
    }

}
