<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

class LC_Page_Admin_Products_Options extends LC_Page_Admin_Ex
{
    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init()
    {
        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/admin/products/options.tpl";
        $this->tpl_subnavi = 'products/subnavi.tpl';
        $this->tpl_mainno = 'products';
        $this->tpl_subno = 'options';
        $this->tpl_subtitle = 'オプション管理';

        $this->arrACTION = plg_ProductOptions_Util::getActionList();
        $this->arrTYPE = plg_ProductOptions_Util::getTypeList();
        $this->arrDESC_FLG = plg_ProductOptions_Util::getDescriptionFlg();
        $this->arrDispFLG = plg_ProductOptions_Util::getDispFlg();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action()
    {

        $objFormParam = new SC_FormParam_Ex();

        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();
        $option_id = $objFormParam->getValue('option_id');

        // 要求判定
        switch ($this->getMode()) {
            // 編集処理
            case 'edit':
                //パラメーターの取得
                $this->arrForm = $objFormParam->getHashArray();
                // 入力パラメーターチェック
                $this->arrErr = $this->lfCheckError($objFormParam);
                if (SC_Utils_Ex::isBlank($this->arrErr)) {
                    //新規規格追加かどうかを判定する
                    $is_insert = $this->lfCheckInsert($this->arrForm);
                    $arrForm = $this->convertRegistData($this->arrForm);
                    if ($is_insert) {
                        $this->lfInsertOption($arrForm); // 新規作成
                    } else {
                        $this->lfUpdateOption($arrForm); // 既存編集
                    }

                    // 再表示
                    SC_Response_Ex::reload();
                }
                break;
            // 削除
            case 'delete':
                if (!$this->lfCheckUseOption($option_id)) {
                    //規格データの削除処理
                    $this->lfDeleteOption($option_id);
                    // 再表示
                    SC_Response_Ex::reload();
                } else {
                    $this->tpl_onload = "window.alert('このオプションは使用中ですので削除できません。');";
                    unset($option_id);
                }
                break;
            // 編集前処理
            case 'pre_edit':
                // 規格名を取得する。
                $option_manage_name = plg_ProductOptions_Util::lfGetOptionManageName($option_id);
                $option_name = plg_ProductOptions_Util::lfGetOptionName($option_id);
                $option_type = plg_ProductOptions_Util::lfGetOptionType($option_id);
                $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                $option_desc_flg = plg_ProductOptions_Util::lfGetOptionDescriptionFlg($option_id);
                $option_description = plg_ProductOptions_Util::lfGetOptionDescription($option_id);
                $option_pricedisp_flg = plg_ProductOptions_Util::lfGetOptionPricedispFlg($option_id);
                $option_is_required = plg_ProductOptions_Util::lfGetOptionIsRequired($option_id);
                // 入力項目にカテゴリ名を入力する。
                $this->arrForm['manage_name'] = $option_manage_name;
                $this->arrForm['name'] = $option_name;
                $this->arrForm['type'] = $option_type;
                $this->arrForm['action'] = $option_action;
                $this->arrForm['description_flg'] = $option_desc_flg;
                $this->arrForm['description'] = $option_description;
                $this->arrForm['pricedisp_flg'] = $option_pricedisp_flg;
                $this->arrForm['is_required'] = $option_is_required;
                break;
            case 'down':
                $this->lfDownRank($option_id);
                $option_id = '';
                $this->tpl_onload = $this->getAnchorHash("list_table");
                break;
            case 'up':
                $this->lfUpRank($option_id);
                $option_id = '';
                $this->tpl_onload = $this->getAnchorHash("list_table");
                break;
            default:
                break;
        }
        // 規格の読込
        $this->arrOption = plg_ProductOptions_Util::lfGetOption();
        $this->arrOptionCatCount = $this->sfGetOptionCatCount();
        // POSTデータを引き継ぐ
        $this->tpl_option_id = $option_id;
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy()
    {
        if (method_exists('LC_Page_Admin_Ex', 'destroy')) {
            parent::destroy();
        }
    }

    /**
     * パラメーターの初期化を行う.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam)
    {
        $objFormParam->addParam('オプション名', 'name', STEXT_LEN, 'KVa', array('EXIST_CHECK', 'SPTAB_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('オプション管理名', 'manage_name', STEXT_LEN, 'KVa', array('EXIST_CHECK', 'SPTAB_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('説明文', 'description', LLTEXT_LEN, 'KVa', array('MAX_LENGTH_CHECK'));
        $objFormParam->addParam('オプションID', 'option_id', INT_LEN, 'n', array('NUM_CHECK'));
        $objFormParam->addParam('タイプ', 'type', INT_LEN, 'n', array('NUM_CHECK'));
        $objFormParam->addParam('アクション', 'action', INT_LEN, 'n', array('NUM_CHECK'));
        $objFormParam->addParam('オプション説明', 'description_flg', INT_LEN, 'n', array('NUM_CHECK'));
        $objFormParam->addParam('価格・ポイントの表示', 'pricedisp_flg', INT_LEN, 'n', array('NUM_CHECK'));
        $objFormParam->addParam('必須チェック', 'is_required', INT_LEN, 'n', array('NUM_CHECK'));
    }

    /**
     * オプション情報を新規登録
     *
     * @param array $arrForm フォームパラメータークラス
     * @return integer 更新件数
     */
    function lfInsertOption($arrForm)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        // INSERTする値を作成する。
        $sqlval['name'] = $arrForm['name'];
        $sqlval['manage_name'] = $arrForm['manage_name'];
        $sqlval['description'] = $arrForm['description'];
        $sqlval['type'] = $arrForm['type'];
        $sqlval['action'] = $arrForm['action'];
        $sqlval['description_flg'] = $arrForm['description_flg'];
        $sqlval['pricedisp_flg'] = $arrForm['pricedisp_flg'];
        $sqlval['is_required'] = $arrForm['is_required'];
        $sqlval['creator_id'] = $_SESSION['member_id'];
        $sqlval['rank'] = $objQuery->max('rank', 'plg_productoptions_dtb_option') + 1;
        $sqlval['create_date'] = 'CURRENT_TIMESTAMP';
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        // INSERTの実行
        $max = $objQuery->max('option_id', 'plg_productoptions_dtb_option') + 1;
        $next = $objQuery->nextVal('plg_productoptions_dtb_option_option_id');
        if ($max > $next) {
            $option_id = $max;
        } else {
            $option_id = $next;
        }
        $sqlval['option_id'] = $option_id;
        $ret = $objQuery->insert('plg_productoptions_dtb_option', $sqlval);
        return $ret;
    }

    /**
     * オプション情報を更新
     *
     * @param array $arrForm フォームパラメータークラス

     * @return integer 更新件数
     */
    function lfUpdateOption($arrForm)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $objQuery->begin();

        $prev_action = plg_ProductOptions_Util::lfGetOptionAction($arrForm['option_id']);

        // UPDATEする値を作成する。
        $sqlval['name'] = $arrForm['name'];
        $sqlval['manage_name'] = $arrForm['manage_name'];
        $sqlval['description'] = $arrForm['description'];
        $sqlval['type'] = $arrForm['type'];
        $sqlval['action'] = $arrForm['action'];
        $sqlval['description_flg'] = $arrForm['description_flg'];
        $sqlval['pricedisp_flg'] = $arrForm['pricedisp_flg'];
        $sqlval['is_required'] = $arrForm['is_required'];
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $where = 'option_id = ?';
        // UPDATEの実行
        $ret = $objQuery->update('plg_productoptions_dtb_option', $sqlval, $where, array($arrForm['option_id']));

        //アクションが変更になった場合は選択肢の値設定をクリアする
        if ($arrForm['action'] != $prev_action) {
            $objQuery->update('plg_productoptions_dtb_optioncategory', array("value" => ""), "option_id = ?", array($arrForm['option_id']));
        }

        $objQuery->commit();

        return $ret;
    }

    /**
     * オプションが使用されているかどうか確認.
     *
     * @param integer $option_id オプションID
     * @return boolean 使用されている true されていない false
     */
    function lfCheckUseOption($search_option_id)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $ret = $objQuery->select('plg_productoptions_flg', 'dtb_products', 'plg_productoptions_flg IS NOT NULL AND del_flg <> 1');
        $cnt = 0;
        foreach ((array) $ret as $item) {
            $options = explode(',', $item['plg_productoptions_flg']);
            foreach ((array) $options as $option_id) {
                if ($search_option_id == $option_id) {
                    $cnt++;
                    break 2;
                }
            }
        }

        if ($cnt > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * オプション情報を削除する.
     *
     * @param integer $option_id オプションID
     * @param SC_Helper_DB $objDb SC_Helper_DBのインスタンス
     * @return integer 削除件数
     */
    function lfDeleteOption($option_id)
    {
        $objDb = new SC_Helper_DB_Ex();
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $ret = $objDb->sfDeleteRankRecord('plg_productoptions_dtb_option', 'option_id', $option_id, '', true);
        $where = 'option_id = ?';
        $objQuery->delete('plg_productoptions_dtb_optioncategory', $where, array($option_id));
        return $ret;
    }

    /**
     * エラーチェック
     *
     * @param array $objFormParam フォームパラメータークラス
     * @return array エラー配列
     */
    function lfCheckError(&$objFormParam)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $arrForm = $objFormParam->getHashArray();
        // パラメーターの基本チェック
        $arrErr = $objFormParam->checkError();
        if (!SC_Utils_Ex::isBlank($arrErr)) {
            return $arrErr;
        } else {
            $arrForm = $objFormParam->getHashArray();
        }

        $where = 'del_flg = 0 AND manage_name = ?';
        $arrClass = $objQuery->select('option_id, manage_name', 'plg_productoptions_dtb_option', $where, array($arrForm['manage_name']));
        // 編集中のレコード以外に同じ名称が存在する場合
        if ($arrClass[0]['option_id'] != $arrForm['option_id'] && $arrClass[0]['manage_name'] == $arrForm['manage_name']) {
            $arrErr['manage_name'] = '※ 既に同じ内容の管理名で登録されています。<br>';
        }
        return $arrErr;
    }

    /**
     * 新規オプション追加かどうかを判定する.
     *
     * @param string $arrForm フォームの入力値
     * @return boolean 新規オプション追加の場合 true
     */
    function lfCheckInsert($arrForm)
    {
        if (empty($arrForm['option_id'])) {
            return true;
        } else {
            return false;
        }
    }

    function convertRegistData($arrForm)
    {
        if ($arrForm['type'] == 3 || $arrForm['type'] == 4) {
            $arrForm['action'] = 0;
//			$arrForm['description_flg'] = 0;
        }
        if ($arrForm['action'] == 0) {
            $arrForm['pricedisp_flg'] = 0;
        }
        return $arrForm;
    }

    /**
     * 並び順を上げる
     *
     * @param integer $option_id 規格ID
     * @return void
     */
    function lfUpRank($option_id)
    {
        $objDb = new SC_Helper_DB_Ex();
        $objDb->sfRankUp('plg_productoptions_dtb_option', 'option_id', $option_id);
    }

    /**
     * 並び順を下げる
     *
     * @param integer $option_id 規格ID
     * @return void
     */
    function lfDownRank($option_id)
    {
        $objDb = new SC_Helper_DB_Ex();
        $objDb->sfRankDown('plg_productoptions_dtb_option', 'option_id', $option_id);
    }

    /* 規格分類の件数取得 */

    function sfGetOptionCatCount()
    {
        $sql = 'select count(plg_productoptions_dtb_option.option_id) as count, plg_productoptions_dtb_option.option_id ';
        $sql.= 'from plg_productoptions_dtb_option inner join plg_productoptions_dtb_optioncategory on plg_productoptions_dtb_option.option_id = plg_productoptions_dtb_optioncategory.option_id ';
        $sql.= 'where plg_productoptions_dtb_option.del_flg = 0 AND plg_productoptions_dtb_optioncategory.del_flg = 0 ';
        $sql.= 'group by plg_productoptions_dtb_option.option_id, plg_productoptions_dtb_option.name';
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $arrList = $objQuery->getAll($sql);
        // キーと値をセットした配列を取得
        $arrRet = SC_Utils_Ex::sfArrKeyValue($arrList, 'option_id', 'count');

        return $arrRet;
    }

    /**
     * アンカーハッシュ文字列を取得する
     * アンカーキーをサニタイジングする
     *
     * @param string $anchor_key フォーム入力パラメーターで受け取ったアンカーキー
     * @return <type>
     */
    function getAnchorHash($anchor_key)
    {
        if ($anchor_key != '') {
            return "location.hash='#" . htmlspecialchars($anchor_key) . "'";
        } else {
            return '';
        }
    }

}
