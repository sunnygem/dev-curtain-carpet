<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

class plg_ProductOptions_LC_Template
{

    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename)
    {
        $objTransform = new SC_Helper_Transform($source);
        $template_dir = PLUGIN_UPLOAD_REALDIR . 'ProductOptions/templates/';

        switch ($objPage->arrPageLayout['device_type_id']) {
            case DEVICE_TYPE_PC:
                $template_dir .= "default/";
                if (strpos($filename, 'products/list.tpl') !== false) {
                    $objTransform->select('.cartin', 0, false)->insertBefore(file_get_contents($template_dir . 'products/list.tpl'));
                }
                if (strpos($filename, 'cart/index.tpl') !== false || strpos($filename, 'quick_cart_index.tpl' !== false)) {
                    $objTransform->select('td', 2, false)->appendChild(file_get_contents($template_dir . 'cart/index.tpl'));
                }
                if (strpos($filename, 'shopping/multiple.tpl') !== false) {
                    $objTransform->select('form input', 5, false)->replaceElement(file_get_contents($template_dir . 'shopping/multiple_cart_no.tpl'));
                }
                if (strpos($filename, 'shopping/confirm.tpl') !== false || strpos($filename, 'quick_shopping_confirm.tpl' !== false)) {
                    $objTransform->select('td', 1, false)->find('ul', 0, false)->appendChild(file_get_contents($template_dir . 'shopping/confirm_order.tpl'));
                }
                if (strpos($filename, 'mypage/history.tpl') !== false) {
                    $objTransform->select('td', 1, false)->appendChild(file_get_contents($template_dir . 'mypage/history.tpl'));
                    $objTransform->select('td', 15, false)->appendChild(file_get_contents($template_dir . 'mypage/history_shipping.tpl'));
                }
                break;
            case DEVICE_TYPE_MOBILE:
                $template_dir .= "mobile/";
                if (strpos($filename, 'products/select_item.tpl') !== false) {
                    $objTransform->select('form', 0, false)->appendFirst(file_get_contents($template_dir . 'products/select_item.tpl'));
                    if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                        $objTransform->select('', 0, false)->replaceElement(file_get_contents($template_dir . 'products/select_item_replace.tpl'));
                    }
                }
                if (strpos($filename, 'cart/index.tpl') !== false || strpos($filename, 'quick_cart_index.tpl' !== false)) {
                    $objTransform->select('form', 0, false)->replaceElement(file_get_contents($template_dir . 'cart/index.tpl'));
                }
                if (strpos($filename, 'mypage/history.tpl') !== false) {
                    $objTransform->select('', 0, false)->replaceElement(file_get_contents($template_dir . 'mypage/history.tpl'));
                }
                break;
            case DEVICE_TYPE_SMARTPHONE:
                $template_dir .= "sphone/";
                if (strpos($filename, 'cart/index.tpl') !== false || strpos($filename, 'quick_cart_index.tpl' !== false)) {
                    $objTransform->select('div.cartitemBox div.cartinContents div p span.mini', 2, false)->insertBefore(file_get_contents($template_dir . 'cart/index.tpl'));
                }
                if (strpos($filename, 'shopping/multiple.tpl') !== false) {
                    $objTransform->select('form input', 5, false)->replaceElement(file_get_contents($template_dir . 'shopping/multiple_cart_no.tpl'));
                }
                if (strpos($filename, 'shopping/confirm.tpl') !== false || strpos($filename, 'quick_shopping_confirm.tpl' !== false)) {
                    $objTransform->select('div.cartconfirmContents div p', 0, false)->appendChild(file_get_contents($template_dir . 'shopping/confirm.tpl'));
                    $objTransform->select('section.delivconfirm_area div.form_area div.formBox div.cartcartconfirmarea div.cartconfirmBox div.cartconfirmContents p', 0, false)->appendChild(file_get_contents($template_dir . 'shopping/confirm.tpl'));
                }
                break;
            case DEVICE_TYPE_ADMIN:
            default:
                $template_dir .= "admin/";
                if (strpos($filename, 'products/subnavi.tpl') !== false) {
                    $objTransform->select('ul.level1', 0)->appendChild(file_get_contents($template_dir . 'products/subnavi.tpl'));
                }
                if (strpos($filename, 'products/index.tpl') !== false) {
                    $objTransform->select('table#products-search-result tr th', 10)->insertBefore(file_get_contents($template_dir . 'products/index_th.tpl'));
                    $objTransform->select('table#products-search-result tr td', 10)->insertBefore(file_get_contents($template_dir . 'products/index_td.tpl'));
                    $objTransform->select('table#products-search-result col', 6)->replaceElement(file_get_contents($template_dir . 'products/index_col.tpl'));
                    $objTransform->select('table#products-search-result col', 5)->replaceElement(file_get_contents($template_dir . 'products/index_col2.tpl'));
                }
                if (strpos($filename, 'order/edit.tpl') !== false || strpos($filename, 'order_edit.tpl') !== false) {
                    $objTransform->select('form#form1', 0, false)->appendFirst(file_get_contents($template_dir . 'order/edit_hidden.tpl'));
                }
                if (strpos($filename, 'order/multiple.tpl') !== false) {
                    $objTransform->select('', 0)->replaceElement(file_get_contents($template_dir . 'order/multiple.tpl'));
                }
                if (strpos($filename, 'order/product_select.tpl') !== false) {
                    $objTransform->select('table.list td', 2)->appendChild(file_get_contents($template_dir . 'order/product_select.tpl'));
                    $objTransform->select('table.list td.center', 1)->replaceElement(file_get_contents($template_dir . 'order/product_select_submit.tpl'));
                }
                if (strpos($filename, 'order/disp.tpl') !== false) {
                    $objTransform->select('table.list td', 1)->replaceElement(file_get_contents($template_dir . 'order/disp.tpl'));
                }
                // スマホnavi_headerブロックへのフックポイントがないのでここで処理する
                if (strpos($filename, 'frontparts/bloc/navi_header.tpl') !== false) {
                    $objCartSess = new SC_CartSession_Ex();
                    $arrInfo = SC_Helper_DB_Ex::sfGetBasisData();
                    $cartItems = $objCartSess->getAllCartList();
                    foreach ($cartItems as $cartkey => $item) {
                        list($sub_total_inctax, $sub_total_point, $deliv_free_cnt) = plg_ProductOptions_Util::lfSetCartProductOptions($item);

                        $tpl_total_inctax[$cartkey] = $sub_total_inctax;

                        $tpl_deliv_free[$cartkey] = $arrInfo['free_rule'] - $tpl_total_inctax[$cartkey];

                        $arrData[$cartkey]['is_deliv_free'] = false;
                        // 送料無料の購入数が設定されている場合
                        if (DELIV_FREE_AMOUNT > 0) {
                            // 商品の合計数量
                            $total_quantity = $objCartSess->getTotalQuantity($cartkey);

                            if ($total_quantity >= DELIV_FREE_AMOUNT) {
                                $arrData[$cartkey]['is_deliv_free'] = true;
                            }
                        }

                        // 送料無料条件が設定されている場合
                        if ($arrInfo['free_rule'] > 0) {
                            // 小計が送料無料条件以上の場合
                            if ($tpl_total_inctax[$cartkey] >= $arrInfo['free_rule']) {
                                $arrData[$cartkey]['is_deliv_free'] = true;
                            }
                        }

                        if ($deliv_free_cnt > 0) {
                            $arrData[$cartkey]['is_deliv_free'] = true;
                        }
                    }
                    if ($arrData[PRODUCT_TYPE_NORMAL]['is_deliv_free']) {
                        $objTransform->select('p.attention', 0, false)->replaceElement('<p class="attention free_money_area">現在、送料無料です</p>');
                    }
                }
                break;
        }
        $source = $objTransform->getHTML();
    }

    function postfilterTransform(&$source, LC_Page_Ex $objPage, $filename)
    {
        
    }

}
