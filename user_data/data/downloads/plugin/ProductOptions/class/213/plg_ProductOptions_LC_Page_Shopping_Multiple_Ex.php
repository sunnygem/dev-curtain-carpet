<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page_Shopping_Multiple.php";

class plg_ProductOptions_LC_Page_Shopping_Multiple_Ex extends plg_ProductOptions_LC_Page_Shopping_Multiple
{

    /**
     * @param LC_Page_Shopping_Multiple $objPage 複数配送先ページクラス
     * @return void
     */
    function before($objPage)
    {
        if ($objPage->getMode() == 'confirm') {
            $objPurchase = new SC_Helper_Purchase_Ex();

            $objSiteSess = new SC_SiteSession_Ex();
            $objCustomer = new SC_Customer_Ex();
            $objFormParam = new SC_FormParam_Ex();
            $objAddress = new SC_Helper_Address_Ex();


            $tpl_uniqid = $objSiteSess->getUniqId();

            $objPage->lfInitParam($objFormParam);
            $objFormParam->setParam($_POST);
            $arrErr = self::lfCheckError($objFormParam);
            if (SC_Utils_Ex::isBlank($arrErr)) {
                // フォームの情報を一時保存しておく
                $_SESSION['multiple_temp'] = $objFormParam->getHashArray();
                self::saveMultipleShippings($tpl_uniqid, $objFormParam, $objCustomer, $objPurchase, $objAddress);

                $objSiteSess->setRegistFlag();

                self::sendRedirect('payment.php');
                exit;
            }
        }
    }

    /**
     * @param LC_Page_Shopping_Multiple $objPage 複数配送先ページクラス
     * @return void
     */
    function after($objPage)
    {
        parent::after($objPage);
    }

    public function saveMultipleShippings($uniqid, &$objFormParam, &$objCustomer, &$objPurchase, &$objAddress)
    {
        $arrParams = $objFormParam->getSwapArray();

        foreach ($arrParams as $arrParam) {
            $other_deliv_id = $arrParam['shipping'];

            if ($objCustomer->isLoginSuccess(true)) {
                if ($other_deliv_id != 0) {
                    if (plg_ProductOptions_Util::getECCUBEVer() >= 2132) {
                        $otherDeliv = $objAddress->getAddress($other_deliv_id, $objCustomer->getValue('customer_id'));
                    } else {
                        $otherDeliv = $objAddress->getAddress($other_deliv_id);
                    }

                    if (!$otherDeliv) {
                        SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, '', false, "入力値が不正です。<br />正しい値を入力してください。");
                        exit;
                    }

                    foreach ($otherDeliv as $key => $val) {
                        $arrValues[$other_deliv_id]['shipping_' . $key] = $val;
                    }
                } else {
                    $objPurchase->copyFromCustomer($arrValues[0], $objCustomer, 'shipping');
                }
            } else {
                $arrValues = $objPurchase->getShippingTemp();
            }
            $arrItemTemp[$other_deliv_id][$arrParam['product_class_id']][$arrParam['cart_no']] += $arrParam['quantity'];
        }

        $objPurchase->clearShipmentItemTemp();

        foreach ($arrValues as $shipping_id => $arrVal) {
            $objPurchase->saveShippingTemp($arrVal, $shipping_id);
        }

        foreach ($arrItemTemp as $other_deliv_id => $arrProductClassIds) {
            foreach ($arrProductClassIds as $product_class_id => $cart_nos) {
                foreach ($cart_nos as $cart_no => $quantity) {
                    if ($quantity == 0)
                        continue;
                    $objPurchase->setShipmentItemTemp($other_deliv_id, $product_class_id, $quantity, $cart_no);
                }
            }
        }

        //不必要な配送先を削除
        foreach ($_SESSION['shipping'] as $id => $arrShipping) {
            if (!isset($arrShipping['shipment_item'])) {
                $objPurchase->unsetOneShippingTemp($id);
            }
        }

        // $arrValues[0] には, 購入者の情報が格納されている
        $objPurchase->saveOrderTemp($uniqid, $arrValues[0], $objCustomer);
    }

}
