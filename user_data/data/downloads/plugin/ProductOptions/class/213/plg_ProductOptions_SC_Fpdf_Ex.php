<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_SC_Fpdf.php";

class plg_ProductOptions_SC_Fpdf_Ex extends plg_ProductOptions_SC_Fpdf
{

    public function setData($arrData)
    {
        $this->arrData = $arrData;

        // ページ番号よりIDを取得
        $tplidx = $this->ImportPage(1);

        // ページを追加（新規）
        $this->AddPage();

        //表示倍率(100%)
        $this->SetDisplayMode($this->tpl_dispmode);

        if (SC_Utils_Ex::sfIsInt($arrData['order_id'])) {
            $this->disp_mode = true;
        }

        // テンプレート内容の位置、幅を調整 ※useTemplateに引数を与えなければ100%表示がデフォルト
        $this->useTemplate($tplidx);

        $this->setShopData2();
        $this->setMessageData2();
        $this->setOrderData2();
        $this->setEtcData2();
    }

    private function setOrderData2()
    {

        unset($this->label_cell);
        $this->label_cell[] = '商品名 / 商品コード / [ 規格 ] / [ オプション ]';
        $this->label_cell[] = '数量';
        $this->label_cell[] = '単価';
        $this->label_cell[] = '金額(税込)';

        $arrOrder = array();
        // DBから受注情報を読み込む
        $this->lfGetOrderData2($this->arrData['order_id']);

        $arrRet = $this->lfGetOrderProductOptions($this->arrData['order_id']);
        $arrRet = SC_Utils_Ex::sfSwapArray($arrRet);
        $this->arrDisp = array_merge($this->arrDisp, $arrRet);

        // 購入者情報
        $text = '〒 ' . $this->arrDisp['order_zip01'] . ' - ' . $this->arrDisp['order_zip02'];

        $this->lfText2(23, 43, $text, 8); //購入者郵便番号

        $y = 47; // 住所1の1行目の高さ

        $text = $this->arrPref[$this->arrDisp['order_pref']] . $this->arrDisp['order_addr01']; //購入者都道府県+住所1

        while (mb_strwidth($text) > 68) {
            $line = mb_strimwidth($text, 0, 68); // 1行分の文字列
            $this->lfText2(27, $y, $line, 8);
            $cut = strlen($line);
            $text = substr($text, $cut, strlen($text) - $cut);
            $y = $y + 3;
        }
        if ($text != "") {
            $this->lfText2(27, $y, $text, 8);
            $y = $y + 3;
        }

        $text = $this->arrDisp['order_addr02']; //購入者住所2

        while (mb_strwidth($text) > 68) {
            $line = mb_strimwidth($text, 0, 68); // 1行分の文字列
            $this->lfText2(27, $y, $line, 8);
            $cut = strlen($line);
            $text = substr($text, $cut, strlen($text) - $cut);
            $y = $y + 3;
        }
        if ($text != "") {
            $this->lfText2(27, $y, $text, 8);
            $y = $y + 3;
        }

        $text = $this->arrDisp['order_name01'] . '　' . $this->arrDisp['order_name02'] . '　様';
        $this->lfText2(27, $y + 2, $text, 11); //購入者氏名
        // お届け先情報
        $this->SetFont('SJIS', '', 10);
        $this->lfText2(25, 125, SC_Utils_Ex::sfDispDBDate($this->arrDisp['create_date']), 10); //ご注文日
        $this->lfText2(25, 135, $this->arrDisp['order_id'], 10); //注文番号

        $this->SetFont('Gothic', 'B', 15);
        $this->Cell(0, 10, $this->tpl_title, 0, 2, 'C', 0, '');  //文書タイトル（納品書・請求書）
        $this->Cell(0, 66, '', 0, 2, 'R', 0, '');
        $this->Cell(5, 0, '', 0, 0, 'R', 0, '');
        $this->SetFont('SJIS', 'B', 15);
        $this->Cell(67, 8, number_format($this->arrDisp['payment_total']) . ' 円', 0, 2, 'R', 0, '');
        $this->Cell(0, 45, '', 0, 2, '', 0, '');

        $this->SetFont('SJIS', '', 8);

        $monetary_unit = '円';
        $point_unit = 'Pt';

        // 購入商品情報
        for ($i = 0; $i < count($this->arrDisp['quantity']); $i++) {

            // 購入数量
            $data[0] = $this->arrDisp['quantity'][$i];

            // 税込金額（単価）
            $data[1] = SC_Helper_DB_Ex::sfCalcIncTax($this->arrDisp['price'][$i], $this->arrDisp['tax_rate'][$i], $this->arrDisp['tax_rule'][$i]);

            // 小計（商品毎）
            $data[2] = $data[0] * $data[1];

            $arrOrder[$i][0] = $this->arrDisp['product_name'][$i] . ' / ';
            $arrOrder[$i][0] .= $this->arrDisp['product_code'][$i] . ' / ';
            if ($this->arrDisp['classcategory_name1'][$i]) {
                $arrOrder[$i][0] .= ' [ ' . $this->arrDisp['classcategory_name1'][$i];
                if ($this->arrDisp['classcategory_name2'][$i] == '') {
                    $arrOrder[$i][0] .= ' ]';
                } else {
                    $arrOrder[$i][0] .= ' * ' . $this->arrDisp['classcategory_name2'][$i] . ' ]';
                }
            }
            if ($this->arrDisp['plg_productoptions'][$i]) {
                $arrOrder[$i][0] .= ' / ';
                foreach ($this->arrDisp['plg_productoptions'][$i] as $option) {
                    $arrOrder[$i][0] .= ' [ ' . $option['option_name'] . ' : ' . $option['optioncategory_name'] . ' ] ';
                }
            }
            $arrOrder[$i][1] = number_format($data[0]);
            $arrOrder[$i][2] = number_format($data[1]) . $monetary_unit;
            $arrOrder[$i][3] = number_format($data[2]) . $monetary_unit;
        }

        $arrOrder[$i][0] = '';
        $arrOrder[$i][1] = '';
        $arrOrder[$i][2] = '';
        $arrOrder[$i][3] = '';

        $i++;
        $arrOrder[$i][0] = '';
        $arrOrder[$i][1] = '';
        $arrOrder[$i][2] = '商品合計';
        $arrOrder[$i][3] = number_format($this->arrDisp['subtotal']) . $monetary_unit;

        $i++;
        $arrOrder[$i][0] = '';
        $arrOrder[$i][1] = '';
        $arrOrder[$i][2] = '送料';
        $arrOrder[$i][3] = number_format($this->arrDisp['deliv_fee']) . $monetary_unit;

        $i++;
        $arrOrder[$i][0] = '';
        $arrOrder[$i][1] = '';
        $arrOrder[$i][2] = '手数料';
        $arrOrder[$i][3] = number_format($this->arrDisp['charge']) . $monetary_unit;

        $i++;
        $arrOrder[$i][0] = '';
        $arrOrder[$i][1] = '';
        $arrOrder[$i][2] = '値引き';
        $arrOrder[$i][3] = '- ' . number_format(($this->arrDisp['use_point'] * POINT_VALUE) + $this->arrDisp['discount']) . $monetary_unit;

        $i++;
        $arrOrder[$i][0] = '';
        $arrOrder[$i][1] = '';
        $arrOrder[$i][2] = '請求金額';
        $arrOrder[$i][3] = number_format($this->arrDisp['payment_total']) . $monetary_unit;

        // ポイント表記
        if ($this->arrData['disp_point'] && $this->arrDisp['customer_id']) {
            $i++;
            $arrOrder[$i][0] = '';
            $arrOrder[$i][1] = '';
            $arrOrder[$i][2] = '';
            $arrOrder[$i][3] = '';

            $i++;
            $arrOrder[$i][0] = '';
            $arrOrder[$i][1] = '';
            $arrOrder[$i][2] = '利用ポイント';
            $arrOrder[$i][3] = number_format($this->arrDisp['use_point']) . $point_unit;

            $i++;
            $arrOrder[$i][0] = '';
            $arrOrder[$i][1] = '';
            $arrOrder[$i][2] = '加算ポイント';
            $arrOrder[$i][3] = number_format($this->arrDisp['add_point']) . $point_unit;
        }

        $this->FancyTable($this->label_cell, $arrOrder, $this->width_cell);
    }

    // PDF_Japanese::Text へのパーサー
    private function lfText2($x, $y, $text, $size = 0, $style = '')
    {
        // 退避
        $bak_font_style = $this->FontStyle;
        $bak_font_size = $this->FontSizePt;

        $this->SetFont('', $style, $size);
        $this->Text($x, $y, $text);

        // 復元
        $this->SetFont('', $bak_font_style, $bak_font_size);
    }

    // 受注データの取得
    private function lfGetOrderData2($order_id)
    {
        if (SC_Utils_Ex::sfIsInt($order_id)) {
            // DBから受注情報を読み込む
            $objPurchase = new SC_Helper_Purchase_Ex();
            $this->arrDisp = $objPurchase->getOrder($order_id);
            list($point) = SC_Helper_Customer_Ex::sfGetCustomerPoint($order_id, $this->arrDisp['use_point'], $this->arrDisp['add_point']);
            $this->arrDisp['point'] = $point;

            // 受注詳細データの取得
            $arrRet = $objPurchase->getOrderDetail($order_id);
            $arrRet = SC_Utils_Ex::sfSwapArray($arrRet);
            $this->arrDisp = array_merge($this->arrDisp, $arrRet);

            // その他支払い情報を表示
            if ($this->arrDisp['memo02'] != '') {
                $this->arrDisp['payment_info'] = unserialize($this->arrDisp['memo02']);
            }
            $this->arrDisp['payment_type'] = 'お支払い';
        }
    }

    private function setShopData2()
    {
        // ショップ情報

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        // ショップ名
        $this->lfText2(125, 60, $arrInfo['shop_name'], 8, 'B');
        // URL
        $this->lfText2(125, 63, $arrInfo['law_url'], 8);
        // 会社名
        $this->lfText2(125, 68, $arrInfo['law_company'], 8);
        // 郵便番号
        $text = '〒 ' . $arrInfo['law_zip01'] . ' - ' . $arrInfo['law_zip02'];
        $this->lfText2(125, 71, $text, 8);
        // 都道府県+所在地
        $text = $this->arrPref[$arrInfo['law_pref']] . $arrInfo['law_addr01'];
        $this->lfText2(125, 74, $text, 8);
        $this->lfText2(125, 77, $arrInfo['law_addr02'], 8);

        $text = 'TEL: ' . $arrInfo['law_tel01'] . '-' . $arrInfo['law_tel02'] . '-' . $arrInfo['law_tel03'];
        //FAX番号が存在する場合、表示する
        if (strlen($arrInfo['law_fax01']) > 0) {
            $text .= '　FAX: ' . $arrInfo['law_fax01'] . '-' . $arrInfo['law_fax02'] . '-' . $arrInfo['law_fax03'];
        }
        $this->lfText2(125, 80, $text, 8);  //TEL・FAX

        if (strlen($arrInfo['law_email']) > 0) {
            $text = 'Email: ' . $arrInfo['law_email'];
            $this->lfText2(125, 83, $text, 8);      //Email
        }

        //ロゴ画像
        $logo_file = PDF_TEMPLATE_REALDIR . 'logo.png';
        $this->Image($logo_file, 124, 46, 40);
    }

    private function setMessageData2()
    {
        // メッセージ
        $this->lfText2(27, 89, $this->arrData['msg1'], 8);  //メッセージ1
        $this->lfText2(27, 92, $this->arrData['msg2'], 8);  //メッセージ2
        $this->lfText2(27, 95, $this->arrData['msg3'], 8);  //メッセージ3
        $text = '作成日: ' . $this->arrData['year'] . '年' . $this->arrData['month'] . '月' . $this->arrData['day'] . '日';
        $this->lfText2(158, 288, $text, 8);  //作成日
    }

    // オプション情報の取得
    private function lfGetOrderProductOptions($order_id)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $col = 'plg_productoptions_flg';
        $where = 'order_id = ?';
        $objQuery->setOrder('order_detail_id');
        $arrRet = $objQuery->select($col, 'dtb_order_detail', $where, array($order_id));

        plg_ProductOptions_Util::lfSetOrderDetailOptions($arrRet);
        return $arrRet;
    }

    /**
     * 備考の出力を行う
     *
     * @param  string $str 入力文字列
     * @return string 変更後の文字列
     */
    private function setEtcData2()
    {
        $this->Cell(0, 10, '', 0, 1, 'C', 0, '');
        $this->SetFont('Gothic', 'B', 9);
        $this->MultiCell(0, 6, '＜ 備考 ＞', 'T', 2, 'L', 0, '');
        $this->SetFont('SJIS', '', 8);
        $text = SC_Utils_Ex::rtrim($this->arrData['etc1'] . "\n" . $this->arrData['etc2'] . "\n" . $this->arrData['etc3']);
        $this->MultiCell(0, 4, $text, '', 2, 'L', 0, '');
    }

}
