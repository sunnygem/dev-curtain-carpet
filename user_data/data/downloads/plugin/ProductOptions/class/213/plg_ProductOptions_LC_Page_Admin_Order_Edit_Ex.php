<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page_Admin_Order_Edit.php";

class plg_ProductOptions_LC_Page_Admin_Order_Edit_Ex extends plg_ProductOptions_LC_Page_Admin_Order_Edit
{

    /**
     * @param LC_Page_Admin_Order_Edit $objPage
     * @return void
     */
    function before($objPage)
    {
        switch ($objPage->getMode()) {
            case 'edit':
            case 'add':
                $objPurchase = new SC_Helper_Purchase_Ex();
                $objFormParam = new SC_FormParam_Ex();

                // パラメーター情報の初期化
                $objPage->lfInitParam($objFormParam);
                plg_ProductOptions_Util::addProductOptionsParam($objFormParam);
                $objFormParam->addParam('商品オプション', 'shipment_plg_productoptions_flg');
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                $order_id = $objFormParam->getValue('order_id');
                $arrValuesBefore = array();

                // DBから受注情報を読み込む
                if (!SC_Utils_Ex::isBlank($order_id)) {
                    $objPage->setOrderToFormParam($objFormParam, $order_id);
                    $objPage->tpl_subno = 'index';
                    $arrValuesBefore['deliv_id'] = $objFormParam->getValue('deliv_id');
                    $arrValuesBefore['payment_id'] = $objFormParam->getValue('payment_id');
                    $arrValuesBefore['payment_method'] = $objFormParam->getValue('payment_method');
                } else {
                    $objPage->tpl_subno = 'add';
                    $objPage->tpl_mode = 'add';
                    $arrValuesBefore['deliv_id'] = NULL;
                    $arrValuesBefore['payment_id'] = NULL;
                    $arrValuesBefore['payment_method'] = NULL;
                    // お届け先情報を空情報で表示
                    $arrShippingIds[] = null;
                    $objFormParam->setValue('shipping_id', $arrShippingIds);

                    // 新規受注登録で入力エラーがあった場合の画面表示用に、会員の現在ポイントを取得
                    if (!SC_Utils_Ex::isBlank($objFormParam->getValue('customer_id'))) {
                        $arrCustomer = SC_Helper_Customer_Ex::sfGetCustomerDataFromId($customer_id);
                        $objFormParam->setValue('customer_point', $arrCustomer['point']);

                        // 新規受注登録で、ポイント利用できるように現在ポイントを設定
                        $objFormParam->setValue('point', $arrCustomer['point']);
                    }
                }

                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                $customer_id = $objFormParam->getValue('customer_id');


                self::setProductsQuantity($objFormParam);
                $objPage->arrErr = $objPage->lfCheckError($objFormParam);
                if ($customer_id > 0) {
                    $arrForm = $objFormParam->getFormParamList();
                    $add_point = 0;
                    foreach ($arrForm['plg_productoptions']['value'] as $key => $item) {
                        $add_point += plg_ProductOptions_Util::getTotalOptionPoint($item);
                    }


                    $totalpoint = 0;
                    $max = count($arrForm['quantity']['value']);
                    for ($i = 0; $i < $max; $i++) {
                        // 加算ポイントの計算
                        $totalpoint += SC_Utils_Ex::sfPrePoint($arrForm['price']['value'][$i], $arrForm['point_rate']['value'][$i]) * $arrForm['quantity']['value'][$i];
                    }

                    $point = SC_Helper_DB_Ex::sfGetAddPoint($totalpoint, $arrForm['use_point']['value']);

                    $objFormParam->setValue('add_point', $point + $add_point);
                }
                if (SC_Utils_Ex::isBlank($objPage->arrErr)) {
                    if ($objPage->getMode() == "edit") {
                        $message = '受注を編集しました。';
                        $order_id = self::doRegister($order_id, $objPurchase, $objFormParam, $message, $arrValuesBefore);
                        if ($order_id >= 0) {
                            $_POST['plg_productoptions_order_id'] = $order_id;
                            $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'plg_productoptions_edit';
                        }
                    } else {
                        $message = '受注を登録しました。';
                        $order_id = self::doRegister(null, $objPurchase, $objFormParam, $message, $arrValuesBefore);
                        if ($order_id >= 0) {
                            $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'plg_productoptions_add';
                            $_POST['plg_productoptions_order_id'] = $order_id;
                        }
                    }

                    $objPage->tpl_onload = "window.alert('" . $message . "');";
                } else {
                    $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'order_id';
                }
                break;

            case 'select_product_detail':
            case 'multiple':
            case 'multiple_set_to':
            case 'delete_product':
            case 'recalculate':
            case 'payment':
            case 'deliv':
            case 'search_customer':
            case 'append_shipping':
                $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = 'plg_productoptions_' . $objPage->getMode();
                break;
        }
    }

    /**
     * @param LC_Page_Admin_Order_Edit $objPage 受注編集ページクラス
     * @return void
     */
    function after($objPage)
    {
        $objPage->arrOptions = plg_ProductOptions_Util::lfGetOption();
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $objFormParam = new SC_FormParam_Ex();

        // パラメーター情報の初期化
        $objPage->lfInitParam($objFormParam);
        plg_ProductOptions_Util::addProductOptionsParam($objFormParam);
        $objFormParam->addParam('商品オプション', 'shipment_plg_productoptions_flg');
        $objFormParam->setParam($_REQUEST);
        $objFormParam->convParam();
        $order_id = $objFormParam->getValue('order_id');

        // DBから受注情報を読み込む
        if (!SC_Utils_Ex::isBlank($order_id)) {
            $objPage->setOrderToFormParam($objFormParam, $order_id);
        } else {
            $arrShippingIds[] = null;
            $objFormParam->setValue('shipping_id', $arrShippingIds);

            // 新規受注登録で入力エラーがあった場合の画面表示用に、会員の現在ポイントを取得
            if (!SC_Utils_Ex::isBlank($objFormParam->getValue('customer_id'))) {
                $arrCustomer = SC_Helper_Customer_Ex::sfGetCustomerDataFromId($customer_id);
                $objFormParam->setValue('customer_point', $arrCustomer['point']);

                // 新規受注登録で、ポイント利用できるように現在ポイントを設定
                $objFormParam->setValue('point', $arrCustomer['point']);
            }
        }

        switch ($objPage->getMode()) {
            case 'plg_productoptions_select_product_detail':
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                self::doRegisterProduct($objFormParam);
                //複数配送時に各商品の総量を設定
                self::setProductsQuantity($objFormParam);
                $objPage->arrErr = $objPage->lfCheckError($objFormParam);
                break;
            case 'plg_productoptions_multiple':
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                self::setProductsQuantity($objFormParam);
                $objPage->arrErr = $objPage->lfCheckError($objFormParam);
                break;
            case 'plg_productoptions_multiple_set_to':
                $objPage->lfInitMultipleParam($objFormParam);
                $objFormParam->addParam('product_index', 'multiple_product_index', INT_LEN, 'n', array('EXIST_CHECK', 'MAX_LENGTH_CHECK', 'NUM_CHECK'), 1);
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                self::setMultipleItemTo($objFormParam);
                break;

            case 'plg_productoptions_delete_product':
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                $delete_no = $objFormParam->getValue('delete_no');
                self::doDeleteProduct($delete_no, $objFormParam);
                self::setProductsQuantity($objFormParam);
                break;
            case 'plg_productoptions_recalculate':
            case 'plg_productoptions_payment':
            case 'plg_productoptions_deliv':
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                //複数配送時に各商品の総量を設定
                self::setProductsQuantity($objFormParam);
                $objPage->arrErr = $objPage->lfCheckError($objFormParam);
                break;
            case 'plg_productoptions_append_shipping':
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                self::setProductsQuantity($objFormParam);
                $objPage->addShipping($objFormParam);
                break;
            case 'plg_productoptions_search_customer':
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                self::setProductsQuantity($objFormParam);
                if (plg_ProductOptions_Util::getECCUBEVer() >= 2132) {
                    $objPage->setCustomerTo($objFormParam->getValue('edit_customer_id'), $objFormParam);
                    $customer_birth = $objFormParam->getValue('order_birth');
                } else {
                    $customer_birth = $objPage->setCustomerTo($objFormParam->getValue('edit_customer_id'), $objFormParam);
                }
                // 加算ポイントの計算
                if (USE_POINT === true && $objPage->tpl_mode == 'add') {
                    $birth_point = 0;
                    if ($customer_birth) {
                        $arrRet = preg_split('|[- :/]|', $customer_birth);
                        $birth_date = intval($arrRet[1]);
                        $now_date = intval(date('m'));
                        // 誕生日月であった場合
                        if ($birth_date == $now_date) {
                            $birth_point = BIRTH_MONTH_POINT;
                        }
                    }
                    $objFormParam->setValue("birth_point", $birth_point);
                }
                $objPage->arrErr = $objPage->lfCheckError($objFormParam);
                break;
            case 'plg_productoptions_add':
                $objPage->tpl_mode = 'edit';
                $objFormParam->setValue('order_id', $_POST['plg_productoptions_order_id']);
            case 'plg_productoptions_edit':
                $objPage->setOrderToFormParam($objFormParam, $_POST['plg_productoptions_order_id']);
                break;
        }

        $objPage->arrForm = $objFormParam->getFormParamList();
        $arrShipmentItemKeys = $objPage->arrShipmentItemKeys;
        $arrShipmentItemKeys[] = 'shipment_plg_productoptions_flg';
        $objPage->arrAllShipping = $objFormParam->getSwapArray(array_merge($objPage->arrShippingKeys, $arrShipmentItemKeys));
        $objPage->tpl_shipping_quantity = count($objPage->arrAllShipping);

        $objPage->top_shipping_id = array_shift((array_keys($objPage->arrAllShipping)));


        $add_point = 0;
        $deliv_free_cnt = 0;
        foreach ($objPage->arrForm['plg_productoptions']['value'] as $key => $item) {
            list($add_price, $option_point, $option_cnt, $arrOption) = plg_ProductOptions_Util::getTotalOptionInfo($item);
            $add_point += $option_point;
            $deliv_free_cnt += $option_cnt;

            if (!empty($arrOption)) {
                unset($objPage->arrForm['plg_productoptions']['value'][$key]);
                $objPage->arrForm['plg_productoptions']['value'][$key] = $arrOption;
            }
        }
        self::lfCheck($objPage->arrForm, $deliv_free_cnt);
        $objPage->arrForm['add_point']['value'] += $add_point;

        if (!SC_Utils_Ex::isBlank($order_id) && ($objPage->getMode() == "pre_edit" || $objPage->getMode() == "order_id")) {
            $objPurchase = new SC_Helper_Purchase_Ex();
            $arrShipmentItemKeys = array(
                'shipment_plg_productoptions_flg'
            );


            $arrShippings = $objPurchase->getShippings($order_id);
            $arrShipmentItem = array();
            foreach ($arrShippings as $shipping_id => $arrShipping) {
                $arrProductQuantity[$shipping_id] = count($arrShipping['shipment_item']);
                foreach ($arrShipping['shipment_item'] as $item_index => $arrItem) {
                    foreach ($arrItem as $item_key => $item_val) {
                        $arrShipmentItem['shipment_' . $item_key][$shipping_id][$item_index] = $item_val;
                    }
                }
            }
            $objFormParam->setParam($arrShipmentItem);

            $arrShippingItemParam = $objFormParam->getSwapArray($arrShipmentItemKeys);
            foreach ($objPage->arrAllShipping as $key => $item) {
                $objPage->arrAllShipping[$key]['shipment_plg_productoptions_flg'] = $arrShippingItemParam[$key]['shipment_plg_productoptions_flg'];
            }
        }

        foreach ($objPage->arrAllShipping as $shipping_id => $shipping) {
            foreach ($shipping['shipment_plg_productoptions_flg'] as $index => $options) {
                $options = @unserialize($options);
                $arrOption = array();
                if (count($options) > 0) {
                    foreach ($options as $option_id => $optioncategory_id) {
                        if (!is_array($optioncategory_id)) {
                            if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                                $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                                $arrOption[$option_id]['optioncategory_name'] = $optioncategory_id;
                            } else {
                                $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                                $arrOption[$option_id]['optioncategory_name'] = plg_ProductOptions_Util::lfGetOptionCatName($optioncategory_id);
                            }
                            $arrOption[$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                        }
                    }
                }
                $objPage->arrAllShipping[$shipping_id]['shipment_plg_productoptions'][$index] = $arrOption;
            }
        }
    }

    /**
     * 受注商品を削除する.
     *
     * @param  integer      $delete_no    削除する受注商品の項番
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function doDeleteProduct($delete_no, &$objFormParam)
    {
        $arrShipmentItemKeys = array(
            'shipment_product_class_id',
            'shipment_product_code',
            'shipment_product_name',
            'shipment_classcategory_name1',
            'shipment_classcategory_name2',
            'shipment_price',
            'shipment_quantity',
            'shipment_plg_productoptions_flg',
        );

        $arrProductKeys = array(
            'product_id',
            'product_class_id',
            'product_type_id',
            'point_rate',
            'product_code',
            'product_name',
            'classcategory_name1',
            'classcategory_name2',
            'quantity',
            'price',
            'tax_rate',
            'tax_rule',
            'plg_productoptions'
        );

        $select_shipping_id = $objFormParam->getValue('select_shipping_id');

        //変更前のproduct_class_idが他の届け先にも存在するか
        $arrPreShipmentProductClassIds = $objFormParam->getValue('shipment_product_class_id');
        $arrPreProductClassIds = $objFormParam->getValue('product_class_id');
        $arrPreProductoptions = $objFormParam->getValue('shipment_plg_productoptions_flg');
        $delete_product_class_id = $arrPreShipmentProductClassIds[$select_shipping_id][$delete_no];
        $delete_productoptions = @unserialize($arrPreProductoptions[$select_shipping_id][$delete_no]);

        //配送先データ削除
        $arrNewShipments = self::deleteShipment($objFormParam, $arrShipmentItemKeys, $select_shipping_id, $delete_no);
        $objFormParam->setParam($arrNewShipments);
        $is_product_delete = true;
        foreach ($arrNewShipments['shipment_product_class_id'] as $shipping_id => $arrShipmentProductClassIds) {
            foreach ($arrShipmentProductClassIds as $relation_index => $shipment_product_class_id) {
                $options = @unserialize($arrNewShipments['shipment_plg_productoptions_flg'][$shipping_id][$relation_index]);
                if ($delete_product_class_id == $shipment_product_class_id) {
                    if (plg_ProductOptions_Util::compareOptions($options, $delete_productoptions)) {
                        $is_product_delete = false;
                        break;
                    } elseif (plg_ProductOptions_Util::getUpdateFlg() != 1) {
                        $is_product_delete = false;
                        break;
                    }
                }
            }
        }

        //商品情報から削除
        if ($is_product_delete) {
            self::checkDeleteProducts($objFormParam, $arrPreProductClassIds, $delete_product_class_id, $arrProductKeys, $delete_productoptions);
        }
    }

    /**
     * 受注商品の追加/更新を行う.
     *
     * 小画面で選択した受注商品をフォームに反映させる.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function doRegisterProduct(&$objFormParam)
    {
        $product_class_id = $objFormParam->getValue('add_product_class_id');
        if (SC_Utils_Ex::isBlank($product_class_id)) {
            $product_class_id = $objFormParam->getValue('edit_product_class_id');
            $changed_no = $objFormParam->getValue('no');
            self::shipmentEditProduct($objFormParam, $product_class_id, $changed_no);
        } else {
            self::shipmentAddProduct($objFormParam, $product_class_id);
        }
    }

    /**
     * 商品を追加
     *
     * @param  SC_FormParam $objFormParam         SC_FormParam インスタンス
     * @param  integer      $add_product_class_id 追加商品規格ID
     * @return void
     */
    public function shipmentAddProduct(&$objFormParam, $add_product_class_id)
    {
        $arrAddOptions = array();
        foreach ($_POST['add_plg_productoptions'] as $option_id => $optioncategory_id) {
            $default_id = plg_ProductOptions_Util::lfGetOptionDefaultCatId($option_id);
            if ($option_id > 0 && strlen($optioncategory_id) > 0 && $default_id != $optioncategory_id) {
                $arrAddOptions[$option_id] = $optioncategory_id;
            }
        }

        //複数配送に商品情報追加
        $select_shipping_id = $objFormParam->getValue('select_shipping_id');

        //届け先に選択済みの商品がある場合
        $arrShipmentProducts = self::getShipmentProducts($objFormParam);

        $exists_flg = false;
        $change_option = false;
        if ($arrShipmentProducts['shipment_product_class_id']) {
            foreach ($arrShipmentProducts['shipment_product_class_id'][$select_shipping_id] as $relation_index => $shipment_product_class_id) {
                $options = $arrShipmentProducts['shipment_plg_productoptions_flg'][$select_shipping_id][$relation_index];
                $options = @unserialize($options);
                if ($shipment_product_class_id == $add_product_class_id) {
                    if (plg_ProductOptions_Util::compareOptions($arrAddOptions, $options)) {
                        $exists_flg = true;
                        $exists_index = $relation_index;
                        break;
                    } elseif (plg_ProductOptions_Util::getUpdateFlg() != 1) {
                        $exists_flg = true;
                        $change_option = true;
                        break;
                    }
                }
            }
        }

        if ($exists_flg && !$change_option) {
            $arrShipmentProducts['shipment_quantity'][$select_shipping_id][$exists_index] ++;
        } else {
            $add_price = plg_ProductOptions_Util::getTotalOptionPrice($arrAddOptions);

            $objProduct = new SC_Product_Ex();
            $arrAddProductInfo = $objProduct->getDetailAndProductsClass($add_product_class_id);

            if (plg_ProductOptions_Util::getUpdateFlg() != 1) {
                foreach ($arrShipmentProducts['shipment_product_class_id'] as $shipping_id => $arrShipmentProductIds) {
                    foreach ($arrShipmentProductIds as $relation_index => $shipment_product_class_id) {
                        if ($shipment_product_class_id == $add_product_class_id) {
                            $exists_index[$shipping_id] = $relation_index;
                        }
                    }
                }
                foreach ($exists_index as $shipping_id => $index) {
                    $arrShipmentProducts['shipment_price'][$shipping_id][$index] = $arrAddProductInfo['price02'] + $add_price;
                    $arrShipmentProducts['shipment_plg_productoptions_flg'][$shipping_id][$index] = serialize($arrAddOptions);
                }
            }

            if (!$change_option) {
                //届け先に選択商品がない場合
                $arrShipmentProducts['shipment_product_class_id'][$select_shipping_id][] = $add_product_class_id;
                $arrShipmentProducts['shipment_product_code'][$select_shipping_id][] = $arrAddProductInfo['product_code'];
                $arrShipmentProducts['shipment_product_name'][$select_shipping_id][] = $arrAddProductInfo['name'];
                $arrShipmentProducts['shipment_classcategory_name1'][$select_shipping_id][] = $arrAddProductInfo['classcategory_name1'];
                $arrShipmentProducts['shipment_classcategory_name2'][$select_shipping_id][] = $arrAddProductInfo['classcategory_name2'];
                $arrShipmentProducts['shipment_price'][$select_shipping_id][] = $arrAddProductInfo['price02'] + $add_price;
                $arrShipmentProducts['shipment_quantity'][$select_shipping_id][] = 1;
                $arrShipmentProducts['shipment_plg_productoptions_flg'][$select_shipping_id][] = serialize($arrAddOptions);
            }

            //受注商品情報に追加
            $arrPreProductClassIds = $objFormParam->getValue('product_class_id');
            $arrProducts = self::checkInsertOrderProducts($objFormParam, $arrPreProductClassIds, $add_product_class_id, $arrAddProductInfo, $arrAddOptions);
            $objFormParam->setParam($arrProducts);
        }
        $objFormParam->setParam($arrShipmentProducts);
    }

    /**
     * 商品を変更
     *
     * @param  SC_FormParam $objFormParam         SC_FormParam インスタンス
     * @param  integer      $add_product_class_id 変更商品規格ID
     * @param  integer      $change_no            変更対象
     * @return void
     */
    public function shipmentEditProduct(&$objFormParam, $edit_product_class_id, $change_no)
    {
        $arrShipmentItemKeys = array(
            'shipment_product_class_id',
            'shipment_product_code',
            'shipment_product_name',
            'shipment_classcategory_name1',
            'shipment_classcategory_name2',
            'shipment_price',
            'shipment_quantity',
            'shipment_plg_productoptions_flg',
        );

        $arrProductKeys = array(
            'product_id',
            'product_class_id',
            'product_type_id',
            'point_rate',
            'product_code',
            'product_name',
            'classcategory_name1',
            'classcategory_name2',
            'quantity',
            'price',
            'tax_rate',
            'tax_rule',
            'plg_productoptions'
        );

        $arrAddOptions = array();
        foreach ($_POST['add_plg_productoptions'] as $option_id => $optioncategory_id) {
            $default_id = plg_ProductOptions_Util::lfGetOptionDefaultCatId($option_id);
            if ($option_id > 0 && strlen($optioncategory_id) > 0 && $default_id != $optioncategory_id) {
                $arrAddOptions[$option_id] = $optioncategory_id;
            }
        }

        $arrPreProductClassIds = $objFormParam->getValue('product_class_id');
        $select_shipping_id = $objFormParam->getValue('select_shipping_id');

        $arrShipmentProducts = self::getShipmentProducts($objFormParam);

        $exists_flg = false;
        $change_option = false;
        if ($arrShipmentProducts['shipment_product_class_id']) {
            foreach ($arrShipmentProducts['shipment_product_class_id'][$select_shipping_id] as $relation_index => $shipment_product_class_id) {
                $options = $arrShipmentProducts['shipment_plg_productoptions_flg'][$select_shipping_id][$relation_index];
                $options = @unserialize($options);
                if ($shipment_product_class_id == $edit_product_class_id) {
                    if (plg_ProductOptions_Util::compareOptions($arrAddOptions, $options)) {
                        $exists_flg = true;
                        $exists_index = $relation_index;
                        break;
                    } elseif (plg_ProductOptions_Util::getUpdateFlg() != 1) {
                        $change_option = true;
                        $exists_index = $relation_index;
                        break;
                    }
                }
            }
        }

        $pre_shipment_product_class_id = $arrShipmentProducts['shipment_product_class_id'][$select_shipping_id][$change_no];
        $pre_shipment_productoptions = @unserialize($arrShipmentProducts['shipment_plg_productoptions_flg'][$select_shipping_id][$change_no]);
        $same_flg = false;
        if ($pre_shipment_product_class_id == $edit_product_class_id) {
            if (plg_ProductOptions_Util::compareOptions($pre_shipment_productoptions, $arrAddOptions)) {
                $same_flg = true;
            }
        }

        //既にあるデータは１つだけ数量を１増やす
        if ($same_flg) {
//           	$arrShipmentProducts['shipment_quantity'][$select_shipping_id][$change_no] ++;
        } elseif ($exists_flg) {


            //配送先データ削除
            $arrShipmentProducts = self::deleteShipment($objFormParam, $arrShipmentItemKeys, $select_shipping_id, $change_no);
            $arrShipmentProducts['shipment_quantity'][$select_shipping_id][$exists_index] ++;
        } else {
            $objProduct = new SC_Product_Ex();
            $arrAddProductInfo = $objProduct->getDetailAndProductsClass($edit_product_class_id);

            //上書き
            if ($change_option && $change_no != $exists_index) {
                $arrShipmentProducts = self::deleteShipment($objFormParam, $arrShipmentItemKeys, $select_shipping_id, $change_no);
                $change_no = $exists_index;
            } else {
                self::changeShipmentProducts($arrShipmentProducts, $arrAddProductInfo, $select_shipping_id, $change_no, $arrAddOptions);
            }
            if ($change_option) {
                foreach ($arrShipmentProducts['shipment_product_class_id'] as $shipping_id => $arrShipmentProductIds) {
                    foreach ($arrShipmentProductIds as $relation_index => $shipment_product_class_id) {
                        if ($shipment_product_class_id == $edit_product_class_id) {
                            self::changeShipmentProducts($arrShipmentProducts, $arrAddProductInfo, $shipping_id, $change_no, $arrAddOptions);
                        }
                    }
                }
            }

            //受注商品情報も上書き
            $arrTax = SC_Helper_TaxRule_Ex::getTaxRule(0, $edit_product_class_id);

            // 実際はedit
            $add_price = plg_ProductOptions_Util::getTotalOptionPrice($arrAddOptions);

            $arrAddProductInfo['product_name'] = ($arrAddProductInfo['product_name']) ? $arrAddProductInfo['product_name'] : $arrAddProductInfo['name'];

            $arrAddProductInfo['price'] = ($arrAddProductInfo['price']) ? $arrAddProductInfo['price'] : $arrAddProductInfo['price02'] + $add_price;

            $arrAddProductInfo['quantity'] = 1;
            $arrAddProductInfo['tax_rate'] = ($objFormParam->getValue('order_tax_rate') == '') ? $arrTax['tax_rate'] : $objFormParam->getValue('order_tax_rate');

            $arrAddProductInfo['tax_rule'] = ($objFormParam->getValue('order_tax_rule') == '') ? $arrTax['tax_rule'] : $objFormParam->getValue('order_tax_rule');

            $arrAddProductInfo['plg_productoptions'] = $arrAddOptions;

            $arrProductClassIds = $objFormParam->getValue('product_class_id');

            foreach ($arrProductClassIds as $key => $product_class_id) {
                if ($product_class_id == $pre_shipment_product_class_id) {
                    foreach ($arrProductKeys as $insert_key) {
                        $value = $objFormParam->getValue($insert_key);
                        $arrAddProducts[$insert_key] = (is_array($value)) ? $value : array();
                        $arrAddProducts[$insert_key][$key] = $arrAddProductInfo[$insert_key];
                    }
                }
            }
            $objFormParam->setParam($arrAddProducts);
        }
        $objFormParam->setParam($arrShipmentProducts);


        foreach ($arrNewShipmentProducts['shipment_product_class_id'] as $shipping_id => $arrShipmentProductClassIds) {
            foreach ($arrShipmentProductClassIds as $index => $product_class_id) {
                $options = @unserialize($arrNewShipmentProducts['shipment_plg_productoptions_flg'][$shipping_id][$index]);
                if ($pre_shipment_product_class_id == $product_class_id) {
                    if (plg_ProductOptions_Util::compareOptions($options, $pre_shipment_productoptions)) {
                        $is_product_delete = false;
                        break;
                    } elseif (plg_ProductOptions_Util::getUpdateFlg() != 1) {
                        $is_product_delete = false;
                        break;
                    }
                }
            }
        }

        //商品情報から削除
        if ($is_product_delete) {
            self::checkDeleteProducts($objFormParam, $arrPreProductClassIds, $pre_shipment_product_class_id, $arrProductKeys, $pre_shipment_productoptions);
        }
    }

    /**
     * 複数配送のパラメータを取り出す
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array        配送データ
     */
    public function getShipmentProducts(&$objFormParam)
    {
        $arrShipmentProducts['shipment_product_class_id'] = $objFormParam->getValue('shipment_product_class_id');
        $arrShipmentProducts['shipment_product_code'] = $objFormParam->getValue('shipment_product_code');
        $arrShipmentProducts['shipment_product_name'] = $objFormParam->getValue('shipment_product_name');
        $arrShipmentProducts['shipment_classcategory_name1'] = $objFormParam->getValue('shipment_classcategory_name1');
        $arrShipmentProducts['shipment_classcategory_name2'] = $objFormParam->getValue('shipment_classcategory_name2');
        $arrShipmentProducts['shipment_price'] = $objFormParam->getValue('shipment_price');
        $arrShipmentProducts['shipment_quantity'] = $objFormParam->getValue('shipment_quantity');
        $arrShipmentProducts['shipment_plg_productoptions_flg'] = $objFormParam->getValue('shipment_plg_productoptions_flg');

        foreach ($arrShipmentProducts as $key => $value) {
            if (!is_array($value)) {
                $arrShipmentProducts[$key] = array();
            }
        }

        return $arrShipmentProducts;
    }

    /**
     * 変更対象のデータを上書きする
     *
     * @param  array   $arrShipmentProducts 変更対象配列
     * @param  array   $arrProductInfo      上書きデータ
     * @param  integer $shipping_id         配送先ID
     * @param  array   $no                  変更対象
     * @return void
     */
    public function changeShipmentProducts(&$arrShipmentProducts, $arrProductInfo, $shipping_id, $no, $arrAddOptions)
    {
        $add_price = plg_ProductOptions_Util::getTotalOptionPrice($arrAddOptions);

        $arrShipmentProducts['shipment_product_class_id'][$shipping_id][$no] = $arrProductInfo['product_class_id'];
        $arrShipmentProducts['shipment_product_code'][$shipping_id][$no] = $arrProductInfo['product_code'];
        $arrShipmentProducts['shipment_product_name'][$shipping_id][$no] = $arrProductInfo['name'];
        $arrShipmentProducts['shipment_classcategory_name1'][$shipping_id][$no] = $arrProductInfo['classcategory_name1'];
        $arrShipmentProducts['shipment_classcategory_name2'][$shipping_id][$no] = $arrProductInfo['classcategory_name2'];
        $arrShipmentProducts['shipment_price'][$shipping_id][$no] = $arrProductInfo['price02'] + $add_price;
        $arrShipmentProducts['shipment_quantity'][$shipping_id][$no] = 1;
        $arrShipmentProducts['shipment_plg_productoptions_flg'][$shipping_id][$no] = serialize($arrAddOptions);
    }

    /**
     * 削除対象の確認、削除をする
     *
     * @param  SC_FormParam $objFormParam                               SC_FormParam インスタンス
     * @param  array        $arrProductClassIds　                      削除対象配列の商品規格ID
     * @param  integer      $delete_product_class_id　削除商品規? ?ID
     * @param  array        $arrDeleteKeys                              削除項目
     * @return void
     */
    public function checkDeleteProducts(&$objFormParam, $arrProductClassIds, $delete_product_class_id, $arrDeleteKeys, $delete_productoptions)
    {
        $arrProductoptions = $objFormParam->getValue('plg_productoptions');
        foreach ($arrProductClassIds as $relation_index => $product_class_id) {
            //product_class_idの重複はないので、１つ削除したら完了
            if ($product_class_id == $delete_product_class_id) {
                if (plg_ProductOptions_Util::compareOptions($delete_productoptions, $arrProductoptions[$relation_index]) || plg_ProductOptions_Util::getUpdateFlg() != 1) {
                    $arrUpdateParams = array();
                    foreach ($arrDeleteKeys as $delete_key) {
                        $arrProducts = $objFormParam->getValue($delete_key);
                        foreach ($arrProducts as $index => $product_info) {
                            if ($index != $relation_index) {
                                $arrUpdateParams[$delete_key][] = $product_info;
                            }
                        }
                    }
                    $objFormParam->setParam($arrUpdateParams);
                    break;
                }
            }
        }
    }

    /**
     * 配送先商品の削除の削除
     *
     * @param  SC_FormParam $objFormParam          SC_FormParam インスタンス
     * @param  array        $arrShipmentDeleteKeys 削除項目
     * @param  integer      $delete_shipping_id　 削除配送ID
     * @param  array        $delete_no             削除対象
     * @return void
     */
    public function deleteShipment(&$objFormParam, $arrShipmentDeletKeys, $delete_shipping_id, $delete_no)
    {
        $arrUpdateParams = array();
        foreach ($arrShipmentDeletKeys as $delete_key) {
            $arrShipments = $objFormParam->getValue($delete_key);
            foreach ($arrShipments as $shipp_id => $arrKeyData) {
                if (empty($arrKeyData))
                    continue;
                foreach ($arrKeyData as $relation_index => $shipment_info) {
                    if ($relation_index != $delete_no || $shipp_id != $delete_shipping_id) {
                        $arrUpdateParams[$delete_key][$shipp_id][] = $shipment_info;
                    }
                }
            }
        }
        //$objFormParam->setParam($arrUpdateParams);
        return $arrUpdateParams;
    }

    /**
     * 受注商品一覧側に商品を追加
     *
     * @param  SC_FormParam $objFormParam                    SC_FormParam インスタンス
     * @param  array        $arrProductClassIds　           対象配列の商品規格ID
     * @param  integer      $indert_product_class_id　追?? 商品規格ID
     * @param  array        $arrAddProductInfo               追加データ
     * @return array        $arrAddProducts           更新データ
     */
    public function checkInsertOrderProducts(&$objFormParam, $arrProductClassIds, $insert_product_class_id, $arrAddProductInfo, $arrAddOptions = array())
    {
        $arrProductKeys = array(
            'product_id',
            'product_class_id',
            'product_type_id',
            'point_rate',
            'product_code',
            'product_name',
            'classcategory_name1',
            'classcategory_name2',
            'quantity',
            'price',
            'tax_rate',
            'tax_rule',
            'plg_productoptions'
        );

        $arrPreOptions = $objFormParam->getValue('plg_productoptions');
        $exists_flg = false;
        $change_option = false;
        if ($arrProductClassIds) {
            foreach ($arrProductClassIds as $index => $produc_class_id) {
                $options = $arrPreOptions[$index];
                if ($insert_product_class_id == $produc_class_id) {
                    if (plg_ProductOptions_Util::compareOptions($options, $arrAddOptions)) {
                        $exists_flg = true;
                        break;
                    } elseif (plg_ProductOptions_Util::getUpdateFlg() != 1) {
                        $exists_flg = true;
                        $exists_index = $index;
                        $change_option = true;
                    }
                }
            }
        }

        // 「変更」の場合、お届け先情報の位置から受注情報の位置を求める
        $product_class_id = $objFormParam->getValue('add_product_class_id');
        if (SC_Utils_Ex::isBlank($product_class_id)) {
            $change_no = $objFormParam->getValue('no');
            $pre_product_class_id = $arrProductClassIds[$change_no];
            $arrWorkProductClassIds = $objFormParam->getValue('product_class_id');
            $pre_change_no = 0;
            foreach ($arrWorkProductClassIds as $productClassId) {
                if ($productClassId == $pre_product_class_id) {
                    break;
                }
                $pre_change_no++;
            }
        }

        $arrTaxRule = SC_Helper_TaxRule_Ex::getTaxRule(0, $insert_product_class_id);

        $add_price = plg_ProductOptions_Util::getTotalOptionPrice($arrAddOptions);

        if (!$exists_flg) {
            $arrAddProducts = array();

            $arrAddProductInfo['product_name'] = ($arrAddProductInfo['product_name']) ?
                    $arrAddProductInfo['product_name'] : $arrAddProductInfo['name'];
            $arrAddProductInfo['price'] = (($arrAddProductInfo['price']) ?
                            $arrAddProductInfo['price'] : $arrAddProductInfo['price02']) + $add_price;
            $arrAddProductInfo['quantity'] = 1;
            $arrAddProductInfo['tax_rate'] = ($objFormParam->getValue('order_tax_rate') == '') ?
                    $arrTaxRule['tax_rate'] : $objFormParam->getValue('order_tax_rate');
            $arrAddProductInfo['tax_rule'] = ($objFormParam->getValue('order_tax_rule') == '') ?
                    $arrTaxRule['tax_rule'] : $objFormParam->getValue('order_tax_rule');
            $arrAddProductInfo['plg_productoptions'] = $arrAddOptions;

            foreach ($arrProductKeys as $insert_key) {
                $value = $objFormParam->getValue($insert_key);
                $arrAddProducts[$insert_key] = (is_array($value)) ? $value : array();

                if (SC_Utils_Ex::isBlank($product_class_id)) {
                    // 「変更」の場合
                    $arrWorkProducts = array();
                    $insert_change_no = 0;
                    foreach ($arrAddProducts[$insert_key] as $data) {
                        if ($insert_change_no == $pre_change_no) {
                            $arrWorkProducts[] = $arrAddProductInfo[$insert_key];
                        } else {
                            $arrWorkProducts[] = $data;
                        }
                        $insert_change_no++;
                    }
                    $arrAddProducts[$insert_key] = $arrWorkProducts;
                } else {
                    // 「商品の追加」の場合
                    $arrAddProducts[$insert_key][] = $arrAddProductInfo[$insert_key];
                }
            }

            return $arrAddProducts;
        } elseif ($change_option) {
            $arrProducts = array();
            foreach ($arrProductKeys as $insert_key) {
                $value = $objFormParam->getValue($insert_key);
                $arrProducts[$insert_key] = (is_array($value)) ? $value : array();
            }
            $arrProducts['plg_productoptions'][$exists_index] = $arrAddOptions;
            $arrProducts['price'][$exists_index] = (($arrAddProductInfo['price']) ?
                            $arrAddProductInfo['price'] : $arrAddProductInfo['price02']) + $add_price;

            return $arrProducts;
        } else {
            //受注商品の数量は、複数配送側の集計で出しているので、重複しても数量を増やさない。
            return null;
        }
    }

    /**
     * 商品側の総量計算&セット
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function setProductsQuantity(&$objFormParam)
    {
        $arrShipmentsItems = $objFormParam->getSwapArray(array('shipment_product_class_id', 'shipment_quantity', 'shipment_plg_productoptions_flg'));
        // 配送先が存在する時のみ、商品個数の再設定を行います
        if (!SC_Utils_Ex::isBlank($arrShipmentsItems)) {
            $arrUpdateQuantity = array();
            foreach ($arrShipmentsItems as $arritems) {
                foreach ($arritems['shipment_product_class_id'] as $relation_index => $shipment_product_class_id) {
                    if (plg_ProductOptions_Util::getUpdateFlg() == 1) {
                        $option_flg = $arritems['shipment_plg_productoptions_flg'][$relation_index];
                        if (empty($option_flg) || !is_array(@unserialize($option_flg)))
                            $option_flg = serialize(array());
                        $arrUpdateQuantity[$shipment_product_class_id][$option_flg] += $arritems['shipment_quantity'][$relation_index];
                    }else {
                        $arrUpdateQuantity[$shipment_product_class_id] += $arritems['shipment_quantity'][$relation_index];
                    }
                }
            }

            $arrProductsClass = $objFormParam->getValue('product_class_id');
            $arrOptions = $objFormParam->getValue('plg_productoptions');
            $arrQuantity = array();
            foreach ($arrProductsClass as $relation_key => $product_class_id) {
                if (plg_ProductOptions_Util::getUpdateFlg() == 1) {
                    $options = $arrOptions[$relation_key];
                    if (empty($options)) {
                        $option_flg = array();
                    } else {
                        $option_flg = $options;
                    }
                    $option_flg = serialize($option_flg);
                    $arrQuantity[$relation_key] = isset($arrUpdateQuantity[$product_class_id][$option_flg]) ? $arrUpdateQuantity[$product_class_id][$option_flg] : 0;
                } else {
                    $arrQuantity[$relation_key] = isset($arrUpdateQuantity[$product_class_id]) ? $arrUpdateQuantity[$product_class_id] : 0;
                }
            }
            $objFormParam->setParam(array('quantity' => $arrQuantity));
        }
    }

}
