<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Admin_Order_ProductSelect extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Admin_Order_ProductSelect $objPage 商品選択ページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Admin_Order_ProductSelect $objPage 商品選択ページクラス
     * @return void
     */
    function after($objPage)
    {
        parent::after($objPage);
    }

}
