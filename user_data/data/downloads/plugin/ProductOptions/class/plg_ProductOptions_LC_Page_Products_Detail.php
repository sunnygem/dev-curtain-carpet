<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page_Products_List.php";

class plg_ProductOptions_LC_Page_Products_Detail extends plg_ProductOptions_LC_Page_Products_List
{

    /**
     * @param LC_Page_Products_Detail $objPage 商品詳細ページクラス
     * @return void
     */
    function before($objPage)
    {
        // モバイル用 ポストバック処理
        if (SC_Display_Ex::detectDevice() == DEVICE_TYPE_MOBILE) {
            $product_id = $_REQUEST['product_id'];

            if ($objPage->getMode() == "selectItem") {
                $option_id = $_REQUEST['option_id'];
                if (is_null($option_id))
                    $option_id = 0;
                $productoptions = $_REQUEST['plg_productoptions'];

                $objPage->plg_productoptions_Err = self::lfCheckErrorOptions($product_id, $productoptions, $option_id);

                if (count($objPage->plg_productoptions_Err) > 0) {
                    $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = "error";
                }
            }
        }

        if ($objPage->getMode() == "cart") {
            // 会員クラス
            $objCustomer = new SC_Customer_Ex();

            // パラメーター管理クラス
            $objFormParam = new SC_FormParam_Ex();
            // パラメーター情報の初期化
            $objPage->lfInitParam($objFormParam);

            if (plg_ProductOptions_Util::getECCUBEVer() >= 2133) {
                $objProduct = new SC_Product_Ex();

                // プロダクトIDの正当性チェック
                $product_id = $objPage->lfCheckProductId($objFormParam->getValue('admin'), $objFormParam->getValue('product_id'), $objProduct);
            } else {
                // プロダクトIDの正当性チェック
                $product_id = $objPage->lfCheckProductId($objFormParam->getValue('admin'), $objFormParam->getValue('product_id'));

                $objProduct = new SC_Product_Ex();
            }
            $objProduct->setProductsClassByProductIds(array($product_id));

            // 規格1が設定されている
            $tpl_classcat_find1 = $objProduct->classCat1_find[$product_id];
            // 規格2が設定されている
            $tpl_classcat_find2 = $objProduct->classCat2_find[$product_id];

            $productoptions = $_REQUEST['plg_productoptions'];

            if (is_array($productoptions)) {
                foreach ($productoptions as $option_id => $optioncategory_id) {
                    $default_id = plg_ProductOptions_Util::lfGetOptionDefaultCatId($option_id);
                    if (is_null($optioncategory_id) || $optioncategory_id == "" || $default_id == $optioncategory_id)
                        unset($productoptions[$option_id]);
                }
            }

            $objPage->plg_productoptions_Err = self::lfCheckErrorOptions($product_id, $productoptions);

            $objPage->arrErr = $objPage->lfCheckError($mode, $objFormParam, $tpl_classcat_find1, $tpl_classcat_find2);
            if (count($objPage->arrErr) == 0 && count($objPage->plg_productoptions_Err) == 0) {
                $objCartSess = new SC_CartSession_Ex();
                $product_class_id = $objFormParam->getValue('product_class_id');
                $quantity = $objFormParam->getValue('quantity');

                $objCartSess->addProduct($product_class_id, $quantity, $productoptions);

                // 開いているカテゴリーツリーを維持するためのパラメーター
                $arrQueryString = array(
                    'product_id' => $objFormParam->getValue('product_id'),
                );

                self::sendRedirect(CART_URLPATH, $arrQueryString);
                exit;
            } else {
                $_REQUEST['mode'] = $_POST['mode'] = $_GET['mode'] = "select";
            }
        }
    }

    /**
     * @param LC_Page_Products_Detail $objPage 商品詳細ページクラス
     * @return void
     */
    function after($objPage)
    {
        list($arrRet, $values) = plg_ProductOptions_Util::lfSetProductOptions($objPage->tpl_product_id);

        $objPage->tpl_productoptions_js = 'values=' . SC_Utils_Ex::jsonEncode($values) . ';';
        if (!$objPage->tpl_classcat_find1) {
            $objPage->tpl_productoptions_js .= "price01_base=" . SC_Utils_Ex::jsonEncode($objPage->arrProduct['price01_min']) . ';';
            $objPage->tpl_productoptions_js .= "price02_base=" . SC_Utils_Ex::jsonEncode($objPage->arrProduct['price02_min']) . ';';
            $objPage->tpl_productoptions_js .= "point_base=" . SC_Utils_Ex::jsonEncode(SC_Utils_Ex::sfPrePoint($objPage->arrProduct['price02_min'], $objPage->arrProduct['point_rate'])) . ';';
        }

        $objPage->arrProductOptions = $arrRet;
        if (count($objPage->arrForm['plg_productoptions']['value']) > 0) {
            foreach ($objPage->arrForm['plg_productoptions']['value'] as $option_id => $value) {
                $value = str_replace('alert', '', $value);
                $value = str_replace('document.cookie', '', $value);
                $objPage->arrForm['plg_productoptions']['value'][$option_id] = $value;
            }
        }
        // var_dump($objPage->arrProductOptions);

        // モバイル用 ポストバック処理
        if (SC_Display_Ex::detectDevice() == DEVICE_TYPE_MOBILE) {
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2133) {
                $objProduct = new SC_Product_Ex();

                // プロダクトIDの正当性チェック
                $product_id = $objPage->lfCheckProductId($objPage->objFormParam->getValue('admin'), $objPage->objFormParam->getValue('product_id'), $objProduct);
            } else {
                // プロダクトIDの正当性チェック
                $product_id = $objPage->lfCheckProductId($objPage->objFormParam->getValue('admin'), $objPage->objFormParam->getValue('product_id'));

                $objProduct = new SC_Product_Ex();
            }
            $objProduct->setProductsClassByProductIds(array($product_id));

            $value1 = $objPage->objFormParam->getValue('classcategory_id1');
            // 規格2が設定されている場合.
            if (SC_Utils_Ex::isBlank($objPage->objFormParam->getValue('classcategory_id2')) == false) {
                $value2 = '#' . $objPage->objFormParam->getValue('classcategory_id2');
            } else {
                $value2 = '#0';
            }

            if (strlen($value1) === 0) {
                $value1 = $value2 = '__unselected';
            }

            $objPage->tpl_product_class_id = $objProduct->classCategories[$product_id][$value1][$value2]['product_class_id'];

            switch ($objPage->mode) {
                case 'select':
                    if ($objPage->tpl_classcat_find1) {
                        break;
                    }
                case 'select2':
                    if ($objPage->tpl_classcat_find2) {
                        break;
                    }
                case 'selectItem':
                    if ($objPage->arrProductOptions) {
                        $objPage->arrHiddenOptions = $objPage->objFormParam->getValue('plg_productoptions');
                        $current_option_id = $objPage->objFormParam->getValue('option_id');
                        reset($objPage->arrProductOptions);
                        $tpl_option_id = key($objPage->arrProductOptions);
                        $end_flg = true;
                        for ($i = 0; $i < count($objPage->arrProductOptions) - 1; $i++) {
                            $option_id = key($objPage->arrProductOptions);
                            if ($current_option_id == $option_id) {
                                next($objPage->arrProductOptions);
                                $tpl_option_id = key($objPage->arrProductOptions);
                                $end_flg = false;
                                break;
                            }
                            next($objPage->arrProductOptions);
                        }
                        if (is_null($current_option_id) || $current_option_id == "" || !$end_flg) {
                            $objPage->tpl_option_id = $tpl_option_id;
                            $objPage->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/mobile/products/select_option.tpl";
                        }
                    }
                    break;
                case 'cart':
                    $objPage->arrHiddenOptions = $objPage->objFormParam->getValue('plg_productoptions');
                    break;
                case 'error':
                    $objPage->arrHiddenOptions = $objPage->objFormParam->getValue('plg_productoptions');
                    $objPage->tpl_option_id = $objPage->objFormParam->getValue('option_id');
                    $objPage->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/mobile/products/select_option.tpl";
                    break;
                default:
                    break;
            }
        }
    }

}
