<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * 商品オプションプラグイン
 *
 * @package ProductOptions
 * @author Bratech CO.,LTD.
 * @version $Id: $
 */
class LC_Page_Plugin_ProductOptions_Config extends LC_Page_Admin_Ex
{

    var $arrForm = array();

    /**
     * 初期化する.
     *
     * @return void
     */
    function init()
    {
        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/admin/config.tpl";
        $this->tpl_subtitle = "商品オプションプラグイン設定";
    }

    /**
     * プロセス.
     *
     * @return void
     */
    function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action()
    {
        $this->arrSelect = array('0' => '上書きモード', '1' => '区別モード');
        $this->arrSelect2 = array('0' => 'ON', '1' => 'OFF');
        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();

        $arrForm = array();

        switch ($this->getMode()) {
            case 'edit':
                $arrForm = $objFormParam->getHashArray();
                $this->arrErr = $objFormParam->checkError();
                // エラーなしの場合にはデータを更新
                if (count($this->arrErr) == 0) {
                    // データ更新
                    $this->updateData($arrForm);
                    if (count($this->arrErr) == 0) {
                        $this->tpl_onload = "alert('登録が完了しました。');";
                        $this->tpl_onload .= 'window.close();';
                    }
                }
                break;
            default:
                break;
        }
        if (empty($arrForm)) {
            $objQuery = & SC_Query_Ex::getSingletonInstance();
            $ret = $objQuery->getRow("free_field1,free_field2", "dtb_plugin", "plugin_code = ?", array('ProductOptions'));
            $arrForm['update_flg'] = $ret['free_field1'];
            $arrForm['list_cartin_flg'] = $ret['free_field2'];
        }
        $this->arrForm = $arrForm;
        $this->setTemplate($this->tpl_mainpage);
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy()
    {
        if (method_exists('LC_Page_Admin_Ex', 'destroy')) {
            parent::destroy();
        }
    }

    /**
     * パラメーター情報の初期化
     *
     * @param object $objFormParam SC_FormParamインスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam)
    {
        $objFormParam->addParam('カートでの上書き処理設定', 'update_flg', INT_LEN, 'n', array('EXIST_CHECK', 'NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('一覧ページカートイン機能設定', 'list_cartin_flg', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
    }

    function updateData($arrData)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();

        if ($arrData['update_flg'] == 1) {
            plg_ProductOptions_Util::dropPkey();
        }

        $objQuery->update("dtb_plugin", array("free_field1" => $arrData['update_flg'], "free_field2" => $arrData['list_cartin_flg']), "plugin_code = ?", array('ProductOptions'));
    }

}

?>
