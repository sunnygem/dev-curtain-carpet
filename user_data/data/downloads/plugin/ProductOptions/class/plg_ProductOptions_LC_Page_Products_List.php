<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Products_List extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Products_List $objPage 商品一覧ページクラス
     * @return void
     */
    function before($objPage)
    {
        
    }

    /**
     * @param LC_Page_Products_List $objPage 商品一覧ページクラス
     * @return void
     */
    function after($objPage)
    {
        $price01_base = array();
        $price02_base = array();
        $point_base = array();
        foreach ($objPage->arrProducts as $key => $arrProduct) {
            list($arrRet, $values) = plg_ProductOptions_Util::lfSetProductOptions($arrProduct['product_id']);
            $objPage->arrProducts[$key]['productoptions'] = $arrRet;
            $arrValues[$arrProduct['product_id']] = $values;
            if (!$objPage->tpl_classcat_find1[$arrProduct['product_id']]) {
                $price01_base[$arrProduct['product_id']] = $arrProduct['price01_min'];
                $price02_base[$arrProduct['product_id']] = $arrProduct['price02_min'];
                $point_base[$arrProduct['product_id']] = SC_Utils_Ex::sfPrePoint($arrProduct['price02_min'], $arrProduct['point_rate']);
            }
        }
        $objPage->tpl_productoptions_js = 'values=' . SC_Utils_Ex::jsonEncode($arrValues) . ';';
        $objPage->tpl_productoptions_js .= "price01_base=" . SC_Utils_Ex::jsonEncode($price01_base) . ';';
        $objPage->tpl_productoptions_js .= "price02_base=" . SC_Utils_Ex::jsonEncode($price02_base) . ';';
        $objPage->tpl_productoptions_js .= "point_base=" . SC_Utils_Ex::jsonEncode($point_base) . ';';

        if (count($objPage->plg_productoptions_Form) > 0) {
            foreach ($objPage->plg_productoptions_Form as $product_id => $options) {
                foreach ($options as $option_id => $value) {
                    $value = str_replace('alert', '', $value);
                    $value = str_replace('document.cookie', '', $value);
                    $objPage->plg_productoptions_Form[$product_id][$option_id] = $value;
                }
            }
        }
    }

    /**
     * パラメーターの読み込み
     *
     * @return void
     */
    function lfGetDisplayNum($display_number)
    {
        // 表示件数
        $masterData = new SC_DB_MasterData_Ex();
        $arrPRODUCTLISTMAX = $masterData->getMasterData('mtb_product_list_max');
        return (SC_Utils_Ex::sfIsInt($display_number)) ? $display_number : current(array_keys($arrPRODUCTLISTMAX));
    }

    function lfCheckErrorOptions($product_id, $productoptions, $target_option_id = NULL)
    {
        list($arrOptions) = plg_ProductOptions_Util::lfSetProductOptions($product_id);
        $arrErr = array();
        if (count($arrOptions) > 0) {
            foreach ($arrOptions as $option_id => $option) {
                if ($option['is_required'] != 1)
                    continue;
                if ((!is_null($target_option_id) && $option_id != $target_option_id) || $target_option_id === 0)
                    continue;
                switch ($option['type']) {
                    case 1:
                    case 2:
                        if ($productoptions[$option_id] == $option['default_id'] || !isset($productoptions[$option_id])) {
                            $arrErr[$option_id] = "※" . $option['name'] . "を選択してください。";
                        }
                        break;
                    case 3:
                    case 4:
                        if (strlen($productoptions[$option_id]) == 0) {
                            $arrErr[$option_id] = "※" . $option['name'] . "を入力してください。";
                        }
                        break;
                }
            }
        }
        return $arrErr;
    }

}
