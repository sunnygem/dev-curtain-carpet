<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_LC_Page.php";

class plg_ProductOptions_LC_Page_Admin_Order_Disp extends plg_ProductOptions_LC_Page
{

    /**
     * @param LC_Page_Admin_Order_Disp $objPage 受注詳細ページクラス
     * @return void
     */
    function before($objPage)
    {
        parent::before($objPage);
    }

    /**
     * @param LC_Page_Admin_Order_Disp $objPage 受注詳細ページクラス
     * @return void
     */
    function after($objPage)
    {
        $objFormParam = new SC_FormParam_Ex();

        // パラメータ情報の初期化
        $objPage->lfInitParam($objFormParam);
        $objFormParam->addParam('商品オプション', 'shipment_plg_productoptions_flg');
        $objFormParam->setParam($_REQUEST);
        $objFormParam->convParam();
        $order_id = $objFormParam->getValue('order_id');

        // DBから受注情報を読み込む
        $objPage->setOrderToFormParam($objFormParam, $order_id);

        $arrKeys = array_merge($objPage->arrShippingKeys, $objPage->arrShipmentItemKeys);
        $arrKeys[] = 'shipment_plg_productoptions_flg';
        $objPage->arrAllShipping = $objFormParam->getSwapArray($arrKeys);

        foreach ($objPage->arrAllShipping as $shipping_id => $shipping) {
            foreach ($shipping['shipment_plg_productoptions_flg'] as $index => $options) {
                $options = @unserialize($options);
                $arrOption = array();
                if (count($options) > 0) {
                    foreach ($options as $option_id => $optioncategory_id) {
                        if (!is_array($optioncategory_id)) {
                            if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                                $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                                $arrOption[$option_id]['optioncategory_name'] = $optioncategory_id;
                            } else {
                                $arrOption[$option_id]['optioncategory_id'] = $optioncategory_id;
                                $arrOption[$option_id]['optioncategory_name'] = plg_ProductOptions_Util::lfGetOptionCatName($optioncategory_id);
                            }
                            $arrOption[$option_id]['option_name'] = plg_ProductOptions_Util::lfGetOptionName($option_id);
                        }
                    }
                }
                $objPage->arrAllShipping[$shipping_id]['shipment_plg_productoptions'][$index] = $arrOption;
            }
        }
    }

}
