<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

class plg_ProductOptions_SC_CartSession extends SC_CartSession
{

    // 全商品の合計価格
    // スマホ用
    function getAllProductsTotal($productTypeId, $pref_id = 0, $country_id = 0)
    {
        // 税込み合計
        $total = 0;
        $max = $this->getMax($productTypeId);
        for ($i = 0; $i <= $max; $i++) {
            $this->cartSession[$productTypeId][$i]['option_value'] = 0;
            foreach ((array) $this->cartSession[$productTypeId][$i]['plg_productoptions'] as $option_id => $optioncategory_id) {
                if (is_null($optioncategory_id) || $optioncategory_id == "")
                    continue;
                $value = "";
                $option_action = "";
                if (plg_ProductOptions_Util::lfGetOptionType($option_id) == 3 || plg_ProductOptions_Util::lfGetOptionType($option_id) == 4) {
                    $value = $optioncategory_id;
                } else {
                    $value = plg_ProductOptions_Util::lfGetOptionCatValue($optioncategory_id);
                }
                $option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
                if ($option_action == 1) {
                    $this->cartSession[$productTypeId][$i]['option_value'] += $value;
                }
            }
            if (!isset($this->cartSession[$productTypeId][$i]['price'])) {
                $this->cartSession[$productTypeId][$i]['price'] = '';
            }

            $price = $this->cartSession[$productTypeId][$i]['price'] + $this->cartSession[$productTypeId][$i]['option_value'];

            if (!isset($this->cartSession[$productTypeId][$i]['quantity'])) {
                $this->cartSession[$productTypeId][$i]['quantity'] = '';
            }
            $quantity = $this->cartSession[$productTypeId][$i]['quantity'];

            if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
                $incTax = SC_Helper_TaxRule_Ex::sfCalcIncTax($price, $this->cartSession[$productTypeId][$i]['productsClass']['product_id'], $this->cartSession[$productTypeId][$i]['productsClass']['product_class_id'], $pref_id, $country_id);
            } else {
                $incTax = SC_Helper_DB_Ex::sfCalcIncTax($price);
            }
            $total+= ($incTax * $quantity);
        }
        return $total;
    }

    // カートへの商品追加
    public function addProduct($product_class_id, $quantity, $productoptions = array())
    {
        $objProduct = new SC_Product_Ex();
        $arrProduct = $objProduct->getProductsClass($product_class_id);
        $productTypeId = $arrProduct['product_type_id'];
        $find = false;
        $max = $this->getMax($productTypeId);
        for ($i = 0; $i <= $max; $i++) {
            if ($this->cartSession[$productTypeId][$i]['id'] == $product_class_id) {
                if (plg_ProductOptions_Util::compareOptions($this->cartSession[$productTypeId][$i]['plg_productoptions'], $productoptions)) {
                    $val = $this->cartSession[$productTypeId][$i]['quantity'] + $quantity;
                    if (strlen($val) <= INT_LEN) {
                        $this->cartSession[$productTypeId][$i]['quantity'] += $quantity;
                    }
                    $find = true;
                    break;
                } elseif (plg_ProductOptions_Util::getUpdateFlg() != 1) {
                    $this->cartSession[$productTypeId][$i]['plg_productoptions'] = $productoptions;
                    $find = true;
                    break;
                }
            }
        }
        if (!$find) {
            $this->cartSession[$productTypeId][$max + 1]['id'] = $product_class_id;
            $this->cartSession[$productTypeId][$max + 1]['quantity'] = $quantity;
            $this->cartSession[$productTypeId][$max + 1]['cart_no'] = $this->getNextCartID($productTypeId);
            $this->cartSession[$productTypeId][$max + 1]['plg_productoptions'] = $productoptions;
        }
    }

}
