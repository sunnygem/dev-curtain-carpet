<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/LC_Page_Ex.php';
require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

class LC_Page_Products_ProductOptions extends LC_Page_Ex
{
    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action()
    {

        if (!isset($_GET['option_id'])) {
            $this->tpl_onload = "window.close();";
        } else {
            $option_id = $_GET['option_id'];
            $this->arrOptionsCatList = plg_ProductOptions_Util::lfGetOptionCatList($option_id);

            $this->tpl_option_name = plg_ProductOptions_Util::lfGetOptionName($option_id);
            $this->tpl_option_action = plg_ProductOptions_Util::lfGetOptionAction($option_id);
            $this->tpl_option_description = plg_ProductOptions_Util::lfGetOptionDescription($option_id);
            $this->tpl_title = $this->tpl_option_name . "について";
        }


        if (SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE) {
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2133) {
                $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/2133/mobile/products/product_options.tpl";
            } else {
                $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/mobile/products/product_options.tpl";
            }
        } elseif (SC_Display_Ex::detectDevice() === DEVICE_TYPE_SMARTPHONE) {
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2133) {
                $this->setTemplate(PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/2133/sphone/products/product_options.tpl");
            } else {
                $this->setTemplate(PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/sphone/products/product_options.tpl");
            }
        } else {
            if (plg_ProductOptions_Util::getECCUBEVer() >= 2133) {
                $this->setTemplate(PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/2133/default/products/product_options.tpl");
            } else {
                $this->setTemplate(PLUGIN_UPLOAD_REALDIR . "ProductOptions/templates/default/products/product_options.tpl");
            }
        }
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy()
    {
        if (method_exists('LC_Page_Ex', 'destroy')) {
            parent::destroy();
        }
    }

}
