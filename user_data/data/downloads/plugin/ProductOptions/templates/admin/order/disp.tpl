<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<td>
    <!--{$arrForm.product_name.value[$product_index]|h}-->/<!--{$arrForm.classcategory_name1.value[$product_index]|default:"(なし)"|h}-->/<!--{$arrForm.classcategory_name2.value[$product_index]|default:"(なし)"|h}-->
    <br>
    <!--{if $arrForm.plg_productoptions.value[$product_index]|@count > 0}-->
    <!--{foreach from=$arrForm.plg_productoptions.value[$product_index] key=option_id item=option}-->
    <!--{$option.option_name|h}-->:<!--{$option.optioncategory_name|h}--><br>
    <!--{/foreach}-->
    <!--{/if}-->
</td>         
