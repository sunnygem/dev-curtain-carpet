<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<!--{foreach from=$arrOptions item=option}-->            
<input type="hidden" id="add_plg_productoptions_<!--{$option.option_id}-->" name="add_plg_productoptions[<!--{$option.option_id}-->]" value="" />
<!--{/foreach}-->