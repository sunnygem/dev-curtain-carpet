<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{foreach from=$arrShipping.shipment_plg_productoptions[$item_index] key=key item=option}-->
<!--{$option.option_name}-->:<!--{$option.optioncategory_name}--><br>
<!--{/foreach}-->
<!--{assign var=key value="shipment_plg_productoptions_flg"}-->
<input type="hidden" name="<!--{$key}-->[<!--{$shipping_index}-->][<!--{$item_index}-->]" value="<!--{$arrShipping[$key][$item_index]|h}-->" />