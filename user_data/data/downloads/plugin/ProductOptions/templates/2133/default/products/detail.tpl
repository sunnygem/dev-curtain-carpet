<!--{*
    *
    * Plugin Code : ProductOptions
    *
    * Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
    * http://www.bratech.co.jp/
    *
    * For the full copyright and license information, please view the LICENSE
    * file that was distributed with this source code.
    *
*}-->

<!--{if $arrProductOptions|@count > 0}-->
<script type="text/javascript">//<![CDATA[
<!--{$tpl_productoptions_js}-->

var option_charge = new Array();
var option_points = new Array();
function setOptionPriceOnChange(Obj, option_id, action){
    optioncategory_id = parseInt(Obj.options[Obj.selectedIndex].value);
    setOptionPrice(optioncategory_id, option_id, action);
}

function setOptionPriceOnChangeRadio(Obj, option_id, action){
    optioncategory_id = $(Obj).val();
    setOptionPrice(optioncategory_id, option_id, action);
}

function setOptionPrice(optioncategory_id, option_id, action){
    optioncategory_id = parseInt(optioncategory_id);
    option_id = parseInt(option_id);
    if ( action === 1 ){
        classcat_id1 = $('[name=classcategory_id1]:checked').val();
        if (classcat_id1){
            classcat_id2 = $('select[name=classcategory_id2]').val();
            classcat_id2 = classcat_id2 ? classcat_id2 : '0';
            var classcat2 = eccube['classCategories'][classcat_id1]['#' + classcat_id2];
        }

        var $form = $('#form1');
        option_value = parseInt(values[option_id][optioncategory_id]['price']);
        option_point = parseInt(values[option_id][optioncategory_id]['point']);
        if (isNaN(option_value))option_value = 0;
        if (isNaN(option_point))option_point = 0;
        option_charge[option_id] = option_value;
        option_points[option_id] = option_point;
        price01 = '';
        price02 = '';
        point = '';
        var option_price = 0;
        for (option_id in option_charge){
            option_price += parseInt(option_charge[option_id]);
        }

        // 通常価格
        var $price01_default = $form.find('[id^=price01_default], .price_sale_default');
        var $price01_dynamic = $form.find('[id^=price01_dynamic], .price_sale_dynamic');
        if (classcat2){
            price01 = classcat2['price01_exctax'];
            tax_rate = classcat2['tax_rate'];
            tax_rule = classcat2['tax_rule'];
        } else {
            price01 = 0;
            tax_rate = 0;
            tax_rule = '';
        } 
        if ((typeof price01 == 'undefined' || price01 == '') && typeof price01_base != 'undefined'){
            price01 = String(price01_base);
            tax_rate = tax_rate_base;
            tax_rule = tax_rule_base;
        }
        if (price01 != 'undefined' && price01 != ''){
            price01 = str2number(price01);
            price01 += option_price;
            price01 = price01 + sfTax(price01, tax_rate, tax_rule);
            $price01_dynamic.text(number_format(price01));
            $price01_default.hide();
            $price01_dynamic.show();
        }

        // 販売価格
        var $price02_default = $form.find('[id^=price02_default]');
        var $price02_dynamic = $form.find('[id^=price02_dynamic]');

        var $price02_inctax_default = $form.find('[id^=price02_inctax_default]');
        var $price02_inctax_dynamic = $form.find('[id^=price02_inctax_dynamic]');

        if (classcat2){
            price02 = classcat2['price02_exctax'];
        }
        if ((typeof price02 == 'undefined' || price02 == '') && typeof price02_base != 'undefined'){
            price02 = String(price02_base);
        }
        if (price02 != 'undefined' && price02 != ''){
            price02 = str2number(price02);
            price02 += option_price;
            $price02_dynamic.text(number_format(price02)+'円').show();

            price02_inctax = price02 + sfTax(price02, tax_rate, tax_rule);
            //$price02_dynamic.text(number_format(price02));
            $price02_default.hide();
            $price02_dynamic.show();

            $price02_inctax_dynamic.text(number_format(price02_inctax)+'円').show();
        }

        // ポイント
        var $point_default = $form.find('[id^=point_default]');
        var $point_dynamic = $form.find('[id^=point_dynamic]');
        if (classcat2){
            point = classcat2['point'];
        }
        if ((typeof point == 'undefined' || point == '') && typeof point_base != 'undefined'){
            point = String(point_base);
        }
        if (point != 'undefined' && point != ''){
            point = str2number(point);
            for (option_id in option_points){
                point += parseInt(option_points[option_id]);
            }

            $point_dynamic.text(number_format(point));
            $point_default.hide();
            $point_dynamic.show();
        }


    }
}

function number_format(num) {
    return num.toString().replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g, '$1,')
}

function str2number(str){
    if(typeof str!="string"){
        return parseInt(String(str).replace(/,/g, ''));
    } else {
        return parseInt(str.replace(/,/g, ''));
    }
}

function sfTax(price, tax, tax_rule) {
    real_tax = tax / 100;
    ret = price * real_tax;
    tax_rule = parseInt(tax_rule);
    switch (tax_rule) {
        // 四捨五入
        case 1:
            $ret = Math.round(ret);
            break;
            // 切り捨て
        case 2:
            $ret = Math.floor(ret);
            break;
            // 切り上げ
        case 3:
            $ret = Math.ceil(ret);
            break;
            // デフォルト:切り上げ
        default:
            $ret = Math.ceil(ret);
            break;
    }
    return $ret;
}

eccube.checkStock = function($form, product_id, classcat_id1, classcat_id2) {
    classcat_id2 = classcat_id2 ? classcat_id2 : '';
    var classcat2;
    // 商品一覧時
    if (eccube.hasOwnProperty('productsClassCategories')) {
        classcat2 = eccube['productsClassCategories'][product_id][classcat_id1]['#' + classcat_id2];
    }
    // 詳細表示時
    else {
        classcat2 = eccube['classCategories'][classcat_id1]['#' + classcat_id2];
    }

    // 商品コード
    var $product_code_default = $form.find('[id^=product_code_default]');
    var $product_code_dynamic = $form.find('[id^=product_code_dynamic]');
    if (classcat2 && typeof classcat2['product_code'] != 'undefined') {
        $product_code_default.hide();
        $product_code_dynamic.show();
        $product_code_dynamic.text(classcat2['product_code']);
    } else {
        $product_code_default.show();
        $product_code_dynamic.hide();
    }

    // 在庫(品切れ)
    var $cartbtn_default = $form.find('[id^=cartbtn_default]');
    var $cartbtn_dynamic = $form.find('[id^=cartbtn_dynamic]');
    if (classcat2 && classcat2['stock_find'] === false) {

        $cartbtn_dynamic.text('申し訳ございませんが、只今品切れ中です。').show();
        $cartbtn_default.hide();
    } else {
        $cartbtn_dynamic.hide();
        $cartbtn_default.show();
    }

    var option_price = 0;
    for (option_id in option_charge){
        option_price += parseInt(option_charge[option_id]);
    }
    if (classcat2 && typeof classcat2['tax_rate'] != 'undefined' && String(classcat2['tax_rate']).length >= 1) {
        tax_rate = classcat2['tax_rate'];
        tax_rule = classcat2['tax_rule'];
    }

    // 通常価格
    var $price01_default = $form.find('[id^=price01_default], .price_sale_default');
    var $price01_dynamic = $form.find('[id^=price01_dynamic], .price_sale_dynamic');
    if (classcat2 && typeof classcat2['price01'] != 'undefined' && String(classcat2['price01']).length >= 1) {
        //    price01 = str2number(classcat2['price01_exctax'].replace(',',''));
        price01 = str2number(classcat2['price01_exctax']);
        price01 += option_price;
        price01 = price01 + sfTax(price01, tax_rate, tax_rule);
        $price01_dynamic.text(number_format(price01)).show();
        $price01_default.hide();
    } else {
        $price01_dynamic.hide();
        $price01_default.show();
    }

    // 販売価格
    var $price02_default = $form.find('[id^=price02_default]');
    var $price02_dynamic = $form.find('[id^=price02_dynamic]');

    var $price02_inctax_default = $form.find('[id^=price02_inctax_default]');
    var $price02_inctax_dynamic = $form.find('[id^=price02_inctax_dynamic]');
    if (classcat2 && typeof classcat2['price02'] != 'undefined' && String(classcat2['price02']).length >= 1) {
        price02 = str2number(classcat2['price02_exctax']);
        //	price02 = str2number(classcat2['price02_exctax'].replace(',',''));
        price02 += option_price;
        $price02_dynamic.text(number_format(price02)+'円').show();
        $price02_default.hide();

        price02_inctax = price02 + sfTax(price02, tax_rate, tax_rule);
        $price02_inctax_default.hide();
        $price02_inctax_dynamic.text(number_format(price02_inctax)+'円').show();
    } else {
        $price02_dynamic.hide();
        $price02_default.show();

        $price02_inctax_dynamic.hide();
        $price02_inctax_default.show();
    }

    // ポイント
    var $point_default = $form.find('[id^=point_default]');
    var $point_dynamic = $form.find('[id^=point_dynamic]');
    if (classcat2 && typeof classcat2['point'] != 'undefined' && String(classcat2['point']).length >= 1) {
        point = str2number(classcat2['point'].replace(',',''));
        for (option_id in option_points){
            point += parseInt(option_points[option_id]);
        }

        $point_dynamic.text(number_format(point)).show();
        $point_default.hide();
    } else {
        $point_dynamic.hide();
        $point_default.show();
    }

    // 商品規格
    var $product_class_id_dynamic = $form.find('[id^=product_class_id]');
    if (classcat2 && typeof classcat2['product_class_id'] != 'undefined' && String(classcat2['product_class_id']).length >= 1) {
        $product_class_id_dynamic.val(classcat2['product_class_id']);
    } else {
        $product_class_id_dynamic.val('');
    }

    //オーダー選択時
    if(classcat2){
        var name = classcat2.name;
        var namearray = name.split('_');
        if(namearray[0]=='order'){
            //				$('input.orderwidth').val(namearray[2]);
            //				$('input.orderheight').val(namearray[4]);
        }
    }

}
//]]></script>

<style type="text/css">
#productoptions select,
#productoptions input[type="radio"] {
    margin: 0 5px 10px 0;
}

.productoptionlist li {
width: 100px;
margin: 0 20px 0 0;
}

.productoptionlist p {
    /*       width: 100px;
height: 75px;*/
display: table-cell;
         text-align: center;
         vertical-align: middle;
         /*        border: 1px solid #ccc;*/
}

.productoptionlist p img {
    max-width: 100px;
    max-height: 75px;
}
.productoptionlist li {
display:inline;
}
.productoptionlist li input[type=radio],
    .productoptionlist li label {
        /*	display:none; */
    }
.productoptionlist li img {
width:30px;
}
.productoptionlist li img.selected {
border: 2px red solid;
}
</style>

<!--▼規格1-->
<!--{if $tpl_classcat_find1}-->
<div class="spec open_type">
<dl>
<dt>
<!--{$tpl_class_name1|h}-->
</dt>
<dd>
<ul class="radio_box">
<!--{foreach from=$arrClassCat1 item=val key=key}-->
<!--{if $key===127}-->
<li>
<input type="radio" name="classcategory_id1" value="<!--{$key}-->" id="classcategory_id1_<!--{$key}-->" <!--{if $arrForm.classcategory_id1.value === $key}-->checked="checked"<!--{/if}-->>
<label for="classcategory_id1_<!--{$key}-->">
<div class="txt"> あり </div>
</label>
</li>
<!--{elseif $key===128}-->
<li>
<input type="radio" name="classcategory_id1" value="<!--{$key}-->"  id="classcategory_id1_<!--{$key}-->" <!--{if $arrForm.classcategory_id1.value === $key}-->checked="checked"<!--{/if}-->>
<label for="classcategory_id1_<!--{$key}-->">
<div class="txt"> なし </div>
</label>
</li>
<!--{/if}-->
<!--{/foreach}-->
</ul>
</dd>
</dl>
<!--{if $arrErr.classcategory_id2 != ""}-->
<span class="attention">※ <!--{$tpl_class_name1}-->を選択して下さい。</span>
<!--{/if}-->
</div>
<!--{/if}-->


<div class="spec">
<!--{assign var=show_flg value=block}-->
<!--{*オーダー品含む*}-->
<!--{if $easy_order_flg === true || $full_order_flg === true }-->
    <!--{if $full_order_flg }-->
        <!--{assign var="str" value='フルオーダー'}-->
    <!--{elseif $easy_order_flg }-->
        <!--{assign var="str" value='イージーオーダー'}-->
        <input type="hidden" name="is_easy_order">
    <!--{/if}-->

    <!--{*既製品を含まない*}-->
    <!--{assign var=is_order value=false}-->
    <!--{if $established_flg !== true }-->
        <!--{assign var=is_order value=true}-->
        <!--{assign var=show_flg value=block}-->
    <!--{/if}-->
    <div class="agree">
        <input type="checkbox" name="check_order" class="checkNone" id="agree01" <!--{if $is_order === true }-->checked=checked disabled<!--{/if}-->>
        <label for="agree01"> サイズ加工（<!--{$str}-->）をお申込み頂く場合はチェックをつけてください </label>
    </div>

    <!--{if $established_flg === false}-->
        <input type="hidden" name="order_flg_disp" id="order_flg_disp" value="2"/>
    <!--{else}-->
        <!--{assign var=show_flg value=none}-->
        <input type="hidden" name="order_flg_disp" id="order_flg_disp" value="0"/>
        <script>
        $(function(){
            // カテゴリ2のためのループ用のキーを取得
            $('#agree01').on('change', function(){
                var flg = $(this).prop('checked');
                if( flg )
                {
                    $('#order_flg_disp').val(2);
                    $('.classlist, .final_result').stop(true, true).slideToggle();
                }
                else
                {
                    $('#order_flg_disp').val(0);
                    $('.classlist, .final_result').stop(true, true).slideToggle();
                }
                $form=$('#form1');
                product_id=$form.find('input[name=product_id]').val();
                $sele1=$("select[name=classcategory_id1], [name=classcategory_id1]:checked");
                $sele2=$("select[name=classcategory_id2]");
                eccube.setClassCategories($form, product_id, $sele1, $sele2, '');
            });
        });
        </script>
    <!--{/if}-->

<!--{*既製品のみ*}-->
<!--{else}-->
    <!--{assign var=show_flg value=none}-->
    <input type="hidden" name="order_flg_disp" id="order_flg_disp" value="0"/>
<!--{/if}-->
</div><!--/.spec -->

<div class="classlist" style="display: <!--{$show_flg}-->;">
<!--{foreach from=$arrProductOptions|@array_reverse:true item=option key=option_id}-->
<!--{assign var=option_name value=plg_productoptions[`$option_id`]}-->
<!--{assign var=option_tag_id value=plg_productoptions_`$option_id`}-->
<!--{assign var=action value=`$option.action`}-->
<!--{assign var=type value=`$option.type`}-->

<!--{if $type === '1'}--><!--{*セレクト*}-->
    <dl>
        <dt><!--{$option.name}--></dt>
        <dd>
            <select name="<!--{$option_name}-->" id="<!--{$option_tag_id}-->" onChange="setOptionPriceOnChange(this, <!--{$option_id}--> , <!--{$action}--> );" style="<!--{$plg_productoptions_Err[$option_id]|sfGetErrorColor}-->">
            <!--{html_options options=$option.options selected=$arrForm.plg_productoptions.value[$option_id]|default:$option.option_init}-->
            </select>
        <!--{if $option.description_flg == 2 || $option.description_flg == 3}-->
        <ul class="productoptionlist clearfix">
            <!--{foreach from=$option.optioncategory item=optioncategory}-->
            <!--{if $optioncategory.default_flg != 1}-->
            <li>
                <!--{$optioncategory.name|h}--><br />
                <!--{if strlen($optioncategory.option_image) > 0}-->
                <p><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$optioncategory.option_image}-->" alt="<!--{$optioncategory.name|h}-->" /></p>
                <!--{/if}-->
            </li>
            <!--{/if}-->
            <!--{/foreach}-->
        </ul>
        <!--{/if}-->
        </dd>
    </dl>
<!--{elseif $type === '2'}--><!--{*ラジオ*}-->
    <!--{if $option.description_flg == 2 || $option.description_flg == 3 }-->
        <dl>
            <dt><!--{$option.name}--></dt>
            <dd>
                <ul class="productoptionlist clearfix">
                    <!--{foreach from=$option.optioncategory item=optioncategory}-->
                    <!--{assign var=selected_value value=`$arrForm.plg_productoptions.value[$option_id]`}-->
                    <!--{if $selected_value == ""}-->
                    <!--{assign var=selected_value value=`$option.option_init`}-->
                    <!--{/if}-->
                    <li>
                        <label><input type="radio" name="<!--{$option_name|h}-->" value="<!--{$optioncategory.optioncategory_id|h}-->" onChange="setOptionPriceOnChangeRadio(this, <!--{$option_id}--> , <!--{$action}--> );" <!--{if $selected_value == $optioncategory.optioncategory_id}-->checked<!--{/if}--> id="<!--{$option_tag_id}-->" data-name="<!--{$optioncategory.name|h}-->"/><!--{$optioncategory.name|h}--></label>
                        <!--{if strlen($optioncategory.option_image) > 0}-->
                        <p><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$optioncategory.option_image}-->" alt="<!--{$optioncategory.name|h}-->" /></p>
                        <!--{/if}-->
                        <!--{if $option.pricedisp_flg == 1}-->
                        <!--{$optioncategory.value|n2s|h}--><!--{if $action==1}-->円<!--{elseif $action==2}-->Pt<!--{/if}-->
                        <!--{/if}-->
                    </li>
                    <!--{/foreach}-->
                </ul>
            </dd>
        </dl>
    <!--{else}-->
<!--{if ( $option.option_id === "5" || $option.option_id === "6" ) && $full_order_flg }-->
    <!--{if $option.option_id === "5" }-->
    <div class="spec">
    <dl class="">
        <dt>オプション</dt>
        <dd>
            <p>タッセルを追加される場合は、両開きと片開きのどちらかを選択してください。</p>
            <ul class="radio_box">
                <li>
                    <input type="checkbox" name="plg_productoptions[<!--{$option_id}-->]" id="<!--{$option_tag_id}-->" class="checkNone option_tassel" value="5" onChange="setOptionPriceOnChangeRadio(this,<!--{$option_id}-->,<!--{$action}-->);" data-option-id="<!--{$option_id}-->">
                    <label for="<!--{$option_tag_id}-->">
                        <div class="txt">タッセル（両開き）</div>
                        <div class="img"><img src="<!--{$TPL_URLPATH}-->img/products/product_status_tassel_double.png" alt="両開き"></div>
                    </label>
                </li>
    <!--{/if}-->
    <!--{if $option.option_id === "6" }-->
                <li>
                    <input type="checkbox" name="plg_productoptions[<!--{$option_id}-->]" id="<!--{$option_tag_id}-->" class="checkNone option_tassel" value="9" onChange="setOptionPriceOnChangeRadio(this,<!--{$option_id}-->,<!--{$action}-->);" data-option-id="<!--{$option_id}-->">
                    <label for="<!--{$option_tag_id}-->">
                        <div class="txt">タッセル（片開き）</div>
                        <div class="img"><img src="<!--{$TPL_URLPATH}-->img/products/product_status_tassel_single.png" alt="両開き"></div>
                    </label>
                </li>
            </ul>
        </dd>
    </dl>
    </div><!--/.cpec-->
    <!--{/if}-->
<!--{elseif $option.option_id === "3" && ( $easy_order_flg || $full_order_flg ) }-->
<!--{*　デザインのために外に出しています
    <div class="spec">
        <dl class="">
        <dt>開き方</dt>
        <dd id="hiraki">
        <ul class="radio_box">
        <li>
        <input type="radio" id="hiraki01" name="plg_productoptions[<!--{$option_id}-->]" id="<!--{$option_tag_id}-->" value="1" onChange="setOptionPriceOnChangeRadio(this,<!--{$option_id}-->,<!--{$action}-->);">
        <label for="hiraki01">
        <div class="txt">
        両開き
        </div>
        <div class="img">
        <img src="<!--{$TPL_URLPATH}-->img/products/product_status_hiraki_double.png" alt="両開き">
        </div>
        </label>
        </li>
        <li>
        <input type="radio" id="hiraki02" name="plg_productoptions[<!--{$option_id}-->]" id="<!--{$option_tag_id}-->" value="2" onChange="setOptionPriceOnChangeRadio(this,<!--{$option_id}-->,<!--{$action}-->);">
        <label for="hiraki02">
        <div class="txt">
        片開き
        </div>
        <div class="img">
        <img src="<!--{$TPL_URLPATH}-->img/products/product_status_hiraki_single.png" alt="片開き">
        </div>
        </label>
        </li>
        </ul>
        </dd>
        </dl>
        </div>
*}-->
        <!--{elseif ($option.option_id === "4" && $easy_order_flg === true || $full_order_flg === true) }-->
        <div class="spec">
            <dl class="">
                <dt>フック</dt>
                <dd>
                    <ul class="radio_box">
                        <li>
                            <input type="radio" id="rail01" name="plg_productoptions[<!--{$option_id}-->]" id="<!--{$option_tag_id}-->" value="3" onChange="setOptionPriceOnChangeRadio(this,<!--{$option_id}-->,<!--{$action}-->);">

                            <label for="rail01">
                                <div class="txt"> レールが見えるAフック </div>
                                <div class="img">
                                    <img src="<!--{$TPL_URLPATH}-->img/products/product_status_rail_a.png" alt="レールが見える上付き">
                                </div>
                            </label>
                        </li>
                        <li>
                            <input type="radio" id="rail02" name="plg_productoptions[<!--{$option_id}-->]" id="<!--{$option_tag_id}-->" value="4" onChange="setOptionPriceOnChangeRadio(this,<!--{$option_id}-->,<!--{$action}-->);">
                            <label for="rail02">
                                <div class="txt">
                                レールが見えないBフック
                                </div>
                                <div class="img">
                                    <img src="<!--{$TPL_URLPATH}-->img/products/product_status_rail_b.png" alt="レールが見えない下付き">
                                </div>
                            </label>
                        </li>
                    </ul>
                </dd>
            </dl>
            <div class="measure_curtaion" style="margin-top:1em;">
                &gt;&gt;<a href="#curtain_fook">フックについて詳しく見る</a>
            </div>
        </div>
        <!--{else}-->
        <dl>
        <dt><!--{$option.name}--></dt>
        <dd>
        <!--{html_radios name=$option_name id=$option_tag_id options=$option.options selected=$arrForm.plg_productoptions.value[$option_id]|default:$option.option_init onChange="setOptionPriceOnChangeRadio(this,$option_id,$action);"}-->
        </dd>
        </dl>
        <!--{/if}-->
        <!--{/if}-->
        <!--{elseif $type === '3'}--><!--{*テキスト*}-->
        <!--{if $option.option_id === "1" && ( $easy_order_flg || $full_order_flg ) }-->
        <div class="spec wide">
        <dl class="">
        <dt>横幅</dt>
        <dd>
        <p>採寸方法を確認して頂き、レールの長さ(cm)を測って下さい。<br>
        <div class="railBox01">
        <img src="<!--{$TPL_URLPATH}-->img/products/product_status_measure.png" alt="レール"><br>
        <div class="input_wrap">
        <p>レールの長さ<span>（単位はセンチメートル（cm）です）</span></p>
        <input type="number" name="input_orderwidth" value="" class="box300" style="<!--{$plg_productoptions_Err[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->" min="0" step="1">
        <input type="hidden" name="<!--{$option_name}-->" value="<!--{$arrForm.plg_productoptions.value[$option_id]|h}-->" class="orderwidth">
        <label>cm</label>
        </div>
        <p class="product_attention">一般的な機能レールや装飾レールにより注意点が異なります。</p>
        <p class="measure_curtaion">
        >><a href="/shop/user_data/measure-curtain.php" target="_blank" >採寸方法を詳しく見る</a>
        </p>
        </div>
        </dd>
        </dl>
        <div class="calc">
        <div class="calc_inner">
        <p>
        仕上がりサイズは、生地のゆとり分を考慮して<br>
        測ったレールの長さ×1.05倍を目安にお仕立ていたします。
        </p>
        <!--<div class="sizeBox_bg01">
        <p>仕上がりサイズ&nbsp;&nbsp;<span></span>cm</p>
        <aside>生地のゆとり分を1.05倍した<br>
        仕上がりサイズを自動で計算します。</aside>
        </div>-->
        <div class="result">
        <p>仕上がりサイズ<span>自動計算項目（小数点以下、切り上げ）</span></p>
        <div class="formula">
        <ul>
        <li><span id="product_order_wide">-</span>cm</li>
        <li class="sym"><span>×</span></li>
        <li><span>1.05</span></li>
        <li class="sym">＝</li>
        <li><span id="product_order_sum_wide">-</span>cm</li>
        </ul>
        </div>
        </div>
        </div>
        <div class="calc_inner hiraki">
        <p> <span class="select_hiraki"></span>を選択されていますので<br> お届けするカー テンの巾は以下となります </p>
        <div class="result">
        <p>巾<!--|* <span>自動計算項目（小数点以下、切り上げ）</span>*}--></p>
        <div class="formula">
        <ul>
        <li><span id="hiraki_molecule">-</span>cm</li>
        <li class="sym"><span>÷</span></li>
        <li><span class="divider">-</span></li>
        <li class="sym">＝</li>
        <li><span id="hiraki_result">-</span>cm</li>
        </ul>
        </div>
        </div>
        </div>
        <div class="agree">
        <input type="checkbox" name="checkwidth" class="checkNone" id="agree_wide">
        <label for="agree_wide">
        横幅は、両端のフック穴を基準に測りました。間違いない場合はチェックをつけてください。
        </label>
        </div>
        </div>
        </div>

<script>
    $(function() {
        // 規格1選択時
        $("input.orderwidth, input.orderheight").on('change', function() {
            var orderwidth  = parseInt( $('input.orderwidth').val() );
            var orderheight = parseInt( $('input.orderheight').val() );
            var orderflag   = 0;
            if( orderwidth && orderheight ){
                if($('[name=classcategory_id1]').val()!="__unselected"){
                    // KJO-188より、開き方の選択による計算は必要ない。
                    //var $is_easy_order = $('[name=is_easy_order]');
                    //var $hiraki        = $('#hiraki [name="plg_productoptions[3]"]:checked');
                    //// イージーオーダー且つ両開きの場合 (todo フルオーダーの規格が変更になった時)
                    //if( $is_easy_order[0] && $hiraki.val() === '1' )
                    //{
                    //    orderwidth = orderwidth / 2;
                    //}

                    $("select[name=classcategory_id2] option").prop("selected", false);
                    $('select[name=classcategory_id2] option[data-type=order]').each(function(i,e){
                        minwidth  = ( $(e).attr('data-minwidth') )  ? parseInt( $(e).attr('data-minwidth') )  : 0;
                        maxwidth  = ( $(e).attr('data-maxwidth') ) ? parseInt( $(e).attr('data-maxwidth') )  : 0;
                        minheight = ( $(e).attr('data-minheight')) ? parseInt( $(e).attr('data-minheight') ) : 0;
                        maxheight = ( $(e).attr('data-maxheight')) ? parseInt( $(e).attr('data-maxheight') ) : 0;
                        dataid=$(e).attr('data-id');

                        if( minwidth<=orderwidth && orderwidth<=maxwidth && minheight<=orderheight && orderheight<=maxheight){
                            $(e).prop("selected",true);
                            var $form = $(this).parents('form');
                            var product_id = $form.find('input[name=product_id]').val();
                            var $sele1 = $form.find('[name=classcategory_id1]:checked');
                            var $sele2 = $(this);
                            eccube.checkStock($form, product_id, $sele1.val(), $sele2.val());
                            orderflag = 1;

                            // 合致したらループを止める
                            return false;
                        }
                    });

                    //最大幅対応
                    //if($('#widthlimit').text()!="" && $('dd#hiraki').length && orderflag==1){
                    //    var tmpmax=$('#widthlimit').text().split(':');
                    //    if($('dd#hiraki input[type=radio]:checked').val()==2){//片開き
                    //        if(orderwidth>parseInt(tmpmax[0])){
                    //            alert("片開きでそのサイズ（幅）は選択出来ません");
                    //            $("select[name=classcategory_id2] option").prop("selected",false);
                    //        }
                    //    } else if($('dd#hiraki input[type=radio]:checked').val()==1) {
                    //        if(orderwidth>=parseInt(tmpmax[1])){
                    //            alert("両開きでそのサイズ（幅）は選択出来ません");
                    //            $("select[name=classcategory_id2] option").prop("selected",false);
                    //        }
                    //    }
                    //}

                    if( orderflag === 0 ){
                        alert("範囲外のオーダーサイズです");
                        $("select[name=classcategory_id2] option").prop("selected",false);
                    }
                } else {
                    alert($('dt[name=classcategory_id1_name]').text()+"を選択してください");
                }
            }
            //eccube.footercart_set();
            return false;
        });

        //$('#hiraki input[type=radio]').on('change', function(){
        //    var orderwidth=parseInt($('input.orderwidth').val());
        //    //最大幅対応
        //    if($('#widthlimit').text()!="" && $('dd#hiraki').length){
        //        var tmpmax=$('#widthlimit').text().split(':');
        //        if($('dd#hiraki input[type=radio]:checked').val()==2){//片開き
        //            if(orderwidth>parseInt(tmpmax[0])){
        //                alert("片開きでそのサイズ（幅）は選択出来ません");
        //                $("select[name=classcategory_id2] option").prop("selected",false);
        //            }
        //        } else if($('dd#hiraki input[type=radio]:checked').val()==1) {
        //            if(orderwidth>=parseInt(tmpmax[1])){
        //                alert("両開きでそのサイズ（幅）は選択出来ません");
        //                $("select[name=classcategory_id2] option").prop("selected",false);
        //            }
        //        }
        //    }
        //});
    });
</script>
<!--{elseif $option.option_id === '2' && ( $easy_order_flg || $full_order_flg ) }-->
<div class="spec">
<dl class="">
<dt>高さ</dt>
<dd>
<p>ランナー（フックをかける部品）の穴の下端から窓枠最下部までの長さを測ってください。</p>
<div class="railBox01">
<img src="<!--{$TPL_URLPATH}-->img/products/product_status_ranner.png" alt="レール"><br>
<div class="input_wrap">
<p>高さ <span>（単位はセンチメートル（cm）です）</span></p>
<input type="number" name="<!--{$option_name}-->" value="<!--{$arrForm.plg_productoptions.value[$option_id]|h}-->" class="box300 orderheight" style="<!--{$plg_productoptions_Err[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->" min="0" step="1">
<label>cm</label>
</div>
<p class="" style="margin-bottom:1em;line-height:1.6em;color:#E86D98;font-weight:bold;">腰高窓や掃き出し窓など、窓の種類によってお仕立てのサイズが変わりますので、採寸をお願いいたします。</p>
<div class="product_attention" style="margin-bottom:1em;">採寸したサイズに、以下の調整をお願いします。<br>
ドレープカーテン・レースカーテンを一緒にお使いの場合<br>
（レースカーテンは一般に、ドレープカーテンよりも -1cmまたは-2cmの高さで仕上げます）
<ul>
    <li><span>腰高窓の場合：ドレープは +15cm、レースは +13cmした数値をご入力ください。</span></li>
    <li><span>掃き出し窓の場合：ドレープは -1cm、レースは -2cmした数値をご入力ください。</span></li>
</ul>
</div>
<div class="product_attention">腰高窓で、窓下に棚や収納などの家具があるような場合は、ご判断・調整をお願いいたします。</p>
<p class="measure_curtaion">
&gt;&gt;<a href="/shop/user_data/measure-curtain.php" target="_blank">採寸方法を詳しく見る</a>
</p>
</div>
</dd>
</dl>
<div class="calc">
<div class="calc_inner height">
    <img src="<!--{$TPL_URLPATH}-->img/products/product_status_height.png">
</div>
<div class="agree">
<input type="checkbox" name="checkheight" class="checkNone" id="agree_height">
<label for="agree_height">
高さはフック穴を基準に測りました。間違いない場合はチェックをつけてください。
</label>
</div>
</div>
</div>
<!--{else}-->
<dl>
<dt><!--{$option.name}--></dt>
<dd>
<input type="text" name="<!--{$option_name}-->" value="<!--{$arrForm.plg_productoptions.value[$option_id]|h}-->" class="box300" style="<!--{$plg_productoptions_Err[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->">
</dd>
</dl>
<!--{/if}-->
<!--{elseif $type === '4'}--><!--{*テキストエリア*}-->
<dl>
<dt><!--{$option.name}--></dt>
<dd>
<textarea name="<!--{$option_name}-->" cols="50" rows="8" class="txtarea" wrap="hard" style="<!--{$plg_productoptions_Err[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->"><!--{$arrForm.plg_productoptions.value[$option_id]|h}--></textarea>
</dd>
</dl>
<!--{/if}-->

<!--{if $plg_productoptions_Err[$option_id]}--><br><span class="attention"><!--{$plg_productoptions_Err[$option_id]}--></span><!--{/if}-->

<!--{if $option.description_flg == 1 || $option.description_flg == 3}-->
&nbsp;<a class="btn-action" href="javascript:;" onclick="win03('<!--{$smarty.const.ROOT_URLPATH}-->products/product_options.php?option_id=<!--{$option_id}-->', 'option_win', 650, 500); return false;">内容説明</a>
<!--{/if}-->

<!--{if $type !== '3' && $type !== '4'}-->
<script type="text/javascript">//<![CDATA[
setOptionPrice( <!--{$arrForm.plg_productoptions.value[$option_id]|default:$option.option_init|h}--> , <!--{$option_id|h}--> , <!--{$action|h}--> );
//]]></script>
<!--{/if}-->

<!--{/foreach}-->

</div><!--/.classlist -->

<!--{if $tpl_classcat_find2}-->
<div class="spec size">
<dl>
    <dt> サイズ </dt>
    <dd>
        <div class="select_wrap">
            <select name="classcategory_id2">
                <option><!--{$tpl_class_name1|h}-->を選択して下さい</option>
            </select>
            <span class="arrow"></span>
        </div>
    </dd>
</dl>
<!--{if $arrErr.classcategory_id2 != ""}-->
<br /><span class="attention">※ <!--{$tpl_class_name2}-->を選択して下さい。</span>
<!--{/if}-->
</div>
<!--{/if}-->

<!--{if $easy_order_flg === true || $full_order_flg === true }-->
<div class="final_result" style="display: <!--{$show_flg}-->;">
    <div class="order_contents">
        <p>お届けするカーテンのサイズと枚数<span>自動計算項目（小数点以下、切り上げ）</span></p>
        <div class="result01">--</div>
        <div class="result02">
            <ul>
                <li>横幅<span class="width">-</span>cm</li>
                <li class="sym">×</li>
                <li>高さ<span class="height">-</span>cm</li>
                <li class="sym">×</li>
                <li><span class="num">0</span>枚</li>
            </ul>
        </div>
    </div>
    <p>上記サイズと枚数のオーダーを承りました。<br>
    お間違いなければ、数量をご確認のうえ、「カートに入れる」ボタンを選択してください。</p>
</div><!--/:final_result-->
<!--{/if}-->


<script>
$(function() {
    // チェックボックス、ラジオボタン対応
    //$(".checkBox01 .clickable").click(function() {
    //	var telm=$(this).parents(".checkBox01").find("input[type=checkbox]");
    //	if(telm.prop("checked")){
    //		telm.prop("checked",false);
    //	} else {
    //		telm.prop("checked",true);
    //	}
    //});
    //$(".railBox02 .clickable").click(function() {
    //	var telm=$(this).parents(".railBox02").find("input[type=radio]");
    //	if(telm.prop("checked")){
    //		//telm.prop("checked",false);
    //	} else {
    //		telm.prop("checked",true);
    //	}
    //});
        //});

    $(window).on('load',function(){
        //$("input#ccolor").val($('.productoptionlist li input[type=radio]:checked').data("name"));

        // 初期選択
        var $targetRadio = $('[type=radio][name="classcategory_id1"]');
        var ko1=$targetRadio.length; //規格１が１つだけの処理
        if(ko1===1){
            $targetRadio.prop('checked', true);

            var $form = $targetRadio.closest('form');
            var product_id = $form.find('input[name=product_id]').val();
            var $sele1 = $targetRadio;
            var $sele2 = $form.find('select[name=classcategory_id2]');

        // 規格1のみの場合
            if (!$sele2.length) {
                eccube.checkStock($form, product_id, $sele1.val(), '0');
            // 規格2ありの場合
            } else {					
                eccube.setClassCategories($form, product_id, $sele1, $sele2);
            }
        }

    });
});

</script>
<!--{else}-->
<!--{* カーペットの場合　*}-->
    <!--{if $zakka_flg === false }-->
        <!--{if $tpl_classcat_find1}-->
            <div class="classlist">
                <div class="spec" <!--{if $tpl_class_name1 === 'dummy'}-->style="display:none;"<!--{/if}-->>
                    <!--▼規格1-->
                    <dl class="">
                        <dt><!--{$tpl_class_name1|h}-->：</dt>
                        <dd>
                            <div class="select_wrap">
                                <select name="classcategory_id1" style="<!--{$arrErr.classcategory_id1|sfGetErrorColor}-->">
                                <!--{html_options options=$arrClassCat1 selected=$arrForm.classcategory_id1.value}-->
                                </select>
                                <!--{if $arrErr.classcategory_id1 != ""}-->
                                <br /><span class="attention">※ <!--{$tpl_class_name1}-->を入力して下さい。</span>
                                <!--{/if}-->
                            </div>
                        </dd>
                    </dl>
                    <!--▲規格1-->
                </div>

                <!--{if $tpl_classcat_find2}-->
                <!--▼規格2-->
                <div class="spec size">
                    <dl class="">
                        <dt> <!--{$tpl_class_name2|h}-->： </dt>
                        <dd>
                            <div class="select_wrap">
                                <select name="classcategory_id2" style="<!--{$arrErr.classcategory_id2|sfGetErrorColor}-->">
                                </select>
                                <!--{if $arrErr.classcategory_id2 != ""}-->
                                <br /><span class="attention">※ <!--{$tpl_class_name2}-->を入力して下さい。</span>
                                <!--{/if}-->
                            </div>
                        </dd>
                    </dl>
                </div>
                <!--▲規格2-->
                <!--{/if}-->
            </div>
        <!--{/if}-->
    <!--{/if}-->
<!--{/if}-->
