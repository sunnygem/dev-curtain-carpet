<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<td><!--{* 商品名 *}--><strong><!--{$arrForm.name.value[$index]|h}--></strong><br />
    <!--{if $arrForm.classcategory_name1.value[$index] != ""}-->
    <!--{$arrForm.class_name1.value[$index]|h}-->：<!--{$arrForm.classcategory_name1.value[$index]|h}--><br />
    <!--{/if}-->
    <!--{if $arrForm.classcategory_name2.value[$index] != ""}-->
    <!--{$arrForm.class_name2.value[$index]|h}-->：<!--{$arrForm.classcategory_name2.value[$index]|h}--><br />
    <!--{/if}-->
    <!--{if $arrForm.plg_productoptions_optioncategory_name.value[$index]|@count > 0}-->
    <!--{foreach from=$arrForm.plg_productoptions_option_name.value[$index] item=item name=plg_options_loop}-->
    <!--{assign var=option value=$arrForm.plg_productoptions_option_name.value[$index]}-->
    <!--{assign var=optioncategory value=$arrForm.plg_productoptions_optioncategory_name.value[$index]}-->
    <!--{assign var=index2 value=$smarty.foreach.plg_options_loop.index}-->
    <!--{$option[$index2]}-->:<!--{$optioncategory[$index2]|h|nl2br}--><br>
    <!--{/foreach}-->
    <!--{/if}-->
    <!--{math assign=price equation="a+b" a=$arrForm.price_inctax.value[$index] b=$arrForm.add_price_inctax.value[$index]}-->
    <!--{$price|n2s}-->円
</td> 
