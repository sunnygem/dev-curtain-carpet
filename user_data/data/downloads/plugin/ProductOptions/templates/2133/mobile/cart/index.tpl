<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<form name="form<!--{$key}-->" id="form<!--{$key}-->" method="post" action="?" utn>
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->">
    <input type="hidden" name="mode" value="confirm">
    <input type="hidden" name="cart_no" value="">
    <input type="hidden" name="cartKey" value="<!--{$key}-->">
    <!--ご注文内容ここから-->
    <!--{if count($cartKeys) > 1}-->
    <hr>
    ■<!--{$arrProductType[$key]}-->
    <hr>
    <!--{/if}-->
    <!--{foreach from=$cartItems[$key] item=item}-->
    ◎<!--{* 商品名 *}--><!--{$item.productsClass.name|h}--><br>
    <!--{* 規格名1 *}--><!--{if $item.productsClass.classcategory_name1 != ""}--><!--{$item.productsClass.class_name1}-->：<!--{$item.productsClass.classcategory_name1}--><br><!--{/if}-->
    <!--{* 規格名2 *}--><!--{if $item.productsClass.classcategory_name2 != ""}--><!--{$item.productsClass.class_name2}-->：<!--{$item.productsClass.classcategory_name2}--><br><!--{/if}-->
    <!--{* オプション *}-->
    <!--{if $item.plg_productoptions|@count > 0}-->
    <!--{foreach from=$item.plg_productoptions_detail item=option}-->
    <!--{$option.option_name|h}-->:<!--{$option.optioncategory_name|h}--><br>
    <!--{/foreach}-->
    <!--{/if}-->
    <!--{* 販売価格 *}-->
    <!--{$item.price|sfCalcIncTax|n2s}-->円
    × <!--{$item.quantity}--><br>
    <br>
    <!--{* 数量 *}-->
    数量:<!--{$item.quantity}-->
    <a href="?mode=up&amp;cart_no=<!--{$item.cart_no}-->&amp;cartKey=<!--{$key}-->">＋</a>
    <a href="?mode=down&amp;cart_no=<!--{$item.cart_no}-->&amp;cartKey=<!--{$key}-->">－</a>
    <a href="?mode=delete&amp;cart_no=<!--{$item.cart_no}-->&amp;cartKey=<!--{$key}-->">削除</a><br>
    <!--{* 合計 *}-->
    小計:<!--{$item.total_inctax|n2s}-->円<br>
    <div align="right"><a href="<!--{$smarty.const.MOBILE_P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->">→商品詳細へ</a></div>
    <HR>
    <!--{/foreach}-->
    <font color="#FF0000">
    商品合計:<!--{$tpl_total_inctax[$key]|n2s}-->円<br>
    合計:<!--{$arrData[$key].total-$arrData[$key].deliv_fee|n2s}-->円<br>
    </font>
    <br>
    <!--{if $key != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
    <!--{if $arrInfo.free_rule > 0}-->
    <!--{if !$arrData[$key].is_deliv_free}-->
    あと「<font color="#FF0000"><!--{$tpl_deliv_free[$key]|n2s}-->円</font>」で<font color="#FF0000">送料無料</font>です！！<br>
    <!--{else}-->
    現在、「<font color="#FF0000">送料無料</font>」です！！<br>
    <!--{/if}-->
    <br>
    <!--{/if}-->
    <!--{/if}-->

    <!--{if $smarty.const.USE_POINT !== false}-->
    <!--{if $arrData[$key].birth_point > 0}-->
    お誕生月ﾎﾟｲﾝﾄ<br>
    <!--{$arrData[$key].birth_point|n2s}-->pt<br>
    <!--{/if}-->
    今回加算ﾎﾟｲﾝﾄ<br>
    <!--{$arrData[$key].add_point|n2s}-->pt<br>
    <br>
    <!--{/if}-->

    <center><input type="submit" value="注文する"></center>
</form>