<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<td>
    <!--{$arrForm.product_name.value[$product_index]|h}-->/<!--{$arrForm.classcategory_name1.value[$product_index]|default:"(なし)"|h}-->/<!--{$arrForm.classcategory_name2.value[$product_index]|default:"(なし)"|h}-->
    <br>
    <input type="hidden" name="plg_productoptions_num[<!--{$product_index}-->]" value="<!--{$arrForm.plg_productoptions.value[$product_index]|@count|default:0}-->" id="plg_productoptions_num_<!--{$product_index}-->" />
    <input type="hidden" name="product_index[<!--{$product_index}-->]" value="<!--{$product_index}-->" id="product_index_<!--{$product_index}-->" />
    <!--{if $arrForm.plg_productoptions.value[$product_index]|@count > 0}-->
    <!--{foreach from=$arrForm.plg_productoptions.value[$product_index] key=option_id item=option name=option_loop}-->
    <!--{if strlen($option.optioncategory_name) > 0}-->
    <!--{$option.option_name|h}-->:<!--{$option.optioncategory_name|h|nl2br}--><br>
    <!--{/if}-->
    <input type="hidden" name="plg_productoptions[<!--{$product_index}-->][<!--{$option_id}-->]" value="<!--{$option.optioncategory_id|h}-->" id="plg_productoptions_<!--{$product_index}-->_<!--{$option_id}-->" />
    <input type="hidden" name="plg_productoptions_disp[<!--{$product_index}-->][<!--{$option_id}-->]" value="<!--{$option.option_name|h}-->:<!--{$option.optioncategory_name|h}-->" id="plg_productoptions_disp<!--{$product_index}-->_<!--{$smarty.foreach.option_loop.index}-->" />
    <!--{/foreach}-->
    <!--{/if}-->                
    <input type="hidden" name="product_name[<!--{$product_index}-->]" value="<!--{$arrForm.product_name.value[$product_index]|h}-->" id="product_name_<!--{$product_index}-->" />
    <input type="hidden" name="classcategory_name1[<!--{$product_index}-->]" value="<!--{$arrForm.classcategory_name1.value[$product_index]|h}-->" id="classcategory_name1_<!--{$product_index}-->" />
    <input type="hidden" name="classcategory_name2[<!--{$product_index}-->]" value="<!--{$arrForm.classcategory_name2.value[$product_index]|h}-->" id="classcategory_name2_<!--{$product_index}-->" />
    <br />
    <a class="btn-normal" href="javascript:;" name="change" onclick="win03('<!--{$smarty.const.ROOT_URLPATH}--><!--{$smarty.const.ADMIN_DIR}-->order/product_select.php?no=<!--{$product_index}-->&amp;order_id=<!--{$arrForm.order_id.value|h}-->', 'search', '615', '500');
            return false;">変更</a>
    <!--{if count($arrForm.quantity.value) > 1}-->
    <a class="btn-normal" href="javascript:;" name="delete" onclick="fnSetFormVal('form1', 'delete_no', <!--{$product_index}--> ); fnModeSubmit('delete_product', 'anchor_key', 'order_products'); return false;">削除</a>
    <!--{/if}-->
    <input type="hidden" name="product_type_id[<!--{$product_index}-->]" value="<!--{$arrForm.product_type_id.value[$product_index]|h}-->" id="product_type_id_<!--{$product_index}-->" />
    <input type="hidden" name="product_id[<!--{$product_index}-->]" value="<!--{$arrForm.product_id.value[$product_index]|h}-->" id="product_id_<!--{$product_index}-->" />
    <input type="hidden" name="product_class_id[<!--{$product_index}-->]" value="<!--{$arrForm.product_class_id.value[$product_index]|h}-->" id="product_class_id_<!--{$product_index}-->" />
    <input type="hidden" name="point_rate[<!--{$product_index}-->]" value="<!--{$arrForm.point_rate.value[$product_index]|h}-->" id="point_rate_<!--{$product_index}-->" />
</td>
