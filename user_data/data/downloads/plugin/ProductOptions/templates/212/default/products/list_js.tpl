<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<script type="text/javascript">//<![CDATA[
    <!--{$tpl_productoptions_js}-->
            var option_charge = new Array();
    var option_points = new Array();

    function setOptionPriceOnChange(product_id, Obj, option_id, action) {
        optioncategory_id = parseInt(Obj.options[Obj.selectedIndex].value);
        setOptionPrice(product_id, optioncategory_id, option_id, action);
    }

    function setOptionPriceOnChangeRadio(product_id, Obj, option_id, action) {
        optioncategory_id = $(Obj).val();
        setOptionPrice(product_id, optioncategory_id, option_id, action);
    }

    function setOptionPrice(product_id, optioncategory_id, option_id, action) {
        optioncategory_id = parseInt(optioncategory_id);
        option_id = parseInt(option_id);

        if (action == 1) {
            var $form = $('form[name=product_form' + product_id + ']');

            classcat_id1 = $form.find('select[name=classcategory_id1]').val();
            if (classcat_id1) {
                classcat_id2 = $form.find('select[name=classcategory_id2]').val();
                classcat_id2 = classcat_id2 ? classcat_id2 : '0';
                var classcat2 = productsClassCategories[product_id][classcat_id1]['#' + classcat_id2];
            }

            option_value = parseInt(values[product_id][option_id][optioncategory_id]['price']);
            option_point = parseInt(values[product_id][option_id][optioncategory_id]['point']);
            if (isNaN(option_value))
                option_value = 0;
            if (isNaN(option_point))
                option_point = 0;
            if (typeof option_charge[product_id] == 'undefined')
                option_charge[product_id] = new Array();
            if (typeof option_points[product_id] == "undefined")
                option_points[product_id] = new Array();
            option_charge[product_id][option_id] = option_value;
            option_points[product_id][option_id] = option_point;
            price01 = '';
            price02 = '';
            point = '';

            var option_price = 0;
            for (option_id in option_charge[product_id]) {
                option_price += parseInt(option_charge[product_id][option_id]);
            }

            // 通常価格
            var $price01_default = $form.find('[id^=price01_default]');
            var $price01_dynamic = $form.find('[id^=price01_dynamic]');
            if (classcat2) {
                price01 = classcat2['price01_exctax'];
            }
            if ((typeof price01 == 'undefined' || price01 == '') && typeof price01_base[product_id] != 'undefined') {
                price01 = String(price01_base[product_id]);
            }
            if (price01 != 'undefined' && price01 != '') {
                price01 = str2number(price01);
                price01 += option_price;
                price01 = price01 + sfTax(price01, tax_rate_base, tax_rule_base);

                $price01_dynamic.text(number_format(price01));
                $price01_default.hide();
                $price01_dynamic.show();
            }


            // 販売価格
            var $price02_default = $form.find('[id^=price02_default]');
            var $price02_dynamic = $form.find('[id^=price02_dynamic]');
            if (classcat2) {
                price02 = classcat2['price02_exctax'];
            }
            if ((typeof price02 == 'undefined' || price02 == '') && typeof price02_base[product_id] != 'undefined') {
                price02 = String(price02_base[product_id]);
            }
            if (price02 != 'undefined' && price02 != '') {
                price02 = str2number(price02);
                price02 += option_price;
                price02 = price02 + sfTax(price02, tax_rate_base, tax_rule_base);

                $price02_dynamic.text(number_format(price02));
                $price02_default.hide();
                $price02_dynamic.show();
            }

            // ポイント
            var $point_default = $form.find('[id^=point_default]');
            var $point_dynamic = $form.find('[id^=point_dynamic]');
            if (classcat2) {
                point = classcat2['point'];
            }
            if ((typeof point == 'undefined' || point == '') && typeof point_base[product_id] != 'undefined') {
                point = String(point_base[product_id]);
            }
            if (point != 'undefined' && point != '') {
                point = str2number(point);
                for (option_id in option_points[product_id]) {
                    point += parseInt(option_points[product_id][option_id]);
                }

                $point_dynamic.text(number_format(point));
                $point_default.hide();
                $point_dynamic.show();
            }
        }
    }

    function number_format(num) {
        return num.toString().replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g, '$1,')
    }

    function str2number(str) {
        return parseInt(str.replace(/,/, ''));
    }

    function sfTax(price, tax, tax_rule) {
        real_tax = tax / 100;
        ret = price * real_tax;
        tax_rule = parseInt(tax_rule);
        switch (tax_rule) {
            // 四捨五入
            case 1:
                $ret = Math.round(ret);
                break;
                // 切り捨て
            case 2:
                $ret = Math.floor(ret);
                break;
                // 切り上げ
            case 3:
                $ret = Math.ceil(ret);
                break;
                // デフォルト:切り上げ
            default:
                $ret = Math.ceil(ret);
                break;
        }
        return $ret;
    }

    function checkStock($form, product_id, classcat_id1, classcat_id2) {
        classcat_id2 = classcat_id2 ? classcat_id2 : '';

        var classcat2;

        // 商品一覧時
        if (typeof productsClassCategories != 'undefined') {
            classcat2 = productsClassCategories[product_id][classcat_id1]['#' + classcat_id2];
        }
        // 詳細表示時
        else {
            classcat2 = classCategories[classcat_id1]['#' + classcat_id2];
        }

        var option_price = 0;
        for (option_id in option_charge[product_id]) {
            option_price += parseInt(option_charge[product_id][option_id]);
        }

        // 商品コード
        var $product_code_default = $form.find('[id^=product_code_default]');
        var $product_code_dynamic = $form.find('[id^=product_code_dynamic]');
        if (classcat2
                && typeof classcat2['product_code'] != 'undefined') {
            $product_code_default.hide();
            $product_code_dynamic.show();
            $product_code_dynamic.text(classcat2['product_code']);
        } else {
            $product_code_default.show();
            $product_code_dynamic.hide();
        }

        // 在庫(品切れ)
        var $cartbtn_default = $form.find('[id^=cartbtn_default]');
        var $cartbtn_dynamic = $form.find('[id^=cartbtn_dynamic]');
        if (classcat2 && classcat2['stock_find'] === false) {

            $cartbtn_dynamic.text('申し訳ございませんが、只今品切れ中です。').show();
            $cartbtn_default.hide();
        } else {
            $cartbtn_dynamic.hide();
            $cartbtn_default.show();
        }

        // 通常価格
        var $price01_default = $form.find('[id^=price01_default]');
        var $price01_dynamic = $form.find('[id^=price01_dynamic]');
        if (classcat2
                && typeof classcat2['price01'] != 'undefined'
                && String(classcat2['price01']).length >= 1) {
            price01 = str2number(classcat2['price01_exctax']);
            price01 += option_price;
            price01 = price01 + sfTax(price01, tax_rate_base, tax_rule_base);

            $price01_dynamic.text(number_format(price01)).show();
            $price01_default.hide();
        } else {
            $price01_dynamic.hide();
            $price01_default.show();
        }

        // 販売価格
        var $price02_default = $form.find('[id^=price02_default]');
        var $price02_dynamic = $form.find('[id^=price02_dynamic]');
        if (classcat2
                && typeof classcat2['price02'] != 'undefined'
                && String(classcat2['price02']).length >= 1) {
            price02 = str2number(classcat2['price02_exctax']);
            price02 += option_price;
            price02 = price02 + sfTax(price02, tax_rate_base, tax_rule_base);

            $price02_dynamic.text(number_format(price02)).show();
            $price02_default.hide();
        } else {
            $price02_dynamic.hide();
            $price02_default.show();
        }

        // ポイント
        var $point_default = $form.find('[id^=point_default]');
        var $point_dynamic = $form.find('[id^=point_dynamic]');
        if (classcat2
                && typeof classcat2['point'] != 'undefined'
                && String(classcat2['point']).length >= 1) {
            point = str2number(classcat2['point']);
            for (option_id in option_points[product_id]) {
                point += parseInt(option_points[product_id][option_id]);
            }

            $point_dynamic.text(number_format(point)).show();
            $point_default.hide();
        } else {
            $point_dynamic.hide();
            $point_default.show();
        }

        // 商品規格
        var $product_class_id_dynamic = $form.find('[id^=product_class_id]');
        if (classcat2
                && typeof classcat2['product_class_id'] != 'undefined'
                && String(classcat2['product_class_id']).length >= 1) {

            $product_class_id_dynamic.val(classcat2['product_class_id']);
        } else {
            $product_class_id_dynamic.val('');
        }
    }
    // カゴに入れる
    function fnInCart(productForm) {
        var searchForm = $("#form1");
        var cartForm = $(productForm);
        // 検索条件を引き継ぐ
        var hiddenValues = ['mode', 'category_id', 'maker_id', 'name', 'orderby', 'disp_number', 'pageno', 'rnd'];
        $.each(hiddenValues, function () {
            // 商品別のフォームに検索条件の値があれば上書き
            if (cartForm.has('input[name=' + this + ']').length != 0) {
                cartForm.find('input[name=' + this + ']').val(searchForm.find('input[name=' + this + ']').val());
            }
            // なければ追加
            else {
                cartForm.append($('<input type="hidden" />').attr("name", this).val(searchForm.find('input[name=' + this + ']').val()));
            }
        });
        // 商品別のフォームを送信
        cartForm.submit();
    }
//]]></script>
