<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<div class="review_btn">
    <!--{if count($arrReview) < $smarty.const.REVIEW_REGIST_MAX}-->
    <!--★新規コメントを書き込む★-->
    <a href="./review.php?product_id=<!--{$arrProduct.product_id}-->" target="_blank" class="btn_sub" />新規コメントを書き込む</a>
<!--{/if}-->
</div>