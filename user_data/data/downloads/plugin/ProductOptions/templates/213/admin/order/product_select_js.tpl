<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<script type="text/javascript">//<![CDATA[
    <!--{$tpl_productoptions_js}-->
            function func_submit2(product_id, class_name1, class_name2) {
                var err_text = '';
                var fm = window.opener.document.form1;
                var fm1 = window.opener.document;
                var class1 = "classcategory_id" + product_id + "_1";
                var class2 = "classcategory_id" + product_id + "_2";

                var class1_id = document.getElementById(class1).value;
                var class2_id = document.getElementById(class2).value;

                var product_class_id = document.getElementById("product_class_id" + product_id).value;
                var opner_product_id = 'add_product_id';
                var opner_product_class_id = 'add_product_class_id';
                var tpl_no = '<!--{$tpl_no}-->';
                var shipping_id = '<!--{$shipping_id}-->';

                if (tpl_no != '') {
                    opner_product_id = 'edit_product_id';
                    opner_product_class_id = 'edit_product_class_id';
                    fm1.getElementById("no").value = escape('<!--{$tpl_no}-->');
                }
                if (shipping_id != '') {
                    fm1.getElementById("select_shipping_id").value = escape('<!--{$shipping_id}-->');
                }
                if (document.getElementById(class1).type == 'select-one' && class1_id == '__unselected') {
                    err_text = class_name1 + "を選択してください。\n";
                }
                if (document.getElementById(class2).type == 'select-one' && class2_id == '') {
                    err_text = err_text + class_name2 + "を選択してください。\n";
                }

                if (!class1_id) {
                    // 規格が存在しない商品の場合
                    err_text = eccube.productsClassCategories[product_id]['__unselected2']['#0']['stock_find'] ? '' : '只今品切れ中です';
                } else if (class1_id && (class1_id != '__unselected') && class2_id && (class2_id != 'undefined')) {
                    // 規格1&規格2の商品の場合
                    err_text = eccube.productsClassCategories[product_id][class1_id]['#' + class2_id]['stock_find'] ? '' : '只今品切れ中です';
                } else if (class1_id && (class1_id != '__unselected') && (typeof eccube.productsClassCategories[product_id][class1_id]['#0'] != 'undefined')) {
                    // 規格1のみの商品の場合
                    err_text = eccube.productsClassCategories[product_id][class1_id]['#0']['stock_find'] ? '' : '只今品切れ中です';
                }

                if (err_text != '') {
                    alert(err_text);
                    return false;
                }

                fm1.getElementById(opner_product_id).value = product_id;
                fm1.getElementById(opner_product_class_id).value = product_class_id;

                for (i in eccube.arrOptionIDs[product_id]) {
                    var option = "plg_productoptions" + product_id + "_" + eccube.arrOptionIDs[product_id][i];
                    var option_ele = document.getElementById(option);
                    if (option_ele) {
                        var optioncategory_id = option_ele.value;
                        var opner_plg_productoptions = "add_plg_productoptions_" + eccube.arrOptionIDs[product_id][i];
                        fm1.getElementById(opner_plg_productoptions).value = optioncategory_id;
                    }
                }

                fm.mode.value = 'select_product_detail';
                fm.anchor_key.value = 'order_products';
                fm.submit();
                window.close();

                return true;
            }
//]]></script>