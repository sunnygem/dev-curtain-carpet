<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->
<!--{if $arrProductOptions|@count > 0}-->
<script type="text/javascript">//<![CDATA[
    <!--{$tpl_productoptions_js}-->
            var option_charge = new Array();
    var option_points = new Array();
    function setOptionPriceOnChange(Obj, option_id, action){
    optioncategory_id = parseInt(Obj.options[Obj.selectedIndex].value);
    setOptionPrice(optioncategory_id, option_id, action);
    }

    function setOptionPriceOnChangeRadio(Obj, option_id, action){
    optioncategory_id = $(Obj).val();
    setOptionPrice(optioncategory_id, option_id, action);
    }

    function setOptionPrice(optioncategory_id, option_id, action){
    optioncategory_id = parseInt(optioncategory_id);
    option_id = parseInt(option_id);
    if (action == 1){
    classcat_id1 = $('select[name=classcategory_id1]').val();
    if (classcat_id1){
    classcat_id2 = $('select[name=classcategory_id2]').val();
    classcat_id2 = classcat_id2 ? classcat_id2 : '0';
    var classcat2 = eccube['classCategories'][classcat_id1]['#' + classcat_id2];
    }

    var $form = $('#form1');
    option_value = parseInt(values[option_id][optioncategory_id]['price']);
    option_point = parseInt(values[option_id][optioncategory_id]['point']);
    if (isNaN(option_value))option_value = 0;
    if (isNaN(option_point))option_point = 0;
    option_charge[option_id] = option_value;
    option_points[option_id] = option_point;
    price01 = '';
    price02 = '';
    point = '';
    var option_price = 0;
    for (option_id in option_charge){
    option_price += parseInt(option_charge[option_id]);
    }

    // 通常価格
    var $price01_default = $form.find('[id^=price01_default]');
    var $price01_dynamic = $form.find('[id^=price01_dynamic]');
    if (classcat2){
    price01 = classcat2['price01_exctax'];
    tax_rate = classcat2['tax_rate'];
    tax_rule = classcat2['tax_rule'];
    }
    if ((typeof price01 == 'undefined' || price01 == '') && typeof price01_base != 'undefined'){
    price01 = String(price01_base);
    tax_rate = tax_rate_base;
    tax_rule = tax_rule_base;
    }
    if (price01 != 'undefined' && price01 != ''){
    price01 = str2number(price01);
    price01 += option_price;
    price01 = price01 + sfTax(price01, tax_rate, tax_rule);
    $price01_dynamic.text(number_format(price01));
    $price01_default.hide();
    $price01_dynamic.show();
    }


    // 販売価格
    var $price02_default = $form.find('[id^=price02_default]');
    var $price02_dynamic = $form.find('[id^=price02_dynamic]');
    if (classcat2){
    price02 = classcat2['price02_exctax'];
    }
    if ((typeof price02 == 'undefined' || price02 == '') && typeof price02_base != 'undefined'){
    price02 = String(price02_base);
    }
    if (price02 != 'undefined' && price02 != ''){
    price02 = str2number(price02);
    price02 += option_price;
    price02 = price02 + sfTax(price02, tax_rate, tax_rule);
    $price02_dynamic.text(number_format(price02));
    $price02_default.hide();
    $price02_dynamic.show();
    }

    // ポイント
    var $point_default = $form.find('[id^=point_default]');
    var $point_dynamic = $form.find('[id^=point_dynamic]');
    if (classcat2){
    point = classcat2['point'];
    }
    if ((typeof point == 'undefined' || point == '') && typeof point_base != 'undefined'){
    point = String(point_base);
    }
    if (point != 'undefined' && point != ''){
    point = str2number(point);
    for (option_id in option_points){
    point += parseInt(option_points[option_id]);
    }

    $point_dynamic.text(number_format(point));
    $point_default.hide();
    $point_dynamic.show();
    }
    }
    }

    function number_format(num) {
    return num.toString().replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g, '$1,')
    }

    function str2number(str){
    return parseInt(str.replace(/,/, ''));
    }

    function sfTax(price, tax, tax_rule) {
    real_tax = tax / 100;
    ret = price * real_tax;
    tax_rule = parseInt(tax_rule);
    switch (tax_rule) {
    // 四捨五入
    case 1:
            $ret = Math.round(ret);
    break;
    // 切り捨て
    case 2:
            $ret = Math.floor(ret);
    break;
    // 切り上げ
    case 3:
            $ret = Math.ceil(ret);
    break;
    // デフォルト:切り上げ
    default:
            $ret = Math.ceil(ret);
    break;
    }
    return $ret;
    }

    eccube.checkStock = function($form, product_id, classcat_id1, classcat_id2) {
    classcat_id2 = classcat_id2 ? classcat_id2 : '';
    var classcat2;
    // 商品一覧時
    if (eccube.hasOwnProperty('productsClassCategories')) {
    classcat2 = eccube['productsClassCategories'][product_id][classcat_id1]['#' + classcat_id2];
    }
    // 詳細表示時
    else {
    classcat2 = eccube['classCategories'][classcat_id1]['#' + classcat_id2];
    }

    // 商品コード
    var $product_code_default = $form.find('[id^=product_code_default]');
    var $product_code_dynamic = $form.find('[id^=product_code_dynamic]');
    if (classcat2
            && typeof classcat2['product_code'] != 'undefined') {
    $product_code_default.hide();
    $product_code_dynamic.show();
    $product_code_dynamic.text(classcat2['product_code']);
    } else {
    $product_code_default.show();
    $product_code_dynamic.hide();
    }

    // 在庫(品切れ)
    var $cartbtn_default = $form.find('[id^=cartbtn_default]');
    var $cartbtn_dynamic = $form.find('[id^=cartbtn_dynamic]');
    if (classcat2 && classcat2['stock_find'] === false) {

    $cartbtn_dynamic.text('申し訳ございませんが、只今品切れ中です。').show();
    $cartbtn_default.hide();
    } else {
    $cartbtn_dynamic.hide();
    $cartbtn_default.show();
    }

    var option_price = 0;
    for (option_id in option_charge){
    option_price += parseInt(option_charge[option_id]);
    }
    if (classcat2
            && typeof classcat2['tax_rate'] != 'undefined'
            && String(classcat2['tax_rate']).length >= 1) {
    tax_rate = classcat2['tax_rate'];
    tax_rule = classcat2['tax_rule'];
    }

    // 通常価格
    var $price01_default = $form.find('[id^=price01_default]');
    var $price01_dynamic = $form.find('[id^=price01_dynamic]');
    if (classcat2
            && typeof classcat2['price01'] != 'undefined'
            && String(classcat2['price01']).length >= 1) {
    price01 = str2number(classcat2['price01_exctax']);
    price01 += option_price;
    price01 = price01 + sfTax(price01, tax_rate, tax_rule);
    $price01_dynamic.text(number_format(price01)).show();
    $price01_default.hide();
    } else {
    $price01_dynamic.hide();
    $price01_default.show();
    }

    // 販売価格
    var $price02_default = $form.find('[id^=price02_default]');
    var $price02_dynamic = $form.find('[id^=price02_dynamic]');
    if (classcat2
            && typeof classcat2['price02'] != 'undefined'
            && String(classcat2['price02']).length >= 1) {
    price02 = str2number(classcat2['price02_exctax']);
    price02 += option_price;
    price02 = price02 + sfTax(price02, tax_rate, tax_rule);
    $price02_dynamic.text(number_format(price02)).show();
    $price02_default.hide();
    } else {
    $price02_dynamic.hide();
    $price02_default.show();
    }

    // ポイント
    var $point_default = $form.find('[id^=point_default]');
    var $point_dynamic = $form.find('[id^=point_dynamic]');
    if (classcat2
            && typeof classcat2['point'] != 'undefined'
            && String(classcat2['point']).length >= 1) {
    point = str2number(classcat2['point']);
    for (option_id in option_points){
    point += parseInt(option_points[option_id]);
    }

    $point_dynamic.text(number_format(point)).show();
    $point_default.hide();
    } else {
    $point_dynamic.hide();
    $point_default.show();
    }

    // 商品規格
    var $product_class_id_dynamic = $form.find('[id^=product_class_id]');
    if (classcat2
            && typeof classcat2['product_class_id'] != 'undefined'
            && String(classcat2['product_class_id']).length >= 1) {

    $product_class_id_dynamic.val(classcat2['product_class_id']);
    } else {
    $product_class_id_dynamic.val('');
    }
    }
//]]></script>

<div class="cart_area">
    <!--{foreach from=$arrProductOptions item=option key=option_id}-->
    <!--{assign var=option_name value=plg_productoptions[`$option_id`]}-->
    <!--{assign var=option_tag_id value=plg_productoptions_`$option_id`}-->
    <!--{assign var=type value=`$option.type`}-->
    <!--{assign var=action value=`$option.action`}-->
    <dl>
        <dt><!--{$option.name|h}-->:</dt>
        <dd>
            <!--{if $type == 1}-->
            <select name="<!--{$option_name}-->" class="data-role-none" onChange="setOptionPriceOnChange(this, <!--{$option_id}--> , <!--{$action}--> );" style="<!--{$plg_productoptions_Err[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->">
                <!--{html_options options=$option.options selected=$arrForm.plg_productoptions.value[$option_id]|default:$option.option_init}-->
            </select>
            <!--{if $option.description_flg == 2 || $option.description_flg == 3}-->
            <ul>
                <!--{foreach from=$option.optioncategory item=optioncategory}-->
                <!--{if $optioncategory.default_flg != 1}-->
                <li>
                    <!--{if strlen($optioncategory.option_image) > 0}-->
                    <p><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$optioncategory.option_image}-->" alt="<!--{$optioncategory.name|h}-->" width="100" /></p>
                    <!--{/if}-->
                    <!--{$optioncategory.name|h}-->
                </li>
                <!--{/if}-->
                <!--{/foreach}-->
            </ul>
            <!--{/if}-->
            <!--{elseif $type == 2}-->
            <!--{if $option.description_flg == 2 || $option.description_flg == 3}-->
            <ul>
                <!--{foreach from=$option.optioncategory item=optioncategory}-->
                <!--{assign var=selected_value value=`$arrForm.plg_productoptions.value[$option_id]`}-->
                <!--{if $selected_value == ""}-->
                <!--{assign var=selected_value value=`$option.option_init`}-->
                <!--{/if}-->
                <li>
                    <label><input type="radio" name="<!--{$option_name|h}-->" class="data-role-none" value="<!--{$optioncategory.optioncategory_id|h}-->" onChange="setOptionPriceOnChangeRadio(this, <!--{$option_id}--> , <!--{$action}--> );" <!--{if $selected_value == $optioncategory.optioncategory_id}-->checked<!--{/if}--> id="<!--{$option_tag_id}-->"/><!--{$optioncategory.name|h}--></label>
                    <!--{if strlen($optioncategory.option_image) > 0}-->
                    <p><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$optioncategory.option_image}-->" alt="<!--{$optioncategory.name|h}-->" width="100" /></p>
                    <!--{if $option.pricedisp_flg == 1}-->
                    <!--{$optioncategory.value|number_format|h}--><!--{if $action==1}-->円<!--{elseif $action==2}-->Pt<!--{/if}-->
                    <!--{/if}-->
                    <!--{/if}-->
                </li>
                <!--{/foreach}-->
            </ul>
            <!--{else}-->
            <!--{html_radios name=$option_name class="data-role-none" options=$option.options selected=$arrForm.plg_productoptions.value[$option_id]|default:$option.option_init onChange="setOptionPriceOnChangeRadio(this,$option_id,$action);" id=$option_tag_id}-->
            <!--{/if}-->
            <!--{elseif $type == 3}-->
            <input type="text" name="<!--{$option_name}-->" value="<!--{$arrForm.plg_productoptions.value[$option_id]|h}-->" class="boxLong text data-role-none" style="<!--{$plg_productoptions_Err[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->">
            <!--{elseif $type == 4}-->
            <textarea name="<!--{$option_name}-->" cols="62" rows="8" class="textarea data-role-none" wrap="hard" style="<!--{$plg_productoptions_Err[$option_id]|sfGetErrorColor}-->" id="<!--{$option_tag_id}-->"><!--{$arrForm.plg_productoptions.value[$option_id]|h}--></textarea>
            <!--{/if}-->
            <!--{if $plg_productoptions_Err[$option_id]}--><br><span class="attention"><!--{$plg_productoptions_Err[$option_id]}--></span><!--{/if}-->
        </dd>
        <!--{if $option.description_flg == 1 || $option.description_flg == 3}-->
        <dd><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/product_options.php?option_id=<!--{$option_id}-->" target="_blank">内容説明</a>
        </dd>
        <!--{/if}-->
    </dl>
    <!--{if $type != 3 && $type != 4}-->
    <script type="text/javascript">//<![CDATA[
        setOptionPrice( <!--{$arrForm.plg_productoptions.value[$option_id]|default:$option.option_init|h}--> , <!--{$option_id|h}--> , <!--{$action|h}--> );
//]]></script>
    <!--{/if}-->
    <!--{/foreach}-->
</div>
<!--{/if}-->
