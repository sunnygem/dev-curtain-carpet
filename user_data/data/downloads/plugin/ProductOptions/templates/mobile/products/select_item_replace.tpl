<!--{*
*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
 *}-->

<!--{strip}-->
<div align="center">数量指定</div>
<hr>

<!--{if $tpl_classcat_find1}-->
<p><!--{$tpl_class_name1}-->は「<!--{$arrClassCat1[$arrForm.classcategory_id1.value]|h}-->」を選択しています。</p>
<!--{if $tpl_classcat_find2}-->
<p><!--{$tpl_class_name2}-->は「<!--{$arrClassCat2[$arrForm.classcategory_id2.value]|h}-->」を選択しています。</p>
<!--{/if}-->
<!--{/if}-->

<!--{foreach from=$arrProductOptions key=key item=item}-->
<!--{if strlen($arrHiddenOptions[$key]) > 0}-->
<!--{assign var=value value=`$arrHiddenOptions[$key]`}-->
<!--{if $item.type == 3 || $item.type == 4}-->
<p><!--{$item.name}-->は「<!--{$value}-->」を入力しています。</p>
<!--{else}-->
<p><!--{$item.name}-->は「<!--{$item.options[$value]}-->」を選択しています。</p>
<!--{/if}-->
<!--{/if}-->
<!--{/foreach}-->

<!--{if $arrErr.quantity != ""}-->
<font color="#FF0000">※数量を入力して下さい｡</font><br>
<!--{/if}-->
<form method="post" action="?">
    <!--{foreach from=$arrHiddenOptions key=option_id item=value}-->
    <input type="hidden" name="plg_productoptions[<!--{$option_id}-->]" value="<!--{$value}-->">
    <!--{/foreach}-->
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->">
    <input type="text" name="quantity" size="3" value="<!--{$arrForm.quantity.value|default:1|h}-->" maxlength=<!--{$smarty.const.INT_LEN}--> istyle="4"><br>
    <input type="hidden" name="mode" value="cart">
    <input type="hidden" name="classcategory_id1" value="<!--{$arrForm.classcategory_id1.value|h}-->">
    <input type="hidden" name="classcategory_id2" value="<!--{$arrForm.classcategory_id2.value|h}-->">
    <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->">
    <input type="hidden" name="product_class_id" value="<!--{$tpl_product_class_id}-->">
    <input type="hidden" name="product_type" value="<!--{$tpl_product_type}-->">
    <center><input type="submit" name="submit" value="かごに入れる"></center>
</form>
<!--{/strip}-->