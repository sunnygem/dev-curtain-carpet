<?php

/*
* Plugin Code : ProductOptions
*
* Copyright (C) 2016 BraTech Co., Ltd. All Rights Reserved.
* http://www.bratech.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

require_once PLUGIN_UPLOAD_REALDIR . "ProductOptions/plg_ProductOptions_Util.php";

/**
 * プラグインのメインクラス
 *
 * @package ProductOptions
 * @author Bratech CO.,LTD.
 * @version $Id: $
 */
class ProductOptions extends SC_Plugin_Base
{

    /**
     * コンストラクタ
     */
    public function __construct(array $arrSelfInfo)
    {
        parent::__construct($arrSelfInfo);
    }

    /**
     * インストール
     * installはプラグインのインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin plugin_infoを元にDBに登録されたプラグイン情報(dtb_plugin)
     * @return void
     */
    function install($arrPlugin)
    {
        // ロゴファイルをhtmlディレクトリにコピーします.
        copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/logo.png", PLUGIN_HTML_REALDIR . $arrPlugin['plugin_code'] . "/logo.png");

        $objQuery = & SC_Query_Ex::getSingletonInstance();

        $objQuery->begin();

        if (DB_TYPE == "pgsql") {
            $objQuery->query("CREATE TABLE plg_productoptions_dtb_option (option_id serial, name text, manage_name text, description text, rank int, creator_id int NOT NULL, create_date timestamp without time zone NOT NULL DEFAULT now(), update_date timestamp without time zone NOT NULL, type smallint DEFAULT 1, action smallint DEFAULT 0, description_flg smallint DEFAULT 0,pricedisp_flg smallint DEFAULT 0, is_required smallint DEFAULT 0, del_flg smallint NOT NULL DEFAULT 0, PRIMARY KEY (option_id))");

            $objQuery->query("CREATE TABLE plg_productoptions_dtb_optioncategory (optioncategory_id serial, name text, option_id int NOT NULL, rank int, creator_id int NOT NULL, create_date timestamp without time zone NOT NULL DEFAULT now(), update_date timestamp without time zone NOT NULL, del_flg smallint NOT NULL DEFAULT 0, value numeric,option_image text,description text, default_flg int DEFAULT 0, init_flg smallint DEFAULT 0, PRIMARY KEY (optioncategory_id))");
            $objQuery->query("ALTER TABLE dtb_products ADD COLUMN plg_productoptions_flg text");
            $objQuery->query("ALTER TABLE dtb_order_detail ADD COLUMN plg_productoptions_flg text");
            $objQuery->query("ALTER TABLE dtb_shipment_item ADD COLUMN plg_productoptions_flg text");
        } elseif (DB_TYPE == "mysql") {
            $objQuery->query("CREATE TABLE plg_productoptions_dtb_option (option_id int auto_increment, name text, manage_name text, description text, rank int, creator_id int NOT NULL, create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, update_date timestamp NOT NULL, type smallint DEFAULT 1, action smallint DEFAULT 0,pricedisp_flg smallint DEFAULT 0, description_flg smallint DEFAULT 0, is_required smallint DEFAULT 0, del_flg smallint NOT NULL DEFAULT 0, PRIMARY KEY (option_id))");

            $objQuery->query("CREATE TABLE plg_productoptions_dtb_optioncategory (optioncategory_id int auto_increment, name text, option_id int NOT NULL, rank int, creator_id int NOT NULL, create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, update_date timestamp NOT NULL, del_flg smallint NOT NULL DEFAULT 0, value real, option_image text, description text, default_flg int DEFAULT 0, init_flg smallint DEFAULT 0, PRIMARY KEY (optioncategory_id))");
            $objQuery->query("ALTER TABLE dtb_products ADD COLUMN plg_productoptions_flg longtext");
            $objQuery->query("ALTER TABLE dtb_order_detail ADD COLUMN plg_productoptions_flg longtext");
            $objQuery->query("ALTER TABLE dtb_shipment_item ADD COLUMN plg_productoptions_flg longtext");
        }

        $objQuery->query("CREATE INDEX plg_productoptions_dtb_optioncategory_option_id ON plg_productoptions_dtb_optioncategory (option_id)");

        $objQuery->update("dtb_plugin", array("free_field1" => '0'), "plugin_code = ?", array("ProductOptions"));

        $objQuery->commit();

        if (plg_ProductOptions_Util::getECCUBEVer() >= 2133) {
            $version = '2133';
        } elseif (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
            $version = '213';
        } else {
            $version = '212';
        }

        if (copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/templates/" . $version . "/default/mail_templates/plg_productoptions_order_mail.tpl", TEMPLATE_REALDIR . "mail_templates/plg_productoptions_order_mail.tpl") === false)
            print_r("失敗");
        if (copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/templates/" . $version . "/mobile/mail_templates/plg_productoptions_order_mail.tpl", MOBILE_TEMPLATE_REALDIR . "mail_templates/plg_productoptions_order_mail.tpl") === false)
            print_r("失敗");
    }

    /**
     * アンインストール
     * uninstallはアンインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     * 
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function uninstall($arrPlugin)
    {
        $objQuery = & SC_Query_Ex::getSingletonInstance();
        $objQuery->query("DROP TABLE plg_productoptions_dtb_option");
        $objQuery->query("DROP TABLE plg_productoptions_dtb_optioncategory");
        $objQuery->query("ALTER TABLE dtb_products DROP COLUMN plg_productoptions_flg");
        $objQuery->query("ALTER TABLE dtb_order_detail DROP COLUMN plg_productoptions_flg");
        $objQuery->query("ALTER TABLE dtb_shipment_item DROP COLUMN plg_productoptions_flg");

        SC_Helper_FileManager_Ex::deleteFile(TEMPLATE_REALDIR . "mail_templates/plg_productoptions_order_mail.tpl");
        SC_Helper_FileManager_Ex::deleteFile(MOBILE_TEMPLATE_REALDIR . "mail_templates/plg_productoptions_order_mail.tpl");
    }

    /**
     * 稼働
     * enableはプラグインを有効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function enable($arrPlugin)
    {
        copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/admin/products/options.php", HTML_REALDIR . ADMIN_DIR . "products/options.php");
        copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/admin/products/optioncategory.php", HTML_REALDIR . ADMIN_DIR . "products/optioncategory.php");
        copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/admin/products/product_option.php", HTML_REALDIR . ADMIN_DIR . "products/product_option.php");
        copy(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/products/product_options.php", HTML_REALDIR . "products/product_options.php");
        
        $mode = fileperms(HTML_REALDIR . ADMIN_DIR . "products/index.php");
        if($mode != false){
            chmod(HTML_REALDIR . ADMIN_DIR . "products/options.php", $mode);
            chmod(HTML_REALDIR . ADMIN_DIR . "products/optioncategory.php", $mode);
            chmod(HTML_REALDIR . ADMIN_DIR . "products/product_option.php", $mode);
            chmod(HTML_REALDIR . "products/product_options.php", $mode);
        }

        $objQuery = SC_Query_Ex::getSingletonInstance();
        // dtb_csvテーブルにレコードを追加
        $sqlval_dtb_csv = array();
        $max = $objQuery->max('no', 'dtb_csv') + 1;
        $next = $objQuery->nextVal('dtb_csv_no');
        if ($max > $next) {
            $no = $max;
        } else {
            $no = $next;
        }
        $sqlval_dtb_csv['no'] = $no;
        $sqlval_dtb_csv['csv_id'] = 1;
        $sqlval_dtb_csv['col'] = 'plg_productoptions_flg';
        $sqlval_dtb_csv['disp_name'] = 'オプション設定';
        $sqlval_dtb_csv['rw_flg'] = 1;
        $sqlval_dtb_csv['status'] = 2;
        $sqlval_dtb_csv['create_date'] = 'CURRENT_TIMESTAMP';
        $sqlval_dtb_csv['update_date'] = 'CURRENT_TIMESTAMP';
        $sqlval_dtb_csv['mb_convert_kana_option'] = "KVa";
        $sqlval_dtb_csv['size_const_type'] = "MTEXT_LEN";
        $sqlval_dtb_csv['error_check_types'] = "MAX_LENGTH_CHECK";
        $objQuery->insert("dtb_csv", $sqlval_dtb_csv);

        $masterData = new SC_DB_MasterData_Ex();
        $masterData->updateMasterData("mtb_mail_tpl_path", array('name', 'name'), array('mail_templates/order_mail.tpl' => 'mail_templates/plg_productoptions_order_mail.tpl'));
        $masterData->createCache('mtb_mail_tpl_path');
    }

    /**
     * 停止
     * disableはプラグインを無効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function disable($arrPlugin)
    {
        SC_Helper_FileManager_Ex::deleteFile(HTML_REALDIR . ADMIN_DIR . "products/options.php");
        SC_Helper_FileManager_Ex::deleteFile(HTML_REALDIR . ADMIN_DIR . "products/optioncategory.php");
        SC_Helper_FileManager_Ex::deleteFile(HTML_REALDIR . ADMIN_DIR . "products/product_option.php");
        SC_Helper_FileManager_Ex::deleteFile(HTML_REALDIR . "products/product_options.php");

        $objQuery = SC_Query_Ex::getSingletonInstance();
        // dtb_csvテーブルからレコードを削除
        $objQuery->delete("dtb_csv", "col = ?", array('plg_productoptions_flg'));

        $masterData = new SC_DB_MasterData_Ex();

        $masterData->updateMasterData("mtb_mail_tpl_path", array('name', 'name'), array('mail_templates/plg_productoptions_order_mail.tpl' => 'mail_templates/order_mail.tpl'));
        $masterData->createCache('mtb_mail_tpl_path');
    }

    /**
     * 処理の介入箇所とコールバック関数を設定
     * registerはプラグインインスタンス生成時に実行されます
     * 
     * @param SC_Helper_Plugin $objHelperPlugin 
     */
    function register(SC_Helper_Plugin $objHelperPlugin)
    {
        if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
            $version = '213';
        } else {
            $version = '212';
        }
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Products_List_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Products_Detail_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Cart_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_FrontParts_Bloc_Cart_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Shopping_Multiple_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Shopping_Confirm_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Mypage_History_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Mypage_Order_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Admin_Order_Edit_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Admin_Order_Disp_Ex.php';
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $version . '/plg_ProductOptions_LC_Page_Admin_Order_ProductSelect_Ex.php';
        if (plg_ProductOptions_Util::getECCUBEVer() >= 2133) {
            $tpl_version = '2133';
        } else {
            $tpl_version = $version;
        }
        require_once PLUGIN_UPLOAD_REALDIR . 'ProductOptions/class/' . $tpl_version . '/plg_ProductOptions_LC_Template_Ex.php';

        $objHelperPlugin->addAction('prefilterTransform', array('plg_ProductOptions_LC_Template_Ex', 'prefilterTransform'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("loadClassFileChange", array(&$this, "loadClassFileChange"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("SC_FormParam_construct", array(&$this, "addParam"), $this->arrSelfInfo['priority']);
        if (plg_ProductOptions_Util::getCartInFlg() != 1) {
            $objHelperPlugin->addAction('LC_Page_Products_List_action_before', array('plg_ProductOptions_LC_Page_Products_List_Ex', 'before'), $this->arrSelfInfo['priority']);
            $objHelperPlugin->addAction('LC_Page_Products_List_action_after', array('plg_ProductOptions_LC_Page_Products_List_Ex', 'after'), $this->arrSelfInfo['priority']);
        }
        $objHelperPlugin->addAction('LC_Page_Products_Detail_action_before', array('plg_ProductOptions_LC_Page_Products_Detail_Ex', 'before'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Products_Detail_action_after', array('plg_ProductOptions_LC_Page_Products_Detail_Ex', 'after'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Cart_action_after', array('plg_ProductOptions_LC_Page_Cart_Ex', 'after'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_FrontParts_Bloc_Cart_action_after", array('plg_ProductOptions_LC_Page_FrontParts_Bloc_Cart_Ex', "after"), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Shopping_Multiple_action_before", array('plg_ProductOptions_LC_Page_Shopping_Multiple_Ex', 'before'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction("LC_Page_Shopping_Multiple_action_after", array('plg_ProductOptions_LC_Page_Shopping_Multiple_Ex', 'after'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Shopping_Confirm_action_after', array('plg_ProductOptions_LC_Page_Shopping_Confirm_Ex', 'after'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Mypage_History_action_after', array('plg_ProductOptions_LC_Page_Mypage_History_Ex', 'after'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Mypage_Order_action_before', array('plg_ProductOptions_LC_Page_Mypage_Order_Ex', 'before'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Admin_Order_Edit_action_before', array('plg_ProductOptions_LC_Page_Admin_Order_Edit_Ex', 'before'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Admin_Order_Edit_action_after', array('plg_ProductOptions_LC_Page_Admin_Order_Edit_Ex', 'after'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Admin_Order_ProductSelect_action_after', array('plg_ProductOptions_LC_Page_Admin_Order_ProductSelect_Ex', 'after'), $this->arrSelfInfo['priority']);
        $objHelperPlugin->addAction('LC_Page_Admin_Order_Disp_action_after', array('plg_ProductOptions_LC_Page_Admin_Order_Disp_Ex', 'after'), $this->arrSelfInfo['priority']);
    }

    function loadClassFileChange(&$classname, &$classpath)
    {
        if (plg_ProductOptions_Util::getECCUBEVer() >= 2130) {
            $version = '213';
        } else {
            $version = '212';
        }
        if ($classname == 'SC_Helper_Purchase_Ex') {
            $classpath = PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/" . $version . "/plg_ProductOptions_SC_Helper_Purchase_Ex.php";
            $classname = "plg_ProductOptions_SC_Helper_Purchase_Ex";
        }
        if ($classname == 'SC_Helper_Mail_Ex') {
            $classpath = PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_SC_Helper_Mail.php";
            $classname = "plg_ProductOptions_SC_Helper_Mail";
        }
        if ($classname == 'SC_Fpdf_Ex') {
            $classpath = PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/" . $version . "/plg_ProductOptions_SC_Fpdf_Ex.php";
            $classname = "plg_ProductOptions_SC_Fpdf_Ex";
        }
        if ($classname == 'SC_CartSession_Ex') {
            $classpath = PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_SC_CartSession.php";
            $classname = "plg_ProductOptions_SC_CartSession";
        }
        if ($classname == 'SC_Product_Ex') {
            $classpath = PLUGIN_UPLOAD_REALDIR . "ProductOptions/class/plg_ProductOptions_SC_Product.php";
            $classname = "plg_ProductOptions_SC_Product";
        }
    }

    function addParam($class_name, $param)
    {
        if (strpos($class_name, 'LC_Page_Products_List') !== false) {
            plg_ProductOptions_Util::addProductOptionsParam($param);
        }
        if (strpos($class_name, 'LC_Page_Products_Detail') !== false) {
            plg_ProductOptions_Util::addProductOptionsParam($param);
            if (SC_Display_Ex::detectDevice() == DEVICE_TYPE_MOBILE) {
                $param->addParam("option_id", 'option_id', INT_LEN, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
            }
        }
        if (strpos($class_name, 'LC_Page_Admin_Order_Edit') !== false) {
            plg_ProductOptions_Util::addProductOptionsParam($param);
            $param->addParam('商品オプション', 'shipment_plg_productoptions_flg');
        }
        if (strpos($class_name, 'LC_Page_Admin_Order_Disp') !== false) {
            plg_ProductOptions_Util::addProductOptionsParam($param);
        }
    }

}

?>