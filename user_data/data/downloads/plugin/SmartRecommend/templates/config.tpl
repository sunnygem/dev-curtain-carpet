<!--{*
 *
 * This file is part of the SmartRecommend
 *
 * Copyright (C) 2017 bitmop, Inc.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *}-->

<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_header.tpl"}-->
<script type="text/javascript">
</script>

<h2><!--{$tpl_subtitle}--></h2>
<form name="plg_form" id="plg_form" method="post" action="<!--{$smarty.server.REQUEST_URI|h}-->">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="edit">
<p><!--{$tpl_note}--><br/>
    <br/>
</p>

<table border="0" cellspacing="1" cellpadding="8" summary=" ">
    <!-- 詳細タイトル -->
    <tr>
        <td colspan="2" bgcolor="#f3f3f3">▼<!--{$tpl_subtitle}--> 詳細</td>
    </tr>
    <!-- TextBox -->
    <tr>
        <!--{assign var=key value="peers_mode"}-->
        <td bgcolor="#f3f3f3">どの区切りで「同世代」と呼ぶか<span class="red">※</span></td>
        <td>
            <!--{if $arrErr[$key]}--><span class="red"><!--{$arrErr[$key]}--></span><br /><!--{/if}-->
            <input type="radio" name="<!--{$key}-->" value="1"<!--{if $arrForm[$key] == 1}--> checked="checked"<!--{/if}--> >誕生日から±N年で区切る<br />
            <input type="radio" name="<!--{$key}-->" value="2"<!--{if $arrForm[$key] == 2}--> checked="checked"<!--{/if}--> >何十代(30代とか40代のように)として、10歳ごとに区切る<br />
        </td>
    </tr>
    <!-- TextBox -->
    <tr>
        <!--{assign var=key value="peers_mode_1_year"}-->
        <td bgcolor="#f3f3f3">誕生日から±N年で区切る場合の年数<span class="red">※</span></td>
        <td>
            <!--{assign var=key value="peers_mode_1_year"}-->
            <!--{if $arrErr[$key]}--><span class="red"><!--{$arrErr[$key]}--></span><br /><!--{/if}-->
            <input type="text" class="box6" name="<!--{$key}-->" value="<!--{$arrForm[$key]|h}-->" >[年]
        </td>
    </tr>
    <!-- TextBox -->
    <tr>
        <!--{assign var=key value="recommend_type"}-->
        <td bgcolor="#f3f3f3">抽出された商品を並び替えるキー<span class="red">※</span></td>
        <td>
            <!--{if $arrErr[$key]}--><span class="red"><!--{$arrErr[$key]}--></span><br /><!--{/if}-->
            <input type="radio" name="<!--{$key}-->" value="1"<!--{if $arrForm[$key] == 1}--> checked="checked"<!--{/if}--> >個数<br />
            <input type="radio" name="<!--{$key}-->" value="2"<!--{if $arrForm[$key] == 2}--> checked="checked"<!--{/if}--> >合計販売金額(売価×個数)<br />
            <input type="radio" name="<!--{$key}-->" value="3"<!--{if $arrForm[$key] == 3}--> checked="checked"<!--{/if}--> >購入された件数(回数)<br />
        </td>
    </tr>

    <!-- TextBox -->
    <tr>
        <!--{assign var=key value="classification_by_sex"}-->
        <td bgcolor="#f3f3f3">性別によるリコメンドの分類<span class="red">※</span></td>
        <td>
            <!--{if $arrErr[$key]}--><span class="red"><!--{$arrErr[$key]}--></span><br /><!--{/if}-->
            <input type="radio" name="<!--{$key}-->" value="1"<!--{if $arrForm[$key] == 1}--> checked="checked"<!--{/if}--> >男女の受注結果を分けて分類<br />
            <input type="radio" name="<!--{$key}-->" value="2"<!--{if $arrForm[$key] == 2}--> checked="checked"<!--{/if}--> >男女の受注結果を分けずに分類<br />
        </td>
    </tr>

    <!-- TextBox -->
    <tr>
        <!--{assign var=key value="display_count"}-->
        <td bgcolor="#f3f3f3">スマートリコメンドブロックに表示する商品の件数<span class="red">※</span></td>
        <td>
            <!--{assign var=key value="display_count"}-->
            <!--{if $arrErr[$key]}--><span class="red"><!--{$arrErr[$key]}--></span><br /><!--{/if}-->
            <input type="text" class="box6" name="<!--{$key}-->" value="<!--{$arrForm[$key]|h}-->" >[時]
        </td>
    </tr>

    <!-- TextBox -->
    <tr>
        <!--{assign var=key value="block_display_title"}-->
        <td bgcolor="#f3f3f3">スマートリコメンドブロックの表示タイトル<span class="red">※</span></td>
        <td>
            <!--{assign var=key value="block_display_title"}-->
            <!--{if $arrErr[$key]}--><span class="red"><!--{$arrErr[$key]}--></span><br /><!--{/if}-->
            <input type="text" class="box30" name="<!--{$key}-->" value="<!--{$arrForm[$key]|h}-->" >
        </td>
    </tr>

    <!-- TextBox -->
    <tr>
        <!--{assign var=key value="block_display_title_flg"}-->
        <td bgcolor="#f3f3f3">スマートリコメンドブロックにタイトルを表示<span class="red">※</span></td>
        <td>
            <!--{if $arrErr[$key]}--><span class="red"><!--{$arrErr[$key]}--></span><br /><!--{/if}-->
            <input type="radio" name="<!--{$key}-->" value="1"<!--{if $arrForm[$key] == 1}--> checked="checked"<!--{/if}--> >表示する<br />
            <input type="radio" name="<!--{$key}-->" value="2"<!--{if $arrForm[$key] == 2}--> checked="checked"<!--{/if}--> >表示しない<br />
        </td>
    </tr>

</table>

<div class="btn-area">
    <ul>
        <li>
            <a class="btn-action" href="javascript:;" onclick="document.plg_form.submit();return false;"><span class="btn-next">この内容で登録する</span></a>
        </li>
    </ul>
</div>

</form>
<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_footer.tpl"}-->
<!-- end   NakwebBlocNewProductStatus -->
