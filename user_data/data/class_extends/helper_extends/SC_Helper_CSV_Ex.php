<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'helper/SC_Helper_CSV.php';

/**
 * CSV関連のヘルパークラス(拡張).
 *
 * LC_Helper_CSV をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Helper
 * @author LOCKON CO.,LTD.
 * @version $Id:SC_Helper_DB_Ex.php 15532 2007-08-31 14:39:46Z nanasess $
 */
class SC_Helper_CSV_Ex extends SC_Helper_CSV
{

    /**
     * 文字コンバートの指定が不十分のため、特殊文字が入るとデータが消失する既知のバグ
     * CSV 出力用のファイルポインタリソースを開く
     *
     * @return resource ファイルポインタリソース
     */
    public static function &fopen_for_output_csv($filename = 'php://output')
    {
        $fp = fopen($filename, 'w');

        //stream_filter_append($fp, 'convert.iconv.utf-8/cp932');
        stream_filter_append($fp, 'convert.iconv.utf-8/cp932//TRANSLIT');
        stream_filter_append($fp, 'convert.eccube_lf2crlf');

        return $fp;
    }

}
