// JavaScript Document

$(function(){
    var moveElm = $('nav > ul > li');
    moveTimer = 0;
    hideTimer = 0;

    moveElm.hover(function(){
        var self = $(this),
        selfMdd = self.find('.navContents');

        self.on('mousemove', function(){

            clearTimeout(moveTimer);
            clearTimeout(hideTimer);

            moveTimer = setTimeout(function(){
                self.addClass('mddActive');
                selfMdd.css({display:'block'}).stop().animate({height:'200px',opacity:'1'},200,'swing');

                self.siblings('li').removeClass('mddActive');
                self.siblings('li').find('.navContents').stop().animate({height:'0',opacity:'0'},function(){
                    $(this).css({display:'none'});
                });
            }, 200);
        });
    },function(){
        var self = $(this),
        selfMdd = self.find('.navContents');

        clearTimeout(moveTimer);

        hideTimer = setTimeout(function(){
            self.removeClass('mddActive');
            selfMdd.css({display:'none'});
        }, 200);
    });

    $('nav > ul').hover(function(e){
        e.preventDefault();
    },function(){
        $('nav > ul > li').removeClass('mddActive').find('.navContents').stop().animate({height:'0',opacity:'0'});
    });
});

$(function(){
	/*
        $('#wrap').prepend('<div class="overlay"></div>');*/
        $('.navBtn').click(function() {
                $('.overlay').toggle();
                $('#spNav').toggleClass('navOpen');/*
                $('#wrap').toggleClass('fixed');*/
        });
        $('.overlay').click(function() {
                $(this).fadeOut(300);
                $('#spNav').removeClass('navOpen');/*
                $('#wrap').removeClass('fixed');*/
        });
        $(window).on('load resize', function() {
                var breakpoint = 768;
                if (window.innerWidth > breakpoint) {//  $('#wrap').removeClass('fixed');
                        $('.overlay').hide();
                        $('header nav').css('marginTop', 0 + 'px');
                }
        });

});

$(function(){

	$("#menu dt").on("click", function() {
		$(this).next().slideToggle();
		if ($(this).children(".accordion_icon").hasClass('active')) {
			$(this).children(".accordion_icon").removeClass('active');
		}
		else {
			$(this).children(".accordion_icon").addClass('active');
		}
	});

	$( 'img.lazy' ).lazyload( {
		threshold : 10 ,
		effect : 'fadeIn' ,
		effect_speed: 300 ,
		failure_limit: 2,
	});

    var count = 68;
    $('.voiceComent').each(function() {
        var thisText = $(this).text();
        var textLength = thisText.length;
        if (textLength > count) {
            var showText = thisText.substring(0, count);
            var insertText = showText;
            insertText += '<span class="omit">…</span>';
            $(this).html(insertText);
        };
    });

	$(window).load(function(){
		var $setElm = $('.ticker');
		var effectSpeed = 1000;
		var switchDelay = 3000;
		var easing = 'swing';

		$setElm.each(function(){
			var effectFilter = $(this).attr('rel'); // 'fade' or 'roll' or 'slide'

			var $targetObj = $(this);
			var $targetUl = $targetObj.children('ul');
			var $targetLi = $targetObj.find('li');
			var $setList = $targetObj.find('li:first');

			var ulWidth = $targetUl.width();
			var listHeight = $targetLi.height();
			$targetObj.css({height:(listHeight)});
			$targetLi.css({top:'0',left:'0',position:'absolute'});

			if(effectFilter == 'fade') {
				$setList.css({display:'block',opacity:'0',zIndex:'98'}).stop().animate({opacity:'1'},effectSpeed,easing).addClass('showlist');
				setInterval(function(){
					var $activeShow = $targetObj.find('.showlist');
					$activeShow.animate({opacity:'0'},effectSpeed,easing,function(){
						$(this).next().css({display:'block',opacity:'0',zIndex:'99'}).animate({opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({display:'none',zIndex:'98'}).removeClass('showlist');
					});
				},switchDelay);

			} else if(effectFilter == 'roll') {
				$setList.css({top:'3em',display:'block',opacity:'0',zIndex:'98'}).stop().animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist');
				setInterval(function(){
					var $activeShow = $targetObj.find('.showlist');
					$activeShow.animate({top:'-3em',opacity:'0'},effectSpeed,easing).next().css({top:'3em',display:'block',opacity:'0',zIndex:'99'}).animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist');
				},switchDelay);

			} else if(effectFilter == 'slide') {
				$setList.css({left:(ulWidth),display:'block',opacity:'0',zIndex:'98'}).stop().animate({left:'0',opacity:'1'},effectSpeed,easing).addClass('showlist');
				setInterval(function(){
					var $activeShow = $targetObj.find('.showlist');
					$activeShow.animate({left:(-(ulWidth)),opacity:'0'},effectSpeed,easing).next().css({left:(ulWidth),display:'block',opacity:'0',zIndex:'99'}).animate({left:'0',opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist');
				},switchDelay);
			}
		});
	});

        $("#mainBanner .slider").slick({
            autoplay: true,
            autoplaySpeed: 3000,
            arrows:false,
            dots: true,
            infinite: true,
            centerMode: true,
            variableWidth: true,
            respondTo: 'slider',
            //adaptiveHeight: true,
            responsive: [
            //{
            //    breakpoint: 780,
            //    settings: {
            //        centerMode: false,
            //        variableWidth: false,
            //    }
            //},
            //{
            //	breakpoint: 480,
            //	settings: {
            //		centerMode: false,
            //		variableWidth: false,
            //	}
            //}
            {
                breakpoint: 640,
                settings: {
                    //centerMode: true,
                    variableWidth: false,
                    centerPadding: 0,
                    slidesToShow: 1,
                }
            },
            ]
        });

        $('.list_curtain .slider').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            arrows:false,
            dots: true,
            infinite: true,
            centerMode: true,
            variableWidth: true,
            //slidesToShow: 3,
            respondTo: 'slider',
            responsive: [
                {
                    breakpoint: 640,
                    settings: {
                        //centerMode: true,
                        variableWidth: false,
                        centerPadding: 0,
                        slidesToShow: 1,
                    }
                }
            ]
        });

        //$(".pickupS").slick({
        //    dots: false,
        //    slidesToShow: 3,
        //    slidesToScroll: 3,
        //    arrows:false,
        //    responsive: [
        //    {
        //      breakpoint: 980,
        //      settings: {
        //        centerMode: true,
        //        slidesToShow: 3,
        //        slidesToScroll: 3,
        //      }
        //    },
        //    {
        //      breakpoint: 600,
        //      settings: {
        //        centerMode: true,
        //        slidesToShow: 2,
        //        slidesToScroll: 2
        //      }
        //    },
        //    {
        //      breakpoint: 480,
        //      settings: {
        //        centerMode: true,
        //        slidesToShow: 1,
        //        slidesToScroll: 1
        //      }
        //    }
        //  ]
        //});
    	//$(".rankS").slick({
    	//	dots: false,
    	//	infinite: true,
    	//	slidesToShow: 5,
    	//	slidesToScroll: 5,
    	//	arrows:false,
    	//	responsive: [
    	//{
    	//  breakpoint: 980,
    	//  settings: {
    	//	centerMode: true,
    	//	slidesToShow: 3,
    	//	slidesToScroll: 3,
    	//  }
    	//},
    	//{
    	//  breakpoint: 600,
    	//  settings: {
    	//	centerMode: true,
    	//	slidesToShow: 3,
    	//	slidesToScroll: 3
    	//  }
    	//},
    	//{
    	//  breakpoint: 480,
    	//  settings: {
    	//	centerMode: true,
    	//	slidesToShow: 1,
    	//	slidesToScroll: 1
    	//  }
    	//}
        //  ]
        //});
//		rankSslick.slick('setPosition');

		//  $(".productsSlide").slick({
		//	autoplay: true,
		//	autoplaySpeed: 3000,
		//	dots: false,
		//	infinite: true,
		//	centerMode: true,
		//	slidesToShow: 4,
		//	slidesToScroll: 1
		//  });

    $(window).on('load', function() {
        $(".curtain,.carpet").addClass("module");
        var Module = $(".module");
        Module.hide();
        Module.first().show();
        Module.find().hide();
        $(".tab").click(function(){
            var index = $(this).index();
            $(".tab").removeClass("active");
            $(this).addClass("active");
            $(Module).hide();
            $(Module).eq(index).show();
        });
	});

	$(".thumbnail").on('mouseover touchend',function(){
	  var dataUrl = $(this).attr('data-original');
	  $("#mainImg").attr('src',dataUrl);
	  $("#mainImg").parents(".cboxElement").attr('href',dataUrl);
	});

    var $cartIn = $('.cartin');
    if( $cartIn[0] ){
        $('#to_cart_button').on('click', function(){
            $('body,html').animate({
                scrollTop: $cartIn.offset().top - 60
            }, 500 );
            return false;
        });
    }

	var pageTop = $('#pagetop,#pagetopSp');
	pageTop.hide();
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			pageTop.fadeIn();
		} else {
			pageTop.fadeOut();
		}
	});
	pageTop.click(function() {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	});
    $('a[href^=#]').on('click',function(){
        var speed = 500; // ミリ秒
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        if( target[0] ){
            var position = target.offset().top;
            $('body,html').animate({scrollTop:position}, speed, 'swing');
            return false;
        }
	});
//	var footerCart = $('#footerCartBox');
//    footerCart.hide();
//    $(window).scroll(function() {
//        if (($(this).scrollTop() > 100)&&($(this).scrollTop() < 5000)) {
//            footerCart.fadeIn();
//        } else {
//            footerCart.fadeOut();
//        }
//    });

    //右メニューアコーディオン
    $('#right_navi .side_menu .side_category .category_select dt,#spNavi .spmenu_product .select dt').on('click',function(){
        if ( $(this).hasClass('open') ){
            $(this).next('dd').slideUp();
            $(this).removeClass('open');
        } else {
            $(this).next('dd').slideDown();
            $(this).addClass('open');
        }
    });
    $('#right_navi .side_menu .func_ttl,#spNavi .spmenu_product .select .func_ttl').on('click',function(){
        if ( $(this).hasClass('open') ){
            $(this).next('.func_list').slideUp();
            $(this).removeClass('open');
        } else {
            $(this).next('.func_list').slideDown();
            $(this).addClass('open');
        }
    });
    $('.product_caution dt').on('click',function(){
        if ( $(this).hasClass('open') ){
            $(this).next('dd').children('div').slideUp();
            $(this).removeClass('open');
        } else {
            $(this).next('dd').children('div').slideDown();
            $(this).addClass('open');
        }
    });

    // 続きを読む
    $('.product_status .status_txt .status_detail').trunk8({
        lines: 5,
        fill: '&hellip; <span class="read-more btn">全文表示</span>'
    });
//    $('.feature_remark div').trunk8({
//        lines: 5,
//        fill: '&hellip; <span class="read-more btn">全文表示</span>'
//    });
    $(document).on('click', '.read-more', function(event) {
        $(this).parent().trunk8('revert').append(' <span class="read-less btn">閉じる</span>');
        return false;
    });

    $(document).on('click', '.read-less', function(event) {
        $(this).parent().trunk8();
        return false;
    });


    // 商品詳細スライダー
    $('.product_ga_thumb').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        vertical : true,
        verticalSwiping: true,
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
        //variableWidth: 534;
        responsive: [
            {
                breakpoint: 780,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    vertical : false,
                }
            },
        ]
    });

    // 開きの選択
    var $hirakiRadio = $('#hiraki input[type=radio]');
    var $result_target = $('.final_result');
    var hiraki_denominator;
    $hirakiRadio.on('change', function(){
        $val = $(this).val();
        fnCheckHiraki($val);

        $wide_input.val('').blur();

        var orderW = ( $wide_input.val() !== '') ? parseInt($wide_input.val()) : 0;
        changeWidthFinishment( orderW, $val );
    });
    function fnCheckHiraki( val )
    {
        if( val === '1' ) {
            var str   = '両開き/2枚仕立て';
            var r_str = '両開き';
            hiraki_denominator   = 2;
        }else if( val === '2' ){
            var str   = '片開き/1枚仕立て';
            var r_str = '片開き';
            hiraki_denominator   = 1;
        }
        var $hiraki_target = $('.calc_inner.hiraki');
        $hiraki_target.find('.select_hiraki').text(str);
        $hiraki_target.find('.divider').text(hiraki_denominator);

        $result_target.find('.result01').text(r_str);
        $result_target.find('.result02 .num').text(hiraki_denominator);

        $form=$(this).closest('form');
        product_id=$form.find('input[name=product_id]').val();
        $sele1=$("input[name=classcategory_id1]:checked");
        $sele2=$("select[name=classcategory_id2]");
        eccube.setClassCategories($form, product_id, $sele1, $sele2, '');

    }
    fnCheckHiraki( $hirakiRadio.val() );

    // 自動計算
    var $wide_input  = $('input[name=input_orderwidth]');
    var $wide_output = $('#product_order_wide');
    var $wide_result = $('#product_order_sum_wide');
    $wide_input.on('blur', function(event){
        var input_val = ( $(this).val() !== '' ) ? parseInt($wide_input.val()) : 0;
        var $is_easy_order = $('[name=is_easy_order]');

        var post_val = ( input_val >  0 ) ? Math.ceil( input_val * 1.05 ) : 0;
        // 奇数禁止
        if( post_val % 2 === 1 ) post_val = post_val + 1;

        // サイズ制限のチェック
        var hirakiType = $('#hiraki input[type=radio]:checked').val();

        // 仕上巾
        var finish_val = ( hirakiType === '1' ) ? ( post_val / 2 ) : post_val;

        // 商品のサイズレンジを取得する
        var $rangeSelect = $('select[name=classcategory_id2]'),
            //$maxwidth = Math.max.apply(null, $rangeSelect.find('option[data-maxwidth]').map(function(o){ return $(this).data('maxwidth');})),
            $minwidth = Math.min.apply(null, $rangeSelect.find('option[data-minwidth]').map(function(o){ return $(this).data('minwidth');}));
            console.log($minwidth);

        // 設定された最小幅をチェックする
        if( !$is_easy_order[0] && finish_val > 0 && finish_val < $minwidth ) {
            alert( "オーダー可能な巾：1枚の仕上がりサイズ40cm以上\n→レールの長さが、片開きでは37cm以上、両開きでは75cm以上必要です。" );
            $(this).val('').blur();
        // イージーオーダーの場合
        }else if( $is_easy_order[0] && finish_val > 0 && finish_val < 40 ) {
            alert( "オーダー可能な巾：1枚の仕上がりサイズ40cm以上\n→レールの長さが、片開きでは37cm以上、両開きでは75cm以上必要です。" );
            $(this).val('').blur();
        // 両開きの場合
        }else if(  hirakiType === '1' && post_val > 600 ) {
            alert( "両開きの場合は、\n仕上がりサイズ（ × 1.05）が600cmまでとなります。" );
            $(this).val('').blur();
        // 両開きの場合
        } else if(  hirakiType === '2' && post_val > 300 ) {
            alert( "片開きの場合は、\n仕上がりサイズ（ × 1.05）が300cmまでとなります。" );
            $(this).val('').blur();
        }
        else
        {
            if( input_val > 0 ) {
                $wide_output.text( input_val );
                $wide_result.text( post_val );
                $('.orderwidth').val( post_val ).change();
            } else {
                $wide_output.text( '-' );
                $wide_result.text( '-' );
                //hidden
                $('.orderwidth').val( '-' ).change();
            }
            // 仕上
            changeWidthFinishment( input_val, hirakiType )
        }
    });
    // 巾のダミーインプットの連動
    //$('input[name=input_orderwidth]').on('blur', function(){
    //    var $val = ( $(this).val() !== '') ? parseInt( $(this).val()) : 0;
    //    if( $val > 0 ) {
    //        var target_class = $(this).attr('name').replace('input_', '');
    //        $('.' + target_class).val( post_val ).change();
    //    }
    //});

    var $hiraki_molecule = $('#hiraki_molecule');
    var $hiraki_result   = $('#hiraki_result');
    function changeWidthFinishment( orderW, hirakiVal )
    {
        if( hirakiVal === '1' ) {
            hiraki_denominator   = 2;
        }else if( hirakiVal === '2' ){
            hiraki_denominator   = 1;
        }

        if( orderW > 0 ){
            var orderW_result = Math.ceil( parseInt(orderW) * 1.05 );
            // 奇数禁止
            if( orderW_result % 2 === 1 ) orderW_result = orderW_result + 1;

            $hiraki_molecule.text( orderW_result );

            var hirakiW = Math.ceil( orderW_result / hiraki_denominator );
            $hiraki_result.text( hirakiW );
            // お届け
            $result_target.find('.result02 .width').text( hirakiW );
        } else {
            $hiraki_molecule.text('-');
            $hiraki_result.text( '-' );
            $result_target.find('.result02 .width').text( '-' );
        }
    }

    $('.orderheight').on('blur', function(event){
        var orderheight = parseInt($(this).val());
        $result_target.find('.result02 .height').text( orderheight );

    });

    // タッセルの対応
    $('.option_tassel').on('change', function(e){
        var $thisVal = $(this).val();
        var $target = $('.option_tassel');
        $target.each(function(){
            if( $(this).val() !== $thisVal )
            {
                $(this).prop('checked', false);
            }

            // プラグインのメソッド呼び出し
            if( $(this).prop('checked') === false )
            {
                var option_id = $(this).data('option-id');
                var optioncategory_id = $(this).val();
                option_charge[option_id] = 0;
                for( let k in values[option_id] )
                {
                    if( k !== optioncategory_id )
                    {
                        setOptionPrice( k, option_id, 1 );
                    }
                }

            }
        });
    });

    //ヘッダーロゴ
    var $win  = $(window);
    var $header  = $('header');
    var $spheader  = $('#spHeader');
    if ( $spheader.hasClass('contents') == false ){
        $win.on('scroll load',function(){

                var s = $win.scrollTop();
                var m = 80;

                if( s > m ){
                    $spheader.addClass('contents');
                    $spheader.find('#splogo h1').addClass('family');
                } else {
                    $spheader.find('#splogo h1').removeClass('family');
                    $spheader.removeClass('contents');
                }
        });
    }
    if ( $header.hasClass('contents') == false ){
        $win.on('scroll load',function(){

                var s = $win.scrollTop();
                var m = 80;

                if( s > m ){
                    $header.addClass('contents');
                    $header.find('.logo h1').addClass('family');
                } else {
                    $header.find('.logo h1').removeClass('family');
                    $header.removeClass('contents');
                }
        });
    }

    // 特集スライダー
    $('.feature_slider_thumbnail img').on('click',function(){
        var src = $(this).attr('src');
        $('.feature_slider_main img').attr('src', src);
    });

    if (window.matchMedia( '(max-width: 768px)' ).matches) {
    }
});
/*
$(function(){
	$("#headerNav").append("<li id='magic-line'></li>");

    var $magicLine = $("#magic-line");

    $magicLine
        .width($(".current_page_item").width())
        .css("left", $(".current_page_item a").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());

    $("#headerNav li").find("a").hover(function() {
        $el = $(this);
        leftPos = $el.position().left;
        newWidth = $el.parent().width();

        $magicLine.stop().animate({
            left: leftPos,
            width: newWidth
        });
    }, function() {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft"),
            width: $magicLine.data("origWidth")
        });
    });

});
*/
